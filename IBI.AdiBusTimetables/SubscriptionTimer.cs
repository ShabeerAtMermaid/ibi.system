﻿using IBI.Shared.ConfigSections;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;
using ICSharpCode.SharpZipLib.Zip;
using IBI.Shared.Common;

namespace IBI.AdiBusTimetables
{
    public class SubscriptionTimer
    {
        Log log { get; set; }

        public Timer VdvTimer { get; set; }
        public VdvSubscriptionConfiguration subConfiguration { get; set; }
        bool DataReadyStatus { get; set; } 


        private static Int64 _subscriptionCounter;
        private static Int64 SubscriptionCounter
        {
            get
            {
                if (_subscriptionCounter != null && _subscriptionCounter >= 0)
                {
                    _subscriptionCounter += 1;
                    return _subscriptionCounter;
                }
                else
                {
                    _subscriptionCounter = 1;
                    return _subscriptionCounter;
                }
            }
            set
            {
                _subscriptionCounter = value;
            }
        }

        private static int Counter { get; set; }

        private string LogFileName
        {
            get { return string.Format("Timetables_{0}", Counter); }
        }
        //public CustomerTimer(){}

        public SubscriptionTimer(VdvSubscriptionConfiguration config)
        {
            Counter++;

            this.subConfiguration = config;

            this.VdvTimer = new Timer(TimeSpan.FromSeconds(5 * (Counter % 2) + 1).TotalMilliseconds);
            this.VdvTimer.Elapsed += VdvTimer_Elapsed;
            this.VdvTimer.Enabled = true;

            log = new Log(LogFileName);
        }

        public Int64 SubscriptionId { get; set; }

        #region Events


        public void Dispose()
        {
            this.VdvTimer.Enabled = false;
            this.VdvTimer.Dispose();

        }

        private void VdvTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            int jTimerInterval = 5;
            try
            {

                jTimerInterval = this.subConfiguration.JourneyTimerInterval;

                log.WriteLog("VdvJourneyImporterTimer starts");
                this.VdvTimer.Enabled = false;

                if (AppSettings.HaConJourneyImporterEnabled())
                {
                    VdvJourneyImporter();
                }
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            finally
            {
                this.VdvTimer.Interval = TimeSpan.FromMinutes(jTimerInterval).TotalMilliseconds;
                this.VdvTimer.Enabled = true;
            }
        }

        #endregion


        #region "Functions"


        public void VdvJourneyImporter()
        {
            //CustomerId = .CustomerId;
            //CustomerConfiguration.ServerAddress = new Uri(this.CustomerConfiguration.CustomerConfiguration.ServerAddress);

            log.WriteLog("Hafas Journey Importer Timer Started");

            string prodcutKey = subConfiguration.ProductKey;

            try
            {

                //int customerId = ConfigurationManager.AppSettings["DefaultCustomer"];
                using (new IBI.Shared.CallCounter("Synchronizer.HafasJourneyImporter"))
                {

                    log.WriteLog("Requesting data from Remote Server");

                    int iteration = 1;

                    RealtimeHafasDataService.RealTimeRawdataSoapClient client = new RealtimeHafasDataService.RealTimeRawdataSoapClient();
                    RealtimeHafasDataService.TransferRequest request = new RealtimeHafasDataService.TransferRequest();
                    request.ProductKey = prodcutKey;
                    request.UniqueKey = DateTime.Now.ToString("yyyyMMdd_HHmmss");

                    RealtimeHafasDataService.RawdataPackage[] data = client.GetRawdataPackages(request);

                    if (data.Length > 0)
                    {
                        log.WriteLog(string.Format("Found following Hafas Raw data packages: \n{0}", data.Select(d => d.filename).ToArray().Aggregate((current, next) => current + ", \n" + next)));
                        
                        RealtimeHafasDataService.RawdataPackage dataPackage = data[data.Length - 1];

                        if (subConfiguration.TimetablePackageDate.Length > 0)
                        {
                            for (int i = data.Length-1; i >=0; i--)
                                if (data[i].filename.StartsWith(subConfiguration.TimetablePackageDate))
                                {
                                    dataPackage = data[i];
                                    break;
                                }
                        }
                        
                        
                        string downloadUrl = dataPackage.downloadUrl;

                        String targetDirectory = Path.Combine(Path.GetTempPath(), "Hafas_Timetables\\" + dataPackage.filename.Substring(0, dataPackage.filename.IndexOf('.')));

                        String targetLogName = Path.Combine(targetDirectory, dataPackage.filename);
                        string zipFileName = Path.Combine(targetDirectory, dataPackage.filename);

                        if (Directory.Exists(targetDirectory))
                        {
                            Directory.Delete(targetDirectory, true);
                        }
                                                
                        Directory.CreateDirectory(targetDirectory);

                        //download that zip file
                        log.WriteLog(string.Format("Downloading most recent ZIP package: [{0}]", downloadUrl));
                        IBIWebClient webClient = new IBIWebClient();
                        //webClient.DownloadFile("http://www.example.com/myfile.zip?param1=value1&param2=value2", @"C:\myfile.zip");
                        webClient.DownloadFile(downloadUrl, zipFileName);

                        //extract it to a temp location            
                        log.WriteLog(string.Format("Extracting Zip file : '{0}' to folder : {1}", zipFileName, targetDirectory));

                        var files = ExtractFiles(new FileInfo(zipFileName));

                         HafasDataProcessor hProcessor = new HafasDataProcessor(targetDirectory, subConfiguration, LogFileName, Counter);

                        hProcessor.DoProcess();

                        //end consuming extracted files.

                        // start polling data
                        //PollData(SubscriptionId, ++chunkNo);

                        log.WriteLog(string.Format("Deleting directory [{0}]", targetDirectory));

                        System.Threading.Thread.Sleep(10000);
                        Directory.Delete(targetDirectory, true);


                    }



                    log.WriteLog("Hafas raw data zip file processing completed");
                    //AdibusDBController.ClearZonesCache();
                    //AdibusDBController.ClearSchedulesCache();
                    //AdibusDBController.ClearRouteCache();
                }
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            finally {
                log.WriteLog(string.Format("Next Journey Import will occur after {0} minutes", subConfiguration.JourneyTimerInterval));
            }
        }

       
        #endregion

        #region Private Helper

        private XDocument PerformRequest(Uri uri, XDocument data)
        {
            string encoding = "iso-8859-15";

            byte[] requestData = Encoding.GetEncoding(encoding).GetBytes(data.ToString());

            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "text/xml; charset=" + encoding;
            request.ContentLength = requestData.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
                requestStream.Close();
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
                {
                    return XDocument.Parse(responseReader.ReadToEnd());
                }
            }
        }


        private List<String> ExtractFiles(FileInfo file)
        {
            String targetPath = file.Directory.FullName;

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(file.FullName, targetPath, "");

            //AppUtility.Log2File("LogManager", "  Extracting files");
            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if (String.Compare(Path.GetFileName(file.FullName), Path.GetFileName(logFilePath), true) != 0)
                {
                    fileList.Add(logFilePath);
                    //AppUtility.Log2File("LogManager", "    Extracte file: " + logFilePath);
                }

            }

            return fileList;
        }


        #endregion

    }
}
