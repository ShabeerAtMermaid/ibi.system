﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace IBI.AdiBusTimetables
{
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["IBIDatabaseConnection"].ConnectionString;

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }

         
        #region "HaCon VDV"


        public static bool HaConJourneyImporterEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConJourneyImporter"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["HaConJourneyImporter"]);
            }

            return retVal;


        }
         

        #endregion


         public static bool LogAllMessages()
         {
             bool retVal = true;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
             {
                 retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
             }

             return retVal;
         }



         public class DayLightSavingInfo {
             public TimeSpan Start { get; set; }
             public TimeSpan End { get; set; }
             public int HourDifference { get; set; }

             public DayLightSavingInfo() 
             { 
             //do nothing
             }
         }


         public static DayLightSavingInfo DayLight01
         {
             get
             {
                 DayLightSavingInfo dl = new DayLightSavingInfo();

                 DateTime dlStart = DateTime.ParseExact(ConfigurationManager.AppSettings["DL1Start"], "MM/dd", CultureInfo.InvariantCulture);
                 DateTime dlEnd = DateTime.ParseExact(ConfigurationManager.AppSettings["DL1End"], "MM/dd", CultureInfo.InvariantCulture).AddYears(1).AddDays(1);                 

                 dl.Start = new TimeSpan(dlStart.Ticks);
                 dl.End = new TimeSpan(dlEnd.Ticks);

                 dl.HourDifference = int.Parse(ConfigurationManager.AppSettings["DL1HourDiff"]);

                 return dl;
             }
             set { }
         }


         public static DayLightSavingInfo DayLight02
         {
             get
             {
                 DayLightSavingInfo dl = new DayLightSavingInfo();

                 DateTime dlStart = DateTime.ParseExact(ConfigurationManager.AppSettings["DL2Start"], "MM/dd", CultureInfo.InvariantCulture);
                 DateTime dlEnd = DateTime.ParseExact(ConfigurationManager.AppSettings["DL2End"], "MM/dd", CultureInfo.InvariantCulture).AddYears(1).AddDays(1);

                 dl.Start = new TimeSpan(dlStart.Ticks);
                 dl.End = new TimeSpan(dlEnd.Ticks);

                 dl.HourDifference = int.Parse(ConfigurationManager.AppSettings["DL2HourDiff"]);

                 return dl;
             }
             set { }
         }

         public static DateTime FixTimeZone(string dateString) {
               
             try
             { 
                 DateTime targetDate = DateTime.ParseExact(dateString.Substring(0, dateString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                 string currentZone = DateTime.Now.ToString("zzz"); // "+02:00"
                 int z1Hour = int.Parse(currentZone.Substring(0, currentZone.IndexOf(":")));
                 int z1Min = int.Parse(currentZone.Substring(currentZone.IndexOf(":") + 1));

                 string futureZone = targetDate.ToString("zzz"); //"+01:00";
                 int z2Hour = int.Parse(futureZone.Substring(0, futureZone.IndexOf(":")));
                 int z2Min = int.Parse(futureZone.Substring(futureZone.IndexOf(":") + 1));

                 TimeSpan t1 = new TimeSpan(z1Hour, z1Min, 0);
                 TimeSpan t2 = new TimeSpan(z2Hour, z2Min, 0);

                 targetDate = targetDate.AddMinutes(t2.Subtract(t1).TotalMinutes);

                 return targetDate;
             }
             catch(Exception ex)
             {
                 //do nothing
             }

             return DateTime.ParseExact(dateString.Substring(0, dateString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
         }


    }
}