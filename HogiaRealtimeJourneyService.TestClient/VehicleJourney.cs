﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IBI.DataAccess.DBModels;
using IBI.Shared.Common;
using System.Configuration;


namespace HogiaRealtimeJourneyService.TestClient
{
    public partial class VehicleJourney : Form
    {


        public VehicleJourney()
        {
            InitializeComponent();
        }

        private void VehicleJourney_Load(object sender, EventArgs e)
        {
            FillStopList();
        }

        private void FillStopList()
        {

            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var stops = dbContext.Stops.Where(s => s.StopSource == "MOVIA" && s.StopName.EndsWith("st.")).OrderBy(s => s.StopName).ToList();
                    Stops.DataSource = stops;
                    Stops.DisplayMember = "StopName";
                    Stops.ValueMember = "GID";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problems in fetching stops data");
            }
        }


        private void btnFetchLines_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(VehicleId.Text))
            {
                MessageBox.Show("Vehicle / Bus number required");
                return;
            }

            try
            {
                if (IsCustomGPS.Checked)
                {
                    //using (IBIDataModel dbContext = new IBIDataModel())
                    //{
                    //    var bus = dbContext.Buses.Where(b => b.BusNumber == VehicleId.Text).FirstOrDefault();
                    //    if (bus != null)
                    //    {
                    //        Latitude.Text = bus.LastKnownLocation.Latitude.ToString().Replace(',', '.');
                    //        Longitude.Text = bus.LastKnownLocation.Longitude.ToString().Replace(',', '.');
                    //    }
                    //}

                    if (String.IsNullOrEmpty(Latitude.Text) || String.IsNullOrEmpty(Longitude.Text)) {
                        MessageBox.Show("Both Latitude & Longitude are required");
                        return;
                    }
                }

                if (string.IsNullOrEmpty(Latitude.Text) || string.IsNullOrEmpty(Longitude.Text))
                {
                    if (IsCustomGPS.Checked)
                    {
                        MessageBox.Show("No Live GPS available for this bus");
                    }
                    else
                    {
                        MessageBox.Show("You must select a stop from Stops list");
                    }

                    return;
                }

                RESTManager rest = RESTManager.Instance;
                rest.BasePath = GetMoviaHogiaJourneyServerPath(VehicleId.Text);

                string templateUri = string.Format("/{0}@{1},{2}/lines", VehicleId.Text, Latitude.Text.Replace(',', '.'), Longitude.Text.Replace(',', '.'));

                Response_Lines.Text = rest.GetString(templateUri);

                IBI.Shared.Models.Movia.HogiaTypes.Line[] lines = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.Line[]>(templateUri);

                LinesUrl.Text = rest.BasePath + "/" + templateUri;

                Lines.DataSource = lines;
                Lines.ValueMember = "number";
                Lines.DisplayMember = "designation";

                //Lines.Items.Insert(0, new string{ "Select a Line" });

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("No Lines/Journeys available for this location \n\n [{0}]", ex.Message));
            }

        }


        private void Lines_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Lines.SelectedValue == null)
            {
                return;
            }

            int selectedLine = ((IBI.Shared.Models.Movia.HogiaTypes.Line)Lines.Items[Lines.SelectedIndex]).number;
            string selectedDesignation = ((IBI.Shared.Models.Movia.HogiaTypes.Line)Lines.Items[Lines.SelectedIndex]).designation;

            try
            {
                RESTManager rest = RESTManager.Instance;
                rest.BasePath = GetMoviaHogiaJourneyServerPath(VehicleId.Text);

                string templateUri = string.Format("/{0}@{1},{2}/lines/{3}/journeys", VehicleId.Text, Latitude.Text.Replace(',', '.'), Longitude.Text.Replace(',', '.'), selectedLine);

                Response_Journeys.Text = rest.GetString(templateUri);

                IBI.Shared.Models.Movia.HogiaTypes.Journey[] journeys = rest.Get<IBI.Shared.Models.Movia.HogiaTypes.Journey[]>(templateUri);

                JourneysUrl.Text = rest.BasePath + "/" + templateUri;

                grdJourney.DataSource = journeys;

                Journeys.DataSource = journeys;
                Journeys.ValueMember = "id";
                Journeys.DisplayMember = "id";

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("No Lines/Journeys available for Line '{0}'\n[{1}]", selectedDesignation, ex.Message));
                Journeys.DataSource = null;
            }

        }

        private void IsLiveGPS_CheckedChanged(object sender, EventArgs e)
        {
            Stops.Visible = lblStops.Visible  = !(IsCustomGPS.Checked);

            lblLongitude.Visible = lblLat.Visible = Latitude.Visible = Longitude.Visible = (IsCustomGPS.Checked);

            Stops_SelectedIndexChanged(Stops, new EventArgs());
        }

        private void Stops_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selectedStop = ((IBI.DataAccess.DBModels.Stop)Stops.Items[Stops.SelectedIndex]);
            if (selectedStop != null)
            {
                Latitude.Text = selectedStop.StopGPS.Latitude.ToString();
                Longitude.Text = selectedStop.StopGPS.Longitude.ToString();
            }
        }


        private void btnSignOnJourney_Click(object sender, EventArgs e)
        {
            string journeyId = ((IBI.Shared.Models.Movia.HogiaTypes.Journey)Journeys.Items[Journeys.SelectedIndex]).id;
           // RESTManager.Instance.Post<string>(string.Format("/{0}/assignment/{1}", VehicleId.Text, journeyId), "" , PostFormat.Json;
            RESTManager rest = RESTManager.Instance; 
            rest.BasePath = GetMoviaHogiaJourneyServerPath(VehicleId.Text);

            string templateUri = string.Format("/{0}/assignment/{1}?force={2}", VehicleId.Text, journeyId, chkForced.Checked);

            Response_SignOn.Text = rest.PostString(string.Format("/{0}/assignment/{1}?force={2}", VehicleId.Text, journeyId, chkForced.Checked), new byte[] { });

            string result = rest.Post(templateUri, new byte[] { });

            JourneySignOnUrl.Text = rest.BasePath + "/" + templateUri;

          if(result.StartsWith("SUCCESS"))
            MessageBox.Show(string.Format("Journey SignOn : {0}\nResponse: {1}", "Success", result.Split('|')[1]));
          else
            MessageBox.Show(string.Format("Journey SignOn : {0}\nResponse: {1}", "Failure", result.Split('|')[1]));
        }

        private static Uri GetMoviaHogiaJourneyServerPath(string busNumber)
        {

            int customerId = int.Parse(ConfigurationSettings.AppSettings["DefaultCustomerId"].ToString());

            Uri serverAddress = IBI.Shared.ConfigSections.HogiaRealtimeAssignmentService.GetServerAddress(busNumber, customerId, "HogiaRealtimeJourneyService");
            return serverAddress;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void JourneysUrl_TextChanged(object sender, EventArgs e)
        {

        }

        private void LinesUrl_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void JourneySignOnUrl_TextChanged(object sender, EventArgs e)
        {

        }

        private void Response_SignOn_TextChanged(object sender, EventArgs e)
        {

        }

        private void Response_Journeys_TextChanged(object sender, EventArgs e)
        {

        }

        private void Response_Lines_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Response_Lines_TextChanged_1(object sender, EventArgs e)
        {

        }
 
    }
}
