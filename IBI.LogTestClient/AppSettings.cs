﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

 
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["IBIDatabaseConnection"].ConnectionString;

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }


        public static String GetSQLCEDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["SQLCEDirectory"];

            return resourceDirectory;
        }


        public static Boolean GetLogProcessingEnabled()
        {
            Boolean logProcessingEnabled = Boolean.Parse(ConfigurationManager.AppSettings["LogProcessingEnabled"]);

            return logProcessingEnabled;
        }

        public static int SQLBulkCopyTimeout()
        {
            int retVal = int.Parse(ConfigurationManager.AppSettings["SQLBulkCopyTimeout"]);

            return retVal;

        }

        public static double ScreenshotCleanUpInterval()
        {
            double retVal = 14;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ScreenshotCleanUpAge"]))
            {
                retVal = double.Parse(ConfigurationManager.AppSettings["ScreenshotCleanUpAge"]);
            }

            return retVal;

        }

        public static bool ScheduleSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ScheduleSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["ScheduleSync"]);
            }

            return retVal;
        }

        public static bool AudioSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AudioSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["AudioSync"]);
            }

            return retVal;
        }

        public static bool NovoTicketSynEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NovoTicketSynchronization"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["NovoTicketSynchronization"]);
            }

            return retVal;
        }

        public static bool NavisionTicketSynEnabled()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionTicketSynchronization"])) ? bool.Parse(ConfigurationManager.AppSettings["NavisionTicketSynchronization"]) : false;
        }

        public static int NavisionSupportSyncTimer()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionSupportSyncTimer"])) ? Int32.Parse(ConfigurationManager.AppSettings["NavisionSupportSyncTimer"]) : 15;
        }

        public static int NovoSupportSyncTimer()
        {
            return (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NovoSupportSyncTimer"])) ? Int32.Parse(ConfigurationManager.AppSettings["NovoSupportSyncTimer"]) : 15;
        }


        public static bool ScreenShotCleanupEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ScreenShotCleanup"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["ScreenShotCleanup"]);
            }

            return retVal;
        }

        public static bool DumpStatusEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DumpStatus"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["DumpStatus"]);
            }

            return retVal;
        }

        public static bool BusesSyncEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["BusesSync"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["BusesSync"]);
            }

            return retVal;
        }

        public static bool LogManagerEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogManager"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogManager"]);
            }

            return retVal;
        }

        public static int JourneyStatusUpdateTimerInterval()
        {
            int retVal = 10;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["JourneyStatusUpdateTimerInterval"]))
            {
                retVal = int.Parse(ConfigurationManager.AppSettings["JourneyStatusUpdateTimerInterval"]);
            }

            return retVal;
        }

        public static bool LogAllMessages()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
            }

            return retVal;
        }
    }
 