﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace WebHandlers
{
    public class ScheduleDataHandler : IHttpHandler
    {
        private HttpContext context;

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            this.context = context;

            String json = String.Empty;

            String requestFile = context.Request.Url.AbsolutePath.ToLower();

            if (requestFile.Contains("/"))
                requestFile = requestFile.Substring(requestFile.LastIndexOf("/") + 1);

            switch (requestFile)
            {
                case "schedulelist.json":
                    json = GetScheduleList();
                    break;

                case "scheduledata.json":
                    json = GetSchedule();
                    break;
            }

            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;

            context.Response.Write(json);
        }

        private String GetScheduleList()
        {
            String json = "[";

            String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = @"SELECT
                                            [ScheduleID]
	                                        ,c.[CustomerId]
                                            ,c.[FullName] AS [CustomerName]
                                            ,[Line]
                                            ,[Destination]
                                            ,[ViaName]    
                                      FROM 
	                                        [IBI_Data_2].[dbo].[Schedules] s
		                                        LEFT JOIN
	                                        [IBI_Data_2].[dbo].[Customers] c
		                                        ON s.[CustomerID]=c.[CustomerId]
                                      WHERE c.[CustomerId] IS NOT NULL";

                if (!String.IsNullOrEmpty(context.Request["customerids"]))
                {
                    String[] customerIDs = context.Request["customerids"].Split(',');

                    if (customerIDs.Length > 0)
                    {
                        script += " AND (";

                        for (int i = 0; i < customerIDs.Length; i++)
                        {
                            if (i > 0)
                                script += "OR ";

                            script += "c.[CustomerId]=" + customerIDs[i];

                        }

                        script += ")";
                    }
                }

                DataTable data = new DataTable();

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int rowCounter = 0;

                        while (reader.Read())
                        {
                            json += (rowCounter == 0 ? "" : ",") + "{\"ScheduleID\":" + reader["ScheduleID"].ToString() + ",\"CustomerID\":\"" + reader["CustomerId"].ToString() + "\",\"CustomerName\":\"" + reader["CustomerName"].ToString() + "\",\"Line\":\"" + reader["Line"].ToString() + "\",\"Destination\":\"" + reader["Destination"].ToString() + "\",\"ViaName\":\"" + reader["ViaName"].ToString() + "\"}";

                            rowCounter++;
                        }
                    }
                }
            }

            json += "]";

            return json;
        }

        private String GetSchedule()
        {
            String json = "[";

            if (!String.IsNullOrEmpty(context.Request["scheduleid"]))
            {
                int scheduleid = int.Parse(context.Request["scheduleid"]);

                String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();

                    String script = @"SELECT 
                                        StopInfo.data.value('(@StopNumber)[1]', 'varchar(260)') AS StopNumber
	                                    ,StopInfo.data.value('(StopName/text())[1]', 'varchar(260)') AS StopName
	                                    ,StopInfo.data.value('(GPSCoordinateNS/text())[1]', 'varchar(260)') AS Latitude
	                                    ,StopInfo.data.value('(GPSCoordinateEW/text())[1]', 'varchar(260)') AS Longitude
	                                    ,StopInfo.data.value('(Zone/text())[1]', 'varchar(260)') AS Zone
	                                    ,StopInfo.data.value('for $i in . return count(../*[. << $i]) + 1', 'int') AS SequenceNumber
                                      FROM 
                                        [Schedules]
  		                                    CROSS APPLY 
                                        [ScheduleXML].nodes('/Schedule/JourneyStops/StopInfo') AS StopInfo(data) 
	                                  WHERE [ScheduleID]=" + scheduleid;


                    DataTable data = new DataTable();

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            int rowCounter = 0;

                            while (reader.Read())
                            {
                                json += (rowCounter == 0 ? "" : ",") + "{\"SequenceNumber\":\"" + reader["SequenceNumber"].ToString() + "\",\"StopNumber\":\"" + reader["StopNumber"].ToString() + "\",\"Name\":\"" + reader["StopName"].ToString() + "\",\"Zone\":\"" + reader["Zone"].ToString() + "\",\"Position\":{\"lat\":" + reader["Latitude"].ToString() + ",\"long\":" + reader["Longitude"].ToString() + "}}";

                                rowCounter++;
                            }
                        }
                    }
                }
            }

            json += "]";

            return json;
        }
    }
}