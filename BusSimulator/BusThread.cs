﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Xml;
using System.ServiceModel;
using System.Net;
using System.Xml.Linq;

namespace BusSimulator
{
    public class BusThread
    {
        public int customerid;
        public string siteurl;
        public int busnumber;
        public int schedulenumber;
        public int startwaittime;
        public int stopinterval;
        public int thread_id;
        public int journeynumber;
        public string rootpath;
        public bool stopped;
        public int stopstoskip;
        public string fromName;
        public string destination;
        public string clientRef;
        public string viaName;
        public bool terminat = false;
        public bool journeycomplete = false;

        private string currentstop = string.Empty;
        private decimal currentStopNumber = 0;
        private int currentStopSequence = 0;
        private int currentStopOccurance = 0;

        private int currentstopindex = 0;
        string logPath = String.Empty;
        // This method that will be called when the thread is started
        private void AppendToLog(string message)
        {
            try
            {
                File.AppendAllText(logPath, "\r\n" + DateTime.Now.ToString() + ": " + message);
            }
            catch (Exception ex)
            {
                // ignore
            }
        }

        private void AppendToLog(Exception ex)
        {
            string log = ex.Message + "\r\n" + ex.StackTrace;
            AppendToLog(log);
        }

        private restibi.IJourneyService getService(string serviceurl)
        {
            var myBinding = new BasicHttpBinding();
            var myEndpoint = new EndpointAddress(serviceurl);
            var myChannelFactory = new ChannelFactory<restibi.IJourneyService>(myBinding, myEndpoint);

            restibi.IJourneyService client = null;

            try
            {
                client = myChannelFactory.CreateChannel();
            }
            catch
            {
                if (client != null)
                {
                    ((ICommunicationObject)client).Abort();
                }
            }
            return client;
        }

        private string GetActiveJourney()
        {
            string result = string.Empty;
            clientRef = Guid.NewGuid().ToString();
            WebRequest request = WebRequest.Create(siteurl + "/REST/Journey/JourneyService.svc/GetActiveJourneyByClientRef?scheduleNumber=" + schedulenumber.ToString() + "&customerId=" + customerid.ToString() + "&busNumber=" + busnumber.ToString() + "&clientRef=" + clientRef);
            ((HttpWebRequest)request).UserAgent = "IBI Test Application for Thread: " + thread_id.ToString();
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            result = reader.ReadToEnd();
            result = result.Replace("&lt;", "<").Replace("&gt;", ">");
            //if (result.IndexOf("<?xml version") >= 0)
            //{
            //    result = result.Substring(result.IndexOf("<?xml version"));
            //    result = result.Substring(0, result.IndexOf("</string>"));
            //}
            result = result.Substring(result.IndexOf("<Journey>"));
            result = result.Substring(0, result.IndexOf("</string>"));
            return result;
        }

        private string SaveActiveJourney(int journeyNumber, string stopname, string stopinfo)
        {
            string result = string.Empty;
            WebRequest request = WebRequest.Create(siteurl + "/REST/Journey/JourneyService.svc/SaveActiveJourney?journeyNumber=" + journeynumber.ToString() + "&stopName=" + stopname + "&journeyXml=" + stopinfo);
            ((HttpWebRequest)request).UserAgent = "IBI Test Application for Thread: " + thread_id.ToString();
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            result = reader.ReadToEnd();
            result = result.Replace("&lt;", "<").Replace("&gt;", ">");
            //result = result.Substring(result.IndexOf("<?xml version"));
            //result = result.Substring(0,result.IndexOf("</string>"));

            //</string>
            AppendToLog("SaveActiveJourney" + result);
            return result;
        }

        public void StartBus()
        {
            try
            {
                
                if (startwaittime != 0)
                {
                    Thread.Sleep(startwaittime * 1000);
                }
                Random rnd = new Random();
                rootpath = Path.Combine(rootpath, busnumber.ToString());
                if (!Directory.Exists(rootpath))
                {
                    Directory.CreateDirectory(rootpath);
                }

                string journeyXmlPath = Path.Combine(rootpath, "journey.xml");
                if (File.Exists(journeyXmlPath))
                {
                    File.Delete(journeyXmlPath);
                }
                logPath = Path.Combine(rootpath, "logs.txt");

                //if (File.Exists(logPath))
                //{
                //    File.Delete(logPath);
                //}

                AppendToLog("Calling GetActive Journey");
                string journeyXml = GetActiveJourney();
                AppendToLog("Journey retrieved");
                File.AppendAllText(journeyXmlPath, journeyXml);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(journeyXml);
                XmlNodeList stops = doc.SelectNodes("/Journey/JourneyStops/StopInfo");
                if (stops == null || stops.Count == 0)
                    return;
                journeynumber = Int32.Parse(doc.SelectSingleNode("/Journey/JourneyNumber").InnerText);

                schedulenumber = Int32.Parse(doc.SelectSingleNode("/Journey/ScheduleNumber").InnerText);
                fromName = doc.SelectSingleNode("/Journey/FromName").InnerText;
                destination = doc.SelectSingleNode("/Journey/DestinationName").InnerText;
                clientRef = doc.SelectSingleNode("/Journey/ClientRef") != null ? doc.SelectSingleNode("/Journey/ClientRef").InnerText : string.Empty;
                viaName = doc.SelectSingleNode("/Journey/ViaName") != null ? doc.SelectSingleNode("/Journey/ViaName").InnerText : string.Empty ;

                AppendToLog("Journey number is: " + journeynumber.ToString());
                while (true)
                {
                    if (stopped)
                    {
                        AppendToLog("Journey has been stopped, will try in 3 seconds");
                        Thread.Sleep(3000);
                        continue;
                    }
                    if (terminat)
                    {
                        AppendToLog("Journey has been terminated");
                        break;
                    }
                    
                    currentstopindex = currentstopindex > stops.Count - 1 ? stops.Count - 1 : currentstopindex;
                    // Get stop name
                    XmlNode currentStopNode = stops[currentstopindex];
                    DateTime depature = DateTime.Now;
                    DateTime arrival = depature.Subtract(TimeSpan.FromSeconds(rnd.Next(20, 40)));

                    currentstop = currentStopNode.SelectSingleNode("StopName").InnerText;
                    currentStopSequence = int.Parse(currentStopNode.SelectSingleNode("StopSequence").InnerText);
                    currentStopNumber = decimal.Parse(currentStopNode.Attributes["StopNumber"].Value);

                    currentStopOccurance = FindStopOccurance(journeyXml, currentStopNumber, currentStopSequence);

                    string plannedArrival = currentStopNode.SelectSingleNode("PlannedArrivalTime") != null ? currentStopNode.SelectSingleNode("PlannedArrivalTime").InnerText : string.Empty;
                    string plannedDept = currentStopNode.SelectSingleNode("PlannedDepartureTime") != null ? currentStopNode.SelectSingleNode("PlannedDepartureTime").InnerText : string.Empty;

                    plannedArrival = string.IsNullOrEmpty(plannedArrival) ? arrival.AddSeconds(-15).ToString("yyyy-MM-ddTHH:mm:ss") : DateTime.Parse(plannedArrival).ToString("yyyy-MM-ddTHH:mm:ss");
                    plannedDept = string.IsNullOrEmpty(plannedDept) ? arrival.AddSeconds(-15).ToString("yyyy-MM-ddTHH:mm:ss") : DateTime.Parse(plannedDept).ToString("yyyy-MM-ddTHH:mm:ss");

                    StringBuilder sb = new StringBuilder();

                    sb.Append("<Stop>");
                    sb.Append("<StopId>" + currentStopNode.Attributes["StopNumber"].Value + "</StopId>");
                    sb.Append("<PlanDifference>0</PlanDifference>");
                    sb.Append("<JourneyNumber>"+ journeynumber.ToString() +"</JourneyNumber>");
                    sb.Append("<ClientRef>" + clientRef + "</ClientRef>");
                    sb.Append("<Status>LATE-TEST</Status>");
                    sb.Append("<BusNumber>" + busnumber + "</BusNumber>");
                    sb.Append("<ActualArrival>" + arrival.ToString("yyyy-MM-ddTHH:mm:ss") + "</ActualArrival>");
                    sb.Append("<ActualDeparture></ActualDeparture>");
                    sb.Append("<ScheduleNumber>" + schedulenumber + "</ScheduleNumber>");
                    sb.Append("<PlannedArrival>" + plannedArrival + "</PlannedArrival>");
                    sb.Append("<PlannedDeparture>" + plannedDept + "</PlannedDeparture>");
                    sb.Append("<FromName>" + fromName + "</FromName>");
                    sb.Append("<ViaName>" + viaName + "</ViaName>");
                    sb.Append("<IsLastStop>" + (currentstopindex == (stops.Count - 1)).ToString().ToLower() + "</IsLastStop>");
                    sb.Append("<StopSequence>" + (currentstopindex + 1) + "</StopSequence>");
                    sb.Append("<CustomerId>" + customerid + "</CustomerId>");
                    sb.Append("<StartTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</StartTime>"); //2015-07-27T10:51:20
                    sb.Append("<ExternalReference>Test</ExternalReference>");
                    sb.Append("<StopOccurance>"+currentStopOccurance+"</StopOccurance>");
                    sb.Append("</Stop>");
                    AppendToLog("Sending information for stopname: " + currentstop);
                    AppendToLog(sb.ToString());

                    //if (File.Exists(journeyXmlPath))
                    //{
                    //    File.Delete(journeyXmlPath);
                    //}

                    //File.AppendAllText(journeyXmlPath, SaveActiveJourney(journeynumber, currentstop, sb.ToString()));
                    AppendToLog("Calling SaveActiveJourney for arrival event");
                    SaveActiveJourney(journeynumber, currentstop, sb.ToString());
                    AppendToLog("Response received from server");
                    AppendToLog("Information saved to server");

                    Thread.Sleep(2000);
                    sb = new StringBuilder();


                    sb.Append("<Stop>");
                    sb.Append("<StopId>" + currentStopNode.Attributes["StopNumber"].Value + "</StopId>");
                    sb.Append("<PlanDifference>0</PlanDifference>");
                    sb.Append("<JourneyNumber>" + journeynumber.ToString() + "</JourneyNumber>");
                    sb.Append("<ClientRef>" + clientRef + "</ClientRef>");
                    sb.Append("<Status>LATE-TEST</Status>");
                    sb.Append("<BusNumber>" + busnumber + "</BusNumber>");
                    sb.Append("<ActualArrival>" + arrival.ToString("yyyy-MM-ddTHH:mm:ss") + "</ActualArrival>");
                    sb.Append("<ActualDeparture>" + arrival.AddSeconds(15).ToString("yyyy-MM-ddTHH:mm:ss") + "</ActualDeparture>");
                    sb.Append("<ScheduleNumber>" + schedulenumber + "</ScheduleNumber>");
                    sb.Append("<PlannedArrival>" + plannedArrival + "</PlannedArrival>");
                    sb.Append("<PlannedDeparture>" + plannedDept + "</PlannedDeparture>");
                    sb.Append("<FromName>" + fromName + "</FromName>");
                    sb.Append("<ViaName>" + viaName + "</ViaName>");
                    sb.Append("<IsLastStop>" + (currentstopindex == (stops.Count - 1)).ToString().ToLower() + "</IsLastStop>");
                    sb.Append("<StopSequence>" + (currentstopindex + 1) + "</StopSequence>");
                    sb.Append("<CustomerId>" + customerid + "</CustomerId>");
                    sb.Append("<StartTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</StartTime>"); //2015-07-27T10:51:20
                    sb.Append("<ExternalReference>Test</ExternalReference>");
                    sb.Append("<StopOccurance>" + currentStopOccurance + "</StopOccurance>");
                    sb.Append("</Stop>");

                    AppendToLog("Sending information for stopname: " + currentstop);
                    AppendToLog(sb.ToString());
                    //if (File.Exists(journeyXmlPath))
                    //{
                    //    File.Delete(journeyXmlPath);
                    //}
                    //File.AppendAllText(journeyXmlPath, SaveActiveJourney(journeynumber, currentstop, sb.ToString()));
                    AppendToLog("Calling SaveActiveJourney for departure event");
                    SaveActiveJourney(journeynumber, currentstop, sb.ToString());
                    AppendToLog("Response received from server");
                    AppendToLog("Information saved to server");


                    if (currentstopindex == (stops.Count - 1))
                    {
                        AppendToLog("Journey completed");
                        journeycomplete = true;
                        break;
                    }

                    int nWaitTime = (stopinterval * 1000); // rnd.Next(1, stopinterval) * 1000; //(stopinterval * 1000);// +rnd.Next(1, 3) * 1000;
                    AppendToLog("Waiting for next stop for: " + nWaitTime.ToString() + " milliseconds");
                    // wait for the stop interval PLUS random time between 1-3 minutes
                    Thread.Sleep(nWaitTime);
                    if (stopstoskip == 0)
                    {
                        currentstopindex = currentstopindex + 1;
                    }
                    else
                    {
                        AppendToLog("Skipping stops: " + stopstoskip);
                        currentstopindex = currentstopindex + stopstoskip + 1;
                    }

                }
            }
            catch (Exception ex)
            {
                AppendToLog(ex);
            }

        }

        private int FindStopOccurance(string journeyXml, decimal currentStopNumber, int currentStopSequence)
        {

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(journeyXml);
            writer.Flush();
            stream.Position = 0;
            
            XDocument xDoc = XDocument.Load(stream);

            //xDoc.Descendants()
            IEnumerable<XElement> stops = (from s in xDoc.Descendants("StopInfo")
                                           where (decimal)s.Attribute("StopNumber") == currentStopNumber                                                
                                            select s);

            int count = 1;
            foreach (var s in stops) {
                if (int.Parse(s.Descendants("StopSequence").FirstOrDefault().Value) == currentStopSequence)
                { 
                    break; 
                }

                count++;
            }

            return count;

        }
    }

}
