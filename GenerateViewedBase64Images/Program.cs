﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using IBI.DataAccess.DBModels;
using IBI.Shared;


namespace GenerateViewedBase64Images
{
    class Program
    {
        private static string GetLEDPreviewXmlData(string imagexml)
        {
            string dataXml = string.Empty;
            try
            {
                var doc = XDocument.Parse(imagexml);
                var targetNodes = doc.Descendants("image");
                foreach (var targetNode in targetNodes)
                {
                    Byte[] bytes = AppUtility.ConvertBase64toLEDImage(targetNode.Attribute("base64").Value);
                    string ledbase64 = Convert.ToBase64String(bytes);

                    if (targetNode.Attribute("base64View") != null)
                    {
                        targetNode.Attribute("base64View").Value = ledbase64;
                    }
                    else
                    {
                        XAttribute attribute = new XAttribute("base64View", ledbase64);
                        targetNode.Add(attribute);
                    }

                    bytes = AppUtility.ConvertBase64toLEDImageLarge(targetNode.Attribute("base64").Value);
                    ledbase64 = Convert.ToBase64String(bytes);

                    XAttribute attribute1 = new XAttribute("base64ViewLarge", ledbase64);
                    targetNode.Add(attribute1);

                }
                return string.Concat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", doc.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                //Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
            }
            return dataXml;
        }

        static void Main(string[] args)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    try
                    {
                        foreach (var signData in dbContext.SignDatas)
                        {
                            string LEDPreviewXmlData = GetLEDPreviewXmlData(signData.Picture);
                            signData.ViewedPicture = LEDPreviewXmlData;
                            Console.WriteLine("Sign Id {0} ViewedImages have been created", signData.SignId);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
                    }
                    dbContext.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            Console.WriteLine("Press Any key to terminate program !");
            Console.ReadLine();
        }
        
    }
}
