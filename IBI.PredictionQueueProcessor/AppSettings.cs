﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace IBI.PredictionQueueProcessor
{
    public class AppSettings
    {
        public static bool PredictionQueueEnabled() {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PredictionQueueEnabled"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["PredictionQueueEnabled"]);
            }

            return retVal;
        }

        public static bool PredictionQueueCall2ThirdParty()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["PredictionQueueCall2ThirdParty"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["PredictionQueueCall2ThirdParty"]);
            }

            return retVal;
        }

         public static bool LogAllMessages()
         {
             bool retVal = true;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
             {
                 retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
             }

             return retVal;
         }

        
        
         
    }
}