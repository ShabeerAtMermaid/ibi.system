﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.PredictionQueueProcessor
{
    partial class PredictionQueueService : ServiceBase
    {
        PredictionQueueController serverInstance;
        public PredictionQueueService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            serverInstance = new PredictionQueueController();
        }

        protected override void OnStop()
        {
            serverInstance.Dispose();
        }
    }
}
