﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="InfotainmentAnalyze.aspx.cs" Inherits="CMS_Aspx.InfotainmentAnalyze" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
     <script language="javascript" type="text/javascript">
         function openScreenshots(busnumber, imagename) {
             window.open("ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busnumber + "&imagename=" + imagename, "ScreenshotViewer", "toolbar=0,scrollbars=0,location=0,menubar=0,resizable=0,width=350,height=280");

             return true;
         }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0">
        <tr>
            <td style="width: 100px">
                History date:
            </td>
            <td>
                <asp:TextBox ID="TxtDate" Width="150px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                <asp:ImageButton ID="BtnSearch" runat="server" ImageUrl="~/Images/ButtonSearch.png"
                    OnClick="BtnSearch_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridHistory" runat="server" GridLines="Horizontal" EnableViewState="False">
                    <AlternatingRowStyle BackColor="LightGray" />
                    <HeaderStyle CssClass="TableHeader" />
                    <RowStyle BackColor="WhiteSmoke" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager" runat="Server" />
    <ajaxToolkit:CalendarExtender ID="CtrlHistoryDate" runat="server" TargetControlID="TxtDate"
        Format="dd-MM-yyyy" ViewStateMode="Enabled" />
</asp:Content>
