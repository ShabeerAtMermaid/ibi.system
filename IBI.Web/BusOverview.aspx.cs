﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class BusOverview : System.Web.UI.Page
    {
        #region Variables

        private String _SortExpression = "BusNumber";
        private SortDirection _SortDirection = SortDirection.Ascending;

        #endregion

        #region Enums

        private BusStates CurrentFilter
        {
            get
            {
                BusStates currentFilter = BusStates.Inactive;

                if (this.ChkFilterUnaccounted.Checked)
                    currentFilter |= BusStates.Unaccounted;

                if (this.ChkFilterUnaccountedCritical.Checked)
                    currentFilter |= BusStates.UnaccountedCritical;

                if (this.ChkFilterWithServiceCase.Checked)
                    currentFilter |= BusStates.WithServiceCase;

                if (this.ChkOnline.Checked)
                    currentFilter |= BusStates.Online;

                if (this.ChkFilterWithoutSchedule.Checked)
                    currentFilter |= BusStates.WithoutSchedule;

                return currentFilter;
            }
        }

        [Flags]
        private enum BusStates
        {
            None = 0,
            UnaccountedCritical = 1,
            Unaccounted = 2,
            WithServiceCase = 4,
            Online = 8,
            WithoutSchedule = 16,
            Inactive = 32
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            String sortExp = (String)this.ViewState["_SortExp_"];

            if (!String.IsNullOrEmpty(sortExp))
                _SortExpression = sortExp;

            Object sortDir = this.ViewState["_Direction_"];

            if (sortDir != null)
                _SortDirection = (SortDirection)sortDir;

            String command = Request.QueryString["command"];
            String busNumber = Request.QueryString["busnumber"];

            if (String.Compare("setInOperation", command, true) == 0)
            {
                String inOperation = Request.QueryString["inoperation"];

                this.setInOperation(busNumber, Boolean.Parse(inOperation));

                Response.Redirect("BusOverview.aspx", true);
            }

            //if (!this.IsPostBack)
            //{
            this.GridOverview.PreRender += new EventHandler(GridOverview_PreRender);
            this.GridOverview.Sorting += new GridViewSortEventHandler(GridOverview_Sorting);
            this.GridOverview.RowCreated += new GridViewRowEventHandler(GridOverview_RowCreated);
            this.GridOverview.RowDataBound += new GridViewRowEventHandler(GridOverview_RowDataBound);
            this.GridOverview.DataBound += new EventHandler(GridOverview_DataBound);
            //this.loadData();
            //}
        }

        private int unaccountedCount = 0;
        private int unaccountedCriticalCount = 0;
        private int onlineCount = 0;
        private int serviceCount = 0;
        private int noScheduleCount = 0;
        private int inOperationCount = 0;
        private int inActiveCount = 0;

        private int visibleRows = 0;

        private void GridOverview_DataBound(object sender, EventArgs e)
        {
            this.CllUnaccounted.InnerText = unaccountedCount.ToString();
            this.CllUnaccountedCritical.InnerText = unaccountedCriticalCount.ToString();
            this.CllOnline.InnerText = onlineCount.ToString();
            this.CllService.InnerHtml = serviceCount.ToString();
            this.CllNoSchedule.InnerText = noScheduleCount.ToString();
            this.CllInOperation.InnerText = inOperationCount.ToString();
            this.CllInactive.InnerText = inActiveCount.ToString();
        }

        private void GridOverview_PreRender(object sender, EventArgs e)
        {
            this.loadData();
        }

        private void loadData()
        {
            this.visibleRows = 0;

            DataTable dataSource = new DataTable("BusStatus");
            dataSource.Columns.Add("BusNumber", typeof(int));
            dataSource.Columns.Add("InOperation", typeof(Boolean));
            dataSource.Columns.Add("IsOK", typeof(Boolean));
            dataSource.Columns.Add("LastDCUPing", typeof(DateTime));
            dataSource.Columns.Add("LastVTCPing", typeof(DateTime));
            dataSource.Columns.Add("LastInfotainmentPing", typeof(DateTime));
            dataSource.Columns.Add("LastHotspotPing", typeof(DateTime));
            dataSource.Columns.Add("Journey", typeof(String));
            dataSource.Columns.Add("LastScheduleTime", typeof(DateTime));
            dataSource.Columns.Add("MarkedForService", typeof(Boolean));
            dataSource.Columns.Add("ServiceLevel", typeof(int));
            dataSource.Columns.Add("MarkedForServiceIcon", typeof(String));
            dataSource.Columns.Add("ServiceLevelIcon", typeof(String));

            DataRow dataRow;

            XmlDocument statusDocument = new XmlDocument();
            statusDocument.Load("http://ibi.mermaid.dk/REST/Core/XML/SystemService.svc/SystemStatus");
            //statusDocument.Load("http://localhost:58055/Core/XML/SystemService.svc/SystemStatus");

            //System.Net.HttpWebRequest statusRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://ibi.mermaid.dk/REST/Core/XML/SystemService.svc/SystemStatus");
            //statusRequest.Timeout = 1000 * 60 * 5;

            //System.Net.WebResponse statusResponse = statusRequest.GetResponse();
            //Stream responseStream = statusResponse.GetResponseStream();

            //statusDocument.Load(responseStream);
            //responseStream.Close(); 

            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(statusDocument.NameTable);
            namespaceManager.AddNamespace("ibi", "http://schemas.mermaid.dk/IBI");

            foreach (XmlNode busNode in statusDocument.SelectNodes("ibi:SystemStatus/ibi:BusStatus/ibi:Bus", namespaceManager))
            {
                Boolean inOperation = Boolean.Parse(busNode.SelectSingleNode("ibi:InOperation", namespaceManager).InnerText);
                Boolean markedForService = Boolean.Parse(busNode.SelectSingleNode("ibi:MarkedForService", namespaceManager).InnerText);
                int serviceLevel = int.Parse(busNode.SelectSingleNode("ibi:ServiceLevel", namespaceManager).InnerText);

                dataRow = dataSource.NewRow();
                dataRow["BusNumber"] = int.Parse(busNode.SelectSingleNode("ibi:BusNumber", namespaceManager).InnerText);
                dataRow["InOperation"] = inOperation;
                dataRow["IsOK"] = Boolean.Parse(busNode.SelectSingleNode("ibi:IsOK", namespaceManager).InnerText);
                dataRow["LastDCUPing"] = DateTime.Parse(busNode.SelectSingleNode("ibi:LastDCUPing", namespaceManager).InnerText);
                dataRow["LastVTCPing"] = DateTime.Parse(busNode.SelectSingleNode("ibi:LastVTCPing", namespaceManager).InnerText);
                dataRow["LastInfotainmentPing"] = DateTime.Parse(busNode.SelectSingleNode("ibi:LastInfotainmentPing", namespaceManager).InnerText);
                dataRow["LastHotspotPing"] = DateTime.Parse(busNode.SelectSingleNode("ibi:LastHotspotPing", namespaceManager).InnerText);
                dataRow["Journey"] = long.Parse(busNode.SelectSingleNode("ibi:ScheduledJourney", namespaceManager).InnerText) + " / " + long.Parse(busNode.SelectSingleNode("ibi:CurrentJourney", namespaceManager).InnerText);
                dataRow["LastScheduleTime"] = DateTime.Parse(busNode.SelectSingleNode("ibi:LastScheduleTime", namespaceManager).InnerText);
                dataRow["MarkedForService"] = markedForService;
                dataRow["ServiceLevel"] = serviceLevel;
                
                if(inOperation && markedForService)
                    dataRow["MarkedForServiceIcon"] = "~/Images/Warning_16.png";
                else
                    dataRow["MarkedForServiceIcon"] = "~/Images/transparent.png";

                if (inOperation && serviceLevel > 0)
                    dataRow["ServiceLevelIcon"] = "~/Images/Wrench_16.png";
                else
                    dataRow["ServiceLevelIcon"] = "~/Images/transparent.png";

                dataSource.Rows.Add(dataRow);
            }

            if (!String.IsNullOrEmpty(_SortExpression))
            {
                if (_SortDirection == SortDirection.Ascending)
                    dataSource.DefaultView.Sort = "InOperation DESC, " + _SortExpression + " ASC";
                else
                    dataSource.DefaultView.Sort = "InOperation DESC, " + _SortExpression + " DESC";
            }

            this.GridOverview.DataSource = dataSource;
            this.GridOverview.DataBind();
        }

        private void GridOverview_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (!String.IsNullOrEmpty(_SortExpression))
                    addSortImage(e.Row);
            }
        }

        private void GridOverview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            /*
             * 0: StatusColor
             * 1: InOperation
             * 2: Bus
             * 3: IsOK (Hidden)
             * 4: DCU
             * 5: VTC
             * 6: Infotainment
             * 7: Hotspot
             * 8: Last Schedule
             * 9: Journey
             * */
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    foreach (TableCell headerCell in e.Row.Cells)
                    {
                        headerCell.HorizontalAlign = HorizontalAlign.Left;
                        headerCell.VerticalAlign = VerticalAlign.Middle;
                    }
                }

                if (e.Row.DataItem != null)
                {
                    BusStates currentState = BusStates.None;

                    //Bus states
                    Boolean busOK = false;
                    Boolean busOfflineToLong = false;
                    Boolean busSubSystemDown = false;

                    e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Center;

                    String busNumber = ((DataRowView)e.Row.DataItem).Row["BusNumber"].ToString();
                    Boolean isOK = (Boolean)((DataRowView)e.Row.DataItem).Row["IsOK"];
                    Boolean inOperation = (Boolean)((DataRowView)e.Row.DataItem).Row["InOperation"];
                    DateTime lastDCUPing = DateTime.Parse(((DataRowView)e.Row.DataItem).Row["LastDCUPing"].ToString());
                    DateTime lastVTCPing = DateTime.Parse(((DataRowView)e.Row.DataItem).Row["LastVTCPing"].ToString());
                    DateTime lastInfotainmentPing = DateTime.Parse(((DataRowView)e.Row.DataItem).Row["LastInfotainmentPing"].ToString());
                    DateTime lastHotspotPing = DateTime.Parse(((DataRowView)e.Row.DataItem).Row["LastHotspotPing"].ToString());
                    long scheduledJourney = long.Parse(((DataRowView)e.Row.DataItem).Row["Journey"].ToString().Split('/')[0]);
                    long actualJourney = long.Parse(((DataRowView)e.Row.DataItem).Row["Journey"].ToString().Split('/')[1]);
                    DateTime lastScheduleTime = DateTime.Parse(((DataRowView)e.Row.DataItem).Row["LastScheduleTime"].ToString());
                    Boolean markedForService = (Boolean)((DataRowView)e.Row.DataItem).Row["MarkedForService"];
                    int serviceLevel = int.Parse(((DataRowView)e.Row.DataItem).Row["ServiceLevel"].ToString());
                    
                    System.Drawing.Color statusColor = e.Row.BackColor;
                    System.Drawing.Color textColor = e.Row.ForeColor;

                    Boolean hasServiceCase = (markedForService || serviceLevel > 0);

                    if (inOperation)
                    {
                        inOperationCount++;
                        currentState = BusStates.Online;

                        Boolean criticalStatus = false;

                        //See if something has been offline for a loong time                    
                        if (lastScheduleTime.Year == DateTime.Now.Year && lastScheduleTime.DayOfYear == DateTime.Now.DayOfYear)
                        {
                            if (lastDCUPing.Year > 2000 && ((lastDCUPing.Year == DateTime.Now.Year && lastDCUPing.DayOfYear < DateTime.Now.DayOfYear) || lastDCUPing.Year < DateTime.Now.Year))
                                criticalStatus = true;
                            else if (lastVTCPing.Year > 2000 && ((lastVTCPing.Year == DateTime.Now.Year && lastVTCPing.DayOfYear < DateTime.Now.DayOfYear) || lastVTCPing.Year < DateTime.Now.Year))
                                criticalStatus = true;
                            else if (lastInfotainmentPing.Year > 2000 && ((lastInfotainmentPing.Year == DateTime.Now.Year && lastInfotainmentPing.DayOfYear < DateTime.Now.DayOfYear) || lastInfotainmentPing.Year < DateTime.Now.Year))
                                criticalStatus = true;
                            else if (lastHotspotPing.Year > 2000 && ((lastHotspotPing.Year == DateTime.Now.Year && lastHotspotPing.DayOfYear < DateTime.Now.DayOfYear) || lastHotspotPing.Year < DateTime.Now.Year))
                                criticalStatus = true;

                            if (criticalStatus)
                            {
                                statusColor = System.Drawing.Color.OrangeRed;
                                e.Row.Cells[0].Text = "!!";

                                currentState = BusStates.UnaccountedCritical;

                                if (!hasServiceCase)
                                    unaccountedCriticalCount++;
                            }
                        }

                        if (lastDCUPing.Year > 2000 && lastDCUPing.Add(AppSettings.GetOfflineTolerance()) < lastScheduleTime)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
                        }
                        if (lastVTCPing.Year > 2000 && lastVTCPing.Add(AppSettings.GetOfflineTolerance()) < lastScheduleTime)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[5].ForeColor = System.Drawing.Color.Red;
                        }
                        if (lastInfotainmentPing.Year > 2000 && lastInfotainmentPing.Add(TimeSpan.FromTicks(AppSettings.GetOfflineTolerance().Ticks / 2)) < lastScheduleTime)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[6].ForeColor = System.Drawing.Color.Red;
                        }
                        if (lastHotspotPing.Year > 2000 && lastHotspotPing.Add(AppSettings.GetOfflineTolerance()) < lastScheduleTime)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[7].ForeColor = System.Drawing.Color.Red;
                        }
                        if (AppSettings.ShouldValidateSchedules() && actualJourney != scheduledJourney && scheduledJourney != 0)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                        }
                        if ( scheduledJourney == -1)
                        {
                            busSubSystemDown = true;

                            e.Row.Cells[9].ForeColor = System.Drawing.Color.Red;
                        }
                                               
                        if (!isOK)
                        {
                            if (!criticalStatus)
                            {
                                statusColor = System.Drawing.Color.Red;

                                if (!hasServiceCase)
                                {
                                    unaccountedCount++;
                                    currentState = BusStates.Unaccounted;
                                }
                            }

                        }
                        else if (scheduledJourney == 0)
                        {
                            if (!criticalStatus)
                            {
                                if (busSubSystemDown)
                                    e.Row.Cells[0].Text = "!";

                                statusColor = System.Drawing.Color.LightGreen;

                                if (!hasServiceCase)
                                {
                                    noScheduleCount++;
                                    currentState = BusStates.WithoutSchedule;
                                }
                            }
                        }
                        else
                        {
                            if (!criticalStatus)
                            {
                                if (busSubSystemDown)
                                    e.Row.Cells[0].Text = "!";

                                statusColor = System.Drawing.Color.Green;

                                if (!hasServiceCase)
                                    onlineCount++;
                            }
                        }

                        if (String.Compare("NA", e.Row.Cells[4].Text, true) == 0)
                            e.Row.Cells[4].ForeColor = System.Drawing.Color.Gray;

                        if (String.Compare("NA", e.Row.Cells[5].Text, true) == 0)
                            e.Row.Cells[5].ForeColor = System.Drawing.Color.Gray;

                        if (String.Compare("NA", e.Row.Cells[6].Text, true) == 0)
                            e.Row.Cells[6].ForeColor = System.Drawing.Color.Gray;

                        if (String.Compare("NA", e.Row.Cells[7].Text, true) == 0)
                            e.Row.Cells[7].ForeColor = System.Drawing.Color.Gray;

                        if (hasServiceCase)
                        {
                            statusColor = System.Drawing.Color.FromArgb(255, 201, 14);
                            serviceCount++;
                            currentState = BusStates.WithServiceCase;
                        }
                    }
                    else
                    {
                        inActiveCount++;
                        currentState = BusStates.Inactive;

                        textColor = System.Drawing.Color.Gray;
                    }

                    if (lastDCUPing.Year < 2000)
                        e.Row.Cells[4].Text = "NA";

                    if (lastVTCPing.Year < 2000)
                        e.Row.Cells[5].Text = "NA";

                    if (lastInfotainmentPing.Year < 2000)
                        e.Row.Cells[6].Text = "NA";

                    if (lastHotspotPing.Year < 2000)
                        e.Row.Cells[7].Text = "NA";

                    e.Row.Cells[0].BackColor = statusColor;
                    e.Row.ForeColor = textColor;

                    ((CheckBox)e.Row.FindControl("ChkInOperation")).Attributes.Add("onclick", "setInOperation('" + busNumber + "', " + (!inOperation ? "true" : "false") + ")");
                    ((ImageButton)e.Row.FindControl("BtnScreenshots")).Attributes.Add("onclick", "openScreenshots('" + busNumber + "')"); ;

                    BusStates currentFilter = this.CurrentFilter;

                    if (!((currentFilter & currentState) == currentState))
                    {
                        e.Row.Visible = false;
                    }
                    else
                    {
                        if (this.visibleRows % 2 == 0)
                            e.Row.BackColor = this.GridOverview.RowStyle.BackColor;
                        else
                            e.Row.BackColor = this.GridOverview.AlternatingRowStyle.BackColor;

                        visibleRows++;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void GridOverview_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (!String.IsNullOrEmpty(_SortExpression))
            {
                if (String.Compare(e.SortExpression, _SortExpression, true) == 0)
                {
                    if (_SortDirection == SortDirection.Ascending)
                        _SortDirection = SortDirection.Descending;
                    else
                        _SortDirection = SortDirection.Ascending;
                }
                else
                {
                    _SortDirection = SortDirection.Ascending;
                }
            }

            ViewState["_Direction_"] = _SortDirection;
            ViewState["_SortExp_"] = e.SortExpression;

            _SortExpression = e.SortExpression;
        }

        private void addSortImage(GridViewRow headerRow)
        {
            int tmpIndex = getCellIndex(_SortExpression);

            if (tmpIndex > -1)
            {
                Image tmpSortImage = new Image();

                if (_SortDirection == SortDirection.Ascending)
                {
                    tmpSortImage.ImageUrl = "Images/arrowdown.png";
                    tmpSortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    tmpSortImage.ImageUrl = "Images/arrowup.png";
                    tmpSortImage.AlternateText = "Descending Order";
                }

                headerRow.Cells[tmpIndex].Controls.Add(tmpSortImage);
            }
        }

        private int getCellIndex(String name)
        {
            for (int i = 0; i < this.GridOverview.Columns.Count; i++)
            {
                DataControlField tmpColumn = this.GridOverview.Columns[i];

                if (tmpColumn.GetType() == typeof(BoundField))
                {
                    if (String.Compare(((BoundField)tmpColumn).DataField, name, true) == 0)
                        return i;
                }
            }

            return -1;
        }

        private void setInOperation(String busNumber, Boolean inOperation)
        {
            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "UPDATE [Clients] SET [InOperation]=" + (inOperation ? "1" : "0") + " WHERE [BusNumber]='" + busNumber + "'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        protected void BtnRefresh_Click(object sender, ImageClickEventArgs e)
        {
            //Do nothing - handled in Page_Load
        }

    }
}