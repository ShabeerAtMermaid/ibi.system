﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScreenshotViewer.aspx.cs"
    Inherits="CMS_Aspx.FrmScreenshotViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Screenshots</title>
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <center>
            <table border="0">
                <tr>
                    <td align="left" colspan="3" runat="server">
                        <asp:HyperLink ID="LnkVTC" runat="server">VTC</asp:HyperLink>
                        &nbsp;
                        <asp:HyperLink ID="LnkDCU" runat="server">DCU</asp:HyperLink>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td id="Td1" align="center" colspan="3" runat="server">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3" runat="server" id="CllImageName">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <asp:Image ID="ImgScreenshot" runat="server" Height="188px" Width="300px" ImageUrl="~/Images/NoScreenshot.jpg" />
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 50px">
                        <asp:ImageButton ID="BtnPrevious" runat="server" ImageUrl="~/Images/Image_Previous.png"
                            OnClick="BtnPrevious_Click" />
                    </td>
                    <td align="center" style="width: 200px" runat="server" id="CllImageCount">
                        0 of 0
                    </td>
                    <td align="right" style="width: 50px">
                        <asp:ImageButton ID="BtnNext" runat="server" ImageUrl="~/Images/Image_Next.png" OnClick="BtnNext_Click" />
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
