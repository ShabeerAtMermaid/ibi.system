﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class SystemLog : System.Web.UI.Page
    {
        #region Variables

        private String _SortExpression = "Timestamp";
        private SortDirection _SortDirection = SortDirection.Descending;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IsAdmin"] == null || !(Boolean)Session["IsAdmin"])
            {
                this.LblMessage.Visible = true;
                this.GridLog.Visible = false;

                return;
            }
            else
            {
                this.LblMessage.Visible = false;
                this.GridLog.Visible = true;
            }

            String sortExp = (String)this.ViewState["_SortExp_"];

            if (!String.IsNullOrEmpty(sortExp))
                _SortExpression = sortExp;

            Object sortDir = this.ViewState["_Direction_"];

            if (sortDir != null)
                _SortDirection = (SortDirection)sortDir;

            this.GridLog.PreRender += new EventHandler(GridLog_PreRender);
            this.GridLog.Sorting += new GridViewSortEventHandler(GridLog_Sorting);
            this.GridLog.RowCreated += new GridViewRowEventHandler(GridLog_RowCreated);
            this.GridLog.RowDataBound += new GridViewRowEventHandler(GridLog_RowDataBound);
        }

        private void GridLog_PreRender(object sender, EventArgs e)
        {
            this.loadData();
        }

        private void loadData()
        {
            DataTable dataSource = new DataTable("SystemLog");

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT TOP 500 *" + Environment.NewLine +
                                "FROM [SystemLog]" + Environment.NewLine +
                                "ORDER BY [Timestamp] DESC" + Environment.NewLine;

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSource);
                    }
                }
            }

            if (!String.IsNullOrEmpty(_SortExpression))
            {
                if (_SortDirection == SortDirection.Ascending)
                    dataSource.DefaultView.Sort = _SortExpression + " ASC";
                else
                    dataSource.DefaultView.Sort = _SortExpression + " DESC";
            }

            this.GridLog.DataSource = dataSource;
            this.GridLog.DataBind();
        }

        private void GridLog_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (!String.IsNullOrEmpty(_SortExpression))
                    addSortImage(e.Row);
            }
        }

        private void GridLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    headerCell.HorizontalAlign = HorizontalAlign.Left;
                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }
            }

            if (e.Row.DataItem != null)
            {
                //TODO: Set logentry icon
            }
        }

        private void GridLog_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (!String.IsNullOrEmpty(_SortExpression))
            {
                if (String.Compare(e.SortExpression, _SortExpression, true) == 0)
                {
                    if (_SortDirection == SortDirection.Ascending)
                        _SortDirection = SortDirection.Descending;
                    else
                        _SortDirection = SortDirection.Ascending;
                }
                else
                {
                    _SortDirection = SortDirection.Ascending;
                }
            }

            ViewState["_Direction_"] = _SortDirection;
            ViewState["_SortExp_"] = e.SortExpression;

            _SortExpression = e.SortExpression;
        }

        private void addSortImage(GridViewRow headerRow)
        {
            int tmpIndex = getCellIndex(_SortExpression);

            if (tmpIndex > -1)
            {
                Image tmpSortImage = new Image();

                if (_SortDirection == SortDirection.Ascending)
                {
                    tmpSortImage.ImageUrl = "Images/arrowdown.png";
                    tmpSortImage.AlternateText = "Ascending Order";
                }
                else
                {
                    tmpSortImage.ImageUrl = "Images/arrowup.png";
                    tmpSortImage.AlternateText = "Descending Order";
                }

                headerRow.Cells[tmpIndex].Controls.Add(tmpSortImage);
            }
        }

        private int getCellIndex(String name)
        {
            for (int i = 0; i < this.GridLog.Columns.Count; i++)
            {
                DataControlField tmpColumn = this.GridLog.Columns[i];

                if (tmpColumn.GetType() == typeof(BoundField))
                {
                    if (String.Compare(((BoundField)tmpColumn).DataField, name, true) == 0)
                        return i;
                }
            }

            return -1;
        }
    }
}