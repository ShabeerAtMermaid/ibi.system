﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Xml;
using System.Web.UI;
using System.Web.UI.WebControls;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class Schedules : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridList.RowDataBound += new GridViewRowEventHandler(GridList_RowDataBound);

            //if (!this.IsPostBack)
            this.LoadData();

        }

        private void LoadData()
        {
            DataTable dataSource = new DataTable();

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();

                String script = "SELECT [ScheduleID], [Line], [FromName], [Destination], [ViaName]" + Environment.NewLine +
                                "FROM [Schedules]" + Environment.NewLine +
                                "ORDER BY dbo.StripNonNumeric([Line])";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                    {
                        dataAdapter.Fill(dataSource);
                    }
                }
            }


            //dataSource.DefaultView.Sort = "Line ASC";

            this.GridList.DataSource = dataSource;
            this.GridList.DataBind();
        }

        private void GridList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    headerCell.HorizontalAlign = HorizontalAlign.Left;

                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }
            }
            else if(e.Row.RowType == DataControlRowType.DataRow)
            {
                int scheduleID = int.Parse(((DataRowView)e.Row.DataItem).Row["ScheduleID"].ToString());
                ((ImageButton)e.Row.FindControl("BtnViewSchedule")).Attributes.Add("onclick", "openSchedule('" + scheduleID + "')");
            }
        }

        protected void BtnUploadSchedule_Click(object sender, ImageClickEventArgs e)
        {
            if (this.FileSchedule.HasFile)
            {
                System.IO.MemoryStream memStream = new System.IO.MemoryStream(this.FileSchedule.FileBytes);

                XmlDocument scheduleDocument = new XmlDocument();
                scheduleDocument.Load(memStream);

                //TODO: Validate against XmlSchema

                int customerID = int.Parse(this.txtCustomerID.Text);

                String line = scheduleDocument.SelectSingleNode("Schedule/Line").InnerText;
                String fromName = scheduleDocument.SelectSingleNode("Schedule/FromName").InnerText;
                String destinationName = scheduleDocument.SelectSingleNode("Schedule/DestinationName").InnerText;
                String viaName = scheduleDocument.SelectSingleNode("Schedule/ViaName").InnerText;

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    String script = "IF NOT EXISTS(SELECT * FROM [Schedules] WHERE [CustomerID]=" + customerID + " AND [Line]='" + SafeSqlLiteral(line) + "' AND [FromName]='" + SafeSqlLiteral(fromName) + "' AND [Destination]='" + SafeSqlLiteral(destinationName) + "' AND [ViaName]='" + SafeSqlLiteral(viaName) + "')" + Environment.NewLine +
                             "BEGIN" + Environment.NewLine +
                             "  INSERT INTO [Schedules]([CustomerID], [Line], [FromName], [Destination], [ViaName], [Schedule], [Updated]) VALUES(" + customerID + ", '" + SafeSqlLiteral(line) + "', '" + SafeSqlLiteral(fromName) + "', '" + SafeSqlLiteral(destinationName) + "', '" + SafeSqlLiteral(viaName) + "', '" + "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + scheduleDocument.OuterXml + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')" + Environment.NewLine +
                             "  UPDATE [Schedules] SET [ScheduleXML]=SUBSTRING([Schedule], 39, 1000000) WHERE [CustomerID]=" + customerID + " AND [Line]='" + SafeSqlLiteral(line) + "' AND [FromName]='" + SafeSqlLiteral(fromName) + "' AND [Destination]='" + SafeSqlLiteral(destinationName) + "' AND [ViaName]='" + SafeSqlLiteral(viaName) + "'" + Environment.NewLine +
                             "END" + Environment.NewLine;

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    script = "IF NOT EXISTS(SELECT * FROM [Destinations] WHERE [CustomerID]=" + customerID + " AND [Line]='" + SafeSqlLiteral(line) + "' AND [FromName]='" + SafeSqlLiteral(fromName) + "' AND [Destination]='" + SafeSqlLiteral(destinationName) + "' AND [ViaName]='" + SafeSqlLiteral(viaName) + "')" + Environment.NewLine +
                             "BEGIN" + Environment.NewLine +
                             "  INSERT INTO [Destinations]([CustomerID], [Line], [FromName], [Destination], [ViaName], [MoviaEntry], [LastSeen], [Excluded]) VALUES(" + customerID + ", '" + SafeSqlLiteral(line) + "', '" + SafeSqlLiteral(fromName) + "', '" + SafeSqlLiteral(destinationName) + "', '" + SafeSqlLiteral(viaName) + "', 0, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "', 0)" + Environment.NewLine +
                             "END" + Environment.NewLine +
                             "ELSE" + Environment.NewLine +
                             "BEGIN" + Environment.NewLine +
                             "  UPDATE [Destinations] SET [MoviaEntry]=0, [LastSeen]='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE [CustomerID]=" + customerID + " AND [Line]='" + SafeSqlLiteral(line) + "' AND [FromName]='" + SafeSqlLiteral(fromName) + "' AND [Destination]='" + SafeSqlLiteral(destinationName) + "' AND [ViaName]='" + SafeSqlLiteral(viaName) + "'" + Environment.NewLine +
                             "END" + Environment.NewLine;

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }
        public static string SafeSqlLiteral(string inputSQL)
        {
            if (!String.IsNullOrEmpty(inputSQL))
                return inputSQL.Replace("'", "''");
            else
                return "";
        }
    }
}