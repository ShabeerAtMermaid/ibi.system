﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BusHistory.aspx.cs"
    Inherits="CMS_Aspx.BusHistory" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="Styles/MainStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0">
        <tr>
            <td style="width: 100px">
                History date:
            </td>
            <td>
                <asp:TextBox ID="TxtDate" Width="150px" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td align="left">
                <asp:ImageButton ID="BtnSearch" runat="server" ImageUrl="~/Images/ButtonSearch.png"
                    OnClick="BtnSearch_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridHistory" runat="server" GridLines="Horizontal" EnableViewState="False">
                    <AlternatingRowStyle BackColor="LightGray" />
                    <HeaderStyle CssClass="TableHeader" />
                    <RowStyle BackColor="WhiteSmoke" />
                </asp:GridView>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager" runat="Server" />
    <ajaxToolkit:CalendarExtender ID="CtrlHistoryDate" runat="server" TargetControlID="TxtDate"
        Format="dd-MM-yyyy" ViewStateMode="Enabled" />
</asp:Content>
