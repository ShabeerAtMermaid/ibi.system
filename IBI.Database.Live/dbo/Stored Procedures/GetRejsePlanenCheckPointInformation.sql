﻿CREATE PROC [dbo].[GetRejsePlanenCheckPointInformation]

@StopNumber numeric,
@TransformedStopNumber varchar(20) = ''
AS
 
 BEGIN

	SELECT 
		s.StopName, 
		ISNULL(rs.[CheckPoint], 0) AS IsCheckPoint,
		ISNULL(rs.LastTimeToShow, 30) AS LastTimeToShow,
		ISNULL(rs.ExcludeLowPriorityMedia, 0) AS ExcludeLowPriorityMedia, 
		ISNULL(rs.RejseplanenStopNumber, @TransformedStopNumber)   AS RejseplanenStopNumber
	from Stops s
	LEFT OUTER JOIN RejsePlanenStops rs ON s.GID = rs.GID
	WHERE s.GID=@StopNumber

END
