﻿/*
Update Schedules Set SelectionStructure = '' , ExcludeFromSelection = 0
EXEC [UpdateSelectionStructure] 1
*/

CREATE PROCEDURE [dbo].[UpdateSelectionStructure] 
         @overwrite bit = 1
        AS
        BEGIN
   DECLARE @Line VARCHAR(200)
   DECLARE @Destination VARCHAR(200)
   DECLARE @FromName varchar(200)
   DECLARE @ViaName varchar(200)
   DECLARE @ScheduleID varchar(200)
   DECLARE @tmpStructure varchar(MAX)
   DECLARE @CustomerId int

   DECLARE tmpCursor CURSOR FOR 
    SELECT CustomerId, ScheduleID, Line, Destination, FromName, ViaName FROM [Schedules]
     WHERE (SelectionStructure IS NULL OR SelectionStructure = '')
    
    OPEN tmpCursor

   FETCH NEXT FROM tmpCursor 
   INTO @CustomerId, @ScheduleID, @Line, @Destination, @FromName, @ViaName
   WHILE @@FETCH_STATUS = 0
   BEGIN
    
    if ( @Line IS NOT NULL AND @Line <> '' )
    BEGIN
     SET @tmpStructure = '#' + CONVERT(varchar(MAX), @Line) + '/' 
    END

    if ( @Destination IS NOT NULL AND @Destination <> '' )
    BEGIN
     SET @tmpStructure = @tmpStructure + CONVERT(varchar(MAX), @Destination)
    END
    
    DECLARE @counter int
    
    SET @counter = (SELECT count(*) FROM Schedules WHERE Line=@Line AND Destination=@Destination)
    IF ( @counter > 0 )
    BEGIN
     if ( @FromName IS NOT NULL AND @FromName <> '' )
     /*
     BEGIN
       SET @tmpStructure = @tmpStructure + '/fra ' + CONVERT(varchar(MAX), @FromName)
     END
	 */
     SET @counter = (SELECT count(*) FROM Schedules WHERE Line=@Line AND Destination=@Destination AND FromName=@FromName)
     IF ( @counter > 0 )
     BEGIN
      if ( @ViaName IS NOT NULL AND @ViaName <> '' )
		 BEGIN
		  
			DECLARE @multipleVia bit
			SELECT @multipleVia = CASE COUNT(*) WHEN 0 THEN 0 ELSE 1 END FROM SCHEDULES WHERE Line = @Line AND Destination=@Destination AND ViaName!=@ViaName
			
			DECLARE @separator varchar(2)
			IF (@multipleVia=1)
				SET @separator='/'
			ELSE
				SET @separator=', '
			
			IF(SUBSTRING(LTRIM(RTRIM(@ViaName)), 1, 3) = 'via')				
				SET @tmpStructure = @tmpStructure + @separator + CONVERT(varchar(MAX), @ViaName)				
			ELSE			
				SET @tmpStructure = @tmpStructure + @separator + 'via ' + CONVERT(varchar(MAX), @ViaName)
				
				
			  
		  END
     END
     
    END
    
	--UPDATE Excluded Colum---------
	--SET excluded value for schedules with same Line, Destination, Via (having most stops.)---------------------------------------------------
	
	 
	Update Schedules 
	SET ExcludeFromSelection=1
	WHERE 
	 (SelectionStructure IS NULL OR SelectionStructure = '')	
	 AND CustomerId = @CustomerId AND Line = @Line AND Destination =  @Destination AND (ISNULL(ViaName, '') = ISNULL(@ViaName, ''))
	
		
	Update Schedules 
		SET ExcludeFromSelection=0
		WHERE 
		(SelectionStructure IS NULL OR SelectionStructure = '')
		 AND ScheduleId = 
				(SELECT TOP 1 ScheduleId			  
					FROM [Schedules] 
						WHERE  CustomerId = @CustomerId   AND Line = @Line AND Destination =  @Destination AND (ISNULL(ViaName, '') = ISNULL(@ViaName, ''))						
						AND StopCount = ( SELECT ISNULL(MAX(StopCount), 0) FROM Schedules WHERE CustomerId = @CustomerId AND Line = @Line AND Destination =  @Destination AND (ISNULL(ViaName, '') = ISNULL(@ViaName, ''))))

 
		
								
	------------------------------------------------------------------------------------

    Update [Schedules] 
    SET SelectionStructure=@tmpStructure
    Where ScheduleID = @ScheduleID AND (SelectionStructure IS NULL OR SelectionStructure = '')

	
    
    FETCH NEXT FROM tmpCursor 
    INTO @CustomerId, @ScheduleID, @Line, @Destination, @FromName, @ViaName
   END

   CLOSE tmpCursor
   DEALLOCATE tmpCursor

END
