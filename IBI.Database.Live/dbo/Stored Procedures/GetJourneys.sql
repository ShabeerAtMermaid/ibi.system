﻿CREATE PROC [dbo].[GetJourneys]
	@Line varchar(50)='',
	@BusNumber varchar(50)='',
	@JourneyNumber varchar(50)='',
	@JourneyDate varchar(20)='',
	@From varchar(50)='',
	@To varchar(50)='', 
	@PlannedDeparture varchar(10)='',
	@PlannedArrival varchar(10)='',
	@StartTime varchar(10)='',
	@EndTime varchar(10)=''
	

AS
	BEGIN
		  
		DECLARE @jdate  datetime
	
		SET @jdate = CONVERT(datetime, @JourneyDate, 102)
		
		/*
		Check if start and end time are specified
			yes - Use that in query
			no - Use defaults Like if StartTime is specified then add 48 hours in it for End time if not specified -- something like this
		Fetch that data into temp table (may be?) or open cusrsor?
		Then check other parameters and joins	
		*/
		 
		SELECT 
			j.BusNumber,
			j.CustomerId, 
			s.Line, 
			j.JourneyId,  
			s.FromName, 
			j.StartTime,
			s.Destination,
			j.PlannedEndTime,
			j.PlannedStartTime,
			--(SELECT MAX(jsa.PlannedArrival) FROM JourneyStops jsa WHERE JourneyNumber = j.JourneyNumber) PlannedArrivalTime,
			--(SELECT MIN(jsd.PlannedDeparture) FROM JourneyStops jsd WHERE JourneyNumber = j.JourneyNumber) PlannedDepartureTime,			 
			j.EndTime
	   FROM Journeys j
			INNER JOIN Schedules s ON s.ScheduleId = j.ScheduleId
			   WHERE
			    (@Line = '' OR Line = @Line) AND
			    (@BusNumber = '' OR BusNumber = @BusNumber) AND
			    (@JourneyNumber = '' OR j.JourneyId = @JourneyNumber) AND
			    (@JourneyDate='' OR (StartTime BETWEEN @jdate AND DateAdd(minute, 1439, @jdate))) AND
			    (@From='' OR s.FromName = @From) AND
			    (@To='' OR Destination = @To)  AND
			    (@PlannedDeparture='' OR Substring(CONVERT(varchar, j.PlannedStartTime, 14), 1, 5) = @PlannedDeparture) AND
			    (@PlannedArrival='' OR Substring(CONVERT(varchar, j.PlannedEndTime, 14), 1, 5) = @PlannedArrival) AND			    
			    (@EndTime='' OR Substring(CONVERT(varchar, EndTime, 14), 1, 5) = @EndTime) 
			 ORDER BY j.StartTime DESC, j.PlannedStartTime DESC
			 
			 
			
	END
