﻿CREATE PROC [dbo].[AppendToJourneyStopsHistory]
	@JourneyNumber int = NULL
   ,@StopName varchar(100) = NULL
   ,@Timestamp datetime
   ,@AheadData xml = NULL
   ,@BehindData xml = NULL
   ,@StopData xml = NULL
   ,@IsLastStop BIT = 0
   ,@PlanDifference INT = NULL
   ,@Status VARCHAR(255) = NULL
   ,@Error text = NULL
   ,@ActualArrival DATETIME = NULL
   ,@ActualDeparture DATETIME = NULL
   ,@PlannedArrival DATETIME = NULL
   ,@PlannedDeparture DATETIME = NULL
 
AS

BEGIN

	INSERT INTO [JourneyStops_History]
           ([JourneyNumber]
           ,[StopName]
           ,[Timestamp]
           ,[AheadData]
           ,[BehindData]
           ,[StopData]
           ,[IsLastStop]
           ,[PlanDifference]
           ,[Status]
           ,[Error]
           ,[ActualArrival]
           ,[ActualDeparture]
           ,[PlannedArrival]
           ,[PlannedDeparture])
     VALUES
           (   @JourneyNumber
			   ,@StopName
			   ,@Timestamp
			   ,@AheadData
			   ,@BehindData
			   ,@StopData
			   ,@IsLastStop
			   ,@PlanDifference
			   ,@Status
			   ,@Error
			   ,@ActualArrival
			   ,@ActualDeparture
			   ,@PlannedArrival
			   ,@PlannedDeparture)


END
