﻿-- =============================================
-- Author:  NAK
-- Create date: 21 Feb 2013
-- Description: It returns list of all stop names that have no audio file
-- =============================================
CREATE PROCEDURE [dbo].[StopCustomers](@stopname varchar(260), @voice varchar(260))
 
AS
BEGIN
 
SELECT distinct c.CustomerID, c.FullName
FROM Customers c INNER JOIN Schedules s ON c.CustomerId=s.CustomerId
INNER JOIN ScheduleStops ss ON s.ScheduleId = ss.ScheduleId
WHERE ss.StopName = @stopname AND c.AnnouncementVoice=@voice

END
