﻿

CREATE PROC [dbo].[UpdateSignGroupsAndItems]
         @custId int = 0, @RetVal varchar(max) OUTPUT 
     AS
       BEGIN
       
		   DECLARE @Line VARCHAR(200)
		   DECLARE @Destination VARCHAR(200)
		   DECLARE @FromName varchar(200)
		   DECLARE @ViaName varchar(200)
		   DECLARE @ScheduleID varchar(200)
		   DECLARE @tmpStructure varchar(MAX)
		   DECLARE @CustomerId int
		    
		   SET @RetVal = ''
		     
		   DECLARE custCursor CURSOR FOR 
			SELECT DISTINCT CustomerId FROM Customers WHERE @custId = 0 OR CustomerId=@custId 
			 
		    
		   OPEN custCursor

		   FETCH NEXT FROM custCursor 
		   INTO @CustomerId
		   WHILE @@FETCH_STATUS = 0
		   BEGIN
		   -- START CUSTOMER LOOP --
		   
		   
				   DECLARE @signs Table (
					[CustomerId] [int] NOT NULL,
					[GroupId] [int] NULL,
					[ScheduleId] [int] NULL,
					[Line] [varchar](50) NULL,
					[MainText] [varchar](255) NULL,
					[SubText] [varchar](1000) NULL,
					[Name] [varchar](255) NULL,
					[StopCount] int	)
				
				   INSERT INTO @signs
						SELECT DISTINCT
						[CustomerId], 
						NULL,
						[ScheduleId], 
						[Line],
						Destination,
						IsNull(ViaName, ''),
						Destination,
						StopCount							
						FROM Schedules s
						WHERE
						 CustomerId = @CustomerId AND
						 ScheduleId NOT IN (SELECT IsNull(ScheduleId, 0) From SignItems WHERE CustomerID = @CustomerId AND MainText = s.Destination AND IsNull(SubText,'') = IsNull(s.ViaName, '')) AND 
						 StopCount = 
							(SELECT Max(StopCount) FROM Schedules 
							  WHERE 
								Line = s.Line AND
								Destination = s.Destination	 AND
								IsNull(ViaName, '') = IsNull(s.ViaName, ''))																
					
					  
				   ;
				   --now delete duplicate records from @signs
				  with cte as (
					  select Line, Name, MainText, SubText, 
						 row_number() over (partition by Line, Name, MainText, SubText order by ScheduleId DESC) as [rn]
					  from @signs
					)
					delete cte where [rn] > 1;

				  -- SELECT CustomerId, ScheduleID, Line, Name, SubText FROM @signs ORDER BY Line
				   
				  DECLARE tmpCursor CURSOR FOR 
					SELECT CustomerId, ScheduleID, Line, Name, SubText FROM @signs WHERE CustomerId = @CustomerId ORDER BY Line
				   
				   OPEN tmpCursor
				   
				   DECLARE @counter int
				   DECLARE @parentGroupId int
				   DECLARE @parentGroupId_Fixed int
		   
				   FETCH NEXT FROM tmpCursor 
						INTO @CustomerId, @ScheduleID, @Line, @Destination, @ViaName
				   
				   WHILE @@FETCH_STATUS = 0
				   BEGIN 	
				   -- START CUSTOMER LOOP --
				      
					Print IsNull(@Line,'') + ' / ' + IsNull(@Destination, '') + ' / ' + IsNull(@ViaName, '')
				    
						IF(EXISTS(SELECT SignID FROM SignItems WHERE CustomerId=@CustomerId  AND Line = @Line AND MainText = @Destination	AND IsNull(SubText, '') =  IsNull(@ViaName, '')))
							BEGIN
								FETCH NEXT FROM tmpCursor 
									INTO @CustomerId, @ScheduleID, @Line, @Destination, @ViaName
							
								CONTINUE							
							END
							
							
						IF ( IsNull(@Line,'') <> '' )
						BEGIN
						
						-- SELECT Top 1 GroupId FROM SignGroups WHERE GroupName = '#' + @Line
					     SET @parentGroupId = null
						 SELECT Top 1 @parentGroupId = GroupId FROM SignGroups WHERE GroupName = '#' + @Line AND CustomerId=@CustomerId
						 SET @parentGroupId_Fixed = @parentGroupId				 
						 --Print '1:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
						 
						 DECLARE @maxGroupSortValue int						 
						 SELECT @maxGroupSortValue = MAX(SortValue) FROM SignGroups WHERE ParentGroupId = @parentGroupId
						 
						 
						 IF @parentGroupId is NULL --Insert Root level Group
							BEGIN				
								INSERT INTO [SignGroups]
								   ([GroupName]
								   ,[ParentGroupId]
								   ,[CustomerId]
								   ,[SortValue]
								   ,[DateAdded]
								   ,[DateModified])
								VALUES
								   ('#' + IsNull(@Line,'')
								   ,@parentGroupId
								   ,@CustomerId
								   ,IsNull(@maxGroupSortValue, 0)+1
								   ,GetDate()
								   ,GetDate())
				 
								SET @parentGroupId =  SCOPE_IDENTITY()
							END 
							  
						END
						
						--Print '2:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
					 		 
						IF ( IsNull(@Destination, '') <> '' )
						BEGIN
							SELECT @counter = COUNT(*) FROM @signs WHERE CustomerId = @CustomerId AND Line = @Line AND Name = @Destination
							-- 
							/*
							if counter = 1
								read record from sign item if it exists
								save its group Id
								create new Subgroup inside @parentGroupId with GroupName = MainText
								Update existing signItem (change groupid = @subGroupId
								add to temporary @signs table
								counter++
							
							*/ 
							 DECLARE @tmpCounter int
							 SELECT @tmpCounter = COUNT(*) FROM SignItems WHERE CustomerId = @CustomerId AND Line = @Line AND MainText = @Destination	AND GroupId = @parentGroupId_Fixed 
							 
							 DECLARE @tmpGroupId int
								 
							 IF @counter >= 1 AND @tmpCounter=1
							 
							 BEGIN
							 
								 DECLARE @tmpSignId int 
								 SELECT @tmpSignId = SignId, @tmpGroupId = [GroupId]										
									FROM SignItems
										WHERE CustomerId = @CustomerId AND Line = @Line AND MainText = @Destination	AND GroupId = @parentGroupId_Fixed  
								 
								 If @tmpSignId IS NOT NULL 
									BEGIN
										
										SET @maxGroupSortValue = 0
										SELECT @maxGroupSortValue = MAX(SortValue) FROM SignGroups WHERE ParentGroupId = @parentGroupId_Fixed 	
									    
										--INSERT SUB GROUP
										INSERT INTO [SignGroups]
										   ([GroupName]
										   ,[ParentGroupId]
										   ,[CustomerId]
										   ,[SortValue]
										   ,[DateAdded]
										   ,[DateModified])
										VALUES(
										   IsNull(@Destination,'')
										   ,@parentGroupId_Fixed 
										   ,@CustomerId
										   ,IsNull(@maxGroupSortValue, 0) + 1
										   ,GetDate()
										   ,GetDate())
											
										SET @parentGroupId = SCOPE_IDENTITY()
									
										--Print '3:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
									
										UPDATE SignItems SET GroupId=@parentGroupId WHERE SignId = @tmpSignId
										
									END
							
							END
							
							ELSE  
							  IF @tmpCounter = 0
								BEGIN
									
									SELECT @tmpGroupId = GroupId FROM SignItems WHERE CustomerId = @CustomerId AND Line = @Line AND MainText = @Destination
									
									IF @tmpGroupId IS NOT NULL
										BEGIN
											SET @parentGroupId = @tmpGroupId
											
											--Print '4:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
										END
							 
								END
								   
								
						 
							--SELECT @counter = COUNT(*) FROM @signs WHERE CustomerId = @CustomerId AND Line = @Line AND Name = @Destination 								 
																			
							DECLARE @subGroupId int
							SET @subGroupId = null
							
							--PRINT 'destination count = ' + CAST(@counter as varchar)
							
							IF(@counter>1)
								BEGIN
								
									SELECT Top 1 @subGroupId = GroupId FROM SignGroups WHERE GroupName = @Destination AND ParentGroupId = @parentGroupId_Fixed  AND CustomerId=@CustomerId
							 
									IF @subGroupId IS NULL
										BEGIN
											
											 SET @maxGroupSortValue = 0
											 SELECT @maxGroupSortValue = MAX(SortValue) FROM SignGroups WHERE ParentGroupId = @parentGroupId						 						
											
											--INSERT SUB GROUP
											INSERT INTO [SignGroups]
											   ([GroupName]
											   ,[ParentGroupId]
											   ,[CustomerId]
											   ,[SortValue]
											   ,[DateAdded]
											   ,[DateModified])
											VALUES(
											   IsNull(@Destination,'')
											   ,@parentGroupId
											   ,@CustomerId
											   ,IsNull(@maxGroupSortValue, 0) + 1
											   ,GetDate()
											   ,GetDate())
												
											SET @parentGroupId = SCOPE_IDENTITY()										
											--Print '5:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
										END
									ELSE
										BEGIN
											SET @parentGroupId = @subGroupId
											--Print '6:Parent Group: ' + CAST(IsNULL(@parentGroupId, '0') as varchar)
										END
									
								END
								 
								 
							DECLARE @maxSignSortValue int	
							SET @maxSignSortValue = 0
							SELECT @maxSignSortValue = MAX(SortValue) FROM SignItems WHERE GroupId = @parentGroupId	
													
							INSERT INTO SignItems(
								 [CustomerId]
								,[GroupId]
								,[ScheduleId]
								,[Line]
								,[MainText]
								,[SubText]
								,[Name]
								,[SortValue]
								,[Excluded]
								,[DateAdded]
								,[DateModified]
							)
							VALUES(
								@CustomerId,
								@parentGroupId,
								@ScheduleId,
								@Line,
								@Destination,
								@ViaName,
								@Destination + (CASE IsNull(@ViaName, '') WHEN '' THEN '' ELSE ', ' + @ViaName END),
								IsNull(@maxSignSortValue,0)+1,
								0,
								GETDATE(),
								GETDATE()		
							)
							
							SET @RetVal = @RetVal + CAST(ISNULL(@@IDENTITY, '') AS VARCHAR(MAX))  + ','
									
							--SET @parentGroupId=null
						END
					     
						--UPDATE Excluded Colum---------
						--SET excluded value for schedules with same Line, Destination, Via (having most stops.)---------------------------------------------------
						
						SET @parentGroupId = null
						 
						FETCH NEXT FROM tmpCursor 
						INTO @CustomerId, @ScheduleID, @Line, @Destination, @ViaName
				   END

				   CLOSE tmpCursor
				   DEALLOCATE tmpCursor
				     
				     
				     
		   
		   -- END CUSTOMER LOOP -- 
			FETCH NEXT FROM custCursor 
			INTO @CustomerId
		   END

		   CLOSE custCursor
		   DEALLOCATE custCursor
		    
		   --SELECT @RetVal AS Result
		    
		   
	   END
