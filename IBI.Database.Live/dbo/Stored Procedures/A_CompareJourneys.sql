﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE [dbo].[A_CompareJourneys] 
	@ScheduleNumber int = 0,
	@J1 int,
	@J2 int,
	@J3 int
AS
BEGIN
	DECLARE @scheduleData xml
	DECLARE @myStopName varchar(100)
	DECLARE @currentData as datetime
	DECLARE @aheadData as datetime
	DECLARE @behindData as datetime

	SET @currentData = NULL
	SET @aheadData = NULL
	SET @behindData = NULL

	DECLARE @CompareTable table
	(
		stopname varchar(100),
		j1data datetime,
		j2data datetime,
		j3data datetime
	) 
	SET @scheduleData = (SELECT ScheduleXML From Schedules Where ScheduleID=@ScheduleNumber)
    	DECLARE cur CURSOR FOR
			SELECT JData.Stops.value('(StopName/text())[1]', 'varchar(100)') StopName
			FROM 
				@scheduleData.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)

				OPEN cur
				FETCH NEXT FROM cur INTO @myStopName
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						SET @currentData = (SELECT ActualArrival FROM JourneyStops WHERE JourneyNumber=@j1 AND StopName=@myStopName)
						SET @aheadData = (SELECT ActualArrival FROM JourneyStops WHERE JourneyNumber=@j2 AND StopName=@myStopName)
						SET @behindData = (SELECT ActualArrival FROM JourneyStops WHERE JourneyNumber=@j3 AND StopName=@myStopName)

						INSERT INTO @CompareTable
						SELECT @myStopName,@currentData,@aheadData,@behindData	

						FETCH NEXT FROM cur INTO @myStopName		
					END
			CLOSE cur   
			DEALLOCATE cur

	SELECT * from @CompareTable
END
