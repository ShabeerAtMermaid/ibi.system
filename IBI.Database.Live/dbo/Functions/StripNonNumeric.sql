﻿CREATE FUNCTION [dbo].[StripNonNumeric]
( 
@string varchar(MAX) 
) 
RETURNS int 
AS 
BEGIN 
DECLARE @IncorrectCharLoc SMALLINT 
SET @IncorrectCharLoc = PATINDEX('%[^0-9]%', @string) 
WHILE @IncorrectCharLoc > 0 
BEGIN 
SET @string = STUFF(@string, @IncorrectCharLoc, 1, '') 
SET @IncorrectCharLoc = PATINDEX('%[^0-9]%', @string) 
END 
SET @string = @string 
RETURN CAST(@string as int) 
END
