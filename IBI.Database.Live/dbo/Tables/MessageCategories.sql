﻿CREATE TABLE [dbo].[MessageCategories] (
    [MessageCategoryId] INT           IDENTITY (1, 1) NOT NULL,
    [CategoryName]      VARCHAR (150) NULL,
    [Description]       VARCHAR (300) NULL,
    [Validity]          INT           CONSTRAINT [DF_MessageCategories_Validity] DEFAULT ((0)) NULL,
    [CustomerId]        INT           CONSTRAINT [DF_MessageCategories_CustomerId] DEFAULT ((2140)) NOT NULL,
    [DateAdded]         DATETIME      CONSTRAINT [DF_MessageCategories_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified]      DATETIME      CONSTRAINT [DF_MessageCategories_DateModified] DEFAULT (getdate()) NULL,
    [ValidityRule]      VARCHAR (20)  NULL,
    [ValidityValue]     VARCHAR (100) NULL,
    CONSTRAINT [PK_MessageCategories] PRIMARY KEY CLUSTERED ([MessageCategoryId] ASC),
    CONSTRAINT [FK_MessageCategories_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Default No. of hours for which this category messages should be valid for.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MessageCategories', @level2type = N'COLUMN', @level2name = N'Validity';

