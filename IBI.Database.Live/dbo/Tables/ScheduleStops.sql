﻿CREATE TABLE [dbo].[ScheduleStops] (
    [ScheduleId]   INT               NOT NULL,
    [StopSequence] INT               NOT NULL,
    [StopId]       DECIMAL (19)      NOT NULL,
    [StopName]     NVARCHAR (MAX)    COLLATE Danish_Norwegian_CI_AS NOT NULL,
    [StopGPS]      [sys].[geography] NOT NULL,
    [Zone]         NVARCHAR (MAX)    COLLATE Danish_Norwegian_CI_AS NULL,
    CONSTRAINT [PK_ScheduleStops] PRIMARY KEY CLUSTERED ([ScheduleId] ASC, [StopSequence] ASC),
    CONSTRAINT [FK_ScheduleStops_Schedules] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[Schedules] ([ScheduleId]) ON DELETE CASCADE ON UPDATE CASCADE
);

