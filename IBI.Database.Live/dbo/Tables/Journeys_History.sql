﻿CREATE TABLE [dbo].[Journeys_History] (
    [SqlId]                INT           IDENTITY (1, 1) NOT NULL,
    [BusNumber]            VARCHAR (50)  NOT NULL,
    [CustomerId]           INT           NOT NULL,
    [ScheduleNumber]       INT           NOT NULL,
    [JourneyDate]          DATETIME      CONSTRAINT [DF_Journey_Journeys_JourneyDate] DEFAULT (getdate()) NULL,
    [StartTime]            DATETIME      NULL,
    [EndTime]              DATETIME      NULL,
    [Line]                 VARCHAR (100) NULL,
    [StartPoint]           VARCHAR (100) NULL,
    [Destination]          VARCHAR (100) NULL,
    [PlannedArrivalTime]   DATETIME      NULL,
    [PlannedDepartureTime] DATETIME      NULL,
    [JourneyData]          XML           NULL,
    [JourneyAhead]         INT           NULL,
    [JourneyBehind]        INT           NULL,
    [IsLastStop]           BIT           CONSTRAINT [DF_Journey_Journeys_IsLastStop] DEFAULT ((0)) NOT NULL,
    [ExternalReference]    VARCHAR (100) NULL,
    [JourneyNumber]        INT           NOT NULL,
    CONSTRAINT [PK_Journeys_History] PRIMARY KEY CLUSTERED ([JourneyNumber] ASC)
);

