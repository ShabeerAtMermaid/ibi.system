﻿CREATE TABLE [dbo].[LanguageDefinitionsWeb] (
    [Id]      VARCHAR (500)  NOT NULL,
    [Module]  VARCHAR (100)  NULL,
    [Page]    VARCHAR (100)  NULL,
    [English] NVARCHAR (500) NULL,
    [Danish]  NVARCHAR (500) NULL,
    CONSTRAINT [PK_LanguageDefinitionsWeb] PRIMARY KEY CLUSTERED ([Id] ASC)
);

