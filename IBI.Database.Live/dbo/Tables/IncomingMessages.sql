﻿CREATE TABLE [dbo].[IncomingMessages] (
    [MessageId]    INT               IDENTITY (1, 1) NOT NULL,
    [BusNumber]    VARCHAR (50)      NOT NULL,
    [CustomerId]   INT               NOT NULL,
    [ClientId]     INT               NULL,
    [MacAddress]   VARCHAR (20)      NULL,
    [Line]         VARCHAR (50)      NULL,
    [Destination]  VARCHAR (50)      NULL,
    [NextStop]     VARCHAR (50)      NULL,
    [Latitude]     VARCHAR (20)      NULL,
    [Longitude]    VARCHAR (20)      NULL,
    [GPS]          [sys].[geography] NULL,
    [MessageText]  VARCHAR (MAX)     NULL,
    [DateAdded]    DATETIME          CONSTRAINT [DF_BusToLandMessages_DateAdded] DEFAULT (getdate()) NULL,
    [DateModified] DATETIME          CONSTRAINT [DF_BusToLandMessages_DateModified] DEFAULT (getdate()) NULL,
    [IsArchived]   BIT               CONSTRAINT [DF_IncomingMessages_IsArchived] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_BusToLandMessages] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [FK_IncomingMessages_Buses] FOREIGN KEY ([BusNumber], [CustomerId]) REFERENCES [dbo].[Buses] ([BusNumber], [CustomerId]),
    CONSTRAINT [FK_IncomingMessages_Clients] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Clients] ([ClientId])
);

