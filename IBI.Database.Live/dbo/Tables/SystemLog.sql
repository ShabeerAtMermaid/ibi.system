﻿CREATE TABLE [dbo].[SystemLog] (
    [EntryID]       INT           IDENTITY (1, 1) NOT NULL,
    [Timestamp]     DATETIME      NOT NULL,
    [EntryType]     VARCHAR (500) NOT NULL,
    [EntryCategory] VARCHAR (500) NOT NULL,
    [Description]   TEXT          NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_SystemLog_Timestamp]
    ON [dbo].[SystemLog]([Timestamp] ASC);

