﻿CREATE TABLE [dbo].[JourneyStops_History] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [JourneyNumber]    INT           NULL,
    [StopName]         VARCHAR (100) NULL,
    [Timestamp]        DATETIME      CONSTRAINT [DF_Journey_JouneyDetail_Timestamp] DEFAULT (getdate()) NULL,
    [AheadData]        XML           NULL,
    [BehindData]       XML           NULL,
    [StopData]         XML           NULL,
    [IsLastStop]       BIT           CONSTRAINT [DF_Journey_JouneyDetail_IsLastStop] DEFAULT ((0)) NOT NULL,
    [PlanDifference]   INT           CONSTRAINT [DF_Journey_JourneyDetail_PlanDifference] DEFAULT ((0)) NULL,
    [Status]           VARCHAR (255) NULL,
    [Error]            TEXT          NULL,
    [ActualArrival]    DATETIME      NULL,
    [ActualDeparture]  DATETIME      NULL,
    [PlannedArrival]   DATETIME      NULL,
    [PlannedDeparture] DATETIME      NULL,
    CONSTRAINT [FK_JourneyStops_History_Journeys_History] FOREIGN KEY ([JourneyNumber]) REFERENCES [dbo].[Journeys_History] ([JourneyNumber])
);


GO
CREATE NONCLUSTERED INDEX [JourneyNumber]
    ON [dbo].[JourneyStops_History]([JourneyNumber] ASC);

