﻿CREATE TABLE [dbo].[Schedules_OLD] (
    [ScheduleID]           INT           IDENTITY (1, 1) NOT NULL,
    [CustomerID]           INT           NOT NULL,
    [Line]                 VARCHAR (50)  NOT NULL,
    [FromName]             VARCHAR (500) NOT NULL,
    [Destination]          VARCHAR (500) NOT NULL,
    [ViaName]              VARCHAR (500) NULL,
    [StopCount]            INT           NULL,
    [Schedule]             VARCHAR (MAX) NOT NULL,
    [Updated]              DATETIME      NOT NULL,
    [ScheduleXML]          XML           NULL,
    [MD5]                  VARCHAR (50)  NULL,
    [SelectionStructure]   VARCHAR (500) NULL,
    [ExcludeFromSelection] BIT           CONSTRAINT [DF_Schedules_ExcludeFromSelection] DEFAULT ((0)) NULL,
    [StopsSequenceMD5]     VARCHAR (50)  NULL
);

