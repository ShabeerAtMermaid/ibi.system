﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Globalization;

namespace REST.TestClient
{
    public class AppUtility
    {
        public static String getClientIP()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties messageProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

            return endpointProperty.Address;
        }

        public static IFormatProvider getDateFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd";
            dateFormat.LongDatePattern = "yyyy-MM-dd";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd";
            dateFormat.DateSeparator = "-";

            return dateFormat;
        }

        public static IFormatProvider getDateTimeFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.LongDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.DateSeparator = "-";
            dateFormat.TimeSeparator = ":";

            return dateFormat;
        }

        public static string SafeSqlLiteral(string inputSQL)
        {
            if (!String.IsNullOrEmpty(inputSQL))
                return inputSQL.Replace("'", "''");
            else
                return "";
        }
    }
}