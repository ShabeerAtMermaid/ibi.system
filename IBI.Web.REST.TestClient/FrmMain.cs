﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.IO;
using System.Windows.Forms;
using REST.TestClient.Services;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlClient;

namespace REST.TestClient
{
    public partial class FrmMain : Form
    {
        CMSService.CMSServiceClient remoteService;
        LocalCMSService.CMSServiceClient localService;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void BtnUploadLog_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(this.TxtLogFile.Text))
            {
                FileInfo logFile = new FileInfo(this.TxtLogFile.Text);

                if (logFile.Exists)
                {
                    try
                    {
                        String requestString = this.TxtServerUrl.Text + "Upload.aspx?uploadtype=ProgressLog";

                        WebClient webClient = new WebClient();
                        byte[] reply = webClient.UploadFile(requestString, logFile.FullName);

                        this.TxtOutput.Text = System.Text.Encoding.UTF8.GetString(reply);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Log file doesn't exist");
                }
            }
            else
            {
                MessageBox.Show("No log file selected");
            }
        }

        private void BtnLogUploadBrowse_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                this.TxtLogFile.Text = this.openFileDialog.FileName;

        }

        private void BtnLogsParse_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                List<String> filelist = new List<string>();

                foreach (String filename in Directory.GetFiles(this.folderBrowserDialog.SelectedPath))
                {
                    filelist.Add(filename);
                }

                this.LogParseWorker.RunWorkerAsync(filelist);
                this.BtnLogsParse.Enabled = false;
            }
        }

        private void LogParseWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
WriteOutput("Starting..");

            List<String> filenames = (List<String>)e.Argument;
            int totalSteps = filenames.Count;
            int currentStep = 1;

            this.LogParseWorker.ReportProgress(0);
            WriteOutput(filenames.Count + " logfiles found");

            List<String> logfiles = new List<string>();

            foreach (String filename in filenames)
            {
                this.UpdateProgress(currentStep / totalSteps * 100);
                logfiles.AddRange(this.extractFiles(filename));

                currentStep++;
            }

            totalSteps = logfiles.Count;
            currentStep = 1;

            foreach (String filename in logfiles)
            {
                WriteOutput("Parsing file: " + filename);

                this.UpdateProgress(currentStep / totalSteps * 100);
                logfiles.AddRange(this.extractFiles(filename));

                currentStep++;
            }
            }
            catch (Exception ex)
            {
                WriteOutput(ex.Message + Environment.NewLine + ex.StackTrace);
            }            
        }

        private String[] extractFiles(String filePath)
        {
            if (String.Compare(".tmp", Path.GetExtension(filePath), true) == 0)
                WriteOutput("Extracting files from: " + filePath);
            else
                WriteOutput("No need for extraction: " + filePath);

            String targetPath = Path.GetDirectoryName(filePath);

            List<String> fileList = new List<String>();

            FastZip zipEngine = new FastZip();
            zipEngine.ExtractZip(filePath, targetPath, "");

            foreach (String logFilePath in Directory.GetFiles(targetPath))
            {
                if (String.Compare(Path.GetFileName(filePath), Path.GetFileName(logFilePath), true) != 0)
                    fileList.Add(logFilePath);
            }

            return fileList.ToArray();
        }

        private void ImportLog(String filePath)
        {

            String details = "";

            using (SqlConnection connection = new SqlConnection("data source='212.60.113.93';Trusted_Connection='no';database='IBI_Data';Pooling='true';Uid='global';Pwd='@sG/001002_'"))
            {
                connection.Open();

                foreach (String line in File.ReadAllLines(filePath, System.Text.Encoding.UTF8))
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        String[] values = line.Split(';');

                        DateTime timestamp = DateTime.MinValue;
                        int busNumber = 0;
                        String clientType = "";
                        Boolean isMaster = false;
                        Boolean isOnline = false;
                        String source = "";
                        Boolean isOverwritten = false;
                        long journeyNumber = 0;
                        String currentLine = "";
                        String from = "";
                        String destination = "";
                        long stopNumber = 0;
                        int zone = 0;
                        String latitude = "";
                        String longitude = "";
                        String logTrigger = "";
                        String data = "";
                        int result = 0;
                        String signData = "";

                        if (String.Compare("VER1", values[0], true) == 0)
                        {
                            //Ignore
                            details += "Ignoring VER1" + Environment.NewLine;
                            details += line + Environment.NewLine;

                            continue;
                        }
                        else if (String.Compare("VER2", values[0], true) == 0)
                        {
                            try
                            {
                                timestamp = DateTime.Parse(values[1], AppUtility.getDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType))
                                    continue;
                            }
                            catch (Exception ex)
                            {
                                //throw new ApplicationException("Error parsing VER2 line. Data is: " + line, ex);

                                details += "Error parsing line." + Environment.NewLine;
                                details += line + Environment.NewLine;

                                continue;
                            }
                        }
                        else if (String.Compare("VER3", values[0], true) == 0)
                        {
                            try
                            {
                                timestamp = DateTime.Parse(values[1], AppUtility.getDateTimeFormatProvider());
                                int.TryParse(values[2], out busNumber);
                                clientType = values[3];
                                isMaster = (values[4] == "1");
                                isOnline = (values[5] == "1");
                                source = values[6];
                                isOverwritten = (values[7] == "1");
                                long.TryParse(values[8], out journeyNumber);
                                currentLine = values[9];
                                from = values[10];
                                destination = values[11];
                                long.TryParse(values[12], out stopNumber);
                                int.TryParse(values[13], out zone);
                                latitude = values[14];
                                longitude = values[15];
                                logTrigger = values[16];
                                data = values[17];
                                int.TryParse(values[18], out result);

                                if (values.Length > 19)
                                    signData = values[19];

                                if (busNumber <= 0 || String.IsNullOrEmpty(clientType))
                                {
                                    details += "Error parsing line. No BusNumber or ClientType." + Environment.NewLine;
                                    details += line + Environment.NewLine;

                                    continue;
                                }
                            }
                            catch (Exception ex)
                            {
                                //throw new ApplicationException("Error parsing VER2 line. Data is: " + line, ex);

                                details += "Error parsing line." + Environment.NewLine;
                                details += line + Environment.NewLine;

                                continue;
                            }
                        }
                        else
                        {
                            details += "Line not recognized." + Environment.NewLine;
                            details += line + Environment.NewLine;

                            //throw new ApplicationException("Syntax Version not supported. Data is: " + line);
                            continue;
                        }

                        String script = "INSERT INTO [BusProgressLog](" +
                                            "[Timestamp], " +
                                            "[BusNumber], " +
                                            "[ClientType], " +
                                            "[IsMaster], " +
                                            "[IsOnline], " +
                                            "[Source], " +
                                            "[IsOverwritten], " +
                                            "[JourneyNumber], " +
                                            "[Line], " +
                                            "[From], " +
                                            "[Destination], " +
                                            "[StopNumber], " +
                                            "[Zone], " +
                                            "[Latitude], " +
                                            "[Longitude], " +
                                            "[LogTrigger], " +
                                            "[Data], " +
                                            "[Result], " +
                                            "[SignData]" +
                                        ")" + Environment.NewLine +
                                        "VALUES(" +
                                            "'" + timestamp.ToString("yyyy-MM-dd HH:mm:ss") + "', " +
                                            busNumber + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(clientType) + "', " +
                                            (isMaster ? 1 : 0) + ", " +
                                            (isOnline ? 1 : 0) + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(source) + "', " +
                                            (isOverwritten ? 1 : 0) + ", " +
                                            journeyNumber + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(currentLine) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(from) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(destination) + "', " +
                                            stopNumber + ", " +
                                            zone + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(latitude) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(longitude) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(logTrigger) + "', " +
                                            "'" + AppUtility.SafeSqlLiteral(data) + "', " +
                                            result + ", " +
                                            "'" + AppUtility.SafeSqlLiteral(signData) + "'" +
                                         ")";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        private void LogParseWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateProgress(e.ProgressPercentage);
        }

        private delegate void OutputHandler(String line);
        private delegate void ProgressHandler(Double percentage);

        private void WriteOutput(String line)
        {
            if (this.InvokeRequired)
                this.BeginInvoke(new OutputHandler(WriteOutput), new object[] { line });
            else
                this.TxtOutput.AppendText(line + Environment.NewLine);
        }

        private void UpdateProgress(Double percentage)
        {
            if (this.InvokeRequired)
                this.BeginInvoke(new ProgressHandler(UpdateProgress), new object[] { percentage });
            else
                this.progressBar.Value = (Int32)percentage;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //int minsuntilNexttick = 
            //DateTime nextTick = DateTime.Now.AddMinutes(DateTime.Now.Minute + (20 - DateTime.Now.Minute % 15));

            //this.TmrProcessLogs.Interval = Math.Max(5000, nextTick.Subtract(DateTime.Now).TotalMilliseconds);
            
        }
    }
}
