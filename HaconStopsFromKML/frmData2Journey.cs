﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Spatial;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using IBI.Shared.Diagnostics;

namespace HaconStopsFromKML
{
    public partial class frmData2Journey : Form
    {

        public int CustomerId = 2164;
    
        public frmData2Journey()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtPath.Text = openFileDialog1.FileName;
                ProcessJourneys();
            }
        }

        private void ProcessJourneys()
        {
            string filePath = txtPath.Text;

            Util.FillRoutesOfCurrentCustomer(CustomerId);
            Util.FillSchedulesOfCurrentCustomer(CustomerId);

            StringBuilder log = new StringBuilder();

            try
            {

                if (File.Exists(filePath))
                {
                    string fileText = File.ReadAllText(filePath, System.Text.Encoding.Default);
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream memStream = new MemoryStream(encoding.GetBytes(fileText));

                    XDocument xDoc = XDocument.Load(memStream);

                    string acknowledgement = xDoc.Descendants("Acknowledge").FirstOrDefault().Attribute("Result").Value.ToLower();

                    if (acknowledgement == "ok")
                    { 
                        //String name = (from p in xDoc.Descendants("name")
                        // select p.Value); 

                        using (IBIDataModel dbContext = new IBIDataModel())
                        {

                            //get all Hacon Stops in memory to avoid repeated queries
                            var stops = (from s in dbContext.Stops
                                         where s.GID.ToString().StartsWith("91")
                                         select s);

                            log.AppendLine(String.Format("/******** Total journeys in this data chunk: {0} ********/",xDoc.Descendants("LineSchedule").Count().ToString()));

                            int jCounter = 1;
                            foreach (var jr in xDoc.Descendants("LineSchedule"))
                            {

                                log.AppendLine(String.Format("## Journey # {0} ##", jCounter.ToString()));


                                string tripId = jr.Descendants("TripName").FirstOrDefault().Value;
                                bool exists = dbContext.Journeys.Where(j => j.ExternalReference == tripId).Count() > 0;

                                //----------------
                                if (exists)
                                {
                                    log.AppendLine(String.Format("## Trip # {0} already exists / imported to Journeys table ##\n", tripId));
                                    jCounter++;
                                    continue;   
                                }
                                //----------------

                                

                                Journey journey = new Journey();
                                journey.JourneyStops = new List<JourneyStop>();

                                string line = jr.Descendants("LineID").FirstOrDefault().Value;
                                string destination = jr.Descendants("DirectionText").FirstOrDefault().Value;
                            
                                DateTime journeyDate = DateTime.ParseExact(jr.Descendants("DayType").FirstOrDefault().Value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

                                DateTime? journeyPlannedStartTime = null;
                                DateTime? journeyPlannedEndTime = null;

                                DateTime? stopPlannedDeparture = null;

                                journey.JourneyStateId = (int)IBI.Shared.Common.Types.JourneyStates.FUTURE;
                                journey.BusNumber = "0";
                                //journey.ClientRef = null;
                                journey.CustomerId = this.CustomerId;
                                //journey.EndTime = null;
                                journey.ExternalReference = tripId;
                                journey.LastUpdated = DateTime.Now;
                                //journey.StartTime = null;

                                int counter = 1;
                                foreach (var st in jr.Descendants("ScheduleStop"))
                                {
                                    JourneyStop jStop = new JourneyStop();

                                    jStop.StopId = decimal.Parse(IBI.Shared.ScheduleHelper.TransformStopNo(st.Descendants("StopID").FirstOrDefault().Value.ToString(), IBI.Shared.ScheduleHelper.StopPrefix.HACON));

                                    var dTime = st.Descendants("DepartureTime").FirstOrDefault();
                                    var aTime = st.Descendants("ArrivalTime").FirstOrDefault();

                                    string pdString = dTime != null ? dTime.Value : "";
                                    string paString = aTime != null ? aTime.Value : "";
                               
                                    if(!(string.IsNullOrEmpty(pdString)))
                                        jStop.PlannedDepartureTime   = DateTime.ParseExact(pdString.Substring(0, pdString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                
                                    if(!(string.IsNullOrEmpty(paString)))
                                        jStop.PlannedArrivalTime   = DateTime.ParseExact(paString.Substring(0, paString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                                    if (jStop.PlannedArrivalTime == null)
                                        jStop.PlannedArrivalTime = jStop.PlannedDepartureTime;

                                    if (jStop.PlannedDepartureTime == null)
                                        jStop.PlannedDepartureTime = jStop.PlannedArrivalTime;

                                
                                    //DbGeography stopGPS = new DbGeography();

                                    var stop = stops.Where(s=>s.GID == jStop.StopId).FirstOrDefault();

                                                               
                                    string lat; 
                                    string lon;
                                    string sqlPoint;

                                    if(stop != null)
                                    {
                                       jStop.StopName = stop.StopName;
                                       //lat = stop.OriginalGPSCoordinateNS.ToString().Trim();
                                       //lon = stop.OriginalGPSCoordinateEW.ToString().Trim();
                                       //sqlPoint =  string.Format("POINT({0} {1})", lat, lon);
                                       jStop.StopGPS = stop.StopGPS; //DbGeography.FromText(sqlPoint, 4326);
                                       jStop.Zone = Util.GetZoneName(stop.StopGPS.Latitude.ToString(), stop.StopGPS.Longitude.ToString(), true);                                                                    
                                    }
                                    else
                                    {
                                        //for the time being, Set default values of GPS of unknown stop for budding st.
                                        jStop.StopName = "";
                                        //lat = "55.747377";
                                        //lon = "12.494961";
                                        //sqlPoint = string.Format("POINT({0} {1})", lat, lon);
                                        //jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                                        //jStop.Zone = Util.GetZoneName(lat, lon, true);  
                                        
                                       
                                    }
                                

                                    jStop.StopSequence = counter;
                                    journey.JourneyStops.Add(jStop);

                                    counter++;
                                }

                                journey.PlannedStartTime = journey.JourneyStops.FirstOrDefault().PlannedDepartureTime;
                                journey.PlannedEndTime = journey.JourneyStops.LastOrDefault().PlannedArrivalTime;
                                string fromName = journey.JourneyStops.FirstOrDefault().StopName;
                                destination = journey.JourneyStops.LastOrDefault().StopName;

                                //determine schedules
                                journey.ScheduleId = Util.GetScheduleNumber(CustomerId, line, fromName, destination, null, journey.JourneyStops, true);

                                try
                                {
                                   // bool exists = dbContext.Journeys.Where(j => j.ExternalReference == journey.ExternalReference).Count() > 0;
                            
                                    //if(exists==false)
                                    {
                                        log.AppendLine(String.Format("Journey:: TripNumber={0}, ScheduleNumber: {1} Line:{2}, From:{3}, Destination:{4}, Total Stops: {5}",
                                            journey.ExternalReference, journey.ScheduleId, line, fromName, destination, journey.JourneyStops.Count()));
                                        dbContext.Journeys.Add(journey);
                                        log.AppendLine(String.Format("Stops without GPS: {0}", journey.JourneyStops.Where(js => js.StopGPS == null).Count()));
                                        foreach (var stop in journey.JourneyStops)
                                        {
                                            log.AppendLine(string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5}",
                                                 stop.StopId, stop.StopName, stop.StopSequence, stop.Zone, stop.PlannedDepartureTime.ToString(), stop.PlannedArrivalTime.ToString()));
                                        }
                                        //WebLogger.Log(log.ToString());
                                        //log.Clear();

                                        dbContext.SaveChanges();   

                                        log.AppendLine(String.Format("Journey Added: {0}", journey.JourneyId));

                                        WebLogger.Log(log.ToString());
                                        log.Clear();
                                    }
                                
                                }
                                catch(Exception exInner)
                                {                                
                                    log.AppendLine(String.Format("Failed to save this journey. {0}\n{1}\n", exInner.Message, exInner.StackTrace ));
                                }

                                journey = null;
                                System.Threading.Thread.Sleep(100);

                                jCounter++;
                            }


                        }
                        
                    }


                }

                WebLogger.Log(log.ToString());
                MessageBox.Show("All stops imported successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failure : " + ex.Message + " Stack : " + ex.StackTrace.ToString());
            }
        }
    }


    public class Util
    {

        private static List<KeyValuePair<string, string>> _RouteCache = null;
        private static List<KeyValuePair<string, string>> _ScheduleCache = null;
        private static List<KeyValuePair<string, string>> _ZoneCache = null;


        static List<KeyValuePair<string, string>> RouteCache
        {
            get
            {
                if (_RouteCache == null)
                    _RouteCache = new List<KeyValuePair<string, string>>();

                return _RouteCache;
            }
            set
            {
                _RouteCache = value;
            }  //value: scheduleId|stopSequenceMD5
        }

        static List<KeyValuePair<string, string>> ScheduleCache
        {
            get
            {
                if (_ScheduleCache == null)
                    _ScheduleCache = new List<KeyValuePair<string, string>>();

                return _ScheduleCache;
            }
            set
            {
                _ScheduleCache = value;
            }  //value: scheduleId|stopSequenceMD5
        }

        static List<KeyValuePair<string, string>> ZoneCache
        {
            get
            {
                if (_ZoneCache == null)
                    _ZoneCache = new List<KeyValuePair<string, string>>();

                return _ZoneCache;
            }
            set
            {
                _ZoneCache = value;
            }
        }

        public static string GetZoneName(string lat, string lon, bool genearateZoneInDB)
        {
            //search in cache first
            string zoneName = "";
            string key = lat + "," + lon;

            var zone = ZoneCache.FirstOrDefault(s => s.Key == key);


            if (!String.IsNullOrEmpty(zone.Value))
            {
                zoneName = zone.Value;
            }
            else
            {
                if (genearateZoneInDB)
                {
                    try
                    {
                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            var dbZone = IBI.REST.Schedule.ScheduleServiceController.GetZone(lat, lon);

                            if (dbZone != null)
                            {
                                zoneName = dbZone.Zone;
                                ZoneCache.Add(new KeyValuePair<string, string>(key, zoneName));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        zoneName = string.Empty;
                    }
                }

                else
                {
                    zoneName = "0";
                }
            }

            return zoneName;
        }

        public static void FillRoutesOfCurrentCustomer(int customerID)
        {
            RouteCache.Clear();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                string custId = customerID.ToString();                
                var routes = dbContext.RouteDirections.Where(r => r.Name.StartsWith(custId));
                foreach (var route in routes)
                {
                    string key = route.RouteDirectionId.ToString();
                    string value = route.Name;
                    RouteCache.Add(new KeyValuePair<string, string>(key, value));
                }

            }
        }

        public static void FillSchedulesOfCurrentCustomer(int customerID)
        {
            ScheduleCache.Clear();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                //int custId = Convert.ToInt32(CustomerID);
                var schedules = dbContext.Schedules.Where(s => s.CustomerId == customerID);
                foreach (var schedule in schedules)
                {
                    string key = customerID.ToString() + "|" + schedule.Line + "|" + schedule.FromName + "|" + schedule.Destination + "|" + schedule.ViaName;
                    string value = schedule.ScheduleId.ToString();
                    ScheduleCache.Add(new KeyValuePair<string, string>(key, value));
                }

            }
        }

        public static int GetScheduleNumber(int customerId, string line, string fromName, string destination, string viaName, ICollection<JourneyStop> journeyStops, bool generateScheduleInDB = true)
        {
            int scheduleId = 0;
                         
            try
            {
                string key = customerId.ToString() + "|" + line + "|" + fromName + "|" + destination + "|" + viaName;

                var scheduleItems = ScheduleCache.Where(s => s.Key == key);

                if (scheduleItems != null)
                {

                    foreach (KeyValuePair<string, string> scheduleItem in scheduleItems)
                    {
                        if (scheduleItem.Value != null && scheduleItem.Value != "0")
                        {
                            scheduleId = Convert.ToInt32(scheduleItem.Value);
                            
                            return scheduleId;
                            
                        }

                    }
                }

                scheduleId = 0; //reset
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedules = dbContext.Schedules.Where(s => s.CustomerId == customerId &&
                                                    s.Line == line &&
                                                    s.FromName == fromName &&
                                                    s.Destination == destination &&
                                                    (s.ViaName == viaName || s.ViaName == null || s.ViaName == "")
                                                    );

                    foreach (var schedule in schedules)
                    {
                        scheduleId = schedule.ScheduleId;
                    }

                    if (scheduleId == 0 && generateScheduleInDB)
                    {
                        Schedule sch = new IBI.DataAccess.DBModels.Schedule
                        {
                            CustomerId = customerId,
                            Destination = destination,
                            FromName = fromName,  
                            Line = line,
                            LastUpdated = DateTime.Now,
                            ViaName = viaName,
                            RouteDirectionId = GetRouteDirectionId(customerId, line, destination, viaName)                                                         
                        };

                        sch.ScheduleStops = new List<ScheduleStop>();
                        
                        foreach (JourneyStop jStop in journeyStops)
                        {
                            ScheduleStop sStop = new ScheduleStop();
                            sStop.StopGPS = jStop.StopGPS;
                            sStop.StopId = jStop.StopId;
                            sStop.StopName = jStop.StopName;
                            sStop.StopSequence = jStop.StopSequence;
                            sStop.Zone = jStop.Zone;
                            sStop.ScheduleId = sch.ScheduleId;

                            sch.ScheduleStops.Add(sStop);
                        } 
                        dbContext.Schedules.Add(sch);

                        dbContext.SaveChanges();


                       // dbContext.SaveChanges();

                        scheduleId = sch.ScheduleId;
                          
                        //add this schedule to ScheduleCache.
                        ScheduleCache.Add(new KeyValuePair<string, string>(key, scheduleId.ToString()));

                    }
                    else
                    {
                        scheduleId = 0;
                    }
                     
                }
                 
            }

            catch (Exception ex)
            {
                MessageBox.Show("Failed to get schedule for current journey [" + ex.Message + "]");
            }

            return scheduleId;
        }


        private static int GetRouteDirectionId(int customerId, string line, string destination, string viaName, bool generateRouteInDB = true)
        {
 	        int routeDirectionId = 0;

           
             
            try
            {
                string name = customerId.ToString() + "|" + line + "|" + destination + "|" + viaName;

                var routeItems = RouteCache.Where(s => s.Value == name);

                if (routeItems != null)
                {

                    foreach (KeyValuePair<string, string> scheduleItem in routeItems)
                    {
                        if (scheduleItem.Value != null && scheduleItem.Value != "0")
                        {
                            routeDirectionId = Convert.ToInt32(scheduleItem.Key);
                            
                            return routeDirectionId;
                            
                        }

                    }
                }

                routeDirectionId = 0; //reset
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var routes = dbContext.RouteDirections.Where(s => s.Name == name);

                    foreach (var route in routes)
                    {
                        routeDirectionId = route.RouteDirectionId;
                    }

                    if (routeDirectionId == 0 && generateRouteInDB)
                    {
                        RouteDirection rd = new RouteDirection
                        {
                            Name = name
                          
                        };

                        dbContext.RouteDirections.Add(rd);

                        dbContext.SaveChanges();

                        routeDirectionId = rd.RouteDirectionId;
                           
                        //add this schedule to ScheduleCache.
                        RouteCache.Add(new KeyValuePair<string, string>(routeDirectionId.ToString(), name));

                    }
                    else
                    {
                        routeDirectionId = 0;
                    }
                     
                }
                 
            }

            catch (Exception ex)
            {
                MessageBox.Show("Failed to get RouteDirectionId for current journey [" + ex.Message + "]");
            }

            return routeDirectionId;
        }
         

    }
}
