﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE [dbo].[A_ScheduleStops] 
AS
BEGIN
	DECLARE @journeyNumber int
	DECLARE @busNumber varchar(100)
	DECLARE @scheduleNumber int
	DECLARE @customerId int
	DECLARE @scheduleXml xml

	DECLARE @StopsTable table
	(
		customerid int,
		schedulenumber int,
		stopname varchar(100),
		latitude varchar(30),
		longitude varchar(30)
	) 

		DECLARE commalist CURSOR FOR
			SELECT CustomerID, ScheduleID FROM Schedules
				OPEN commalist
				FETCH NEXT FROM commalist INTO @customerId, @scheduleNumber
				WHILE @@FETCH_STATUS = 0   
					BEGIN
						SET @scheduleXml = (SELECT ScheduleXml FROM Schedules WHERE ScheduleId=@scheduleNumber)

						DECLARE @tmpStopName varchar(100)
						DECLARE @tmpStopData xml
						DECLARE @tmpLatitude varchar(50)
						DECLARE @tmoLongitude varchar(50)

						DECLARE stopsdata CURSOR FOR
							SELECT 
								JData.Stops.value('(StopName/text())[1]', 'varchar(50)') StopName,
								JData.Stops.value('(GPSCoordinateNS/text())[1]', 'varchar(50)') Latitude,
								JData.Stops.value('(GPSCoordinateEW/text())[1]', 'varchar(50)') Longitude
							FROM 
								@scheduleXml.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)
							
							OPEN stopsdata
							FETCH NEXT FROM stopsdata INTO @tmpStopName,@tmpLatitude,@tmoLongitude
							WHILE @@FETCH_STATUS = 0   
							BEGIN

								INSERT INTO @StopsTable
									SELECT @customerId,@scheduleNumber,@tmpStopName,@tmpLatitude,@tmoLongitude
							FETCH NEXT FROM stopsdata INTO @tmpStopName,@tmpLatitude,@tmoLongitude
							END
						CLOSE stopsdata
						DEALLOCATE stopsdata
					FETCH NEXT FROM commalist INTO @customerId, @scheduleNumber		
					END
		CLOSE commalist   
		DEALLOCATE commalist

		SELECT * FROM @StopsTable
END
