﻿CREATE PROC [dbo].[UpdateCounter]
@CounterName varchar(255)
AS
	BEGIN
	
		IF EXISTS(SELECT * FROM [SystemCounters] WHERE [Name]=@CounterName)
           UPDATE [SystemCounters] SET [Value]=[Value]+1 WHERE [Name]=@CounterName
        ELSE
            INSERT INTO [SystemCounters]([Name],[Value]) VALUES(@CounterName, 1)
 
	END
