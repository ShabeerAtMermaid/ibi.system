﻿CREATE PROC [dbo].[xml_GetActiveJourney]
@ScheduleId int,
@CustomerId int,
@BusNumber varchar(20),
@PlannedStartTime datetime,
@PlannedEndTime datetime
AS

BEGIN

DECLARE @JourneyId int

INSERT INTO [Journeys]
           ([BusNumber]
           ,[ScheduleId]
           ,[CustomerId]
           ,[PlannedStartTime]
           ,[PlannedEndTime]
           ,[StartTime]
           ,[EndTime]
           ,[Active]
           ,[ExternalReference]
           ,[ClientRef])
     VALUES
           (@BusNumber
           ,@ScheduleId
           ,@CustomerId
           ,@PlannedStartTime
           ,@PlannedEndTime
           ,null
           ,null
           ,1
           ,null
           ,null)
 
 
SET @JourneyId = SCOPE_IDENTITY()

INSERT INTO JourneyStops
	SELECT @JourneyId,
		   ss.StopSequence,
		   ss.StopId,
		   ss.StopName,
		   ss.StopGPS,
		   ss.Zone,
		   null,
		   null,
		   null,
		   null,
		   null,
		   null		   
		FROM ScheduleStops ss WHERE ScheduleId = @ScheduleId 	
 
select 
@JourneyId AS [JourneyNumber],
@BusNumber AS [BusNumber],
 ScheduleId  AS [ScheduleNumber],
 Schedules.Line AS [Line],  
 Schedules.FromName AS [FromName],  
 Schedules.Destination AS [DestinationName], 
 Schedules.ViaName AS [ViaName], 
	(
	select StopId as [@StopNumber], 
	 StopName as [StopName], 
	 CAST(StopGPS.STPointN(1).Lat AS VARCHAR(10))  AS [GPSCoordinateNS], 
	 CAST(StopGPS.STPointN(1).Long AS VARCHAR(10)) AS [GPSCoordinateEW],
	 Zone as [Zone],
	 '' AS [Ahead],
	 '' AS [Behind]
		From ScheduleStops
		where ScheduleId=@ScheduleId 
		FOR XML path ('StopInfo'),  type, ROOT('JourneyStops')
		) as [*]
from Schedules Where ScheduleId = @ScheduleId
for xml path ('Journey')



END
 