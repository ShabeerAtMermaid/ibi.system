﻿CREATE PROC [dbo].[GetServiceCase] 
	@AccountIds varchar(1000),
	@BusNumber varchar(10)
AS 
	BEGIN
	
		SELECT top 1 levels.[DEPART_ID] AS [LEVEL_ID] 
 
            FROM 
              NovoSupport_Grandpa..[UDF_VALUES] customValues 
              INNER JOIN 
              NovoSupport_Grandpa..[CUSTOMERS] customers 
              ON customValues.[TABLE_REF]=customers.[CUSTOMER_ID] 
              INNER JOIN 
              NovoSupport_Grandpa..[ACCOUNTS] accounts 
              ON customers.[ACCOUNT_REF]=accounts.[ACCOUNT_ID] 
              INNER JOIN 
              NovoSupport_Grandpa..[CASES] cases 
              ON customValues.[TABLE_REF]=cases.[CUSTOMER_REF] 
              INNER JOIN 
              NovoSupport_Grandpa..[DEPARTMENTS] levels 
              ON cases.[DEPART_REF]=levels.[DEPART_ID] 
            WHERE customValues.[DEFINITION_REF]=2 
              AND customValues.[UDF_VALUES_VARCHAR]=@BusNumber 
              AND cases.[CASE_STATUS]<>8 
              AND (  accounts.[ACCOUNT_ID] IN (SELECT Value FROM dbo.ListToTable(@AccountIds)) )
              
    END
