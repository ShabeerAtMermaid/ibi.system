﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE [dbo].[A_TimelyStopArrivals] 
	@BusNumber int = 0,
	@J varchar(MAX) = '',
	@StartTime datetime = NULL,
	@EndTime datetime = NULL
AS
BEGIN
	DECLARE @scheduleData xml
	DECLARE @myStopName varchar(100)
	DECLARE @JourneyNo int
	DECLARE @missing bit
	DECLARE @JS varchar(MAX)
	DECLARE @actualarrival datetime
	DECLARE @plannedarrival datetime
	DECLARE @tmpbusnumber varchar(100)
	SET @missing = 0
	
	DECLARE @CompareTable table
	(
		journeyNo int,
		busnumber varchar(100),
		stopname varchar(100),
		actualarrival datetime,
		plannedarrival datetime
	) 
	if ( @J = '' )
	BEGIN
		select top 100 @JS = 
		coalesce (case when @JS = ''
					   then CAST(JourneyNumber as varchar(10))
					   else @JS + ',' + CAST(JourneyNumber as varchar(10))
				   end
				  ,'')
		from Journeys WHERE IsLastStop=1 
		AND (@BusNumber=0 OR @BusNumber=BusNumber)
		AND (@EndTime IS NULL OR (@EndTime IS NOT NULL AND JourneyDate <= @EndTime)) 
		AND  (@StartTime IS NULL OR (@StartTime IS NOT NULL AND JourneyDate >= @StartTime)) 
		ORDER BY JourneyDate DESC
	END
	ELSE
		SET @JS = @J

	IF @JS = ''
		SELECT 'No Journey'
    	DECLARE cur CURSOR FOR
			SELECT j.JourneyNumber, js.StopName, js.ActualArrival, js.PlannedArrival,j.BusNumber FROM JourneyStops js, Journeys j WHERE j.JourneyNumber=js.JourneyNumber AND js.ActualArrival = js.PlannedArrival AND js.JourneyNumber IN (SELECT Number FROM splitnumber(@JS,',')) 
				OPEN cur
				FETCH NEXT FROM cur INTO @JourneyNo,@myStopName,@actualarrival,@plannedarrival,@tmpbusnumber
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						INSERT INTO @CompareTable
						SELECT @JourneyNo,@tmpbusnumber,@myStopName,@actualarrival,@plannedarrival	
					FETCH NEXT FROM cur INTO @JourneyNo,@myStopName,@actualarrival,@plannedarrival,@tmpbusnumber		
					END
			CLOSE cur   
			DEALLOCATE cur
		SELECT *, CAST(DATEDIFF(mi, actualarrival, plannedarrival) AS VARCHAR) minutesdifference from @CompareTable ORDER BY journeyNo asc
END
