﻿CREATE PROCEDURE [dbo].[TTSData]
	-- Add the parameters for the stored procedure here
	@CustomerId int=0
AS
BEGIN
SELECT af.*
  FROM [IBI_System].[dbo].[CustomerAudioFiles] ca, [TTSServer_Grandpa].[dbo].[AudioFiles] af
  where 
  (ca.CustomerId = @CustomerId OR @CustomerId = 0 )-- AND af.Rejected != 1
  and ca.Voice COLLATE Danish_Norwegian_CI_AS = af.Voice and ca.StopName COLLATE Danish_Norwegian_CI_AS = af.Text
  END
