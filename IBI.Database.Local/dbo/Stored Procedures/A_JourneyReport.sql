﻿-- =============================================
-- Author:		nak
-- Description:	Report about journeys
-- =============================================
CREATE PROCEDURE [dbo].[A_JourneyReport] 
	@customerId int = 0,
	@scheduleNumber int = 0,
	@busNumber int = 0,
	@Line varchar(100) = NULL,
	@From varchar(100) = NULL,
	@Destination varchar(100) = NULL,
	@StartTime datetime = NULL,
	@EndTime datetime = NULL,
	@IsLastStop bit = 1,
	@journeyMinutes int = 0,
	@showLastStopReportedRecords bit = 0,
	@showLastStopMissingRecords bit = 0,
	@showShortJourneysRecords bit = 0,
	@showMissingEndTimeRecords bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @completedJourneys int
	DECLARE @flagCompletedJourneys int
	DECLARE @shortJourneys int
	DECLARE @missingEndTime int
	
	SET @completedJourneys = (SELECT COUNT(*) FROM Journeys_History
	WHERE (@customerId=0 OR CustomerId=@customerId) AND 
	(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
	(@Line IS NULL OR Line=@Line) AND
	(@From IS NULL OR StartPoint = @From) AND
	(@Destination IS NULL OR Destination=@Destination) AND
	(@busNumber=0 OR BusNumber=@busNumber) AND
	(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
	(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
	IsLAstStop=@IsLastStop AND 
	JourneyNumber  IN (SELECT JourneyNumber FROM JourneyStops_History WHERE IsLastStop=1))
	
	if @showLastStopReportedRecords = 1 
	begin
		SELECT JourneyNumber, ScheduleNumber, BusNumber, StartTime, EndTime, Line, Destination FROM Journeys_History
			WHERE (@customerId=0 OR CustomerId=@customerId) AND 
			(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
			(@Line IS NULL OR Line=@Line) AND
			(@From IS NULL OR StartPoint = @From) AND
			(@Destination IS NULL OR Destination=@Destination) AND
			(@busNumber=0 OR BusNumber=@busNumber) AND
			(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
			(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
			IsLAstStop=@IsLastStop AND 
			JourneyNumber  IN (SELECT JourneyNumber FROM JourneyStops_History WHERE IsLastStop=1)
	end
	
	SET @flagCompletedJourneys = (SELECT COUNT(*) FROM Journeys_History
	WHERE (@customerId=0 OR CustomerId=@customerId) AND 
	(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
	(@Line IS NULL OR Line=@Line) AND
	(@From IS NULL OR StartPoint = @From) AND
	(@Destination IS NULL OR Destination=@Destination) AND
	(@busNumber=0 OR BusNumber=@busNumber) AND
	(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
	(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
	IsLAstStop=@IsLastStop AND 
	JourneyNumber NOT IN (SELECT JourneyNumber FROM JourneyStops_History WHERE IsLastStop=1))

	if @showLastStopMissingRecords = 1
	begin
			SELECT JourneyNumber, ScheduleNumber, BusNumber, StartTime, EndTime, Line, Destination FROM Journeys_History
			WHERE (@customerId=0 OR CustomerId=@customerId) AND 
			(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
			(@Line IS NULL OR Line=@Line) AND
			(@From IS NULL OR StartPoint = @From) AND
			(@Destination IS NULL OR Destination=@Destination) AND
			(@busNumber=0 OR BusNumber=@busNumber) AND
			(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
			(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
			IsLAstStop=@IsLastStop AND 
			JourneyNumber NOT IN (SELECT JourneyNumber FROM JourneyStops_History WHERE IsLastStop=1)
	end

	SET @shortJourneys = (SELECT COUNT(*) FROM Journeys_History
	WHERE (@customerId=0 OR CustomerId=@customerId) AND 
	(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
	(@Line IS NULL OR Line=@Line) AND
	(@From IS NULL OR StartPoint = @From) AND
	(@Destination IS NULL OR Destination=@Destination) AND
	(@busNumber=0 OR BusNumber=@busNumber) AND
	(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
	(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
	IsLAstStop=@IsLastStop AND
	(EndTime IS NOT NULL AND DATEDIFF(MINUTE, StartTime, EndTime) <= @journeyMinutes)) 
	if @showShortJourneysRecords = 1
	begin
		SELECT JourneyNumber, DATEDIFF(MINUTE, StartTime, EndTime) duration, ScheduleNumber, BusNumber, StartTime, EndTime, Line, Destination, JourneyData FROM Journeys_History
		WHERE (@customerId=0 OR CustomerId=@customerId) AND 
		(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
		(@Line IS NULL OR Line=@Line) AND
		(@From IS NULL OR StartPoint = @From) AND
		(@Destination IS NULL OR Destination=@Destination) AND
		(@busNumber=0 OR BusNumber=@busNumber) AND
		(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
		(@EndTime IS NULL OR EndTime IS NULL OR EndTime<=@EndTime) AND
		IsLAstStop=@IsLastStop AND
		(EndTime IS NOT NULL AND DATEDIFF(MINUTE, StartTime, EndTime) <= @journeyMinutes)
		ORDER BY BusNumber,duration ASC
	end

	SET @missingEndTime = (SELECT COUNT(*) FROM Journeys_History
	WHERE (@customerId=0 OR CustomerId=@customerId) AND 
	(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
	(@Line IS NULL OR Line=@Line) AND
	(@From IS NULL OR StartPoint = @From) AND
	(@Destination IS NULL OR Destination=@Destination) AND
	(@busNumber=0 OR BusNumber=@busNumber) AND
	(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
	(EndTime IS NULL) AND
	IsLAstStop=@IsLastStop)
	
	if @showMissingEndTimeRecords = 1
	begin
		SELECT JourneyNumber, ScheduleNumber, BusNumber, StartTime, EndTime, Line, Destination, JourneyData FROM Journeys_History
		WHERE (@customerId=0 OR CustomerId=@customerId) AND 
		(@scheduleNumber=0 OR ScheduleNumber=@scheduleNumber) AND
		(@Line IS NULL OR Line=@Line) AND
		(@From IS NULL OR StartPoint = @From) AND
		(@Destination IS NULL OR Destination=@Destination) AND
		(@busNumber=0 OR BusNumber=@busNumber) AND
		(@StartTime IS NULL OR StartTime IS NULL OR StartTime>=@StartTime) AND
		(EndTime IS NULL) AND
		IsLAstStop=@IsLastStop
		
	end

		 
	SELECT @completedJourneys as LastStopReportedJourneys, @flagCompletedJourneys as LastStopNotReportedJourneys, @shortJourneys as ShortJourneys, @missingEndTime as MissingEndTime
END
