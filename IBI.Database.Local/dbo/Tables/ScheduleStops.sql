﻿CREATE TABLE [dbo].[ScheduleStops] (
    [ScheduleId]   INT               NOT NULL,
    [StopSequence] INT               NOT NULL,
    [StopId]       DECIMAL (19)      NOT NULL,
    [StopName]     NVARCHAR (MAX)    NOT NULL,
    [StopGPS]      [sys].[geography] NOT NULL,
    [Zone]         NVARCHAR (MAX)    NULL,
    CONSTRAINT [FK_ScheduleStops_Schedules] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[Schedules] ([ScheduleId]) ON DELETE CASCADE ON UPDATE CASCADE
);

