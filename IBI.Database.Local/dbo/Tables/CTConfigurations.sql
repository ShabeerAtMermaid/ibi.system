﻿CREATE TABLE [dbo].[CTConfigurations] (
    [SqlId]      INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId] INT           NOT NULL,
    [ConfigKey]  VARCHAR (50)  NOT NULL,
    [Value]      VARCHAR (300) NOT NULL,
    CONSTRAINT [PK_CorrespondingTrafficConfigurations] PRIMARY KEY CLUSTERED ([SqlId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CTConfigurations]
    ON [dbo].[CTConfigurations]([CustomerId] ASC, [ConfigKey] ASC);

