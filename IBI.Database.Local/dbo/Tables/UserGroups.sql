﻿CREATE TABLE [dbo].[UserGroups] (
    [UserGroupId]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (100) NULL,
    [CustomerId]   INT           NULL,
    [Description]  TEXT          NULL,
    [DateAdded]    DATETIME      NULL,
    [DateModified] DATETIME      NULL,
    CONSTRAINT [PK_UserGroups] PRIMARY KEY CLUSTERED ([UserGroupId] ASC)
);

