﻿CREATE TABLE [dbo].[UserGroupUsers] (
    [UserGroupId] INT NOT NULL,
    [UserId]      INT NOT NULL,
    CONSTRAINT [PK_GroupUsers] PRIMARY KEY CLUSTERED ([UserGroupId] ASC, [UserId] ASC),
    CONSTRAINT [FK_UserGroupUsers_UserGroups] FOREIGN KEY ([UserGroupId]) REFERENCES [dbo].[UserGroups] ([UserGroupId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UserGroupUsers_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE ON UPDATE CASCADE
);

