﻿using IBI.Shared.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Configuration;
using IBI.Shared.Diagnostics;

namespace IBI.Web.CorrespondingTraffic.OldDepartureBoard
{
    class SyncDepartureBoard_Rajseplanen :  ISyncDepartureBoard
    {
        #region Attributes

        private int _customerId;
        private string _username;
        private string _password;
        private string _url;
        private string _stopInfoUrl;

        #endregion

        #region "Properties"
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                this._customerId = value;
            }
        }

        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }

        public string StopInfoUrl
        {
            get
            {
                return _stopInfoUrl;
            }
            set
            {
                _stopInfoUrl = value;
            }
        }
        #endregion

        public string GetDepartureBoard(List<KeyValuePair<string, object>> parameters) { return ""; }

        public string GetDepartureBoard(string busNumber, string stopNumber, string busline, int showJourneys, DateTime dateTime)
        {
            //e.g date: 23.05.13
            //e.g time: 13:30
            string dbStopNumber = stopNumber;
            string stopName = "";
            bool isCheckPoint = false;
            try
            {
                
                //untransform Movia's StopNumber
                
                if(stopNumber.Length==16)//it's a movia's transformed stop number
                {
                    stopNumber = Convert.ToInt32(stopNumber.Substring(7)).ToString();
                }

                try
               
                {
                    isCheckPoint = GetCheckPoint(this.StopInfoUrl, stopNumber, out stopName);

                }
                catch(Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.DEPARTURE_BOARD, "Error while getting Stop Info - StopNumber: " + dbStopNumber + "exception:" + ex.Message);
                }
                
                WebClient feedClient = new WebClient();
                ICredentials credentials = null;
                feedClient.Credentials = new NetworkCredential(this.Username, this.Password);


                string date = dateTime.ToString("dd.MM.yy");
                string time = dateTime.ToString("HH:mm");

                string rUrl = String.Format(this.Url + "?id={0}&showJourneys={1}&date={2}&time={3}", stopNumber, showJourneys.ToString(), date, time);
                feedClient.Encoding = System.Text.Encoding.UTF8;
                string tmpXml = feedClient.DownloadString(rUrl); 
                
                XmlDocument doc = new XmlDocument();                
                doc.LoadXml(tmpXml);

                //Log tmpXML
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.DEPARTURE_BOARD, "Data From RejesPlannen: " + tmpXml);

                
                doc.DocumentElement.RemoveAllAttributes();

                string lineFilter;
                if (busline.EndsWith("N", StringComparison.OrdinalIgnoreCase))
                {
                    lineFilter = "NatBus " + busline;
                }
                else
                {
                    lineFilter = "Bus " + busline;
                }

                

                bool onlyBuses = false; 
                string templateType = "none";
                // clean up XML
                cleanupXml(doc, lineFilter, dateTime, out onlyBuses);
                
                //Log XML After Cleanup
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.DEPARTURE_BOARD, "Data After Cleanup: " + doc.OuterXml);                 

                XmlNodeList busNodes = doc.SelectNodes("DepartureBoard/Departure[@type='BUS' or @type='NB' or @type='EXB' or @type='TB']");

                


                XmlNodeList trainNodes = doc.SelectNodes("DepartureBoard/Departure[@type='S' or @type='M'  or @type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN']");

                //ArrayList arrBuses = new ArrayList();
                //Dictionary<string> dict = new Dictionary<string>();
                Dictionary<string, XmlNodeList> dictBuses = new Dictionary<string, XmlNodeList>();
                
                //for (int i = 0; i < busNodes.Count; i++)
                //{
                //    string key = 
                //}
                int index = 0;
                foreach(XmlNode n in busNodes)
                {
                    string name = n.Attributes["name"].InnerText;
                    string direction = n.Attributes["direction"].InnerText;
                    string item = name + "|" + direction;

                    if (dictBuses.Where(b=>b.Key==item).FirstOrDefault().Value == null)
                    {
                        XmlNodeList nodeList = doc.SelectNodes("DepartureBoard/Departure[@name='" + name + "' and @direction='" + direction + "']");
                        dictBuses.Add(item, nodeList);

                        index++;
                    }
                      
                    //dictBuses[item] = n;

                }

                int totBuses = dictBuses.Count;
                int totTrains = trainNodes.Count;

                int maxBuses2Show = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxBuses_SmallStop"]); // from config
                int maxTrains2Show = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxTrains_SmallStop"]); // from config
                if ((isCheckPoint && (totBuses > 0 || totTrains > 0)) || (onlyBuses && totBuses > 6) || (!onlyBuses))
                {
                    templateType = "large";
                    maxBuses2Show = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxBuses_LargeStop"]);//14;
                    maxTrains2Show = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxTrains_LargeStop"]); //7;
                }
                else
                    if (onlyBuses && totBuses > 0)
                    {
                        templateType = "small";                    
                    }

                // remove extra nodes
                while (totBuses > maxBuses2Show)
                {
                     doc.DocumentElement.RemoveChild(busNodes[maxBuses2Show]);
                     busNodes = doc.SelectNodes("DepartureBoard/Departure[@type='BUS' or @type='NB' or @type='EXB' or @type='TB']"); //[Added by KHI] need to check it
                     totBuses = busNodes.Count;
                }
                
                while (totTrains > maxTrains2Show)
                {
                    doc.DocumentElement.RemoveChild(trainNodes[maxTrains2Show]);
                    trainNodes = doc.SelectNodes("DepartureBoard/Departure[@type='S' or @type='M'  or @type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN']");//[Added by KHI] need to check it
                    totTrains = trainNodes.Count;
                }

                ////get stopName
                //string stopName = "";
                //using(IBI.DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                //{
                //    stopName = dbContext.GetStopName(CustomerId, dbStopNumber).FirstOrDefault();

                //}
                 
                //int timePerPage = int.Parse(ConfigurationSettings.AppSettings["CTraffic_TimePerPage"]); // from config
                StringBuilder sb = new StringBuilder();
                sb.Append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.Append("<CorrespondingTraficData>");
                sb.Append(" <FetchTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</FetchTime>");
                sb.Append(" <FetchFromTime>" + dateTime.ToString("yyyy-MM-ddTHH:mm:ss") + "</FetchFromTime>");
                sb.Append(" <StopNumber>" + dbStopNumber + "</StopNumber>");
                sb.Append(" <StopName>" + stopName  + "</StopName>");//TODO - what to add - different stopnames [KHI]: I am getting stop name from IBI Data
                sb.Append(" <Checkpoint>" + (isCheckPoint ? "true" : "false") + "</Checkpoint>");
                sb.Append(" <PlaybackInformation>");
                sb.Append(" <TemplateType>" + templateType + "</TemplateType>");
                //sb.Append(" <NumberOfBuses>" + doc.SelectNodes("DepartureBoard/Departure[@type='BUS' or @type='NB' or @type='EXB' or @type='TB']").Count.ToString() + "</NumberOfBuses>");
                //sb.Append(" <NumberOfTrains>" + doc.SelectNodes("DepartureBoard/Departure[@type='S' or @type='M'  or @type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN']").Count.ToString() + "</NumberOfTrains>");
                sb.Append(" <NumberOfBuses>" + ((dictBuses.Count < maxBuses2Show) ? dictBuses.Count.ToString() : maxBuses2Show.ToString()) + "</NumberOfBuses>");
                sb.Append(" <NumberOfTrains>" + totTrains.ToString() + "</NumberOfTrains>");
                
                //sb.Append(" <TimePerPage>" + timePerPage.ToString() + "</TimePerPage>");
                sb.Append(" </PlaybackInformation>");
                sb.Append(" <Departures>");
                sb.Append(" <Buses>");

                //foreach (XmlNode node in doc.SelectNodes("DepartureBoard/Departure[@type='BUS' or @type='NB' or @type='EXB' or @type='TB']"))
                //foreach (XmlNodeList bNodes in dictBuses.AsEnumerable())
                index = 0;
                foreach (KeyValuePair<string, XmlNodeList> item in dictBuses)
                {
                    if (++index >= maxBuses2Show)
                        break;

                    
                    XmlNodeList bNodes = item.Value;

                    if (bNodes == null || bNodes.Count == 0)
                        continue;
                    
                    XmlNode node = bNodes[0];
                    //if(bIndexes.Count>0)
                    //{
                    //    doc.SelectSingleNode("DepartureBoard/Departure[@name='"+busName+"' and @direction='"+busDestination+"']")
                    //}

                    sb.Append("     <Bus>");
                    
                    //string line = node.Attributes["name"].Value;
                    //if (line.StartsWith("Bus", StringComparison.OrdinalIgnoreCase))
                    //{
                    //    line = line.Substring(3).Trim();
                    //}
                    //if (line.StartsWith("NatBus", StringComparison.OrdinalIgnoreCase))
                    //{
                    //    line = line.Substring(6).Trim();
                    //}

                    string nameAttribute = node.Attributes["name"].Value;
                    string typeAttribute = node.Attributes["type"].Value;

                    string[] arrLineTypeName = getBusLine(nameAttribute, typeAttribute);
                    string line = arrLineTypeName.Length > 0 ? arrLineTypeName[0] : "";
                    string type = arrLineTypeName.Length > 1 ? arrLineTypeName[1] : "";
                    string name = arrLineTypeName.Length > 2 ? arrLineTypeName[2] : ""; 

                    sb.Append("     <Line>" +  line + "</Line>");
                    sb.Append("     <Type>" + type + "</Type>");
                    sb.Append("     <Destination>" + node.Attributes["direction"].Value + "</Destination>");
                       
                        foreach(XmlNode n in bNodes)
                        {
                            sb.Append("     <Departure>");
                    
                            CultureInfo provider = CultureInfo.InvariantCulture;
                            DateTime plannedDeparturetime = DateTime.ParseExact(n.Attributes["date"].Value + " " + n.Attributes["time"].Value, "dd.MM.yy HH:mm", provider);
                            sb.Append("        <PlanedDepartureTime>" + plannedDeparturetime.ToString("yyyy-MM-ddTHH:mm:ss") + "</PlanedDepartureTime>");
                            if (n != null && n.Attributes["rtTime"] != null && n.Attributes["rtDate"] != null)
                            {
                                DateTime estimatedDeparturetime = DateTime.ParseExact(n.Attributes["rtDate"].Value + " " + n.Attributes["rtTime"].Value, "dd.MM.yy HH:mm", provider);
                                sb.Append("         <EstimatedDepartureTime>" + estimatedDeparturetime.ToString("yyyy-MM-ddTHH:mm:ss") + "</EstimatedDepartureTime>");
                            }
                            else
                            {
                                sb.Append("         <EstimatedDepartureTime></EstimatedDepartureTime>");
                            }
                            
                            sb.Append("     </Departure>");
                        }
                    
                   
                    sb.Append("     </Bus>");
                }

                sb.Append(" </Buses>");
                sb.Append(" <Trains>");

                foreach (XmlNode node in doc.SelectNodes("DepartureBoard/Departure[@type='S' or @type='M'  or @type='REG'  or @type='TOG'  or @type='IC'  or @type='LYN']"))
                {
                    sb.Append("     <Train>"); 
                    string nameAttribute = node.Attributes["name"].Value;
                    string typeAttribute = node.Attributes["type"].Value;

                    string[] arrLineTypeName = getTrainLine(nameAttribute, typeAttribute);
                    string line = arrLineTypeName.Length>0 ? arrLineTypeName[0] : "";
                    string type = arrLineTypeName.Length > 1 ? arrLineTypeName[1] : "";
                    string name = arrLineTypeName.Length > 2 ? arrLineTypeName[2] : ""; 

                    sb.Append("     <Line>" + line + "</Line>");
                    sb.Append("     <Type>" + type + "</Type>");
                    sb.Append("     <Destination>" + node.Attributes["direction"].Value + "</Destination>");

                    sb.Append("     <Departure>");

                    CultureInfo provider = CultureInfo.InvariantCulture;
                    if (node.Attributes["time"] != null && node.Attributes["date"] != null)
                    {
                        DateTime plannedDeparturetime = DateTime.ParseExact(node.Attributes["date"].Value + " " + node.Attributes["time"].Value, "dd.MM.yy HH:mm", provider);
                        sb.Append("         <PlanedDepartureTime>" + plannedDeparturetime.ToString("yyyy-MM-ddTHH:mm:ss") + "</PlanedDepartureTime>");

                    }
                    else
                    {
                        sb.Append("         <PlanedDepartureTime></PlanedDepartureTime>");
                    }
                    if (node.Attributes["rtTime"] != null && node.Attributes["rtDate"] != null)
                    {
                        DateTime estimatedDeparturetime = DateTime.ParseExact(node.Attributes["rtDate"].Value + " " + node.Attributes["rtTime"].Value, "dd.MM.yy HH:mm", provider);
                        sb.Append("         <EstimatedDepartureTime>" + estimatedDeparturetime.ToString("yyyy-MM-ddTHH:mm:ss") + "</EstimatedDepartureTime>");
                    }
                    else
                    {
                        sb.Append("         <EstimatedDepartureTime></EstimatedDepartureTime>");
                    }
                    if (node.Attributes["track"] != null)
                    {
                        sb.Append("         <PlanedDepartureTrack>" + node.Attributes["track"].Value + "</PlanedDepartureTrack>");
                    }
                    else
                    {
                        sb.Append("         <PlanedDepartureTrack></PlanedDepartureTrack>");
                    }
                    if (node.Attributes["rtTrack"] != null)
                    {
                        sb.Append("         <EstimatedDepartureTrack>" + node.Attributes["rtTrack"].Value + "</EstimatedDepartureTrack>");
                    }
                    else
                    {
                        sb.Append("         <EstimatedDepartureTrack></EstimatedDepartureTrack>");
                    }

                    sb.Append("     </Departure>");
                    sb.Append("     </Train>");
                }

                sb.Append(" </Trains>");
                sb.Append(" </Departures>");
                sb.Append(" </CorrespondingTraficData>");
                //TODO: Night buses?

                
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.DEPARTURE_BOARD, ex);
            }



            return "<CorrespondingTraficData><FetchTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</FetchTime><FetchFromTime>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</FetchFromTime><StopNumber>"+dbStopNumber+"</StopNumber><StopName>"+stopName+"</StopName><Checkpoint>"+ (isCheckPoint ? "true": "false") +"</Checkpoint><PlaybackInformation><TemplateType>none</TemplateType><NumberOfBuses>0</NumberOfBuses><NumberOfTrains>0</NumberOfTrains></PlaybackInformation><Departures><Buses /><Trains /></Departures></CorrespondingTraficData>";
        }

        private void cleanupXml(XmlDocument doc, string lineFilter, DateTime dateTime, out bool onlyBuses)
        {
            try
            {
                onlyBuses = false;
                int dayBusMinutes = int.Parse(ConfigurationSettings.AppSettings["CTraffic_DayBusMinutes"]); // from config
                int nightBusMinutes = int.Parse(ConfigurationSettings.AppSettings["CTraffic_NightBusMinutes"]); // from config
                int trainMinutes = int.Parse(ConfigurationSettings.AppSettings["CTraffic_TrainMinutes"]); // from config
                if (doc.SelectNodes("DepartureBoard/Departure[@type='BUS']").Count + 
                    doc.SelectNodes("DepartureBoard/Departure[@type='NB']").Count  + 
                    doc.SelectNodes("DepartureBoard/Departure[@type='TB']").Count + 
                    doc.SelectNodes("DepartureBoard/Departure[@type='EXB']").Count                 
                     == doc.DocumentElement.ChildNodes.Count)
                {
                    onlyBuses = true;
                    dayBusMinutes = int.Parse(ConfigurationSettings.AppSettings["CTraffic_DayBusMinutesInCaseOfOnlyBuses"]); // from config
                }
                DateTime now = dateTime;
                DateTime busToDateTime = now.AddMinutes(dayBusMinutes);
                DateTime trainToDateTime = now.AddMinutes(trainMinutes);
                DateTime nightBusToDateTime = now.AddMinutes(nightBusMinutes);
                DateTime tbBusToDateTime = now.AddMinutes(dayBusMinutes);
                DateTime exbBusToDateTime = now.AddMinutes(dayBusMinutes);

                ArrayList keys = new ArrayList();
                ArrayList extraNodes = new ArrayList();
                CultureInfo provider = CultureInfo.InvariantCulture;
                foreach (XmlNode node in doc.SelectNodes("DepartureBoard/Departure"))
                {
                    if (lineFilter.CompareTo(node.Attributes["name"].Value) == 0)
                    {
                        extraNodes.Add(node);
                        continue;
                    }
                    string typeAttribute = node.Attributes["type"].Value;
                    string timeAttribute = node.Attributes["time"].Value;
                    string dateAttribute = node.Attributes["date"].Value;

                    DateTime plannedDeparturetime = DateTime.ParseExact(dateAttribute + " " + timeAttribute, "dd.MM.yy HH:mm", provider);
                    if (typeAttribute == "BUS" && plannedDeparturetime > busToDateTime)
                    {
                        extraNodes.Add(node);
                        continue;
                    }
                    if (typeAttribute == "NB" && plannedDeparturetime > nightBusToDateTime)
                    {
                        extraNodes.Add(node);
                        continue;
                    }
                    if (typeAttribute == "EXB" && plannedDeparturetime > exbBusToDateTime)
                    {
                        extraNodes.Add(node);
                        continue;
                    }
                    if (typeAttribute == "TB" && plannedDeparturetime > tbBusToDateTime)
                    {
                        extraNodes.Add(node);
                        continue;
                    }

                    if ( GetVehicleType(typeAttribute) == "Train" && plannedDeparturetime > trainToDateTime) // typeAttribute != "NB" && typeAttribute != "BUS" && typeAttribute != "EXB" && typeAttribute != "TB" && plannedDeparturetime > trainToDateTime)
                    {
                        extraNodes.Add(node);
                        continue;
                    }


                    // Only need to pick first 2 departures of similar vehicle //
                    int maxDeparturesOfSameBus = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxDeparturesOfSameBus"].ToString());
                    int maxDeparturesOfSameTrain = int.Parse(ConfigurationSettings.AppSettings["CTraffic_MaxDeparturesOfSameTrain"].ToString());
                    
                    int totDeparturesForCurrentVehicle = 0;
                                    
                    foreach(string key in keys)
                    {
                        if(key == node.Attributes["name"].Value + node.Attributes["direction"].Value) //direction == destination
                            totDeparturesForCurrentVehicle += 1;
                    }

                    if ((GetVehicleType(typeAttribute) == "Bus" && totDeparturesForCurrentVehicle < maxDeparturesOfSameBus)
                            || (GetVehicleType(typeAttribute) == "Train" && totDeparturesForCurrentVehicle < maxDeparturesOfSameTrain))
                    {
                        keys.Add(node.Attributes["name"].Value + node.Attributes["direction"].Value);                                                    
                    }
                    else
                        extraNodes.Add(node);
                    // ******************************************************* //


                    //if (!keys.Contains(node.Attributes["name"].Value + node.Attributes["direction"].Value)) //direction == destination
                    //{
                    //    keys.Add(node.Attributes["name"].Value + node.Attributes["direction"].Value);
                    //}
                    //else
                    //{
                    //    extraNodes.Add(node);
                    //}
                }
                foreach (XmlNode node in extraNodes)
                {
                    doc.DocumentElement.RemoveChild(node);
                }
                if (doc.SelectNodes("DepartureBoard/Departure[@type='BUS']").Count +
                    doc.SelectNodes("DepartureBoard/Departure[@type='NB']").Count + 
                    doc.SelectNodes("DepartureBoard/Departure[@type='EXB']").Count + 
                    doc.SelectNodes("DepartureBoard/Departure[@type='TB']").Count  == doc.DocumentElement.ChildNodes.Count)
                {
                    onlyBuses = true;
                }
                else
                {
                    onlyBuses = false;
                }

            }
            catch (Exception ex)
            {
                // log?
                throw ex;
            }
        }

        private string GetVehicleType(string type)
        {
            switch(type.ToUpper())
            {
                case "S":
                case "M":
                case "REG":
                case "TOG":
                case "IC":
                case "LYN":
                {
                    return "Train";
                    break;
                } 
                case "BUS":
                case "TB":
                case "EXB":
                case "NB":
                {
                    return "Bus";
                    break;
                }
                default:
                {
                    return "";
                    break;
                }


            }    
        }

        //returns string[]{Line, Type, Name}
        private string[] getTrainLine(string name, string type)
        {           
            string retName = name;
            string retType = string.Empty;
            string retLine = string.Empty;

            string line = string.Empty;

            //S Train
            if (type.ToUpper() == "S")
            {
                retLine = name;
                retType ="S-Train";
            }

            //Metro Train
            if (type.ToUpper() == "M")
            {
                retLine = name.Substring(name.IndexOf(' ')+1);
                retType = "Metro";
            }

            //REG train
            if (type.ToUpper() == "REG")
            {
                retLine = "Re";
                retType = "REG";
            }

            //IR + ØR trains
            if (type.ToUpper() == "TOG")
            {
                retLine = name.Substring(0, name.IndexOf(' '));
                retType = "IRØR";
            }

            //IC trains
            if (type.ToUpper() == "IC")
            {
                retLine = name.Substring(0, name.IndexOf(' '));
                retType = "IC";
            }

            //ICL trains
            if (type.ToUpper() == "LYN")
            {
                retLine = name.Substring(0, name.IndexOf(' '));
                retType = "ICL";
            }
            
            retLine = retLine.Trim();
            
            return new string[] { retLine, retType, retName};

        }

        //returns string[]{Line, Type, Name}
        private string[] getBusLine(string name, string type)
        {

            string retName = name;
            string retType = string.Empty;
            string retLine = string.Empty;

            string line = string.Empty;
            
            //Bus & A Bus
            if (type.ToUpper() == "BUS")  
            {
                retLine = name.Substring(name.IndexOf(' ')+1);
                retType = name.EndsWith("A") ? "A-Bus" : "Bus";
            }

            //T Bus (TeleBus) & H Bus (Harbor Bus)
            if (type.ToUpper() == "TB")
            {
                retLine = name.Substring(name.IndexOf(' ') + 1);
                retType =  name.StartsWith("Bus") ? "T-Bus" : "H-Bus";
            }


            //Express Bus & S Bus
            if (type.ToUpper() == "EXB")
            {
                retLine = name.Substring(name.IndexOf(' ') + 1);
                retType = name.EndsWith("S") ? "S-Bus" : "EX-Bus";
            }

            //Night Bus
            if (type.ToUpper() == "NB")
            {
                retLine = name.Substring(name.IndexOf(' ') + 1);
                retType =  "NB";
            }


            retLine = retLine.Replace("Bus", "").Trim();


            return new string[] {retLine, retType, retName};
        }

        private bool GetCheckPoint(string stopInfoUrl, string stopNumber, out string stopName)
        {
            WebClient feedClient = new WebClient();
            //ICredentials credentials = null;
            feedClient.Credentials = new NetworkCredential(this.Username, this.Password);
            stopName = "";


            string rUrl = String.Format(stopInfoUrl.Replace("{STOPNUMBER}", "{0}"), stopNumber);
            
            feedClient.Encoding = System.Text.Encoding.UTF8;
            string tmpXml = feedClient.DownloadString(rUrl);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(tmpXml);


            XmlNode stopLocation = doc.SelectSingleNode("LocationList/StopLocation");
            if(stopLocation !=null)
            {
                stopName = stopLocation.Attributes["name"].Value.ToString();
            }

            XmlNode node = doc.SelectSingleNode("LocationList/StopLocation/InfotextList/Infotext[@code='KP' and @value='1']");
            if (node!=null)
            {
                return true;
            }

            return false;

        }
    }
}
