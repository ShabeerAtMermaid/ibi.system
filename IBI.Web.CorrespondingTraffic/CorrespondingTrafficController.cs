﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models;
using mermaid.BaseObjects.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.Linq;
using IBI.DataAccess.DBModels;
using System.Net;


namespace IBI.Web.CorrespondingTraffic
{
    public class CorrespondingTrafficController
    {
        /// <summary>
        /// CustomerID & StopNumber are compulsory
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="busNumber"></param>
        /// <param name="stopNumber"></param>
        /// <param name="currentLine"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string GetCorrespondingTraffic(int customerId, string busNumber, string stopNumber, string currentLine, string dateTime)
        {
            //return string.Empty;


            DateTime startDate = DateTime.Now;

            string logEntry = "";
            logEntry += String.Format(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " [StopNumber: {0} , Departures: {1}], Call Started.", stopNumber, ConfigurationManager.AppSettings["CTraffic_ShowJourneys"].ToString()) + Environment.NewLine;
           
            int showJourneys = int.Parse(ConfigurationSettings.AppSettings["CTraffic_ShowJourneys"]);

            DateTime serviceDateTime = DateTime.Now;

            if (!String.IsNullOrEmpty(dateTime))
            {
                serviceDateTime = DateTime.ParseExact(dateTime, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture);

            }

            IBI.ServiceProviders.ProviderFactory dBoardFactory = new IBI.ServiceProviders.ProviderFactory();
            IBI.Shared.Interfaces.ServiceProviders.ICorrespondingTrafficProvider dBoard = dBoardFactory.GetCorrespondingTrafficProvider(customerId);

            string strTraffic = String.Empty;

            List<KeyValuePair<string, object>> parameters = new List<KeyValuePair<string, object>>();
            KeyValuePair<string, object> prmBusNumber = new KeyValuePair<string, object>("BusNumber", busNumber);
            KeyValuePair<string, object> prmStopNumber = new KeyValuePair<string, object>("StopNumber", stopNumber);
            KeyValuePair<string, object> prmBusLine = new KeyValuePair<string, object>("BusLine", currentLine);
            KeyValuePair<string, object> prmShowJourneys = new KeyValuePair<string, object>("ShowJourneys", showJourneys);
            KeyValuePair<string, object> prmDateTime = new KeyValuePair<string, object>("DateTime", dateTime);

            parameters.Add(prmBusNumber);
            parameters.Add(prmStopNumber);
            parameters.Add(prmBusLine);
            parameters.Add(prmShowJourneys);
            parameters.Add(prmDateTime);
             
            strTraffic = dBoard.GetDepartureBoard(parameters, ref logEntry);

            logEntry += String.Format(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " [StopNumber: {0} , Departures: {1}], Call Completed in {2} seconds", stopNumber, ConfigurationManager.AppSettings["CTraffic_ShowJourneys"].ToString(), DateTime.Now.Subtract(startDate).Seconds.ToString()) + Environment.NewLine;
          
            parameters.Clear();
            dBoard = null;
            //WebLogger.Log(String.Format("[StopNumber: {0} , Departures: {1}], Call Completed in {2} seconds", stopNumber, ConfigurationManager.AppSettings["CTraffic_ShowJourneys"].ToString(), DateTime.Now.Subtract(startDate).Seconds.ToString()));
            WebLogger.Log(logEntry);
            return strTraffic;
        }

         

        #region Private Helper
          
        
        #endregion


        //internal static List<Shared.Models.CorrespondingTraffic.ClientConfiguration> GetClientConfigurations(int customerId)
        //{
        //    List<Shared.Models.CorrespondingTraffic.ClientConfiguration> data = new List<Shared.Models.CorrespondingTraffic.ClientConfiguration>();
        //    try
        //    {
        //      using (IBIDataModel dbContext = new IBIDataModel())
        //      {
        //        data =  dbContext.CTConfigurations.Where(c => c.CustomerId == customerId).Select(c => new IBI.Shared.Models.CorrespondingTraffic.ClientConfiguration
        //          {
        //              CustomerId = customerId,
        //              Key = c.ConfigKey,
        //              Value = c.Value
        //          }).ToList();
        //      }   
        //    }
        //    catch(Exception ex)
        //    {
        //        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.CORRESPONDING_TRAFFIC, ex);
        //    }

        //    return data;
        //}

        internal static string GetClientConfigurations(int customerId)
        {
            string data = string.Format("<ClientConfigurations CustomerId = \"{0}\">", customerId.ToString());
 
            //<![CDATA[]]>

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var configs = dbContext.CTConfigurations.Where(c => c.CustomerId == customerId || (c.CustomerId == 0)).OrderBy(o=>o.ConfigKey).ToList(); 

                    foreach(var config in configs)
                    {
                        if (config.CustomerId == 0 && configs.Where(c => c.ConfigKey == config.ConfigKey && c.CustomerId != 0).Count()>0)
                            continue;

                        //data += string.Format("<Config Key=\"{0}\"><Value><![CDATA[{1}]]></Value></Config>", config.ConfigKey, config.Value);
                        data += string.Format("<Config Key=\"{0}\"><Value>{1}</Value></Config>", config.ConfigKey, config.Value);
                        
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.CORRESPONDING_TRAFFIC, ex);
            }

            data += "</ClientConfigurations>";

            return data;
        }
    }
}