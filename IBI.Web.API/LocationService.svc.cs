﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Globalization;
using IBI.DataAccess.DBModels;

namespace IBI.Web.API
{
    public class LocationService : ILocationService
    {
        public Contracts.BusLocation GetBusLocation(int customerID, string busNumber)
        {
            using (IBIDataModel dataModel = new IBIDataModel())
            {
                Bus bus = dataModel.Buses.SingleOrDefault(b => b.CustomerId == customerID && b.BusNumber == busNumber);

                if (bus == null)
                    throw new ApplicationException("LocationService.GetBusLocation: Unknown bus (CustomerID: " + customerID + " , BusNumber: " + busNumber + ")");

                DateTime lastUpdate = DateTime.MinValue;

                /* Loop all clients of the bus to get the latest timestamp */
                var clients = (from c in dataModel.Clients
                               where c.CustomerId == bus.CustomerId && c.BusNumber == bus.BusNumber
                               select c).ToList();

                foreach (Client client in clients)
                {
                    lastUpdate = (lastUpdate > client.LastPing) ? lastUpdate : client.LastPing;
                }

                Contracts.BusLocation location = new Contracts.BusLocation();
                //location.Location = bus.LastKnownLocation.Longitude.Value.ToString(CultureInfo.InvariantCulture) + " " + bus.LastKnownLocation.Latitude.Value.ToString(CultureInfo.InvariantCulture);
                location.Location = bus.LastKnownLocation.Latitude.Value.ToString(CultureInfo.InvariantCulture) + " " + bus.LastKnownLocation.Longitude.Value.ToString(CultureInfo.InvariantCulture);
                location.LastUpdate = lastUpdate;
                location.IsOnline = lastUpdate.AddMinutes(10) > DateTime.Now;

                return location;
            }
        }
    }
}
