﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace IBI.Models.Messages
{
    [DataContract(Namespace = "")]  
    [KnownType(typeof(IBI.Models.Messages.Message))]
    public class MessageCategory
    {
        [DataMember]
        public int MessageCategoryId { get; set; }

        [DataMember]
        public string CategoryName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? Validity { get; set; }

        [DataMember]
        public DateTime? DateAdded { get; set; }

        [DataMember]
        public DateTime? DateModified { get; set; }

        [DataMember]
        public virtual List<Message> Messages {get; set;}
    }
}
