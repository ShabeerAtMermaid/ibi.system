﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IBI.Models.Messages
{
    public class ListAndEditModel
    {
        public Message MessageToEdit { get; set; }
        public List<Message> Messages { get; set; }
    }
}
