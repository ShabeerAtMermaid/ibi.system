using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IBI.Models.BusTree
{
    /// <summary>
    /// IBusTreeItem only satisfies the needs of JQuery DynaTree, even the naming conventions are according to that.
    /// </summary>  
    public interface IBusNode
    { 
        string title { get; set; }
         
        bool isFolder { get; set; }
         
        bool isLazy { get; set; }
         
        string key { get; set; }
         
        List<BusNode> children { get; set; }
    }
}