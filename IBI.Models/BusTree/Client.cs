﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Models.BusTree
{
    public class Client
    {
        [DataMember]
        public int ClientId { get; set; }

        [DataMember]                        
        public string BusNumber { get; set; }

        [DataMember]
        public string ClientType { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public string MacAddress { get; set; }

        [DataMember]
        public DateTime LastPing { get; set; }

        [DataMember]
        public string SimId { get; set; }

        [DataMember]
        public bool MarkedForService { get; set; }
    }
}
