﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Timers;
using System.Diagnostics;

namespace IBI.Test.MakeCall
{
    class Counters
    {
        #region "Variables and Properties"
        private static int connect_counter = 0;
        private static int send_counter = 0;
        private static int receive_counter = 0;
        private static int close_counter = 0;

        private static int connect_error_counter = 0;
        private static int send_error_counter = 0;
        private static int receive_error_counter = 0;
        private static int close_error_counter = 0;
        private static int attempt_counter = 0;


        public static object lock_object = new object();

        public static object lock_object_c = new object();
        public static object lock_object_ce = new object();

        public static object lock_object_s = new object();
        public static object lock_object_se = new object();

        public static object lock_object_r = new object();
        public static object lock_object_re = new object();

        public static object lock_object_cc = new object();
        public static object lock_object_cce = new object();

        public static object lock_object_a = new object();

        public static int ConnectCounter
        {
            get
            {
                lock (lock_object_c)
                {
                    return connect_counter;
                }
            }
            set
            {
                lock (lock_object_c)
                {
                    connect_counter = value;
                }
            }
        }
        public static int ConnectErrorCounter
        {
            get
            {
                lock (lock_object_ce)
                {
                    return connect_error_counter;
                }
            }
            set
            {
                lock (lock_object_ce)
                {
                    connect_error_counter = value;
                }
            }
        }
        public static int SendCounter
        {
            get
            {
                lock (lock_object_s)
                {
                    return send_counter;
                }
            }
            set
            {
                lock (lock_object_s)
                {
                    send_counter = value;
                }
            }
        }
        public static int SendErrorCounter
        {
            get
            {
                lock (lock_object_se)
                {
                    return send_error_counter;
                }
            }
            set
            {
                lock (lock_object_se)
                {
                    send_error_counter = value;
                }
            }
        }
        public static int ReceiveCounter
        {
            get
            {
                lock (lock_object_r)
                {
                    return receive_counter;
                }
            }
            set
            {
                lock (lock_object_r)
                {
                    receive_counter = value;
                }
            }
        }
        public static int ReceiveErrorCounter
        {
            get
            {
                lock (lock_object_re)
                {
                    return receive_error_counter;
                }
            }
            set
            {
                lock (lock_object_re)
                {
                    receive_error_counter = value;
                }
            }
        }
        public static int CloseCounter
        {
            get
            {
                lock (lock_object_cc)
                {
                    return close_counter;
                }
            }
            set
            {
                lock (lock_object_cc)
                {
                    close_counter = value;
                }
            }
        }
        public static int CloseErrorCounter
        {
            get
            {
                lock (lock_object_cce)
                {
                    return close_error_counter;
                }
            }
            set
            {
                lock (lock_object_cce)
                {
                    close_error_counter = value;
                }
            }
        }

        public static int AttemptsCounter
        {
            get
            {
                lock (lock_object_a)
                {
                    return attempt_counter;
                }
            }
            set
            {
                lock (lock_object_a)
                {
                    attempt_counter = value;
                }
            }
        }
        #endregion
    }
    class MainClass
    {
        public static System.Timers.Timer counter_timer = new System.Timers.Timer();
        public static System.Timers.Timer netstat_timer = new System.Timers.Timer();
        private static Alpha[] objectsArray;

        public static void Main(string[] args)
        {
            MainClass clsMain = new MainClass();
            Random random = new Random();
            if (args.Length == 4)
            {
                objectsArray = new Alpha[Int32.Parse(args[3])];
                for (int i = 0; i < Int32.Parse(args[3]); i++)
                {
                    Alpha oAlpha = new Alpha();
                    oAlpha.threadid = i;
                    oAlpha.URL = args[0];
                    oAlpha.querystring = args[1];
                    oAlpha.timeout = int.Parse(args[2]);
                    Thread oThread = new Thread(new ThreadStart(oAlpha.Download));
                    // Start the thread
                    oThread.Start();
                    objectsArray[i] = oAlpha;
                }
                bool net_stat = NetStateStatus();
                for (int i = 0; i < objectsArray.Length; i++)
                {
                    objectsArray[i].time_wait_flag = net_stat;
                }
                while (true)
                {
                    Console.WriteLine("Please type exit to exit");
                    string str = Console.ReadLine();
                    if (str == "exit") break;
                }
            }
        }

        static void netstat_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                bool net_stat = NetStateStatus();
                for (int i = 0; i < objectsArray.Length; i++)
                {
                    objectsArray[i].time_wait_flag = net_stat;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }

        static void counter_timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PrintCounters();
        }
        static private bool NetStateStatus()
        {
            try
            {
                ProcessStartInfo si = new ProcessStartInfo("netstat");
                si.RedirectStandardInput = true;
                si.RedirectStandardOutput = true;
                si.UseShellExecute = false;

                Process nslookup = new Process();
                nslookup.StartInfo = si;
                nslookup.Start();
                string output = nslookup.StandardOutput.ReadToEnd();
                string[] toks = output.Split(new char[4] { ' ', '\t', '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

                int count = 0;
                for (int index = 0; index < toks.Length - 1; index++)
                {
                    if (toks[index] == "192.168.1.22:9090")
                    {
                        if (toks[index + 1] == "TIME_WAIT")
                        {
                            count++;
                            if (count > 1000)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
            return false;
        }
        public static void PrintCounters()
        {
            try
            {
                /// Print Counters
                Console.WriteLine("===================================================================");
                string strLine = "Total Attempts: " + Counters.AttemptsCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Connects: " + Counters.ConnectCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Errors in Connect: " + Counters.ConnectErrorCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Sends: " + Counters.SendCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Errors in Send: " + Counters.SendErrorCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Receive: " + Counters.ReceiveCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Errors in Receive: " + Counters.ReceiveErrorCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Close: " + Counters.CloseCounter.ToString();
                Console.WriteLine(strLine);
                strLine = "Total Errors in Close: " + Counters.CloseErrorCounter.ToString();
                Console.WriteLine(strLine);
                Console.WriteLine("===================================================================");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }
    }

    public class Alpha
    {
        public string URL;
        public string querystring;
        public int timeout;
        public int threadid;
        public bool time_wait_flag = true;

        // This method that will be called when the thread is started
        public void Download()
        {
            while (true)
            {
                Socket server = null;
                try
                {
                    // Cleanup
                    while (true)
                    {
                        if (querystring.StartsWith("Ping", StringComparison.OrdinalIgnoreCase))
                        {
                            string[] pinginfo = querystring.Split(new char[] { ':' });
                            if (pinginfo.Length == 4)
                            {
                                string pingdata = "customerid=" + pinginfo[1] + ";";
                                pingdata += "mac=MAC;";
                                pingdata += "journeynumber=23;";
                                pingdata += "simid=simid123;";
                                pingdata += "lat=40.45455;";
                                pingdata += "lon=41.4545;";
                                pingservice.IBIService service = new pingservice.IBIService();
                                service.Url = URL;

                                service.Ping(pinginfo[2], pinginfo[3], pingdata);
                            }
                            else
                            {
                                Console.WriteLine(DateTime.Now.ToString() + "Thread " + threadid.ToString() + " Ping data is invalid: " + querystring);
                            }
                        }
                        else
                        {
                            using (WebClient wc = new WebClient())
                            {
                                wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                                byte[] result = wc.DownloadData(URL + "?" + querystring);
                                Console.WriteLine(DateTime.Now.ToString() + "Thread " + threadid.ToString() + " Bytes received: " + result.Length);
                            }
                        }
                        Console.WriteLine(DateTime.Now.ToString() + "Thread " + threadid.ToString() + " waiting for " + timeout.ToString() + " milliseconds");
                        Thread.Sleep(timeout);
                        Console.WriteLine(DateTime.Now.ToString() + "Thread " + threadid.ToString() + " waking up after " + timeout.ToString() + " milliseconds");
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine(DateTime.Now.ToString() + "- #29");
                    if (server != null)
                    {
                        if (server.Connected)
                        {
                            server.Shutdown(SocketShutdown.Both);
                            server.Close();
                        }
                    }
                    Console.WriteLine(threadid + " Unable to connect to server.: " + e.Message);
                    Console.WriteLine(threadid + e.StackTrace);
                    //return;
                }
                Thread.Sleep(timeout);
            }
        }
    }
}
