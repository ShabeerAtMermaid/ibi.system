﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using IBI.Shared.Models;
using System.ServiceModel;
using System.Xml.Serialization;

namespace IBI.REST.Types
{

    

    [XmlRootAttribute("List", Namespace = "", IsNullable = false)]
    [CollectionDataContract(Name = "Data", ItemName = "Message", IsReference=false)]     
    public class MessageList : List<IBI.Shared.Models.Messages.Message>
    {
       
    }

    [CollectionDataContract(Name = "Data", ItemName = "MessageCategory")]
    public class MessageCategoryList : List<IBI.Shared.Models.Messages.MessageCategory>
    {
        
    }

    public class Dates
    {
        public static DateTime MinDateTime 
        {
            get
            {
                return new DateTime(0L, DateTimeKind.Utc);
            }
        }
    }

  
}