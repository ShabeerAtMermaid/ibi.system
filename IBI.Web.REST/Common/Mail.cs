﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace IBI.REST.Common
{
    public class Mail
    {

        public static bool SendSimpleMail(string fromAccount, string toAccount, string title, string msg, System.Net.Mail.Attachment[] attachments)
        {

            try
            {
                msg += "<br /><br /><hr size=1 /><br />"; //If you no longer want to recieve this email, please <a href='" + AppPath + "/unsubscribe.aspx'><u>click here to unsubscibe from this email</u></a>.";


                string MyHost = System.Configuration.ConfigurationManager.AppSettings["SMTPHost"].ToString();
                string MySMTPPort = System.Configuration.ConfigurationManager.AppSettings["SMTPPort"].ToString();
                string MySMTPSSL = System.Configuration.ConfigurationManager.AppSettings["SMTPSSL"].ToString();

                System.Net.Mail.MailAddress toAddr = new System.Net.Mail.MailAddress(toAccount);
                System.Net.Mail.MailAddress fromAddr = new System.Net.Mail.MailAddress(fromAccount, "IBI Schedular");

                System.Net.Mail.MailMessage smtpMssg = new System.Net.Mail.MailMessage(fromAddr, toAddr);

                smtpMssg.Subject = title;
                smtpMssg.Body = msg;
                smtpMssg.IsBodyHtml = true;
                smtpMssg.Priority = System.Net.Mail.MailPriority.Normal;

                if ((attachments != null))
                {
                    foreach (System.Net.Mail.Attachment att in attachments)
                    {
                        smtpMssg.Attachments.Add(att);
                    }
                }


                //SmtpClient is built in SMTP class. set SmtpServer= IIS SMTP default domain.
                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(MyHost, Convert.ToInt32(MySMTPPort));
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtpClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;



                if (MySMTPSSL == "true")
                {
                    smtpClient.EnableSsl = true;
                }



                smtpClient.Send(smtpMssg);
                return true;
            }
            catch (Exception ex)
            {
                //Logger.appendToSystemLog();
                //HttpContext.Current.Response.Write("<script>alert('Email not sent | Reason: [" & ex.Message & "]')</script>")
                //throw new Exception("Email not sent | Reason: [" + ex.Message + "]");
                return false;
            }

        }

        public static bool SendSimpleMailWithEmbededImage(string fromAccount, string toAccount, string title, string bodyText, List<Attachment> attachments)
        {
            try
            {
                string MyHost = System.Configuration.ConfigurationManager.AppSettings["SMTPHost"].ToString();
                string MySMTPPort = System.Configuration.ConfigurationManager.AppSettings["SMTPPort"].ToString();
                string MySMTPSSL = System.Configuration.ConfigurationManager.AppSettings["SMTPSSL"].ToString();

                MailAddress toAddr = new MailAddress(toAccount);
                MailAddress fromAddr = new MailAddress(fromAccount, "IBI Schedular");
                MailMessage smtpMssg = new MailMessage(fromAddr, toAddr);

                smtpMssg.Subject = title;
                smtpMssg.Body = bodyText;
                //smtpMssg.AlternateViews.Add(htmlView);
                smtpMssg.IsBodyHtml = true;
                smtpMssg.Priority = System.Net.Mail.MailPriority.Normal;

                if (attachments != null && attachments.Count() > 0)
                {
                    foreach (Attachment att in attachments)
                    {
                        smtpMssg.Attachments.Add(att);
                    }
                }

                //SmtpClient is built in SMTP class. set SmtpServer= IIS SMTP default domain.
                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(MyHost, Convert.ToInt32(MySMTPPort));
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtpClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

                if (MySMTPSSL == "true")
                {
                    smtpClient.EnableSsl = true;
                }

                smtpClient.Send(smtpMssg);
                return true;
            }
            catch (Exception ex)
            {
                //Logger.appendToSystemLog();
                //HttpContext.Current.Response.Write("<script>alert('Email not sent | Reason: [" & ex.Message & "]')</script>")
                //throw new Exception("Email not sent | Reason: [" + ex.Message + "]");
                return false;
            }

        }
    }
}