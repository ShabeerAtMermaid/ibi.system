﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IBI.REST.Common
{
    public class SignSort : IComparer<string>
    {
        public SignSort() { }

        public int Compare(string s1, string s2)
        {
            s1 = s1.TrimStart('#');
            s2 = s2.TrimStart('#');

            int s1_i;
            int s2_i;
            string s1_s;
            string s2_s;

            int intPartLength = 0;

            foreach (char c in s1.ToCharArray())
            {
                if (IsNumeric(c))
                    intPartLength++;
                else
                    break;
            }

            bool result;
            result = int.TryParse(s1.Substring(0, intPartLength), out s1_i);

            if (intPartLength > 0)
                s1_s = s1.Substring(intPartLength);


            intPartLength = 0;
            foreach (char c in s2.ToCharArray())
            {
                if (IsNumeric(c))
                    intPartLength++;
                else
                    break;
            }

            result = int.TryParse(s2.Substring(0, intPartLength), out s2_i);

            if (intPartLength > 0)
                s2_s = s2.Substring(intPartLength);


            if (s1_i > 0 && s2_i > 0)
            {
                if (s1_i > s2_i)
                    return 1;
                if (s1_i < s2_i)
                    return -1;
                if (s1_i == s2_i)
                {
                    return string.Compare(s1, s2, true);
                    //return 0;
                }
            }

            //if (IsNumeric(s1) && IsNumeric(s2))
            //{
            //    if (Convert.ToInt32(s1) > Convert.ToInt32(s2)) return 1;
            //    if (Convert.ToInt32(s1) < Convert.ToInt32(s2)) return -1;
            //    if (Convert.ToInt32(s1) == Convert.ToInt32(s2)) return 0;
            //}

            //if (IsNumeric(s1) && !IsNumeric(s2))
            //    return -1;

            //if (!IsNumeric(s1) && IsNumeric(s2))
            //    return 1;

            return string.Compare(s1, s2, true);
        }

        public static bool IsNumeric(object value)
        {
            try
            {
                int i = Convert.ToInt32(value.ToString());
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}