﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class ScheduleListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("ScheduleListHandler"))
            {
                String customerID = context.Request["customerid"];

                String content = string.Empty;
                if (String.IsNullOrEmpty(customerID))
                    customerID = "2140";

                object customerIDCache = context.Cache[customerID + "schedulelistexpiry"];
                StringBuilder sb = new StringBuilder();
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.ScheduleListCache) > DateTime.Now)
                {
                    content = (String)context.Cache[customerID + "schedulelistcontent"];
                }
                else
                {

                    sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    sb.AppendLine("<Data>");
                    sb.AppendLine("  <Schedules>");

                    using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                    {
                        connection.Open();

                        using (new IBI.Shared.CallCounter("Database connections"))
                        {
                            String script = string.Format("SELECT s.[ScheduleId], s.[Line], s.[FromName], s.[Destination], s.[ViaName], s.LastUpdated, ISNULL((SELECT COUNT(*) FROM ScheduleStops WHERE ScheduleId = s.[ScheduleID]),0) AS StopCount FROM Schedules s WHERE s.CustomerId = {0} ORDER BY dbo.StripNonNumeric([Line]), [Destination]", customerID);

                            using (SqlCommand command = new SqlCommand(script, connection))
                            {
                                SqlDataReader dataReader;

                                using (new IBI.Shared.CallCounter("Database calls"))
                                {
                                    dataReader = command.ExecuteReader();
                                }

                                using (dataReader)
                                {
                                    if (dataReader.HasRows)
                                    {
                                        while (dataReader.Read())
                                        {
                                            int scheduleID = dataReader.GetInt32(dataReader.GetOrdinal("ScheduleId"));
                                            int nOrdinal = dataReader.GetOrdinal("Line");
                                            string line = string.Empty;
                                            if (!dataReader.IsDBNull(nOrdinal))
                                            {
                                                line = dataReader.GetString(nOrdinal);
                                            }

                                            nOrdinal = dataReader.GetOrdinal("Destination");
                                            string destination = string.Empty;
                                            if (!dataReader.IsDBNull(nOrdinal))
                                            {
                                                destination = dataReader.GetString(nOrdinal);
                                            }

                                            nOrdinal = dataReader.GetOrdinal("FromName");
                                            string fromName = string.Empty;
                                            if (!dataReader.IsDBNull(nOrdinal))
                                            {
                                                fromName = dataReader.GetString(nOrdinal);
                                            }

                                            nOrdinal = dataReader.GetOrdinal("ViaName");
                                            string viaName = string.Empty;
                                            if (!dataReader.IsDBNull(nOrdinal))
                                            {
                                                viaName = dataReader.GetString(nOrdinal);
                                            }

                                            nOrdinal = dataReader.GetOrdinal("LastUpdated");
                                            DateTime lastUpdated = DateTime.MinValue;
                                            if (!dataReader.IsDBNull(nOrdinal))
                                            {
                                                lastUpdated = dataReader.GetDateTime(nOrdinal);
                                            }

                                            int stopCount = dataReader.GetInt32(dataReader.GetOrdinal("StopCount"));

                                            if (string.IsNullOrEmpty(viaName))
                                                sb.AppendLine("    <Schedule ID=\"" + scheduleID + "\" line=\"" + line + "\" fromname=\"" + fromName + "\" destination=\"" + destination + "\" stops=\"" + stopCount + "\" lastupdated=\"" + lastUpdated.ToString("yyyy-MM-ddThh:mm:ss") + "\"></Schedule>");
                                            else
                                                sb.AppendLine("    <Schedule ID=\"" + scheduleID + "\" line=\"" + line + "\" fromname=\"" + fromName + "\" destination=\"" + destination + "\" vianame=\"" + viaName + "\" stops=\"" + stopCount + "\"  lastupdated=\"" + lastUpdated.ToString("yyyy-MM-ddThh:mm:ss") + "\"></Schedule>");
                                        }
                                    }
                                }
                            }
                        }
                    }

                    sb.AppendLine("  </Schedules>");
                    sb.AppendLine("</Data>");
                    content = sb.ToString();
                    context.Cache[customerID + "schedulelistcontent"] = content;
                    context.Cache[customerID + "schedulelistexpiry"] = DateTime.Now;
                }
                Byte[] fileContent = Encoding.UTF8.GetBytes(content);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
