﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class DestinationListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("DestinationListHandler"))
            {
                String customerID = context.Request["customerid"];

                if (String.IsNullOrEmpty(customerID))
                    customerID = "2140";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sb.AppendLine("<Data>");
                sb.AppendLine("  <SpecialDestinations>");

                String staticDestinationPath = Path.Combine(AppSettings.GetResourceDirectory(), "StaticDestinations_" + customerID + ".xml");

                XmlDocument staticDestinationDocument = null;

                if (File.Exists(staticDestinationPath))
                {
                    staticDestinationDocument = new XmlDocument();
                    staticDestinationDocument.Load(staticDestinationPath);

                    foreach (XmlNode destinationNode in staticDestinationDocument.SelectNodes("Data/SpecialDestinations/SpecialDestination"))
                    {
                        sb.AppendLine("    <SpecialDestination>" + destinationNode.InnerText + "</SpecialDestination>");
                    }
                }

                sb.AppendLine("  </SpecialDestinations>");
                sb.AppendLine("  <Destinations>");

                if (staticDestinationDocument != null)
                {
                    foreach (XmlNode destinationNode in staticDestinationDocument.SelectNodes("Data/Destinations/Destination"))
                    {
                        XmlAttribute lineAttribute = (XmlAttribute)destinationNode.Attributes.GetNamedItem("line");

                        sb.AppendLine("    <Destination line=\"" + lineAttribute.Value + "\">" + destinationNode.InnerText + "</Destination>");
                    }
                }

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    List<String> addedDestinations = new List<string>();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "SELECT [Line], [Destination], [ViaName]" + Environment.NewLine +
                                        "FROM [Destinations]" + Environment.NewLine +
                                        "WHERE [CustomerID]=" + customerID + " AND ([Excluded]=0 OR [Excluded] IS NULL)" + Environment.NewLine +
                                        "ORDER BY dbo.StripNonNumeric([Line]), [Destination]";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            SqlDataReader dataReader;

                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {
                                        String line = dataReader.GetString(dataReader.GetOrdinal("Line"));
                                        String destination = dataReader.GetString(dataReader.GetOrdinal("Destination"));
                                        String viaName = dataReader.GetString(dataReader.GetOrdinal("ViaName"));

                                        viaName = (viaName == null) ? "" : viaName;

                                        String destinationKey = line + destination + viaName;

                                        if (!addedDestinations.Contains(destinationKey))
                                        {
                                            if (!String.IsNullOrEmpty(viaName))
                                                destination += "/" + viaName;

                                            sb.AppendLine("    <Destination line=\"" + line + "\">" + destination + "</Destination>");

                                            addedDestinations.Add(destinationKey);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                sb.AppendLine("  </Destinations>");
                sb.AppendLine("</Data>");

                Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
