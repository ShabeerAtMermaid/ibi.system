﻿using System;
using System.Web;
using System.IO;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Collections.Generic;
using IBI.REST.Shared;
using IBI.Shared;


namespace IBI.REST.Schedule
{
    public class SignListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("SignListHandler"))
            {
                String customerId = context.Request["customerid"];
                String maintext = context.Request["maintext"];
                String subtext = context.Request["subtext"];
                String line = context.Request["line"];

                if (String.IsNullOrEmpty(customerId))
                    throw new ArgumentException("customerId is not specified", "customerId");

                String key = string.Concat(customerId, maintext, subtext, line);
                String content = string.Empty;

                object customerIDCache = context.Cache[key + "signlistexpiry"];
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.SignListCache) > DateTime.Now)
                {
                    content = (String)context.Cache[key + "signlistcontent"];
                }
                else
                {
                    content = ScheduleServiceController.GetSignsData(int.Parse(customerId), maintext, subtext, line);
                    context.Cache[key + "signlistexpiry"] = DateTime.Now;
                    context.Cache[key + "signlistcontent"] = content;
                }
                Byte[] fileContent = Encoding.UTF8.GetBytes(content);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion
    }
}
