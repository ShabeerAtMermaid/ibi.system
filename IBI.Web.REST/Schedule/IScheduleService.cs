﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared;
using IBI.Shared.Models;
using IBI.Shared.Models.Destinations;
using IBI.Shared.Models.Signs;
using System.Xml;
using IBI.Shared.Models.SignAddons;
using IBI.Shared.Models.ScheduleTreeItems; 

namespace IBI.REST.Schedule
{
    [ServiceContract(Namespace="http://schemas.mermaid.dk/IBI")]
    public interface IScheduleService
    {
        [OperationContract]
        XmlDocument GetCurrentSchedule(String busNo, String customerId);

        [OperationContract]
        XmlDocument DownloadCurrentSchedule(String busNo, String customerId);

        [OperationContract]
        ActiveBusReply GetActiveBus();

        [OperationContract]
        ActiveBusReply GetActiveBusFiltered(int searchSeed, Boolean preferVia);  

        [OperationContract]
        ZoneLookup GetZone(String latitude, String longitude);

        //[OperationContract]
        //List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string plannedDeparture, string plannedArrival, string actualDeparture, string actualArrival);

        //[OperationContract]
        //List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string stopName, string plannedDeparture, string actualDeparture, string actualArrival, string planDifference, string status);

        [OperationContract]
        List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(DateTime startDate, string busNumber);

        [OperationContract]
        List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(DateTime startDate, string busNumber, int journeyNumber);


        #region Signs

        [OperationContract]
        List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsData(int signId);
         

        [OperationContract]
        bool SaveSign(IBI.Shared.Models.Signs.Sign sign, string userName);


        [OperationContract]
        bool DeleteSign(int signId, string userName);

        //[OperationContract]
        //IBI.Shared.Models.Destinations.Destination FindSpecialDestination(int customerId, string line, string destinationName, string selectionStructure);


        [OperationContract]
        SignTree GetSignTree(SignTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping);

        [OperationContract]
        Sign GetSign(int signId);

        [OperationContract]
        Group GetGroup(int groupId);

        [OperationContract]
        bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, int? oldParentId, string userName);

        [OperationContract]
        bool SaveSignGroupSimple(IBI.Shared.Models.Signs.Group group, string userName);


        [OperationContract]
        Sign FindSign(int customerId, int? groupId, string line, string name, string mainText, string subText);

        [OperationContract]
        Group FindSignGroup(int customerId, int? parentGroupId, string groupName);

        [OperationContract]
        bool DeleteSignGroup(int groupId, string userName);

        [OperationContract]
        bool HasChildSignGroup(int groupId);

        [OperationContract]
        bool FindScheduleId(int scheduleId);

        [OperationContract]
        SignGraphicRule GetGraphicRule(int graphicRuleId, string userName);

        [OperationContract]
        int SaveGraphicRule(SignGraphicRule graphicRule, string userName);

        [OperationContract]
        bool SaveSignDataImages(int layoutId,SignXmlData signData, string userName);

        [OperationContract]
        bool SaveSignData(int signItemId, int signRuleId, string userName);

        [OperationContract]
        bool SaveImageGeneratorData(int layoutId, int signItemId, int signRuleId, string userName);

        [OperationContract]
        List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignPreview(int signItemId, int signRuleId, int layoutId, string userName);

        [OperationContract]
        IBI.Shared.Models.Signs.SignXmlLayout  GetSignLayout(int signLayoutId);

        [OperationContract]
        IBI.Shared.Models.Signs.SignGraphicRule  GetSignGraphicRule(int signGraphicRuleId);

        [OperationContract]
        List<SignDataImageItem> GetSignImages(int customerId, int layoutid, int contentid);

        [OperationContract]
        List<string> GetImagePreview(int signItemId, SignGraphicRule signGraphicRule, int layoutId, string userName);

        #endregion

         
        #region SignAddons

        [OperationContract]
        List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonsData(int signAddonItemId);


        [OperationContract]
        bool SaveSignAddon(SignAddon signAddon, string userName);

        [OperationContract]
        bool DeleteSignAddon(int signAddonItemId, string userName);


        [OperationContract]
        SignAddonTree GetSignAddonTree(SignAddonTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping);

        [OperationContract]
        SignAddon GetSignAddon(int signAddonItemId);


        [OperationContract]
        SignAddon FindSignAddon(int customerId, int? groupId, string line, string name, string signText, string udpText);

        [OperationContract]
        bool SaveSignAddonData(int signAddonItemId, int signRuleId, string userName);

        [OperationContract]
        List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonPreview(int signAddonItemId, int signRuleId, int layoutId, string userName);
        

        [OperationContract]
        List<SignDataImageItem> GetSignAddonImages(int customerId, int layoutid, int signAddonItemId);

        [OperationContract]
        bool SaveSignAddonDataImages(int layoutId, SignXmlData signAddonData, string userName);

        [OperationContract]
        List<string> GetSignAddonImagePreview(int signAddonItemId, SignGraphicRule signGraphicRule, int layoutId, string userName);

        [OperationContract]
        bool SaveSignAddonImageGeneratorData(int layoutId, int signAddonItemId, int signRuleId, string userName);

        #endregion 
         
        #region ScheduleTree

        [OperationContract]
        ScheduleTree GetScheduleTree(ScheduleTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping);

        [OperationContract]
        IBI.Shared.Models.ScheduleTreeItems.Schedule GetSchedule(int scheduleId);

        [OperationContract]
        bool CheckExistingSchedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName);


        [OperationContract]
        int UpdateSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule schedule, string forcedOption);


        [OperationContract]
        bool DeleteSchedule(int scheduleId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sch"></param>
        /// <returns>
        ///  > 0 : new schedule created
        ///  0 : error, no schedule saved
        /// -1 : schedule already exists - no changes made (may not occur)
        /// -2 : schedule already exists - with changes
        /// -3 : schedule already exists - no changes
        /// -4 : schedule already exists - updated successfully
        /// </returns>
        [OperationContract]
        int SaveSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule sch, string forcedOption);


        [OperationContract]
        int UpdateScheduleStops(IBI.Shared.Models.ScheduleTreeItems.Schedule sch);


        #endregion

        [OperationContract]
        IBI.Shared.Models.UserScheduleModels.Schedule GetUserSchedule(int userId, int scheduleId);
    }   
}
