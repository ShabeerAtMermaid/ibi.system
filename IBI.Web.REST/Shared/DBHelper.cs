﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using IBI.REST.Types;

namespace IBI.REST.Shared
{
    public class DBHelper
    {
        public static int GetBusCustomerId(String busNumber)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetBusCustomerId"))
            {
                try
                {
                    int customerId = 0;
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        var buses = dbContext.Buses.Where(b => b.BusNumber == busNumber);
                        if (buses != null && buses.Count()>0)
                        {
                            customerId = buses.First().CustomerId;
                        }
                    }
                    return customerId;
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                }
                return 2140;
            }

        }

        public static string GetCustomerName(int customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.GetBusCustomerId"))
            {
                try
                {
                    string customerName = string.Empty;
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        var customers = dbContext.Customers.Where(c => c.CustomerId == customerId);
                        if (customers != null && customers.Count() > 0)
                        {
                            customerName = customers.First().FullName;
                        }
                    }
                    return customerName;
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                }
                return "";
            }

        }



        //returns list of DB entities
        public static List<Group> GetUserGroups(int userId, IBIDataModel dbContext)
        {
            List<Group> list = new List<Group>();
             
                var users = dbContext.Users.Where(u => u.UserId == userId);
                if (users != null && users.Count() > 0)
                {
                    var user = users.First();
                    var uGroups = user.UserGroups;

                    foreach (var uGroup in uGroups)
                    {
                        var groups = uGroup.Groups;
                        if (groups != null)
                        {
                            foreach (Group group in groups)
                            {
                                GetChildGroups(group, ref list, dbContext);
                            }
                        }

                    }
                } 

            return list;
        }

        //recursive function - //returns list of DB entities
        private static void GetChildGroups(Group pGroup, ref List<Group> list, IBIDataModel dbContext)
        {
            //using (dbContext)
            //{
            if(!list.Contains(pGroup))
            {
                list.Add(pGroup); 
                
                foreach (var group in pGroup.Groups)
                { 
                    GetChildGroups(group, ref list, dbContext);

                }
            }
             
        }

        //returns list of DB entities
        private static List<Bus> GetBuses(List<Group> groups, IBIDataModel dbContext)
        {
            List<Bus> list = new List<Bus>();

            //using (dbContext)
            //{
                foreach (Group group in groups)
                {
                    foreach (Bus bus in group.Buses)
                    {
                        if(!list.Contains(bus))
                        {
                            list.Add(bus);                            
                        }
                    }
                }
            //}

            return list;
        }


        public static List<Bus> GetUserBuses(int userId, IBIDataModel dbContext)
        { 
            List<Group> lstGroups = new List<Group>();

            //using (dbContext)
            //{
                var users = dbContext.Users.Where(u => u.UserId == userId);
                if (users != null && users.Count() > 0)
                {
                    var user = users.First();
                    var uGroups = user.UserGroups;

                    foreach (var uGroup in uGroups)
                    {
                        var groups = uGroup.Groups;
                        if (groups != null)
                        {
                            foreach (Group group in groups)
                            {
                                GetChildGroups(group, ref lstGroups, dbContext);
                            }
                        }

                    }
                }

                List<Bus> lstBuses = DBHelper.GetBuses(lstGroups, dbContext);

                return lstBuses;
            //}
        }


        public static List<Bus> GetAllChildGroupsBuses(int groupId, IBIDataModel dbContext)
        {
            List<Group> lstGroups = new List<Group>();
            if (groupId != null && groupId > 0)
            {
                var uGroups = dbContext.Groups.Where(g => g.GroupId == groupId).ToList();
                foreach (var uGroup in uGroups)
                {
                    lstGroups.Add(uGroup); 
                    var groups = uGroup.Groups;
                    if (groups != null)
                    {
                        foreach (Group group in groups)
                        {
                            GetChildGroups(group, ref lstGroups, dbContext);
                        }
                    }
                }
            }
            List<Bus> lstBuses = DBHelper.GetBuses(lstGroups, dbContext);
            return lstBuses;
        }
         

    }
}