﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using IBI.REST.Shared;
using System.ServiceModel.Activation;

namespace IBI.REST.Core
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SystemService : ISystemService
    {
       
        public void SyncAllSupportData()
        {
            using (new IBI.Shared.CallCounter("SystemService.SyncAllSupportData"))
            {
                SystemServiceController.SyncAllSupportData();
            }
            
        }

        public void DumpCurrentStatus()
        {
            using (new IBI.Shared.CallCounter("SystemService.DumpCurrentStatus"))
            {
                SystemServiceController.DumpCurrentStatus();
            }
            
        }

        public void PerformDatabaseMaintenance()
        {
            using (new IBI.Shared.CallCounter("SystemService.PerformDatabaseMaintenance"))
            {
                SystemServiceController.PerformDatabaseMaintenance();
            }
            
        }

        public void SyncAllBusesData()
        {
            using (new IBI.Shared.CallCounter("SystemService.SyncAllBusesData"))
            {
                SystemServiceController.SyncAllBusesData();
            }
            
        }
    }
}