﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using IBI.Shared.Models;
using IBI.REST.Shared;
using IBI.Shared;
using System.ServiceModel.Activation;

namespace IBI.REST.Core
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class CMSService : ICMSService
    {
        public Bus[] getBusList()
        {
            using (new IBI.Shared.CallCounter("CMDService.getBusList"))
            {
                return SystemServiceController.getBusList();
            }
            
        }

        public IBI.Shared.Models.Schedule[] getBusJourneyHistory(int busNumber, DateTime date)
        {
            using (new IBI.Shared.CallCounter("CMSService.getBusJourneyHistory"))
            {
                List<IBI.Shared.Models.Schedule> scheduleList = new List<IBI.Shared.Models.Schedule>();

                using (SqlConnection connection = IBI.Shared.AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        String script = "SELECT DISTINCT [JourneyNumber], [From], [Destination], [Line]" + Environment.NewLine +
                                        "FROM [BusJourneyProgressLog]" + Environment.NewLine +
                                        "WHERE [Timestamp] BETWEEN '" + date.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + date.AddDays(1).ToString("yyyy-MM-dd 00:00:00") + "'" + Environment.NewLine +
                                        "   AND [BusNumber]=" + busNumber.ToString() + Environment.NewLine +
                                        "ORDER BY [Timestamp] ASC";

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            SqlDataReader dataReader;

                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                dataReader = command.ExecuteReader();
                            }

                            using (dataReader)
                            {
                                if (dataReader.HasRows)
                                {
                                    while (dataReader.Read())
                                    {
                                        long journeyNumber = dataReader.GetInt64(dataReader.GetOrdinal("JourneyNumber"));
                                        String from = dataReader.GetString(dataReader.GetOrdinal("From"));
                                        String destination = dataReader.GetString(dataReader.GetOrdinal("Destination"));
                                        String journeyLine = dataReader.GetString(dataReader.GetOrdinal("Line"));

                                        IBI.Shared.Models.Schedule schedule = new IBI.Shared.Models.Schedule();
                                        schedule.BusNumber = busNumber.ToString();
                                        schedule.JourneyNumber = journeyNumber.ToString();
                                        schedule.Line = journeyLine;
                                        schedule.FromName = from;
                                        schedule.DestinationName = destination;

                                        scheduleList.Add(schedule);
                                    }
                                }
                            }
                        }
                    }
                }

                return scheduleList.ToArray();
            }
        }

        public StopInfo[] getJourneyHistory(int busNumber, DateTime date, String line, long journeyNumber)
        {
            //SecurityToken check

            using (new IBI.Shared.CallCounter("CMSService.getJourneyHistory"))
            {
                List<StopInfo> stopList = new List<StopInfo>();

                using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
                {
                    connection.Open();

                    using (new IBI.Shared.CallCounter("Database connections"))
                    {
                        using (SqlConnection stopNameconnection = AppSettings.GetIBIDatabaseConnection())
                        {
                            stopNameconnection.Open();

                            using (new IBI.Shared.CallCounter("Database connections"))
                            {
                                //TODO: difference on line
                                String script = "SELECT *" + Environment.NewLine +
                                                "FROM [BusJourneyProgressLog]" + Environment.NewLine +
                                                "WHERE [Timestamp] BETWEEN '" + date.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + date.AddDays(1).ToString("yyyy-MM-dd 00:00:00") + "'" + Environment.NewLine +
                                                "   AND [BusNumber]=" + busNumber.ToString() + Environment.NewLine +
                                                "   AND [Line]='" + line + "'" + Environment.NewLine +
                                                "   AND [JourneyNumber]=" + journeyNumber.ToString() + Environment.NewLine +
                                                "ORDER BY [Timestamp] ASC";

                                using (SqlCommand command = new SqlCommand(script, connection))
                                {
                                    SqlDataReader dataReader;

                                    using (new IBI.Shared.CallCounter("Database calls"))
                                    {
                                        dataReader = command.ExecuteReader();
                                    }

                                    using (dataReader)
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            while (dataReader.Read())
                                            {
                                                long stopNumber = dataReader.GetInt64(dataReader.GetOrdinal("StopNumber"));
                                                
                                                int customerId = DBHelper.GetBusCustomerId(busNumber.ToString());
                                                String stopName = Schedule.ScheduleServiceController.GetStopName(customerId, stopNumber, ""); //StopName is not 100% correct in reply, so we have to look it up in database
                                                DateTime arrivalTime = dataReader.GetDateTime(dataReader.GetOrdinal("Timestamp"));
                                                DateTime departureTime = dataReader.GetDateTime(dataReader.GetOrdinal("Timestamp"));
                                                int zone = dataReader.GetInt32(dataReader.GetOrdinal("Zone"));

                                                StopInfo stopData = new StopInfo();
                                                stopData.StopNumber = stopNumber.ToString();
                                                stopData.StopName = stopName;
                                                stopData.ArrivalTime = arrivalTime;
                                                stopData.DepartureTime = departureTime;
                                                stopData.Zone = zone.ToString();

                                                stopList.Add(stopData);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                return stopList.ToArray();
            }
        }
    }
}
