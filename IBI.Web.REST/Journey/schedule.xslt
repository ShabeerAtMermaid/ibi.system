﻿<?xml version="1.0" encoding="utf-8"?>
<!-- DWXMLSource="source.xml" -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <Journey>
      <Line>
        <xsl:value-of select="Schedule/Line" />
      </Line>
      <ScheduleNumber>
        <xsl:value-of select="Schedule/ScheduleNumber" />
      </ScheduleNumber>
      <FromName>
        <xsl:value-of select="Schedule/FromName" />
      </FromName>
      <DestinationName>
        <xsl:value-of select="Schedule/DestinationName" />
      </DestinationName>
      <JourneyStops>
        <xsl:for-each select="Schedule/JourneyStops/StopInfo">
          <StopInfo>
            <xsl:attribute name="StopNumber">
              <xsl:value-of select="@StopNumber" />
            </xsl:attribute>
            <StopName>
              <xsl:value-of select="StopName" />
            </StopName>
            <GPSCoordinateNS>
              <xsl:value-of select="GPSCoordinateNS" />
            </GPSCoordinateNS>
            <GPSCoordinateEW>
              <xsl:value-of select="GPSCoordinateEW" />
            </GPSCoordinateEW>
            <Zone>
              <xsl:value-of select="Zone" />
            </Zone>
            <IsCheckpoint>
              <xsl:value-of select="IsCheckpoint" />
            </IsCheckpoint>
            <ActualArrivalTime>              
            </ActualArrivalTime>
            <ActualDepartureTime>              
            </ActualDepartureTime>
            <PlannedDepartureTime>
              <xsl:value-of select="PlannedDepartureTime" />
            </PlannedDepartureTime>
            <PlannedArrivalTime>
              <xsl:value-of select="PlannedDepartureTime" />
            </PlannedArrivalTime>
            <EstimatedArrivalTime>
              <xsl:value-of select="ArrivalTime" />
            </EstimatedArrivalTime>
            <EstimatedDepartureTime>
              <xsl:value-of select="DepartureTime" />
            </EstimatedDepartureTime>
            <Ahead></Ahead>
            <Behind></Behind>
          </StopInfo>
        </xsl:for-each>
      </JourneyStops>
    </Journey>
  </xsl:template>
</xsl:stylesheet>