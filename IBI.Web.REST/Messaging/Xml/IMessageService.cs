﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models.Messages;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;


namespace IBI.REST.Messaging.Xml
{
    
    [XmlSerializerFormat]
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI2")]    
    public interface IMessageService
    {
        /* GetMessages /*********************/ 
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, 
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "Messages?busNumber={busNumber}&customerId={customerId}")]
        List<Message> GetMessages(string busNumber, string customerId);
        //System.ServiceModel.Channels.Message GetMessagesXML(string busNumber, string customerId);


        /* GetMessagesByCategories /*********************/
        [OperationContract]
        [WebGet(            
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "MessageCategories?busNumber={busNumber}&customerId={customerId}")]
        List<MessageCategory> GetMessagesByCategories(string busNumber, string customerId);

        /* MESSAGE /*************************************/
             
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "Message/{messageId}")]
        Message GetMessage(string messageId);

        /* SAVE MESSAGE /*************************************/
          
        [OperationContract]                   
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "SaveMessage")]
        int SaveMessage(string msg);

        /* Get MESSAGE CATEGORIES /*************************************/
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "MessageCategory?customerId={customerId}")]
        List<MessageCategory> GetMessageCategories(string customerId);
         

        [OperationContract]
        [AspNetCacheProfile("IncomingMessageTypesCache")]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "GetIncomingMessageTypes?busNumber={busNumber}&customerId={customerId}")]
            string GetIncomingMessageTypes(string busNumber, int customerId);

         }

}

