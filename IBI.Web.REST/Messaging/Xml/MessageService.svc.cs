﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.Messages;
using IBI.REST.Shared;
using IBI.Shared; 
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace IBI.REST.Messaging.Xml
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MessageService : Xml.IMessageService
    {
        public List<Message> GetMessages(string busNumber, string customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.Xml.GetMessages"))
            {
                IBI.REST.Messaging.MessageService svc = new Messaging.MessageService();
                return svc.GetMessages(busNumber, int.Parse(customerId));
            }
            
            
        }
          
        public List<MessageCategory> GetMessagesByCategories(string busNumber, string customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.Xml.GetMessagesByCategories"))
            {
                IBI.REST.Messaging.MessageService svc = new Messaging.MessageService();

                List<MessageCategory> list = svc.GetMessagesByCategories(busNumber, int.Parse(customerId));
                return list;
            }
            
        }

        public Message GetMessage(string messageId)
        {
            using (new IBI.Shared.CallCounter("MessageService.Xml.GetMessage"))
            {
                IBI.REST.Messaging.MessageService svc = new Messaging.MessageService();
                return svc.GetMessage(int.Parse(messageId));
            }
            
        }
          
        public int SaveMessage(string msg)
        {
            using (new IBI.Shared.CallCounter("MessageService.Xml.SaveMessage"))
            {
                IBI.Shared.Models.Messages.Message m = XmlUtility.Deserialize<IBI.Shared.Models.Messages.Message>(msg);
                return MessageServiceController.Save(m);
            }
            
        }

        public List<MessageCategory> GetMessageCategories(string customerId)
        {
            using (new IBI.Shared.CallCounter("MessageService.Xml.GetMessageCategories"))
            {
                IBI.REST.Messaging.MessageService svc = new Messaging.MessageService();
                return svc.GetMessageCategories(int.Parse(customerId));
            }
            
        }


        public string GetIncomingMessageTypes(string busNumber, int customerId) {
            using (new IBI.Shared.CallCounter("MessageService.Xml.GetIncomingMessageTypes"))
            {
                return MessageServiceController.GetIncomingMessageTypes(busNumber, customerId);
            }
            
        }
     
    }
}