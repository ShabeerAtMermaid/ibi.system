﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models.Messages;
using System.ServiceModel.Web;

namespace IBI.REST.Messaging
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface IMessageService
    {
        /* GetMessages /*********************/

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<Message> GetMessages(string busNumber, int customerId);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<Message> GetArchiveMessages(string busNumber, int customerId);

        //[OperationContract]
        //[WebGet(
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "Messages?busNo={busNumber}&custId={customerId}")]        
        //List<Message> GetMessagesJSON(string busNumber, string customerId);

         

        /* GetMessagesByCategories /*********************/

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<MessageCategory> GetMessagesByCategories(string busNumber, int customerId);

        //[OperationContract]
        //[WebGet(
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "Json/MessageCategories?busNo={busNumber}&custId={customerId}")]
        //List<MessageCategory> GetMessagesByCategoriesJSON(string busNumber, string customerId);

      
        
        /* MESSAGE /*************************************/

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare, 
            ResponseFormat = WebMessageFormat.Json)]
        Message GetMessage(int messageId);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        MessageCategory GetCategory(int categoryId);


        //[OperationContract]
        //[WebGet(
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "Json/Message/{messageId}")]
        //Message GetMessageJSON(string messageId);

      

        /* SAVE MESSAGE /*************************************/
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        int SaveMessage(Message msg);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //    BodyStyle = WebMessageBodyStyle.Bare,
        //    RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "Json/SaveMessage")]
        //bool SaveMessageJSON(string msg);
         

        /* Message Categories /*****************************/

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<MessageCategory> GetMessageCategories(int customerId);
         
        //[KHI]: MessagePing() currenly consumed from IBIService.svc.MessagePing().
        ///*Message Ping from BUS*/
        //[OperationContract]        
        //void MessagePing(string pingData);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,         
            UriTemplate = "Json/IncomingMessages/{userId}"
            )]
        List<IncomingMessage> GetIncomingMessages(string userId);

        [OperationContract]
        [WebGet(BodyStyle=WebMessageBodyStyle.Bare,
             ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Messages.IncomingMessage> GetIncomingMessagesByFilters(int userId, DateTime date, string busNumber, string messageText, string position, string destination, string line, string nextStop, int groupId, bool isArchived);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "Json/ArchiveIncomingMessages/{userId}"
            )]
        List<IncomingMessage> GetArchiveIncomingMessages(string userId);


        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Messages.Message> GetOutgoingMessages(int userId, string busNumber, DateTime validFrom, DateTime validTo, string title, string messageText, string messageCategoryName, int groupId, string senderName, bool showExpiredOnly);


        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        IBI.Shared.Models.Messages.MessageConfirmationsAndReplys GetMessageConfirmationsAndReplys(int messageId, string busNumber);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Messages.IncomingMessage> GetMessageReplies(int messageId, string busNumber);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Messages.MessageDisplayConfirmations> GetMessageConfirmations(int messageId, string busNumber);




        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        List<IBI.Shared.Models.Messages.MessageReplyType> GetMessageReplyTypes();

        [OperationContract]
        [WebGet()]
        bool SetExpireMessage(int messageId);
    }
}
