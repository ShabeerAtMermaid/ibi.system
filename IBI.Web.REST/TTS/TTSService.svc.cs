﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using System.ServiceModel.Activation;

namespace IBI.REST.TTS
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TTSService : ITTSService
    {
        public TTSData GetTTSData()
        {
            using (new IBI.Shared.CallCounter("TTSService.GetTTSData"))
            {
                return TTSServiceController.GetTTSData(String.Empty);
            }
            
        }

        public TTSData GetTTSDataFromDate(DateTime fromDate)
        {
            using (new IBI.Shared.CallCounter("TTSService.GetTTSDataFromDate"))
            {
                return TTSServiceController.GetTTSDataFromDate(fromDate);
            }
            
        }

        public TTSData GetUnfilteredTTSData()
        {
            using (new IBI.Shared.CallCounter("TTSService.GetUnfilteredTTSData"))
            {
                return TTSServiceController.GetUnfilteredTTSData();
            }
            
        }

        public void SyncAudioFiles()
        {
            using (new IBI.Shared.CallCounter("TTSService.SyncAudioFiles"))
            {
                TTSServiceController.SyncAudioFiles();
            }
            
        }

        public void SyncAudioFilesCache()
        {
            using (new IBI.Shared.CallCounter("TTSService.SyncAudioFilesCache"))
            {
                TTSServiceController.SyncAudioFilesCache();
            }
            
        }

        public List<IBI.Shared.Models.TTS.AudioFile> GetStopAudioFiles(string stopName, bool approved, bool rejected, bool unapproved)
        {
            using (new IBI.Shared.CallCounter("TTSService.GetStopAudioFiles"))
            {
                return TTSServiceController.GetStopAudioFiles(stopName, approved, rejected, unapproved);     
            }
                   
        }

        public IBI.Shared.Models.TTS.AudioFile GetAudioFileByVoice(string filename, string voice)
        {
            using (new IBI.Shared.CallCounter("TTSService.GetAudioFileByVoice"))
            {
                return TTSServiceController.GetAudioFile(filename, voice);       
            }
                 
        }

        public byte[] GenerateAudioFile(string stopName, string sourceText, string voice)
        {
            using (new IBI.Shared.CallCounter("TTSService.GenerateAudioFile"))
            {
                return TTSServiceController.GenerateAudioFile(stopName, sourceText, voice);
            }
            
        }

        public byte[] GenerateAudioPreview(string stopName, string sourceText, string voice)
        {
            using (new IBI.Shared.CallCounter("TTSService.GenerateAudioPreview"))
            {
                return TTSServiceController.GenerateAudioPreview(stopName, sourceText, voice);
            }
            
        }

         //public string GenerateAudioFilePath(string stopName, string sourceText, string voice)
         //{
         //    return TTSServiceController.GenerateAudioFilePath(stopName, sourceText, voice);
         //}

        public bool SaveAudioFile(string stopName, string sourceText, string voice, string status)
        {
            using (new IBI.Shared.CallCounter("TTSService.SaveAudioFile"))
            {
                return TTSServiceController.SaveAudioFile(stopName, sourceText, voice, status);
            }
            
        }

        public bool SaveAudioFileMp3(string stopName, string sourceText, string voice, string status, byte[] mp3, string alternateSourceVoiceText)
        {
            using (new IBI.Shared.CallCounter("TTSService.SaveAudioFileMp3"))
            {
                return TTSServiceController.SaveAudioFileMp3(stopName, sourceText, voice, status, mp3, alternateSourceVoiceText);
            }
            
        }


        public List<String> GetAllSourceVoices() {
            using (new IBI.Shared.CallCounter("TTSService.GetAllSourceVoices"))
            {
                return TTSServiceController.GetAllSourceVoices();
            }
            
        }
    }
}
