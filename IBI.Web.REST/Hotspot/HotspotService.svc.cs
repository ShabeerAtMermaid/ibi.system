﻿using IBI.Shared.Diagnostics;
using IBI.Shared.Models;
using IBI.Shared.Models.Hotspot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;

namespace IBI.REST.Hotspot
{
    [XmlSerializerFormat]
    //[ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI", ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class HotspotService : IHotspotService
    {
        public ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> Hotspots(int id, string name, bool fetchAccessPoints)
        {
            using (new IBI.Shared.CallCounter("HotspotService.Hotspots"))
            {
                ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> response = new ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>>();

                try
                {
                    //retriev data here
                    response.Data = HotspotServiceController.GetHotspots(id, name, fetchAccessPoints);

                    response.Message = string.Format("{0} records found", response.Data.Count());
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex)); Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                    response.Status = false;
                    response.Message = string.Format("Failed to get hotspot data [System error log: {0}]", errorId);
                }

                return response;
            }
            
        }

        public ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointGroup>> AccessPointGroups(int hotspotId, int id, string name, bool fetchAccessPoints)
        {
            ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointGroup>> response = new ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointGroup>>();
            using (new IBI.Shared.CallCounter("HotspotService.AccessPointGroups"))
            {
                try
                {
                    //retriev data here
                    response.Data = HotspotServiceController.GetAccessPointGroups(hotspotId, id, name, fetchAccessPoints);

                    response.Message = string.Format("{0} records found", response.Data.Count());
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                    response.Status = false;
                    response.Message = string.Format("Failed to get Access point groups data [System error log: {0}]", errorId);

                }
            }
            

            return response;
        }

        public ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPoint>> AccessPoints(int hotspotId, int accessPointGroupId, int id, string name, string mac, string searchMode)
        {
            ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPoint>> response = new ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPoint>>();
            using (new IBI.Shared.CallCounter("HotspotService.AccessPoints"))
            {
                try
                {
                    //retriev data here
                    response.Data = HotspotServiceController.GetAccessPoints(hotspotId, accessPointGroupId, id, name, mac, searchMode);

                    response.Message = string.Format("{0} records found", response.Data.Count());
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                    response.Status = false;
                    response.Message = string.Format("Failed to get access points data [System error log: {0}]", errorId);

                }
            }
            

            return response;
        }

        public ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointDailyActivity>> AccessPointDailyActivity(int accessPointId, DateTime startDate, DateTime endDate)
        {
            ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointDailyActivity>> response = new ServiceResponse<List<AccessPointDailyActivity>>();
            using (new IBI.Shared.CallCounter("HotspotService.AccessPointDailyActivity"))
            {
                try
                {
                    //retriev data here
                    response.Data = HotspotServiceController.GetAccessPointDailyActivity(accessPointId, startDate, endDate);

                    response.Message = string.Format("{0} records found", response.Data.Count());
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                    response.Status = false;
                    response.Message = string.Format("Failed to get access points daily activity [System error log: {0}]", errorId);
                }
            }
            

            return response;
        }

        public ServiceResponse<bool> SaveAccessPoint(IBI.Shared.Models.Hotspot.AccessPoint accessPoint)
        {
            ServiceResponse<bool> response = new ServiceResponse<bool>();
            using (new IBI.Shared.CallCounter("HotspotService.SaveAccessPoint"))
            {
                try
                {
                    //retriev data here
                    response.Data = HotspotServiceController.SaveAccessPoint(accessPoint);

                    response.Message = string.Format("Accesspont saved successfully");
                    response.Status = true;
                }
                catch (Exception ex)
                {
                    decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                    response.Status = false;
                    response.Message = string.Format("Failed save accesspoint [System error log: {0}]", errorId);
                }
            }
            

            return response;
        }
    }
}


/* SAMPLE WCF FUNCTION
        public ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> Hotspots()
        {
            ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> response = new ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>>();

            try
            {
                //retriev data here
                //response.Data = HotspotController.GetData();

                response.Message = string.Format("{0} records found", response.Data.Count());
                response.Status = true;
            }
            catch(Exception ex)
            {
                decimal errorId = Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.HOTSPOT, Logger.GetDetailedError(ex));
                response.Status = false;
                response.Message = string.Format("Failed to get hotspot data [System erro log: {0}]", errorId);
            }

            return response;
        }
 */