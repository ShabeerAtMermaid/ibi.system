﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.DataAccess.DBModels;

namespace IBI.REST.Hotspot
{
    public class HotspotServiceController
    {


        internal static List<IBI.Shared.Models.Hotspot.Hotspot> GetHotspots(int id, string name, bool fetchAccessPoints)
        {
            using (HotSpotDataModel dbContext = new HotSpotDataModel())
            {
                var data = dbContext.Hotspots.Where(h =>
                    (id == 0 || h.ID == id) &&
                    (string.IsNullOrEmpty(name) || (h.Name.Contains(name) || h.HotspotID.Contains(name)))
                    ).Select(h => new IBI.Shared.Models.Hotspot.Hotspot
                {
                    AccessPointGroups = h.AccessPointGroups.Select(apg => new IBI.Shared.Models.Hotspot.AccessPointGroup
                    {
                        AccessPoints = apg.AccessPoints.Where(ap => fetchAccessPoints != false).Select(ap => new IBI.Shared.Models.Hotspot.AccessPoint
                        {
                            AccessPointGroup_ID = ap.AccessPointGroup_ID,
                            Enabled = ap.Enabled,
                            GlobalIP = ap.GlobalIP,
                            ID = ap.ID,
                            IsDeleted = ap.IsDeleted,
                            LandingPage = ap.LandingPage,
                            LastActivity = ap.LastActivity,
                            LastPing = ap.LastPing,
                            MAC = ap.MAC,
                            Name = ap.Name,
                            PendingCommand = ap.PendingCommand,
                            SSID = ap.SSID,
                            StartPage = ap.StartPage,
                            Theme = ap.Theme
                        }).ToList(),
                        Hotspot_ID = apg.Hotspot_ID,
                        ID = apg.ID,
                        IsDeleted = apg.IsDeleted,
                        LandingPage = apg.LandingPage,
                        Name = apg.Name,
                        SSID = apg.SSID,
                        StartPage = apg.StartPage,
                        Theme = apg.Theme,
                        //TotalAccessPoints = apg.AccessPoints.Count()
                        TotalAccessPoints = apg.AccessPoints.Where(aps => aps.IsDeleted==false).Count()
                    }).ToList(),
                    DailyUserUsageLimitMB = h.DailyUserUsageLimitMB,
                    HotspotID = h.HotspotID,
                    IsDeleted = h.IsDeleted,
                    ID = h.ID,
                    LandingPage = h.LandingPage,
                    Name = h.Name,
                    RegistrationRequired = h.RegistrationRequired,
                    SSID = h.SSID,
                    StartPage = h.StartPage,
                    Theme = h.Theme,
                    UsageLoggingEnabled = h.UsageLoggingEnabled

                }).ToList();

                return data;
            }
        }


        internal static List<IBI.Shared.Models.Hotspot.AccessPointGroup> GetAccessPointGroups(int hotspotId, int id, string name, bool fetchAccessPoints)
        {
            using (HotSpotDataModel dbContext = new HotSpotDataModel())
            {
                var data = dbContext.AccessPointGroups
                    .Where(apg =>
                    (id == 0 || apg.ID == id) &&
                    (hotspotId == 0 || apg.Hotspot_ID == hotspotId) &&
                    (string.IsNullOrEmpty(name) || apg.Name.Contains(name)))
                    .Select(apg => new IBI.Shared.Models.Hotspot.AccessPointGroup
                    {
                        AccessPoints = apg.AccessPoints.Where(ap => fetchAccessPoints != false).Select(ap => new IBI.Shared.Models.Hotspot.AccessPoint
                        {
                            AccessPointGroup_ID = ap.AccessPointGroup_ID,
                            Enabled = ap.Enabled,
                            GlobalIP = ap.GlobalIP,
                            ID = ap.ID,
                            IsDeleted = ap.IsDeleted,
                            LandingPage = ap.LandingPage,
                            LastActivity = ap.LastActivity,
                            LastPing = ap.LastPing,
                            MAC = ap.MAC,
                            Name = ap.Name,
                            PendingCommand = ap.PendingCommand,
                            SSID = ap.SSID,
                            StartPage = ap.StartPage,
                            Theme = ap.Theme
                        }).ToList(),
                        Hotspot_ID = apg.Hotspot_ID,
                        ID = apg.ID,
                        IsDeleted = apg.IsDeleted,
                        LandingPage = apg.LandingPage,
                        Name = apg.Name,
                        SSID = apg.SSID,
                        StartPage = apg.StartPage,
                        Theme = apg.Theme
                    }).ToList();

                return data;
            }
        }


        internal static List<IBI.Shared.Models.Hotspot.AccessPoint> GetAccessPoints(int hotspotId, int accessPointGroupId, int id, string name, string mac, string searchMode)
        {

            DateTime today = DateTime.Now.Date;

            using (HotSpotDataModel dbContext = new HotSpotDataModel())
            {
                List<IBI.Shared.Models.Hotspot.AccessPoint> data = new List<IBI.Shared.Models.Hotspot.AccessPoint>();


                if (searchMode == "and")
                {
                    data = dbContext.AccessPoints.Where(ap =>
                        (hotspotId == 0 || ap.AccessPointGroup.Hotspot_ID == hotspotId) &&
                        (accessPointGroupId == 0 || ap.AccessPointGroup_ID == accessPointGroupId) &&
                        (id == 0 || ap.ID == id) &&
                        (string.IsNullOrEmpty(name) || ap.Name.Contains(name)) &&
                        (string.IsNullOrEmpty(mac) || ap.MAC.Contains(mac))
                        )
                        .Select(ap => new IBI.Shared.Models.Hotspot.AccessPoint
                        {
                            AccessPointGroup_ID = ap.AccessPointGroup_ID,
                            AccessPointGroupName = ap.AccessPointGroup.Name,
                            HotspotId = ap.AccessPointGroup.Hotspot_ID,
                            HotspotName = ap.AccessPointGroup.Hotspot.Name,
                            Enabled = ap.Enabled,
                            GlobalIP = ap.GlobalIP,
                            ID = ap.ID,
                            IsDeleted = ap.IsDeleted,
                            LandingPage = ap.LandingPage,
                            LastActivity = ap.LastActivity,
                            LastPing = ap.LastPing,
                            MAC = ap.MAC,
                            Name = ap.Name,
                            PendingCommand = ap.PendingCommand,
                            SSID = ap.SSID,
                            StartPage = ap.StartPage,
                            Theme = ap.Theme,
                            TodayUsage = ap.UsageStatistics.Where(u => u.StatisticDate == today).Select(u => new IBI.Shared.Models.Hotspot.AccessPointUsage
                            {
                                BytesReceived = u.BytesReceived,
                                BytesSent = u.BytesSent
                            }).FirstOrDefault()

                        }).ToList();
                }
                else
                {
                    data = dbContext.AccessPoints.Where(ap =>
                        ((hotspotId == 0 || ap.AccessPointGroup.Hotspot_ID == hotspotId) &&
                        (accessPointGroupId == 0 || ap.AccessPointGroup_ID == accessPointGroupId)) &&
                        ((id == 0 &&  string.IsNullOrEmpty(name) && string.IsNullOrEmpty(mac))
                            ||
                          ((ap.ID == id && id!=0) || (ap.Name.Contains(name) && !string.IsNullOrEmpty(name)) || (ap.MAC.Contains(mac) && !string.IsNullOrEmpty(mac)))
                        )) 
                        .Select(ap => new IBI.Shared.Models.Hotspot.AccessPoint
                        {
                            AccessPointGroup_ID = ap.AccessPointGroup_ID,
                            AccessPointGroupName = ap.AccessPointGroup.Name,
                            HotspotId = ap.AccessPointGroup.Hotspot_ID,
                            HotspotName = ap.AccessPointGroup.Hotspot.Name,
                            Enabled = ap.Enabled,
                            GlobalIP = ap.GlobalIP,
                            ID = ap.ID,
                            IsDeleted = ap.IsDeleted,
                            LandingPage = ap.LandingPage,
                            LastActivity = ap.LastActivity,
                            LastPing = ap.LastPing,
                            MAC = ap.MAC,
                            Name = ap.Name,
                            PendingCommand = ap.PendingCommand,
                            SSID = ap.SSID,
                            StartPage = ap.StartPage,
                            Theme = ap.Theme,
                            TodayUsage = ap.UsageStatistics.Where(u => u.StatisticDate == today).Select(u => new IBI.Shared.Models.Hotspot.AccessPointUsage
                            {
                                BytesReceived = u.BytesReceived,
                                BytesSent = u.BytesSent
                            }).FirstOrDefault()

                        }).ToList();

                }

                return data;
            }
        }

        internal static List<IBI.Shared.Models.Hotspot.AccessPointDailyActivity> GetAccessPointDailyActivity(int accessPointId, DateTime startDate, DateTime endDate)
        {
            using (HotSpotDataModel dbContext = new HotSpotDataModel())
            {

                var data = dbContext.GetAccessPointDailyActivity(accessPointId, startDate, endDate).OrderByDescending(d => d.StatisticalDate).Select(a =>
                     new IBI.Shared.Models.Hotspot.AccessPointDailyActivity
                     {
                         AccessPoint_Id = a.AccessPoint_ID,
                         ActivityTime = a.ActivityTime,
                         BytesReceived = a.BytesReceived,
                         BytesSent = a.BytesSent,
                         Date = a.StatisticalDate,
                         FailedLogins = a.FailedLogins,
                         SuccessfulLogins = a.SuccesfullLogins
                     }).ToList();

                return data;
            }
        }



        internal static bool SaveAccessPoint(IBI.Shared.Models.Hotspot.AccessPoint accessPoint)
        {
            bool result = false;
            using (HotSpotDataModel dbContext = new HotSpotDataModel())
            {
                var ap = dbContext.AccessPoints.FirstOrDefault(a => a.ID == accessPoint.ID);
                if (ap != null)
                {
                    ap.Name = accessPoint.Name;
                    ap.AccessPointGroup_ID = accessPoint.AccessPointGroup_ID;
                    ap.Enabled = accessPoint.Enabled;
                    ap.IsDeleted = accessPoint.IsDeleted;
                }

                dbContext.SaveChanges();
                result = true;
            }

            return result;
        }
    }
}