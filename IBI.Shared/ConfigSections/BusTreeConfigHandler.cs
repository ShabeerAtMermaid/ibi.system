﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace IBI.Shared.ConfigSections
{


    public class BusTreeService
    {
        public string Name { get; set; }
        public Uri DefaultEndPoint { get; set; }
        public bool Active { get; set; }

        public HogiaCustomer[] Customers { get; set; }

        public BusTreeService(XElement xdoc)
        {

            if (xdoc.Attribute("name") != null)
            {
                this.Name = xdoc.Attribute("name").Value;
            }

            if (xdoc.Attribute("defaultEndPoint") != null)
            {
                this.DefaultEndPoint = new Uri(xdoc.Attribute("defaultEndPoint").Value);
            }

            if (xdoc.Attribute("active") != null)
            {
                this.Active = bool.Parse(xdoc.Attribute("active").Value);
            }

            this.Customers = xdoc.Descendants("Customer").Select(c => new HogiaCustomer(c)).ToArray();

        }
    }

    public class BusTreeCustomer
    {
        public int CustomerId { get; set; }
        public bool Active { get; set; }
        public bool CheckInfotainment { get; set; }
        //public bool CheckHotspot { get; set; }

        public BusTreeCustomer() { }
        public BusTreeCustomer(XElement xdoc, XElement defaultSettingElement)
        {
            bool defaultCheckInfotainment = defaultSettingElement.Attribute("checkInfotainment") == null ? false : bool.Parse(defaultSettingElement.Attribute("checkInfotainment").Value);
            bool defaultCheckHotspot = defaultSettingElement.Attribute("checkHotspot") == null ? false : bool.Parse(defaultSettingElement.Attribute("checkHotspot").Value);
            bool defaultActive = defaultSettingElement.Attribute("active") == null ? false : bool.Parse(defaultSettingElement.Attribute("active").Value);

            if (xdoc.Attribute("customerId") != null)
            {
                this.CustomerId = int.Parse(xdoc.Attribute("customerId").Value);
            }

            if (xdoc.Attribute("active") != null)
            {
                this.Active = bool.Parse(xdoc.Attribute("active").Value);
            }
            else
            {
                this.Active = defaultActive;
            }

            if (xdoc.Descendants("CheckInfotainment") != null)
            {
                this.CheckInfotainment = bool.Parse(xdoc.Descendants("CheckInfotainment").FirstOrDefault().Value);
            }
            else
            {
                this.CheckInfotainment = defaultCheckInfotainment;
            }

            //if (xdoc.Descendants("CheckHotspot") != null)
            //{
            //    this.CheckHotspot = bool.Parse(xdoc.Descendants("CheckHotspot").FirstOrDefault().Value);
            //}
            //else
            //{
            //    this.CheckHotspot = defaultCheckHotspot;
            //}

        }
    }


    public class BusTreeServiceConfigurations
    {

        public BusTreeServiceConfigurations() { }

        public static BusTreeCustomer[] ReadAllConfigurations()
        {
            BusTreeServiceConfig config = (IBI.Shared.ConfigSections.BusTreeServiceConfig)ConfigurationManager.GetSection("BusTreeServiceConfiguration");


            //var subConfigs = (from e in config.Xdoc.Descendants("Service")
            //                  select e);

            XElement defaultSettingElement = config.Xdoc.Descendants("Customers").FirstOrDefault();

            BusTreeCustomer[] services = config.Xdoc.Descendants("Customer").Select(c => new BusTreeCustomer(c, defaultSettingElement)).ToArray();

            return services;
        }

        public static BusTreeCustomer GetCustomerSettings(int customerId)
        {

            try
            {
                BusTreeCustomer[] customers = BusTreeServiceConfigurations.ReadAllConfigurations();
                var customer = customers.Where(c => c.Active && c.CustomerId == customerId).FirstOrDefault();

                if (customer != null)
                {
                    return customer;
                }
                else
                {

                    BusTreeServiceConfig config = (IBI.Shared.ConfigSections.BusTreeServiceConfig)ConfigurationManager.GetSection("BusTreeServiceConfiguration");
                    XElement defaultSettingElement = config.Xdoc.Descendants("Customers").FirstOrDefault();

                    bool defaultActive = defaultSettingElement.Attribute("active") == null ? false : bool.Parse(defaultSettingElement.Attribute("active").Value);
                    bool defaultCheckInfotainment = defaultSettingElement.Attribute("checkInfotainment") == null ? false : bool.Parse(defaultSettingElement.Attribute("checkInfotainment").Value);
                    //bool defaultCheckHotspot = defaultSettingElement.Attribute("checkHotspot") == null ? false : bool.Parse(defaultSettingElement.Attribute("checkHotspot").Value);

                    var cust = new BusTreeCustomer();
                    cust.CustomerId = customerId;
                    cust.Active = defaultActive;
                    cust.CheckInfotainment = defaultCheckInfotainment;
                    //cust.CheckHotspot = defaultCheckHotspot;
                    
                    return cust;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Crashed while reading BusTreeService Customer Configurations. [" + ex.Message + "]");
            }
        }
    }


    public class BusTreeConfigHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return BusTreeServiceConfig.ParseXml(section);
        }
    }
    public class BusTreeServiceConfig
    {
        public XDocument Xdoc { get; set; }
        public static BusTreeServiceConfig ParseXml(XmlNode section)
        {
            BusTreeServiceConfig config = new BusTreeServiceConfig();
            config.Xdoc = XDocument.Parse(section.OuterXml.ToString());
            return config;
        }
    }


}