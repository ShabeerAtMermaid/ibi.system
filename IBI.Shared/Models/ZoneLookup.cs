﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;  


namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class ZoneLookup
    {
        [DataMember(Order = 0, IsRequired = true)]
        public String GPSCoordinate
        {
            get;
            set;
        }

        [DataMember(Order = 1, IsRequired = true)]
        public String Comments
        {
            get;
            set;
        }

        [DataMember(Order = 2, IsRequired = true)]
        public String Zone
        {
            get;
            set;
        
        }

        [DataMember(Order = 3)]
        public String Operator
        {
            get;
            set;

        }

        [DataMember(Order = 4)]
        public String OperatorName
        {
            get;
            set;

        }

    }
}