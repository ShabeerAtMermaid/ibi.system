﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;  


namespace IBI.Shared.Models.Accounts
{
    [DataContract(Namespace = "")] 
    public class Credentials {
        [DataMember]
        public String Username { get; set; }
        [DataMember]
        public String Password { get; set; }
    }

   [DataContract(Namespace = "")] 
    public class User
    {
        [DataMember]
        public int UserId
        {
            get;
            set;
        }

        [DataMember(IsRequired = true)]
        public String Username
        {
            get;
            set;
        }

        [DataMember(IsRequired = true)]
        public String Password
        {
            get;
            set;
        }

        [DataMember]
        public String FullName
        {
            get;
            set;
        }

        [DataMember]
        public String ShortName
        {
            get;
            set;
        }

        [DataMember]
        public String PhoneNumber
        {
            get;
            set;
        }

        [DataMember]
        public String Email
        {
            get;
            set;
        }

        [DataMember]
        public String Company
        {
            get;
            set;
        }


        [DataMember]
        public String Domain
        {
            get;
            set;
        }


        [DataMember]
        public String Description
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateAdded
        {
            get;
            set;
        }

        [DataMember]
        public DateTime DateModified
        {
            get;
            set;
        }

        [DataMember]
        public DateTime? LastLogin
        {
            get;
            set;
        }

        [DataMember]
        public String ErrorMessage
        {
            get;
            set;
        }

        public User()
        {

        }

        public User(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

       [DataMember]
        public bool IsAdmin { get; set; }
    }
}