﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    [DataContract]
    [KnownType(typeof(Sign))]
    public class SignTree
    { 
        [DataMember]
        public List<Sign> Signs { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public int CustomerID { get; set; }

        public SignTree()
        {

        }

        public SignTree(int userId)
        {
            this.UserID = userId;
        }
    }
}
