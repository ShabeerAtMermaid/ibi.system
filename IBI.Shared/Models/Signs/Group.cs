﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    [DataContract]
    public class Group
    {

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public Nullable<int> ParentGroupId { get; set; }

        [DataMember]
        public string ParentGroupName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int SortValue { get; set; }

        [DataMember]
        public bool Excluded { get; set; }

        [DataMember]
        public DateTime DateAdded { get; set; }

        [DataMember]
        public DateTime DateModified { get; set; }
    }
}
