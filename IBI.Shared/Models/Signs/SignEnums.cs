﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Signs
{
    public class SignEnums
    {
        public enum ActiveStatusColor
        {
            NONE = 0,
            GREEN = 1,
            RED = 2
        }

        public enum SignType
        {
            SPECIAL = 100001,
            NORMAL = 100001
        }
    }
}
