﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models.TTS
{
    [DataContract]
    public class AudioFile
    {
         
        [DataMember]
        public DateTime? LastModified
        {
            get;
            set;
        }
         
        [DataMember]
        public String Text
        {
            get;
            set;
        }

         
        [DataMember]
        public String Voice
        {
            get;
            set;
        }
         
        [DataMember]
        public String Filename
        {
            get;
            set;
        }

         
        [DataMember]
        public String FileHash
        {
            get;
            set;
        } 

        [DataMember]
        public bool Approved
        {
            get;
            set;
        }
         
        [DataMember]
        public bool Rejected
        {
            get;
            set;
        }  

        [DataMember]
        public int? Version
        {
            get;
            set;
        }
         
        [DataMember]
        public string SourceVoice
        {
            get;
            set;
        }
         
        [DataMember]
        public string SourceText
        {
            get;
            set;
        }

        [DataMember]
        public byte[] FileContent
        {
            get;
            set;
        }

         
    }
}