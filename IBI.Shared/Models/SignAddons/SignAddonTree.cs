﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.SignAddons
{
    [DataContract]
    [KnownType(typeof(SignAddon))]
    public class SignAddonTree
    { 
        [DataMember]
        public List<SignAddon> SignAddons { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public int CustomerID { get; set; }

        public SignAddonTree()
        {

        }

        public SignAddonTree(int userId)
        {
            this.UserID = userId;
        }
    }
}
