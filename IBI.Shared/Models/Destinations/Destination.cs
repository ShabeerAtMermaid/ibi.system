﻿using IBI.Shared.Models.BusTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Destinations
{
    [DataContract]
    public class Destination : IEquatable<Destination>
    {
        private const string IMAGE_ROOT_PATH = "/images/destinations";

        //basic properties from interface --> restricted to keep naming convention bcoz of JQuery Tree internal naming structure.

        [DataMember(Order = 1)]
        public string title { get; set; }

        [DataMember(Order = 2)]
        public bool isFolder { get; set; }

        [DataMember(Order = 3)]
        public bool isLazy { get; set; }


        [DataMember(Order = 4)]
        public string key { get; set; }

        [DataMember(Order = 5)]
        public bool expand { get; set; }


        [DataMember(Order = 6)]
        public List<Destination> children { get; set; }

        //additional properties

        [DataMember]
        public int Pk { get; set; }

        [DataMember]
        public string Line { get; set; }

        [DataMember]
        public int SqlId { get; set; }

        [DataMember]
        public string DestinationName { get; set; }

        [DataMember]
        public string Text { get; set; }
        
        [DataMember]
        public string MainText { get; set; }

        [DataMember]
        [DefaultValue("")]
        public string SubText { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ViaName { get; set; }
         
        [DataMember]
        public string SelectionStructure
        {
            get;
            set;
           
        }

        [DataMember]
        public string Route { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public bool IsGroup { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string DateAdded { get; set; }

        [DataMember]
        public string DateModified { get; set; }

        [DataMember]
        public string ParentGroupId { get; set; }

        [DataMember]
        public string ParentGroupName { get; set; }

        [DataMember]
        public int Priority { get; set; }



        [DataMember]
        public String AlternateRowCss { get; set; }


        [DataMember]
        public String ClientInformation { get; set; }

        private static int rowNum;


        public Destination()
        {
            DestinationName = "";
            Line = "";
            SelectionStructure = "";
            CustomerName = "";
            Description = "";
            IsGroup = false;
            IsActive = false;
            DateAdded = DateTime.MinValue.ToString();
            DateModified = DateTime.MinValue.ToString();
            ParentGroupId = "0";
            ParentGroupName = "";
            Priority = 0;
            Text = "";

            MainText = SubText = Name = "";

            children = new List<Destination>();


        }


        //this function needs to be changed if any requirements changes for Tree GUI.
        public void SetNode()
        
        {
            this.Text = this.Text ?? "";
            this.MainText = this.MainText ?? "";
            this.SubText = this.SubText ?? "";
            this.Line = this.Line ?? "";
            this.DestinationName = this.DestinationName ?? "";
            this.SelectionStructure = this.SelectionStructure ?? "";
            this.Name = this.Name ?? "";

            int type = 1;

            if (this.isFolder)
                type = 1;
            else
                type = 2;


            this.expand = true;

            if (string.IsNullOrEmpty(this.Text))
                this.Text = this.DestinationName;
            
            if (this.Text.Contains("/"))
            {
                string[] arrText = this.Text.Split('/');

                if (arrText.Length > 0)
                    MainText = arrText[0];
                if (arrText.Length > 1)
                    SubText = arrText[1];
            }
            else
            {
                this.MainText = this.Text;
                this.SubText = "";
            }

            if (this.SelectionStructure.Contains("/"))
                this.Name = this.SelectionStructure.Substring(this.SelectionStructure.LastIndexOf("/") + 1);
            else
                this.Name = this.SelectionStructure;

            

            this.Name = this.Name.Replace("#", "");
            
            
            
            //set alternate rowCSS

            if (type == 2 && this.IsGroup == false)
            {
                this.AlternateRowCss = rowNum % 2 == 0 ? " grey" : " white";
                //increment rowNum
                rowNum++;
            }
            else { this.AlternateRowCss = " emptyRow"; }


            if (IsGroup == true)
            {
                //this.Text = "";

                if (this.DestinationName == "Special Destinations")
                {
                    this.title = "~~~~~~~~";
                    return;
                }

                if (this.DestinationName == "Destinations")
                {
                    this.title = "~~~~~~~~";
                    return;
                }

                this.title = this.Name + "~~~~~~~~~";

            }
            else
            {

               // string text = String.IsNullOrEmpty(this.SubText) ? this.MainText : this.MainText + "/" + this.SubText;

                this.title = this.Name + "~" + this.Line + "~" + this.MainText + "~" + this.SubText +"~" + (this.SqlId.ToString().EndsWith("0000") ? "" : this.SqlId.ToString()) + "~" + GetStatusImage(this.IsActive) + "~" + GetEditLink() + "~" + GetDestImageLink();
            }

        }

        private string GetEditLink()
        {
            return "[EDIT]";
        }


        private string GetDestImageLink()
        {
            return "[IMAGE]";
        }

        private string GetStatusImage(bool status)
        {

            string img = "";

            img = status ? "/green.png" : "/red.png";

            if (img == "")
                return "";

            string tooltip = status ? "Active" : "Inactive";


            string imgPath = BusUtility.AppPath + IMAGE_ROOT_PATH + img;

            string retImg = @"<img title='" + tooltip +
                            "' src='" + imgPath +
                            "' style='vertical-align: middle; width: 12px; height: 12px;' />";
            return retImg;
        }

        private string GetScroolImages()
        {


            string imgUpPath = BusUtility.AppPath + IMAGE_ROOT_PATH + "/scrollup_grey.png";
            string imgDownPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/scrolldown.png";

            string retImg1 = @"<img title='" +
                            "' src='" + imgUpPath +
                            "' style='display: inline; vertical-align: middle; width: 14px; height: 10px;' />";

            string retImg2 = @"<img title='" +
                            "' src='" + imgDownPath +
                            "' style='display: inline; vertical-align: middle; width: 14px; height: 10px;' />";
            return retImg1 + retImg2;
        }

        private string GetSpecialDestImage()
        {
            string imgPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/DCU1.png";

            string retImg1 = @"<img title='" +
                            "' src='" + imgPath +
                            "' style='display: inline; vertical-align: middle; width: 64px; height: 39px;' />";

            return retImg1;
        }


        private string GetDestImage()
        {
            string imgPath = BusUtility.AppPath + ".." + IMAGE_ROOT_PATH + "/DCU2.png";

            string retImg1 = @"<img title='" +
                            "' src='" + imgPath +
                            "' style='display: inline; vertical-align: middle; width: 64px; height: 39px;' />";

            return retImg1;
        }

        public bool Equals(Destination other)
        {
            return this.DestinationName == other.DestinationName &&
               this.ParentGroupName == other.ParentGroupName &&
                this.SelectionStructure == other.SelectionStructure &&
               this.Line == other.Line &&
               (other.IsGroup==true || this.Text == other.Text )
               ;
        }

    }
}
