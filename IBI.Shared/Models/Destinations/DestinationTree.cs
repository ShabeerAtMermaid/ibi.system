﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Destinations
{
    [DataContract]
    [KnownType(typeof(Destination))]
    public class DestinationTree
    {
        [DataMember]
        public List<Destination> SpecialDestinations { get; set; }

        [DataMember]
        public List<Destination> Destinations { get; set; }

        [DataMember]
        public int UserID { get; set; }

        [DataMember]
        public int CustomerID { get; set; }

        public DestinationTree()
        {

        }

        public DestinationTree(int userId)
        {
            this.UserID = userId;
        }
    }
}
