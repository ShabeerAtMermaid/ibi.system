﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IBI.Shared.Models.BusTree
{
    [DataContract]
    public class Group
    {

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public Nullable<int> CustomerId { get; set; }

        [DataMember]
        public string CustomerName { get; set; }

        [DataMember]
        public Nullable<int> ParentGroupId { get; set; }

        [DataMember]
        public string ParentGroupName { get; set; }

        [DataMember]
        public string Description { get; set; }


    }
}
