﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class AudioFile
    {
        [XmlAttribute]
        [DataMember(Order = 0)]
        public int Version
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(Order = 1)]
        public String File
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(IsRequired = true, Order = 2)]
        public String Hash
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(IsRequired = true, Order = 3)]
        public int Approved
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(IsRequired = true, Order = 4)]
        public int Rejected
        {
            get;
            set;
        }


        [XmlAttribute]
        [DataMember(IsRequired = true, Order = 5)]
        public String Stop
        {
            get;
            set;
        }
    }
}