﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Common
{

    //USAGE

    /*
     Usage : in case u have to create POST type request

        class Program
        {
            private static void Main()
            {
                ExtWebClient webclient = new ExtWebClient();
                webclient.PostParam = new NameValueCollection();
                webclient.PostParam["param1"] = "value1";
                webclient.PostParam["param2"] = "value2";

                webclient.DownloadFile("http://www.example.com/myfile.zip", @"C:\myfile.zip");
            }
        }

        Usage : for GET type request, u can simply use the Normal webclient

        class Program
        {
            private static void Main()
            {
                WebClient webclient = new WebClient();

                webclient.DownloadFile("http://www.example.com/myfile.zip?param1=value1&param2=value2", @"C:\myfile.zip");
            }
        }
  
     */

    public class IBIWebClient : WebClient
    {

        public NameValueCollection PostParam { get; set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest tmprequest = base.GetWebRequest(address);

            HttpWebRequest request = tmprequest as HttpWebRequest;

            if (request != null && PostParam != null && PostParam.Count > 0)
            {
                StringBuilder postBuilder = new StringBuilder();
                request.Method = "POST";
                //build the post string

                for (int i = 0; i < PostParam.Count; i++)
                {
                    postBuilder.AppendFormat("{0}={1}", Uri.EscapeDataString(PostParam.GetKey(i)),
                                             Uri.EscapeDataString(PostParam.Get(i)));
                    if (i < PostParam.Count - 1)
                    {
                        postBuilder.Append("&");
                    }
                }
                byte[] postBytes = Encoding.ASCII.GetBytes(postBuilder.ToString());
                request.ContentLength = postBytes.Length;
                request.ContentType = "application/x-www-form-urlencoded";

                var stream = request.GetRequestStream();
                stream.Write(postBytes, 0, postBytes.Length);
                stream.Close();
                stream.Dispose();

            }

            return tmprequest;
        }
    }
}
