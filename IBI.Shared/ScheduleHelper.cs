﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared
{
    public class ScheduleHelper
    {

        public enum StopPrefix
        {
            MOVIA = 9025200,
            HACON = 9125200,
            NORGBUS = 9225200,
            HAFAS = 9325200
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stopNo"></param>
        /// <param name="prefix">
        /// default prefix of Movia = 9025200
        /// default prefix of Hacon = 9125200
        /// </param>
        /// <returns></returns>        
        public static string TransformStopNo(string stopNo, StopPrefix prefix)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = prefix.GetHashCode().ToString() + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        public static string UnTransformStopNo(string stopNo, StopPrefix prefix)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.unTransformStopNo"))
            {

                //eg. 9025200000028662
                if (stopNo.Length == 16) // it's a transformed stop number
                {
                    stopNo = Convert.ToInt32(stopNo.Replace(prefix.GetHashCode().ToString(), "")).ToString();
                }

                return stopNo;
            }
        }
    }
}
