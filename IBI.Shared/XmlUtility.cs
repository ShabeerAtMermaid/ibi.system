﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace IBI.Shared
{
    public class XmlUtility
    {
        
        public static string Serialize<T>(T objectToSerialize)
        {
            StringWriter outStream = new StringWriter();
            System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(T));
            s.Serialize(outStream, objectToSerialize);
            return outStream.ToString();

        }

        public static string SerializeBinary<T>(T objectToSerialize)
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream memStr = new MemoryStream();

            try
            {
                bf.Serialize(memStr, objectToSerialize);
                memStr.Position = 0;

                return Convert.ToBase64String(memStr.ToArray());
            }
            finally
            {
                memStr.Close();
            }
        }

        //Deserialize from XML String
        public static T Deserialize<T>(string xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);

            return Deserialize<T>(xmlDoc);

            //code below will not be reached.


            MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(xmlString));
            stream.Position = 0;
            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());

            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            T obj = (T)ser.ReadObject(reader, true);

            return obj;
        }

        //Deserialize from xmlDocument.
        public static T Deserialize<T>(XmlDocument xmlDoc)
        {
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
            xmlDoc.WriteTo(xmlWriter);

            MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(stringWriter.ToString()));
            stream.Position = 0;
            XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());

            DataContractSerializer ser = new DataContractSerializer(typeof(T));
            T obj = (T)ser.ReadObject(reader, true);

            return obj;
        }

    }
}
