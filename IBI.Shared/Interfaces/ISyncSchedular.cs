﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBI.Shared.Models;

namespace IBI.Shared.Interfaces
{
    public interface ISyncSchedular
    {
        int CustomerId { get; set; } 
        void SyncAllStops();
        string DownloadCurrentSchedule(string busNumber);
        string GetCurrentSchedule(string resourcePath, string busNumber);
        ActiveBusReply GetActiveBus(string resourcepath, int searchSeed, bool preferViaNames);
        ZoneLookup GetZone(string latitude, string longitude);
        string GetStopName(long stopNo, string originalStopName);
       
    }
}
