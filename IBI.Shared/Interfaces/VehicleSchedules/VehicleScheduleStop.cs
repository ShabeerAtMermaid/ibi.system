﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Interfaces.VehicleSchedules
{
    public class VehicleScheduleStop
    {
        #region Attributes
        private string _stopid;
        private string _stopName;
        private string _stopShortName;
        private VehicleScheduleStopInfotainment _infotainment;
        #endregion

        #region Properties
        public string StopId
        {
            get
            {
                return _stopid;
            }
            set
            {
                _stopid = value;
            }
        }

        public string StopName
        {
            get
            {
                return _stopName;
            }
            set
            {
                _stopName = value;
            }
        }

        public string StopShortName
        {
            get
            {
                return _stopShortName;
            }
            set
            {
                _stopShortName = value;
            }
        }

        public VehicleScheduleStopInfotainment Announcement
        {
            get
            {
                return _infotainment;
            }
            set
            {
                _infotainment = value;
            }
        }
        #endregion

        public VehicleScheduleStop(string stopid, string stopName, string stopShortName, VehicleScheduleStopInfotainment infotainment)
        {
            _stopid = stopid;
            _stopName = stopName;
            _stopShortName = stopShortName;
            _infotainment = infotainment;
        }
    }
}
