﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcContrib.UI.Grid;

namespace IBI.Web.Infrastructure.ContribGrid
{
    public class GridNoHeaderRenderer<T> :
     HtmlTableGridRenderer<T> where T : class
    {
        protected override bool RenderHeader()
        {
            // Explicitly returning true would suppress the header
            // just fine, however, Render() will always assume that
            // items exist in collection and RenderEmpty() will
            // never be called.  
            // In other words, return ShouldRenderHeader() if you
            // want to maintain the Empty text when no items exist.
            return ShouldRenderHeader();
        }
    }
}