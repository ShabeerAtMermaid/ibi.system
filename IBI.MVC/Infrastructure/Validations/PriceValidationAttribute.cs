﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace IBI.Web.Infrastructure.Validations
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PriceValidationAttribute : ValidationAttribute
    {
        private decimal minPrice = 0.01M;
        private decimal maxPrice = 100.00M;

        public PriceValidationAttribute()
            : base("The price is not in a valid range")
        {
        }

        public override bool IsValid(object value)
        {
            decimal price = (decimal)value;
            if (price < this.minPrice || price > this.maxPrice)
                return false;
            return true;
        }

    }
}