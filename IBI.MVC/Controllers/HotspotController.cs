﻿using IBI.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.Controllers
{
    public class HotspotController : Controller
    {
        //
        // GET: /Hotspot/

        [HttpGet]
        public ActionResult Hotspots()
        {
            var model = HotspotServiceManager.Hotspots();
            
            return View(model);
        }

        [HttpGet]
        public ActionResult HotspotList(string search, bool showDeleted = false)
        {
            search = Server.UrlDecode(search);
            var model = HotspotServiceManager.Hotspots(@name: search);

            if (!showDeleted) {
                model.Data = model.Data.Where(d => d.IsDeleted == false).ToList();
            } 
            return View(model);           
        }



        [HttpGet]
        public ActionResult AccessPoints(int hotspotId = 0, int apgId = 0, int id = 0, string name = "", string mac = "")
        {
            //var model = HotspotServiceManager.AccessPoints(hotspotId, apgId, id, name, mac);
            ViewBag.HotspotId = hotspotId;
            ViewBag.AccessPointGroupId = apgId;
            ViewBag.Id = id;
            ViewBag.Name = name;
            ViewBag.MAC = mac;

            ViewBag.Hotspots =  new SelectList(HotspotServiceManager.Hotspots().Data, "ID", "Name", hotspotId);
            ViewBag.AccessPointGroups = new SelectList(HotspotServiceManager.AccessPointGroups(hotspotId: hotspotId).Data, "ID", "Name", apgId); 

            return View();
        }


        [HttpGet]
        public ActionResult AccessPointList(int hotspotId = 0, int apgId = 0, int id = 0, string name = "", string mac = "", bool showDeleted = false, string searchMode = "and")
        {
            //System.Threading.Thread.Sleep(3000);
            var model = HotspotServiceManager.AccessPoints(hotspotId, apgId, id, name, mac, searchMode);

            if (!showDeleted)
            {
                model.Data = model.Data.Where(d => d.IsDeleted == false).ToList();
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult EditAccessPoint(int accessPointId)
        {

            var response = HotspotServiceManager.AccessPoints(id: accessPointId);

            IBI.Shared.Models.Hotspot.AccessPoint model = new Shared.Models.Hotspot.AccessPoint();
            
            if (response.Data.Count() > 0)
            {

                model = response.Data.FirstOrDefault();
                ViewBag.AccessPointGroups = new SelectList(HotspotServiceManager.AccessPointGroups(hotspotId: model.HotspotId).Data, "ID", "Name", model.AccessPointGroup_ID); 
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EditAccessPoint(IBI.Shared.Models.Hotspot.AccessPoint model)
        {
            bool result = HotspotServiceManager.SaveAccessPoint(model);

            if (Request.IsAjaxRequest())
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult AccessPointActivity(int accessPointId)
        {
            ViewBag.AccessPointId = accessPointId;

            //var model = HotspotServiceManager.AccessPointDailyActivity(accessPointId, startDate.Value, endDate.Value); 
            // return View(model);

            return View();
        }


        [HttpGet]
        public ActionResult AccessPointActivityList(int accessPointId, int days)
        {
            System.Threading.Thread.Sleep(2000);
            DateTime endDate = DateTime.Now.Date;
            DateTime startDate = endDate.AddDays(-(days-1)).Date;

            var model = HotspotServiceManager.AccessPointDailyActivity(accessPointId, startDate, endDate);

            return View(model);
        }


    #region Helper Functions

        public JsonResult AccessPointGroupsByHotspotId(int hotspotId)
        {
            var data = new SelectList(HotspotServiceManager.AccessPointGroups(@hotspotId: hotspotId).Data, "ID", "Name", hotspotId);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

    #endregion
    
    }


    #region Hotspot Service Manager

    public class HotspotServiceManager{

        public static ServiceResponse<List<IBI.Shared.Models.Hotspot.Hotspot>> Hotspots(int id=0, string name="", bool fetchAccessPoints=false)
        {
            HotspotService.HotspotServiceClient client = new HotspotService.HotspotServiceClient();
            var retValue = client.Hotspots(id, name, fetchAccessPoints);
            client.Close();
            return retValue;
        }

        public static ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointGroup>> AccessPointGroups(int hotspotId = 0, int id=0, string name="", bool fetchAccessPoints=false)
        {
            HotspotService.HotspotServiceClient client = new HotspotService.HotspotServiceClient();
            var retValue = client.AccessPointGroups(hotspotId, id, name, fetchAccessPoints);
            client.Close();
            return retValue;
        }

        public static ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPoint>> AccessPoints(int hotspotId=0, int accessPointGroupId=0, int id=0, string name="", string mac="", string searchMode = "and")
        {
            HotspotService.HotspotServiceClient client = new HotspotService.HotspotServiceClient();
            var retValue = client.AccessPoints(hotspotId, accessPointGroupId, id, name, mac, searchMode);
            client.Close();
            return retValue;
        }

        public static ServiceResponse<List<IBI.Shared.Models.Hotspot.AccessPointDailyActivity>> AccessPointDailyActivity(int accessPointId, DateTime startDate, DateTime endDate)
        {
            HotspotService.HotspotServiceClient client = new HotspotService.HotspotServiceClient();
            var retValue = client.AccessPointDailyActivity(accessPointId, startDate, endDate);
            client.Close();
            return retValue;
        }


        internal static bool SaveAccessPoint(Shared.Models.Hotspot.AccessPoint accessPoint)
        {
            HotspotService.HotspotServiceClient client = new HotspotService.HotspotServiceClient();
            var retValue = client.SaveAccessPoint(accessPoint);
            client.Close();
            return retValue.Data;
        }
    }
        #endregion
}
