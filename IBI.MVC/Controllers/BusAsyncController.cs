﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Text;
using System.Net;
using System.Web.Script.Serialization;
using IBI.Shared.Models.BusTree;
using IBI.Web.Infrastructure.Ajax;
using IBI.Shared.Models;
using IBI.Shared.Models.Messages;
using IBI.Shared.Models.BusTree;
using IBI.Web.ViewModels.BusStatusLog;
using IBI.Web.Common;
using IBI.Web.Infrastructure;
using MvcContrib.UI.Grid;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using IBI.Web.Infrastructure.Keys;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.Logger;
using IBI.Web.ViewModels.BusTree.BusGroup;


namespace IBI.Web.Controllers
{
    [AuthorizeLogin]
    public class BusAsyncController : AsyncController
    {

        private List<BusStatusColor> Filters
        {
            get
            {

                List<BusStatusColor> filters = SessionManager.Get<List<BusStatusColor>>(SessionKeys.BusTreeFilter);
                if (filters == null)
                {
                    filters = new List<BusStatusColor>
                    { 
                        BusStatusColor.GREEN,
                        BusStatusColor.GREY,
                        BusStatusColor.LIGHT_GREY,
                        BusStatusColor.ORANGE,
                        BusStatusColor.RED,
                        BusStatusColor.YELLOW
                    };
                    SessionManager.Store(SessionKeys.BusTreeFilter, filters);
                }

                return filters;
            }
            set
            {
                SessionManager.Store(SessionKeys.BusTreeFilter, value);
            }
        }
        //
        // GET: /Bus/

        #region Index Controller

        public ActionResult Index()
        {
            return View();
        }

        #endregion


        #region BusTree
        [HttpGet]
        [ActionName("BusTree")]
        public ActionResult BusTree(bool? importLiveDB)
        {
            return View("BusTree");
        }

        [HttpPost]
        [ActionName("BusTree")]
        public ActionResult BusTree()
        {
            List<BusStatusColor> filters = new List<BusStatusColor>();

            if (Request.Form["chkGreen"] == "on")
                filters.Add(BusStatusColor.GREEN);

            if (Request.Form["chkGrey"] == "on")
                filters.Add(BusStatusColor.GREY);
            if (Request.Form["chkLightGrey"] == "on")
                filters.Add(BusStatusColor.LIGHT_GREY);
            if (Request.Form["chkOrange"] == "on")
                filters.Add(BusStatusColor.ORANGE);
            if (Request.Form["chkRed"] == "on")
                filters.Add(BusStatusColor.RED);
            if (Request.Form["chkYellow"] == "on")
                filters.Add(BusStatusColor.YELLOW);

            this.Filters = filters;

            return View("BusTree");
        }

        public void GetTreeAsync(bool[] filters)
        {
            ViewBag.SyncOrAsync = "Asynchronous";

            IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);



            if (usr != null && usr.UserId > 0)
            {

                this.Filters.Clear();

                if (filters == null) 
                {
                    this.Filters.Add(BusStatusColor.RED);
                    this.Filters.Add(BusStatusColor.YELLOW);
                    this.Filters.Add(BusStatusColor.GREEN);
                    this.Filters.Add(BusStatusColor.ORANGE);
                    this.Filters.Add(BusStatusColor.LIGHT_GREY);
                    this.Filters.Add(BusStatusColor.GREY);
                }
                else
                {
                    if (filters.Length > 0 && filters[0] == true)
                        this.Filters.Add(BusStatusColor.RED);
                    if (filters.Length > 1 && filters[1] == true)
                        this.Filters.Add(BusStatusColor.YELLOW);
                    if (filters.Length > 2 && filters[2] == true)
                        this.Filters.Add(BusStatusColor.GREEN);
                    if (filters.Length > 3 && filters[3] == true)
                        this.Filters.Add(BusStatusColor.ORANGE);
                    if (filters.Length > 4 && filters[4] == true)
                        this.Filters.Add(BusStatusColor.LIGHT_GREY);
                    if (filters.Length > 5 && filters[5] == true)
                        this.Filters.Add(BusStatusColor.GREY);
                }



                WebLogger.Log("TreeDataByREST - start", new object[] { usr.UserId });
                Tree tree = new Tree(null, this.Filters, null, usr.UserId);

                BusTreeService.BusTreeServiceClient client = new BusTreeService.BusTreeServiceClient();

                tree = client.GetBusTree(tree);

                string clientInformation = "";

                //set color counters in ClientInformation

                Dictionary<string, int> ColorCounter = tree.Counters;

                foreach (string key in ColorCounter.Keys)
                {
                    int val = 0;
                    ColorCounter.TryGetValue(key, out val);
                    clientInformation += val.ToString() + ",";
                }

                clientInformation = clientInformation.TrimEnd(',');
                clientInformation += "|";


                //set filters in ClientInformation
                clientInformation += Filters.Contains(BusStatusColor.GREEN) + ",";
                clientInformation += Filters.Contains(BusStatusColor.GREY) + ",";
                clientInformation += Filters.Contains(BusStatusColor.LIGHT_GREY) + ",";
                clientInformation += Filters.Contains(BusStatusColor.ORANGE) + ",";
                clientInformation += Filters.Contains(BusStatusColor.RED) + ",";
                clientInformation += Filters.Contains(BusStatusColor.YELLOW);


                if (tree.Groups.Count > 0)
                {
                    tree.Groups[0].key = "0";
                    tree.Groups[0].ClientInformation = clientInformation; //JsonConvert.SerializeObject(ColorCounter.ToList());
                }

                AsyncManager.Parameters["data2"] = tree;
            }
        }

        public JsonResult GetTreeCompleted(IBI.Shared.Models.BusTree.Tree data2)
        {

            var jsonResult = Json(data2, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

      
        public void TreeDataByRESTAsync()
        {
            IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);

            if (usr != null && usr.UserId > 0)
            {
                WebLogger.Log("TreeDataByREST - start", new object[] { usr.UserId });
                Tree tree = ServiceManager.GetBusTree(usr.UserId, this.Filters);
                WebLogger.Log("TreeDataByREST - end");

                List<BusNode> groups = tree.Groups;

                string clientInformation = "";

                //set color counters in ClientInformation

                Dictionary<string, int> ColorCounter = tree.Counters;

                foreach (string key in ColorCounter.Keys)
                {
                    int val = 0;
                    ColorCounter.TryGetValue(key, out val);
                    clientInformation += val.ToString() + ",";
                }

                clientInformation = clientInformation.TrimEnd(',');
                clientInformation += "|";


                //set filters in ClientInformation
                clientInformation += Filters.Contains(BusStatusColor.GREEN) + ",";
                clientInformation += Filters.Contains(BusStatusColor.GREY) + ",";
                clientInformation += Filters.Contains(BusStatusColor.LIGHT_GREY) + ",";
                clientInformation += Filters.Contains(BusStatusColor.ORANGE) + ",";
                clientInformation += Filters.Contains(BusStatusColor.RED) + ",";
                clientInformation += Filters.Contains(BusStatusColor.YELLOW);


                if (groups.Count > 0)
                {
                    groups[0].key = "0";
                    groups[0].ClientInformation = clientInformation; //JsonConvert.SerializeObject(ColorCounter.ToList());
                }

                AsyncManager.Parameters["data"] = groups;

            }

        }

        public JsonResult TreeDataByRESTCompleted(List<IBI.Shared.Models.BusTree.BusNode> data)
        {
            //List<IBI.Shared.Models.BusTree.BusNode> groups = new List<Shared.Models.BusTree.BusNode>();
            var jsonResult = Json(data, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

         


        #endregion

         

        [HttpGet]
        public JsonResult GetBusGroupTree(string groupId = "0")
        {
            WebLogger.Log("GetBusGroupTree - start", new object[] { CurrentUser.UserId.ToString(), groupId });

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();

            WebLogger.Log("GetBusGroupTree - end");

            Tree t = srv.GetBusGroupTree(CurrentUser.UserId.ToString(), groupId);
            return Json(t.Groups, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public JsonResult GetBusGroupTreeForCustomer(int customerId, string groupId = "0")
        {
            WebLogger.Log("GetBusGroupTree - start", new object[] { CurrentUser.UserId.ToString(), groupId });

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();

            WebLogger.Log("GetBusGroupTree - end");

            Tree t = srv.GetBusGroupTree(CurrentUser.UserId.ToString(), groupId);
            t.Groups = t.Groups.Where(n => n.CustomerID == customerId).ToList();

            return Json(t.Groups, JsonRequestBehavior.AllowGet);

        }



        [HttpGet]
        public ActionResult ModifyBusAlias(string busNumber, int customerId, string busAlias)
        {
            //IBI.Shared.Models.BusTree.Bus bus = ServiceManager.GetBus(customerId, busNumber);
            ViewModels.BusTree.BusStatus.BusStatusModel b = new ViewModels.BusTree.BusStatus.BusStatusModel()
            {
                BusNumber = busNumber,
                CustomerId = customerId,
                BusAlias = busAlias

            };
            ViewBag.ShouldClose = false;
            return View("ModifyBusAlias", b);

        }

        [HttpPost]
        public JsonResult ModifyBusAlias(ViewModels.BusTree.BusStatus.BusStatusModel b)
        {
            if (string.IsNullOrEmpty(b.BusAlias))
            {
                b.LoadAfterSave = false;
                b.ErrorMessage = "Please Enter BusAlias";
            }
            else
            {
                b.LoadAfterSave = ServiceManager.ModifyBusAlias(b.CustomerId, b.BusNumber, b.BusAlias);
                b.ErrorMessage = b.LoadAfterSave ? "" : "This BusAlias is already selected";
            }


            return Json(b, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusStatus(string busNumber, int customerId)
        {
            IBI.Shared.Models.BusTree.Bus bus = ServiceManager.GetBus(customerId, busNumber);
            ViewModels.BusTree.BusStatus.BusStatusModel b = new ViewModels.BusTree.BusStatus.BusStatusModel()
            {
                BusNumber = bus.BusNumber,
                CustomerId = bus.CustomerId ?? Common.Settings.DefaultCustomerId,
                InOperation = bus.InOperation ?? false
            };
            ViewBag.ShouldClose = false;
            return View("BusStatus", b);

        }

        [HttpPost]
        //[Ajax(true)]
        public ActionResult ChangeBusStatus(ViewModels.BusTree.BusStatus.BusStatusModel b)
        {
            ServiceManager.ChangeBusStatus(b.CustomerId, b.BusNumber, b.InOperation);
            ViewBag.ShouldClose = true;
            b.LoadAfterSave = true;
            return View("BusStatus", b);
        }

        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusMap()
        {
            ViewBag.Username = Common.CurrentUser.Username;
            ViewBag.Fullname = Common.CurrentUser.Fullname;
            return View("BusMap");
        }
         

        #region Helper Functions

        #endregion

    }

     
}
