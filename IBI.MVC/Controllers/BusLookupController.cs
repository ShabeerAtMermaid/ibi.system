﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using IBI.Shared.Models.BusTree;
using IBI.Web.Infrastructure.CustomAttributes;


namespace IBI.Web.Controllers
{
     
    [AuthorizeLogin]
    public class BusLookupController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult BusLookup(  
            string busNumber, 
            string macAddress)
        {

            ViewModels.BusLookup.BusLookupViewModel model = new ViewModels.BusLookup.BusLookupViewModel();

            if (!(String.IsNullOrEmpty(busNumber))) 
            {
                model.BusNumber = busNumber;
                
                List<BusDetail> busDetail = ServiceManager.GetBusLookUpDetail(busNumber);

                if (busDetail == null)
                    return Json("No data found, Invalid Bus Number", JsonRequestBehavior.AllowGet);
                
                model.BusDetail = busDetail;
                
                return View("BusDetail", model.BusDetail);
            }

            if (!(String.IsNullOrEmpty(macAddress)))
            {
                model.MacAddress = macAddress;

                List<Client> client = ServiceManager.GetClient(macAddress);


                if (client == null)
                    return Json("No data found, Invalid MAC Address", JsonRequestBehavior.AllowGet);
                
                model.ClientDetail = client;

                return View("ClientDetail", model.ClientDetail);
            }

            return Json("No data found", JsonRequestBehavior.AllowGet);
            //return View(model);
        }



    }
}
