﻿$(document).ready(function () {

    //if there is any datepicker control on the current page
    var datePicker = $(".mydatepicker");
    if (datePicker != null && datePicker != undefined && datePicker.length!=0) {
        $(".mydatepicker").datepicker({
            showAnim: '',
            dateFormat: 'm/d/yy',
            showOn: 'both',
            buttonImageOnly: true,
            buttonImage: '../Scripts/custom-datetime/datepicker-btn.png',
            numberOfMonths: 1
        });
    }
    //------------------------------------------------------------------------------

    //if there is any time dropdown on the current page.
    var timeDropDown = $(".timedropdown");
    if (timeDropDown!=null && timeDropDown!=undefined && timeDropDown.length!=0) {
        $(".timedropdown").timedropdown();
    }
    //------------------------------------------------------------------------------

     
});

//default ajax form begin Submit action
function beginAjaxSubmit() {
    $('input[type=submit]').attr("disabled", true).addClass("disabled");
}

//default ajax form begin Submit action
function endAjaxSubmit() {
    $('input[type=submit]').removeAttr("disabled").removeClass("disabled");
}

//default html form submit action
function beginHtmlSubmit() {
    $('input[type=submit]').attr("disabled", true).addClass("disabled");
    document.forms[0].submit();
}