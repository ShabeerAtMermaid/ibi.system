﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;

namespace IBI.Web.ViewModels.Messaging
{
    public class MessageEditModel
    {                                 

        public Message MessageToEdit { get; set; }
                                                            
        public SelectList MessageCategories { get; set; }
        public SelectList CustomerList { get; set; }
        public SelectList MessageReplyType { get; set; }

        public int CustomerId { get; set; }

        public string SelectedCategoryId { get; set; }

        public string PostResult { get; set; }

        public string CancelAction { get; set; } //"back" OR "close"

        public bool LoadAfterSave { get; set; }
        //Constructor
        public MessageEditModel(int customerId, int messageId = -1) 
        {
            
            this.MessageToEdit = messageId <= 0 ? new Message() : ServiceManager.GetMessage(messageId);

            if(messageId==-1 || messageId==0)
            {
                this.CustomerId = this.MessageToEdit.CustomerId = customerId;
            }

            this.CustomerList = GetCustomers(MessageToEdit.CustomerId);

            FormStatus = this.CustomerList.Count() > 1 ? "1" : "0";

            if (this.CustomerList.Count() == 1)
            {
                MessageToEdit.CustomerId = this.CustomerId = int.Parse(this.CustomerList.FirstOrDefault().Value);
                FormStatus = "1";
            }

            if(this.CustomerList.Count()>1 && customerId<=0 )
            {
                MessageToEdit.CustomerId = this.CustomerId = -1;
                FormStatus = "0";
            }

            this.MessageCategories = GetMessageCategories(MessageToEdit.CustomerId, MessageToEdit.MessageCategoryId);

            this.MessageReplyType = GetMessageReplyType(MessageToEdit.MessageReplyTypeId);
           
            PostResult = "";
            
        }
        public MessageEditModel() { }

        public string BusNumbers { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MessageReplyTypeChanged { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string CategoryChanged { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CustomerChanged { get; set; }

        public string FormStatus { get; set; }

        public static MessageCategory GetCategory(int selectedCategoryId)
        {
            return ServiceManager.GetCategory(selectedCategoryId);
        }

        public static SelectList GetMessageCategories(int customerId, int? selectedCategoryId)
        {
            //[KHI]: temporary Ugly fix for PBI" 20554 //////
             customerId = customerId == 0 ? Common.Settings.DefaultCustomerId : customerId;
            /////////////////////////////////////////////////

            return new SelectList( ServiceManager.GetMessageCategories(customerId),
                            "MessageCategoryId",
                            "CategoryName",
                            selectedCategoryId ?? 0);
        }

        public static SelectList GetCustomers(int? selectedCustomerId)
        {
            List<IBI.Shared.Models.Customer> custs = Common.CurrentUser.CustomerList;
            
            return new SelectList(custs,
                            "CustomerId",
                            "CustomerName",
                            selectedCustomerId ?? 0);
        }

        public static SelectList GetMessageReplyType(int? selectedMessageReplyType)
        {
            //List<IBI.Shared.Models.Message> custs = Common.CurrentUser.CustomerList;

            return new SelectList(ServiceManager.GetMessagesReplyTypes(),
                            "MessageReplyTypeId",
                            "MessageReplyTypeName",
                            selectedMessageReplyType ?? 0);
        }
    }

    public class MessageListModel
    {
        
        public List<Message> Messages{get; set;}
        public int CustomerID   {get; set;}
        public string BusNumber {get; set;}
        public bool LoadAfterSave { get; set; }

        public MessageListModel(){}

        public MessageListModel(int customerId, string busNumber, bool loadAfterSave = false) 
        {
            Messages = ServiceManager.GetMessages(busNumber, customerId);
            this.CustomerID = customerId;
            this.BusNumber = busNumber;
            this.LoadAfterSave = loadAfterSave;
        } 
    }   
}