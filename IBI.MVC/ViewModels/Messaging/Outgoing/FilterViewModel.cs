﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Controls;
using System.Runtime.Serialization;

namespace IBI.Web.ViewModels.Messaging.Outgoing
{
    public class FilterViewModel
    {  
            private int selectedGroupID = -1;
            //private int selectedMessageCategoryID = -1;
            
            public SelectList Groups { get; set; }
            //public SelectList MessageCategories { get; set; }
            public string BusNumber { get; set; }
            public DateTime Date { get; set; }
            public string Title { get; set; } 
            public string MessageText { get; set; } 
            public string MessageCategoryName{get;set;}
            public DateTime ValidFrom { get; set; }
            public DateTime ValidTo { get; set; }
            public string SenderName { get; set; }
            public bool ShowExpiredOnly { get; set; }
            public string SelectedMsgs { get; set; }
            public int GroupId
            {
                get
                {
                    return selectedGroupID;
                }
                set
                {
                    selectedGroupID = value;
                }
            }
            //public int MessageCategoryId
            //{
            //    get
            //    {
            //        return selectedMessageCategoryID;
            //    }
            //    set
            //    {
            //        selectedMessageCategoryID = value;
            //    }
            //}
                            
            public void Fill()
            {
                Groups = GetGroups(GroupId);
                //MessageCategories = GetMessageCategories();
            }

            private static SelectList GetGroups(int selectedGroupId)
            {
                return new SelectList(Common.CurrentUser.Groups(),
                                "GroupId",
                                "Description",
                                selectedGroupId);
            }

            //private static SelectList GetMessageCategories(int selectedMessageCategoryId)
            //{
            //    return new SelectList(Common.CurrentUser.Groups(),
            //                    "MessageCategoryId",
            //                    "CaetgoryName",
            //                    selectedMessageCategoryId);
            //}
        }

             
}