﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    [DataContract]
    public class SignAddonImageGeneratorPager
    {
        [DataMember]
        public List<string> Img { get; set; }

        [DataMember]
        public int Index { get; set; }

        [DataMember]
        public int Count { get; set; }
    }
}