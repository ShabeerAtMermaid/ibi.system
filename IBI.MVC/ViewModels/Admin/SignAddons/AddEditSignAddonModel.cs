﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Shared.Models.BusTree;
using IBI.Shared.Models.Messages;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Logger;
using IBI.Shared.Models.Signs;
using IBI.Shared.Models.SignAddons;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    public class AddEditSignAddonModel
    {

        public Sign Groups
        {
            get;
            set;
        }

        public int CustomerId { get; set; }

        //public string Name { get; set; }
        public string ParentGroup { get; set; }
        //public string Line {get; set;}

        //public string Text {get; set;}
        //public string Schedule {get; set;}

        //public string SelectionStructure{get; set;}
        //public bool Excluded{get; set;}

        public string PostResult { get; set; }
        public string CancelAction { get; set; } //"back" OR "close"
        public bool LoadAfterSave { get; set; }
        public string ErrorMessage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CustomerChanged { get; set; }
         
        public SignAddon SignAddon { get; set; }

        //Constructor
        public AddEditSignAddonModel(int customerId, int signAddonItemId)
        {
            this.CustomerId = customerId;

            this.PostResult = ""; 
            this.SignAddon = ServiceManager.GetSignAddonItem(signAddonItemId);

            if (this.SignAddon == null)
            {
                this.SignAddon = new SignAddon
                {
                    CustomerId = customerId,
                    IsActive = true
                };
            }
        }

        public AddEditSignAddonModel() { }



    }
}

