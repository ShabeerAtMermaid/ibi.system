﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.ViewModels.Admin.Configurations
{
    public class ConfigurationModel
    {
        [HiddenInput(DisplayValue = false)]
        public string ConfgurationChanged1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ConfgurationChanged2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ConfgurationChanged3 { get; set; }

        public int ConfgurationPropertyId1 { get; set; }
        public int ConfgurationPropertyId2 { get; set; }
        public int ConfgurationPropertyId3 { get; set; }

        public SelectList ConfigurationPropertiesList { get; set; }

        public string ConfigSelectionStatus { get; set; }

        //Constructor
        public ConfigurationModel() 
        {
            this.ConfigurationPropertiesList = GetConfigurationProperties();
            ConfigSelectionStatus = this.ConfigurationPropertiesList.Count() > 1 ? "1" : "0";
        }

        public static SelectList GetConfigurationProperties()
        {
            List<IBI.Shared.Models.Configuration> confgs = Common.CurrentUser.ConfigurationList;
           
            return new SelectList(confgs,
                            "ConfgurationPropertyId",
                            "PropertyName"
                            );
        }
        
    }
}