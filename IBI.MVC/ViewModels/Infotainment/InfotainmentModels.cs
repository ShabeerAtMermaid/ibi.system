﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IBI.Web.ViewModels.Infotainment
{

    public class VTCScreenDumb
    {
        [DataMember]
        public string C1 { get; set; }
        [DataMember]
        public string C2 { get; set; }
        [DataMember]
        public string C3 { get; set; }
        [DataMember]
        public string C4 { get; set; }
        [DataMember]
        public string C5 { get; set; }
        [DataMember]
        public string C6 { get; set; }
    }
}