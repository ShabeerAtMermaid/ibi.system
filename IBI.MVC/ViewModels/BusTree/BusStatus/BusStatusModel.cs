﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IBI.Web.ViewModels.BusTree.BusStatus
{
    public class BusStatusModel
    {
        public string BusNumber { get; set; }
        public bool InOperation { get; set; }
        public int CustomerId { get; set; }
        public bool LoadAfterSave { get; set; }

        [Required(ErrorMessage = "BusAlias is required")]
        [StringLength(6, ErrorMessage = "BusAlias can be no larger than 40 characters")]
        public string BusAlias { get; set; }
        public string ErrorMessage { get; set; }
    }
}