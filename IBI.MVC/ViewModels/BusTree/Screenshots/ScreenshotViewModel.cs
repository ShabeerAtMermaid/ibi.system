﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace IBI.Web.ViewModels.BusTree.Screenshots
{
    public class Screenshot
    {
        [DataMember]
        public string BusNumber { get; set; }
        [DataMember]
        public string ClientType { get; set; }
        [DataMember]
        public string ImageIndex { get; set; }
        [DataMember]
        public string ImagePath { get; set; }
        [DataMember]
        public string ImageDate { get; set; }

        
    }
}