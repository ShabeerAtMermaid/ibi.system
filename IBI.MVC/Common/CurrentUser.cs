﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.Keys;
using IBI.Shared.Models.Accounts;
using IBI.Shared.Common.Types;

namespace IBI.Web.Common
{
    public class CurrentUser
    {
        public static int UserId
        {
            get
            {
                IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);
                return usr!=null ? usr.UserId: 0;
            }            
        }
        public static string Username
        {
            get
            {                                     
                IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);
                return usr != null ? usr.Username : "";
            }
        }
        public static string Fullname
        {
            get
            {                                        
                IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);
                return usr != null ? usr.FullName : "";         
            }
        }

        public static bool IsAdmin {

            get {
                IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);
                return usr != null ? usr.IsAdmin : false;         
            }
        }

        public static List<IBI.Shared.Models.BusTree.Group> Groups()
        {
            List<IBI.Shared.Models.BusTree.Group> groupList = SessionManager.Get<List<IBI.Shared.Models.BusTree.Group>>(SessionKeys.CurUser_GroupList);

            if (groupList == null)            
            {
                groupList = ServiceManager.GetUserAccessGroups(CurrentUser.UserId.ToString());
                SessionManager.Store(SessionKeys.CurUser_GroupList, groupList); 
            }
            
            return groupList;
        }

        public static string ShowHidPageLink(string pagePath)
        {
            List<UserPage> usrPages = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserPages);

            if (usrPages != null)
            {
                UserPage uPage = usrPages.Where(p => p.Path == pagePath).FirstOrDefault();
                if (uPage != null && uPage.Read)
                {
                    return "show";
                }
                
            }
            return "hide";        
        }

        public static bool IsValidUserPageUrl(string pagePath)
        {
            List<UserPage> usrPages = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserPages);

            if (usrPages != null)
            {
                UserPage uPage = usrPages.Where(p => p.Path == pagePath).LastOrDefault();
                if (uPage != null && uPage.Read)
                {
                    //ServiceManager.SetUserActivityConfiguration(CurrentUser.UserId, "LastPage", pagePath.TrimStart('/'));
                    CurrentUser.SetUserActivityConfiguration("LastPage", pagePath.TrimStart('/'));
                    return true;
                }
            }

            return false;        
        }

        public static List<UserPage> UserPages
        {
            get
            {
                List<UserPage> usrPages = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserPages);

                if(usrPages==null)
                {
                    usrPages = Infrastructure.ServiceManager.GetUserPages(CurrentUser.UserId);   
                }

                return usrPages;
            }
            set
            {
                SessionManager.Store(SessionKeys.CurUser_UserPages, value);
            }
        }


        /// <summary>
        /// This function returns default Action for current logged in user.
        /// </summary>
        /// <returns>A String[] array with 2 elements. 1: ActionName, 2: ControllerName </returns>
        public static string[] GetDefaultAction() 
        {
            string[] retString = new string[2];
            List<UserPage> usrPages = SessionManager.Get<List<UserPage>>(SessionKeys.CurUser_UserPages);
            foreach (UserPage uPage in usrPages.Where(p=>p.ParentPageId>0))
            {
                if (uPage.Read)
                {
                    retString[0] = uPage.Action;
                    retString[1] = uPage.Controller;
                    return retString;
                }
            }

            //if code reach here . it means no active sub page page assigned to any active role of current user
            //in that case we need to open the first active module.
            foreach (UserPage uPage in usrPages.Where(p => p.ParentPageId == 0))
            {
                if (uPage.Read)
                {
                    retString[0] = uPage.Action;
                    retString[1] = uPage.Controller;
                    return retString;
                }
            }

            retString[0] = "Index";
            retString[1] = "Error";
            return retString;

        }


        public static Queue<String> VisitedPages
        {
            get 
            {
                Queue<String> uVisitedPages = SessionManager.Get<Queue<String>>(SessionKeys.CurUser_VisitedPages);

                if (uVisitedPages == null)
                {
                    uVisitedPages = new Queue<String>();

                    //string lastUrl = HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
                    //string curUrl = HttpContext.Current.Request.RawUrl;
                    //string curUrl = HttpContext.Current.Request.ServerVariables["URL"];

                    //if(!(String.IsNullOrEmpty(lastUrl)))
                    //{
                    //    uVisitedPages.Enqueue(lastUrl);
                    //}

                    //if(!(String.IsNullOrEmpty(curUrl)))
                    //{
                    //    uVisitedPages.Enqueue(curUrl);
                    //}
                    
                    SessionManager.Store(SessionKeys.CurUser_VisitedPages, uVisitedPages);
                    return uVisitedPages;
                }

                return uVisitedPages;
            }
            set
            {
                SessionManager.Store(SessionKeys.CurUser_VisitedPages, value);
            }
        }

        public static void PushAVisistedPage(string url)
        {
            if(VisitedPages.LastOrDefault() != url)
            {
                VisitedPages.Enqueue(url);
            }
        }

        public static string CurrentUrl() 
        {
            string url  = VisitedPages.LastOrDefault();
            return url;
        }

        public static string LastUrl()
        {
            int lastUrlIndex = VisitedPages.Count() > 1 ? VisitedPages.Count() - 1 : 0;
            if (VisitedPages.Count > 0)
            {
                string url = VisitedPages.ElementAt(lastUrlIndex);

                if (url == HttpContext.Current.Request.ServerVariables["URL"] && lastUrlIndex > 0)
                {
                    url = VisitedPages.ElementAt(lastUrlIndex - 1);
                }
                return url;
            }
            else
            {
                return "";
            }
           
                             
        }


        public static List<String> BusListHavingVTC
        {
            get
            {
                List<String> busList = busList = ServiceManager.GetUserAccessBusesHavingVTC(UserId);
                return busList;

            }
        }

        public static List<String> BusListHavingDCU
        {
            get
            {
                List<String> busList = busList = ServiceManager.GetUserAccessBusesHavingClientType(UserId,"DCU");
                return busList;

            }
        }
        public static List<String> BusListHavingCCU
        {
            get
            {
                List<String> busList = busList = ServiceManager.GetUserAccessBusesHavingClientType(UserId,"CCU");
                return busList;

            }
        }

        public static List<String> BusListHavingLFF
        {
            get
            {
                List<String> busList = busList = ServiceManager.GetUserAccessBusesHavingClientType(UserId, "LFF");
                return busList;

            }
        }

        public static List<String> BusListHavingLFR
        {
            get
            {
                List<String> busList = busList = ServiceManager.GetUserAccessBusesHavingClientType(UserId, "LFR");
                return busList;

            }
        }


        public static List<String> BusList
        {
            get 
            {
                List<String> busList = new List<string>();
                busList = SessionManager.Get<List<String>>(SessionKeys.CurUser_BusList);

                if (busList == null)
                {
                    busList = ServiceManager.GetUserAccessBuses(UserId);
                    SessionManager.Store(SessionKeys.CurUser_BusList, busList);
                }

                return busList;

            }
        }


        public static List<IBI.Shared.Models.Customer> CustomerList
        {
            get
            {
                List<IBI.Shared.Models.Customer> custList = new List<IBI.Shared.Models.Customer>();
                custList = SessionManager.Get<List<IBI.Shared.Models.Customer>>(SessionKeys.CurUser_CustomerList);

                if (custList == null)
                {
                    custList = ServiceManager.GetUserCustomers(UserId);
                    SessionManager.Store(SessionKeys.CurUser_CustomerList, custList);
                }

                return custList;

            }
        }

        public static List<IBI.Shared.Models.Configuration> ConfigurationList
        {
            get
            {
                List<IBI.Shared.Models.Configuration> configPropsList = new List<IBI.Shared.Models.Configuration>();
                configPropsList = SessionManager.Get<List<IBI.Shared.Models.Configuration>>(SessionKeys.CurUser_ConfigPropsList);

                if (configPropsList == null)
                {
                    configPropsList = ServiceManager.GetUserConfigurations(UserId);
                    SessionManager.Store(SessionKeys.CurUser_ConfigPropsList, configPropsList);
                }

                return configPropsList;

            }
        }

        #region User Activity

        public static bool GetUserSpecificActivityValue(string property)
        {
            try
            {
                var propertyValue = CurrentUser.UserActivityConfigurations.Where(uac => uac.Key == property).FirstOrDefault();
                if (!string.IsNullOrEmpty(propertyValue.Value))
                {
                    return WebUtil.IsTrue(propertyValue.Value);
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int GetUserSpecificIntActivityValue(string property)
        {
            try
            {
                var propertyValue = CurrentUser.UserActivityConfigurations.Where(uac => uac.Key == property).FirstOrDefault();
                if (!string.IsNullOrEmpty(propertyValue.Value))
                {
                    return int.Parse(propertyValue.Value);
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //public static T GetUserSpecificActivityValue<T>(string property)
        //{
        //    var propertyValue = CurrentUser.UserActivityConfigurations.Where(uac => uac.Key == property).FirstOrDefault();
        //    object parsedValue = default(T);
        //    try
        //    {
        //        parsedValue = Convert.ChangeType(propertyValue.Value, typeof(T));
        //    }
        //    catch (InvalidCastException)
        //    {
        //        parsedValue = null;
        //    }
        //    catch (ArgumentException)
        //    {
        //        parsedValue = null;
        //    }
        //    return (T)parsedValue;
        //}

        public static List<IBIKeyValuePair> UserActivityConfigurations
        {
            get
            {
                List<IBIKeyValuePair> userActivityConfig = SessionManager.Get<List<IBIKeyValuePair>>(SessionKeys.CurUser_ActivityConfig);
                if (userActivityConfig == null)
                {
                    userActivityConfig = Infrastructure.ServiceManager.GetUserActivityConfigurations(CurrentUser.UserId);
                    SessionManager.Store(SessionKeys.CurUser_ActivityConfig, userActivityConfig);
                }
                return userActivityConfig;
            }
            set
            {
                SessionManager.Store(SessionKeys.CurUser_ActivityConfig, value);
            }
        }

        public static void SetUserActivityConfiguration(string property, string value)
        {
            //read from session
            List<IBIKeyValuePair> UserActivityConfig = CurrentUser.UserActivityConfigurations;

            IBIKeyValuePair kvp = new IBIKeyValuePair(property, value);

            var existingKvp = UserActivityConfig.Where(uac => uac.Key == property).FirstOrDefault();
            existingKvp.Value = value;

            ServiceManager.SetUserActivityConfiguration(CurrentUser.UserId, property, value);


            //save back to session after making changes to property value
            UserActivityConfigurations = UserActivityConfig;
        }

        #endregion
    }
}