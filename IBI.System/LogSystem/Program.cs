﻿using Elasticsearch.Net;
using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogSystem
{

    public class Log
    {
        public DateTime Timestamp { get; set; }
        public int Id { get; set; }
        public string BusNumber { get; set; }
        public string ClientType { get; set; }
        public string Source { get; set; }
        public string Line { get; set; }
        public string From { get; set; }
        public string Destination { get; set; }
        public string JourneyNumber { get; set; }
        public string StopNumber { get; set; }
        public string StopName { get; set; }
        public string Zone { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EventId { get; set; }
        public string EventData { get; set; }
        public string Result { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {

            var node = new Uri("http://localhost:9200");
            var settings = new ConnectionSettings(node, defaultIndex: "ibi-buseventlogs");


            var client = new Nest.ElasticClient(settings);


            for (int i = 1; i <= 10000000; i++)
            {
                string[] gps = GetRandomWord("gps").Trim().Split(new string[]{"\t"}, StringSplitOptions.RemoveEmptyEntries);
                string[] stop = GetRandomWord("stop").Trim().Split(new string[] { "\t" }, StringSplitOptions.RemoveEmptyEntries);
                var log = new Log
                {
                    Timestamp = DateTime.Now.AddDays(-1),
                    Id = i,
                    BusNumber = GetRandomWord("bus"),
                    ClientType = i % 2 == 0 ? "DCU" : "VTC",
                    Destination = GetRandomWord("destination"),
                    EventData = GetRandomWord("event"),
                    EventId = (i % 2).ToString(),
                    From = GetRandomWord("from"),
                    JourneyNumber = (i + 2000).ToString(),
                    Latitude = gps[0],
                    Longitude = gps[1],
                    Line = GetRandomWord("line"),
                    Result = "true",
                    Source = "bus event logs",
                    StopNumber = stop[0],
                    StopName = stop[1],
                    Zone = "1"
                };


                //client.Index("log", "log", log);
                client.Index<Log>(log);

                System.Threading.Thread.Sleep(5);

             
            }

            //SearchDescriptor<Log> searchDescriptor = new SearchDescriptor<Log>().From(0).Size(10).Query(q => q.Term(l => l.Id, "51"));
            //ISearchResponse<Log> searchResults = client.Search<Log>(searchDescriptor);

            //string dslQuery = Encoding.UTF8.GetString(client.Serializer.Serialize(searchDescriptor));

            //QueryContainer query = new TermQuery
            //{
            //    Field = "Id",
            //    Value = "51"
            //};

            //var searchRequest = new SearchRequest<Log>
            //{
            //    From = 0,
            //    Size = 10,
            //    Query = query
            //};

            //ISearchResponse<Log> searchResultsOIS = client.Search<Log>(searchRequest);
            //string dslQueryOIS = Encoding.UTF8.GetString(client.Serializer.Serialize(searchRequest));


            //var clientElasticSearchNET = new ElasticsearchClient();

            //var elasticSearchNetQuery = new { query = new { term = new { Id = new { value = "51" } } } };
            //string dslQueryNext = Encoding.UTF8.GetString(clientElasticSearchNET.Serializer.Serialize(elasticSearchNetQuery));
            //ElasticsearchResponse<string> result = clientElasticSearchNET.Search<string>("logstash-logs", "log", elasticSearchNetQuery);


        }

        private static string GetRandomWord(string type) {
            string path = "D:\\Mermaid\\Mermaid SW\\Development\\IBI\\Source\\IBI.System\\LogSystem";
            switch (type) { 
                case "line":{
                    path = Path.Combine(path, "lines.txt");
                    break;
                }
                case "destination":{
                    path = Path.Combine(path, "destinations.txt");
                    break;
                }
                case "from":{
                    path = Path.Combine(path, "from.txt");
                    break;
                }
                case "stop":{
                    path = Path.Combine(path, "stops.txt");
                    break;
                }
                case "bus":{
                    path = Path.Combine(path, "buses.txt");
                    break;
                }
               case "event":{
                    path = Path.Combine(path, "events.txt");
                    break;
                }
               case "gps":{
                    path = Path.Combine(path, "gps.txt");
                    break;
                }
            }

            string[] allLines = File.ReadAllLines(path);
            Random rnd1 = new Random();
            return allLines[rnd1.Next(allLines.Length-1)];
        }
    }
}
