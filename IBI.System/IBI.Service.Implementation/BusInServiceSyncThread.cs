using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using IBI.Shared.Models;
using IBI.DataAccess.DBModels;
using IBI.Implementation.Schedular;
using IBI.Shared.Interfaces;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.BusTree;
using IBI.Shared;

namespace IBI.Service.Implementation
{
    class BusInServiceSyncThread
    {
        #region Variables
        
        string busNumber;
        int customerId;
        Guid guid;
        int tnumber;
        private readonly ISyncScheduleFactory schedularFactory = new SyncScheduleFactory();
        
        #endregion
        
        #region Properties

        public int ThreadNo
        {
            get
            {
                return tnumber;
            }
            set
            {
                tnumber = value;
            }
        }
        public Guid MyGUID
        {
            get
            {
                return guid;
            }
            set
            {
                guid = value;
            }
        }

        public string BusNumber
        {
            get
            {
                return busNumber;
            }
            set
            {
                busNumber = value;
            }
        }
        
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        public ISyncScheduleFactory SchedularFactory
        {
            get
            {
                return schedularFactory;
            }
        }

        #endregion
       
        #region Implementation
        
        public BusInServiceSyncThread(string busNumber, int customerId)
        {
            this.busNumber = busNumber;
            this.customerId = customerId;
        }
        
        public static void ProcessBusInService(Object busObject)
        {
            BusInServiceSyncThread thread = (BusInServiceSyncThread)busObject;
            try
            {
                //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("Thread started for bus {0}, GUID {1}, thread number {2}", thread.BusNumber, thread.MyGUID, thread.ThreadNo));
                            
                ISyncInService inService = thread.schedularFactory.GetSyncInService(thread.CustomerId);
                if (inService != null)
                {
                    //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("Syncing InService for bus {0}", thread.BusNumber)); 
                    inService.SyncInServiceStatus(thread.BusNumber);
                    //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("Syncing Completed for bus {0}", thread.BusNumber)); 
                }

                System.Threading.Thread.Sleep(1000); // until next 1 seconds this bus cannot be added to thread pool again

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.InServiceStatus, ex);
            }
            finally
            {
                ThreadPool.QueueUserWorkItem(BusInServiceSyncThread.ProcessBusInService, thread);
                //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("Queued thread for bus {0}", thread.BusNumber)); 
            }

            System.Threading.Thread.Sleep(50);
        }
         
        
        #endregion
    }
}