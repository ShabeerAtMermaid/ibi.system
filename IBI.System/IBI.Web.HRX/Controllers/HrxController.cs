﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace IBI.Web.VDV.Controllers
{
    public class HrxController : Controller
    {
        public HttpVerbs RequestHttpVerb
        {
            get { return (HttpVerbs)Enum.Parse(typeof(HttpVerbs), this.Request.HttpMethod, true); }
        }



        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SenderResponse(string data)
        {

            Logger.Log(string.Format("Post Data received from Sender \"{0}\".", Request.UserHostAddress));
            string mimeType = "text/xml";
            //return this.Content(response, mimeType, Encoding.Default); 

            return this.Content("<Response>Data Received</Response>", mimeType, Encoding.Default);

        }

        //[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Estimation()
        {
            StringBuilder log = new StringBuilder();        

            var postData = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

                        //data = string.IsNullOrEmpty(postData) ? "{empty}" : postData;
            log.AppendLine(string.Format("Post Data received from Sender \"{0}\"\n\n DATA: {1}\n\n", Request.UserHostAddress, string.IsNullOrEmpty(postData) ? "{EMPTY}" : (postData.Length / 1024).ToString() + " KB"));
            string mimeType = "text/xml";
            //return this.Content(response, mimeType, Encoding.Default); 

            SaveEstimations(postData, ref log);

            Logger.Log(log.ToString());

            log = null;
            
            string hrxResponse = string.Format( "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:hrx=\"urn:hrx\">  <SOAP-ENV:Header/>   <SOAP-ENV:Body>      <hrx:RealtimeResponse version=\"2.3.2\" timestamp=\"{0}\">         <hrx:ErrorNumber>0</hrx:ErrorNumber>         <hrx:Errortext>no error</hrx:Errortext>      </hrx:RealtimeResponse>   </SOAP-ENV:Body></SOAP-ENV:Envelope>", DateTime.Now.ToString("yyyy-MM-ddThh:mm:sszzz"));


            return this.Content(hrxResponse, "text/xml");
             
            //Response.Write(this.Content(string.Format("<Response>{0}</Response><Data>{1}</Data>", "Data Received", postData), mimeType, Encoding.Default).Content.ToString());
            //return null;

        }


        private void SaveEstimations(string tripData, ref StringBuilder log)
        {
            if (string.IsNullOrEmpty(tripData))
                return;

            try
            {

                if (bool.Parse(ConfigurationSettings.AppSettings["SaveEstimationDataInDB"]))
                {
                    if (bool.Parse(ConfigurationSettings.AppSettings["QueueSystem"]))
                    {
                        IBI.Journey.PredictionImporter.Importer.AddEstimateToQueue(tripData);
                    }
                    else
                    {
                        IBI.Journey.PredictionImporter.Importer.ImportPredictions(tripData);
                    }
                }

                if (bool.Parse(ConfigurationSettings.AppSettings["SaveEstimationDataInFiles"]))
                {
                    string fileText = tripData; //File.ReadAllText(filePath, System.Text.Encoding.UTF8);
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream memStream = new MemoryStream(encoding.GetBytes(fileText));

                    XDocument xDoc = XDocument.Load(memStream);


                    XNamespace ns = "urn:hrx";
                    //foreach (XElement element in xdoc.Descendants(ns + "nationalList")


                    string line = xDoc.Descendants(ns + "TripName").FirstOrDefault().Value;

                    foreach (var st in xDoc.Descendants(ns + "UniqueID"))
                    {
                        string fileName = Path.Combine(Server.MapPath("~/Logs/") + line + "_" + st.Value.ToString() + "_" + DateTime.Now.Ticks + ".XML");

                        //System.IO.File.Create(fileName);
                        System.IO.File.WriteAllText(fileName, tripData);
                    }
                }

                log.AppendLine(string.Format("Posted Data successfully saved"));

            }
            catch (Exception ex)
            {
                log.AppendLine(string.Format("ERROR !!! while saving Posted Data: {0} ", IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
            }
        }

        #region "Private Helper"

        private string ProcessRequest(string sender)
        {

            Logger.Log(string.Format("Call received for Sender \"{0}\"", sender));

            try
            {
                string timeZone = ConfigurationManager.AppSettings["TimeZone"].ToString();

                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><Response>Data Received<//Response>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + timeZone));

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return string.Empty;
        }

        #endregion

    }

    
}
