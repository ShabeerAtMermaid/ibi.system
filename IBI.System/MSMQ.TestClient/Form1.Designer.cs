﻿namespace MSMQ.TestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtQueueName = new System.Windows.Forms.TextBox();
            this.txtQueueData = new System.Windows.Forms.TextBox();
            this.btnQueue = new System.Windows.Forms.Button();
            this.lstQueue = new System.Windows.Forms.ListBox();
            this.btnEnqueue = new System.Windows.Forms.Button();
            this.txtEnqueue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtQueueName
            // 
            this.txtQueueName.Location = new System.Drawing.Point(106, 41);
            this.txtQueueName.Name = "txtQueueName";
            this.txtQueueName.Size = new System.Drawing.Size(218, 20);
            this.txtQueueName.TabIndex = 0;
            this.txtQueueName.Text = "TestQueue";
            // 
            // txtQueueData
            // 
            this.txtQueueData.Location = new System.Drawing.Point(106, 91);
            this.txtQueueData.Name = "txtQueueData";
            this.txtQueueData.Size = new System.Drawing.Size(218, 20);
            this.txtQueueData.TabIndex = 1;
            // 
            // btnQueue
            // 
            this.btnQueue.Location = new System.Drawing.Point(350, 91);
            this.btnQueue.Name = "btnQueue";
            this.btnQueue.Size = new System.Drawing.Size(120, 23);
            this.btnQueue.TabIndex = 2;
            this.btnQueue.Text = "Send to Queue";
            this.btnQueue.UseVisualStyleBackColor = true;
            this.btnQueue.Click += new System.EventHandler(this.btnQueue_Click);
            // 
            // lstQueue
            // 
            this.lstQueue.FormattingEnabled = true;
            this.lstQueue.Location = new System.Drawing.Point(106, 232);
            this.lstQueue.Name = "lstQueue";
            this.lstQueue.Size = new System.Drawing.Size(218, 238);
            this.lstQueue.TabIndex = 3;
            // 
            // btnEnqueue
            // 
            this.btnEnqueue.Location = new System.Drawing.Point(350, 232);
            this.btnEnqueue.Name = "btnEnqueue";
            this.btnEnqueue.Size = new System.Drawing.Size(120, 23);
            this.btnEnqueue.TabIndex = 4;
            this.btnEnqueue.Text = "Enqueue";
            this.btnEnqueue.UseVisualStyleBackColor = true;
            this.btnEnqueue.Click += new System.EventHandler(this.btnEnqueue_Click);
            // 
            // txtEnqueue
            // 
            this.txtEnqueue.Location = new System.Drawing.Point(350, 270);
            this.txtEnqueue.Multiline = true;
            this.txtEnqueue.Name = "txtEnqueue";
            this.txtEnqueue.Size = new System.Drawing.Size(217, 26);
            this.txtEnqueue.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "QueueName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Data";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(707, 500);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEnqueue);
            this.Controls.Add(this.btnEnqueue);
            this.Controls.Add(this.lstQueue);
            this.Controls.Add(this.btnQueue);
            this.Controls.Add(this.txtQueueData);
            this.Controls.Add(this.txtQueueName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtQueueName;
        private System.Windows.Forms.TextBox txtQueueData;
        private System.Windows.Forms.Button btnQueue;
        private System.Windows.Forms.ListBox lstQueue;
        private System.Windows.Forms.Button btnEnqueue;
        private System.Windows.Forms.TextBox txtEnqueue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

