﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace IBI.Shared.ConfigSections
{

    //public class ScheduleRewriterDefinition
    //{
    //    public List<VdvSchedule> Schedules { get; set; }
    //    public ScheduleRewriterDefinition() {

    //    }

    //    public ScheduleRewriterDefinition(string rewriterDefinitionXML)
    //    { 
    //        this.Schedules = new List<VdvSchedule>();

    //        if (String.IsNullOrEmpty(rewriterDefinitionXML))
    //            return;

    //        XDocument xDoc = XDocument.Parse(rewriterDefinitionXML);

    //            var schedules = (from e in xDoc.Descendants("Schedule")
    //                             select e);


    //            foreach (var schedule in schedules)
    //            {
    //                string l, d, rd, rv;
    //                string[] c = null;
    //                string[] m = null;


    //                l = schedule.Attribute("line").Value;
    //                d = schedule.Attribute("destination").Value;


    //                var rdNode = schedule.Descendants("RewriteDestination").FirstOrDefault();
    //                var rvNode = schedule.Descendants("RewriteVia").FirstOrDefault();

    //                rd = rdNode != null ? rdNode.Value : "";
    //                rv = rvNode != null ? rvNode.Value : "";

    //                var cNode = schedule.Descendants("ContainsStops").FirstOrDefault();
    //                if (cNode != null)
    //                {
    //                    c = Array.ConvertAll(cNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
    //                }

    //                var mNode = schedule.Descendants("MissingStops").FirstOrDefault();
    //                if (mNode != null)
    //                {
    //                    m = Array.ConvertAll(mNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
    //                }

    //                this.Schedules.Add(new VdvSchedule(l, d, rd, rv, c, m));
    //            }

    //            xDoc = null;

    //    }

    //    public ScheduleRewriterDefinition(List<VdvSchedule> schedules)
    //    {
    //        this.Schedules = schedules;
    //    }

    //    public static List<VdvSchedule> ReadAllConfigurations()
    //    {
    //        ScheduleRewriterConfig config = (IBI.Shared.ConfigSections.ScheduleRewriterConfig)ConfigurationManager.GetSection("ScheduleRewriteDefinitions");

    //        List<VdvSchedule> data = new List<VdvSchedule>();

    //        //here should be a null check to avoid exception in case of missing config
    //        if (config.Xdoc == null)
    //            return data;

    //        //XDocument xDoc = XDocument.Parse(rewriterDefinitionXML);

    //        var schedules = (from e in config.Xdoc.Descendants("Schedule")
    //                         select e);


    //        foreach (var schedule in schedules)
    //        {
    //            string l, d, rd, rv;
    //            string[] c = null;
    //            string[] m = null;


    //            l = schedule.Attribute("line").Value;
    //            d = schedule.Attribute("destination").Value;


    //            var rdNode = schedule.Descendants("RewriteDestination").FirstOrDefault();
    //            var rvNode = schedule.Descendants("RewriteVia").FirstOrDefault();

    //            rd = rdNode != null ? rdNode.Value : "";
    //            rv = rvNode != null ? rvNode.Value : "";

    //            var cNode = schedule.Descendants("ContainsStops").FirstOrDefault();
    //            if (cNode != null)
    //            {
    //                c = Array.ConvertAll(cNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
    //            }

    //            var mNode = schedule.Descendants("MissingStops").FirstOrDefault();
    //            if (mNode != null)
    //            {
    //                m = Array.ConvertAll(mNode.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries), p => p.Trim());
    //            }

    //            data.Add(new VdvSchedule(l, d, rd, rv, c, m));
    //        }

    //        //config.Xdoc = null;
    //        return data;

    //    }
    //}

    //public class VdvSchedule
    //{
    //    public string Line { get; set; }
    //    public string Destination { get; set; }
    //    public string RewriteDestination { get; set; }
    //    public string RewriteVia { get; set; }

    //    public string[] ContainsStops { get; set; }
    //    public string[] MissingStops { get; set; }

    //    public VdvSchedule() { }

    //    public VdvSchedule(string line, string destination, string rewriteDestination, string rewriteVia, string[] containsStops, string[] missingStops)
    //    {
    //        this.Line = line;
    //        this.Destination = destination;
    //        this.RewriteDestination = rewriteDestination;
    //        this.RewriteVia = rewriteVia;
    //        this.ContainsStops = containsStops;
    //        this.MissingStops = missingStops;
    //    }
    //}

    //public class VdvCustomerConfiguration{
    //   public int CustomerId {get; set;}
    //    public bool Active {get; set;}
    //    public bool SaveDataInDB { get; set; }
    //    public string[] LineFilter { get; set; }        
    //    public ScheduleRewriterDefinition ScheduleRewriteDefinitions { get; set; }

    //}


    public class HogiaService
    {
        public string Name { get; set; }
        public Uri DefaultEndPoint { get; set; }
        public bool Active { get; set; }

        public HogiaCustomer[] Customers { get; set; }

        public HogiaService(XElement xdoc)
        {

            if (xdoc.Attribute("name") != null)
            {
                this.Name = xdoc.Attribute("name").Value;
            }

            if (xdoc.Attribute("defaultEndPoint") != null)
            {
                this.DefaultEndPoint = new Uri(xdoc.Attribute("defaultEndPoint").Value);
            }

            if (xdoc.Attribute("active") != null)
            {
                this.Active = bool.Parse(xdoc.Attribute("active").Value);
            }

            this.Customers = xdoc.Descendants("Customer").Select(c => new HogiaCustomer(c)).ToArray();

        }
    }

    public class HogiaCustomer
    {
        public int CustomerId { get; set; }
        public Uri DefaultEndPoint { get; set; }
        public bool Active { get; set; }
        public HogiaSetting[] Settings { get; set; }

        public HogiaCustomer(XElement xdoc)
        {
            if (xdoc.Attribute("customerId") != null)
            {
                this.CustomerId = int.Parse(xdoc.Attribute("customerId").Value);
            }

            if (xdoc.Attribute("active") != null)
            {
                this.Active = bool.Parse(xdoc.Attribute("active").Value);
            }

            if (xdoc.Attribute("defaultEndPoint") != null)
            {
                this.DefaultEndPoint = new Uri(xdoc.Attribute("defaultEndPoint").Value);
            }

            this.Settings = xdoc.Descendants("Setting").Select(s => new HogiaSetting(s)).ToArray();
        }
    }

    public class HogiaSetting
    {
        public string Name { get; set; }
        public bool Active { get; set; }

        public bool ExtendedSearch { get; set; }

        public Uri EndPoint { get; set; }
        public string[] Buses { get; set; }

        public HogiaSetting(XElement xdoc)
        {

            if (xdoc.Attribute("name") != null)
            {
                this.Name = xdoc.Attribute("name").Value;
            }

            if (xdoc.Attribute("active") != null)
            {
                this.Active = bool.Parse(xdoc.Attribute("active").Value);
            }

            if (xdoc.Attribute("sorting") != null)
            {
                this.ExtendedSearch = bool.Parse(xdoc.Attribute("sorting").Value);
            }
            else
            {
                this.ExtendedSearch = false;
            }

            var endpoint = xdoc.Descendants("Endpoint").FirstOrDefault();

            if (endpoint != null)
                this.EndPoint = new Uri(endpoint.Value);

            var busFilter = xdoc.Descendants("BusFilter").FirstOrDefault();
            if (busFilter != null)
                this.Buses = busFilter.Value.Replace(" ", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

        }
    }

    public class HogiaRealtimeAssignmentService
    {

        public HogiaRealtimeAssignmentService() { }

        public static HogiaService[] ReadAllConfigurations()
        {
            HogiaRealtimeAssignmentServiceConfig config = (IBI.Shared.ConfigSections.HogiaRealtimeAssignmentServiceConfig)ConfigurationManager.GetSection("HogiaRealtimeAssignmentService");


            //var subConfigs = (from e in config.Xdoc.Descendants("Service")
            //                  select e);

            HogiaService[] services = config.Xdoc.Descendants("Service").Select(c => new HogiaService(c)).ToArray();

            return services;
        }

        public static bool ExtendedSearchEnabled(string busNumber, int customerId, string serviceName)
        {
            HogiaService[] services = HogiaRealtimeAssignmentService.ReadAllConfigurations();
            var service = services.Where(s => s.Active && s.Name == serviceName).FirstOrDefault();

            if (service != null)
            {
                var customer = service.Customers.Where(cust => cust.Active && cust.CustomerId == customerId).FirstOrDefault();

                if (customer != null)
                {
                    var setting = customer.Settings.Where(s => s.Active).FirstOrDefault();

                    if (setting != null)
                        return setting.ExtendedSearch;
                }
            }
            //default
            return false;
        }

        public static Uri GetServerAddress(string busNumber, int customerId, string serviceName)
        {
            HogiaService[] services = HogiaRealtimeAssignmentService.ReadAllConfigurations();
            var service = services.Where(s => s.Active && s.Name == serviceName).FirstOrDefault();

            if (service != null)
            {
                var customer = service.Customers.Where(cust => cust.Active && cust.CustomerId == customerId).FirstOrDefault();

                if (customer != null)
                {
                    var setting = customer.Settings.Where(s => s.Active && s.Buses.Contains(busNumber)).FirstOrDefault();

                    if (setting != null)
                        return setting.EndPoint;
                    else
                        return customer.DefaultEndPoint;
                }
                else
                {
                    return service.DefaultEndPoint;
                }

            }

            throw new Exception(string.Format("Service Name \"{0}\" is not implemented in configuration file OR it's marked Inactive", serviceName));
        }



     public static bool BusExistsForService(string busNumber, int customerId, string serviceName)
        {
            HogiaService[] services = HogiaRealtimeAssignmentService.ReadAllConfigurations();
            var service = services.Where(s => s.Active && s.Name == serviceName).FirstOrDefault();

            if (service != null)
            {
                var customer = service.Customers.Where(cust => cust.Active && cust.CustomerId == customerId).FirstOrDefault();

                if (customer != null)
                {
                    //check if it's being denied by some settings
                    var setting = customer.Settings.Where(s => s.Active == false && s.Buses.Contains(busNumber)).FirstOrDefault();
                    if(setting!=null)
                    {
                        return false;
                    }


                    //if setting for bus exists OR a setting exists where there are no buses spcified [i.e. no buses specified means it open for all the buses]
                    setting = customer.Settings.Where(s => s.Active && (s.Buses.Contains(busNumber) || s.Buses.Count()==0)).FirstOrDefault();

                    if (setting != null)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }

            }

            throw new Exception(string.Format("Service Name \"{0}\" is not implemented in configuration file OR it's marked Inactive", serviceName));
        }
    }
     


    public class HogiaRealtimeAssignmentServiceConfigHandler : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            return HogiaRealtimeAssignmentServiceConfig.ParseXml(section);
        }
    }
    public class HogiaRealtimeAssignmentServiceConfig
    {
        public XDocument Xdoc { get; set; }
        public static HogiaRealtimeAssignmentServiceConfig ParseXml(XmlNode section)
        {
            HogiaRealtimeAssignmentServiceConfig config = new HogiaRealtimeAssignmentServiceConfig();
            config.Xdoc = XDocument.Parse(section.OuterXml.ToString());
            return config;
        }
    }


}