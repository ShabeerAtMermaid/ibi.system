﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Globalization;
using Microsoft.SqlServer.Types;
using System.IO;
using mermaid.BaseObjects.IO;
using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;
using System.Xml.Linq;

namespace IBI.Shared
{
    public class AppUtility
    {
        public static String getClientIP()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties messageProperties = context.IncomingMessageProperties;
            RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;

            return endpointProperty.Address;
        }

        public static IFormatProvider getDateFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd";
            dateFormat.LongDatePattern = "yyyy-MM-dd";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd";
            dateFormat.DateSeparator = "-";

            return dateFormat;
        }

        public static IFormatProvider getDateTimeFormatProvider()
        {
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.LongDatePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.FullDateTimePattern = "yyyy-MM-dd HH:mm:ss";
            dateFormat.DateSeparator = "-";
            dateFormat.TimeSeparator = ":";

            return dateFormat;
        }

        public static string SafeSqlLiteral(string inputSQL)
        {
            if (!String.IsNullOrEmpty(inputSQL))
                return inputSQL.Replace("'", "''");
            else
                return "";
        }


        public static Bitmap Base64StringToBitmap(string base64String)
        {
            try
            {
            Bitmap bmpReturn = null;

            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);

            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);

            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;

            return bmpReturn;
        }
            catch (Exception ex)
            {
                throw  new Exception("Message " +  ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }

        /*
        public void ConvertHtmlToImage(string html)
        {
            Bitmap m_Bitmap = new Bitmap(400, 600);
            PointF point = new PointF(0, 0);
            SizeF maxSize = new System.Drawing.SizeF(500, 500);
            HtmlRender.Render(Graphics.FromImage(m_Bitmap),
                                                    html,
                                                     point, maxSize);

            m_Bitmap.Save(@"C:\Test.png", ImageFormat.Png);
        }
        */

/*
        public static byte[] ConvertBase64toLEDImage(string base64Image)
        {
            try
            {
            if (string.IsNullOrEmpty(base64Image)) return null;

            string Image = base64Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);

            Bitmap finalImage = null;

            string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

            System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on1.png"));
            System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off1.png"));

            int width = img.Width * 2;
            int height = img.Height * 2;

            //create a bitmap to hold the combined image
            finalImage = new System.Drawing.Bitmap(width, height);

            float brightness = 1.0f; // no change in brightness
            float contrast = 2.0f; // twice the contrast
            float gamma = 1.0f; // no change in gamma

            float adjustedBrightness = brightness - 1.0f;
            // create matrix that will brighten and contrast the image
            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, 1.0f, 0}, // don't scale alpha
            new float[] {adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1}};

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.ClearColorMatrix();
            imageAttributes.SetColorMatrix(new ColorMatrix(ptsArray), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);

            //get a graphics object from the image so we can draw on it
            using (Graphics graphics = Graphics.FromImage(finalImage))
            {
                //set background color
                //graphics.Clear(Color.Black);

                int x = 0;
                int y = 0;

                for (int j = 0; j < img.Height; j++)
                {
                    //string tmpRow = "";
                    for (int i = 0; i < img.Width; i++)
                    {
                        Color pixel = img.GetPixel(i, j);

                        int r = pixel.R;
                        int g = pixel.G;
                        int b = pixel.B;

                        x = i * 2;
                        y = j * 2;

                        if ((r < 100) && (g < 100) && (b < 100))
                        {

                            graphics.DrawImage(goldenPixel, new Rectangle(x, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                        }
                        else
                        {
                            graphics.DrawImage(blackPixel, new Rectangle(x, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                        }
                    }
                }
            }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
        }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }

        public static byte[] ConvertBase64toLEDImageLarge(string base64Image)
        {
            if (string.IsNullOrEmpty(base64Image)) return null;

            string Image = base64Image;

            string grayScaleImage = string.Empty;
            Bitmap img = Base64StringToBitmap(Image);

            Bitmap finalImage = null;

            string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

            System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on1.png"));
            System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off1.png"));

            int width = img.Width * 4;
            int height = img.Height * 4;

            //create a bitmap to hold the combined image
            finalImage = new System.Drawing.Bitmap(width, height);

            float brightness = 1.0f; // no change in brightness
            float contrast = 2.0f; // twice the contrast
            float gamma = 1.0f; // no change in gamma

            float adjustedBrightness = brightness - 1.0f;
            // create matrix that will brighten and contrast the image
            float[][] ptsArray ={
            new float[] {contrast, 0, 0, 0, 0}, // scale red
            new float[] {0, contrast, 0, 0, 0}, // scale green
            new float[] {0, 0, contrast, 0, 0}, // scale blue
            new float[] {0, 0, 0, 1.0f, 0}, // don't scale alpha
            new float[] {adjustedBrightness, adjustedBrightness, adjustedBrightness, 0, 1}};

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.ClearColorMatrix();
            imageAttributes.SetColorMatrix(new ColorMatrix(ptsArray), ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            imageAttributes.SetGamma(gamma, ColorAdjustType.Bitmap);

            //get a graphics object from the image so we can draw on it
            using (Graphics graphics = Graphics.FromImage(finalImage))
            {
                //set background color
                //graphics.Clear(Color.Black);

                int x = 0;
                int y = 0;

                for (int j = 0; j < img.Height; j++)
                {
                    //string tmpRow = "";
                    for (int i = 0; i < img.Width; i++)
                    {
                        Color pixel = img.GetPixel(i, j);

                        int r = pixel.R;
                        int g = pixel.G;
                        int b = pixel.B;

                        x = i * 4;
                        y = j * 4;


                        //if ((r < 100) && (g < 100) && (b < 100))
                        //{
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x+1, y, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x, y+1, goldenPixel.Width, goldenPixel.Height));
                        //    graphics.DrawImage(goldenPixel, new Rectangle(x+1, y+1, goldenPixel.Width, goldenPixel.Height));
                        //}
                        //else
                        //{
                        //    graphics.DrawImage(blackPixel, new Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x+1, y, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x, y+1, blackPixel.Width, blackPixel.Height));
                        //    graphics.DrawImage(blackPixel, new Rectangle(x+1, y+1, blackPixel.Width, blackPixel.Height));
                        //}


                        if ((r < 100) && (g < 100) && (b < 100))
                        {

                            graphics.DrawImage(goldenPixel, new Rectangle(x, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x, y + 2, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x, y + 3, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);

                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y + 2, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 1, y + 3, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);

                            graphics.DrawImage(goldenPixel, new Rectangle(x + 2, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 2, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 2, y + 2, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 2, y + 3, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);

                            graphics.DrawImage(goldenPixel, new Rectangle(x + 3, y, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 3, y + 1, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 3, y + 2, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(goldenPixel, new Rectangle(x + 3, y + 3, goldenPixel.Width, goldenPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);

                        }
                        else
                        {
                            graphics.DrawImage(blackPixel, new Rectangle(x, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x, y + 2, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x, y + 3, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);


                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y + 2, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 1, y + 3, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);


                            graphics.DrawImage(blackPixel, new Rectangle(x + 2, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 2, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 2, y + 2, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 2, y + 3, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);


                            graphics.DrawImage(blackPixel, new Rectangle(x + 3, y, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 3, y + 1, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 3, y + 2, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);
                            graphics.DrawImage(blackPixel, new Rectangle(x + 3, y + 3, blackPixel.Width, blackPixel.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imageAttributes);

                        }
                    }
                }
            }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
        }
*/

        public static byte[] ConvertBase64toLEDImage(string base64Image)
        {
            try
            {
                if (string.IsNullOrEmpty(base64Image)) return null;

                string Image = base64Image;

                string grayScaleImage = string.Empty;
                Bitmap img = Base64StringToBitmap(Image);

                Bitmap finalImage = null;

                string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

                System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on1.png"));
                System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off1.png"));

                int width = img.Width * 2;
                int height = img.Height * 2;

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);

                //get a graphics object from the image so we can draw on it
                using (Graphics graphics = Graphics.FromImage(finalImage))
                {
                    //set background color
                    //graphics.Clear(Color.Black);

                    int x = 0;
                    int y = 0;

                    for (int j = 0; j < img.Height; j++)
                    {
                        //string tmpRow = "";
                        for (int i = 0; i < img.Width; i++)
                        {
                            Color pixel = img.GetPixel(i, j);

                            int r = pixel.R;
                            int g = pixel.G;
                            int b = pixel.B;

                            x = i * 2;
                            y = j * 2;

                            if ((r < 100) && (g < 100) && (b < 100))
                            {
                                graphics.DrawImage(goldenPixel, new System.Drawing.Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                        }
                            else
                            {
                                graphics.DrawImage(blackPixel, new System.Drawing.Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                    }
                }
            }
                }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
        }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }

        public static byte[] ConvertBase64toLEDImageLarge(string base64Image)
        {
            try
            {
                if (string.IsNullOrEmpty(base64Image)) return null;

                string Image = base64Image;

                string grayScaleImage = string.Empty;
                Bitmap img = Base64StringToBitmap(Image);

                Bitmap finalImage = null;

                string resourceDirectory = System.Configuration.ConfigurationManager.AppSettings["ResourceDirectory"].ToString();

                System.Drawing.Bitmap goldenPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_on.png"));
                System.Drawing.Bitmap blackPixel = new Bitmap(System.IO.Path.Combine(resourceDirectory, "Images/dot_off.png"));

                int width = img.Width * 4;
                int height = img.Height * 4;

                //create a bitmap to hold the combined image
                finalImage = new System.Drawing.Bitmap(width, height);

                //get a graphics object from the image so we can draw on it
                using (Graphics graphics = Graphics.FromImage(finalImage))
                {
                    //set background color
                    //graphics.Clear(Color.Black);

                    int x = 0;
                    int y = 0;

                    for (int j = 0; j < img.Height; j++)
                    {
                        //string tmpRow = "";
                        for (int i = 0; i < img.Width; i++)
                        {
                            Color pixel = img.GetPixel(i, j);

                            int r = pixel.R;
                            int g = pixel.G;
                            int b = pixel.B;

                            x = i * 4;
                            y = j * 4;

                            if ((r < 100) && (g < 100) && (b < 100))
                            {
                                graphics.DrawImage(goldenPixel, new System.Drawing.Rectangle(x, y, goldenPixel.Width, goldenPixel.Height));
                            }
                            else
                            {
                                graphics.DrawImage(blackPixel, new System.Drawing.Rectangle(x, y, blackPixel.Width, blackPixel.Height));
                        }
                    }
                }
            }
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(finalImage, typeof(byte[]));
        }
            catch (Exception ex)
            {
                throw new Exception("Message " + ex.Message + Environment.NewLine + ex.StackTrace);
            }

        }

        public static List<String> CommaValuesToList(string commaSeparatedText)
        {
            List<String> list = new List<string>();

            if (commaSeparatedText == null || commaSeparatedText == "")
                return list;

            list = commaSeparatedText.Split(',').Select(x => x.ToString().Trim().ToLower()).ToList();
            return list;

        }

        public static string ListToCommaValues(List<string> lst)
        {
            if (lst == null)
                return "";

            string retVal = "";
            foreach (string s in lst)
                retVal += s + ",";

            retVal = retVal.Trim().TrimEnd(',');

            return retVal;
        }

        public static string ListToSingleLineText(List<string> lst)
        {
            if (lst == null)
                return "";

            string retVal = "";
            foreach (string s in lst)
                retVal += s;

            retVal = retVal.Trim();

            return retVal;
        }


        public static byte[] Text2Bytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string Bytes2Text(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        #region Geography

        //public static GetGeography(){
        //    var point = SqlGeography.Point(double.Parse(latitude), double.Parse(longitude), 4326); 
        //    var poly = point.BufferWithTolerance(1, 0.01, true);
        //}

        #endregion

        #region Safe Type Conversion


        public static DateTime ToDateTime(Object val)
        {
            if (val == null)
                return DateTime.MinValue;

            DateTime dt;
            return DateTime.TryParse(val.ToString(), out dt) ? dt : DateTime.MinValue;
        }

        public static DateTime ToDateTime(Object val, string alternateVal)
        {
            if (val == null)
                return DateTime.MinValue;

            DateTime dt;
            return DateTime.TryParse(val.ToString(), out dt) ? dt : Convert.ToDateTime(alternateVal);
        }

        public static DateTime ToDateTime(Object val, DateTime alternateVal)
        {
            if (val == null)
                return DateTime.MinValue;

            DateTime dt;
            return DateTime.TryParse(val.ToString(), out dt) ? dt : alternateVal;
        }

        public static string ToStr(Object val, string defaultValue = "")
        {
            return val == null ? defaultValue : val.ToString();
        }

        public static int ToInt(Object val, int alternateVal)
        {
            int i;
            return int.TryParse(ToStr(val), out i) ? i : alternateVal;
        }

        public static int ToInt(Object val)
        {
            int i;
            return int.TryParse(ToStr(val), out i) ? i : 0;
        }

        public static Int16 ToInt16(Object val, Int16 alternateVal)
        {
            Int16 i;
            return Int16.TryParse(ToStr(val), out i) ? i : alternateVal;
        }

        public static Int16 ToInt16(Object val)
        {
            Int16 i;
            return Int16.TryParse(ToStr(val), out i) ? i : Convert.ToInt16(0);
        }

        public static Int32 ToInt32(Object val, Int32 alternateVal)
        {
            Int32 i;
            return Int32.TryParse(ToStr(val), out i) ? i : alternateVal;
        }

        public static Int32 ToConvertInt32(Object val)
        {
            Int32 i;
            return Int32.TryParse(ToStr(val), out i) ? i : 0;
        }

        public static Int64 ToInt64(Object val, Int64 alternateVal)
        {
            Int64 i;
            return Int64.TryParse(ToStr(val), out i) ? i : alternateVal;
        }

        public static Int64 ToInt64(Object val)
        {
            Int64 i;
            return Int64.TryParse(ToStr(val), out i) ? i : Convert.ToInt64(0);
        }

        public static Double ToDouble(Object val)
        {
            Double dbl;
            return Double.TryParse(ToStr(val), out dbl) ? dbl : 0.0;
        }

        public static float Tofloat(Object val)
        {
            float fl;
            return float.TryParse(ToStr(val), out fl) ? fl : (float)0.0;
        }

        public static Decimal ToDecimal(Object val)
        {
            Decimal dec;
            return Decimal.TryParse(ToStr(val), out dec) ? dec : (Decimal)0.0;
        }

        public static Boolean ToBool(Object val)
        {
            bool b;
            return bool.TryParse(ToStr(val), out b) ? b : false;
        }

        public static Boolean ToBool(Object val, bool alternateVal)
        {
            bool b;
            return bool.TryParse(ToStr(val), out b) ? b : alternateVal;
        }

        public static byte ToByte(Object val, byte alternateVal)
        {
            byte i;
            return byte.TryParse(ToStr(val), out i) ? i : alternateVal;
        }

        public static byte ToByte(Object val)
        {
            byte i;
            return byte.TryParse(ToStr(val), out i) ? i : (byte)(0);
        }

        #endregion

        #region Null Values

        public static int IsNullInt(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return 0;
            else
                return Convert.ToInt32(val);
        }

        public static long IsNullLong(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return 0;
            else
                return Convert.ToInt64(val);
        }

        public static Object IsNull(Object val, Object defaultVal)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return defaultVal;
            else
                return val;
        }

        public static string IsNullStr(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return string.Empty;
            else
                return val.ToString();
        }

        public static string IsNullStr(Object val, string defaultVal)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return defaultVal;
            else
                return val.ToString();
        }

        public static bool IsNullBool(Object val, Boolean defaultVal)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return defaultVal;
            else
                return Convert.ToBoolean(val);
        }

        public static bool IsNullBool(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return false;
            else
                return Convert.ToBoolean(val);
        }

        public static DateTime IsNullDate(Object val, DateTime defaultVal)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return defaultVal;
            else
                return Convert.ToDateTime(val);
        }

        public static DateTime? IsNullDate(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return null;
            else
                return Convert.ToDateTime(val);
        }

        public static decimal IsNullDec(Object val)
        {
            if (val == null || val.ToString() == DBNull.Value.ToString())
                return 0;
            else
                return Convert.ToDecimal(val);
        }

        #endregion

        #region Strings

        public static string TrimText(string text, int length)
        {
            if (text.Length < length)
                return text;

            return text.Substring(0, length) + "...";
        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static string ConvertStringtoMD5(string strword)
        {
            return FileHash.FromStream(GenerateStreamFromString(strword)).ToString();
        }

        #endregion

        #region Logging
        public static void Log2File(string logName, string message, bool doWrite = false)
        {
            if (doWrite || AppSettings.LogAllMessages())
                mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI_" + logName, message);
        }
        #endregion


        #region Schedules


        private string TransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.transformStopNo"))
            {
                String transformedStopNo = stopNo;

                //eg. 9025200000028662
                if (stopNo.Length < 16)
                {
                    transformedStopNo = "9025200" + mermaid.BaseObjects.Functions.ConvertToFixedLength('0', mermaid.BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 9, stopNo);
                }

                return transformedStopNo;
            }
        }

        private static string UnTransformStopNo(string stopNo)
        {
            using (new IBI.Shared.CallCounter("ScheduleServiceController.unTransformStopNo"))
            {
                //eg. 9025200000028662
                if (stopNo.Length == 16) // it's a transformed stop number
                {
                    stopNo = Convert.ToInt32(stopNo.Replace("9025200", "")).ToString();
                }

                return stopNo;
            }
        }


        public static string FixScheduleStopAddonsXML(string scheduleXml)
        {
            StringReader rd = new StringReader(scheduleXml);
            XDocument xDoc = XDocument.Load(rd);

            foreach (var stopInfo in xDoc.Descendants("StopInfo"))
            {
                foreach (var addon in stopInfo.Descendants("Addon"))
                {
                    var key = addon.Descendants("Key").FirstOrDefault();
                    var val = addon.Descendants("Value").FirstOrDefault();


                    if (key != null)
                    {
                        XElement customElement = null;
                        if (val != null)
                        {
                            string elementName = key.Value.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
                            customElement = new XElement(elementName, new XCData(val.Value.ToString()));
                            XAttribute attr = new XAttribute("isAddon", "true");
                            customElement.Add(attr);
                        }

                        stopInfo.Add(customElement);
                    }
                }
                stopInfo.Descendants("ScheduleStopAddons").Remove();
            }

            return xDoc.ToString();
        }

        public static string FixScheduleAddonsXML(string scheduleXml, string rootNode)
        {
            StringReader rd = new StringReader(scheduleXml);
            XDocument xDoc = XDocument.Load(rd);

            foreach (var scheduleInfo in xDoc.Descendants(rootNode))
            {
                foreach (var addon in scheduleInfo.Descendants("Addon"))
                {
                    var key = addon.Descendants("Key").FirstOrDefault();
                    var val = addon.Descendants("Value").FirstOrDefault();


                    if (key != null)
                    {
                        XElement customElement = null;
                        if (val != null)
                        {
                            string elementName = key.Value.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
                            customElement = new XElement(elementName, new XCData(val.Value.ToString()));
                            XAttribute attr = new XAttribute("isAddon", "true");
                            customElement.Add(attr);
                        }

                        scheduleInfo.Add(customElement);
                    }
                }
                scheduleInfo.Descendants("ScheduleAddons").Remove();
            }

            return xDoc.ToString();
        }

        #endregion
    }
}