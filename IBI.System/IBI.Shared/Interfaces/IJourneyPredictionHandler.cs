﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBI.Shared.Models;

namespace IBI.Shared.Interfaces
{
    public interface IJourneyPredictionHandler
    {
        int CustomerId { get; set; }

        bool FetchJourneyPredictions(int journeyId, string clientRef);
       
    }
}
