﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using IBI.DataAccess.DBModels;
using System.Text;
using System.Reflection;

namespace IBI.Shared.Diagnostics
{
    public class ActivityLogger
    {
        #region Enums

        public enum Entity
        {
            SIGN_GROUP,
            SIGN_ITEM,
            SIGN_IMAGE,
            SIGN_ADDON_ITEM
        }

        public enum Action
        {
            //SIGN_GROUP_CREATED,
            //SIGN_GROUP_EDITED,
            //SIGN_GROUP_DELETED,
            //SIGN_ITEM_CREATED,
            //SIGN_ITEM_EDITED,
            //SIGN_ITEM_DELETED
            CREATE,
            EDIT,
            DELETE
        }

        public enum Module
        {
            SIGN_MANAGEMENT,
            SIGN_ADDON_MANAGEMENT
        }

        #endregion

        //public static void LogError(Exception ex)
        //{
        //    LogMessage(GetDetailedError(ex));
        //}

        //public static void LogMessage(string debugInfo)
        //{
        //    try
        //    {
        //        mermaid.BaseObjects.Diagnostics.Logger.WriteLine("IBI", debugInfo);
        //    }
        //    catch (Exception ex)
        //    {
        //        //UNDONE - ignore
        //    }
        //}

        //private static string GetDetailedError(Exception ex)
        //{
        //    string description = string.Empty;

        //    Exception tmpException = ex;

        //    while (tmpException != null)
        //    {
        //        description += tmpException.Message + Environment.NewLine;
        //        description += tmpException.StackTrace + Environment.NewLine;
        //        description += "--" + Environment.NewLine;

        //        tmpException = tmpException.InnerException;
        //    }

        //    return description;
        //}

        public static void AddActivityLog(string userName, Module module, Entity entity, Action action, string description, object firstObject = null, object secondObject = null)
        {
            try
            {
                using (IBILogsModel dbContext = new IBILogsModel())
                {

                    if (string.IsNullOrEmpty(description) && secondObject != null)
                    {
                        if (firstObject != null)
                            description = DetectChangesIn2Objects(firstObject, secondObject);
                        else
                            description = GetObjectToString(secondObject);
                    }

                    dbContext.AppendActivityLog(userName, module.ToString(), entity.ToString(), action.ToString(), description);
                    dbContext.SaveChanges();

                }
                
            }
            catch (Exception ex)
            {
                //UNDONE: Silent log
               
            }
        }

        public static string GetObjectToString(object obj)
        {
            StringBuilder retVal = new StringBuilder();
            retVal.Append("");

            if (obj == null)
                return "null object";

            Type objType = obj.GetType();
            foreach (PropertyInfo propertyInfo in objType.GetProperties())
            {
                if (propertyInfo.CanRead && (propertyInfo.PropertyType.IsClass == false || propertyInfo.PropertyType.Name == "String") && propertyInfo.Name != "EntityState")
                {
                    object value = propertyInfo.GetValue(obj, null);

                    retVal.AppendFormat("{0} : [{1}] ~ ", propertyInfo.Name, (value == null ? "null" : value.ToString()));
                        
                }
            }

            return retVal.ToString().Trim().Trim('~');

        }

        public static string DetectChangesIn2Objects(object first, object second)
        {
            StringBuilder retVal = new StringBuilder();
            retVal.Append("");

            if (first == null && second == null)
            {
                return "Null object Updated to Null";
            }
            if (first == null || second == null)
            {
                return "invlid data";
            }
            Type firstType = first.GetType();
            if (second.GetType() != firstType)
            {
                return "Incompatible Data"; // Or throw an exception
            }
            // This will only use public properties. Is that enough?
            foreach (PropertyInfo propertyInfo in firstType.GetProperties())
            {
                if (propertyInfo.CanRead && (propertyInfo.PropertyType.IsClass == false || propertyInfo.PropertyType.Name=="String") && propertyInfo.Name != "EntityState")
                {
                    object firstValue = propertyInfo.GetValue(first, null);
                    object secondValue = propertyInfo.GetValue(second, null);
                    if (!object.Equals(firstValue, secondValue))
                    {
                        retVal.AppendFormat("{0} : [{1}] --> [{2}] ~ ", propertyInfo.Name, 
                            (firstValue == null ? "null" : firstValue.ToString()),
                            (secondValue == null ? "null" : secondValue.ToString()));
                    }
                }
            }

            if (String.IsNullOrEmpty(retVal.ToString()))
                retVal.Append("No Change");

            return retVal.ToString().Trim().Trim('~');
        }

        //public static void increaseCounter(String counterName)
        //{
        //    try
        //    {
        //        using (IBIDataModel dbContext = new IBIDataModel())
        //        {
                    
        //            dbContext.UpdateCounter(counterName);
        //            dbContext.SaveChanges();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        AppendToSystemLog(EntryTypes.Error, EntryCategories.IBI, ex);
        //    }
        //}
    }
}