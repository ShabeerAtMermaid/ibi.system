﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Shared.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class Schedule
    {
        private String scheduleNumber;
        private String busNumber;
        private String line;
        private String journeyNumber;
        private string externalReference;
        private String fromName;
        private String destinationName;

        StopInfo[] journeyStops;

        [DataMember(Order = 1)]
        public String ScheduleNumber
        {
            get { return scheduleNumber; }
            set { scheduleNumber = value; }
        }

        [DataMember(Order = 2)]
        public String BusNumber
        {
            get { return busNumber; }
            set { busNumber = value; }
        }

        [DataMember(Order = 3)]
        public String Line
        {
            get { return line; }
            set { line = value; }
        }

        [DataMember(Order = 4)]
        public String JourneyNumber
        {
            get { return journeyNumber; }
            set { journeyNumber = value; }
        }

        [DataMember(Order = 5)]
        public String ExternalReference
        {
            get { return externalReference; }
            set { externalReference = value; }
        }


        [DataMember(Order = 6)]
        public String FromName
        {
            get { return fromName; }
            set { fromName = value; }
        }

        [DataMember(Order = 7)]
        public String DestinationName
        {
            get { return destinationName; }
            set { destinationName = value; }
        }

        [DataMember(IsRequired = true, Order = 8)]
        public String ViaName
        {
            get;
            set;
        }

        [DataMember(IsRequired = true, Order = 9)]
        public Boolean Cached
        {
            get;
            set;
        }
        
        [DataMember(Order = 10)]
        public StopInfo[] JourneyStops
        {
            get { return this.journeyStops; }
            set { this.journeyStops = value; }
        }
    }

}

namespace IBI.Shared.Models.Realtime
{
   [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class Schedule
    {
        private String scheduleNumber;
        private String busNumber;
        private String line;
        private String journeyNumber;
        private string externalReference;
        private String fromName;
        private String destinationName;

        StopInfo[] journeyStops;

        [DataMember(Order = 1)]
        public String ScheduleNumber
        {
            get { return scheduleNumber; }
            set { scheduleNumber = value; }
        }

        [DataMember(Order = 2)]
        public String BusNumber
        {
            get { return busNumber; }
            set { busNumber = value; }
        }

        [DataMember(Order = 3)]
        public String Line
        {
            get { return line; }
            set { line = value; }
        }

        [DataMember(Order = 4)]
        public String JourneyNumber
        {
            get { return journeyNumber; }
            set { journeyNumber = value; }
        }

        [DataMember(Order = 5)]
        public String ExternalReference
        {
            get { return externalReference; }
            set { externalReference = value; }
        }


        [DataMember(Order = 6)]
        public String FromName
        {
            get { return fromName; }
            set { fromName = value; }
        }

        [DataMember(Order = 7)]
        public String DestinationName
        {
            get { return destinationName; }
            set { destinationName = value; }
        }

        [DataMember(IsRequired = true, Order = 8)]
        public String ViaName
        {
            get;
            set;
        }

        [DataMember(IsRequired = true, Order = 9)]
        public Boolean Cached
        {
            get;
            set;
        }

        [DataMember(Order = 10)]
        public StopInfo[] JourneyStops
        {
            get { return this.journeyStops; }
            set { this.journeyStops = value; }
        }
    }
}