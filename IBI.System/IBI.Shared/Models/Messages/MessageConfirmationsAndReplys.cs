﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;


namespace IBI.Shared.Models.Messages
{
    //[DataContract(Namespace = "")]
    //[KnownType(typeof(IBI.Shared.Models.Messages.Message))]
    public class MessageConfirmationsAndReplys
    {
        public int MessageId { get; set; }
        public string MessageTitle { get; set; }
        public List<MessageDisplayConfirmations> MessageDisplayConfirmations { get; set; }
        public List<IncomingMessage> ReplysMessages { get; set; }
    }
}
