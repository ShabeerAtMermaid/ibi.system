﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml.Serialization;

namespace IBI.Shared.Models.Messages
{
    [DataContract(Namespace = "")]
    [KnownType(typeof(IBI.Shared.Models.Messages.Message))]
    public class MessageDisplayConfirmations
    {
        [DataMember]
        public int MessageId { get; set; }

        [DataMember]
        public string BusNumber { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public DateTime DisplayTime { get; set; }
         
        [DataMember]
        public int BusGroupId { get; set; }

         [DataMember]
        public string BusGroupName { get; set; }
    }
}
