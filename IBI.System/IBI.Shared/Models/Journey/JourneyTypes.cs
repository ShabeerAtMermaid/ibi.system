﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.Journey
{

    public class RouteDirection
    {
        public int RouteDirectionId { get; set; }

        public string Name { get; set; }

    }

   public class Schedule
    {
        public int ScheduleId { get; set; }

        public int CustomerId { get; set; }

        public int RouteDirectionId { get; set; }

        public string Line { get; set; }

        public string FromName { get; set; }

        public string Destination { get; set; }

        public string ViaName { get; set; }

        public DateTime LastUpdated { get; set; }

        public List<ScheduleStop> ScheduleStops
        {
            get; set; 
        }

    }

   public class ScheduleStop
   {
       public int ScheduleId { get; set; }

       public int StopSequence { get; set; }

       public decimal StopId { get; set; }

       public string StopName { get; set; }

       public System.Data.Entity.Spatial.DbGeography StopGPS { get; set; }

       public decimal Latitude { get; set; }
       public decimal Longitude { get; set; }

       public string Zone { get; set; }

   }


   public class Journey
   {
       public int JourneyId { get; set; }

       public string BusNumber { get; set; }

       public int ScheduleId { get; set; }

       public int CustomerId { get; set; }

       public DateTime? PlannedStartTime { get; set; }
       public DateTime? PlannedEndTime { get; set; }
       public DateTime? StartTime { get; set; }
       public DateTime? EndTime { get; set; }
       public DateTime? LastUpdated { get; set; }
       
       public byte? JourneyStateId { get; set; }

       public string ExternalReference { get; set; }

       public Guid? ClientRef { get; set; }


       public string Line { get; set; }
       public string FromName { get; set; }
       public string Destination { get; set; }
       public string ViaName { get; set; }
       public int? PunctualityData { get; set; }
       public DateTime? JourneyStartTime { get; set; }

       public int? Row { get; set; }

       public List<JourneyStop> JourneyStops
       {
           get;
           set; 
       }

       public int TotalRows { get; set; }


   }

   public class JourneyStop
   {
       public int JourneyId { get; set; }

       public int StopSequence { get; set; }

       public decimal StopId { get; set; }

       public string StopName { get; set; }

       public System.Data.Entity.Spatial.DbGeography StopGPS { get; set; }

       public decimal Latitude { get; set; }
       public decimal Longitude { get; set; }

       public string Zone { get; set; }

       public DateTime PlannedArrivalTime { get; set; }

       public DateTime PlannedDepartureTime { get; set; }

       public DateTime ActualArrivalTime { get; set; }

       public DateTime ActualDepartureTime { get; set; }

       public int JourneyBehind { get; set; }

       public int JourneyAhead { get; set; }

   }

    public class JourneyPrediction
    {
        public int JourneyId {get; set;}
        public int EstimatedStopSequence {get; set;}
        public DateTime EstimationTime {get; set;}
        public string EstimationSource {get; set;}
        public int? EstimatedAtStopSequence {get; set;}
        public DateTime? EstimatedArrivalTime {get; set;}
        public DateTime? EstimatedDepartureTime {get; set;}

    }
     
}
