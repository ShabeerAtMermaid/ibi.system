﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Shared.Models.UserScheduleModels
{
    public class Position
    {
        public double? Lat { get; set; }
        public double? Long { get; set; }
    }

    public class ScheduleStop
    {
        public int SequenceNumber { get; set; }
        public decimal StopNumber { get; set; }
        public string Name { get; set; }
        public string Zone { get; set; }
        public Position Position { get; set; }
    }

    public class Schedule
    {
        public int ScheduleID { get; set; }
        public string Line { get; set; }
        public string FromName { get; set; }
        public string Destination { get; set; }
        public string Via { get; set; }
        public List<ScheduleStop> Stops { get; set; }
    }
}
