﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace IBI.Shared.Models.Schedules
{
    [DataContract]
    public class SignAddonListModel 
    {
        [DataMember]
        public int Id { get; set; }
         
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string NodeText { get; set; }

        [DataMember]
        public string UdpText { get; set; }
        
        [DataMember]
        public int Priority { get; set; }

        public class Comparer : IEqualityComparer<SignAddonListModel>
        {
            public bool Equals(SignAddonListModel x, SignAddonListModel y)
            {
                return x.Name == y.Name;
            }

            public int GetHashCode(SignAddonListModel obj)
            {
                return (obj.Name).GetHashCode();
            }
        }
    }

}
