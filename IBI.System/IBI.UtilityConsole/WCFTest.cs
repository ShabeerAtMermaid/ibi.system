﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IBI.UtilityConsole
{
    class WCFTest
    {
        internal static void TestCorrespndingTraffice()
        {


            //Console.Write("Enter CustomerID: ");
            string customerId = "2140"; //Console.ReadLine();
            string stopNumber = "";
            
            //Console.Write("Enter IBI Stop Number: ");
            //string stopNumber = Console.ReadLine();

            Console.Write("delay in milliseconds: ");
            int delay = int.Parse(Console.ReadLine());


            string[] stops = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "stops.txt"));
           int counter = 0;

            Console.WriteLine("Press ESC to stop");
            do
            {
                while (!Console.KeyAvailable)
                {
                    DateTime startDate = DateTime.Now;

                    if (counter == stops.Length - 1)
                        counter = 0;

                    stopNumber = stops[counter++];


                    try
                    {

                        if (String.IsNullOrEmpty(stopNumber))
                        {
                            Console.WriteLine("No Stopnumber available from stops.txt file");
                            return;
                        }
                            


                        string url = System.Configuration.ConfigurationSettings.AppSettings["CorrespondingTrafficURL"].ToString().Replace("{0}", customerId).Replace("{1}", stopNumber);

                        System.Net.WebClient webClient = new System.Net.WebClient();
                        string result = webClient.DownloadString(url);

                        IBI.Shared.AppUtility.Log2File("CorrespondingTraffic", String.Format("[Stop Number: {0}] Response received in {1} seconds", stopNumber, DateTime.Now.Subtract(startDate).TotalSeconds) + Environment.NewLine, true);
                        Console.WriteLine(String.Format("[Stop Number: {0}] Response received in {1} seconds", stopNumber, DateTime.Now.Subtract(startDate).TotalSeconds) + Environment.NewLine);

                        System.Threading.Thread.Sleep(delay);
                    }
                    catch (Exception ex)
                    {
                        IBI.Shared.AppUtility.Log2File("CorrespondingTraffic", String.Format("[Stop Number: {0}] call took: {1}, Exception occured: {2} - {3}", stopNumber, DateTime.Now.Subtract(startDate).TotalSeconds, ex.Message, ex.StackTrace) + Environment.NewLine, true);
                        Console.WriteLine(String.Format("[Stop Number: {0}] call took: {1}, Exception occured: {2} - {3}", stopNumber, DateTime.Now.Subtract(startDate).TotalSeconds, ex.Message, ex.StackTrace) + Environment.NewLine);
                    }

                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            
           
        }
    }
}
