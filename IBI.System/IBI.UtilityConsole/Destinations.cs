﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace IBI.UtilityConsole
{
    class Destinations
    {
        /*
         * Stored Procedure Script
         CREATE PROCEDURE [dbo].[UpdateSelectionStructure] 
	        @overwrite bit = 1
        AS
        BEGIN
	        Update [IBI_DATA].[dbo].[Schedules] 
            SET SelectionStructure=case when (Line IS NULL OR Line='') then '' else ('#' + CONVERT(varchar(MAX), Line) + '/') end + case when (Destination IS NULL OR Destination='') then '' else (CONVERT(varchar(MAX), Destination)) end + case when (FromName IS NULL OR FromName='') then '' else ('/' + CONVERT(varchar(MAX), FromName)) end  + case when (ViaName IS NULL OR ViaName='') then '' else ('/' + CONVERT(varchar(MAX), ViaName)) end 
        END

         * - Add a field in Schedules 'SelectionStructure' of type varchar(MAX)
         */
        public static void UpdateDestinationStructure(bool bOverwrite)
        {
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                dbContext.UpdateSelectionStructure(true);
            }
        }

        //public static string GetDestinationTree(int customerId)
        //{
        //    string result = string.Empty;

        //    using (IBIDataModel dbContext = new IBIDataModel())
        //    {
        //        string destinationString = "<Data></Data>";
        //        XmlDocument doc = new XmlDocument();
        //        doc.LoadXml(destinationString);

        //        var schedules = dbContext.Schedules.Where(s => s.CustomerID == customerId).ToList();
        //        foreach (var schedule in schedules)
        //        {
        //            string structure = schedule.SelectionStructure;
        //            string[] path = structure.Split(new char[] { '/' });
        //            if (path.Length > 0)
        //            {
        //                XmlElement lineElement;
        //                XmlNode destinationsNode = doc.SelectSingleNode("/Data/Destinations");
        //                if (destinationsNode == null)
        //                {
        //                    XmlElement tmpDestinationElement = doc.CreateElement("Destinations");
        //                    doc.DocumentElement.AppendChild(tmpDestinationElement);
        //                    destinationsNode = doc.SelectSingleNode("/Data/Destinations");
        //                }
        //                XmlNode lineNode = doc.SelectSingleNode("/Data/Destinations/Line[@line='" + path[0].Substring(1) + "']");
        //                if (path[0].StartsWith("#"))
        //                {
        //                    // This is Line
        //                    bool makeGroup = false;
        //                    bool alreadyGroup = false;
        //                    XmlNode destinationNode = doc.SelectSingleNode("/Data/Destinations/Line[@line='" + path[0].Substring(1) + "']/Destination[@destination='" + path[1] + "']");
        //                    if (destinationNode != null)
        //                    {
        //                        makeGroup = true;
        //                    }

        //                    XmlNode groupNode = doc.SelectSingleNode("/Data/Destinations/Line[@line='" + path[0].Substring(1) + "']/Group[@name='" + path[1] + "']");
        //                    if (groupNode != null)
        //                    {
        //                        alreadyGroup = true;
        //                    }

        //                    // Create Destination element
        //                    XmlElement destinationElement = doc.CreateElement("Destination");
        //                    XmlAttribute attLine = doc.CreateAttribute("line");
        //                    attLine.Value = path[0].Substring(1);
        //                    XmlAttribute attDestination = doc.CreateAttribute("destination");
        //                    attDestination.Value = path[1];
        //                    XmlAttribute attSchedule = doc.CreateAttribute("schedule");
        //                    attSchedule.Value = schedule.ScheduleID.ToString();

        //                    destinationElement.Attributes.Append(attLine);
        //                    destinationElement.Attributes.Append(attDestination);
        //                    destinationElement.Attributes.Append(attSchedule);

        //                    destinationElement.InnerText = path[path.Length-1];

        //                    // end
        //                    if (!makeGroup && !alreadyGroup)
        //                    {
        //                        // insert new destination
        //                        if (lineNode != null)
        //                        {
        //                            lineNode.AppendChild(destinationElement);
        //                        }
        //                        else
        //                        {
        //                            lineElement = doc.CreateElement("Line");
        //                            XmlAttribute line = doc.CreateAttribute("line");
        //                            line.Value = path[0].Substring(1);
        //                            lineElement.Attributes.Append(line);

        //                            lineElement.AppendChild(destinationElement);
        //                            destinationsNode.AppendChild(lineElement);
        //                        }
        //                        continue;
        //                    }

        //                    if (alreadyGroup)
        //                    {
        //                        groupNode.AppendChild(destinationElement);
        //                    }

        //                    if (makeGroup)
        //                    {
        //                        XmlElement groupElement = doc.CreateElement("Group");
        //                        XmlAttribute groupNameAtt = doc.CreateAttribute("name");
        //                        groupNameAtt.Value = path[1];
        //                        groupElement.Attributes.Append(groupNameAtt);

        //                        groupElement.AppendChild(destinationNode);
        //                        groupElement.AppendChild(destinationElement);

        //                        if (lineNode != null)
        //                        {
        //                            lineNode.AppendChild(groupElement);
        //                        }
                                
        //                    }
                             
        //                }
                        
        //            }
        //        }
        //        result = doc.OuterXml;
        //    }

        //    return result;
        //}
    }
}
