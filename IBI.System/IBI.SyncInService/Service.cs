﻿using System;
using System.Diagnostics;
using System.Timers;
using IBI.Service.Implementation;
using System.Collections;
using IBI.Shared.ConfigSections;

namespace IBI.SyncInService
{
    public class Service : IDisposable
    {
        private Timer InServiceSyncTimer;
       

        private Synchronizer synchronizer = new Synchronizer();

        #region Properties
         

        #endregion


        public Service()
        {
            this.InServiceSyncTimer = new Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
            this.InServiceSyncTimer.Elapsed += new ElapsedEventHandler(InServiceSyncTimer_Elapsed);
            this.InServiceSyncTimer.Enabled = true; 
             
        }
          
        private void InServiceSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.InServiceSyncTimer.Enabled = false;
            try
            {
                if (AppSettings.InServiceSyncEnabled())
                {
                    synchronizer.SyncAllInService();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                Dispose();

            }
            finally
            {
                this.InServiceSyncTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
                this.InServiceSyncTimer.Enabled = true;
            }
        }
         
        private void LogError(Exception ex)
        {
            try
            {
                String message = "";
                Exception exception = ex;

                while (exception != null)
                {
                    message += exception.Message + Environment.NewLine;
                    message += exception.StackTrace + Environment.NewLine;
                    message += "--" + Environment.NewLine;

                    exception = exception.InnerException;
                }

                EventLog log = new EventLog();
                log.Log = "Application";
                log.Source = "IBI SyncInService";
                log.WriteEntry(message, EventLogEntryType.Error);
            }
            catch (Exception)
            {
                // SILENT
            }
        }

        public void Dispose()
        {
            this.InServiceSyncTimer.Enabled = false;
            this.InServiceSyncTimer.Dispose();
 
        }
    }
}
