﻿Public Class Encoding

    Public Shared Function XMLEncode(ByVal text As String) As String
        Dim tmpResult As String = text

        If String.IsNullOrEmpty(tmpResult) Then tmpResult = String.Empty

        tmpResult = tmpResult.Replace("&", "&amp;")
        tmpResult = tmpResult.Replace("<", "&lt;")
        tmpResult = tmpResult.Replace(">", "&gt;")
        tmpResult = tmpResult.Replace("""", "&quot;")
        tmpResult = tmpResult.Replace("'", "&apos;")

        Return tmpResult

    End Function

    Public Shared Function XMLUTF32Encode(ByVal text As String) As String
        If Not String.IsNullOrEmpty(text) Then
            Dim tmpResult As String = String.Empty

            Dim arr As Char() = text.ToCharArray

            For i As Integer = 0 To arr.GetUpperBound(0)
                tmpResult &= "&#" & Char.ConvertToUtf32(arr(i), 0) & ";"

            Next

            Return tmpResult

        End If

        Return String.Empty

    End Function

    Public Shared Function XMLDecode(ByVal text As String) As String
        Dim tmpResult As String = Text

        If String.IsNullOrEmpty(tmpResult) Then tmpResult = String.Empty

        tmpResult = tmpResult.Replace("&amp;", "&")
        tmpResult = tmpResult.Replace("&lt;", "<")
        tmpResult = tmpResult.Replace("&gt;", ">")
        tmpResult = tmpResult.Replace("&quot;", """")
        tmpResult = tmpResult.Replace("&apos;", "'")

        Return tmpResult

    End Function

End Class
