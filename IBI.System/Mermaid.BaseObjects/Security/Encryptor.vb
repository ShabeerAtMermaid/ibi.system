Imports System.Security.Cryptography
Imports System.Text
Imports System.IO

Namespace Security

    Public Class Encryptor

#Region "Attributes"

        Private Shared IVValue() As Byte = {&H78, &HEF, &H12, &H56, &HCD, &H90, &HAB, &H34}

#End Region

#Region "Properties"

        Private Shared ReadOnly Property IV() As Byte()
            Get
                Return IVValue

            End Get
        End Property

#End Region

        Public Shared Function Encrypt(ByVal text As String, ByVal key As String) As String
            Dim ms As MemoryStream = Nothing
            Dim cs As CryptoStream = Nothing

            Try
                If Not key.Length = 8 Then Throw New Exception("Key must be 8 characters!")

                If Not text.StartsWith("[ENCRYPTED]") Then
                    Dim byKey() As Byte

                    byKey = System.Text.Encoding.UTF8.GetBytes(Left(key, 8))

                    Dim des As New DESCryptoServiceProvider
                    Dim inputByteArray() As Byte = System.Text.Encoding.UTF8.GetBytes(text)
                    ms = New MemoryStream
                    cs = New CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write)

                    cs.Write(inputByteArray, 0, inputByteArray.Length)
                    cs.FlushFinalBlock()

                    Dim encryptedString As String = Convert.ToBase64String(ms.ToArray())

                    Return "[ENCRYPTED]" & encryptedString

                Else
                    Return text

                End If

            Catch ex As Exception
                Return text

            Finally
                Try
                    If Not ms Is Nothing Then ms.Close()
                    If Not cs Is Nothing Then cs.Close()

                Catch ex As Exception

                End Try
            End Try
        End Function

        Public Shared Function Decrypt(ByVal text As String, ByVal key As String) As String
            Dim ms As MemoryStream = Nothing
            Dim cs As CryptoStream = Nothing

            Try
                If text = String.Empty Or text Is Nothing Then
                    Return text

                End If

                If Not key.Length = 8 Then Throw New Exception("Key must be 8 characters!")

                text = text.Replace("[ENCRYPTED]", "")

                Dim byKey() As Byte
                Dim inputByteArray(text.Length) As Byte

                byKey = System.Text.Encoding.UTF8.GetBytes(Left(key, 8))
                Dim des As New DESCryptoServiceProvider
                inputByteArray = Convert.FromBase64String(text)

                ms = New MemoryStream
                cs = New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

                cs.Write(inputByteArray, 0, inputByteArray.Length)
                cs.FlushFinalBlock()

                Return System.Text.Encoding.UTF8.GetString(ms.ToArray())

            Catch ex As Exception
                Return text

            Finally
                Try
                    If Not ms Is Nothing Then ms.Close()
                    If Not cs Is Nothing Then cs.Close()

                Catch ex As Exception

                End Try
            End Try
        End Function

    End Class

End Namespace