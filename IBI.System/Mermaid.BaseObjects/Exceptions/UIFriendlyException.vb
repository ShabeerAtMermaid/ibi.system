﻿Namespace Exceptions

    <System.Serializable()> Public Class UIFriendlyException
        Inherits System.ApplicationException

#Region "Variables"

        Private _ErrorCode As String

#End Region

#Region "Properties"

        Public ReadOnly Property ErrorCode() As String
            Get
                Return Me._ErrorCode

            End Get
        End Property

#End Region

        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub

        Public Sub New(ByVal message As String, ByVal errorCode As String)
            MyBase.New(message)

            Me._ErrorCode = errorCode

        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

        End Sub

        Public Sub New(ByVal message As String, ByVal errorCode As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

            Me._ErrorCode = errorCode

        End Sub

        Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)

        End Sub

        Public Shared Function FindUIFriendlyException(ByVal exception As Exception) As UIFriendlyException
            Dim tmpException As Exception = exception

            While tmpException IsNot Nothing
                If TypeOf tmpException Is UIFriendlyException Then
                    Return tmpException
                Else
                    tmpException = tmpException.InnerException
                End If
            End While

            Return Nothing

        End Function

    End Class

End Namespace