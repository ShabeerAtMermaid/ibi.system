Namespace Exceptions

    <System.Serializable()> Public Class PersistenceLayerNotReadyException
        Inherits System.ApplicationException

#Region "Variables"

        Private _Layer As Persistence.PersistenceLayer
        Private _WillTryAgain As Boolean
        Private _NextTryIn As TimeSpan

#End Region

#Region "Properties"

        Public ReadOnly Property Layer() As Persistence.PersistenceLayer
            Get
                Return Me._Layer

            End Get
        End Property

        Public ReadOnly Property WillTryAgain() As Boolean
            Get
                Return Me._WillTryAgain

            End Get
        End Property

        Public ReadOnly Property NextTryIn() As TimeSpan
            Get
                Return Me._NextTryIn

            End Get
        End Property

        Public Overrides ReadOnly Property Message() As String
            Get
                Dim sb As New Text.StringBuilder

                sb.Append("PersistenceLayer not ready." & vbCrLf)
                sb.Append("PersistenceLayer: ")
                If Not Me.Layer Is Nothing Then sb.Append(Me.Layer.Key)
                sb.Append(vbCrLf)

                If Me.WillTryAgain Then
                    sb.Append("Another connection attempt will be performed in " & Me.NextTryIn.TotalSeconds & " seconds.")

                Else
                    sb.Append("No more connection attempts will be performed.")

                End If

                Return sb.ToString

            End Get
        End Property

#End Region

#Region "Fields"

        Private Shared DefaultErrorMessage As String = "PersistenceLayer is not ready."

#End Region

        Public Sub New()
            MyBase.New(DefaultErrorMessage)

        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub

        Public Sub New(ByVal layer As Persistence.PersistenceLayer, ByVal willTryAgain As Boolean, ByVal nextTryIn As TimeSpan)
            MyBase.New(DefaultErrorMessage)

            Me._Layer = layer
            Me._WillTryAgain = willTryAgain
            Me._NextTryIn = nextTryIn

        End Sub

        Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)

        End Sub

    End Class

End Namespace