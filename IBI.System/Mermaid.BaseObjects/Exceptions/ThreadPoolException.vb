Imports mermaid.BaseObjects.Threading

Namespace Exceptions

    Public Class ThreadPoolException
        Inherits ApplicationException

#Region "Variables"

        Private _Threadpool As ThreadPool

#End Region

#Region "Properties"

        Public ReadOnly Property Threadpool() As ThreadPool
            Get
                Return Me._Threadpool

            End Get
        End Property

#End Region

        Public Sub New(ByVal threadpool As ThreadPool, ByVal message As String)
            MyBase.New(message)

            Me._Threadpool = threadpool

        End Sub

        Public Sub New(ByVal threadpool As ThreadPool, ByVal innerException As Exception)
            MyBase.New("ThreadPoolException occurred", innerException)

            Me._Threadpool = threadpool

        End Sub

        Public Sub New(ByVal threadpool As ThreadPool, ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

            Me._Threadpool = threadpool

        End Sub

    End Class

End Namespace