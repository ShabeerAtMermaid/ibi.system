Namespace Exceptions

    <System.Serializable()> Public Class DownloadAbortedException
        Inherits System.ApplicationException

#Region "Fields"

        Private Shared DefaultErrorMessage As String = "Download has been aborted."

#End Region

        Public Sub New()
            MyBase.New(DefaultErrorMessage)

        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub

        Public Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)

        End Sub

    End Class

End Namespace