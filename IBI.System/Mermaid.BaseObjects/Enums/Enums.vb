Namespace Enums

    Public Enum HashTableChangedTypes As Integer
        ItemAdded = 1
        ItemRemoved = 2
        ItemChanged = 4
        Reset = 8

    End Enum

    Public Enum ArrayListChangedTypes As Integer
        ItemAdded = 1
        ItemRemoved = 2
        ItemChanged = 4
        ItemMoved = 8
        Reset = 16

    End Enum

    Public Enum QueryComparer As Integer
        None = 0
        LessThan = 1
        GreaterThan = 2
        Equals = 4

    End Enum

    Public Enum CloneLevels As Integer
        Simple = 1
        Shallow = 2
        AlmostDeep = 4
        Deep = 8

    End Enum

    Public Enum FixedLengthConversionTypes As Integer
        PrefixFillChar = 0
        PostfixFillChar = 1

    End Enum

    Public Enum SortDirections As Integer
        Descending = -1
        Ascending = 1

    End Enum

    Public Enum FontAlignmentTypes As Integer
        Left = 1
        Center = 2
        Right = 3

    End Enum

    <Flags()> Public Enum ExceptionCategories As Integer
        None = 0
        Misc = 1
        Threading = 2
        Persistence = 4
        Sockets = 8
        PerformanceCounters = 16

    End Enum

End Namespace