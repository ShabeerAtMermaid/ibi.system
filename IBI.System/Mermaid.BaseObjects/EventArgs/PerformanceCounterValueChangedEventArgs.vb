﻿Namespace EventArgs

    Public Class PerformanceCounterValueChangedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _PerformanceCounter As Diagnostics.PerformanceCounter
        Private _Value As Long

#End Region

#Region "Properties"

        Public ReadOnly Property PerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Me._PerformanceCounter

            End Get
        End Property

        Public ReadOnly Property Value() As Long
            Get
                Return Me._Value

            End Get
        End Property

#End Region

        Public Sub New(ByVal performanceCounter As Diagnostics.PerformanceCounter, ByVal value As Long)
            MyBase.New()

            Me._PerformanceCounter = performanceCounter
            Me._Value = value

        End Sub

    End Class

End Namespace