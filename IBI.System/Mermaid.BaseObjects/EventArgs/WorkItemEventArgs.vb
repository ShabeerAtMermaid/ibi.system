﻿Imports mermaid.BaseObjects.Threading

Namespace EventArgs

    Public Class WorkItemEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _WorkItem As WorkItem

#End Region

#Region "Properties"

        Public ReadOnly Property WorkItem() As WorkItem
            Get
                Return Me._WorkItem

            End Get
        End Property

#End Region

        Public Sub New(ByVal workItem As WorkItem)
            MyBase.New()

            Me._WorkItem = workItem

        End Sub

    End Class

End Namespace