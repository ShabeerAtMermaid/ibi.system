﻿Namespace EventArgs

    Public Class ExceptionEventArgs
        Inherits System.EventArgs

#Region "Attributes"

        Private _ExceptionSource As String
        Private _Exception As Exception
        Private _Remarks As String
        Private _Category As Enums.ExceptionCategories

#End Region

#Region "Properties"

        Public ReadOnly Property ExceptionSource As String
            Get
                Return Me._ExceptionSource

            End Get
        End Property

        Public ReadOnly Property Exception() As Exception
            Get
                Return Me._Exception

            End Get
        End Property

        Public ReadOnly Property Remarks() As String
            Get
                Return Me._Remarks

            End Get
        End Property

        Public ReadOnly Property Category() As Enums.ExceptionCategories
            Get
                Return Me._Category

            End Get
        End Property

#End Region

        Public Sub New(ByVal exception As Exception, ByVal category As Enums.ExceptionCategories)
            Me._Exception = exception
            Me._Category = category
            Me._Remarks = String.Empty

            Me.SetExceptionSource()

        End Sub

        Public Sub New(ByVal exception As Exception, ByVal category As Enums.ExceptionCategories, ByVal remarks As String)
            Me._Exception = exception
            Me._Category = category
            Me._Remarks = remarks

            Me.SetExceptionSource()

        End Sub

        Private Sub SetExceptionSource()
            Try
                Dim tmpStackLines() As String = System.Environment.StackTrace.Split(New Char() {Environment.NewLine})
                Dim tmpStackLine As String = tmpStackLines(3) 'Has been 3 until now??

                tmpStackLine = tmpStackLine.Substring(0, tmpStackLine.IndexOf("("))
                tmpStackLine = tmpStackLine.Substring(tmpStackLine.LastIndexOf(" ") + 1)

                Me._ExceptionSource = tmpStackLine

            Catch ex As Exception
                Me._ExceptionSource = "UNKNOWN"

            End Try
        End Sub

    End Class

End Namespace