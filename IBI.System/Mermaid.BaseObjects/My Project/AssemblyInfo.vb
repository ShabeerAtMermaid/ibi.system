﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Mermaid BaseObjects")> 
<Assembly: AssemblyDescription("Common Objects used in applications by Mermaid")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ab8170ac-5bd9-49af-a0bc-711fa3d2a9bf")> 

<Assembly: AssemblyVersion("4.4.2.0")> 
<Assembly: AssemblyFileVersion("4.4.2.0")> 