Imports System.Runtime.Remoting.Lifetime
Imports mermaid.BaseObjects.Interfaces

Namespace Remoting

    Public MustInherit Class MBRObject
        Inherits MarshalByRefObject
        Implements IDisposable, IMBRObject

#Region "Attributes"

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Protected Friend Overridable Property IsDisposed() As Boolean Implements IMBRObject.IsDisposed
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal Value As Boolean)
                Me._IsDisposed = Value

            End Set
        End Property

#End Region

#Region "Events"

        Public Event Disposing As System.EventHandler Implements IMBRObject.Disposing

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Controls the lifetime of the remote object.
        ''' </summary>
        ''' <returns>Returns the lease of the remote object.</returns>
        ''' <remarks>
        '''     This remote object is controlled by a sponsor, and will live untill this is unregistered.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Overrides Function InitializeLifeTimeService() As Object
            Dim tmpLease As ILease = Nothing

            If Not MyBase.InitializeLifetimeService Is Nothing Then
                tmpLease = CType(MyBase.InitializeLifetimeService, ILease)

                If Not tmpLease Is Nothing Then
                    tmpLease.Register(CType(MBRSponsorResources.MBRSponsor, ISponsor))

                End If
            End If

            Return tmpLease

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Deletes the remote reference to this object.
        ''' </summary>
        ''' <remarks>
        '''     Unregisters the sponsor controlling this objects lifetime.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Protected Overridable Sub KillRemoteReferences()
            If Not MyBase.GetLifetimeService Is Nothing Then
                Dim tmpLease As ILease = CType(MyBase.GetLifetimeService, ILease)

                If Not tmpLease Is Nothing Then
                    tmpLease.Unregister(CType(MBRSponsorResources.MBRSponsor, ISponsor))

                End If
            End If

        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Calls KillRemoteReferences.
        ''' </summary>
        ''' <remarks>
        '''     If overridden, remember to call MyBase.Dispose().
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Overridable Sub Dispose() Implements System.IDisposable.Dispose
            If Not Me.IsDisposed Then
                Me.IsDisposed = True

                Me.KillRemoteReferences()

                Me.OnDisposing()

            End If

        End Sub

        Protected Overridable Sub OnDisposing()
            RaiseEvent Disposing(Me, New System.EventArgs)

        End Sub

    End Class

End Namespace