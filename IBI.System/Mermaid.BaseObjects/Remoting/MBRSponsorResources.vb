Namespace Remoting

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Class containing the ISponsor of the remote objects in the application
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[peter]	18-11-2004	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Class MBRSponsorResources

        Private Shared MBRSponsorValue As MBRSponsor

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Shared property to retrieve the ISponsor.
        ''' </summary>
        ''' <value>ISponsor of the remote objects in the application.</value>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[peter]	15-02-2004	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Shared ReadOnly Property MBRSponsor() As MBRSponsor
            Get
                If MBRSponsorValue Is Nothing Then
                    MBRSponsorValue = New MBRSponsor

                End If

                Return MBRSponsorValue

            End Get
        End Property

        Public Shared Sub RegisterCustomSponsor(ByVal sponsor As MBRSponsor)
            MBRSponsorValue = sponsor

        End Sub

    End Class

End Namespace