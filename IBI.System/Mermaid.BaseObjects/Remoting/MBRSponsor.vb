Imports System.Runtime.Remoting.Lifetime

Namespace Remoting

    ''' -----------------------------------------------------------------------------
    ''' Project	 : VictorFilm.BaseObjects
    ''' Class	 : BaseObjects.Remoting.MBRKeepAliveSponsor
    ''' 
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''      ISponsor controlling the lifeTime of the remote objects.
    ''' </summary>
    ''' <remarks>
    '''     All remote objects who has registered this sponsor will be kept alive.
    ''' </remarks>
    ''' <history>
    ''' 	[peter]	18-11-2004	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Class MBRSponsor
        Inherits MarshalByRefObject
        Implements ISponsor

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Constructor creating a new Sponsor.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[peter]	15-02-2004	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub New()

        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Renews the lease by five minutes.
        ''' </summary>
        ''' <param name="lease">ILease to be renewed.</param>
        ''' <returns>Returns a TimeSpan of five minutes.</returns>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[peter]	15-02-2004	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Overridable Function Renewal(ByVal lease As System.Runtime.Remoting.Lifetime.ILease) As System.TimeSpan Implements System.Runtime.Remoting.Lifetime.ISponsor.Renewal
            Return TimeSpan.FromMinutes(5)

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Lifetime manager for the sponsor. 
        ''' </summary>
        ''' <returns>Nothing</returns>
        ''' <remarks>
        '''     The sponsor will always stay alive
        ''' </remarks>
        ''' <history>
        ''' 	[peter]	13-02-2004	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Overrides Function InitializeLifeTimeService() As Object
            Return Nothing

        End Function

    End Class

End Namespace