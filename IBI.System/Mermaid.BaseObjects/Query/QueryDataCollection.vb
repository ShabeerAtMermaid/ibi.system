﻿Namespace Query

    <System.Serializable()> Public Class QueryDataCollection
        Inherits System.Collections.Generic.List(Of QueryData)

#Region "Variables"

        Private _QueryFilters As QueryFilterCollection

#End Region

#Region "Properties"

        Public ReadOnly Property QueryFilters() As QueryFilterCollection
            Get
                Return Me._QueryFilters

            End Get
        End Property

#End Region

        Public Sub New()
            MyBase.New()

            Me._QueryFilters = New QueryFilterCollection

        End Sub

    End Class

End Namespace