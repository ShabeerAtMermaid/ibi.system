﻿Namespace Query

    <System.Serializable()> Public Class QueryFilterCollection
        Inherits System.Collections.Generic.List(Of QueryFilter)

    End Class

End Namespace