﻿'Imports System.Reflection
'Imports System.ComponentModel

'Namespace Query

'    Friend Class QueryEngine
'        Implements IDisposable

'#Region "Variables"

'        Private _QueryObject As Interfaces.IBaseObject

'#End Region

'#Region "Properties"

'        Private Property QueryObject() As Interfaces.IBaseObject
'            Get
'                Return Me._QueryObject

'            End Get
'            Set(ByVal value As Interfaces.IBaseObject)
'                Me._QueryObject = value

'            End Set
'        End Property

'        'Private ReadOnly Property TypeName() As String
'        '    Get
'        '        Dim tmpTypedCollectionAttribute As TypedCollectionAttribute = TypeDescriptor.GetAttributes(Me.Collection).Item(GetType(TypedCollectionAttribute))

'        '        If Not tmpTypedCollectionAttribute Is Nothing Then
'        '            Dim tmpTypeName As String = tmpTypedCollectionAttribute.CollectionType.Name

'        '            If tmpTypeName.EndsWith("Base") Then
'        '                tmpTypeName = tmpTypeName.Substring(0, tmpTypeName.Length - 4)

'        '            End If

'        '            Return tmpTypeName

'        '        Else
'        '            Return Me.Collection.GetType.Name

'        '        End If
'        '    End Get
'        'End Property

'#End Region

'        Public Sub New(ByVal queryObject As Interfaces.IBaseObject)
'            MyBase.New()

'            Me.QueryObject = queryObject

'        End Sub

'        Public Function PerformQuery(ByVal data As QueryDataCollection) As DataSet
'            Dim tmpDataSet As New DataSet("QueryResult")
'            Dim tmpRefTable As DataTable = tmpDataSet.Tables.Add(Me.QueryObject.GetType.Name & "_CollectionREF")
'            Dim tmpObjTable As DataTable = tmpDataSet.Tables.Add(Me.QueryObject)

'            tmpRefTable.Columns.Add("Container", GetType(Integer))
'            tmpRefTable.Columns.Add("InstanceId", GetType(Integer))

'            Dim tmpRefRow As DataRow = Nothing
'            Dim tmpObjRow As DataRow = Nothing

'            For Each tmpValue As Object In Me.Collection
'                Dim tmpIsFiltered As Boolean = True

'                Dim tmpPropertyDescriptors As PropertyDescriptorCollection = TypeDescriptor.GetProperties(tmpValue)

'                If data.QueryFilters.Count > 0 Then
'                    For Each tmpFilter As QueryFilter In data.QueryFilters
'                        Dim tmpFilterProperty As Object = Me.GetPropertyValue(tmpValue, tmpPropertyDescriptors, tmpFilter.PropertyName)

'                        Select Case tmpFilter.Comparer
'                            Case QueryFilter.FilterComparers.Equals
'                                If Not IsDirty(tmpFilterProperty, tmpFilter.FilterValue) Then
'                                    tmpIsFiltered = False

'                                End If

'                            Case QueryFilter.FilterComparers.Differs
'                                If IsDirty(tmpFilterProperty, tmpFilter.FilterValue) Then
'                                    tmpIsFiltered = False

'                                End If

'                        End Select

'                    Next

'                Else
'                    tmpIsFiltered = False

'                End If

'                If Not tmpIsFiltered Then
'                    tmpRefRow = tmpRefTable.NewRow
'                    tmpRefRow("Container") = Me.Collection.InstanceId
'                    tmpRefRow("InstanceId") = tmpValue.InstanceId

'                    tmpRefTable.Rows.Add(tmpRefRow)

'                    tmpObjRow = tmpObjTable.NewRow

'                    For Each tmpData As QueryData In data
'                        Dim tmpPropertyName As String = tmpData.PropertyName
'                        Dim tmpPropertyValue As Object = Nothing

'                        If tmpData.QueryData Is Nothing Then
'                            tmpPropertyValue = Me.GetPropertyValue(tmpValue, tmpPropertyDescriptors, tmpPropertyName)

'                            If Not tmpPropertyValue Is Nothing Then
'                                If Not tmpObjTable.Columns.Contains(tmpData.DisplayName) Then tmpObjTable.Columns.Add(tmpData.DisplayName, tmpPropertyValue.GetType)

'                                tmpObjRow(tmpData.DisplayName) = tmpPropertyValue

'                            End If
'                        Else
'                            tmpPropertyValue = Me.GetPropertyValue(tmpValue, tmpPropertyDescriptors, tmpPropertyName)

'                            If Not tmpPropertyValue Is Nothing Then
'                                Dim tmpResult As DataSet = tmpPropertyValue.PerformQuery(tmpData.QueryData)

'                                For Each tmpTable As DataTable In tmpResult.Tables
'                                    tmpDataSet.Merge(tmpTable)

'                                Next

'                            End If
'                        End If

'                    Next

'                    tmpObjTable.Rows.Add(tmpObjRow)

'                End If

'            Next

'            Return tmpDataSet

'        End Function

'        Private Function GetPropertyValue(ByVal value As Object, ByVal propertyDescriptors As PropertyDescriptorCollection, ByVal propertyName As String) As Object
'            Dim tmpPropertyNames() As String = propertyName.Split(New String() {"."}, StringSplitOptions.RemoveEmptyEntries)

'            If tmpPropertyNames.Length = 1 Then
'                Dim tmpPropertyDescriptor As PropertyDescriptor = propertyDescriptors(propertyName)

'                If Not tmpPropertyDescriptor Is Nothing Then
'                    Return tmpPropertyDescriptor.GetValue(value)

'                End If
'            ElseIf tmpPropertyNames.Length > 1 Then
'                Dim tmpFirstPropertyValue As Object = Me.GetPropertyValue(value, propertyDescriptors, tmpPropertyNames(0))

'                If Not tmpFirstPropertyValue Is Nothing Then
'                    Dim tmpPropertyDescriptors As PropertyDescriptorCollection = TypeDescriptor.GetProperties(tmpFirstPropertyValue)

'                    Return Me.GetPropertyValue(tmpFirstPropertyValue, tmpPropertyDescriptors, propertyName.Substring(tmpPropertyNames(0).Length + 1))

'                End If
'            End If

'            Return Nothing

'        End Function

'        Public Sub Dispose() Implements IDisposable.Dispose
'            Me.QueryObject = Nothing

'        End Sub

'    End Class

'End Namespace