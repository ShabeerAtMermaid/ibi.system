﻿Namespace Query

    <System.Serializable()> Public Class QueryFilter

#Region "Variables"

        Private _PropertyName As String
        Private _Comparer As FilterComparers
        Private _FilterValue As Object

#End Region

#Region "Properties"

        Public Property PropertyName() As String
            Get
                Return Me._PropertyName

            End Get
            Set(ByVal value As String)
                Me._PropertyName = value

            End Set
        End Property

        Public Property Comparer() As FilterComparers
            Get
                Return Me._Comparer

            End Get
            Set(ByVal value As FilterComparers)
                Me._Comparer = value

            End Set
        End Property

        Public Property FilterValue() As Object
            Get
                Return Me._FilterValue

            End Get
            Set(ByVal value As Object)
                Me._FilterValue = value

            End Set
        End Property

#End Region

#Region "Enums"

        Public Enum FilterComparers As Integer
            Equals = 1
            Differs = 2

        End Enum

#End Region

        Public Sub New(ByVal propertyName As String, ByVal comparer As FilterComparers, ByVal value As Object)
            MyBase.New()

            Me.PropertyName = propertyName
            Me.Comparer = comparer
            Me.FilterValue = value

        End Sub

    End Class

End Namespace