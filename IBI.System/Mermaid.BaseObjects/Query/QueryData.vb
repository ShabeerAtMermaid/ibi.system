﻿Namespace Query

    <System.Serializable()> Public Class QueryData

#Region "Variables"

        Private _PropertyName As String
        Private _DisplayName As String
        Private _QueryData As QueryDataCollection

#End Region

#Region "Properties"

        Public ReadOnly Property PropertyName() As String
            Get
                Return Me._PropertyName

            End Get
        End Property

        Public ReadOnly Property DisplayName() As String
            Get
                Return Me._DisplayName

            End Get
        End Property

        Public ReadOnly Property QueryData() As QueryDataCollection
            Get
                Return Me._QueryData

            End Get
        End Property

#End Region

#Region "Constants"

        'Private Const TOPLEVELREFERENCE_IDENTIFIER As String = "TOPLEVELREFERENCE_IDENTIFIER"

        'Public Shared ROOTQUERY As QueryData = New QueryData(TOPLEVELREFERENCE_IDENTIFIER)

#End Region

        Public Sub New(ByVal propertyName As String)
            Me.New(propertyName, propertyName, Nothing)

        End Sub

        Public Sub New(ByVal propertyName As String, ByVal displayName As String)
            Me.New(propertyName, displayName, Nothing)

        End Sub

        Public Sub New(ByVal propertyName As String, ByVal queryData As QueryDataCollection)
            Me.New(propertyName, propertyName, queryData)

        End Sub

        Public Sub New(ByVal propertyName As String, ByVal displayName As String, ByVal queryData As QueryDataCollection)
            MyBase.New()

            Me._PropertyName = propertyName
            Me._DisplayName = displayName
            Me._QueryData = queryData

        End Sub

    End Class

End Namespace