'Imports mermaid.BaseObjects.Persistence.Interfaces
'Imports mermaid.BaseObjects.Threading
'Imports System.Threading

'Namespace Management

'    Public Class LockManager

'#Region "Variables"

'        Private Shared _LockDataList As ArrayList
'        Private Shared _Lock As Object
'        Private Shared _LogCallerStack As Boolean

'#End Region

'#Region "Properties"

'        Private Shared ReadOnly Property LockDataList() As ArrayList
'            Get
'                If _LockDataList Is Nothing Then
'                    _LockDataList = New ArrayList

'                End If

'                Return _LockDataList

'            End Get
'        End Property

'        Private Shared ReadOnly Property Lock() As Object
'            Get
'                If _Lock Is Nothing Then
'                    _Lock = New Object

'                End If

'                Return _Lock

'            End Get
'        End Property

'        Public Shared Property LogCallerStack() As Boolean
'            Get
'                Return _LogCallerStack

'            End Get
'            Set(ByVal value As Boolean)
'                _LogCallerStack = value

'            End Set
'        End Property

'#End Region

'        Public Shared Sub LockEntered(ByVal collection As Persistence.Collections.PersistentArrayListCollection)
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpLockData As LockData = GetLockData(collection)

'                If tmpLockData Is Nothing Then
'                    LockDataList.Add(New LockData(collection, LogCallerStack))

'                End If

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Sub LockEntered(ByVal threadpool As BaseObjects.Threading.ThreadPool)
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpLockData As LockData = GetLockData(threadpool)

'                If tmpLockData Is Nothing Then
'                    LockDataList.Add(New LockData(threadpool, LogCallerStack))

'                End If

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Sub LockExited(ByVal collection As Persistence.Collections.PersistentArrayListCollection)
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpLockData As LockData = GetLockData(collection)

'                If Not tmpLockData Is Nothing Then
'                    LockDataList.Remove(tmpLockData)

'                End If

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Sub LockExited(ByVal threadpool As BaseObjects.Threading.ThreadPool)
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpLockData As LockData = GetLockData(threadpool)

'                If Not tmpLockData Is Nothing Then
'                    LockDataList.Remove(tmpLockData)

'                End If

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Sub ReleaseCollectionLock(ByVal collectionID As Integer)
'            Monitor.Enter(Lock)

'            Try
'                For Each tmpLockData As LockData In LockDataList
'                    If Not tmpLockData.Collection Is Nothing Then
'                        If tmpLockData.Collection.InstanceID = collectionID Then
'                            tmpLockData.Collection.ReleaseLock()

'                        End If
'                    End If

'                Next

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Sub ReleaseThreadpoolLock(ByVal threadpoolName As String)
'            Monitor.Enter(Lock)

'            Try
'                For Each tmpLockData As LockData In LockDataList
'                    If Not tmpLockData.ThreadPool Is Nothing Then
'                        If tmpLockData.ThreadPool.Name.ToLower = threadpoolName.ToLower Then
'                            Throw New NotImplementedException()

'                        End If
'                    End If

'                Next

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        Public Shared Function GetLockedCollections() As LockData()
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpList As New ArrayList

'                For i As Integer = 0 To LockDataList.Count - 1
'                    If Not CType(LockDataList(i), LockData).Collection Is Nothing Then
'                        tmpList.Add(CType(LockDataList(i), LockData))

'                    End If

'                Next

'                Return tmpList.ToArray(GetType(LockData))

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Function

'        Public Shared Function GetLockedThreadpools() As LockData()
'            Monitor.Enter(Lock)

'            Try
'                Dim tmpList As New ArrayList

'                For i As Integer = 0 To LockDataList.Count - 1
'                    If Not CType(LockDataList(i), LockData).ThreadPool Is Nothing Then
'                        tmpList.Add(CType(LockDataList(i), LockData))

'                    End If

'                Next

'                Return tmpList.ToArray(GetType(LockData))

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Function

'        Private Shared Function GetLockData(ByVal collection As Persistence.Collections.PersistentArrayListCollection) As LockData
'            For i As Integer = 0 To LockDataList.Count - 1
'                Dim tmpLockData As LockData = LockDataList(i)

'                If tmpLockData.Thread Is System.Threading.Thread.CurrentThread And tmpLockData.Collection Is collection Then
'                    Return tmpLockData

'                End If
'            Next

'            Return Nothing

'        End Function

'        Private Shared Function GetLockData(ByVal threadpool As BaseObjects.Threading.ThreadPool) As LockData
'            For i As Integer = 0 To LockDataList.Count - 1
'                Dim tmpLockData As LockData = LockDataList(i)

'                If tmpLockData.Thread Is System.Threading.Thread.CurrentThread And tmpLockData.ThreadPool Is threadpool Then
'                    Return tmpLockData

'                End If
'            Next

'            Return Nothing

'        End Function

'    End Class

'End Namespace