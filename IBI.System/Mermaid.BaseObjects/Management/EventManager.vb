Imports mermaid.BaseObjects.Threading

Namespace Management

    Public Class EventManager

#Region "Attributes"

        'Private Shared _ListChangedEventsEnabled As Boolean
        'Private Shared _ListChangedEventsRaisedInThreads As Boolean
        
#End Region

#Region "Properties"

        'Public Shared Property ListChangedEventsEnabled() As Boolean
        '    Get
        '        Return _ListChangedEventsEnabled

        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _ListChangedEventsEnabled = Value

        '    End Set
        'End Property

        'Public Shared Property ListChangedEventsRaisedInThreads() As Boolean
        '    Get
        '        Return _ListChangedEventsRaisedInThreads

        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _ListChangedEventsRaisedInThreads = Value

        '        If _ListChangedEventsRaisedInThreads Then
        '            _ListChangedEventsEnabled = True

        '        End If

        '    End Set
        'End Property

        Public Shared ReadOnly Property EventThreadPool() As BaseObjects.Threading.ThreadPool
            Get
                Return ThreadPoolManager.GetThreadPool("EventsPool")
                
            End Get
        End Property

#End Region

    End Class

End Namespace