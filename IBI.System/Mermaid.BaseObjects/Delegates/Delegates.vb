Namespace Delegates

    Public Delegate Sub DownloadProgressHandler(ByVal sender As Object, ByVal e As EventArgs.DownloadProgressEventArgs)

End Namespace