''' <summary>
''' Converts a base data type to another base data type without throwing an Exception if an error occurs.
''' </summary>
''' <remarks>This Class differs from the CLR Convert Module, because no exceptions will be thrown when using this Class.</remarks>
Public Class Converter

#Region "Properties"

    Private Shared ReadOnly Property ExceptionPerformanceCounter() As Diagnostics.PerformanceCounter
        Get
            Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Exceptions: Converter")

        End Get
    End Property

#End Region

    ''' <summary>
    ''' Converts the value into a TimeSpan Object.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <param name="avoidNothing">Specifies whether the method can return Nothing if an error occurs.</param>
    ''' <returns>Returns the converted TimeSpan.</returns>
    ''' <remarks>If an error occurs and avoidNothing is True, a TimeSpan with 0 Ticks will be returned.</remarks>
    Public Shared Function ToTimeSpan(ByVal obj As Object, Optional ByVal avoidNothing As Boolean = True) As TimeSpan
        Try
            If TypeOf obj Is TimeSpan Then
                Return obj

            ElseIf TypeOf obj Is String Then
                If CStr(obj).ToLower.StartsWith("p") Then
                    Return System.Xml.XmlConvert.ToTimeSpan(obj)

                Else
                    Return TimeSpan.Parse(obj)

                End If
            Else
                Return TimeSpan.Parse(obj)

            End If

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            If avoidNothing Then
                Return New TimeSpan(0)

            End If

            Return Nothing

        End Try
    End Function

    Public Shared Function ToXMLDuration(ByVal value As TimeSpan) As String
        'PnYnMnDTnHnMnS
        Try
            Dim tmpResult As String = "P"

            If value.Days > 0 Then tmpResult &= value.Days & "D"

            tmpResult &= "T"
            If value.Hours > 0 Then tmpResult &= value.Hours & "H"
            If value.Minutes > 0 Then tmpResult &= value.Minutes & "M"
            If value.Seconds > 0 Then tmpResult &= value.Seconds & "S"

            Return tmpResult

        Catch ex As Exception
            Return "PT0S"

        End Try
    End Function

    Public Shared Function ToXMLDate(ByVal datetime As DateTime) As String
        'YYYY-MM-DD
        Dim tmpResult As String = ""

        tmpResult &= datetime.Year
        tmpResult &= "-"
        tmpResult &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, datetime.Month)
        tmpResult &= "-"
        tmpResult &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, datetime.Day)

        Return tmpResult

    End Function

    Public Shared Function ToXMLTime(ByVal datetime As DateTime) As String
        'hh:mm:ss
        Dim tmpResult As String = ""

        tmpResult &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, datetime.Hour)
        tmpResult &= ":"
        tmpResult &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, datetime.Minute)
        tmpResult &= ":"
        tmpResult &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, datetime.Second)

        Return tmpResult

    End Function

    Public Shared Function ToXMLDateTime(ByVal datetime As DateTime) As String
        'YYYY-MM-DDThh:mm:ss
        Dim tmpResult As String = ""

        tmpResult &= ToXMLDate(datetime)
        tmpResult &= "T"
        tmpResult &= ToXMLTime(datetime)

        Return tmpResult

    End Function

    ''' <summary>
    ''' Converts the value into an Int16.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Int16.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToInt16(ByVal obj As Object) As Short
        Try
            If TypeOf obj Is String Then
                If String.IsNullOrEmpty(obj) Then
                    Return 0

                End If
            End If

            Return Convert.ToInt16(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an Int32.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Int32.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToInt32(ByVal obj As Object) As Integer
        Try
            If TypeOf obj Is String Then
                If String.IsNullOrEmpty(obj) Then
                    Return 0

                End If
            End If

            If obj Is DBNull.Value Then
                Return 0

            Else
                Return Convert.ToInt32(obj)

            End If

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an Int64.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Int64.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToInt64(ByVal obj As Object) As Long
        Try
            If TypeOf obj Is String Then
                If String.IsNullOrEmpty(obj) Then
                    Return 0

                End If
            End If

            Return Convert.ToInt64(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an Single.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Single.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToSingle(ByVal obj As Object) As Single
        Try
            Return Convert.ToSingle(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an Double.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Double.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToDouble(ByVal obj As Object) As Double
        Try
            If TypeOf obj Is DBNull Then
                Return 0

            End If

            Return Convert.ToDouble(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an Long.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Long.</returns>
    ''' <remarks>If an error occurs, 0 is returned</remarks>
    Public Shared Function ToLong(ByVal obj As Object) As Long
        Try
            If obj Is Nothing Then
                Return 0
            End If

            If TypeOf obj Is DBNull Then
                Return 0
            End If

            Return Long.Parse(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return 0

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a String. 
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted String.</returns>
    ''' <remarks>If an error occurs, String.Empty is returned.</remarks>
    Public Overloads Shared Function ToString(ByVal obj As Object) As String
        Try
            Return Convert.ToString(obj)

        Catch ex1 As Exception
            ExceptionPerformanceCounter.Increment()

            Try
                Return obj.ToString

            Catch ex2 As Exception
                ExceptionPerformanceCounter.Increment()

                Return String.Empty

            End Try
        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a Boolean. 
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Returns the converted Boolean.</returns>
    ''' <remarks>If an error occurs, False is returned.</remarks>
    Public Shared Function ToBoolean(ByVal obj As Object) As Boolean
        Try
            If TypeOf obj Is Integer Then
                If CInt(obj) > 0 Then
                    Return True

                Else
                    Return False

                End If
            End If

            Return Convert.ToBoolean(obj)

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return False

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a Date Object.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <returns>Return the converted Date Object.</returns>
    ''' <remarks>If an error occurs, Date.Min is returned.</remarks>
    Public Shared Function ToDateTime(ByVal obj As Object) As Date
        Try
            If TypeOf obj Is String AndAlso String.IsNullOrEmpty(obj) Then
                Return Date.MinValue

            ElseIf obj Is Nothing Then
                Return Date.MinValue

            Else
                Return Convert.ToDateTime(obj)

            End If

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            Return Date.MinValue

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into an IPAddress Object.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <param name="avoidNothing">Specifies whether the method can return Nothing if an error occurs.</param>
    ''' <returns>Returns the converted IPAddress.</returns>
    ''' <remarks>If an error occurs and avoidNothing is True, an IPAddress of "127.0.0.1" will be returned.</remarks>
    Public Shared Function ToIPAddress(ByVal obj As Object, Optional ByVal avoidNothing As Boolean = True) As System.Net.IPAddress
        Try
            If TypeOf obj Is System.Net.IPAddress Then
                Return obj

            ElseIf TypeOf obj Is String Then
                Dim strings() As String = obj.Split(".")

                If strings.Length = 4 Then
                    Dim ip(3) As Byte

                    For i As Integer = 0 To strings.Length - 1
                        ip(i) = Byte.Parse(strings(i))

                    Next

                    Return New System.Net.IPAddress(ip)

                End If
            End If

            If avoidNothing Then
                Return New System.Net.IPAddress(New Byte() {Byte.Parse("127"), Byte.Parse("0"), Byte.Parse("0"), Byte.Parse("1")})

            End If

            Return Nothing

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            If avoidNothing Then
                Return New System.Net.IPAddress(New Byte() {Byte.Parse("127"), Byte.Parse("0"), Byte.Parse("0"), Byte.Parse("1")})

            End If

            Return Nothing

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a FileSize Object.
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <param name="avoidNothing">Specifies whether the method can return Nothing if an error occurs.</param>
    ''' <returns>Returns the converted FileSize.</returns>
    ''' <remarks>If an error occurs and AvoidNothing is True, a FileSize of 0 will be returned.</remarks>
    Public Shared Function ToFileSize(ByVal obj As Object, Optional ByVal avoidNothing As Boolean = True) As DataTypes.FileSize
        Try
            If TypeOf obj Is DataTypes.FileSize Then
                Return obj

            ElseIf TypeOf obj Is String Then
                Return New DataTypes.FileSize(Val(obj))

            End If

            If avoidNothing Then
                Return New DataTypes.FileSize(0)

            End If

            Return Nothing

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            If avoidNothing Then
                Return New DataTypes.FileSize(0)

            End If

            Return Nothing

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a ProgramVersion Object. 
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <param name="avoidNothing">Specifies whether the method can return Nothing if an error occurs.</param>
    ''' <returns>Returns the converted ProgramVersion.</returns>
    ''' <remarks>If an error occurs and AvoidNothing is True, a ProgramVersion of "0.0.0.0" will be returned.</remarks>
    Public Shared Function ToProgramVersion(ByVal obj As Object, Optional ByVal avoidNothing As Boolean = True) As DataTypes.ProgramVersion
        Try
            If TypeOf obj Is DataTypes.ProgramVersion Then
                Return obj

            ElseIf TypeOf obj Is String Then
                Return New DataTypes.ProgramVersion(obj)

            End If

            If avoidNothing Then
                Return New DataTypes.ProgramVersion("0.0.0.0")

            End If

            Return Nothing

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            If avoidNothing Then
                Return New DataTypes.ProgramVersion("0.0.0.0")

            End If

            Return Nothing

        End Try
    End Function

    ''' <summary>
    ''' Converts the value into a Fontos Object. 
    ''' </summary>
    ''' <param name="obj">Object to convert.</param>
    ''' <param name="avoidNothing">Specifies whether the method can return Nothing if an error occurs.</param>
    ''' <returns>Returns the converted Fontos.</returns>
    ''' <remarks>If an error occurs and AvoidNothing is True, a default Fontos will be returned.</remarks>
    Public Shared Function ToFontos(ByVal obj As Object, Optional ByVal avoidNothing As Boolean = True) As DataTypes.Fontos
        Try
            If TypeOf obj Is DataTypes.Fontos Then
                Return obj

            ElseIf TypeOf obj Is String Then
                Return New DataTypes.Fontos(CStr(obj))

            ElseIf TypeOf obj Is Drawing.Font Then
                Return New DataTypes.Fontos(CType(obj, Drawing.Font))

            End If

            If avoidNothing Then
                Return DataTypes.Fontos.Empty

            End If

            Return Nothing

        Catch ex As Exception
            ExceptionPerformanceCounter.Increment()

            If avoidNothing Then
                Return DataTypes.Fontos.Empty

            End If

            Return Nothing

        End Try
    End Function

    ''' <summary>
    ''' Converts the specified value into the specified type.
    ''' </summary>
    ''' <param name="value">Value to convert.</param>
    ''' <param name="type">Type to convert the value to.</param>
    ''' <returns>The converted value if succeeded, otherwise the value is returned in it's original form.</returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertToType(ByVal value As Object, ByVal type As Type) As Object
        If type.IsEnum Then
            Return [Enum].Parse(type, value, True)

        End If

        Select Case type.Name
            Case GetType(TimeSpan).Name
                Return Converter.ToTimeSpan(value)

            Case GetType(Int16).Name, GetType(Short).Name
                Return Converter.ToInt16(value)

            Case GetType(Int32).Name, GetType(Integer).Name
                Return Converter.ToInt32(value)

            Case GetType(Int64).Name, GetType(Long).Name
                Return Converter.ToInt64(value)

            Case GetType(Single).Name
                Return Converter.ToSingle(value)

            Case GetType(Double).Name
                Return Converter.ToDouble(value)

            Case GetType(String).Name
                Return Converter.ToString(value)

            Case GetType(Boolean).Name
                Return Converter.ToBoolean(value)

            Case GetType(Date).Name, GetType(DateTime).Name
                Return Converter.ToDateTime(value)

            Case GetType(System.Net.IPAddress).Name
                Return Converter.ToIPAddress(value)

            Case GetType(DataTypes.FileSize).Name
                Return Converter.ToFileSize(value)

            Case GetType(DataTypes.ProgramVersion).Name
                Return Converter.ToProgramVersion(value)

            Case GetType(DataTypes.Fontos).Name
                Return Converter.ToFontos(value)

        End Select

        Return value

    End Function

    Public Shared Function ToRGBHex(ByVal color As System.Drawing.Color) As String
        Dim tmpR As String = Hex(color.R)
        Dim tmpG As String = Hex(color.G)
        Dim tmpB As String = Hex(color.B)

        If tmpR.Length = 1 Then tmpR = "0" & tmpR
        If tmpG.Length = 1 Then tmpG = "0" & tmpG
        If tmpB.Length = 1 Then tmpB = "0" & tmpB

        Return "0x" & tmpR & tmpG & tmpB

        'Return System.Drawing.ColorTranslator.ToHtml(color).Replace("#", "0x")

    End Function

    Public Shared Function FromRGBHex(ByVal color As String) As System.Drawing.Color
        Return System.Drawing.ColorTranslator.FromHtml(color.Replace("0x", "#"))

    End Function

End Class