Imports mermaid.BaseObjects.Enums

Namespace DataTypes

    <Serializable()> _
    Public Structure FileSize
        Implements IComparable, IComparable(Of FileSize)

#Region "Attributes"

        Private _Size As Long

#End Region

#Region "Properties"

        Public Property Size() As Long
            Get
                Return Me._Size

            End Get
            Set(ByVal value As Long)
                Me._Size = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal size As Long)
            Me._Size = size

        End Sub

        Public Overrides Function ToString() As String
            Return FormatBytes(Me.Size)

        End Function

#Region "Helpers"

        Public Shared Function FormatBytes(ByVal num_bytes As Double) As String
            If num_bytes = 0 Then
                Return String.Empty

            End If

            Const ONE_KB As Double = 1024
            Const ONE_MB As Double = ONE_KB * 1024
            Const ONE_GB As Double = ONE_MB * 1024
            Const ONE_TB As Double = ONE_GB * 1024
            Const ONE_PB As Double = ONE_TB * 1024
            Const ONE_EB As Double = ONE_PB * 1024
            Const ONE_ZB As Double = ONE_EB * 1024
            Const ONE_YB As Double = ONE_ZB * 1024

            ' See how big the value is.
            If num_bytes <= 999 Then
                ' Format in bytes.
                FormatBytes = Format$(num_bytes, "0") & " bytes"
            ElseIf num_bytes <= ONE_KB * 999 Then
                ' Format in KB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_KB) & " KB"
            ElseIf num_bytes <= ONE_MB * 999 Then
                ' Format in MB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_MB) & " MB"
            ElseIf num_bytes <= ONE_GB * 999 Then
                ' Format in GB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_GB) & " GB"
            ElseIf num_bytes <= ONE_TB * 999 Then
                ' Format in TB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_TB) & " TB"
            ElseIf num_bytes <= ONE_PB * 999 Then
                ' Format in PB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_PB) & " PB"
            ElseIf num_bytes <= ONE_EB * 999 Then
                ' Format in EB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_EB) & " EB"
            ElseIf num_bytes <= ONE_ZB * 999 Then
                ' Format in ZB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_ZB) & " ZB"
            Else
                ' Format in YB.
                FormatBytes = ThreeNonZeroDigits(num_bytes / _
                    ONE_YB) & " YB"
            End If

        End Function

        Private Shared Function ThreeNonZeroDigits(ByVal value As Double) As String
            If value >= 100 Then
                ' No digits after the decimal.
                ThreeNonZeroDigits = Format$(CInt(value))
            ElseIf value >= 10 Then
                ' One digit after the decimal.
                ThreeNonZeroDigits = Format$(value, "0.0")
            Else
                ' Two digits after the decimal.
                ThreeNonZeroDigits = Format$(value, "0.00")
            End If

        End Function

#End Region

#Region "Shared functions"

        Public Shared Function GetSortComparer(ByVal sortDirection As SortDirections) As FileSizeSortComparer
            Return New FileSizeSortComparer(sortDirection)

        End Function

#End Region

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If obj Is Nothing Then
                Return 1

            End If

            If TypeOf obj Is FileSize Then
                Dim tmpFileSize As FileSize = CType(obj, FileSize)

                Return Me.Size.CompareTo(tmpFileSize.Size)

            End If

            Throw New ArgumentException("Object is not a FileSize")

        End Function

        Public Function CompareTo(ByVal other As FileSize) As Integer Implements System.IComparable(Of FileSize).CompareTo
            Return Me.Size.CompareTo(other.Size)

        End Function

    End Structure

    <Serializable()> _
    Public Class FileSizeSortComparer
        Implements IComparer

#Region "Attributes"

        Private _SortDirection As SortDirections = SortDirections.Ascending

#End Region

#Region "Properties"

        Public Property SortDirection() As SortDirections
            Get
                Return Me._SortDirection

            End Get
            Set(ByVal value As SortDirections)
                Me._SortDirection = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal sortDirection As SortDirections)
            Me.SortDirection = sortDirection

        End Sub

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim tmpFileSizeX As FileSize = x
            Dim tmpFileSizeY As FileSize = y

            If x Is Nothing Then
                If y Is Nothing Then
                    Return 0 * Me.SortDirection

                Else
                    Return -1 * Me.SortDirection

                End If
            Else
                If y Is Nothing Then
                    Return 1 * Me.SortDirection

                Else
                    Return tmpFileSizeX.CompareTo(tmpFileSizeY) * Me.SortDirection

                End If
            End If

        End Function

    End Class

End Namespace