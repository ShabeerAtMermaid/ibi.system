﻿Imports mermaid.BaseObjects.Enums

Namespace DataTypes

    <Serializable()> _
    Public Structure NullableDate
        Implements IComparable, IComparable(Of NullableDate)

#Region "Variables"

        Private _Date As Date

#End Region

#Region "Properties"

        Public Property [Date]() As Date
            Get
                Return Me._Date

            End Get
            Set(ByVal value As Date)
                Me._Date = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal [date] As Date)
            Me._Date = [date]

        End Sub

        Public Overrides Function ToString() As String
            If Not Me.Date = Date.MinValue Then
                Return Me.Date.ToString

            End If

            Return String.Empty

        End Function

#Region "Shared functions"

        Public Shared Function GetSortComparer(ByVal sortDirection As SortDirections) As NullableDateSortComparer
            Return New NullableDateSortComparer(sortDirection)

        End Function

#End Region

        Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
            If obj Is Nothing Then
                Return 1

            End If

            If TypeOf obj Is NullableDate Then
                Dim tmpNullableDate As NullableDate = CType(obj, NullableDate)

                Return Me.Date.CompareTo(tmpNullableDate.Date)

            End If

            Throw New ArgumentException("Object is not a NullableDate")

        End Function

        Public Function CompareTo(ByVal other As NullableDate) As Integer Implements System.IComparable(Of NullableDate).CompareTo
            Return Me.Date.CompareTo(other.Date)

        End Function

    End Structure

    <Serializable()> _
    Public Class NullableDateSortComparer
        Implements IComparer

#Region "Attributes"

        Private _SortDirection As SortDirections = SortDirections.Ascending

#End Region

#Region "Properties"

        Public Property SortDirection() As SortDirections
            Get
                Return Me._SortDirection

            End Get
            Set(ByVal value As SortDirections)
                Me._SortDirection = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal sortDirection As SortDirections)
            Me.SortDirection = sortDirection

        End Sub

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim tmpNullableDateX As NullableDate = x
            Dim tmpNullableDateY As NullableDate = y

            If x Is Nothing Then
                If y Is Nothing Then
                    Return 0 * Me.SortDirection

                Else
                    Return -1 * Me.SortDirection

                End If
            Else
                If y Is Nothing Then
                    Return 1 * Me.SortDirection

                Else
                    Return tmpNullableDateX.CompareTo(tmpNullableDateY) * Me.SortDirection

                End If
            End If

        End Function

    End Class

End Namespace