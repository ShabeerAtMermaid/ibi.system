﻿Imports System.IO

Namespace DataTypes

    <System.Serializable()> _
    Public Class FileInformation

#Region "Variables"

        Private _Name As String
        Private _FullName As String
        Private _Extension As String
        Private _Size As FileSize
        Private _Created As Date
        Private _Modified As Date
        Private _CompanyName As String
        Private _FileDescription As String
        Private _FileVersion As String
        Private _FileLanguage As String
        Private _ProductName As String
        Private _ProductVersion As String
        Private _IsRegistered As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property Name() As String
            Get
                Return Me._Name

            End Get
        End Property

        Public ReadOnly Property FullName() As String
            Get
                Return Me._FullName

            End Get
        End Property

        Public ReadOnly Property Extension() As String
            Get
                Return Me._Extension

            End Get
        End Property

        Public ReadOnly Property Size() As FileSize
            Get
                Return Me._Size

            End Get
        End Property

        Public ReadOnly Property Created() As Date
            Get
                Return Me._Created

            End Get
        End Property

        Public ReadOnly Property Modified() As Date
            Get
                Return Me._Modified

            End Get
        End Property

        Public ReadOnly Property CompanyName() As String
            Get
                Return Me._CompanyName

            End Get
        End Property

        Public ReadOnly Property FileDescription() As String
            Get
                Return Me._FileDescription

            End Get
        End Property

        Public ReadOnly Property FileVersion() As String
            Get
                Return Me._FileVersion

            End Get
        End Property

        Public ReadOnly Property FileLanguage() As String
            Get
                Return Me._FileLanguage

            End Get
        End Property

        Public ReadOnly Property ProductName() As String
            Get
                Return Me._ProductName

            End Get
        End Property

        Public ReadOnly Property ProductVersion() As String
            Get
                Return Me._ProductVersion

            End Get
        End Property

        Public ReadOnly Property IsRegistered() As Boolean
            Get
                Return Me._IsRegistered

            End Get
        End Property

#End Region

#Region "WinAPI"

        Private Declare Function LoadLibraryA Lib "kernel32.dll" (ByVal lpLibFileName As String) As Integer
        Private Declare Function FreeLibrary Lib "kernel32.dll" (ByVal lpLibFileName As String) As Boolean

#End Region

#Region "Constants"

        Private Const SPLIT As String = "<|>"

#End Region

        Private Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal path As String)
            Me.New(New FileInfo(path))

        End Sub

        Public Sub New(ByVal file As FileInfo)
            MyBase.New()

            If file.Exists Then
                Me._Name = file.Name
                Me._FullName = file.FullName
                Me._Extension = file.Extension
                Me._Size = New FileSize(file.Length)
                Me._Created = file.CreationTime
                Me._Modified = file.LastWriteTime

                Me.ApplyFileInformation()

            Else
                Throw New FileNotFoundException("Cannot generate FileInformation", file.FullName)

            End If

        End Sub

        Private Sub ApplyFileInformation()
            Dim tmpInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Me.FullName)

            With tmpInfo
                Me._CompanyName = .CompanyName
                Me._FileDescription = .FileDescription
                Me._FileVersion = .FileVersion
                Me._FileLanguage = .Language
                Me._ProductName = .ProductName
                Me._ProductVersion = .ProductVersion

            End With

            Me.ValidateRegistration()

        End Sub

        Private Sub ValidateRegistration()
            'Try
            '    Dim proceed As Boolean = False

            '    If String.Compare(".dll", Me.Extension, True) = 0 Then
            '        proceed = True

            '    ElseIf String.Compare(".ocx", Me.Extension, True) = 0 Then
            '        proceed = True

            '    End If

            '    If proceed Then
            '        Dim tmpLibraryID As Integer = LoadLibraryA(Me.FullName)

            '        If tmpLibraryID > 0 Then FreeLibrary(tmpLibraryID)

            '        Me._IsRegistered = tmpLibraryID > 0

            '    End If

            'Catch ex As Exception
            '    BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Misc))

            'End Try
        End Sub

        Friend Function Serialize() As String
            Return Me.Name & SPLIT & _
                   Me.FullName & SPLIT & _
                   Me.Extension & SPLIT & _
                   Me.Size.Size & SPLIT & _
                   Me.Created.ToString & SPLIT & _
                   Me.Modified.ToString & SPLIT & _
                   Me.CompanyName & SPLIT & _
                   Me.FileDescription & SPLIT & _
                   Me.FileVersion & SPLIT & _
                   Me.FileLanguage & SPLIT & _
                   Me.ProductName & SPLIT & _
                   Me.ProductVersion & SPLIT & _
                   Me.IsRegistered

        End Function

        Friend Shared Function DeSerialize(ByVal data As String) As FileInformation
            Dim tmpSplittedData() As String = data.Split(SPLIT)

            If tmpSplittedData.Length = 13 Then
                Dim tmpFileInformation As New FileInformation

                tmpFileInformation._Name = tmpSplittedData(0)
                tmpFileInformation._FullName = tmpSplittedData(1)
                tmpFileInformation._Extension = tmpSplittedData(2)
                tmpFileInformation._Size = New FileSize(tmpSplittedData(3))
                tmpFileInformation._Created = Converter.ToDateTime(tmpSplittedData(4))
                tmpFileInformation._Modified = Converter.ToDateTime(tmpSplittedData(5))
                tmpFileInformation._CompanyName = tmpSplittedData(6)
                tmpFileInformation._FileDescription = tmpSplittedData(7)
                tmpFileInformation._FileVersion = tmpSplittedData(8)
                tmpFileInformation._FileLanguage = tmpSplittedData(9)
                tmpFileInformation._ProductName = tmpSplittedData(10)
                tmpFileInformation._ProductVersion = tmpSplittedData(11)
                tmpFileInformation._IsRegistered = Converter.ToBoolean(tmpSplittedData(12))

                Return tmpFileInformation

            End If

            Return Nothing

        End Function

    End Class

End Namespace