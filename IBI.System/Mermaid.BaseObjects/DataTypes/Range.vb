Namespace DataTypes

    <Serializable()> _
    Public Structure Range(Of t)

#Region "Variables"

        Private _From As t
        Private _To As t

#End Region

#Region "Properties"

        Public Property From() As t
            Get
                Return Me._From

            End Get
            Set(ByVal value As t)
                Me._From = value

            End Set
        End Property

        Public Property [To]() As t
            Get
                Return Me._To

            End Get
            Set(ByVal value As t)
                Me._To = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal from As t, ByVal [to] As t)
            Me.From = from
            Me.To = [to]

        End Sub

        Public Function IsWithinRange(ByVal value As t) As Boolean
            If Array.IndexOf(GetType(t).GetInterfaces, GetType(IComparable)) <> -1 Then
                If CType(Me.From, IComparable).CompareTo(value) <= 0 And CType(Me.To, IComparable).CompareTo(value) >= 0 Then
                    Return True

                End If
            End If

            Return False

        End Function

        Public Shadows Function Equals(ByVal obj As Object) As Boolean
            If obj IsNot Nothing Then
                If TypeOf obj Is Range(Of t) Then
                    Dim tmpCompareRange As Range(Of t) = obj

                    If Me.From.Equals(tmpCompareRange.From) Then
                        If Me.To.Equals(tmpCompareRange.To) Then
                            Return True

                        End If
                    End If
                End If
            End If

            Return False

        End Function

    End Structure

End Namespace