Imports mermaid.BaseObjects.Enums

Namespace DataTypes

    <Serializable()> _
    Public Structure Fontos

#Region "Attributes"

        Private fontFamilyValue As String
        Private fontSizeValue As Single
        Private fontStyleValue As Integer
        Private fontAlignmentValue As Integer
        Private fontGraphicsUnitValue As Integer

#End Region

#Region "Properties"

        Public Property FontString() As String
            Get
                If Not Me.FontFamily Is Nothing AndAlso Not Me.FontFamily = String.Empty Then

                Else
                    Me.ResetFont()

                End If

                Return Me.FontFamily & ";" & Me.FontSize & ";" & Me.fontStyleValue & ";" & Me.fontAlignmentValue & ";" & Me.fontGraphicsUnitValue

            End Get
            Set(ByVal Value As String)
                Dim values() As String = Value.Split(";")

                If values.Length >= 3 Then
                    FontFamily = values(0)
                    FontSize = Single.Parse(values(1))
                    FontStyle = values(2)

                    If values.Length >= 4 Then FontAlignment = values(3)
                    If values.Length >= 5 Then FontGraphicsUnit = values(4)

                Else
                    'Throw New Exception("The FontString has an incorrect format.")
                    Me.ResetFont()

                End If
            End Set
        End Property

        Public Property FontFamily() As String
            Get
                Return Me.fontFamilyValue

            End Get
            Set(ByVal Value As String)
                Me.fontFamilyValue = Value

            End Set
        End Property

        Public Property FontSize() As Single
            Get
                Return Me.fontSizeValue

            End Get
            Set(ByVal Value As Single)
                Me.fontSizeValue = Value

            End Set
        End Property

        Public Property FontStyle() As System.Drawing.FontStyle
            Get
                Return [Enum].Parse(GetType(System.Drawing.FontStyle), Me.fontStyleValue)

            End Get
            Set(ByVal Value As System.Drawing.FontStyle)
                Me.fontStyleValue = CInt(Value)

            End Set
        End Property

        Public Property FontAlignment() As FontAlignmentTypes
            Get
                Return [Enum].Parse(GetType(FontAlignmentTypes), Me.fontAlignmentValue)

            End Get
            Set(ByVal value As FontAlignmentTypes)
                Me.fontAlignmentValue = CInt(value)

            End Set
        End Property

        Public Property FontGraphicsUnit() As Drawing.GraphicsUnit
            Get
                Return [Enum].Parse(GetType(Drawing.GraphicsUnit), Me.fontGraphicsUnitValue)

            End Get
            Set(ByVal value As Drawing.GraphicsUnit)
                Me.fontGraphicsUnitValue = CInt(value)

            End Set
        End Property

#End Region

#Region "Fields"

        Public Shared Empty As Fontos = New Fontos("Times New Roman;10;0;1;3")

#End Region

        Public Sub New(ByVal fromFont As System.Drawing.Font)
            Me.ResetFont()

            Me.SetFont(fromFont)

        End Sub

        ''' <summary>
        ''' Creates a new Fontos Object from the specified font string.
        ''' </summary>
        ''' <param name="fromFontString">Font string in the following format: "[FontFamily];[FontSize];[FontStyle];[FontAlignment];[FontGraphicsUnit]</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal fromFontString As String)
            Me.ResetFont()

            FontString = fromFontString

        End Sub

        Public Sub ResetFont()
            FontString = "Times New Roman;10;0;1;3"

        End Sub

        Public Function ValidateFontString(ByVal fromFontString As String) As Boolean
            Try
                Dim values() As String = fromFontString.Split(";")

                Dim tmpFontFamily As String
                Dim tmpFontSize As Single
                Dim tmpFontStyle As Integer
                Dim tmpFontAlignment As Integer = 1
                Dim tmpFontGraphicsUnit As Integer = 3

                If values.Length >= 3 Then
                    tmpFontFamily = values(0)
                    tmpFontSize = Single.Parse(values(1))
                    tmpFontStyle = values(2)

                    If values.Length >= 4 Then tmpFontAlignment = values(3)
                    If values.Length >= 5 Then tmpFontGraphicsUnit = values(4)

                    Dim tmpFont As New System.Drawing.Font(tmpFontFamily, tmpFontSize, tmpFontStyle, tmpFontGraphicsUnit)

                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                Return False

            End Try
        End Function

        Public Function GetFont() As System.Drawing.Font
            Try
                Return New System.Drawing.Font(Me.FontFamily, Me.FontSize, Me.FontStyle, Me.FontGraphicsUnit)

            Catch ex As Exception
                Return New System.Drawing.Font("Times New Roman", 10, Drawing.FontStyle.Regular, Drawing.GraphicsUnit.Point)

            End Try
        End Function

        Public Sub SetFont(ByVal font As System.Drawing.Font)
            If Not font Is Nothing Then
                FontFamily = font.FontFamily.Name
                FontSize = font.Size
                FontStyle = font.Style
                FontGraphicsUnit = font.Unit

            End If

        End Sub

    End Structure

End Namespace