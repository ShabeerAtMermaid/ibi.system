Namespace DataTypes

    <Serializable()> _
    Public Structure ProgramVersion
       
#Region "Fields"

        Public Shared ReadOnly Empty As ProgramVersion = New ProgramVersion(0, 0, 0, 0)

#End Region

#Region "Attributes"

        Private _Major As Integer
        Private _Minor As Integer
        Private _Build As Integer
        Private _Revision As Integer

#End Region

#Region "Properties"

        Public Property Major() As Integer
            Get
                Return Me._Major

            End Get
            Set(ByVal value As Integer)
                Me._Major = value

            End Set
        End Property

        Public Property Minor() As Integer
            Get
                Return Me._Minor

            End Get
            Set(ByVal value As Integer)
                Me._Minor = value

            End Set
        End Property

        Public Property Build() As Integer
            Get
                Return Me._Build

            End Get
            Set(ByVal value As Integer)
                Me._Build = value

            End Set
        End Property

        Public Property Revision() As Integer
            Get
                Return Me._Revision

            End Get
            Set(ByVal value As Integer)
                Me._Revision = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal major As Integer, ByVal minor As Integer, ByVal build As Integer, ByVal revision As Integer)
            Me.Major = major
            Me.Minor = minor
            Me.Build = build
            Me.Revision = revision

        End Sub

        Public Sub New(ByVal version As String)
            Try
                Dim tmpString() As String = version.Split(".")

                If tmpString.Length > 0 Then Me.Major = tmpString(0)
                If tmpString.Length > 1 Then Me.Minor = tmpString(1)
                If tmpString.Length > 2 Then Me.Build = tmpString(2)
                If tmpString.Length > 3 Then Me.Revision = tmpString(3)

            Catch ex As Exception
                Me.Major = 0
                Me.Minor = 0
                Me.Build = 0
                Me.Revision = 0

            End Try
        End Sub

        Public Shared Function CurrentProgramVersion() As ProgramVersion
            Try
                Dim tmpFileInfo As New System.IO.FileInfo(My.Application.Info.AssemblyName & ".exe")
                Dim tmpAssembly As System.Reflection.Assembly = Nothing

                If tmpFileInfo.Exists Then
                    tmpAssembly = System.Reflection.Assembly.LoadFrom(tmpFileInfo.FullName)

                Else
                    tmpFileInfo = New System.IO.FileInfo(My.Application.Info.AssemblyName & ".dll")

                    If tmpFileInfo.Exists Then
                        tmpAssembly = System.Reflection.Assembly.LoadFrom(tmpFileInfo.FullName)

                    End If
                End If

                If Not tmpAssembly Is Nothing Then
                    Return New ProgramVersion(tmpAssembly.GetName.Version.Major, tmpAssembly.GetName.Version.Minor, tmpAssembly.GetName.Version.Build, tmpAssembly.GetName.Version.Revision)

                End If

                Return New ProgramVersion(My.Application.Info.Version.Major, My.Application.Info.Version.Minor, My.Application.Info.Version.Build, My.Application.Info.Version.Revision)

            Catch ex As Exception
                Return Nothing

            End Try
        End Function

        ''' <summary>
        ''' Compares the supplied ProgramVersion with this instance.
        ''' </summary>
        ''' <param name="obj"><see cref="ProgramVersion" /> to compare.</param>
        ''' <returns>True if the ProgramVersions are identical.</returns>
        ''' <remarks>This method only compares the Major, Minor and Build Versions - not the Revision.</remarks>
        Public Shadows Function Equals(ByVal obj As Object) As Boolean
            Return Me.Equals(obj, True, True, True, False)

        End Function

        Public Shadows Function Equals(ByVal obj As Object, ByVal compareMajor As Boolean, ByVal compareMinor As Boolean, ByVal compareBuild As Boolean, ByVal compareRevision As Boolean) As Boolean
            If Not obj Is Nothing Then
                If TypeOf obj Is ProgramVersion Then
                    Dim tmpProgramVersion As ProgramVersion = obj

                    Dim majorApproved As Boolean = False
                    Dim minorApproved As Boolean = False
                    Dim buildApproved As Boolean = False
                    Dim revisionApproved As Boolean = False

                    If Not compareMajor Then
                        majorApproved = True

                    ElseIf tmpProgramVersion.Major = Me.Major Then
                        majorApproved = True

                    End If

                    If Not compareMinor Then
                        minorApproved = True

                    ElseIf tmpProgramVersion.Minor = Me.Minor Then
                        minorApproved = True

                    End If

                    If Not compareBuild Then
                        buildApproved = True

                    ElseIf tmpProgramVersion.Build = Me.Build Then
                        buildApproved = True

                    End If

                    If Not compareRevision Then
                        revisionApproved = True

                    ElseIf tmpProgramVersion.Revision = Me.Revision Then
                        revisionApproved = True

                    End If

                    If majorApproved And minorApproved And buildApproved And revisionApproved Then
                        Return True

                    End If
                End If
            End If

            Return False

        End Function

        Public Overrides Function ToString() As String
            Return Me.Major & "." & Me.Minor & "." & Me.Build & "." & Me.Revision

        End Function

    End Structure

End Namespace