Imports mermaid.BaseObjects.Localization.Interfaces
Imports System.Resources
Imports System.Reflection
Imports System.Threading
Imports System.ComponentModel
Imports System.Globalization
Imports System.IO

Namespace Localization

    Public Class ResourceUtility

#Region "Attributes"

        Private Shared languageFilePathMaskValue As String = String.Empty
        Private Shared languageFileBaseNameMaskValue As String = String.Empty
        Private Shared resourceManagersValue As New Hashtable
        Private Shared listenersValue As New ArrayList

#End Region

#Region "Properties"

        ''' <summary>
        ''' Filename mask of the used language files.
        ''' </summary>
        ''' <value>String containing the mask.</value>
        ''' <returns>Returns the mask.</returns>
        ''' <remarks>An example of a mask is: "Localization\mermaid.vTouch.Administrator.{LANG}.dll" 
        ''' This will cause the application to look for the file: "Localization\mermaid.vTouch.Administrator.da-DK.dll". 
        ''' "LANG" must be in UPPER case.
        ''' </remarks>
        Public Shared Property LanguageFilePathMask() As String
            Get
                Return languageFilePathMaskValue

            End Get
            Set(ByVal Value As String)
                languageFilePathMaskValue = Value

            End Set
        End Property

        ''' <summary>
        ''' Mask for the root name of the resources.
        ''' Eg. the root name could be "mermaid.vTouch.Administrator.{LANG}.Language".
        ''' </summary>
        ''' <value>String containing the Base Name.</value>
        ''' <returns>Return the Base Name.</returns>
        ''' <remarks>"LANG" must be in UPPER case.</remarks>
        Public Shared Property LanguageFileBaseNameMask() As String
            Get
                Return languageFileBaseNameMaskValue

            End Get
            Set(ByVal value As String)
                languageFileBaseNameMaskValue = value

            End Set
        End Property

        Private Shared Property ResourceManagers() As Hashtable
            Get
                Return resourceManagersValue

            End Get
            Set(ByVal Value As Hashtable)
                resourceManagersValue = Value

            End Set
        End Property

        Private Shared Property Listeners() As ArrayList
            Get
                Return listenersValue

            End Get
            Set(ByVal Value As ArrayList)
                listenersValue = Value

            End Set
        End Property

#End Region

#Region "Events"

        Public Shared Event CultureChanged()

#End Region

        Public Shared Function GetString(ByVal key As String) As String
            Dim rm As ResourceManager

            'Dim resourceKey As String = Application & "." & Thread.CurrentThread.CurrentUICulture.Name
            Dim resourceKey As String = LanguageFilePathMask.Replace("{LANG}", Thread.CurrentThread.CurrentUICulture.Name)

            If ResourceManagers.Item(resourceKey) Is Nothing Then
                If File.Exists(System.AppDomain.CurrentDomain.BaseDirectory & resourceKey) Then
                    Dim tmpAssembly As [Assembly]
                    tmpAssembly = [Assembly].LoadFrom(System.AppDomain.CurrentDomain.BaseDirectory & resourceKey)

                    'Dim tmpLangUnder As String = Replace(Thread.CurrentThread.CurrentUICulture.Name, "-", "_")
                    'rm = New ResourceManager("VictorFilm." & Application & ".LanguageLibrary." & tmpLangUnder & ".Language", tmpAssembly)
                    Dim tmpBaseName As String = LanguageFileBaseNameMask.Replace("{LANG}", Thread.CurrentThread.CurrentUICulture.Name.Replace("-", "_"))

                    rm = New ResourceManager(tmpBaseName, tmpAssembly)
                    ResourceManagers.Add(resourceKey, rm)

                Else
                    Return ""

                End If

            Else
                rm = ResourceManagers.Item(resourceKey)

            End If

            Dim tmpRes As String = rm.GetString(key)

            If tmpRes Is Nothing Then
                Return String.Empty

            Else
                Return tmpRes.Replace("\n", vbCrLf)

            End If

        End Function

        'Public Shared Function GetAllStrings() As String()
        '    Dim rm As ResourceManager

        '    Dim resourceKey As String = LanguageFilePathMask.Replace("{LANG}", Thread.CurrentThread.CurrentUICulture.Name)

        '    If ResourceManagers.Item(resourceKey) Is Nothing Then
        '        If File.Exists(System.AppDomain.CurrentDomain.BaseDirectory & resourceKey) Then
        '            Dim tmpAssembly As [Assembly]
        '            tmpAssembly = [Assembly].LoadFrom(System.AppDomain.CurrentDomain.BaseDirectory & resourceKey)

        '            Dim tmpBaseName As String = LanguageFileBaseNameMask.Replace("{LANG}", Thread.CurrentThread.CurrentUICulture.Name.Replace("-", "_"))

        '            rm = New ResourceManager(tmpBaseName, tmpAssembly)
        '            ResourceManagers.Add(resourceKey, rm)

        '        Else
        '            Throw New FileNotFoundException("Cannot find file: " & System.AppDomain.CurrentDomain.BaseDirectory & resourceKey)

        '        End If

        '    Else
        '        rm = ResourceManagers.Item(resourceKey)

        '    End If

        '    rm.
        '    Dim tmpRes As String = rm.GetString(key)

        '    If tmpRes Is Nothing Then
        '        Return String.Empty

        '    Else
        '        Return tmpRes.Replace("\n", vbCrLf)

        '    End If
        'End Function

        Public Shared Sub ChangeLanguage(ByVal culture As CultureInfo)
            Thread.CurrentThread.CurrentUICulture = culture

            OnCultureChanged()

        End Sub

        Private Shared Sub OnCultureChanged()
            RaiseEvent CultureChanged()

            For Each tmpListener As ILocalizable In Listeners
                tmpListener.LoadLocalization()

            Next

        End Sub

        Public Shared Sub AddLocalizableListener(ByRef listener As ILocalizable)
            If Not Listeners.Contains(listener) Then
                Listeners.Add(listener)

            End If

        End Sub

        Public Shared Sub RemoveLocalizableListener(ByRef listener As ILocalizable)
            Listeners.Remove(listener)

        End Sub

    End Class

End Namespace