Namespace Net

    Public Class FileProviderProxy
        Inherits BaseObject
        Implements Interfaces.IFileProvider

#Region "Variables"

        Private _FileProvider As Interfaces.IFileProvider

#End Region

#Region "Properties"

        Public ReadOnly Property FileProvider() As Interfaces.IFileProvider
            Get
                Return Me._FileProvider

            End Get
        End Property

        Public ReadOnly Property Path() As String Implements Interfaces.IFileProvider.Path
            Get
                Return Me.FileProvider.Path

            End Get
        End Property

        Public ReadOnly Property URI() As String Implements Interfaces.IFileProvider.URI
            Get
                Return Me.FileProvider.URI

            End Get
        End Property

        Public ReadOnly Property TotalSize() As Long Implements Interfaces.IFileProvider.TotalSize
            Get
                Return Me.FileProvider.TotalSize

            End Get
        End Property

        Public ReadOnly Property MD5() As IO.FileHash Implements Interfaces.IFileProvider.MD5
            Get
                Return Me.FileProvider.MD5

            End Get
        End Property

        Public ReadOnly Property Modified() As Date Implements Interfaces.IFileProvider.Modified
            Get
                Return Me.FileProvider.Modified

            End Get
        End Property

#End Region

        Public Sub New(ByVal fileProvider As Interfaces.IFileProvider)
            MyBase.New()

            Me._FileProvider = fileProvider

        End Sub

        Public Function GetNextPortion(Optional ByVal portionSize As Integer = 32768) As Byte() Implements Interfaces.IFileProvider.GetNextPortion
            Return Me.FileProvider.GetNextPortion(portionSize)

        End Function

        Public Sub DownloadComplete() Implements Interfaces.IFileProvider.DownloadComplete
            Me.FileProvider.DownloadComplete()

        End Sub

        Public Sub Abort() Implements Interfaces.IFileProvider.Abort
            Me.FileProvider.Abort()

        End Sub

        Public Shadows Sub Dispose()
            MyBase.Dispose()

            If Not Me.FileProvider Is Nothing Then
                Me.FileProvider.Dispose()

            End If

        End Sub

    End Class

End Namespace