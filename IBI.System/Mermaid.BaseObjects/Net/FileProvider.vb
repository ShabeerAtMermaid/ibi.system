Imports mermaid.BaseObjects.Net.Interfaces
Imports mermaid.BaseObjects.Delegates
Imports mermaid.BaseObjects.EventArgs
Imports mermaid.BaseObjects.Exceptions
Imports mermaid.BaseObjects.IO
Imports System.Threading
Imports System.Text
Imports System.IO

Namespace Net

    ''' <summary>
    ''' Is used to fetch files in small chunks.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FileProvider
        Inherits BaseObject
        Implements IFileProvider

#Region "Attributes"

        Private _File As FileInfo
        Private _Path As String
        Private _URI As String
        Private _Offset As Long
        Private _MD5 As FileHash
        Private _AsyncProgressEvents As Boolean
        Private _Aborted As Boolean
        Private _DownloadFinished As Boolean
        Private _TimedOut As Boolean
        Private _TimeOut As TimeSpan = New TimeSpan(0, 1, 0)
        Private WithEvents _TmrTimeOut As Timers.Timer

        Private _CompletionResetEvent As ManualResetEvent

        Private Shared _ActiveURIs As New System.Collections.Generic.List(Of String)
        Private Shared _ActiveURIsLock As Object = New Object

#End Region

#Region "Properties"

        ''' <summary>
        ''' Information about the file to provide as a download.
        ''' </summary>
        ''' <value><see cref="FileInfo"/> Object of the file.</value>
        ''' <returns><see cref="FileInfo"/> with information about the file.</returns>
        ''' <remarks></remarks>
        Private Property File() As FileInfo
            Get
                Return Me._File

            End Get
            Set(ByVal Value As FileInfo)
                Me._File = Value

            End Set
        End Property

        ''' <summary>
        ''' Relative path to the file.
        ''' </summary>
        ''' <value>String containing the path.</value>
        ''' <returns>Return the relative path.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Path() As String Implements IFileProvider.Path
            Get
                Return Me._Path

            End Get
        End Property

        ''' <summary>
        ''' The URI at which this provider has been marshalled.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property URI() As String Implements Interfaces.IFileProvider.URI
            Get
                Return Me._URI

            End Get
        End Property

        ''' <summary>
        ''' The total size of the file.
        ''' </summary>
        ''' <value>The filesize.</value>
        ''' <returns>Returns the size of the file.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TotalSize() As Long Implements IFileProvider.TotalSize
            Get
                If Me.File Is Nothing Then
                    Throw New FileNotFoundException("The file provided doesn't exist.")

                End If

                If Not Me.File.Exists Then
                    Throw New FileNotFoundException("The file provided doesn't exist.", Me.File.FullName)

                End If

                Return Me.File.Length

            End Get
        End Property

        ''' <summary>
        ''' Offset controlling where to read in the file.
        ''' </summary>
        ''' <value><see cref="Long"/> defining the offset.</value>
        ''' <returns>Returns the offset.</returns>
        ''' <remarks></remarks>
        Private Property Offset() As Integer
            Get
                Return Me._Offset

            End Get
            Set(ByVal Value As Integer)
                Me._Offset = Value

            End Set
        End Property

        ''' <summary>
        ''' MD5 checksum of the file.
        ''' </summary>
        ''' <value><see cref="FileHash"/> specifying the MD5.</value>
        ''' <returns>Returns a <see cref="FileHash"/> specifying the MD5.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MD5() As FileHash Implements IFileProvider.MD5
            Get
                Return Me._MD5

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether calls to a registered <see cref="DownloadProgressHandler"/> is done async.
        ''' </summary>
        ''' <value>Boolean defining whether DownloadProgress events are raised async.</value>
        ''' <returns>Return whether DownloadProgress events are raised async.</returns>
        ''' <remarks></remarks>
        Public Property AsyncProgressEvents() As Boolean
            Get
                Return Me._AsyncProgressEvents

            End Get
            Set(ByVal Value As Boolean)
                Me._AsyncProgressEvents = Value

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether the download has been aborted.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has been aborted.</value>
        ''' <returns>Return whether the download has been aborted.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Aborted() As Boolean
            Get
                Return Me._Aborted

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has finished sucessfully.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has finished sucessfully.</value>
        ''' <returns>Return whether the download has finished sucessfully.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DownloadFinished() As Boolean
            Get
                Return Me._DownloadFinished

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has timed out.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TimedOut() As Boolean
            Get
                Return Me._TimedOut

            End Get
        End Property

        ''' <summary>
        ''' The latest <see cref="Date"/> the file has been modified.
        ''' </summary>      
        ''' <value>Last modified date.</value>
        ''' <returns>Returns the last modified date.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Modified() As Date Implements IFileProvider.Modified
            Get
                If Me.File Is Nothing Then
                    Throw New FileNotFoundException("The file provided doesn't exist.")

                End If

                If Not Me.File.Exists Then
                    Throw New FileNotFoundException("The file provided doesn't exist.", Me.File.FullName)

                End If

                Return Me.File.LastWriteTime

            End Get
        End Property

        ''' <summary>
        ''' The time the download can be inactive before a DownloadTimeOut Event is raised.
        ''' </summary>
        ''' <value>TimeSpan defining the Time Out.</value>
        ''' <returns>Return the effective Time Out.</returns>
        ''' <remarks></remarks>
        Public Property TimeOut() As TimeSpan
            Get
                Return Me._TimeOut

            End Get
            Set(ByVal value As TimeSpan)
                Me._TimeOut = value

                Me.TmrTimeOut.Interval = value.TotalMilliseconds

            End Set
        End Property

        ''' <summary>
        ''' The Timer controlling the Time Out.
        ''' </summary>
        ''' <value><see cref="Timers.Timer" /> controlling the Time Out of the download.</value>
        ''' <returns>Returns the Time Out <see cref="Timers.Timer" />.</returns>
        ''' <remarks>Never return Nothing.</remarks>
        Private ReadOnly Property TmrTimeOut() As Timers.Timer
            Get
                If Me._TmrTimeOut Is Nothing Then
                    Me._TmrTimeOut = New Timers.Timer
                    Me._TmrTimeOut.Interval = Me.TimeOut.TotalMilliseconds

                End If

                Return Me._TmrTimeOut

            End Get
        End Property

        ''' <summary>
        ''' Reset event the user can use to wait for completion (or error or abort or....)
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Property CompletionResetEvent() As ManualResetEvent
            Get
                Return Me._CompletionResetEvent

            End Get
            Set(ByVal value As ManualResetEvent)
                Me._CompletionResetEvent = value

            End Set
        End Property

        ''' <summary>
        ''' A list of URIs currently in use.
        ''' </summary>
        ''' <value>List of Strings containing the URIs.</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared ReadOnly Property ActiveURIs() As System.Collections.Generic.List(Of String)
            Get
                Return _ActiveURIs

            End Get
        End Property

        ''' <summary>
        ''' Lock used to access the ActiveURIs list.
        ''' </summary>
        ''' <value>The Lock object.</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Shared ReadOnly Property ActiveURIsLock() As Object
            Get
                Return _ActiveURIsLock

            End Get
        End Property

#End Region

#Region "Events"

        ''' <summary>
        ''' Event that is raised when a portion of a file is downloaded.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadProgress As Delegates.DownloadProgressHandler

        ''' <summary>
        ''' Event that is raised when the download has finished.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadCompleted As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download has timed out.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadTimeOut As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download is aborted.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadAborted As System.EventHandler

#End Region

        ''' <summary>
        ''' Creates a new <see cref="FileProvider"/> with the specified file.
        ''' </summary>
        ''' <param name="localpath">Path to the file to provide.</param>
        ''' <param name="remotePath">Path to save the file when downloaded.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal localpath As String, ByVal remotePath As String)
            MyBase.New()

            Me.File = New FileInfo(localpath)
            Me._Path = remotePath

            If Not Me.File.Exists Then
                Throw New FileNotFoundException("The file provided doesn't exist.", localpath)

            End If

            Me._MD5 = FileHash.FromFile(Me.File, True, False)

            Me._URI = Me.GenerateURI
            Runtime.Remoting.RemotingServices.Marshal(Me, Me._URI)

            Me.TmrTimeOut.Enabled = True

        End Sub

        ''' <summary>
        '''     Returns the next portion or null if the entire file was downloaded.
        ''' </summary>
        ''' <returns>Next portion or null.</returns>
        ''' <remarks>
        ''' </remarks>
        Public Function GetNextPortion(Optional ByVal portionSize As Integer = 32768) As Byte() Implements IFileProvider.GetNextPortion
            Me.ResetTimeOutTimer()

            If Me.Aborted Then
                Me.OnDownloadAborted(New System.EventArgs)

                Throw New DownloadAbortedException()

            End If

            If Me.Offset >= Me.TotalSize Then Return Nothing

            Dim sizeToSend As Integer = Math.Min(Me.TotalSize - Offset, portionSize)

            Dim tmpStream As Stream = Me.File.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            Dim buffer(sizeToSend - 1) As Byte

            Try
                tmpStream.Position = Me.Offset
                tmpStream.Read(buffer, 0, buffer.Length)

            Finally
                tmpStream.Dispose()

            End Try

            Me.Offset += sizeToSend

            Me.OnDownloadProgress(New EventArgs.DownloadProgressEventArgs(Me, sizeToSend, Offset, TotalSize))

            Return buffer

        End Function

        ''' <summary>
        ''' Indicates that the download has completed. 
        ''' The FileProvider is disposed and a DownloadComplete Event is raised.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub DownloadComplete() Implements Interfaces.IFileProvider.DownloadComplete
            Me.Dispose()

            Me._DownloadFinished = True

            Me.OnDownloadCompleted(New System.EventArgs)

        End Sub

        ''' <summary>
        ''' Aborts the download.
        ''' </summary>
        ''' <remarks>This will cause a <see cref="DownloadAbortedException"/> to be thrown.</remarks>
        Public Sub Abort() Implements IFileProvider.Abort
            If Not Me.Aborted Then
                Me.Dispose()

                Me._Aborted = True

            End If

        End Sub

        ''' <summary>
        ''' Generates the URI at which this provider will be marshalled. 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GenerateURI() As String
            Monitor.Enter(ActiveURIsLock)

            Try
                Dim tmpURI As String = "FileProvider_" & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 4, Date.Now.Year) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Month) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Day) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Hour) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Minute) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Second) & Functions.ConvertToFixedLength("0", BaseObjects.Enums.FixedLengthConversionTypes.PrefixFillChar, 4, Date.Now.Millisecond) & ".rem"
                Dim tmpBaseURI As String = tmpURI

                Dim tmpCount As Integer = 0

                While ActiveURIs.Contains(tmpURI)
                    tmpCount += 1

                    tmpURI = tmpBaseURI.Replace(".rem", String.Empty) & "_" & tmpCount & ".rem"

                End While

                ActiveURIs.Add(tmpURI)

                Return tmpURI

            Finally
                Monitor.Exit(ActiveURIsLock)

            End Try
        End Function

        ''' <summary>
        ''' Resets the Time Out Timer and starts it again.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ResetTimeOutTimer()
            If Not Me.Aborted Then
                Me.TmrTimeOut.Enabled = False

                Me.TmrTimeOut.Interval = Me.TimeOut.TotalMilliseconds

                Me.TmrTimeOut.Enabled = True

            End If

        End Sub

        ''' <summary>
        ''' Disposes the Time Out Timer.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub DisposeTimeOutTimer()
            Me.TmrTimeOut.Enabled = False

            Me.TmrTimeOut.Dispose()

            Me._TmrTimeOut = Nothing

        End Sub

        ''' <summary>
        ''' Raises the DownloadProgress Event with the specified <see cref="DownloadProgressEventArgs"/>.
        ''' </summary>
        ''' <param name="e"><see cref="DownloadProgressEventArgs"/> to be raised.</param>
        ''' <remarks>If <see cref="AsyncProgressEvents"/> is True, the Event is raised in seperate threads.</remarks>
        Private Sub OnDownloadProgress(ByVal e As EventArgs.DownloadProgressEventArgs)
            If Me.AsyncProgressEvents Then
                If Not Me.DownloadProgressEvent Is Nothing Then
                    For Each tmpHandler As Delegates.DownloadProgressHandler In Me.DownloadProgressEvent.GetInvocationList
                        Dim tmpThreadData As DownloadProgressThreadData = New DownloadProgressThreadData(Me, e, tmpHandler)

                        'THREAD: ThreadManager
                        'Dim tmpThread As New Thread(AddressOf tmpThreadData.NotifyHandler)
                        'tmpThread.Name = "FileDownloader DownloadProgress Thread"

                        Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("FileDownloader DownloadProgress Thread", AddressOf tmpThreadData.NotifyHandler)
                        tmpThread.Start()

                        Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Increment()

                    Next
                End If

            Else
                RaiseEvent DownloadProgress(Me, e)

            End If

        End Sub

        ''' <summary>
        ''' Raises the DownloadCompleted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadCompleted(ByVal e As System.EventArgs)
            RaiseEvent DownloadCompleted(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadTimeOut Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadTimeOut(ByVal e As System.EventArgs)
            RaiseEvent DownloadTimeOut(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadAborted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadAborted(ByVal e As System.EventArgs)
            RaiseEvent DownloadAborted(Me, e)

        End Sub

        Public Sub WaitForCompletion()
            Using New Threading.Lock(Me, True)
                If Not Me.DownloadFinished And Not Me.Aborted And Not Me.IsDisposed Then
                    If Me.CompletionResetEvent Is Nothing Then
                        Me.CompletionResetEvent = New ManualResetEvent(False)

                    End If
                End If

            End Using

            If Not Me.CompletionResetEvent Is Nothing Then
                Me.CompletionResetEvent.WaitOne()

            End If

        End Sub

        Private Class DownloadProgressThreadData

#Region "Attributes"

            Private _FileProvider As FileProvider
            Private _EventArgs As EventArgs.DownloadProgressEventArgs
            Private _Handler As Delegates.DownloadProgressHandler

#End Region

#Region "Properties"

            Public ReadOnly Property FileProvider() As FileProvider
                Get
                    Return Me._FileProvider

                End Get
            End Property

            Public ReadOnly Property EventArgs() As EventArgs.DownloadProgressEventArgs
                Get
                    Return Me._EventArgs

                End Get
            End Property

            Public ReadOnly Property Handler() As Delegates.DownloadProgressHandler
                Get
                    Return Me._Handler

                End Get
            End Property

#End Region

            Public Sub New(ByVal fileProvider As FileProvider, ByVal eventArgs As EventArgs.DownloadProgressEventArgs, ByVal handler As Delegates.DownloadProgressHandler)
                Me._FileProvider = fileProvider
                Me._EventArgs = eventArgs
                Me._Handler = handler

            End Sub

            Public Sub NotifyHandler()
                Try
                    Me.Handler.Invoke(Me.FileProvider, Me.EventArgs)

                Catch ex As Exception
                    'SILENT

                Finally
                    Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Decrement()
                End Try
            End Sub

        End Class

        ''' <summary>
        ''' Disposes the Object and it's resources.
        ''' </summary>
        ''' <remarks></remarks>
        Public Overrides Sub Dispose()
            If Not Me.IsDisposed Then
                MyBase.Dispose()

                Me.DisposeTimeOutTimer()

                Try
                    System.Runtime.Remoting.RemotingServices.Disconnect(Me)

                Catch ex As Exception
                    'SILENT: Unmarshal FileProvider

                End Try

            End If

            Using New Threading.Lock(Me, True)
                If Not Me.CompletionResetEvent Is Nothing Then
                    Me.CompletionResetEvent.Set()

                End If

            End Using

        End Sub

        ''' <summary>
        ''' Indicates the there has been no activity for the specified Time Out interval.
        ''' The FileProvider is disposed and a DownloadTimeOut Event is raised.
        ''' </summary>
        ''' <param name="sender">Object raising the Elapsed Event.</param>
        ''' <param name="e">The supplied EventArgs.</param>
        ''' <remarks></remarks>
        Private Sub TmrTimeOut_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrTimeOut.Elapsed
            Me._TimedOut = True

            Me.Dispose()

            Me.OnDownloadTimeOut(New System.EventArgs)

        End Sub

    End Class

End Namespace