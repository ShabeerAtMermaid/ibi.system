Namespace Net

    Public Class IPRange

#Region "Variables"

        Private _StartRange As System.Net.IPAddress = Converter.ToIPAddress("0.0.0.0")
        Private _EndRange As System.Net.IPAddress = Converter.ToIPAddress("255.255.255.255")

#End Region

#Region "Properties"

        Public Property StartRange() As System.Net.IPAddress
            Get
                Return Me._StartRange

            End Get
            Set(ByVal value As System.Net.IPAddress)
                Me._StartRange = value

            End Set
        End Property

        Public Property EndRange() As System.Net.IPAddress
            Get
                Return Me._EndRange

            End Get
            Set(ByVal value As System.Net.IPAddress)
                Me._EndRange = value

            End Set
        End Property

#End Region

        Public Sub New()

        End Sub

        Public Sub New(ByVal startRange As System.Net.IPAddress, ByVal endRange As System.Net.IPAddress)
            Me.StartRange = startRange
            Me.EndRange = endRange

        End Sub

        Public Function IsWithinRange(ByVal address As System.Net.IPAddress) As Boolean
            Dim tmpRangeStart As Long = Me.ConvertToComparableValue(Me.StartRange)
            Dim tmpRangeEnd As Long = Me.ConvertToComparableValue(Me.EndRange)
            Dim tmpCompareAddress As Long = Me.ConvertToComparableValue(address)

            If tmpCompareAddress >= tmpRangeStart And tmpCompareAddress <= tmpRangeEnd Then
                Return True

            End If

        End Function

        Private Function ConvertToComparableValue(ByVal address As System.Net.IPAddress) As Long
            Dim tmpSplitList() As String = address.ToString.Split(".")
            Dim tmpComparableValueString As String = String.Empty

            For i As Integer = 0 To tmpSplitList.Length - 1
                tmpComparableValueString &= Functions.ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 3, tmpSplitList(i))

            Next

            Return Converter.ToInt64(tmpComparableValueString)

        End Function
        
        Public Shadows Function Equals(ByVal obj As Object) As Boolean
            If obj IsNot Nothing Then
                If TypeOf obj Is IPRange Then
                    Dim tmpCompareAddress As IPRange = obj

                    If Me.StartRange.Equals(tmpCompareAddress.StartRange) Then
                        If Me.EndRange.Equals(tmpCompareAddress.EndRange) Then
                            Return True

                        End If
                    End If
                End If
            End If

            Return False

        End Function

        Public Overrides Function ToString() As String
            Return Me.StartRange.ToString & " - " & Me.EndRange.ToString

        End Function

    End Class

End Namespace