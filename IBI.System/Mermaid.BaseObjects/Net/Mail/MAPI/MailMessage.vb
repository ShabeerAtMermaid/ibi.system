Imports System.Xml

Namespace Net.Mail.MAPI

    Public Class MailMessage

#Region "Variables"

        Private _Connection As ExchangeConnection
        Private _XMLData As XmlElement

#End Region

#Region "Properties"

        Private Property Connection() As ExchangeConnection
            Get
                Return Me._Connection

            End Get
            Set(ByVal value As ExchangeConnection)
                Me._Connection = value

            End Set
        End Property

        Private Property XMLData() As XmlElement
            Get
                Return Me._XMLData

            End Get
            Set(ByVal value As XmlElement)
                Me._XMLData = value

            End Set
        End Property

        Public ReadOnly Property Subject() As String
            Get
                Return Me.GetInnerText("d:subject")

            End Get
        End Property

        Public ReadOnly Property FromName() As String
            Get
                Return Me.GetInnerText("e:sendername")

            End Get
        End Property

        Public ReadOnly Property Read() As Boolean
            Get
                Return Converter.ToBoolean(Me.GetInnerText("e:read"))

            End Get
        End Property

        Public ReadOnly Property Flag() As MessageFlags
            Get
                Dim tmpFlagString As String = Me.GetInnerText("d:x-message-flag")

                If String.IsNullOrEmpty(tmpFlagString) Then
                    tmpFlagString = Me.GetInnerText("d:x-message-completed")

                    If Not String.IsNullOrEmpty(tmpFlagString) Then
                        tmpFlagString = MessageFlags.Completed.ToString

                    End If
                End If

                If String.IsNullOrEmpty(tmpFlagString) Then
                    Return MessageFlags.None

                Else
                    Return Converter.ConvertToType(tmpFlagString.Replace(" ", String.Empty), GetType(MessageFlags))

                End If
            End Get
        End Property

        Public ReadOnly Property URL() As String
            Get
                Return Me.GetInnerText("a:href")

            End Get
        End Property

#Region "Enums"

        Public Enum MessageFlags As Integer
            None = 0
            FollowUp = 1
            Completed = 2

        End Enum

#End Region

#End Region

        Friend Sub New(ByVal connection As ExchangeConnection, ByVal xmlData As XmlElement)
            MyBase.New()

            Me.Connection = connection
            Me.XMLData = xmlData

        End Sub

        Public Sub Delete()
            With Me.Connection.CommunicationObject
                .open("DELETE", Me.URL, False, Me.Connection.User, Me.Connection.Password)
                .setRequestHeader("Depth", "infinity")
                .send()

            End With

        End Sub

#Region "Helper functions"

        Private Function GetInnerText(ByVal tagName As String) As String
            Dim tmpElement As XmlElement = Nothing
            Dim tmpNodeList As XmlNodeList = Me.XMLData.GetElementsByTagName(tagName)

            If tmpNodeList.Count > 0 Then
                Return tmpNodeList(0).InnerText

            End If

            Return String.Empty

        End Function

#End Region

    End Class

End Namespace