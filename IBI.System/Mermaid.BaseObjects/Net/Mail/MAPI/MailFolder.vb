Imports System.Xml
Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports mermaid.BaseObjects

Namespace Net.Mail.MAPI

    Public Class MailFolder

#Region "Variables"

        Private _Connection As ExchangeConnection
        Private _XMLData As XmlDataDocument
        Private _FullContentLoaded As Boolean

#End Region

#Region "Properties"

        Private Property Connection() As ExchangeConnection
            Get
                Return Me._Connection

            End Get
            Set(ByVal value As ExchangeConnection)
                Me._Connection = value

            End Set
        End Property

        Private Property XMLData() As XmlDataDocument
            Get
                Return Me._XMLData

            End Get
            Set(ByVal value As XmlDataDocument)
                Me._XMLData = value

            End Set
        End Property

        Private Property FullContentLoaded() As Boolean
            Get
                Return Me._FullContentLoaded

            End Get
            Set(ByVal value As Boolean)
                Me._FullContentLoaded = value

            End Set
        End Property

        Public ReadOnly Property Name() As String
            Get
                Return Me.GetInnerText("a:displayname")

            End Get
        End Property

        Public ReadOnly Property URL() As String
            Get
                Return Me.GetInnerText("a:href")

            End Get
        End Property

        Public ReadOnly Property Folders() As ReadOnlyCollection(Of MailFolder)
            Get
                Me.LoadFullContent()

                Dim tmpList As New List(Of MailFolder)

                Dim xmlNdLstMails As XmlNodeList = Me.XMLData.GetElementsByTagName("a:response")

                For Each tmpMailNode As XmlElement In xmlNdLstMails
                    Dim tmpContentClass As String = tmpMailNode.GetElementsByTagName("a:contentclass")(0).InnerText

                    If String.Compare(tmpContentClass, "urn:content-classes:mailfolder", True) = 0 Then
                        Dim tmpXMLDataDoc As XmlDataDocument = New XmlDataDocument
                        tmpXMLDataDoc.LoadXml(tmpMailNode.OuterXml)

                        If Not IsMe(tmpXMLDataDoc) Then
                            tmpList.Add(New MailFolder(Me.Connection, tmpXMLDataDoc, False))

                        End If
                    End If

                Next

                Return New ReadOnlyCollection(Of MailFolder)(tmpList)

            End Get
        End Property

        Public ReadOnly Property Mails() As ReadOnlyCollection(Of MailMessage)
            Get
                Me.LoadFullContent()

                Dim tmpList As New List(Of MailMessage)

                Dim xmlNdLstMails As XmlNodeList = Me.XMLData.GetElementsByTagName("a:response")

                For Each tmpMailNode As XmlElement In xmlNdLstMails
                    Dim tmpContentClass As String = tmpMailNode.GetElementsByTagName("a:contentclass")(0).InnerText

                    If String.Compare(tmpContentClass, "urn:content-classes:message", True) = 0 Then
                        tmpList.Add(New MailMessage(Me.Connection, tmpMailNode))

                    End If

                Next

                Return New ReadOnlyCollection(Of MailMessage)(tmpList)

            End Get
        End Property

        Public ReadOnly Property UnreadMailCount() As Integer
            Get
                Return Converter.ToInt32(Me.GetInnerText("e:unreadcount"))

            End Get
        End Property

        Public ReadOnly Property Size() As DataTypes.FileSize
            Get
                Return New DataTypes.FileSize(Converter.ToInt64(Me.GetInnerText("f:foldersize")))

            End Get
        End Property

#End Region

        Friend Sub New(ByVal connection As ExchangeConnection, ByVal xmlData As XmlDataDocument, ByVal fullContent As Boolean)
            MyBase.New()

            Me.Connection = connection
            Me.XMLData = xmlData
            Me.FullContentLoaded = fullContent

        End Sub

        Private Sub LoadFullContent()
            System.Threading.Monitor.Enter(Me)

            Try
                If Not Me.FullContentLoaded Then
                    If Not String.IsNullOrEmpty(Me.URL) Then
                        Dim xmlDOMParams As New System.Xml.XmlDataDocument

                        With Me.Connection.CommunicationObject
                            'Open and read all the mails of a folder
                            .open("PROPFIND", Me.URL & Name, False, Me.Connection.User, Me.Connection.Password)
                            .setRequestHeader("Depth", "1")
                            .setRequestHeader("Content-type", "xml")
                            .setRequestHeader("Timeout", "Infinite")
                            .send()

                            'Load the read mails into XML document
                            xmlDOMParams.LoadXml(.responseText)

                            Me.XMLData = xmlDOMParams

                        End With

                        Me.FullContentLoaded = True

                    End If
                End If
            Finally
                System.Threading.Monitor.Exit(Me)

            End Try

        End Sub

#Region "Helper functions"

        Private Function GetInnerText(ByVal tagName As String) As String
            Dim tmpElement As XmlElement = Nothing
            Dim tmpNodeList As XmlNodeList = Me.XMLData.GetElementsByTagName(tagName)

            If tmpNodeList.Count > 0 Then
                Return tmpNodeList(0).InnerText

            End If

            Return String.Empty

        End Function

        Private Function IsMe(ByVal xmlData As XmlDataDocument) As Boolean
            Dim tmpElement As XmlElement = Nothing
            Dim tmpNodeList As XmlNodeList = xmlData.GetElementsByTagName("a:href")

            If tmpNodeList.Count > 0 Then
                If String.Compare(tmpNodeList(0).InnerText, Me.URL, True) = 0 Then
                    Return True

                End If
            End If

            Return False

        End Function

#End Region

    End Class

End Namespace