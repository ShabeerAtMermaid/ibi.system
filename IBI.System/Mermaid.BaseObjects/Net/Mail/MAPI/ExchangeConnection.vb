Namespace Net.Mail.MAPI

    Public Class ExchangeConnection

#Region "Variables"

        Private _URL As String
        Private _User As String
        Private _Password As String
        Private _Type As ConnectionTypes = ConnectionTypes.Mailbox
        Private _MailboxName As String

#End Region

#Region "Properties"

        Public Property URL() As String
            Get
                Return Me._URL

            End Get
            Set(ByVal value As String)
                Me._URL = value

            End Set
        End Property

        Public Property User() As String
            Get
                Return Me._User

            End Get
            Set(ByVal value As String)
                Me._User = value

            End Set
        End Property

        Public Property Password() As String
            Get
                Return Me._Password

            End Get
            Set(ByVal value As String)
                Me._Password = value

            End Set
        End Property

        Public Property Type() As ConnectionTypes
            Get
                Return Me._Type

            End Get
            Set(ByVal value As ConnectionTypes)
                Me._Type = value

            End Set
        End Property

        Public Property MailboxName() As String
            Get
                If String.IsNullOrEmpty(Me._MailboxName) Then
                    Return Me.User

                Else
                    Return Me._MailboxName

                End If

            End Get
            Set(ByVal value As String)
                Me._MailboxName = value

            End Set
        End Property

        Friend ReadOnly Property CommunicationObject() As Object 'MSXML2.ServerXMLHTTP40
            Get
                Return New Object  'MSXML2.ServerXMLHTTP40

            End Get
        End Property

#End Region

#Region "Enums"

        Public Enum ConnectionTypes As Integer
            Mailbox = 0
            PublicFolders = 1

        End Enum

#End Region

        Public Sub New(ByVal url As String, ByVal user As String, ByVal password As String)
            MyBase.New()

            Me.URL = url
            Me.User = user
            Me.Password = password

        End Sub

        Public Function GetFolder(ByVal name As String)
            Dim xmlDOMParams As New System.Xml.XmlDataDocument

            With Me.CommunicationObject
                'Open and read all the mails of a folder
                Select Case Me.Type
                    Case ConnectionTypes.Mailbox
                        .open("PROPFIND", "http://" & Me.URL & "/exchange/" & Me.MailboxName & "/" & name, False, Me.User, Me.Password)

                    Case ConnectionTypes.PublicFolders
                        .open("PROPFIND", "http://" & Me.URL & "/public/" & name, False, Me.User, Me.Password)

                End Select

                .setRequestHeader("Depth", "1")
                .setRequestHeader("Content-type", "xml")
                .setRequestHeader("Timeout", "Infinite")
                .send()

                'Load the read mails into XML document
                xmlDOMParams.LoadXml(.responseText)

                Return New MailFolder(Me, xmlDOMParams, True)

            End With

            'Dim xmlDOMParams As New System.Xml.XmlDataDocument

            'With Me.CommunicationObject
            '    Dim tmpQueryLocation As String = String.Empty

            '    Select Case Me.Type
            '        Case ConnectionTypes.Mailbox
            '            tmpQueryLocation = "http://" & Me.URL & "/exchange/" & Me.MailboxName & "/" & name

            '        Case ConnectionTypes.PublicFolders
            '            tmpQueryLocation = "http://" & Me.URL & "/public/" & name

            '    End Select

            '    .open("SEARCH", tmpQueryLocation, False, Me.User, Me.Password)

            '    Dim sSql As New System.Text.StringBuilder()
            '    sSql.Append("SELECT ""DAV:displayname"", ""DAV:href"", ""DAV:contentclass""")
            '    sSql.Append(" FROM scope ('deep traversal of """)
            '    sSql.Append(tmpQueryLocation)
            '    sSql.Append("""')")
            '    sSql.Append(" WHERE ""DAV:ishidden"" = false")

            '    Dim sReq As New System.Text.StringBuilder()
            '    sReq.Append("<?xml version='1.0'?><d:searchrequest xmlns:d='DAV:'>")
            '    sReq.Append("<d:sql>")
            '    sReq.Append(sSql.ToString)
            '    sReq.Append("</d:sql></d:searchrequest>")

            '    .setRequestHeader("Range", "rows=1-20")
            '    .setRequestHeader("Content-type", "text/xml")
            '    .setRequestHeader("Depth", "1")

            '    .send(sReq.ToString)

            '    xmlDOMParams.LoadXml(.responseText)

            '    Return New MailFolder(Me, xmlDOMParams, True)

            'End With

        End Function

    End Class

End Namespace