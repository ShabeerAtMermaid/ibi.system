Imports System.Net.Sockets

Namespace Net.FTP
    Class ProxySocks4
        Inherits ProxySocks4Base

        Public Sub New(ByVal socket As Socket)
            MyBase.New(socket, False)
        End Sub
    End Class
End Namespace
