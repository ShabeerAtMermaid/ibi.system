Imports System
Imports System.Net

Namespace Net.FTP
    Public Class FtpException
        Inherits InvalidOperationException
        Private _status As FtpExceptionStatus
        Private _response As FtpReply
        Private _transfered As Long

        Public ReadOnly Property Status() As FtpExceptionStatus
            Get
                Return _status
            End Get
        End Property

        Public ReadOnly Property Response() As FtpReply
            Get
                Return _response
            End Get
        End Property

        Public ReadOnly Property Transfered() As Long
            Get
                Return _transfered
            End Get
        End Property

        Public Sub New()
            MyClass.New("UnclassifiableError", Nothing, FtpExceptionStatus.UnclassifiableError)
        End Sub

        Public Sub New(ByVal message As String)
            MyClass.New(message, Nothing, FtpExceptionStatus.UnclassifiableError)
        End Sub

        Public Sub New(ByVal message As String, ByVal status As FtpExceptionStatus)
            MyClass.New(message, Nothing, status)
        End Sub

        Friend Sub New(ByVal e As FtpException)
            MyBase.New(e.Message, e.InnerException)
            _status = e.Status
            _response = e.Response
            _transfered = 0
        End Sub

        Friend Sub New(ByVal e As FtpException, ByVal transfered As Long)
            MyBase.New(e.Message, e.InnerException)
            _status = e.Status
            _response = e.Response
            _transfered = transfered
        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyClass.New(message, innerException, FtpExceptionStatus.UnclassifiableError)
        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As Exception, ByVal status As FtpExceptionStatus)
            MyBase.New(message, innerException)
            If status = FtpExceptionStatus.ProtocolError Then
                _status = FtpExceptionStatus.UnclassifiableError
                Return
            End If
            _status = status
        End Sub

        Friend Sub New(ByVal message As String, ByVal inner As FtpException, ByVal status As FtpExceptionStatus, ByVal transfered As Long)
            MyClass.New(message, inner, status)
            _transfered = transfered
        End Sub

        Public Sub New(ByVal response As FtpReply)
            MyBase.New(String.Concat(New Object() {response.Description.Trim(New Char() {Chr(32), Chr(46)}), " (", response.Code, ")."}))
            If Not (response Is Nothing) Then
                _response = response
                _status = FtpExceptionStatus.ProtocolError
                Return
            End If
            _status = FtpExceptionStatus.UnclassifiableError
        End Sub

        Friend Function ToWebException() As WebException
            Dim webExceptionStatus As WebExceptionStatus
            Select Case _status
                Case FtpExceptionStatus.ConnectFailure
                    webExceptionStatus = webExceptionStatus.ConnectFailure
                Case FtpExceptionStatus.ConnectionClosed
                    webExceptionStatus = webExceptionStatus.ConnectionClosed
                Case FtpExceptionStatus.NameResolutionFailure
                    webExceptionStatus = webExceptionStatus.NameResolutionFailure
                Case FtpExceptionStatus.Pending
                    webExceptionStatus = webExceptionStatus.Pending
                Case FtpExceptionStatus.ProtocolError
                    webExceptionStatus = webExceptionStatus.ProtocolError
                Case FtpExceptionStatus.ProxyNameResolutionFailure
                    webExceptionStatus = webExceptionStatus.ProxyNameResolutionFailure
                Case FtpExceptionStatus.ReceiveFailure
                    webExceptionStatus = webExceptionStatus.ReceiveFailure
                Case FtpExceptionStatus.SendFailure
                    webExceptionStatus = webExceptionStatus.SendFailure
                Case FtpExceptionStatus.ServerProtocolViolation
                    webExceptionStatus = webExceptionStatus.ServerProtocolViolation
                Case FtpExceptionStatus.Timeout
                    webExceptionStatus = webExceptionStatus.Timeout
                Case Else
                    webExceptionStatus = webExceptionStatus.RequestCanceled
            End Select
            If Not (_status = FtpExceptionStatus.AsyncError) Then
                Return New WebException(MyBase.Message, Me, webExceptionStatus, Nothing)
            Else
                Return New WebException(MyBase.Message, MyBase.InnerException, webExceptionStatus, Nothing)
            End If
        End Function
    End Class
End Namespace