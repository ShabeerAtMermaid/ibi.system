Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Text.RegularExpressions
Imports System.Threading

Namespace Net.FTP
    Public Class ProxySocket
        Private _proxy As ProxyBase = Nothing
        Private _socket As Socket = Nothing
        Private _type As ProxyType = ProxyType.None
        Private _userName As String = ""
        Private _password As String = ""
        Private _host As String = Nothing
        Private _port As Integer = 21
        Private Shared _getInitialized As Boolean = False
        Private Shared _getAddressBytes As MethodInfo = Nothing
        Private Shared _getAddress As PropertyInfo = Nothing

        Public Property ProxyPort() As Integer
            Get
                Return _port
            End Get
            Set(ByVal Value As Integer)
                If Value < 1 OrElse Value > 65535 Then
                    Throw New ArgumentOutOfRangeException("value")
                End If
                _port = Value
            End Set
        End Property

        Public Property ProxyType() As ProxyType
            Get
                Return _type
            End Get
            Set(ByVal Value As ProxyType)
                If Not [Enum].IsDefined(GetType(ProxyType), Value) Then
                    Throw New ArgumentOutOfRangeException("value")
                End If
                _type = Value
            End Set
        End Property

        Public ReadOnly Property Socket() As Socket
            Get
                Return _socket
            End Get
        End Property

        Public ReadOnly Property RemoteEndPoint() As IPEndPoint
            Get
                Return CType(_socket.RemoteEndPoint, IPEndPoint)
            End Get
        End Property

        Public ReadOnly Property LocalEndPoint() As IPEndPoint
            Get
                Return CType(_socket.LocalEndPoint, IPEndPoint)
            End Get
        End Property

        Public ReadOnly Property Connected() As Boolean
            Get
                Return _socket.Connected
            End Get
        End Property

        Public ReadOnly Property Available() As Integer
            Get
                Return _socket.Available
            End Get
        End Property

        Public ReadOnly Property Handle() As IntPtr
            Get
                Return _socket.Handle
            End Get
        End Property

        Public Sub New()
            _socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        End Sub

        Private Sub New(ByVal socket As Socket)
            _socket = socket
        End Sub

        Public Sub New(ByVal proxyType As ProxyType, ByVal host As String, ByVal port As Integer)
            MyClass.New()
            proxyType = proxyType
            _host = host
            ProxyPort = port
        End Sub

        Public Sub New(ByVal proxyType As ProxyType, ByVal host As String, ByVal port As Integer, ByVal username As String)
            MyClass.New()
            proxyType = proxyType
            _host = host
            ProxyPort = port
            _userName = username
        End Sub

        Public Sub New(ByVal proxyType As ProxyType, ByVal host As String, ByVal port As Integer, ByVal username As String, ByVal password As String)
            MyClass.New()
            proxyType = proxyType
            _host = host
            ProxyPort = port
            _userName = username
            _password = password
        End Sub

        Public Function Send(ByVal buffer As Byte()) As Integer
            Return _socket.Send(buffer, 0, buffer.Length, SocketFlags.None)
        End Function

        Public Function Send(ByVal buffer As Byte(), ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Send(buffer, 0, buffer.Length, socketFlags)
        End Function

        Public Function Send(ByVal buffer As Byte(), ByVal size As Integer, ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Send(buffer, 0, size, socketFlags)
        End Function

        Public Function Send(ByVal buffer As Byte(), ByVal offset As Integer, ByVal size As Integer, ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Send(buffer, offset, size, socketFlags)
        End Function

        Public Function BeginSend(ByVal buffer As Byte(), ByVal offset As Integer, ByVal size As Integer, ByVal socketFlags As SocketFlags, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Return _socket.BeginSend(buffer, offset, size, socketFlags, callback, state)
        End Function

        Public Function Receive(ByVal buffer As Byte()) As Integer
            Return _socket.Receive(buffer, 0, buffer.Length, SocketFlags.None)
        End Function

        Public Function Receive(ByVal buffer As Byte(), ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Receive(buffer, 0, buffer.Length, socketFlags)
        End Function

        Public Function Receive(ByVal buffer As Byte(), ByVal size As Integer, ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Receive(buffer, 0, size, socketFlags)
        End Function

        Public Function Receive(ByVal buffer As Byte(), ByVal offset As Integer, ByVal size As Integer, ByVal socketFlags As SocketFlags) As Integer
            Return _socket.Receive(buffer, offset, size, socketFlags)
        End Function

        Public Function BeginReceive(ByVal buffer As Byte(), ByVal offset As Integer, ByVal size As Integer, ByVal socketFlags As SocketFlags, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Return _socket.BeginReceive(buffer, offset, size, socketFlags, callback, state)
        End Function

        Public Function EndSend(ByVal asyncResult As IAsyncResult) As Integer
            Return _socket.EndSend(asyncResult)
        End Function

        Public Function EndReceive(ByVal asyncResult As IAsyncResult) As Integer
            Return _socket.EndReceive(asyncResult)
        End Function

        Public Sub Close()
            _socket.Close()
        End Sub

        Public Sub Shutdown(ByVal how As SocketShutdown)
            _socket.Shutdown(how)
        End Sub

        Friend Shared Function ToEndPoint(ByVal host As String, ByVal port As Integer) As IPEndPoint
            Dim match As Match = (New Regex("^\s*([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\s*$")).Match(host)
            If Not match.Success OrElse Not (match.Groups.Count = 5) Then
                Return Nothing
            End If
            Dim i As Integer = 0
            Dim j As Integer = 0
            While j < 4
                Dim k As Integer = Integer.Parse(match.Groups(j + 1).Value)
                If k > 255 Then
                    Return Nothing
                End If
                i = i Or (k << (8 * j And 31))
                j += 1
            End While
            Return New IPEndPoint(i, port)
        End Function

        Private Shared Sub InitGetAddress()
            If _getInitialized Then
                Return
            End If
            _getAddress = GetType(IPAddress).GetProperty("Address", GetType(Long))
            _getInitialized = True
        End Sub

        Public Shared Function GetAddressLong(ByVal address As IPAddress) As Long
            InitGetAddress()
            If address Is Nothing Then
                Throw New ArgumentNullException("address")
            End If
            If Not (_getAddress Is Nothing) Then
                Return CType(_getAddress.GetValue(address, Nothing), Long)
            End If
            If _getAddressBytes Is Nothing Then
                Throw New NotSupportedException
            End If
            Dim bs As Byte() = CType(_getAddressBytes.Invoke(address, Nothing), Byte())
            If Not (CType(bs.Length, Integer) = 4) Then
                Throw New ArgumentOutOfRangeException("address")
            Else
                Return bs(0) + (bs(1) << 8) + (bs(2) << 16) + (bs(3) << 24)
            End If
        End Function

        Public Shared Function GetAddressBytes(ByVal address As IPAddress) As Byte()
            InitGetAddress()
            If address Is Nothing Then
                Throw New ArgumentNullException("address")
            End If
            If Not (_getAddressBytes Is Nothing) Then
                Return CType(_getAddressBytes.Invoke(address, Nothing), Byte())
            End If
            If _getAddress Is Nothing Then
                Throw New NotSupportedException
            End If
            Dim l As Long = CType(_getAddress.GetValue(address, Nothing), Long)
            Return New Byte() {CType((l And 255), Byte), CType((l >> 8 And CType(255, Long)), Byte), CType((l >> 16 And CType(255, Long)), Byte), CType((l >> 24 And CType(255, Long)), Byte)}
        End Function

        Public Function Listen(ByVal controlSocket As ProxySocket) As IPEndPoint
            Dim iAsyncResult As IAsyncResult = BeginListen(controlSocket, Nothing, Nothing)
            Return EndListen(iAsyncResult)
        End Function

        Public Sub Connect(ByVal serverName As String, ByVal serverPort As Integer)
            Dim iAsyncResult As IAsyncResult = BeginConnect(serverName, serverPort, Nothing, Nothing)
            EndConnect(iAsyncResult)
        End Sub

        Public Sub Connect(ByVal remoteEP As IPEndPoint)
            Dim iAsyncResult As IAsyncResult = BeginConnect(remoteEP, Nothing, Nothing)
            EndConnect(iAsyncResult)
        End Sub

        Public Sub Accept()
            Dim iAsyncResult As IAsyncResult = BeginAccept(Nothing, Nothing)
            EndAccept(iAsyncResult)
        End Sub

        Public Function BeginListen(ByVal controlSocket As ProxySocket, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If controlSocket Is Nothing Then
                Throw New ArgumentNullException("ArgumentNullSocket")
            End If
            If Not controlSocket.Connected Then
                Throw New ProxySocketException("ControlSocketNotConnected", ProxySocketExceptionStatus.NotConnected)
            End If
            Monitor.Enter(Me)
            Try
                If Not (_proxy Is Nothing) Then
                    Throw New InvalidOperationException("InvalidOperation")
                End If
                _proxy = ProxyBase.Create(_socket, controlSocket._proxy)
            Finally
                Monitor.Exit(Me)
            End Try
            Return _proxy.BeginListen(controlSocket._proxy, callback, state)
        End Function

        Public Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If _proxy Is Nothing Then
                Throw New InvalidOperationException("InvalidOperation")
            Else
                Return _proxy.BeginAccept(callback, state)
            End If
        End Function

        Public Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If serverName Is Nothing Then
                Throw New ArgumentNullException("serverName")
            End If
            If serverPort < 1 OrElse serverPort > 65535 Then
                Throw New ArgumentOutOfRangeException("serverPort")
            End If
            Monitor.Enter(Me)
            Try
                If Not (_proxy Is Nothing) Then
                    Throw New InvalidOperationException("InvalidOperation")
                End If
                _proxy = ProxyBase.Create(_socket, _type, _host, _port, _userName, _password)
            Finally
                Monitor.Exit(Me)
            End Try
            Dim iPEndPoint As IPEndPoint = ToEndPoint(serverName, serverPort)
            If Not (iPEndPoint Is Nothing) Then
                Return _proxy.BeginConnect(iPEndPoint, callback, state)
            Else
                Return _proxy.BeginConnect(serverName, serverPort, callback, state)
            End If
        End Function

        Public Function BeginConnect(ByVal remoteEP As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If remoteEP Is Nothing Then
                Throw New ArgumentNullException("remoteEP")
            End If
            Monitor.Enter(Me)
            Try
                If Not (_proxy Is Nothing) Then
                    Throw New InvalidOperationException("InvalidOperation")
                End If
                _proxy = ProxyBase.Create(_socket, _type, _host, _port, _userName, _password)
            Finally
                Monitor.Exit(Me)
            End Try
            Return _proxy.BeginConnect(remoteEP, callback, state)
        End Function

        Public Sub EndConnect(ByVal asyncResult As IAsyncResult)
            _proxy.EndMethod(asyncResult, "Connect")
        End Sub

        Public Sub EndAccept(ByVal asyncResult As IAsyncResult)
            _proxy.EndMethod(asyncResult, "Accept")
            _socket = _proxy.Socket
        End Sub

        Public Function EndListen(ByVal asyncResult As IAsyncResult) As IPEndPoint
            Return CType(_proxy.EndMethod(asyncResult, "Listen"), IPEndPoint)
        End Function
    End Class
End Namespace