#Region "See http://www.faqs.org/rfcs/rfc959.html"
#End Region

Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Threading
Imports System.Reflection
Imports System.Net.Sockets
Imports System.Collections
Imports System.Collections.Generic
Imports System.Text.RegularExpressions

Namespace Net.FTP

    Public Enum FtpTransferState As Integer
        None = 0
        Uploading = 1
        Downloading = 2
    End Enum

    <FlagsAttribute()> _
    Public Enum FtpOptions As Integer
        None = 0
        DoNotSendSignals = 1
        DoNotSendAbort = 2
    End Enum

    Public Enum FtpState As Integer
        Disconnected = 0
        Connecting = 1
        Ready = 2
        Sending = 3
        Reading = 4
        Processing = 5
        Disposed = 6
    End Enum

    Public Enum FtpTransferType As Integer
        UTF8 = 0
        Binary = 1
    End Enum

    Public Class FtpCommandSentEventArgs
        Inherits System.EventArgs
        Private _command As String

        Public ReadOnly Property Command() As String
            Get
                Return _command
            End Get
        End Property

        Public Sub New(ByVal command As String)
            _command = command
        End Sub
    End Class

    Public Class FtpReplyReadEventArgs
        Inherits System.EventArgs
        Private _response As String

        Public ReadOnly Property Response() As String
            Get
                Return _response
            End Get
        End Property

        Public Sub New(ByVal response As String)
            _response = response
        End Sub
    End Class

    Public Class FtpTransferProgressEventArgs
        Inherits System.EventArgs
        Private _state As FtpTransferState
        Private _bytesTransfered As Long
        Private _bytesSinceLastEvent As Integer

        Public ReadOnly Property State() As FtpTransferState
            Get
                Return _state
            End Get
        End Property

        Public ReadOnly Property BytesTransfered() As Long
            Get
                Return _bytesTransfered
            End Get
        End Property

        Public ReadOnly Property BytesSinceLastEvent() As Long
            Get
                Return _bytesSinceLastEvent
            End Get
        End Property

        Public Sub New(ByVal state As FtpTransferState, ByVal bytesTransfered As Long)
            _state = state
            If state = FtpTransferState.None Then
                _bytesTransfered = 0
                _bytesSinceLastEvent = 0
                Return
            End If
            _bytesTransfered = bytesTransfered
            _bytesSinceLastEvent = 0
        End Sub

        Public Sub New(ByVal state As FtpTransferState, ByVal bytesTransfered As Long, ByVal bytesSinceLastEvent As Integer)
            _state = state
            If state = FtpTransferState.None Then
                _bytesTransfered = 0
                _bytesSinceLastEvent = 0
                Return
            End If
            _bytesTransfered = bytesTransfered
            _bytesSinceLastEvent = bytesSinceLastEvent
        End Sub
    End Class

    Public Class FtpStateChangedEventArgs
        Inherits System.EventArgs
        Private _oldState As FtpState
        Private _newState As FtpState

        Public ReadOnly Property OldState() As FtpState
            Get
                Return _oldState
            End Get
        End Property

        Public ReadOnly Property NewState() As FtpState
            Get
                Return _newState
            End Get
        End Property

        Public Sub New(ByVal oldState As FtpState, ByVal newState As FtpState)
            _oldState = oldState
            _newState = newState
        End Sub
    End Class

    Public Delegate Sub FtpReplyReadEventHandler(ByVal sender As Object, ByVal e As FtpReplyReadEventArgs)
    Public Delegate Sub FtpTransferProgressEventHandler(ByVal sender As Object, ByVal e As FtpTransferProgressEventArgs)
    Public Delegate Sub FtpStateChangedEventHandler(ByVal sender As Object, ByVal e As FtpStateChangedEventArgs)
    Public Delegate Sub FtpCommandSentEventHandler(ByVal sender As Object, ByVal e As FtpCommandSentEventArgs)

    Public Enum FtpExceptionStatus As Integer
        ConnectFailure = 0
        ConnectionClosed = 1
        SocketError = 2
        NameResolutionFailure = 3
        Pending = 4
        ProtocolError = 5
        ProxyNameResolutionFailure = 6
        ReceiveFailure = 7
        OperationAborted = 8
        UnclassifiableError = 9
        SendFailure = 10
        ServerProtocolViolation = 11
        Timeout = 12
        AsyncError = 13
    End Enum

    Public Enum ProxySocketExceptionStatus As Integer
        ConnectFailure = 0
        ConnectionClosed = 1
        SocketError = 2
        NameResolutionFailure = 3
        ProtocolError = 4
        ProxyNameResolutionFailure = 5
        ReceiveFailure = 6
        UnclassifiableError = 7
        ServerProtocolViolation = 8
        yncError = 9
        NotConnected = 10
    End Enum

    Public Class ProxySocketException
        Inherits Exception
        Private Const WSAENOTCONN As Integer = 10057
        Private _status As ProxySocketExceptionStatus = ProxySocketExceptionStatus.UnclassifiableError
        Private _errorCode As Integer = 0

        Public ReadOnly Property Status() As ProxySocketExceptionStatus
            Get
                Return _status
            End Get
        End Property

        Public ReadOnly Property ErrorCode() As Integer
            Get
                Return _errorCode
            End Get
        End Property

        Public Sub New(ByVal message As String, ByVal status As ProxySocketExceptionStatus)
            MyBase.New(message)
            _status = status
        End Sub

        Public Sub New(ByVal message As String, ByVal status As ProxySocketExceptionStatus, ByVal innerException As Exception)
            MyBase.New(message, innerException)
            _status = status
        End Sub

        Public Sub New(ByVal e As SocketException)
            MyBase.New(GetSocketExceptionMessage(e), e)
            _errorCode = e.ErrorCode
            If e.ErrorCode = 10057 Then
                _status = ProxySocketExceptionStatus.NotConnected
                Return
            End If
            _status = ProxySocketExceptionStatus.SocketError
        End Sub

        Private Shared Function GetSocketExceptionMessage(ByVal e As SocketException) As String
            If e.ErrorCode = 10057 Then
                Return "NotConnected"
            Else
                Return String.Format("Socket error {0}: {1}", e.ErrorCode, e.Message)
            End If
        End Function
    End Class

    Public Class FtpService

        Friend Shared ReadOnly InvalidPathChars As Char() = New Char() {Chr(0), Chr(8)}
        '{'\0', '\b', '\u0010', '\u0011', '\u0012', '\u0014', '\u0015', '\u0016', '\u0017', '\u0018', '\u0019'};

        Private myState As FtpState
        Private myDefaultPort As Integer = 21

        Private _async As FtpAsyncResult = Nothing
        Private _locked As Boolean = False
        Private _aborting As Boolean = False
        Private _abortedTransfer As Boolean = True
        Private _sync As Object = New Object

        Private _timeout As Integer = 20000 '60000
        Private _abortTimeout As Integer = 3000
        Private _timerStart As Integer
        Private _proxy As FtpProxy = New FtpProxy
        Private _activeProxy As FtpProxy = Nothing

        Private _appendUser As String = Nothing
        Private _appendPassword As String = Nothing
        Private _encoding As System.Text.Encoding = System.Text.Encoding.Default
        Private _data As FtpData = Nothing
        Private _control As Ftp = Nothing
        Private _passive As Boolean = False
        Private _lastOffset As Long = 0
        Private _transferState As FtpTransferState = FtpTransferState.None
        Private _options As FtpOptions = FtpOptions.None

        Public Property Encoding() As System.Text.Encoding
            Get
                Return _encoding
            End Get
            Set(ByVal Value As System.Text.Encoding)
                If Value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _encoding = Value
            End Set
        End Property

        Public Property Proxy() As FtpProxy
            Get
                Return _proxy
            End Get
            Set(ByVal Value As FtpProxy)
                If Value Is Nothing Then
                    Throw New ArgumentNullException("ArgumentNullProxy")
                End If
                _proxy = Value
            End Set
        End Property

        Friend ReadOnly Property ActiveProxy() As FtpProxy
            Get
                Return _activeProxy
            End Get
        End Property

        Public ReadOnly Property State() As FtpState
            Get
                Return myState
            End Get
        End Property

        Public Property DefaultPort() As Integer
            Get
                Return myDefaultPort
            End Get
            Set(ByVal Value As Integer)
                myDefaultPort = Value
            End Set
        End Property

        Public Property Timeout() As Integer
            Get
                Return _timeout
            End Get
            Set(ByVal Value As Integer)
                If Value < -1 Then
                    Throw New ArgumentOutOfRangeException("value")
                End If
                If Value < 1 Then
                    Value = -1
                Else
                    If Value < 1000 Then
                        Value = 1000
                    End If
                End If
                Lock()
                _timeout = Value
                Unlock()
            End Set
        End Property

        Public Property AbortTimeout() As Integer
            Get
                Return _abortTimeout
            End Get
            Set(ByVal Value As Integer)
                If Value < -1 Then
                    Throw New ArgumentOutOfRangeException("value")
                End If
                If Value < 1000 Then
                    Value = 1000
                End If
                Lock()
                _abortTimeout = Value
                Unlock()
            End Set
        End Property

        Public Property Passive() As Boolean
            Get
                Return Me._passive

            End Get
            Set(ByVal value As Boolean)
                Me._passive = value

            End Set
        End Property

        Public Shared ReadOnly Property DateTimeFormats() As List(Of String)
            Get
                Return Parser.DateTimeFormats

            End Get
        End Property

        Public Event ResponseRead As FtpReplyReadEventHandler
        Public Event TransferProgress As FtpTransferProgressEventHandler
        Public Event CommandSent As FtpCommandSentEventHandler
        Public Event StateChanged As FtpStateChangedEventHandler


        Public Sub New()

        End Sub

        Public Function GetFileLength(ByVal remotePath As String) As Long
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            Else
                Return Parser.ParseFileLength(Execute(String.Concat("SIZE ", remotePath)).ReplyData)
            End If
        End Function

        Public Function GetCurrentDirectory() As String
            Dim FtpReply As FtpReply
            Lock()
            Try
                SendCommand("PWD")
                FtpReply = GetReply(0)
            Finally
                Unlock()
            End Try
            Return Parser.ParseCurrentDirectory(FtpReply.ReplyData)
            'Return Execute("PWD").ReplyData

        End Function

        Friend Sub ReportResponseRead(ByVal line As String)
            RaiseEvent ResponseRead(Me, New FtpReplyReadEventArgs(line))
        End Sub

        Public Sub SetTransferType(ByVal type As FtpTransferType)
            Dim ch As Char
            If type = FtpTransferType.UTF8 Then
                ch = "A"c
            ElseIf type = FtpTransferType.Binary Then
                ch = "I"c
            End If
        End Sub

        Private Sub Lock()
            'Dim local As Object
            Monitor.Enter(_sync)
            Try
                'If _locked Then
                '  Throw New FtpException("OperationPending"), FtpExceptionStatus.Pending)
                'End If
                'If myState = FtpState.Disposed Then
                '  Throw New ObjectDisposedException("Ftp", "IsDisposed"))
                'End If
                'If Not (_async Is Nothing) AndAlso Not (_async.Thread Is Thread.CurrentThread) Then
                '  Throw New FtpException("OperationPending"), FtpExceptionStatus.Pending)
                'End If
                _locked = True
            Finally
                Monitor.Exit(_sync)
            End Try
        End Sub

        Private Sub Unlock()
            Monitor.Enter(_sync)
            Try
                If Not _locked Then
                    Throw New InvalidOperationException("NotLocked")
                End If
                _locked = False
                _aborting = False
                _abortedTransfer = False
                _async = Nothing
            Finally
                Monitor.Exit(_sync)
            End Try
        End Sub

        Private Sub ResetTimeout()
            If Not _aborting Then
                _timerStart = Environment.TickCount
            End If
        End Sub

        Private Sub CheckTimeout()
            Monitor.Enter(_sync)
            Try
                Dim timeSpent As Integer = Environment.TickCount - _timerStart
                If _async Is Nothing AndAlso _timeout > 0 AndAlso timeSpent > _timeout Then
                    Throw New FtpException("Timeout", FtpExceptionStatus.Timeout)
                End If
                If _aborting AndAlso _abortTimeout > 0 AndAlso timeSpent > _abortTimeout Then
                    Throw New FtpException("AbortTimeout", FtpExceptionStatus.Timeout)
                End If
            Finally
                Monitor.Exit(_sync)
            End Try
        End Sub

        Friend Sub WaitForCompletion(ByVal result As IAsyncResult)
            While Not result.IsCompleted
                CheckTimeout()
                Thread.Sleep(1)
            End While
        End Sub

        Public Function Connect(ByVal serverName As String, ByVal serverPort As Integer) As String
            If serverName Is Nothing Or serverName.Trim().Length = 0 Then
                Throw New ArgumentNullException("serverName", "Server name must be a valid string")
            End If
            Lock()
            Try
                SetState(FtpState.Connecting)
                Dim urlString As String() = New String() {"ftp://", serverName, ":", serverPort}
                Dim url As Uri = New Uri(String.Concat(urlString))
                If _proxy.IsBypassed(url) Then
                    _activeProxy = New FtpProxy
                Else
                    _activeProxy = _proxy
                End If
                ResetTimeout()
                _control = New Ftp(Me, serverName, serverPort)
                SetState(FtpState.Reading)
                Dim FtpReply As FtpReply = GetReply(2)
                _appendUser = ""
                _appendPassword = ""
                Dim ftpProxyType1 As FtpProxyType = _activeProxy.ProxyType
                If Not (serverPort = 21) Then
                    serverName = String.Concat(serverName, ":", serverPort)
                End If
                Dim ftpProxyType2 As FtpProxyType = ftpProxyType1
                Select Case ftpProxyType2
                    Case 5
                        SendCommand(String.Concat("SITE ", serverName))
                        FtpReply = GetReply(2)
                        _activeProxy = New FtpProxy
                        _activeProxy.ProxyType = ftpProxyType1
                    Case 7
                        SendCommand(String.Concat("OPEN ", serverName))
                        FtpReply = GetReply(2)
                        _activeProxy = New FtpProxy
                        _activeProxy.ProxyType = ftpProxyType1
                    Case 6
                        _appendUser = String.Concat("@", serverName)
                        If Not (_activeProxy.UserName Is Nothing) AndAlso _activeProxy.UserName.Length > 0 Then
                            _appendUser = String.Concat("@", _activeProxy.UserName, _appendUser)
                            If Not (_activeProxy.Password Is Nothing) AndAlso _activeProxy.Password.Length > 0 Then
                                _appendPassword = String.Concat("@", _activeProxy.Password)
                            End If
                        End If
                        _activeProxy = New FtpProxy
                        _activeProxy.ProxyType = ftpProxyType1
                    Case Else
                        _activeProxy = _activeProxy.Clone
                End Select
                Return FtpReply.ReplyData
            Catch e1 As SocketException
                Throw New FtpException(e1.Message)
            Catch e2 As ProxySocketException
                Dim ftpExceptionStatus As FtpExceptionStatus
                Select Case e2.Status
                    Case ProxySocketExceptionStatus.ConnectFailure
                        ftpExceptionStatus = ftpExceptionStatus.ConnectFailure
                    Case ProxySocketExceptionStatus.NotConnected
                        ftpExceptionStatus = ftpExceptionStatus.NameResolutionFailure
                    Case ProxySocketExceptionStatus.ProxyNameResolutionFailure
                        ftpExceptionStatus = ftpExceptionStatus.ProxyNameResolutionFailure
                    Case ProxySocketExceptionStatus.ConnectionClosed
                        ftpExceptionStatus = ftpExceptionStatus.ConnectionClosed
                    Case Else
                        ftpExceptionStatus = ftpExceptionStatus.SocketError
                End Select
                Throw New FtpException(e2.Message, e2, ftpExceptionStatus)
            Finally
                If Not (myState = FtpState.Ready) Then
                    Reset()
                Else
                    Unlock()
                End If
            End Try
        End Function

        Public Function Disconnect() As String
            Lock()
            Dim flag As Boolean = IIf((Not (myState = FtpState.Ready)), (myState = FtpState.Processing = False), False)
            Dim FtpReply As FtpReply = Nothing
            Try
                SendCommand("QUIT")
                If Not flag Then
                    FtpReply = GetReply(2)
                End If
            Catch generatedExceptionVariable0 As FtpException
                If Not (_control Is Nothing) Then
                    FtpReply = _control.GetPartialResponse
                End If
            Finally
                Reset()
            End Try
            If FtpReply Is Nothing Then
                Return ""
            End If
            If FtpReply.ReplyData Is Nothing Then
                Return ""
            Else
                Return FtpReply.ReplyData
            End If
        End Function

        Public Function Connect(ByVal serverName As String) As String
            Return Me.Connect(serverName, Me.DefaultPort)
        End Function

        Private Sub SendCommand(ByVal command As String)
            Try
                SetState(FtpState.Sending)
                ResetTimeout()
                Dim bs As Byte() = Encoding.GetBytes(String.Concat(command, "" & vbCrLf))
                _control.Write(bs)
                OnCommandSent(New FtpCommandSentEventArgs(command))
                SetState(FtpState.Reading)
                Return
            Catch generatedExceptionVariable0 As FtpConnectionClosedException
                Reset()
                Throw New FtpException("ConnectionClosed", FtpExceptionStatus.ConnectionClosed)
            Catch e1 As SocketException
                Reset()
                Throw New FtpException(e1.Message, e1, FtpExceptionStatus.SendFailure)
            Catch e2 As ProxySocketException
                Dim ftpExceptionStatus As FtpExceptionStatus
                Reset()
                If e2.Status = 1 Then
                    ftpExceptionStatus = ftpExceptionStatus.ConnectionClosed
                Else
                    ftpExceptionStatus = ftpExceptionStatus.SendFailure
                End If
                Throw New FtpException(e2.Message, e2, ftpExceptionStatus)
            End Try
        End Sub

        Public Sub ChangeDirectory(ByVal remotePath As String)
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.Length = 0 Then
                Execute("CWD")
                Return
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath = "." Then
                Return
            End If
            If remotePath = ".." Then
                Execute("CDUP")
                Return
            End If
            Execute(String.Concat("CWD ", remotePath))
        End Sub

        Public Sub CreateDirectory(ByVal name As String)
            If String.IsNullOrEmpty(name) Then
                Throw New ArgumentNullException("name", "ArgumentNullPath")
            End If
            If name.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "name")
            End If
            If name = "." Then
                Return
            End If
            If name = ".." Then
                Return
            End If
            Execute(String.Concat("MKDIR ", name))
        End Sub

        Public Sub DeleteDirectory(ByVal path As String)
            If String.IsNullOrEmpty(path) Then
                Throw New ArgumentNullException("path", "ArgumentNullPath")
            End If
            If path.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "path")
            End If
            If path = "." Then
                Return
            End If
            If path = ".." Then
                Return
            End If
            Execute(String.Concat("RMDIR ", path))
        End Sub

        Private Function Execute(ByVal command As String) As FtpReply
            Dim FtpReply As FtpReply
            Lock()
            Try
                SendCommand(command)
                FtpReply = GetReply(2)
            Finally
                Unlock()
            End Try
            Return FtpReply
        End Function

        Public Sub Abort()
            Monitor.Enter(_sync)
            Try
                If _locked Then
                    If Not _aborting Then
                        ResetTimeout()
                        _aborting = True
                    End If
                End If
            Finally
                Monitor.Exit(_sync)
            End Try
        End Sub

        Public Function Login(ByVal userName As String, ByVal password As String) As String
            Dim str As String
            Lock()
            Try
                If String.IsNullOrEmpty(userName) Then
                    userName = "anonymous"
                    If String.IsNullOrEmpty(password) Then
                        password = "guest"
                    End If
                End If
                Dim reply As FtpReply = Nothing
                SendCommand(String.Concat("USER ", userName, _appendUser))
                If Not (_appendUser Is Nothing) Then
                    Do
                        Try
                            reply = GetReply(0)
                        Catch e As FtpException
                            If Not (e.Status = FtpExceptionStatus.ServerProtocolViolation) Then
                                Throw New FtpException(e)
                            End If
                            _control.Reset()
                        End Try
                    Loop While reply Is Nothing
                Else
                    reply = GetReply(0)
                End If

                If reply.Group = 2 Then
                    _appendUser = ""
                    _appendPassword = ""
                    str = reply.ReplyData
                Else
                    If Not (reply.Group = 3) Then
                        Throw New FtpException(reply.Clone)
                    End If
                    SendCommand(String.Concat("PASS ", password, _appendPassword))
                    reply = GetReply(0)
                    If reply.Group = 2 Then
                        _appendUser = ""
                        _appendPassword = ""
                        str = reply.ReplyData
                    Else
                        If Not (reply.Group = 3) Then
                            Throw New FtpException(reply.Clone)
                        End If
                        SendCommand("ACCT unknown")
                        reply = GetReply(0)
                        If Not (reply.Group = 2) Then
                            Throw New FtpException(reply.Clone)
                        End If
                        _appendUser = ""
                        _appendPassword = ""
                        str = reply.ReplyData
                    End If
                End If
            Finally
                Unlock()
            End Try
            Return str
        End Function

        Private Sub CheckState(ByVal state As FtpState)
            If Not (myState = state) Then
                Throw New InvalidOperationException("InvalidState")
            Else
                Return
            End If
        End Sub

        Private Function ReadData() As Integer
            Dim i As Integer
            If _aborting AndAlso Not _abortedTransfer Then
                _abortedTransfer = True
                Throw New FtpAbortException
            End If
            Try
                i = _data.ReadData
            Catch e As SocketException
                Throw New FtpTransferException(e)
            End Try
            If i <= 0 Then
                CheckTimeout()
            Else
                ResetTimeout()
                OnTransferProgress(_data.Length, i)
            End If
            Return i
        End Function

        Private Function WriteData() As Integer
            Dim i As Integer
            If _aborting AndAlso Not _abortedTransfer Then
                _abortedTransfer = True
                Throw New FtpAbortException
            End If
            Try
                i = _data.WriteData
            Catch e As SocketException
                Throw New FtpTransferException(e)
            End Try

            OnTransferProgress(_data.Length, i)

            Return i
        End Function

        Public Function GetFile(ByVal remotePath As String, ByVal localPath As String, ByVal remoteOffset As Long, ByVal localOffset As Long) As Long
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            End If
            If localPath Is Nothing Then
                Throw New ArgumentNullException("localPath", "ArgumentNullPath")
            End If
            If localPath.IndexOfAny(Path.InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "localPath")
            End If
            If localPath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "localPath")
            End If
            If remoteOffset < 0 Then
                Throw New ArgumentOutOfRangeException("remoteOffset")
            End If
            If localOffset < 0 Then
                Throw New ArgumentOutOfRangeException("localOffset")
            End If

            Dim l As Long
            Dim fileStream As FileStream = Nothing
            Lock()
            Try
                If localOffset > 0 Then
                    fileStream = File.OpenWrite(localPath)
                    fileStream.Seek(localOffset, SeekOrigin.Current)
                Else
                    fileStream = File.Create(localPath)
                End If
                Dim ftpDataDownload As FtpData = New FtpData(Me, fileStream)
                l = Transfer(String.Concat("RETR ", remotePath), ftpDataDownload, remoteOffset)
            Finally
                If Not (fileStream Is Nothing) Then
                    fileStream.Close()
                End If
                Unlock()
            End Try
            Return l
        End Function

        Public Function PutFile(ByVal localPath As String, ByVal remotePath As String, ByVal localOffset As Long, ByVal remoteOffset As Long) As Long
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            End If
            If localPath Is Nothing Then
                Throw New ArgumentNullException("localPath", "ArgumentNullPath")
            End If
            If localPath.IndexOfAny(Path.InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "localPath")
            End If
            If localPath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "localPath")
            End If
            If remoteOffset < 0 Then
                Throw New ArgumentOutOfRangeException("remoteOffset")
            End If
            If localOffset < 0 Then
                Throw New ArgumentOutOfRangeException("localOffset")
            End If

            Dim l As Long
            Dim fileStream As FileStream = Nothing
            Lock()
            Try
                fileStream = File.OpenRead(localPath)

                If localOffset > 0 Then
                    fileStream.Seek(localOffset, SeekOrigin.Current)
                End If

                Dim ftpDataDownload As FtpData = New FtpData(Me, fileStream, FtpTransferState.Uploading)

                l = Transfer(String.Concat("STOR ", remotePath), ftpDataDownload, remoteOffset)
            Finally
                If Not (fileStream Is Nothing) Then
                    fileStream.Close()
                End If
                Unlock()
            End Try
            Return l
        End Function

        Public Sub DeleteFile(ByVal path As String)
            If String.IsNullOrEmpty(path) Then
                Throw New ArgumentNullException("path", "ArgumentNullPath")
            End If
            If path.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "path")
            End If
            If path = "." Then
                Return
            End If
            If path = ".." Then
                Return
            End If
            Execute(String.Concat("DELETE ", path))
        End Sub

        Private Function AbortTransfer() As Exception
            Dim ftpData As FtpData = _data
            Dim e1 As FtpException = New FtpException("OperationAborted", Nothing, FtpExceptionStatus.OperationAborted, ftpData.Length)
            If myState = FtpState.Ready Then
                Return e1
            End If
            If _transferState = FtpTransferState.Uploading Then
                ftpData.Dispose()
                _data = Nothing
                GetReply(2)
                Return e1
            End If
            Dim ftpResponse As FtpReply = Nothing
            Dim e2 As FtpException = Nothing
            Dim flag1 As Boolean = False
            Dim flag2 As Boolean = False
            If (_options And FtpOptions.DoNotSendAbort) = 0 Then
                _control.Abort(IIf(((_options And FtpOptions.DoNotSendSignals) = 0), (_activeProxy.ProxyType = FtpProxyType.None), False))
                OnCommandSent(New FtpCommandSentEventArgs("ABOR"))
                Try
                    _timerStart = Environment.TickCount
                    ftpResponse = GetReply(0)
                    If Not (ftpResponse.Group = 4) Then
                        flag2 = True
                        SetState(FtpState.Reading)
                    End If
                Catch e3 As FtpException
                    ftpResponse = Nothing
                    e2 = e3
                End Try
                If Not (e2 Is Nothing) Then
                    If Not (e2.Status = FtpExceptionStatus.Timeout) Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                    flag1 = True
                End If
            Else
                flag2 = True
            End If
            If Not flag1 AndAlso Not flag2 Then
                e2 = Nothing
                SetState(FtpState.Reading)
                Try
                    _timerStart = Environment.TickCount
                    GetReply(2)
                Catch e5 As FtpException
                    e2 = e5
                End Try
                If Not (e2 Is Nothing) Then
                    If e2.Status = FtpExceptionStatus.ProtocolError Then
                        If Not (e2.Response.Code = 500) Then
                            Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                        End If
                        If e2.Response.Description.IndexOf("BOR") < 0 Then
                            Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                        End If
                    Else
                        If Not (e2.Status = FtpExceptionStatus.Timeout) Then
                            Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                        End If
                        If _control Is Nothing Then
                            Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                        End If
                        ftpResponse = _control.GetPartialResponse
                        If Not (ftpResponse.Group = 0) AndAlso Not (ftpResponse.Group = 2) Then
                            Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                        End If
                        _control.Reset()
                        SetState(FtpState.Ready)
                    End If
                End If
                Return e1
            End If
            ftpData.Dispose()
            _data = Nothing
            Try
                _timerStart = Environment.TickCount
                ftpResponse = GetReply(0)
            Catch e4 As FtpException
                If Not (e4.Status = FtpExceptionStatus.Timeout) Then
                    Dim e6 As Exception = New FtpException(e4.Message, e4, e4.Status, ftpData.Length)
                    Return e6
                End If
            End Try
            If flag2 Then
                Return e1
            End If
            e2 = Nothing
            SetState(FtpState.Reading)
            Try
                _timerStart = Environment.TickCount
                GetReply(2)
            Catch e5 As FtpException
                e2 = e5
            End Try
            If Not (e2 Is Nothing) Then
                If e2.Status = FtpExceptionStatus.ProtocolError Then
                    If Not (e2.Response.Code = 500) Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                    If e2.Response.Description.IndexOf("BOR") < 0 Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                Else
                    If Not (e2.Status = FtpExceptionStatus.Timeout) Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                    If _control Is Nothing Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                    ftpResponse = _control.GetPartialResponse
                    If Not (ftpResponse.Group = 0) AndAlso Not (ftpResponse.Group = 2) Then
                        Return New FtpException(e2.Message, e2, e2.Status, ftpData.Length)
                    End If
                    _control.Reset()
                    SetState(FtpState.Ready)
                End If
            End If
            Return e1
        End Function

        Private Function Transfer(ByVal command As String, ByVal data As FtpData, ByVal serverOffset As Long) As Long
            Try
                Dim i As Integer
                Dim transferReply As FtpReply = Nothing
                If _passive OrElse _activeProxy.ProxyType = FtpProxyType.HttpConnect Then
                    Dim iPEndPoint1 As IPEndPoint = StartPassive()
                    Restart(serverOffset)
                    data.Connect(iPEndPoint1)
                    SetTransferState(data.State)
                    SendCommand(command)
                    transferReply = GetReply(0)
                    If Not (transferReply.Group = 1) AndAlso Not (transferReply.Group = 2) Then
                        Throw New FtpException(transferReply.Clone)
                    End If
                Else
                    Dim iPEndPoint2 As IPEndPoint = data.Listen(_control.ControlSocket)
                    Port(iPEndPoint2)
                    Restart(serverOffset)
                    SendCommand(command)
                    transferReply = GetReply(0)
                    If Not (transferReply.Group = 1) AndAlso Not (transferReply.Group = 2) Then
                        Throw New FtpException(transferReply.Clone)
                    End If
                    data.Accept()
                    SetTransferState(data.State)
                End If
                _data = data
                If transferReply.Group = 1 Then
                    transferReply = Nothing
                    transferReply = GetReply(2)
                End If
                If data.State = FtpTransferState.Downloading Then
                    Do
                        i = ReadData()
                    Loop While i >= 0
                Else
                    Do
                        i = WriteData()
                    Loop While i >= 0
                End If

                Return data.Length
            Catch exAbort As FtpAbortException
                Throw AbortTransfer()
            Catch exTransfer As FtpTransferException
                Throw New FtpException(exTransfer.InnerException.Message, exTransfer, FtpExceptionStatus.SocketError)
            Finally
                _data = Nothing
                data.Dispose()
                SetTransferState(FtpTransferState.None)
            End Try
        End Function

        Public Function GetRawList(ByVal arguments As String) As String()
            Dim strs As String()
            Lock()
            Try
                Dim memoryStream As MemoryStream = New MemoryStream
                Dim ftpDataDownload As FtpData = New FtpData(Me, memoryStream)
                Dim str As String = "LIST"
                If Not (arguments Is Nothing) Then
                    str = String.Concat(str, " ", arguments)
                End If
                Transfer(str, ftpDataDownload, 0)
                strs = Parser.SplitRawList(_encoding, memoryStream)
                Return strs
            Finally
                Unlock()
            End Try
        End Function

        Public Function GetList() As FtpList
            Return New FtpList(GetRawList(Nothing))
        End Function

        Public Function GetList(ByVal arguments As String) As FtpList
            Return New FtpList(GetRawList(arguments))
        End Function

        Private Function GetReply(ByVal success As Integer) As FtpReply
            CheckState(FtpState.Reading)
            Dim reply As FtpReply = Nothing
            Try
                ResetTimeout()
                While True
                    reply = _control.Read
                    If Not (reply Is Nothing) Then
                        If Not (reply.Group = 1) Then
                            If reply.Group = 3 Then
                                SetState(FtpState.Processing)
                            Else
                                SetState(FtpState.Ready)
                            End If
                        End If
                        If Not (reply.Group = success) AndAlso Not (success = 0) Then
                            Throw New FtpException(reply.Clone)
                        End If
                        Return reply
                    End If
                    If Not (_data Is Nothing) Then
                        ReadData()
                    Else
                        CheckTimeout()
                        Thread.Sleep(1)
                    End If
                End While
            Catch e1 As FtpConnectionClosedException
                Throw New FtpException("ConnectionClosed", FtpExceptionStatus.ConnectionClosed)
            Catch e3 As SocketException
                Throw New FtpException(e3.Message, FtpExceptionStatus.SocketError)
            Catch e2 As ProxySocketException
                Reset()
                Dim exceptionStatus As FtpExceptionStatus
                If e2.Status = 1 Then
                    exceptionStatus = FtpExceptionStatus.ConnectionClosed
                Else
                    exceptionStatus = FtpExceptionStatus.SocketError
                End If
                Throw New FtpException(e2.Message, e2, exceptionStatus)
            End Try
            Return reply
        End Function

        'This is custom implementation of the asynchronous invocation of any methods without using delegates
        Private Function BeginAsync(ByVal callback As AsyncCallback, ByVal state As Object, ByVal method As String, ByVal ParamArray parameters As Object()) As IAsyncResult
            Dim asyncResult As FtpAsyncResult = New FtpAsyncResult(Me, method, callback, state, parameters)
            Monitor.Enter(_sync)
            Try
                If _locked Then
                    Throw New FtpException("OperationPending", FtpExceptionStatus.Pending)
                End If
                If Not (_async Is Nothing) Then
                    Throw New FtpException("OperationPending", FtpExceptionStatus.Pending)
                End If
                _async = asyncResult
            Finally
                Monitor.Exit(_sync)
            End Try
            Try
                asyncResult.Thread.Start()
                Dim iAsyncResult As IAsyncResult = asyncResult
                Return iAsyncResult
            Catch e As Exception
                Monitor.Enter(_sync)
                Try
                    _async = Nothing
                Finally
                    Monitor.Exit(_sync)
                End Try
                Throw New FtpException("UnclassifiableError", e)
            End Try
        End Function

        Private Function EndAsync(ByVal asyncRes As IAsyncResult, ByVal method As String) As Object
            Dim asyncResult As FtpAsyncResult = CType(asyncRes, FtpAsyncResult)
            If asyncResult Is Nothing Then
                Throw New ArgumentException("InvalidEndCall")
            End If
            Dim local As Object = asyncResult.Finish(Me, method)

            If TypeOf local Is Exception Then
                Dim e1 As Exception = CType(local, Exception)
                If TypeOf e1 Is FtpException Then
                    Dim e2 As FtpException = CType(e1, FtpException)
                    Throw New FtpException(e2)
                Else
                    Throw New FtpException("AsyncException", e1, FtpExceptionStatus.AsyncError)
                End If
            Else
                Return local
            End If
        End Function

        Public Function BeginGetFile(ByVal remotePath As String, ByVal localPath As String, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            End If
            If localPath Is Nothing Then
                Throw New ArgumentNullException("localPath", "ArgumentNullPath")
            End If
            If localPath.IndexOfAny(Path.InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "localPath")
            End If
            If localPath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "localPath")
            Else
                Return BeginAsync(callback, state, "GetFile", New Object() {remotePath, localPath, 0, 0})
            End If
        End Function

        Public Function BeginGetFile(ByVal remotePath As String, ByVal outputStream As Stream, ByVal remoteOffset As Long, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            End If
            If outputStream Is Nothing Then
                Throw New ArgumentNullException("outputStream", "ArgumentNullStream")
            End If
            If remoteOffset < CType(0, Long) Then
                Throw New ArgumentOutOfRangeException("remoteOffset")
            Else
                Return BeginAsync(callback, state, "GetFile", New Object() {remotePath, outputStream, remoteOffset})
            End If
        End Function

        Public Function EndGetFile(ByVal asyncResult As IAsyncResult) As Long
            Return CType(EndAsync(asyncResult, "GetFile"), Long)
        End Function

        Public Function BeginPutFile(ByVal localPath As String, ByVal remotePath As String, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            If remotePath Is Nothing Then
                Throw New ArgumentNullException("remotePath", "ArgumentNullPath")
            End If
            If remotePath.IndexOfAny(InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "remotePath")
            End If
            If remotePath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "remotePath")
            End If
            If localPath Is Nothing Then
                Throw New ArgumentNullException("localPath", "ArgumentNullPath")
            End If
            If localPath.IndexOfAny(Path.InvalidPathChars) >= 0 Then
                Throw New ArgumentException("ArgumentIllegalPath", "localPath")
            End If
            If localPath.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyPath", "localPath")
            Else
                Return BeginAsync(callback, state, "PutFile", New Object() {localPath, remotePath, 0, 0})
            End If
        End Function

        Public Function EndPutFile(ByVal asyncResult As IAsyncResult) As Long
            Return CType(EndAsync(asyncResult, "PutFile"), Long)
        End Function

        Private Sub SetState(ByVal newState As FtpState)
            Dim flag As Boolean = False
            Dim ftpState2 As FtpState = newState
            Select Case ftpState2
                Case FtpState.Disconnected
                    flag = True
                Case FtpState.Connecting
                    If Not (myState = FtpState.Disconnected) Then
                        Throw New InvalidOperationException("AlreadyConnected")
                    End If
                    flag = True
                Case Else
                    If Not (ftpState2 = FtpState.Disposed) Then
                        ftpState2 = myState
                        Select Case ftpState2
                            Case 1, 3
                                If newState = FtpState.Reading Then
                                    flag = True
                                End If
                            Case 2
                                If (newState = FtpState.Sending) OrElse (newState = FtpState.Reading) Then
                                    flag = True
                                End If
                            Case 4
                                If newState = FtpState.Reading Then
                                    Return
                                End If
                                If newState = FtpState.Processing OrElse newState = FtpState.Ready Then
                                    flag = True
                                End If
                            Case 5
                                If (newState = FtpState.Sending) OrElse (newState = FtpState.Ready) OrElse (newState = FtpState.Reading) Then
                                    flag = True
                                End If
                            Case 6
                                Throw New ObjectDisposedException("Ftp")
                        End Select
                    Else
                        flag = True
                    End If
            End Select
            If Not flag Then
                Throw New InvalidOperationException("InvalidStateChange")
            End If
            If myState = newState Then
                Return
            End If
            Dim ftpState1 As FtpState = myState
            myState = newState
            OnStateChanged(New FtpStateChangedEventArgs(ftpState1, newState))
        End Sub

        Public Function Flush(ByVal timeLimit As Integer) As FtpReply
            Lock()
            Dim l As Long
            Dim ftpResponse2 As FtpReply
            Dim ftpResponse1 As FtpReply
            Try
                Dim proxySocket As ProxySocket = _control.ControlSocket
                Select Case myState
                    Case 2
                        If (proxySocket.Available = 0) Then
                            If Not proxySocket.Socket.Poll(1000, SelectMode.SelectRead) Then
                                ftpResponse2 = Nothing
                            Else
                                If proxySocket.Available = 0 Then
                                    Throw New FtpException("ConnectionClosed", FtpExceptionStatus.ConnectionClosed)
                                End If
                                ftpResponse2 = Nothing
                            End If
                            Return ftpResponse2
                        End If
                    Case 3
                        SetState(FtpState.Reading)
                    Case 5
                        SetState(FtpState.Reading)
                    Case 4
                    Case Else
                        Throw New InvalidOperationException("InvalidState")
                End Select
                l = Environment.TickCount + timeLimit
                While proxySocket.Available = 0
                    If proxySocket.Socket.Poll(1000, SelectMode.SelectRead) AndAlso proxySocket.Available = 0 Then
                        Throw New FtpException("ConnectionClosed", FtpExceptionStatus.ConnectionClosed)
                    End If
                    If Environment.TickCount > l Then
                        SetState(FtpState.Ready)
                        ftpResponse2 = Nothing
                        Return ftpResponse2
                    End If
                    Thread.Sleep(1)
                End While
                ftpResponse1 = GetReply(0)
                If Not (ftpResponse1.Group = 1) AndAlso Not (ftpResponse1.Group = 3) Then
                    ftpResponse2 = ftpResponse1
                Else
                    ftpResponse2 = GetReply(0)
                End If
            Finally
                Unlock()
            End Try
            Return ftpResponse2
        End Function

        Private Sub Restart(ByVal offset As Long)
            If offset < 0 Then
                offset = 0
            End If
            If _lastOffset = offset AndAlso offset = 0 Then
                Return
            End If
            SendCommand(String.Concat("REST ", offset))
            GetReply(3)
            _lastOffset = offset
        End Sub

        Private Function StartPassive() As IPEndPoint
            SendCommand("PASV")

            Return Parser.ParseResponseEndPoint(GetReply(2).ReplyData)

        End Function

        Private Sub Port(ByVal endPoint As IPEndPoint)
            Dim str As String = Parser.EndPointToString(endPoint)
            SendCommand(String.Concat("PORT ", str))
            GetReply(2)
        End Sub

        Private Sub SetTransferState(ByVal newState As FtpTransferState)
            If _transferState = newState Then
                Return
            End If
            Monitor.Enter(_sync)
            Try
                _transferState = newState
            Finally
                Monitor.Exit(_sync)
            End Try
            OnTransferProgress(0, 0)
        End Sub

        Protected Overridable Sub OnStateChanged(ByVal e As FtpStateChangedEventArgs)
            RaiseEvent StateChanged(Me, e)
        End Sub

        Protected Overridable Sub OnCommandSent(ByVal e As FtpCommandSentEventArgs)
            RaiseEvent CommandSent(Me, e)
        End Sub

        Private Sub OnTransferProgress(ByVal bytesTransferred As Long, ByVal bytesSinceLastEvent As Integer)
            RaiseEvent TransferProgress(Me, New FtpTransferProgressEventArgs(_transferState, bytesTransferred, bytesSinceLastEvent))
        End Sub

    End Class

End Namespace
