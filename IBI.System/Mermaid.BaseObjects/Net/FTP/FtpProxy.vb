Imports System
Imports System.Collections
Imports System.Net

Namespace Net.FTP

    Public Enum FtpProxyType As Integer
        None = 0
        Socks4 = 1
        Socks4a = 2
        Socks5 = 3
        HttpConnect = 4
        FtpSite = 5
        FtpUser = 6
        FtpOpen = 7
    End Enum

    Public Class FtpProxy
        Implements IWebProxy

        Private myCredentials As NetworkCredential = Nothing
        Private _type As FtpProxyType = FtpProxyType.None
        Private _host As String = Nothing
        Private _port As Integer = 21
        Private _proxy As WebProxy = New WebProxy

        Public Property Host() As String
            Get
                Return _host
            End Get
            Set(ByVal Value As String)
                _host = Value
            End Set
        End Property

        Public Property Port() As Integer
            Get
                Return _port
            End Get
            Set(ByVal Value As Integer)
                _port = Value
            End Set
        End Property

        Public Property ProxyType() As FtpProxyType
            Get
                Return _type
            End Get
            Set(ByVal Value As FtpProxyType)
                _type = Value
            End Set
        End Property

        Public Overridable Property Credentials() As ICredentials Implements IWebProxy.Credentials
            Get
                Return myCredentials
            End Get
            Set(ByVal Value As ICredentials)
                myCredentials = CType(Value, NetworkCredential)
            End Set
        End Property

        Public Property UserName() As String
            Get
                If myCredentials Is Nothing Then
                    Return Nothing
                Else
                    Return myCredentials.UserName
                End If
            End Get
            Set(ByVal Value As String)
                If myCredentials Is Nothing Then
                    myCredentials = New NetworkCredential
                End If
                myCredentials.UserName = Value
            End Set
        End Property

        Public Property Password() As String
            Get
                If myCredentials Is Nothing Then
                    Return Nothing
                Else
                    Return myCredentials.Password
                End If
            End Get
            Set(ByVal Value As String)
                If myCredentials Is Nothing Then
                    myCredentials = New NetworkCredential
                End If
                myCredentials.Password = Value
            End Set
        End Property

        Public Property BypassProxyOnLocal() As Boolean
            Get
                Return _proxy.BypassProxyOnLocal
            End Get
            Set(ByVal Value As Boolean)
                _proxy.BypassProxyOnLocal = Value
            End Set
        End Property

        Public Property BypassList() As String()
            Get
                Return _proxy.BypassList
            End Get
            Set(ByVal Value As String())
                If Value Is Nothing Then
                    Throw New ArgumentNullException("value")
                End If
                _proxy.BypassList = Value
            End Set
        End Property

        Friend Function Clone() As FtpProxy
            Dim ftpProxy As FtpProxy = New FtpProxy
            ftpProxy.ProxyType = ProxyType
            ftpProxy.Host = Host
            ftpProxy.Port = Port
            ftpProxy.BypassProxyOnLocal = BypassProxyOnLocal
            ftpProxy.BypassList = BypassList
            ftpProxy.Credentials = Credentials
            Return ftpProxy
        End Function

        Private Sub SetAddress()
            Dim str As String
            Select Case _type
                Case FtpProxyType.None
                    _proxy.Address = Nothing
                    Return
                Case FtpProxyType.Socks4
                    str = "socks4"
                Case FtpProxyType.Socks4a
                    str = "socks4a"
                Case FtpProxyType.Socks5
                    str = "socks5"
                Case FtpProxyType.HttpConnect
                    str = "http"
                Case Else
                    str = "ftp"
            End Select
            _proxy.Address = New Uri(String.Concat(New Object() {str, "://", _host, ":", _port}))
        End Sub

        Public Overridable Function GetProxy(ByVal destination As Uri) As Uri Implements IWebProxy.GetProxy
            If destination Is Nothing Then
                Throw New ArgumentNullException("destination")
            End If
            If _proxy.IsBypassed(destination) Then
                Return destination
            End If
            SetAddress()
            Return _proxy.Address
        End Function

        Public Overridable Function IsBypassed(ByVal host As Uri) As Boolean Implements IWebProxy.IsBypassed
            If host Is Nothing Then
                Throw New ArgumentNullException("host")
            End If
            SetAddress()
            Return _proxy.IsBypassed(host)
        End Function

        Public Sub New()
        End Sub

        Friend Function CreateSocket() As ProxySocket
            Dim prType As ProxyType
            Select Case _type
                Case 1
                    prType = 1
                Case 2
                    prType = 2
                Case 3
                    prType = 3
                Case 4
                    prType = 4
                Case Else
                    prType = 0
            End Select
            Dim userName As String = Nothing
            Dim pass As String = Nothing
            If Not (myCredentials Is Nothing) Then
                userName = myCredentials.UserName
                pass = myCredentials.Password
            End If
            Return New ProxySocket(prType, _host, _port, userName, pass)
        End Function
    End Class
End Namespace