Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection

Namespace Net.FTP
    Public Enum ProxyType As Integer
        None = 0
        Socks4 = 1
        Socks4a = 2
        Socks5 = 3
        HttpConnect = 4
    End Enum

    MustInherit Class ProxyBase
        Protected myProxyUserName As String = Nothing
        Protected myProxyPassword As String = Nothing
        Protected myProxyHost As String = Nothing
        Protected myProxyPort As Integer = -1
        Protected myAsyncResult As ProxyAsyncResult = Nothing
        Private mySocket As Socket

        Public ReadOnly Property Socket() As Socket
            Get
                Return mySocket
            End Get
        End Property

        Protected ReadOnly Property Available() As Integer
            Get
                Try
                    Dim dataToRead As Integer
                    Dim availableData As Integer = mySocket.Available
                    If availableData > 0 Then
                        dataToRead = availableData
                    Else
                        If Not mySocket.Poll(1000, SelectMode.SelectRead) Then
                            dataToRead = 0
                        Else
                            availableData = mySocket.Available
                            If availableData > 0 Then
                                dataToRead = availableData
                            Else
                                dataToRead = -1
                            End If
                        End If
                    End If
                    Return dataToRead
                Catch e As SocketException
                    Throw New ProxySocketException(e)
                End Try
            End Get
        End Property

        Friend Sub SetSocket(ByVal socket As Socket)
            mySocket = socket
        End Sub

        Public Shared Function Create(ByVal socket As Socket, ByVal controlSocket As ProxyBase) As ProxyBase
            If TypeOf controlSocket Is ProxyNull Then
                Return New ProxyNull(socket)
            End If
            controlSocket.GetType()
            Dim proxyBase As ProxyBase = CType(controlSocket.GetType.InvokeMember(Nothing, BindingFlags.CreateInstance, Nothing, Nothing, New Object() {socket}), ProxyBase)
            proxyBase.myProxyHost = controlSocket.myProxyHost
            proxyBase.myProxyPort = controlSocket.myProxyPort
            proxyBase.myProxyUserName = controlSocket.myProxyUserName
            proxyBase.myProxyPassword = controlSocket.myProxyPassword
            Return proxyBase
        End Function


        Public Shared Function Create(ByVal socket As Socket, ByVal type As ProxyType, ByVal host As String, ByVal port As Integer, ByVal userName As String, ByVal password As String) As ProxyBase
            Dim proxyBase As ProxyBase
            If Not (type = ProxyType.None) Then
                If host Is Nothing Then
                    Throw New ProxySocketException("ArgumentNullHostname", ProxySocketExceptionStatus.ProxyNameResolutionFailure)
                End If
                If host.Trim.Length = 0 Then
                    Throw New ProxySocketException("ArgumentEmptyHostname", ProxySocketExceptionStatus.ProxyNameResolutionFailure)
                End If
            End If
            Select Case type
                Case ProxyType.Socks4
                    proxyBase = New ProxySocks4(socket)
                Case ProxyType.Socks4a
                    proxyBase = New ProxySocks4a(socket)
                Case ProxyType.Socks5
                    proxyBase = New ProxySocks5(socket)
                Case ProxyType.HttpConnect
                    proxyBase = New ProxyHttp(socket)
                Case Else
                    proxyBase = New ProxyNull(socket)
            End Select
            proxyBase.myProxyHost = host
            proxyBase.myProxyPort = port
            If Not (userName Is Nothing) Then
                proxyBase.myProxyUserName = userName
            Else
                proxyBase.myProxyUserName = ""
            End If
            If Not (password Is Nothing) Then
                proxyBase.myProxyPassword = password
            Else
                proxyBase.myProxyPassword = ""
            End If
            Return proxyBase
        End Function

        Protected Sub Begin(ByVal method As String, ByVal callback As AsyncCallback, ByVal state As Object)
            If Not (myAsyncResult Is Nothing) Then
                Throw New InvalidOperationException("AsyncCallPending")
            End If
            myAsyncResult = New ProxyAsyncResult(Me, method, callback, state)
        End Sub

        Public Function EndMethod(ByVal asyncResult As IAsyncResult, ByVal method As String) As Object
            If asyncResult Is Nothing Then
                Throw New ArgumentNullException("asyncResult")
            End If
            Dim proxyAResult As ProxyAsyncResult = CType(asyncResult, ProxyAsyncResult)
            If proxyAResult Is Nothing OrElse Not (proxyAResult.AsyncObject Is Me) OrElse Not (proxyAResult.Method = method) Then
                Throw New ArgumentException("InvalidEndCall")
            End If
            proxyAResult.Wait()
            myAsyncResult = Nothing
            Return proxyAResult.EndMethod(method)
        End Function

        Protected Function CancelBegin(ByVal e As Exception) As Exception
            myAsyncResult = Nothing
            If TypeOf e Is SocketException Then
                Return New ProxySocketException(e)
            Else
                Return e
            End If
        End Function

        Protected Sub SetError(ByVal e As Exception)
            If Not (myAsyncResult Is Nothing) Then
                Dim e2 As SocketException = Nothing

                If TypeOf e Is SocketException Then
                    e2 = CType(e, SocketException)
                End If

                If e2 Is Nothing Then
                    myAsyncResult.SetError(e)
                    Return
                End If
                myAsyncResult.SetError(New ProxySocketException(e2))
            End If
        End Sub

        Protected Sub Finish()
            If Not (myAsyncResult Is Nothing) Then
                myAsyncResult.Finish()
            End If
        End Sub

        Public Sub New(ByVal socket As Socket)
            mySocket = socket
        End Sub

        Public MustOverride Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult

        Public MustOverride Function BeginConnect(ByVal endPoint As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult

        Public MustOverride Function BeginListen(ByVal controlSocket As ProxyBase, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult

        Public MustOverride Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
    End Class
End Namespace