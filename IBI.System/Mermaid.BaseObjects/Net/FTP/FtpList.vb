Imports System
Imports System.Collections
Imports System.Text.RegularExpressions
Imports System.Threading

Namespace Net.FTP
    Public Class FtpList
        Implements IEnumerable

        Private myHashTable As Hashtable = New Hashtable
        Private myItems As ArrayList = New ArrayList

        Public ReadOnly Property Count() As Integer
            Get
                Return myItems.Count
            End Get
        End Property

        Public Sub New(ByVal stringList As String())
            If stringList Is Nothing Then
                Throw New ArgumentNullException("stringList")
            End If
            Dim i As Integer = 0
            While i < stringList.Length
                Dim ftpItem1 As FtpItem = FtpItem.Parse(stringList(i))
                If Not (ftpItem1 Is Nothing) AndAlso Not myHashTable.Contains(ftpItem1.Name) Then
                    myItems.Add(ftpItem1)
                    myHashTable.Add(ftpItem1.Name, ftpItem1)
                End If
                i += 1
            End While
        End Sub

        Public Overridable Function GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
            Return myItems.GetEnumerator
        End Function

        Public Function GetEnumerator(ByVal index As Integer, ByVal count As Integer) As IEnumerator
            Return myItems.GetEnumerator(index, count)
        End Function

        Public Function GetNamedItem(ByVal name As String) As FtpItem
            Return myHashTable(name)

        End Function
    End Class
End Namespace