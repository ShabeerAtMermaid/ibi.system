Imports System
Imports System.Threading

Namespace Net.FTP
    Class ProxyAsyncResult
        Implements IAsyncResult
        Private myAsyncObject As Object
        Private myState As Object
        Private myResult As Object = Nothing
        Private myException As Exception = Nothing
        Private myCallback As AsyncCallback
        Private myMethod As String
        Private myIsFinished As Boolean = False
        Private myIsInvoked As Boolean = False
        Private myAsyncHandle As ManualResetEvent = New ManualResetEvent(False)

        Public Overridable ReadOnly Property AsyncState() As Object Implements IAsyncResult.AsyncState
            Get
                Return myState
            End Get
        End Property

        Public Overridable ReadOnly Property AsyncWaitHandle() As WaitHandle Implements IAsyncResult.AsyncWaitHandle
            Get
                Return myAsyncHandle
            End Get
        End Property

        Public Overridable ReadOnly Property CompletedSynchronously() As Boolean Implements IAsyncResult.CompletedSynchronously
            Get
                Return False
            End Get
        End Property

        Public Overridable ReadOnly Property IsCompleted() As Boolean Implements IAsyncResult.IsCompleted
            Get
                Return myIsFinished
            End Get
        End Property

        Public ReadOnly Property AsyncObject() As Object
            Get
                Return myAsyncObject
            End Get
        End Property

        Public ReadOnly Property Method() As String
            Get
                Return myMethod
            End Get
        End Property

        Public Sub New(ByVal asyncObject As Object, ByVal method As String, ByVal callback As AsyncCallback, ByVal state As Object)
            myAsyncObject = asyncObject
            myCallback = callback
            myState = state
            myMethod = method
        End Sub

        Public Sub Wait()
            While Not Me.IsCompleted
                Thread.Sleep(1)
            End While
        End Sub

        Public Sub Finish()
            myIsFinished = True
            myAsyncHandle.Set()
            If Not (myCallback Is Nothing) Then
                myCallback.Invoke(Me)
            End If
        End Sub

        Public Function EndMethod(ByVal method As String) As Object
            If myIsInvoked Then
                Throw New InvalidOperationException("EndAlreadyCalled")
            End If
            myIsInvoked = True
            If Not (myException Is Nothing) Then
                Throw myException
            Else
                Return myResult
            End If
        End Function

        Public Sub SetResult(ByVal result As Object)
            myResult = result
            myException = Nothing
        End Sub

        Public Sub SetError(ByVal err As Exception)
            myResult = Nothing
            myException = err
        End Sub
    End Class
End Namespace