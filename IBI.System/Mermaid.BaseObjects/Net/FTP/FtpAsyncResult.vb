#Region "Represents the status of an asynchronous ftp operation"
' Custom IAsyncResult implementation for asynchronous FTP commands
' This class implements custom asynchronous invcation mechanism of any methods (through reflection).
' So there is no need to create separate delegate for each method we want to invoke asynchronously
#End Region
Imports System
Imports System.Reflection
Imports System.Threading

Namespace Net.FTP
    Class FtpAsyncResult
        Implements IAsyncResult
        Private myInvokeObject As Object
        Private myInvokeMethod As String
        Private myMethodInfo As MethodInfo
        Private myInvokeArgs As Object()
        Private myCallback As AsyncCallback
        Private myStateObject As Object
        Private myResult As Object = Nothing
        Private myError As Exception = Nothing
        Private myThread As Thread = Nothing
        Private myResetEvent As ManualResetEvent = New ManualResetEvent(False)
        Private myCompleted As Boolean = False
        Private myClosed As Boolean = False
        Private mySync As Object = New Object

        Public Overridable ReadOnly Property AsyncState() As Object Implements IAsyncResult.AsyncState
            Get
                Return myStateObject
            End Get
        End Property

        Public Overridable ReadOnly Property AsyncWaitHandle() As WaitHandle Implements IAsyncResult.AsyncWaitHandle
            Get
                Return myResetEvent
            End Get
        End Property

        Public Overridable ReadOnly Property CompletedSynchronously() As Boolean Implements IAsyncResult.CompletedSynchronously
            Get
                Return False
            End Get
        End Property

        Public Overridable ReadOnly Property IsCompleted() As Boolean Implements IAsyncResult.IsCompleted
            Get
                Dim flag As Boolean = False
                Monitor.Enter(mySync)
                Try
                    flag = myCompleted
                Finally
                    Monitor.Exit(mySync)
                End Try
                Return flag
            End Get
        End Property

        Public ReadOnly Property Thread() As Thread
            Get
                Return myThread
            End Get
        End Property

        Public Sub New(ByVal obj As Object, ByVal method As String, ByVal userCallback As AsyncCallback, ByVal stateObject As Object, ByVal ParamArray parameters As Object())
            myInvokeObject = obj
            myCallback = userCallback
            myStateObject = stateObject
            myInvokeArgs = parameters
            Dim types() As Type = New Type(myInvokeArgs.Length - 1) {}
            Dim i As Integer = 0
            While i < myInvokeArgs.Length
                types(i) = myInvokeArgs(i).GetType
                i += 1
            End While
            myInvokeMethod = method
            myMethodInfo = myInvokeObject.GetType.GetMethod(myInvokeMethod, BindingFlags.Instance Or BindingFlags.InvokeMethod Or BindingFlags.Public, Nothing, types, Nothing)
            If myMethodInfo Is Nothing Then
                Throw New NotSupportedException("InvalidAsyncCall")
            End If
            'THREAD: ThreadManager
            'myThread = New Thread(New ThreadStart(AddressOf Me.Start))
            myThread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("FTPAsyncResult thread", AddressOf Start)
        End Sub

        Private Sub Start()
            Try
                myResult = myMethodInfo.Invoke(myInvokeObject, myInvokeArgs)
            Catch ex As TargetInvocationException
                myError = ex.InnerException
            Catch ex1 As Exception
                myError = ex1
            Finally
                Dim asyncCallback As AsyncCallback
                Monitor.Enter(mySync)
                Try
                    myCompleted = True
                    myResetEvent.Set()
                    asyncCallback = myCallback
                    myCallback = Nothing
                Finally
                    Monitor.Exit(mySync)
                End Try
                If Not (asyncCallback Is Nothing) Then
                    asyncCallback.Invoke(Me)
                End If
            End Try
        End Sub

        Public Function Finish(ByVal obj As Object, ByVal method As String) As Object
            If Not (myInvokeMethod = method) OrElse Not (myInvokeObject Is obj) Then
                Dim locals As Object() = New Object() {method}
                Throw New ArgumentException("InvalidEndCall")
            End If
            Monitor.Enter(mySync)
            Try
                If myClosed Then
                    Throw New InvalidOperationException("EndAlreadyCalled")
                End If
                myClosed = True
            Finally
                Monitor.Exit(mySync)
            End Try
            myResetEvent.WaitOne()
            myResetEvent.Close()
            myResetEvent = Nothing
            Dim aResult As Object = myResult
            Dim e As Exception = myError
            myStateObject = Nothing
            myResult = Nothing
            myInvokeArgs = Nothing
            myError = Nothing
            If Not (e Is Nothing) Then
                Return e
            Else
                Return aResult
            End If
        End Function
    End Class

End Namespace