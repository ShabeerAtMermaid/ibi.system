Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Runtime.CompilerServices
Imports System.Text

Namespace Net.FTP

    Class Ftp
        Implements IDisposable

        Private Const MaxResponseLength As Integer = 65536
        Private Const ResponseBufferLength As Integer = 4096
        Private myService As FtpService = Nothing
        Private _socket As ProxySocket = Nothing
        Private _raw As String = Nothing
        Private _offset As Integer = 0
        Private _codeText As String = Nothing
        Private _step As Integer = 0
        Private _array As Byte() = New Byte(4096) {}
        Private _response As FtpReply = New FtpReply
        Dim buffer As String = ""
        Private _lastPoll As Integer = 0

        Private Shared ReadOnly _abort As Byte() = New Byte() {Byte.MaxValue, 244, Byte.MaxValue, 242, 65, 66, 79, 82, 13, 10}

        Public ReadOnly Property LocalEndPoint() As IPEndPoint
            Get
                Return _socket.LocalEndPoint
            End Get
        End Property

        Public ReadOnly Property RemoteEndPoint() As IPEndPoint
            Get
                Return _socket.RemoteEndPoint
            End Get
        End Property

        Public ReadOnly Property ControlSocket() As ProxySocket
            Get
                Return _socket
            End Get
        End Property

        Public Function GetPartialResponse() As FtpReply
            Return _response
        End Function

        Public Sub New(ByVal ftp As FtpService, ByVal serverName As String, ByVal serverPort As Integer)
            Dim iAsyncResult As IAsyncResult
            _socket = ftp.ActiveProxy.CreateSocket
            myService = ftp
            Dim str As String = ftp.ActiveProxy.Host
            Dim i As Integer = ftp.ActiveProxy.Port
            Select Case ftp.ActiveProxy.ProxyType
                Case 5, 6, 7
                    iAsyncResult = _socket.BeginConnect(str, i, Nothing, Nothing)
                Case Else
                    str = serverName
                    i = serverPort
                    iAsyncResult = _socket.BeginConnect(str, i, Nothing, Nothing)
            End Select
            Try
                ftp.WaitForCompletion(iAsyncResult)
                _socket.EndConnect(iAsyncResult)
                Return
            Catch e As Exception
                _socket.Close()
                Throw e
            End Try
        End Sub

        Public Overridable Sub Dispose() Implements IDisposable.Dispose
            If _socket Is Nothing Then
                Return
            End If
            _socket.Close()
            _socket = Nothing
            GC.SuppressFinalize(Me)
        End Sub

        Public Sub Abort(ByVal sendSignals As Boolean)
            If sendSignals Then
                _socket.Send(_abort, 0, 4, SocketFlags.OutOfBand)
                _socket.Send(_abort, 4, 6, SocketFlags.None)
                Return
            End If
            _socket.Send(_abort, 4, 6, SocketFlags.None)
        End Sub

        Public Sub Reset()
            _step = 0
            _response.Reset()
        End Sub

        Private Sub Flush()
            While GetAvailable() > 0
                Dim i As Integer = Receive(_array)
                myService.ReportResponseRead(myService.Encoding.GetString(_array, 0, i))
            End While
        End Sub

        Public Sub Write(ByVal command As Byte())
            Flush()
            _socket.Send(command)
        End Sub

        Private Function Receive(ByVal buffer As Byte()) As Integer
            Return _socket.Receive(_array)
        End Function

        Private Function GetAvailable() As Integer
            Dim i As Integer = _socket.Available
            If i = 0 Then
                Dim j As Integer = Environment.TickCount
                If j - _lastPoll < 1000 Then
                    Return 0
                End If
                _lastPoll = j
                If Not _socket.Socket.Poll(100, SelectMode.SelectRead) Then
                    Return 0
                End If
                i = _socket.Available
                If i = 0 Then
                    Throw New FtpException("Connection closed") 'FtpConnectionClosedException
                End If
            End If
            Return i
        End Function

        Private Function ReadLine(ByRef response As String) As Boolean
            Do
                If buffer.Length > 0 Then
                    Dim i As Integer = buffer.IndexOf("" & vbCrLf)
                    If i >= 0 Then
                        Dim str As String = buffer.Substring(0, i)
                        If str.IndexOf("linefeed") > 0 Then
                            str = String.Concat(str, "")
                        End If
                        myService.ReportResponseRead(str)
                        response = String.Concat(response, str, "" & vbCrLf)
                        buffer = buffer.Substring(i + 2)
                        If _step = 0 Then
                            _step = 1
                        End If
                        Return True
                    End If
                End If
                Dim j As Integer = GetAvailable()
                If j = 0 Then
                    Return False
                End If
                j = Receive(_array)
                buffer = String.Concat(buffer, myService.Encoding.GetString(_array, 0, j))
            Loop While buffer.Length <= 65536
            Throw New FtpException("Reply timeout")
        End Function

        Public Function Read() As FtpReply
            Dim i As Integer
            If _step = 0 Then
                _raw = ""
                _offset = 0
                _codeText = Nothing
                _response.Reset()
            End If
            Do
                Do
                    If Not ReadLine(_raw) Then
                        Return Nothing
                    End If
                    If _step = 1 Then
                        _offset = _raw.IndexOf("" & vbCrLf)
                        If _offset < 4 Then
                            Throw New FtpException("InvalidResponse")
                        End If
                        _response.Initialize(_raw, _offset)
                        Dim ch As Char = _raw.Chars(3)
                        If ch = " "c Then
                            _step = 0
                            Return _response
                        End If
                        If Not (ch = "-"c) Then
                            Throw New FtpException("InvalidResponse")
                        End If
                        _codeText = String.Concat("" & vbCrLf, _response.Code, " ")
                        _step = 2
                    End If
                Loop While Not (_step = 2)
                i = _raw.IndexOf(_codeText, _offset)
            Loop While i < 0
            _step = 0
            _response.SetReplyData(_raw, i + 2)
            Return _response
        End Function

    End Class

End Namespace