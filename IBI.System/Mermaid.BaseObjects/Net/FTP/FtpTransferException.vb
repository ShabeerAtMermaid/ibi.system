Imports System

Namespace Net.FTP
    Class FtpTransferException
        Inherits Exception

        Public Sub New(ByVal inner As Exception)
            MyBase.New(inner.Message, inner)
        End Sub
    End Class
End Namespace