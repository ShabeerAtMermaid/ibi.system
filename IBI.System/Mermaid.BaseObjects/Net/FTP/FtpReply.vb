#Region "See http://www.faqs.org/rfcs/rfc959.html"
' FtpReply class is a wraper around command replies
#End Region

Imports System
Namespace Net.FTP
    Public Class FtpReply
        Private myCode As Integer
        Private myDescription As String
        Private myData As String

        Public ReadOnly Property ReplyData() As String
            Get
                If myData Is Nothing Then
                    myData = String.Concat(New Object() {myCode, " ", myDescription, "" & vbCrLf})
                End If
                Return myData
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return myDescription
            End Get
        End Property

        'Command reply code in the range between 100 and 600
        Public ReadOnly Property Code() As Integer
            Get
                Return myCode
            End Get
        End Property

        'Ftp state diagrams use first digit of the reply code
        Public ReadOnly Property Group() As Integer
            Get
                Return myCode / 100
            End Get
        End Property

        Friend Sub New()
        End Sub

        Public Sub New(ByVal code As Integer, ByVal description As String, ByVal data As String)
            If code < 100 OrElse code >= 600 Then
                Throw New ArgumentOutOfRangeException("code")
            End If
            myCode = code
            myDescription = description
            myData = data
        End Sub

        Friend Sub Initialize(ByVal firstLine As String, ByVal endOfLine As Integer)
            Try
                myCode = Int32.Parse(firstLine.Substring(0, 3))
                myDescription = firstLine.Substring(4, endOfLine - 4).Trim
                myData = Nothing
            Catch
                Throw New FtpException("Invalid Reply", FtpExceptionStatus.ServerProtocolViolation)
            End Try
            If myCode < 100 OrElse myCode >= 600 Then
                Throw New FtpException("Invalid Reply Code", FtpExceptionStatus.ServerProtocolViolation)
            Else
                Return
            End If
        End Sub

        Friend Sub SetReplyData(ByVal data As String, ByVal offset As Integer)
            myDescription = data.Substring(offset + 4, data.Length - offset - 6)
            myData = data
        End Sub

        Friend Function Clone() As FtpReply
            Return New FtpReply(myCode, myDescription, myData)
        End Function

        Friend Sub Reset()
            myCode = 0
            myDescription = Nothing
            myData = Nothing
        End Sub

    End Class

End Namespace