Imports System
Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Net
Imports System.Collections.Generic

Namespace Net.FTP

    Public NotInheritable Class Parser
        Private Shared months As String() = New String() {"jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"}
        Private Shared _DateTimeFormats As List(Of String)

        Friend Shared ReadOnly Property DateTimeFormats() As List(Of String)
            Get
                If _DateTimeFormats Is Nothing Then
                    _DateTimeFormats = New List(Of String)
                    _DateTimeFormats.Add("MM-dd-yy  hh:mmtt")
                    _DateTimeFormats.Add("MMM dd HH:mm")

                End If

                Return _DateTimeFormats

            End Get
        End Property

        Private Shared Function GuessYear(ByVal month As Integer, ByVal day As Integer) As Integer
            Dim j As Integer
            Try
                Dim i As Integer = DateTime.Now.Year - 1
                Dim date1 As DateTime = New DateTime(i, month, day)
                Dim timeLeft As TimeSpan = DateTime.Now.Subtract(date1)
                If timeLeft.TotalDays < 350 Then
                    j = i
                Else
                    j = i + 1
                End If
            Catch
                j = DateTime.Now.Year
            End Try
            Return j
        End Function

        Private Shared Function GetLong(ByVal buf As String) As Long
            Dim l As Long
            Try
                l = Int64.Parse(buf)
            Catch
                l = -1
            End Try
            Return l
        End Function

        Private Shared Function GetInt(ByVal buf As String) As Integer
            Dim i As Integer
            Try
                i = Int32.Parse(buf)
            Catch
                i = -1
            End Try
            Return i
        End Function

        Private Shared Function GetMonth(ByVal buf As String) As Integer
            If buf.Length = 3 Then
                buf = buf.ToLower
                Dim i As Integer = 0
                While i < 12
                    If buf = months(i) Then
                        Return i + 1
                    End If
                    i += 1
                End While
            End If
            Return 0
        End Function

        Private Shared Function NextPart(ByVal buf As String, ByRef i As Integer) As String
            If i < 0 OrElse i > buf.Length Then
                i = -1
                Return Nothing
            End If
            If i = buf.Length Then
                i = -1
                Return Nothing
            End If
            While buf.Chars(i) = " "c OrElse buf.Chars(i) = Chr(9)
                i += 1
                If i = buf.Length Then
                    i = -1
                    Return Nothing
                End If
            End While
            Dim i2 As Integer = i
            While Not (buf.Chars(i) = " "c) AndAlso Not (buf.Chars(i) = Chr(9))
                i += 1
                If i = buf.Length Then
                End If
            End While
            Return buf.Substring(i2, i - i2)
        End Function

        Private Shared Function Parse(ByVal buf As String) As FtpItem
            Dim m As Match = GetMatchingRegex(buf)

            If m Is Nothing Then
                Return Nothing

            Else
                Dim tmpFilename As String = m.Groups("name").Value

                Dim tmpSize As Long
                Int64.TryParse(m.Groups("size").Value, tmpSize)

                Dim tmpPermission As String = m.Groups("permission").Value

                Dim tmpDir As String = m.Groups("dir").Value
                Dim tmpItemType As FtpItemType

                If tmpDir <> "" And tmpDir <> "-" Then
                    tmpItemType = FtpItemType.Directory

                Else
                    tmpItemType = FtpItemType.File

                End If

                Dim tmpFileDate As Date = Date.MinValue

                Dim tmpDate As String = m.Groups("timestamp").Value

                tmpFileDate = ParseDate(tmpDate)

                Return CreateFtpItem(tmpFilename, tmpSize, tmpItemType, tmpFileDate, Nothing)

            End If

        End Function

        Private Shared Function ParseDate(ByVal dateString As String) As Date
            For i As Integer = 0 To DateTimeFormats.Count - 1
                Try
                    Return DateTime.ParseExact(dateString, DateTimeFormats(i), Globalization.CultureInfo.InvariantCulture)

                Catch inner_ex As Exception
                    'SILENT

                End Try
            Next

            Return Converter.ToDateTime(dateString)

        End Function

        Private Shared Function GetMatchingRegex(ByVal line As String) As Match
            Dim rx As Regex
            Dim m As Match

            For i As Integer = 0 To _ParseFormats.Length - 1
                rx = New Regex(_ParseFormats(i))
                m = rx.Match(line)

                If m.Success Then
                    Return m

                End If
            Next

            Return Nothing

        End Function

#Region "Regular expressions for parsing LIST results"

        Private Shared _ParseFormats As String() = New String() { _
         "(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\w+\s+\w+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{4})\s+(?<name>.+)", _
         "(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\d+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{4})\s+(?<name>.+)", _
         "(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\d+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{1,2}:\d{2})\s+(?<name>.+)", _
         "(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})\s+\d+\s+\w+\s+\w+\s+(?<size>\d+)\s+(?<timestamp>\w+\s+\d+\s+\d{1,2}:\d{2})\s+(?<name>.+)", _
         "(?<dir>[\-d])(?<permission>([\-r][\-w][\-xs]){3})(\s+)(?<size>(\d+))(\s+)(?<ctbit>(\w+\s\w+))(\s+)(?<size2>(\d+))\s+(?<timestamp>\w+\s+\d+\s+\d{2}:\d{2})\s+(?<name>.+)", _
         "(?<timestamp>\d{2}\-\d{2}\-\d{2}\s+\d{2}:\d{2}[Aa|Pp][mM])\s+(?<dir>\<\w+\>){0,1}(?<size>\d+){0,1}\s+(?<name>.+)", _
         "(?<timestamp>\d{2}\-\d{2}\-\d{2}\s+\d{2}:\d{2}[Aa|Pp][mM])\s+(?<size>\d+){0,1}\s+(?<name>.+)"}

#End Region

        Private Shared Function CreateFtpItem(ByVal filename As String, ByVal size As Long, ByVal type As FtpItemType, ByVal lastModified As DateTime, ByVal symlinkPath As String) As FtpItem
            If size < 0 Then
                size = 0
            End If
            If filename Is Nothing OrElse filename.Length = 0 Then
                Return Nothing
            Else
                Return New FtpItem(filename, size, type, lastModified, symlinkPath)
            End If
        End Function

        Public Shared Function ParseLine(ByVal line As String) As FtpItem
            Dim item As FtpItem = Nothing
            If line Is Nothing Then
                Return Nothing
            End If
            line = line.Trim
            If line.Length < 2 Then
                Return Nothing
            End If
            Return Parse(line)
        End Function

        Private Shared Function ParseDescription(ByVal text As String) As String
            Return text.Substring(4, text.IndexOfAny(New Char() {vbCr, vbLf}) - 4).Trim

        End Function

        Public Shared Function ParseFileLength(ByVal text As String) As Long
            Try
                Return Int64.Parse(ParseDescription(text))
            Catch
                Throw New FtpException("CannotParse", FtpExceptionStatus.ServerProtocolViolation)
            End Try
        End Function

        Public Shared Function ParseCurrentDirectory(ByVal text As String) As String
            Try
                Dim startIndex As Integer = text.IndexOf("""") + 1
                Dim noMessageID As String = text.Substring(startIndex)
                Dim endIndex As Integer = noMessageID.IndexOf("""")
                Return noMessageID.Substring(0, endIndex)

            Catch
                Throw New FtpException("CannotParse", FtpExceptionStatus.ServerProtocolViolation)
            End Try
        End Function

        Public Shared Function SplitRawList(ByVal encoding As System.Text.Encoding, ByVal buffer As MemoryStream) As String()
            Dim str As String = encoding.GetString(buffer.GetBuffer, 0, buffer.Length)
            Dim chs As Char() = New Char() {vbCr, vbLf}
            str = str.TrimEnd(chs).Replace(vbCrLf, vbCr)
            If str.Length = 0 Then
                Return New String(0) {}
            End If
            chs = New Char() {vbCr}
            Return str.Split(chs)
        End Function

        Public Shared Function EndPointToString(ByVal endPoint As IPEndPoint) As String
            Dim bs As Byte() = ProxySocket.GetAddressBytes(endPoint.Address)
            Dim i As Integer = endPoint.Port
            Return String.Format("{0},{1},{2},{3},{4},{5}", New Object() {bs(0), bs(1), bs(2), bs(3), (i >> 8 And 255), (i And 255)})
        End Function

        Public Shared Function ParseResponseEndPoint(ByVal text As String) As IPEndPoint
            Dim description As String = ParseDescription(text)
            Dim address As String = ""
            Dim i As Integer = 0
            While i < description.Length
                Dim ch As Char = description.Chars(i)
                If Not (ch = " "c) OrElse Not (address.Length = 0) AndAlso Not (address.Chars(address.Length - 1) = " "c) Then
                    If ch >= "0"c AndAlso ch <= "9"c Then
                        address = String.Concat(address, ch)
                    Else
                        If ch = "."c OrElse ch = ","c Then
                            address = String.Concat(address, "."c)
                        Else
                            If ch = " "c Then
                                address = String.Concat(address, " "c)
                            End If
                        End If
                    End If
                End If
                i += 1
            End While
            Dim separators As Char() = New Char() {" "c, "."c}
            Dim str2 = address.Trim(separators)
            Dim spaceCh = New Char() {" "c}
            Dim dotCh = New Char() {"."c}
            Dim strs1 As String() = str2.Split(spaceCh)
            Dim j As Integer = 0
            While j < strs1.Length
                str2 = strs1(j).Trim(separators)
                If str2.Length >= 11 AndAlso str2.Length <= 23 Then
                    Dim strs2 As String() = str2.Split(dotCh)
                    If strs2.Length = 6 Then
                        Try
                            Dim addr As Long = CType(Byte.Parse(strs2(0)), Long) + (CType(Byte.Parse(strs2(1)), Long) << 8) + (CType(Byte.Parse(strs2(2)), Long) << 16) + (CType(Byte.Parse(strs2(3)), Long) << 24)
                            Dim prt As Integer = (CType(Byte.Parse(strs2(4)), Integer) << 8) + Byte.Parse(strs2(5))
                            Dim endPoint As IPEndPoint = New IPEndPoint(addr, prt)
                            Return endPoint
                        Catch ex As Exception
                        End Try
                    End If
                End If
                j += 1
            End While

            Throw New FtpException("CannotParse", FtpExceptionStatus.ServerProtocolViolation)
        End Function

    End Class
End Namespace