Imports System
Namespace Net.FTP

    Public Enum FtpItemType As Integer
        File = 0
        Directory = 1
        SymLink = 2
    End Enum

    Public Class FtpItem

        Private myFileName As String
        Private myType As FtpItemType
        Private mySize As Long
        Private myModifiedTime As DateTime
        Private mySymLinkPath As String

        Public ReadOnly Property Name() As String
            Get
                Return myFileName
            End Get
        End Property

        Public ReadOnly Property IsDirectory() As Boolean
            Get
                Return myType = FtpItemType.Directory
            End Get
        End Property

        Public ReadOnly Property IsFile() As Boolean
            Get
                Return myType = FtpItemType.File
            End Get
        End Property

        Public ReadOnly Property Size() As Integer
            Get
                Return mySize
            End Get
        End Property

        Public ReadOnly Property Modified() As DateTime
            Get
                Return myModifiedTime
            End Get
        End Property

        Public Shared Function Parse(ByVal rawLine As String) As FtpItem
            Return Parser.ParseLine(rawLine)
        End Function

        Public Sub New(ByVal filename As String, ByVal size As Long, ByVal type As FtpItemType, ByVal lastModified As DateTime, ByVal symlinkPath As String)
            If filename Is Nothing Then
                Throw New ArgumentNullException("filename")
            End If
            If filename.Length = 0 Then
                Throw New ArgumentException("ArgumentEmptyString", filename)
            End If
            If size < 0 Then
                Throw New ArgumentOutOfRangeException("size")
            End If
            If Not [Enum].IsDefined(GetType(FtpItemType), type) Then
                Throw New ArgumentOutOfRangeException("type")
            End If
            If type = FtpItemType.SymLink AndAlso symlinkPath Is Nothing Then
                Throw New ArgumentNullException("symlinkPath")
            End If
            myFileName = filename
            mySize = size
            myType = type
            myModifiedTime = lastModified
            mySymLinkPath = symlinkPath
        End Sub

        Public Sub New(ByVal filename As String, ByVal size As Long, ByVal type As FtpItemType)
            MyClass.New(filename, size, type, DateTime.Now, Nothing)
        End Sub

    End Class
End Namespace