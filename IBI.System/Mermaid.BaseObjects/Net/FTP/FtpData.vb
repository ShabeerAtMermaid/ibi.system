Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets

Namespace Net.FTP
    Class FtpData
        Implements IDisposable
        Protected Const BufferSize As Integer = 65536
        Private myInTransfer As Boolean = False
        Private myIsDisposed As Boolean = False
        Private mySize As Long = 0
        Private myState As FtpTransferState = FtpTransferState.None
        Private myService As FtpService
        Protected mySocket As ProxySocket
        Protected myStream As Stream = Nothing
        Protected myBuffer As Byte() = New Byte(BufferSize) {}


        Public ReadOnly Property Length() As Long
            Get
                Return mySize
            End Get
        End Property

        Public ReadOnly Property State() As FtpTransferState
            Get
                Return myState
            End Get
        End Property

        Public Sub New(ByVal ftp As FtpService, ByVal stream As Stream)
            myService = ftp
            myStream = stream
            myState = FtpTransferState.Downloading
            mySocket = ftp.ActiveProxy.CreateSocket
        End Sub

        Friend Sub New(ByVal ftp As FtpService, ByVal stream As Stream, ByVal state As FtpTransferState)
            myService = ftp
            myStream = stream
            myState = state
            mySocket = ftp.ActiveProxy.CreateSocket
        End Sub

        Private Function GetAvailable() As Integer
            Return mySocket.Available
        End Function

        Private Function Receive(ByVal buffer As Byte()) As Integer
            Return mySocket.Receive(myBuffer)
        End Function

        Public Function ReadData() As Integer
            If Not myInTransfer Then
                Return -1
            End If
            If GetAvailable() = 0 Then
                If Not mySocket.Socket.Poll(100, SelectMode.SelectRead) Then
                    Return 0
                End If
                If mySocket.Available = 0 Then
                    Return -1
                End If
            End If

            Dim i As Integer = Receive(myBuffer)
            If i = 0 Then
                Return 0
            End If
            If i > 0 Then
                myStream.Write(myBuffer, 0, i)
                mySize += i
                Return i
            End If
            Try
                mySocket.Shutdown(SocketShutdown.Send)
            Catch
            End Try
            mySocket.Close()
            mySocket = Nothing
            myInTransfer = False
            Return -1
        End Function

        Public Function WriteData() As Integer
            If Not myInTransfer Then
                Return -1
            End If

            Dim i As Integer = myStream.Read(myBuffer, 0, myBuffer.Length)
            If i > 0 Then
                i = mySocket.Send(myBuffer, i, SocketFlags.None)
                mySize += i
                Return i
            End If
            Try
                mySocket.Shutdown(SocketShutdown.Send)
            Catch
            End Try
            mySocket.Close()
            mySocket = Nothing
            myInTransfer = False
            Return -1
        End Function

        Public Sub Connect(ByVal remote As IPEndPoint)
            Dim iAsyncResult As IAsyncResult = mySocket.BeginConnect(remote, Nothing, Nothing)
            Try
                myService.WaitForCompletion(iAsyncResult)
                mySocket.EndConnect(iAsyncResult)
                myInTransfer = True
                Return
            Catch e1 As ProxySocketException
                mySocket.Close()
                Throw New FtpException(e1.Message, e1, FtpExceptionStatus.ConnectFailure)
            Catch e2 As Exception
                mySocket.Close()
                Throw e2
            End Try
        End Sub

        Public Function Listen(ByVal controlSocket As ProxySocket) As IPEndPoint
            Dim iAsyncResult As IAsyncResult = mySocket.BeginListen(controlSocket, Nothing, Nothing)
            Try
                myService.WaitForCompletion(iAsyncResult)
                Dim iPEndPoint As IPEndPoint = mySocket.EndListen(iAsyncResult)
                Return iPEndPoint
            Catch proxyEx As ProxySocketException
                mySocket.Close()
                Throw New FtpException(proxyEx.Message, proxyEx, FtpExceptionStatus.ConnectFailure)
            Catch ex As Exception
                mySocket.Close()
                Throw ex
            End Try
        End Function

        Public Sub Accept()
            Dim iAsyncResult As IAsyncResult = mySocket.BeginAccept(Nothing, Nothing)
            Try
                myService.WaitForCompletion(iAsyncResult)
                mySocket.EndAccept(iAsyncResult)
                myInTransfer = True
                Return
            Catch e As Exception
                mySocket.Close()
                Throw e
            End Try
        End Sub

        Public Overridable Sub Dispose() Implements IDisposable.Dispose
            If myIsDisposed Then
                Return
            End If
            If Not (mySocket Is Nothing) Then
                mySocket.Close()
                mySocket = Nothing
            End If
            myIsDisposed = True
            GC.SuppressFinalize(Me)
        End Sub
    End Class

End Namespace