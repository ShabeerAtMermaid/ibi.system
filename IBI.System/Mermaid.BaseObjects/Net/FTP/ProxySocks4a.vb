Imports System.Net.Sockets

Namespace Net.FTP
    Class ProxySocks4a
        Inherits ProxySocks4Base

        Public Sub New(ByVal socket As Socket)
            MyBase.New(socket, True)
        End Sub
    End Class
End Namespace
