Imports System
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading

Namespace Net.FTP
    MustInherit Class ProxySocks4Base
        Inherits ProxyBase
        Private _remoteEndPoint As IPEndPoint
        Private _socks4a As Boolean = False
        Private _command As Byte = 0
        Private _address As Long = 16777216
        Private _port As Integer = 0
        Private _serverName As String = Nothing

        Public Sub New(ByVal socket As Socket, ByVal socks4a As Boolean)
            MyBase.New(socket)
            _socks4a = socks4a
        End Sub

        Public Overloads Overrides Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            _command = 1
            _address = 16777216
            _port = serverPort
            _serverName = serverName
            MyBase.Begin("Connect", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = ProxySocket.ToEndPoint(myProxyHost, myProxyPort)
                If iPEndPoint Is Nothing Then
                    Dns.BeginResolve(myProxyHost, New AsyncCallback(AddressOf Me.Connect0), Nothing)
                Else
                    If Not _socks4a AndAlso Not (_serverName Is Nothing) Then
                        Dns.BeginResolve(_serverName, New AsyncCallback(AddressOf Me.Connect1), iPEndPoint)
                    Else
                        MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect2), Nothing)
                    End If
                End If
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginConnect(ByVal endPoint As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            _command = 1
            _address = ProxySocket.GetAddressLong(endPoint.Address)
            _port = endPoint.Port
            _serverName = Nothing
            MyBase.Begin("Connect", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = ProxySocket.ToEndPoint(myProxyHost, myProxyPort)
                If Not (iPEndPoint Is Nothing) Then
                    MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect2), Nothing)
                Else
                    Dns.BeginResolve(myProxyHost, New AsyncCallback(AddressOf Me.Connect0), Nothing)
                End If
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginListen(ByVal controlSocket As ProxyBase, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Dim proxySocks4Base As ProxySocks4Base = CType(controlSocket, ProxySocks4Base)
            Dim l As Long = ProxySocket.GetAddressLong(proxySocks4Base._remoteEndPoint.Address)
            If Not ((l And 16777215) = 0) Then
                _command = 2
                _address = l
                _port = proxySocks4Base._remoteEndPoint.Port
                _serverName = Nothing
            Else
                _command = 2
                _address = 16777216
                _port = proxySocks4Base._remoteEndPoint.Port
                _serverName = proxySocks4Base._serverName
            End If
            MyBase.Begin("Listen", callback, state)
            Try
                MyBase.Socket.BeginConnect(controlSocket.Socket.RemoteEndPoint, New AsyncCallback(AddressOf Me.Connect2), Nothing)
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            MyBase.Begin("Accept", callback, state)
            Try
                BeginReadReply()
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Private Sub Connect0(ByVal result As IAsyncResult)
            Try
                Dim iPHostEntry As IPHostEntry
                Try
                    iPHostEntry = Dns.EndResolve(result)
                Catch e1 As SocketException
                    MyBase.SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.ProxyNameResolutionFailure, e1))
                    MyBase.Finish()
                    Return
                End Try
                Dim iPEndPoint As IPEndPoint = New IPEndPoint(iPHostEntry.AddressList(0), myProxyPort)
                If Not _socks4a AndAlso Not (_serverName Is Nothing) Then
                    Try
                        Dns.BeginResolve(_serverName, New AsyncCallback(AddressOf Me.Connect1), iPEndPoint)
                    Catch e2 As SocketException
                        MyBase.SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.NameResolutionFailure, e2))
                        MyBase.Finish()
                    End Try
                Else
                    MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect2), Nothing)
                End If
            Catch e3 As Exception
                MyBase.SetError(e3)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect1(ByVal result As IAsyncResult)
            Try
                Dim iPHostEntry As IPHostEntry
                Dim iPEndPoint As IPEndPoint = CType(result.AsyncState, IPEndPoint)
                Try
                    iPHostEntry = Dns.EndResolve(result)
                Catch e1 As SocketException
                    MyBase.SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.NameResolutionFailure, e1))
                    MyBase.Finish()
                    Return
                End Try
                _address = ProxySocket.GetAddressLong(iPHostEntry.AddressList(0))
                _serverName = Nothing
                MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect2), Nothing)
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect2(ByVal result As IAsyncResult)
            Try
                MyBase.Socket.EndConnect(result)
            Catch e1 As Exception
                MyBase.SetError(New ProxySocketException(e1.Message, ProxySocketExceptionStatus.ConnectFailure, e1))
                MyBase.Finish()
                Return
            End Try
            Try
                Command()
                BeginReadReply()
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Command()
            Dim i As Integer = 9 + myProxyUserName.Length
            If Not (_serverName Is Nothing) Then
                i += _serverName.Length + 1
            End If
            Dim bs(i) As Byte
            bs(0) = 4
            bs(1) = _command
            bs(2) = CType((_port >> 8), Byte)
            bs(3) = CType((_port And 255), Byte)
            bs(4) = CType((_address And 255), Byte)
            bs(5) = CType((_address >> 8 And CType(255, Long)), Byte)
            bs(6) = CType((_address >> 16 And CType(255, Long)), Byte)
            bs(7) = CType((_address >> 24 And CType(255, Long)), Byte)
            System.Text.Encoding.UTF8.GetBytes(myProxyUserName, 0, myProxyUserName.Length, bs, 8)
            If Not (_serverName Is Nothing) Then
                System.Text.Encoding.UTF8.GetBytes(_serverName, 0, _serverName.Length, bs, myProxyUserName.Length + 9)
            End If
            MyBase.Socket.Send(bs)
        End Sub

        Private Sub BeginReadReply()
            Dim bs(8) As Byte
            MyBase.Socket.BeginReceive(bs, 0, 8, SocketFlags.None, New AsyncCallback(AddressOf Me.ReadReplyCallback), bs)
        End Sub

        Private Sub ReadReplyCallback(ByVal result As IAsyncResult)
            Try
                Dim bs As Byte() = CType(result.AsyncState, Byte())
                Dim i As Integer = MyBase.Socket.EndReceive(result)
                While i < 8
                    While MyBase.Available = 0
                        Thread.Sleep(1)
                    End While
                    i += MyBase.Socket.Receive(bs, i, 8 - i, SocketFlags.None)
                End While
                If Not (bs(1) = 90) Then
                    Throw New ProxySocketException("Socks4RequestFailed", ProxySocketExceptionStatus.ProtocolError)
                End If
                _remoteEndPoint = GetEndPoint(bs, 2)
                myAsyncResult.SetResult(_remoteEndPoint)
            Catch e As Exception
                MyBase.SetError(e)
            Finally
                MyBase.Finish()
            End Try
        End Sub

        Private Function GetEndPoint(ByVal buffer As Byte(), ByVal offset As Integer) As IPEndPoint
            Dim i As Integer = (buffer(offset) << 8) + buffer(offset + 1)
            Dim l As Long = CType((buffer(offset + 5) << 24), Integer) + CType((buffer(offset + 4) << 16), Long) + CType((buffer(offset + 3) << 8), Long) + CType(buffer(offset + 2), Integer)
            If l = CType(0, Long) Then
                l = _address
            End If
            If i = 0 Then
                i = _port
            End If
            Return New IPEndPoint(l, i)
        End Function
    End Class
End Namespace