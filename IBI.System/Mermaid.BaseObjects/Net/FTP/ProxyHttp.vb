Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Namespace Net.FTP
    Class ProxyHttp
        Inherits ProxyBase
        Private request As String
        Private r As Byte() = New Byte(16) {}

        Public Sub New(ByVal socket As Socket)
            MyBase.New(socket)
        End Sub

        Public Overloads Overrides Function BeginConnect(ByVal endPoint As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Return BeginConnect(endPoint.Address.ToString, endPoint.Port, callback, state)
        End Function

        Public Overloads Overrides Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            request = String.Concat(New Object() {"CONNECT ", serverName, ":", serverPort, " HTTP/1.1" & vbCrLf & "Host: ", serverName, ":", serverPort, "" & vbCrLf})
            If myProxyUserName.Length > 0 AndAlso myProxyPassword.Length > 0 Then
                Dim bs As Byte() = System.Text.Encoding.UTF8.GetBytes(String.Concat(myProxyUserName, ":", myProxyPassword))
                request = String.Concat(request, "Proxy-Authorization: basic ", Convert.ToBase64String(bs, 0, CType(bs.Length, Integer)), "" & vbCrLf)
            End If
            request = String.Concat(request, "" & vbCrLf)
            MyBase.Begin("Connect", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = ProxySocket.ToEndPoint(myProxyHost, myProxyPort)
                If Not (iPEndPoint Is Nothing) Then
                    MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
                Else
                    Dns.BeginResolve(myProxyHost, New AsyncCallback(AddressOf Me.Connect0), Nothing)
                End If
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Private Sub Connect0(ByVal result As IAsyncResult)
            Try
                Dim iPHostEntry As IPHostEntry
                Try
                    iPHostEntry = Dns.EndResolve(result)
                Catch e1 As SocketException
                    MyBase.SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.ProxyNameResolutionFailure, e1))
                    MyBase.Finish()
                    Return
                End Try
                Dim iPEndPoint As IPEndPoint = New IPEndPoint(iPHostEntry.AddressList(0), myProxyPort)
                MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect1(ByVal result As IAsyncResult)
            Try
                MyBase.Socket.EndConnect(result)
            Catch e1 As Exception
                MyBase.SetError(New ProxySocketException(e1.Message, ProxySocketExceptionStatus.ConnectFailure, e1))
                MyBase.Finish()
                Return
            End Try
            Try
                Dim bs As Byte() = System.Text.Encoding.UTF8.GetBytes(request)
                MyBase.Socket.BeginSend(bs, 0, CType(bs.Length, Integer), SocketFlags.None, New AsyncCallback(AddressOf Me.Connect2), Nothing)
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect2(ByVal result As IAsyncResult)
            Try
                Dim i As Integer
                If result.AsyncState Is Nothing Then
                    MyBase.Socket.EndSend(result)
                    i = 0
                Else
                    i = CType(result.AsyncState, Integer)
                    i += MyBase.Socket.EndReceive(result)
                End If
                If i < 12 Then
                    MyBase.Socket.BeginReceive(r, i, 12 - i, SocketFlags.None, New AsyncCallback(AddressOf Me.Connect2), i)
                Else
                    Dim str As String = System.Text.Encoding.UTF8.GetString(r, 0, 12)
                    Dim memoryStream As MemoryStream = New MemoryStream
                    memoryStream.Write(r, 0, 12)
                    If Not (str.Substring(0, 5) = "HTTP/") Then
                        Throw New ProxySocketException("HttpInvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
                    End If
                    MyBase.Socket.BeginReceive(r, 0, 1, SocketFlags.None, New AsyncCallback(AddressOf Me.Connect3), memoryStream)
                End If
            Catch e As Exception
                MyBase.SetError(e)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect3(ByVal result As IAsyncResult)
            Try
                Dim i As Integer = MyBase.Socket.EndReceive(result)
                Dim memoryStream As MemoryStream = CType(result.AsyncState, MemoryStream)
                memoryStream.Write(r, 0, i)
                If Not MyBase.Socket.Connected Then
                    Throw New ProxySocketException("HttpInvalidResponse", ProxySocketExceptionStatus.ConnectionClosed)
                End If
                Dim bs As Byte() = memoryStream.GetBuffer
                i = CType(memoryStream.Length, Integer)
                If Not (bs(i - 4) = 13) OrElse Not (bs(i - 3) = 10) OrElse Not (bs(i - 2) = 13) OrElse Not (bs(i - 1) = 10) Then
                    MyBase.Socket.BeginReceive(r, 0, 1, SocketFlags.None, New AsyncCallback(AddressOf Me.Connect3), memoryStream)
                Else
                    Dim str As String = System.Text.Encoding.UTF8.GetString(bs, 0, i)
                    Dim j As Integer = str.IndexOf("" & vbCrLf)
                    If j < 13 Then
                        Throw New ProxySocketException("HttpInvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
                    End If
                    If Not (str.Chars(9) = "2"c) Then
                        Throw New ProxySocketException("HttpError", ProxySocketExceptionStatus.ProtocolError)
                    End If
                    memoryStream.Close()
                    MyBase.Finish()
                End If
            Catch e As Exception
                MyBase.SetError(e)
                MyBase.Finish()
            End Try
        End Sub

        Public Overloads Overrides Function BeginListen(ByVal controlSocket As ProxyBase, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Throw New NotSupportedException("HttpNotSupported")
        End Function

        Public Overloads Overrides Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Throw New NotSupportedException("HttpNotSupported")
        End Function
    End Class
End Namespace