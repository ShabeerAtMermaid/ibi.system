#Region "Implementation of SOCKS Protocol Version 5"
' For protocol specification, please see:
' RFC 1928 http://www.faqs.org/rfcs/rfc1928.html
' RFC 1929 http://www.faqs.org/rfcs/rfc1929.html
#End Region

Imports System
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Threading
Imports System.Reflection
Imports System.Net.Sockets
Imports System.Collections
Imports System.Text.RegularExpressions

Namespace Net.FTP
    Class ProxySocks5
        Inherits ProxyBase
        Private _command As Byte = 0
        Private _address As Long = 16777216
        Private _port As Integer = 0
        Private _serverName As String = Nothing

        Public Sub New(ByVal socket As Socket)
            MyBase.New(socket)
        End Sub

        Public Overloads Overrides Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            _command = 1
            _address = 16777216
            _port = serverPort
            _serverName = serverName
            MyBase.Begin("Connect", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = ProxySocket.ToEndPoint(myProxyHost, myProxyPort)
                If Not (iPEndPoint Is Nothing) Then
                    MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
                Else
                    Dns.BeginResolve(myProxyHost, New AsyncCallback(AddressOf Me.Connect0), Nothing)
                End If
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginConnect(ByVal endPoint As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            _command = 1
            _address = ProxySocket.GetAddressLong(endPoint.Address)
            _port = endPoint.Port
            _serverName = Nothing
            MyBase.Begin("Connect", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = ProxySocket.ToEndPoint(myProxyHost, myProxyPort)
                If Not (iPEndPoint Is Nothing) Then
                    MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
                Else
                    Dns.BeginResolve(myProxyHost, New AsyncCallback(AddressOf Me.Connect0), Nothing)
                End If
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginListen(ByVal controlSocket As ProxyBase, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Dim proxySocks5 As ProxySocks5 = CType(controlSocket, ProxySocks5)
            _command = 2
            _address = proxySocks5._address
            _port = proxySocks5._port
            _serverName = proxySocks5._serverName
            MyBase.Begin("Listen", callback, state)
            Try
                MyBase.Socket.BeginConnect(proxySocks5.Socket.RemoteEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Public Overloads Overrides Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            MyBase.Begin("Accept", callback, state)
            Try
                BeginReadReply()
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw MyBase.CancelBegin(e)
            End Try
        End Function

        Private Sub Connect0(ByVal result As IAsyncResult)
            Try
                Dim iPHostEntry As IPHostEntry
                Try
                    iPHostEntry = Dns.EndResolve(result)
                Catch e1 As SocketException
                    MyBase.SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.ProxyNameResolutionFailure, e1))
                    MyBase.Finish()
                    Return
                End Try
                Dim iPEndPoint As IPEndPoint = New IPEndPoint(iPHostEntry.AddressList(0), myProxyPort)
                MyBase.Socket.BeginConnect(iPEndPoint, New AsyncCallback(AddressOf Me.Connect1), Nothing)
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub Connect1(ByVal result As IAsyncResult)
            Try
                MyBase.Socket.EndConnect(result)
            Catch e1 As Exception
                MyBase.SetError(New ProxySocketException(e1.Message, ProxySocketExceptionStatus.ConnectFailure, e1))
                MyBase.Finish()
                Return
            End Try
            Try
                Authenticate()
                Command()
                BeginReadReply()
            Catch e2 As Exception
                MyBase.SetError(e2)
                MyBase.Finish()
            End Try
        End Sub

        Private Sub ReceiveAll(ByVal buffer As Byte(), ByVal offset As Integer, ByVal count As Integer)
            Dim i As Integer
            While count > 0
                While MyBase.Available = 0
                    Thread.Sleep(1)
                End While
                i = MyBase.Socket.Receive(buffer, offset, count, SocketFlags.None)
                offset += i
                count -= i
            End While
        End Sub

        Private Sub Authenticate()
            Dim authenticationResponse As Byte()

            'Request authentication methods
            '+----+----------+----------+
            '|VER | NMETHODS | METHODS  |
            '+----+----------+----------+
            '| 1  |    1     | 1 to 255 |
            '+----+----------+----------+
            ' We are interested in:
            ' X'00' NO AUTHENTICATION REQUIRED
            ' X'02' USERNAME/PASSWORD
            Dim methodSelector As Byte() = New Byte() {5, 2, 0, 2}
            MyBase.Socket.Send(methodSelector)

            Dim selectionMessage(2) As Byte
            ReceiveAll(selectionMessage, 0, 2)

            If selectionMessage(0) = 0 Then
                Throw New ProxySocketException("Socks5NotSupported", ProxySocketExceptionStatus.ProtocolError)
            End If
            If Not (selectionMessage(0) = 5) Then
                Throw New ProxySocketException("Socks5InvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
            End If

            'Check returned method and perform authentication
            Select Case selectionMessage(1)
                Case 0 'no authentication
                    Return
                Case 2 'name/password authentication
                    'Request
                    '+----+------+----------+------+----------+
                    '|VER | ULEN |  UNAME   | PLEN |  PASSWD  |
                    '+----+------+----------+------+----------+
                    '| 1  |  1   | 1 to 255 |  1   | 1 to 255 |
                    '+----+------+----------+------+----------+
                    Dim authenticationRequest(CType((3 + myProxyUserName.Length + myProxyPassword.Length), Integer)) As Byte
                    System.Text.Encoding.UTF8.GetBytes(myProxyUserName, 0, myProxyUserName.Length, authenticationRequest, 2)
                    System.Text.Encoding.UTF8.GetBytes(myProxyPassword, 0, myProxyPassword.Length, authenticationRequest, 3 + myProxyUserName.Length)
                    authenticationRequest(0) = 1
                    authenticationRequest(1) = CType(myProxyUserName.Length, Byte)
                    authenticationRequest(2 + myProxyUserName.Length) = CType(myProxyPassword.Length, Byte)
                    MyBase.Socket.Send(authenticationRequest)
                    authenticationResponse = New Byte(2) {}
                    ReceiveAll(authenticationResponse, 0, 2)
                    If Not (authenticationResponse(0) = 1) Then
                        Throw New ProxySocketException("Socks5InvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
                    End If
                    If Not (authenticationResponse(1) = 0) Then
                        Throw New ProxySocketException("Socks5AuthenticationFailed", ProxySocketExceptionStatus.ProtocolError)
                    End If
                Case Else
                    If Not (selectionMessage(1) = Byte.MaxValue) Then
                        Throw New ProxySocketException("Socks5InvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
                    Else
                        Throw New ProxySocketException("Socks5NoAcceptableMethods", ProxySocketExceptionStatus.ProtocolError)
                    End If
            End Select
        End Sub

        Private Sub Command()
            Dim bs As Byte()
            If _serverName Is Nothing Then
                bs = New Byte(10) {}
                bs(3) = 1
                bs(4) = CType((_address And 255), Byte)
                bs(5) = CType((_address >> 8 And CType(255, Long)), Byte)
                bs(6) = CType((_address >> 16 And CType(255, Long)), Byte)
                bs(7) = CType((_address >> 24 And CType(255, Long)), Byte)
            Else
                bs = New Byte(CType((7 + _serverName.Length), Integer)) {}
                bs(3) = 3
                bs(4) = CType(_serverName.Length, Byte)
                System.Text.Encoding.UTF8.GetBytes(_serverName, 0, _serverName.Length, bs, 5)
            End If
            bs(0) = 5
            bs(1) = _command
            bs(2) = 0
            bs(bs.Length - 2) = CType((_port >> 8), Byte)
            bs(bs.Length - 1) = CType((_port And 255), Byte)
            MyBase.Socket.Send(bs)
        End Sub

        Private Sub BeginReadReply()
            Dim bs(4) As Byte
            MyBase.Socket.BeginReceive(bs, 0, 4, SocketFlags.None, New AsyncCallback(AddressOf Me.ReadReplyCallback), bs)
        End Sub

        Private Sub ReadReplyCallback(ByVal result As IAsyncResult)
            Try
                Dim bs1 As Byte() = CType(result.AsyncState, Byte())
                Dim i As Integer = MyBase.Socket.EndReceive(result)
                If i < 4 Then
                    ReceiveAll(bs1, i, 4 - i)
                End If
                If Not (bs1(0) = 5) Then
                    Throw New ProxySocketException("Socks5InvalidResponse", ProxySocketExceptionStatus.ServerProtocolViolation)
                End If
                Dim b As Byte = bs1(1)
                Select Case b
                    Case 1
                        Throw New ProxySocketException("Socks5GeneralFailure", ProxySocketExceptionStatus.ProtocolError)
                    Case 2
                        Throw New ProxySocketException("Socks5ConnectionNotAllowed", ProxySocketExceptionStatus.ProtocolError)
                    Case 3
                        Throw New ProxySocketException("Socks5NetworkUnreachable", ProxySocketExceptionStatus.ProtocolError)
                    Case 4
                        Throw New ProxySocketException("Socks5HostUnreachable", ProxySocketExceptionStatus.ProtocolError)
                    Case 5
                        Throw New ProxySocketException("Socks5ConnectionRefused", ProxySocketExceptionStatus.ProtocolError)
                    Case 6
                        Throw New ProxySocketException("Socks5TtlExpired", ProxySocketExceptionStatus.ProtocolError)
                    Case 7
                        Throw New ProxySocketException("Socks5CommandNotSupported", ProxySocketExceptionStatus.ProtocolError)
                    Case 8
                        Throw New ProxySocketException("Socks5AddressTypeNotSupported", ProxySocketExceptionStatus.ProtocolError)
                    Case 0
                        b = bs1(3)
                    Case Else
                        Throw New ProxySocketException("Socks5UnknownError", ProxySocketExceptionStatus.ProtocolError)
                End Select
                Select Case b
                    Case 3
                        Throw New ProxySocketException("Socks5DomainResponseInsteadIp", ProxySocketExceptionStatus.ProtocolError)
                    Case 4
                        Throw New ProxySocketException("Socks5NewIpResponse", ProxySocketExceptionStatus.ProtocolError)
                    Case 1
                        Dim bs2(6) As Byte
                        ReceiveAll(bs2, 0, 6)
                        Dim iPEndPoint As IPEndPoint = GetEndPoint(bs2, 0)
                        myAsyncResult.SetResult(iPEndPoint)
                    Case Else
                        Throw New ProxySocketException("Socks5UnknownAddressType", ProxySocketExceptionStatus.ProtocolError)
                End Select
            Catch e As Exception
                MyBase.SetError(e)
            Finally
                MyBase.Finish()
            End Try
        End Sub

        Private Function GetEndPoint(ByVal buffer As Byte(), ByVal offset As Integer) As IPEndPoint
            Dim i As Integer = (buffer(offset + 4) << 8) + buffer(offset + 5)
            Dim l As Long = CType((buffer(offset + 3) << 24), Long) + CType((buffer(offset + 2) << 16), Long) + CType((buffer(offset + 1) << 8), Long) + CType(buffer(offset), Long)
            If l <> 0 Then
                Return New IPEndPoint(l, i)
            Else
                Return New IPEndPoint(CType(MyBase.Socket.RemoteEndPoint, IPEndPoint).Address, i)
            End If
        End Function
    End Class
End Namespace
