Imports System
Imports System.Net
Imports System.Net.Sockets

Namespace Net.FTP
    Class ProxyNull
        Inherits ProxyBase

        Public Sub New(ByVal s As Socket)
            MyBase.New(s)
        End Sub

        Public Overloads Overrides Function BeginConnect(ByVal serverName As String, ByVal serverPort As Integer, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Begin("Connect", callback, state)
            Dns.BeginResolve(serverName, New AsyncCallback(AddressOf Me.Resolve), serverPort)
            Return myAsyncResult
        End Function

        Public Overloads Overrides Function BeginConnect(ByVal endPoint As IPEndPoint, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Begin("Connect", callback, state)
            Socket.BeginConnect(endPoint, New AsyncCallback(AddressOf Me.Connect), Nothing)
            Return myAsyncResult
        End Function

        Public Overloads Overrides Function BeginListen(ByVal controlSocket As ProxyBase, ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Begin("Listen", callback, state)
            Try
                Dim iPEndPoint As IPEndPoint = New IPEndPoint(CType(controlSocket.Socket.LocalEndPoint, IPEndPoint).Address, 0)
                Socket.Bind(iPEndPoint)
                Socket.Listen(0)
                myAsyncResult.SetResult(CType(Socket.LocalEndPoint, IPEndPoint))
            Catch e As Exception
                SetError(e)
            Finally
                Finish()
            End Try
            Return myAsyncResult
        End Function

        Public Overloads Overrides Function BeginAccept(ByVal callback As AsyncCallback, ByVal state As Object) As IAsyncResult
            Begin("Accept", callback, state)
            Try
                Socket.BeginAccept(New AsyncCallback(AddressOf Me.Accept), Nothing)
                Dim iAsyncResult As IAsyncResult = myAsyncResult
                Return iAsyncResult
            Catch e As Exception
                Throw CancelBegin(e)
            End Try
        End Function

        Private Sub Accept(ByVal result As IAsyncResult)
            Try
                Dim accSocket As Socket = Socket
                Dim resSocket As Socket = accSocket.EndAccept(result)
                SetSocket(resSocket)
                accSocket.Close()
            Catch ex As Exception
                SetError(New ProxySocketException(ex.Message, ProxySocketExceptionStatus.SocketError, ex))
            Finally
                Finish()
            End Try
        End Sub

        Private Sub Resolve(ByVal result As IAsyncResult)
            Try
                Dim hostEntry As IPHostEntry
                Try
                    hostEntry = Dns.EndResolve(result)
                Catch e1 As SocketException
                    SetError(New ProxySocketException("ResolveFailed", ProxySocketExceptionStatus.NameResolutionFailure, e1))
                    Finish()
                    Return
                End Try
                Dim endPoint As IPEndPoint = New IPEndPoint(hostEntry.AddressList(0), result.AsyncState)
                Socket.BeginConnect(endPoint, New AsyncCallback(AddressOf Me.Connect), Nothing)
            Catch e2 As Exception
                SetError(e2)
                Finish()
            End Try
        End Sub

        Private Sub Connect(ByVal asyncResult As IAsyncResult)
            Try
                Socket.EndConnect(asyncResult)
            Catch e As Exception
                SetError(New ProxySocketException(e.Message, ProxySocketExceptionStatus.ConnectFailure, e))
            Finally
                Finish()
            End Try
        End Sub
    End Class
End Namespace