Imports mermaid.BaseObjects.Net.Sockets
Imports System.IO
Imports System.Reflection

Namespace Net.Sockets

    ''' <summary>
    ''' Uses the dll "ICSharpCode.SharpZipLib.dll" to perform the compression tasks.
    ''' This dll needs to be in the program folder.
    ''' </summary>
    ''' <remarks>Build for "ICSharpCode.SharpZipLib.dll" version 0.85.4.369</remarks>
    Friend Class CompressionStreamHandler
        Implements mermaid.BaseObjects.Net.Sockets.Interfaces.IStreamHandler

#Region "Variables"

        Private _Assembly As Assembly
        Private _GZipOutputStreamType As Type
        Private _GZipInputStreamType As Type

        Private _BufferSize As Integer = 1024

#End Region

#Region "Properties"

        Public Property BufferSize() As Integer
            Get
                Return Me._BufferSize

            End Get
            Set(ByVal value As Integer)
                Me._BufferSize = value

            End Set
        End Property

        Private Shared ReadOnly Property SentBytesOptimizedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Compression saved bytes sent")

            End Get
        End Property

        Private Shared ReadOnly Property ReceivedBytesOptimizedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Compression saved bytes received")

            End Get
        End Property

        Private Shared ReadOnly Property CompressingTimePerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Compression time")

            End Get
        End Property

        Private Shared ReadOnly Property DecompressingTimePerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Decompression time")

            End Get
        End Property

#End Region

        Public Sub New()
            _Assembly = [Assembly].LoadFrom(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "ICSharpCode.SharpZipLib.dll"))
            _GZipOutputStreamType = _Assembly.GetType("ICSharpCode.SharpZipLib.GZip.GZipOutputStream")
            _GZipInputStreamType = _Assembly.GetType("ICSharpCode.SharpZipLib.GZip.GZipInputStream")

        End Sub

        Public Function SerializeStream(ByVal input As System.IO.MemoryStream) As System.IO.MemoryStream Implements Net.Sockets.Interfaces.IStreamHandler.SerializeStream
            Try
                Dim tmpCompressionStart As Integer = System.Environment.TickCount And Integer.MaxValue

                Dim tmpComressedSize As Long = 0
                Dim tmpOriginalSize As Long = input.Length

                Dim tmpWriteData() As Byte

                input.Seek(0, SeekOrigin.Begin)

                Dim tmpCompressedStream As New MemoryStream()
                'Dim tmpZipStream As New ICSharpCode.SharpZipLib.GZip.GZipOutputStream(tmpCompressedStream)
                Dim tmpZipStream As Object = Activator.CreateInstance(_GZipOutputStreamType, New Object() {tmpCompressedStream})

                tmpWriteData = input.ToArray()

                tmpZipStream.Write(tmpWriteData, 0, tmpWriteData.Length)
                tmpZipStream.Finish()

                tmpComressedSize = tmpCompressedStream.Length

                SentBytesOptimizedPerformanceCounter.Increment(tmpOriginalSize - tmpComressedSize)

                input.Dispose()

                Dim tmpCompressionEnd As Integer = System.Environment.TickCount And Integer.MaxValue

                CompressingTimePerformanceCounter.Increment(tmpCompressionEnd - tmpCompressionStart)

                tmpZipStream.Dispose()

                Return tmpCompressedStream

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return input

            End Try
        End Function

        Public Function DeSerializeStream(ByVal input As System.IO.MemoryStream) As System.IO.MemoryStream Implements Net.Sockets.Interfaces.IStreamHandler.DeSerializeStream
            Try
                Dim tmpDecompressionStart As Integer = System.Environment.TickCount And Integer.MaxValue

                Dim tmpComressedSize As Long = input.Length
                Dim tmpOriginalSize As Long = 0

                'Dim tmpZipStream As ICSharpCode.SharpZipLib.GZip.GZipInputStream = New ICSharpCode.SharpZipLib.GZip.GZipInputStream(input)
                Dim tmpZipStream As Object = Activator.CreateInstance(_GZipInputStreamType, New Object() {input})

                Dim tmpOutput As New System.IO.MemoryStream(input.Length)

                Dim tmpBuffer(Me.BufferSize) As Byte

                Dim tmpReadSize As Integer = tmpZipStream.Read(tmpBuffer, 0, tmpBuffer.Length)

                While tmpReadSize > 0
                    tmpOutput.Write(tmpBuffer, 0, tmpReadSize)
                    tmpReadSize = tmpZipStream.Read(tmpBuffer, 0, tmpBuffer.Length)

                End While

                tmpOutput.Position = 0

                tmpOriginalSize = tmpOutput.Length

                ReceivedBytesOptimizedPerformanceCounter.Increment(tmpOriginalSize - tmpComressedSize)

                Dim tmpDecompressionEnd As Integer = System.Environment.TickCount And Integer.MaxValue

                DecompressingTimePerformanceCounter.Increment(tmpDecompressionEnd - tmpDecompressionStart)

                Return tmpOutput

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return input

            End Try
        End Function

    End Class

End Namespace