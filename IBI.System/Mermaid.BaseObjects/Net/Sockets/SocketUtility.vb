Imports mermaid.BaseObjects.Net.Sockets.Interfaces
Imports System.Threading

Namespace Net.Sockets

    Public Class SocketUtility

#Region "Variables"

        Private Shared _EOSBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$EOS$>")
        Private Shared _EOIBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$EOI$>")
        Private Shared _PingBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$PING$>")
        Private Shared _DiagBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$DIAG$>")
        Private Shared _ShutdownBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$DISPOSE$>")
        Private Shared _CompressedContentBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$ZIP$>")
        Private Shared _SimpleSerializationBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$S$>")
        Private Shared _ValueSplitBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$>")

        'Private Shared _ReceiveThreadSize As Long = 512L * KB
        Private Shared _BufferSize As Integer = 16 * KB

        Private Shared _SocketPerformanceLog As New Diagnostics.PerformanceLog("mermaid BaseObjects\Net\Sockets")

        <ThreadStatic()> Private Shared _CurrentSession As CommunicatorSession
        <ThreadStatic()> Private Shared _CurrentCallData As CommunicatorDataObject

#End Region

#Region "Properties"

        ''' <summary>
        ''' EndOfStream byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property EOSBytes() As Byte()
            Get
                Return _EOSBytes

            End Get
        End Property

        ''' <summary>
        ''' EndOfIDentifier byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property EOIBytes() As Byte()
            Get
                Return _EOIBytes

            End Get
        End Property

        ''' <summary>
        ''' Ping byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property PingBytes() As Byte()
            Get
                Return _PingBytes

            End Get
        End Property

        ''' <summary>
        ''' Diag byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property DiagBytes() As Byte()
            Get
                Return _DiagBytes

            End Get
        End Property

        ''' <summary>
        ''' Shutdown byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property ShutdownBytes() As Byte()
            Get
                Return _ShutdownBytes

            End Get
        End Property

        ''' <summary>
        ''' CompressedContent byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property CompressedContentBytes() As Byte()
            Get
                Return _CompressedContentBytes

            End Get
        End Property

        ''' <summary>
        ''' SimpleSerialization byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property SimpleSerializationBytes() As Byte()
            Get
                Return _SimpleSerializationBytes

            End Get
        End Property

        ''' <summary>
        ''' ValueSplit byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property ValueSplitBytes() As Byte()
            Get
                Return _ValueSplitBytes

            End Get
        End Property

        '''' <summary>
        '''' Size of the thread used to receive data.
        '''' </summary>
        'Public Shared Property ReceiveThreadSize() As Long
        '    Get
        '        Return _ReceiveThreadSize

        '    End Get
        '    Set(ByVal value As Long)
        '        _ReceiveThreadSize = value

        '    End Set
        'End Property

        ''' <summary>
        ''' Size of the buffer used to receive data.
        ''' </summary>
        Public Shared Property BufferSize() As Integer
            Get
                Return _BufferSize

            End Get
            Set(ByVal value As Integer)
                _BufferSize = value

            End Set
        End Property

        Public Shared ReadOnly Property SocketPerformanceLog() As Diagnostics.PerformanceLog
            Get
                Return _SocketPerformanceLog

            End Get
        End Property

#End Region

#Region "Constants"

        Friend Const IDENTIFIER_LENGTH As Integer = 36

        Public Const B As Long = 1L
        Public Const KB As Long = 1024L * B
        Public Const MB As Long = 1024L * KB

        Friend Const THREADDATASLOTNAME_CURRENTSESSION As String = "Net.Sockets.Communicator_CurrentSession"
        Friend Const THREADDATASLOTNAME_CURRENTCALLDATA As String = "Net.Sockets.Communicator_CurrentCallData"

#End Region

        Friend Shared Function ExtractIDentifier(ByVal buffer() As Byte) As String
            Try
                Dim tmpEndIndex As Long = IndexOfEOI(buffer)

                If tmpEndIndex <> -1 Then
                    Dim tmpBuffer(tmpEndIndex - 1) As Byte

                    Array.Copy(buffer, tmpBuffer, tmpEndIndex)

                    Return System.Text.Encoding.UTF8.GetString(tmpBuffer)

                End If

                Return String.Empty

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return String.Empty

            End Try
        End Function

        Public Shared Function IndexOfEOI(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - EOIBytes.Length
                    For y As Integer = 0 To EOIBytes.Length - 1
                        If buffer(i + y) = EOIBytes(y) Then
                            If y = EOIBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfEOS(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - SocketUtility.EOSBytes.Length
                    For y As Integer = 0 To SocketUtility.EOSBytes.Length - 1
                        If buffer(i + y) = SocketUtility.EOSBytes(y) Then
                            If y = SocketUtility.EOSBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfPing(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - SocketUtility.PingBytes.Length
                    For y As Integer = 0 To SocketUtility.PingBytes.Length - 1
                        If buffer(i + y) = SocketUtility.PingBytes(y) Then
                            If y = SocketUtility.PingBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfDiag(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - SocketUtility.DiagBytes.Length
                    For y As Integer = 0 To SocketUtility.DiagBytes.Length - 1
                        If buffer(i + y) = SocketUtility.DiagBytes(y) Then
                            If y = SocketUtility.DiagBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfShutdown(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - SocketUtility.ShutdownBytes.Length
                    For y As Integer = 0 To SocketUtility.ShutdownBytes.Length - 1
                        If buffer(i + y) = SocketUtility.ShutdownBytes(y) Then
                            If y = SocketUtility.ShutdownBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IsCompressedStream(ByVal buffer() As Byte) As Boolean
            If Not buffer Is Nothing Then
                If buffer.Length >= CompressedContentBytes.Length Then
                    For y As Integer = 0 To SocketUtility.CompressedContentBytes.Length - 1
                        If buffer(y) = SocketUtility.CompressedContentBytes(y) Then
                            If y = SocketUtility.CompressedContentBytes.Length - 1 Then
                                Return True

                            End If
                        Else
                            Exit For

                        End If
                    Next
                End If
            End If

            Return False

        End Function

        Public Shared Function IsSimpleSerialized(ByVal buffer() As Byte) As Boolean
            If Not buffer Is Nothing Then
                If buffer.Length >= SimpleSerializationBytes.Length Then
                    For y As Integer = 0 To SocketUtility.SimpleSerializationBytes.Length - 1
                        If buffer(y) = SocketUtility.SimpleSerializationBytes(y) Then
                            If y = SocketUtility.SimpleSerializationBytes.Length - 1 Then
                                Return True

                            End If
                        Else
                            Exit For

                        End If
                    Next
                End If
            End If

            Return False

        End Function

        Friend Shared Sub SetCurrentSession(ByVal session As CommunicatorSession)
            Try
                'Thread.SetData(Thread.GetNamedDataSlot(THREADDATASLOTNAME_CURRENTSESSION), session)
                _CurrentSession = session

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Public Shared Function FetchCurrentSession() As CommunicatorSession
            Try
                'Return Thread.GetData(Thread.GetNamedDataSlot(THREADDATASLOTNAME_CURRENTSESSION))
                Return _CurrentSession

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return Nothing

            End Try
        End Function

        Friend Shared Sub SetCurrentCallData(ByVal data As CommunicatorDataObject)
            Try
                'Thread.SetData(Thread.GetNamedDataSlot(THREADDATASLOTNAME_CURRENTCALLDATA), data)
                _CurrentCallData = data

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Public Shared Function FetchCurrentCallData() As CommunicatorDataObject
            Try
                'Return Thread.GetData(Thread.GetNamedDataSlot(THREADDATASLOTNAME_CURRENTCALLDATA))
                Return _CurrentCallData

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return Nothing

            End Try
        End Function

        Public Shared Function ReplyCurrentCaller(ByVal returnData As Object) As Boolean
            Try
                Dim tmpCallData As CommunicatorDataObject = SocketUtility.FetchCurrentCallData

                If Not tmpCallData Is Nothing Then
                    If Not tmpCallData.Session Is Nothing Then
                        If Not tmpCallData.Session.Communicator Is Nothing Then
                            Dim tmpReply As New CommunicatorDataObject
                            tmpReply.Tag = tmpCallData.Tag & "_REPLY"
                            tmpReply.Data = returnData

                            tmpCallData.Session.Communicator.SendReply(tmpCallData, tmpReply)

                            Return True
                        End If
                    Else
                        'Throw New Exception("No session associated with CallData """ & tmpCallData.Tag & """.")
                        SocketPerformanceLog.WriteEntry("SocketUtility.ReplyCurrentCaller()" & vbCrLf & vbTab & "No session associated with CallData """ & tmpCallData.Tag & """.")

                    End If
                Else
                    'Throw New Exception("Cannot find CallData.")
                    SocketPerformanceLog.WriteEntry("SocketUtility.ReplyCurrentCaller()" & vbCrLf & vbTab & "Cannot find CallData.")

                End If

                Return False

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Return False

            End Try
        End Function

        Public Shared Sub BeginReplyCurrentCaller(ByVal returnData As Object)
            Dim tmpCallData As CommunicatorDataObject = SocketUtility.FetchCurrentCallData

            If Not tmpCallData Is Nothing Then
                If Not tmpCallData.Session Is Nothing Then
                    If Not tmpCallData.Session.Communicator Is Nothing Then
                        Dim tmpThreadData As New ReplyCurrentCallerThreadData(tmpCallData, returnData)

                        Threading.AsyncHelper.BeginWork(AddressOf ReplyCurrentCaller_Work, AddressOf ReplyCurrentCaller_End, tmpThreadData)

                    End If
                Else
                    'Throw New Exception("No session associated with CallData """ & tmpCallData.Tag & """.")

                End If
            Else
                'Throw New Exception("Cannot find CallData.")

            End If

        End Sub

        Private Class ReplyCurrentCallerThreadData

#Region "Variables"

            Private _CallData As CommunicatorDataObject
            Private _ReturnData As Object

#End Region

#Region "Properties"

            Public Property CallData() As CommunicatorDataObject
                Get
                    Return Me._CallData

                End Get
                Set(ByVal value As CommunicatorDataObject)
                    Me._CallData = value

                End Set
            End Property

            Public Property ReturnData() As Object
                Get
                    Return Me._ReturnData

                End Get
                Set(ByVal value As Object)
                    Me._ReturnData = value

                End Set
            End Property

#End Region

            Public Sub New(ByVal callData As CommunicatorDataObject, ByVal returnData As Object)
                MyBase.New()

                Me.CallData = callData
                Me.ReturnData = returnData

            End Sub

        End Class

        Private Shared Function ReplyCurrentCaller_Work(ByVal ar As Threading.AsyncResult) As Object
            Dim tmpThreadData As ReplyCurrentCallerThreadData = ar.AsyncState

            Dim tmpReply As New CommunicatorDataObject
            tmpReply.Tag = tmpThreadData.CallData.Tag & "_REPLY"
            tmpReply.Data = tmpThreadData.ReturnData

            tmpThreadData.CallData.Session.Communicator.SendReply(tmpThreadData.CallData, tmpReply)

            Return Nothing

        End Function

        Private Shared Function ReplyCurrentCaller_End(ByVal ar As Threading.AsyncResult) As Object
            Threading.AsyncHelper.EndWork(ar)

            Return Nothing

        End Function

    End Class

End Namespace