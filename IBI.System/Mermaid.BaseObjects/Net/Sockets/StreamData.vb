﻿Namespace Net.Sockets

    Friend Class StreamData
        Implements IDisposable

#Region "Variables"

        Private _WasCompressed As Boolean
        Private _Stream As System.IO.MemoryStream

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public Property WasCompressed() As Boolean
            Get
                Return Me._WasCompressed

            End Get
            Set(ByVal value As Boolean)
                Me._WasCompressed = value

            End Set
        End Property

        Public Property Stream() As System.IO.MemoryStream
            Get
                Return Me._Stream

            End Get
            Set(ByVal value As System.IO.MemoryStream)
                Me._Stream = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal wasCompressed As Boolean, ByVal stream As System.IO.MemoryStream)
            MyBase.New()

            Me.WasCompressed = wasCompressed
            Me.Stream = stream

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    '  free other state (managed objects).

                End If

                Me.Stream = Nothing

            End If

            Me.IsDisposed = True

        End Sub

    End Class

End Namespace