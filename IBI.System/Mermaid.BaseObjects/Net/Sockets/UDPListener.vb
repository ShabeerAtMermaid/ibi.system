﻿Imports System.Threading
Imports System.Net.Sockets
Imports System.Net

Namespace Net.Sockets

    Public Class UDPListener

#Region "Variables"

        Private _Port As Integer
        Private _Socket As Socket
        Private _GroupEP As EndPoint
        Private _ReceiveBuffer As String

        Private _PackageSplitter As String = DEFAULT_PACKAGESPLITTER
        Private _Enabled As Boolean = True

#End Region

#Region "Properties"

        Public ReadOnly Property Port As Integer
            Get
                Return _Port

            End Get
        End Property

        Public ReadOnly Property PackageSplitter As String
            Get
                Return _PackageSplitter

            End Get
        End Property

#End Region

#Region "Events"

        Public Event PackageReceived(ByVal sender As Object, ByVal data As String)

        Private Sub OnPackageReceived(ByVal data As String)
            Try
                RaiseEvent PackageReceived(Me, data)

            Catch ex As Exception
                'SILENT

            End Try
        End Sub

#End Region

#Region "Constants"

        Private Const DEFAULT_PACKAGESPLITTER As String = vbCrLf

#End Region

        Public Sub New(ByVal port As Integer)
            Me.New(port, DEFAULT_PACKAGESPLITTER)

        End Sub

        Public Sub New(ByVal port As Integer, ByVal packageSplitter As String)
            _Port = port
            _PackageSplitter = packageSplitter

            ConfigureSocket()

        End Sub

        Private Sub ConfigureSocket()
            _Socket = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
            _Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, True)

            _GroupEP = New IPEndPoint(IPAddress.Any, _Port)

            _Socket.Bind(_GroupEP)

            'THREAD: ThreadManager
            'Dim ListenThread As Thread
            'ListenThread = New Thread(AddressOf BeginListen)

            Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("UDPListener listen thread", AddressOf BeginListen)
            tmpThread.Start()

        End Sub

        Private Sub BeginListen()

            Dim tmpBuffer(1024) As Byte

            Dim tmpReceivedBytes As Integer = 0

            While _Enabled
                Try
                    tmpReceivedBytes = _Socket.ReceiveFrom(tmpBuffer, _GroupEP)

                    If tmpReceivedBytes > 0 Then
                        _ReceiveBuffer &= System.Text.Encoding.UTF8.GetString(tmpBuffer, 0, tmpReceivedBytes)

                        Dim tmpResult As String = Me.GetString

                        While Not String.IsNullOrEmpty(tmpResult)
                            OnPackageReceived(tmpResult)

                            tmpResult = Me.GetString

                        End While

                    End If

                Catch ex As Exception
                    'SILENT

                End Try

            End While

            _Socket.Close()


        End Sub

        Private Function GetString() As String
            If Not String.IsNullOrEmpty(_ReceiveBuffer) Then
                Dim tmpFirstIndex As Integer = _ReceiveBuffer.IndexOf(PackageSplitter)

                If tmpFirstIndex <> -1 Then
                    Dim tmpResult As String = _ReceiveBuffer.Substring(0, tmpFirstIndex)
                    _ReceiveBuffer = _ReceiveBuffer.Substring(tmpFirstIndex + PackageSplitter.Length)

                    Return tmpResult

                End If
            End If

            Return String.Empty

        End Function

        Public Sub Dispose()
            _Enabled = False

            Try
                _Socket.Close()
                _Socket = Nothing

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

    End Class

End Namespace