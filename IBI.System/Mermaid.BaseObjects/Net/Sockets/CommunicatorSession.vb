Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Namespace Net.Sockets

    Public Class CommunicatorSession

#Region "Variables"

        Private _Identifier As String
        Friend _SocketInfo As IPEndPoint
        Private Shared _Timeout As TimeSpan = TimeSpan.FromMinutes(5)
        Private WithEvents _Communicator As Communicator
        Private WithEvents _TmrTimeout As Timers.Timer
        Private _LastActivity As Date
        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public Shared Property Timeout() As TimeSpan
            Get
                Return _Timeout

            End Get
            Set(ByVal value As TimeSpan)
                _Timeout = value

            End Set
        End Property

        Public ReadOnly Property Identifier() As String
            Get
                Return Me._IDentifier

            End Get
        End Property

        Public ReadOnly Property SocketInfo() As IPEndPoint
            Get
                Return Me._SocketInfo

            End Get
        End Property

        Public ReadOnly Property Communicator() As Communicator
            Get
                Return Me._Communicator

            End Get
        End Property

        Private Property TmrTimeout() As Timers.Timer
            Get
                Return Me._TmrTimeout

            End Get
            Set(ByVal value As Timers.Timer)
                Me._TmrTimeout = value

            End Set
        End Property

        Friend Property LastActivity() As Date
            Get
                Return Me._LastActivity

            End Get
            Set(ByVal value As Date)
                Me._LastActivity = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

        Private Shared ReadOnly Property SessionsCreatedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicatorSessions created")

            End Get
        End Property

        Private Shared ReadOnly Property ActiveSessionsPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicatorSessions active")

            End Get
        End Property

#End Region

#Region "Events"

        Public Event Disposing As System.EventHandler

#End Region

        Friend Sub New(ByVal identifier As String, ByVal socket As Socket, ByVal streamHandler As Interfaces.IStreamHandler)
            MyBase.New()

            SessionsCreatedPerformanceCounter.Increment()
            ActiveSessionsPerformanceCounter.Increment()

            Me._Identifier = identifier
            Me._SocketInfo = socket.RemoteEndPoint

            Me._Communicator = New Communicator(identifier, socket)
            Me._Communicator.Session = Me
            Me._Communicator.StreamHandler = streamHandler

        End Sub

        Friend Sub Initialize()
            Me._Communicator.Initialize()

            Me.TmrTimeout = New Timers.Timer
            Me.TmrTimeout.Interval = TimeSpan.FromMinutes(1).TotalMilliseconds
            Me.TmrTimeout.Enabled = True

            'If Not Communicator.DiagBroadcaster Is Nothing Then
            '    Communicator.DiagBroadcaster.Write(Me.Communicator.GetDebugString)

            'End If
            If Not Communicator.DiagLogWriter Is Nothing Then
                Communicator.DiagLogWriter.UpdateSessionInfo(Identifier, Me.Communicator.GetDebugString)

            End If

        End Sub

        Private Sub Communicator_BeforeSend(ByVal sender As Object, ByVal e As EventArgs.CommunicatorBeforeSendEventArgs) Handles _Communicator.BeforeSend
            Me.LastActivity = Date.Now

        End Sub

        Private Sub Communicator_CallReceived(ByVal sender As Object, ByVal e As EventArgs.CommunicatorCallReceivedEventArgs) Handles _Communicator.CallReceived
            Me.LastActivity = Date.Now

            e.Data._Session = Me

        End Sub

        Private Sub Communicator_Disposing(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Communicator.Disposing
            Me.Dispose()

        End Sub

        Private Sub TmrTimeout_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrTimeout.Elapsed
            If CommunicatorSession.Timeout > TimeSpan.FromSeconds(0) Then
                If Date.Now.Subtract(Me.LastActivity) > CommunicatorSession.Timeout Then
                    Me.Dispose()

                End If
            End If

        End Sub

        Public Sub Dispose()
            If Not Me.IsDisposed Then
                Me.IsDisposed = True

                ActiveSessionsPerformanceCounter.Decrement()

                'If Not Communicator.DiagBroadcaster Is Nothing Then
                '    Communicator.DiagBroadcaster.Write("<sessiondead>" & Me.IDentifier & "<@>")

                'End If
                If Not Communicator.DiagLogWriter Is Nothing Then
                    Communicator.DiagLogWriter.UpdateSessionInfo(Me.Identifier, Me.Communicator.GetDebugString)

                End If

                Me.TmrTimeout.Enabled = False
                Me.TmrTimeout.Dispose()
                Me.TmrTimeout = Nothing

                Me.OnDisposing(New System.EventArgs)

                If Not Me._Communicator Is Nothing Then
                    Me._Communicator.Dispose()

                End If

                Me._Communicator = Nothing

            End If

        End Sub

        Private Sub OnDisposing(ByVal e As System.EventArgs)
            RaiseEvent Disposing(Me, e)

        End Sub

    End Class

End Namespace