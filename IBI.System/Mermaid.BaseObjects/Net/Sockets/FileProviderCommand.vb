Namespace Net.Sockets

    <System.Serializable()> Friend Class FileProviderCommand

#Region "Variables"

        Private _FileProviderURI As String
        Private _Command As Commands
        Private _FileChunkRequest As FileChunkRequest

#End Region

#Region "Properties"

        Public Property FileProviderURI() As String
            Get
                Return Me._FileProviderURI

            End Get
            Set(ByVal value As String)
                Me._FileProviderURI = value

            End Set
        End Property

        Public Property Command() As Commands
            Get
                Return Me._Command

            End Get
            Set(ByVal value As Commands)
                Me._Command = value

            End Set
        End Property

        Public Property FileChunkRequest() As FileChunkRequest
            Get
                Return Me._FileChunkRequest

            End Get
            Set(ByVal value As FileChunkRequest)
                Me._FileChunkRequest = value

            End Set
        End Property

#End Region

#Region "Enums"

        Public Enum Commands As Integer
            GetNextPortion = 1
            DownloadComplete = 2
            Abort = 3

        End Enum

#End Region

        Public Sub New(ByVal fileProviderURI As String, ByVal command As Commands)
            MyBase.New()

            Me.FileProviderURI = fileProviderURI
            Me.Command = command

        End Sub

    End Class

End Namespace