Namespace Net.Sockets

    <Serializable()> _
    Public Class CommunicatorDataObject

#Region "Variables"

        Private _SerializationMethod As SerializationMethods = SerializationMethods.Default
        Private _InvocationID As String
        Friend _IsReply As Boolean
        <NonSerialized()> Friend _Session As CommunicatorSession
        <NonSerialized()> Private _Compress As Nullable(Of Boolean) = Nothing
        Private _Sent As Date
        <NonSerialized()> Private _Received As Date
        <NonSerialized()> Private _ReplyProcessingTime As TimeSpan
        Private _Tag As String
        Private _Data As Object

#End Region

#Region "Properties"

        Public Property SerializationMethod() As SerializationMethods
            Get
                Return Me._SerializationMethod

            End Get
            Set(ByVal value As SerializationMethods)
                Me._SerializationMethod = value

            End Set
        End Property

        Friend Property InvocationID() As String
            Get
                Return Me._InvocationID

            End Get
            Set(ByVal value As String)
                Me._InvocationID = value

            End Set
        End Property

        Public ReadOnly Property IsReply() As Boolean
            Get
                Return Me._IsReply

            End Get
        End Property

        Public ReadOnly Property Session() As CommunicatorSession
            Get
                Return Me._Session

            End Get
        End Property

        Public Property Compress() As Nullable(Of Boolean)
            Get
                Return Me._Compress

            End Get
            Set(ByVal value As Nullable(Of Boolean))
                Me._Compress = value

            End Set
        End Property

        Friend Property Sent() As Date
            Get
                Return Me._Sent

            End Get
            Set(ByVal value As Date)
                Me._Sent = value

            End Set
        End Property

        Public ReadOnly Property Received() As Date
            Get
                Return Me._Received

            End Get
        End Property

        Friend Property ReplyProcessingTime() As TimeSpan
            Get
                Return Me._ReplyProcessingTime

            End Get
            Set(ByVal value As TimeSpan)
                Me._ReplyProcessingTime = value

            End Set
        End Property

        Public Property Tag() As String
            Get
                Return Me._Tag

            End Get
            Set(ByVal value As String)
                Me._Tag = value

            End Set
        End Property

        Public Property Data() As Object
            Get
                Return Me._Data

            End Get
            Set(ByVal value As Object)
                Me._Data = value

            End Set
        End Property

#End Region

#Region "Enums"

        Public Enum SerializationMethods As Integer
            [Default] = 0
            Simple = 1

        End Enum

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Friend Sub SetReceived()
            Me._Received = Date.Now

        End Sub

        Friend Sub SetReceived(ByVal stamp As Date)
            Me._Received = stamp

        End Sub

        Friend Function ResolveSerializationMethod() As SerializationMethods
            Select Case Me.SerializationMethod
                Case SerializationMethods.Default
                    Return SerializationMethods.Default

                Case SerializationMethods.Simple
                    If Me.Data Is Nothing Then
                        Return SerializationMethods.Simple

                    Else
                        If TypeOf Me.Data Is String Then
                            Return SerializationMethods.Simple

                        ElseIf TypeOf Me.Data Is Integer Then
                            Return SerializationMethods.Simple

                        ElseIf TypeOf Me.Data Is Double Then
                            Return SerializationMethods.Simple

                        ElseIf TypeOf Me.Data Is Date Then
                            Return SerializationMethods.Simple

                        End If

                        Return SerializationMethods.Default

                    End If

            End Select

        End Function

        Friend Function Serialize() As System.IO.MemoryStream
            Dim tmpStream As New System.IO.MemoryStream

            Select Case Me.ResolveSerializationMethod
                Case SerializationMethods.Default
                    Dim tmpFormatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                    tmpFormatter.AssemblyFormat = Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple

                    tmpFormatter.Serialize(tmpStream, Me)

                Case SerializationMethods.Simple
                    Dim tmpIsReply As Integer = 0
                    If Me.IsReply Then tmpIsReply = 1

                    Dim tmpData As String

                    If TypeOf Me.Data Is Date Then
                        tmpData = CType(Me.Data, Date).ToString

                        'ElseIf Array.IndexOf(Me.Data.GetType.GetInterfaces, GetType(ISimpleSerializable)) <> -1 Then
                        '    tmpData = CType(Me.Data, ISimpleSerializable).Serialize

                    Else
                        tmpData = Me.Data

                    End If

                    Dim tmpSimpleSerializationString As String = System.Text.Encoding.UTF8.GetString(SocketUtility.SimpleSerializationBytes)
                    Dim tmpValueSplitter As String = System.Text.Encoding.UTF8.GetString(SocketUtility.ValueSplitBytes)

                    Dim tmpBuffer() As Byte = System.Text.Encoding.UTF8.GetBytes(tmpSimpleSerializationString & Me.InvocationID & tmpValueSplitter & tmpIsReply & tmpValueSplitter & Me.Sent.ToString & tmpValueSplitter & Me.Tag & tmpValueSplitter & tmpData)

                    tmpStream.SetLength(tmpBuffer.Length)
                    tmpStream.Write(tmpBuffer, 0, tmpBuffer.Length)

            End Select

            Return tmpStream

        End Function

        Friend Shared Function Deserialize(ByVal stream As System.IO.MemoryStream) As CommunicatorDataObject
            Dim tmpBuffer() As Byte = stream.ToArray

            If Not SocketUtility.IsSimpleSerialized(tmpBuffer) Then
                Array.Clear(tmpBuffer, 0, tmpBuffer.Length)
                Array.Resize(Of Byte)(tmpBuffer, 0)

                Dim tmpFormatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                tmpFormatter.AssemblyFormat = Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple

                Return tmpFormatter.Deserialize(stream)

            Else
                Dim tmpSerializedString As String = "N/A"

                Try
                    tmpSerializedString = System.Text.Encoding.UTF8.GetString(tmpBuffer)

                    Array.Clear(tmpBuffer, 0, tmpBuffer.Length)
                    Array.Resize(Of Byte)(tmpBuffer, 0)

                    Dim tmpValues() As String = tmpSerializedString.Split(New String() {System.Text.Encoding.UTF8.GetString(SocketUtility.ValueSplitBytes)}, StringSplitOptions.None)

                    If tmpValues.Length = 5 Then
                        Dim tmpData As New CommunicatorDataObject
                        tmpData.InvocationID = tmpValues(0).Replace(System.Text.Encoding.UTF8.GetString(SocketUtility.SimpleSerializationBytes), String.Empty)
                        tmpData._IsReply = tmpValues(1)
                        tmpData.Sent = Converter.ToDateTime(tmpValues(2))
                        tmpData.Tag = tmpValues(3)
                        tmpData.Data = tmpValues(4)

                        Return tmpData

                    Else
                        Throw New FormatException("Token Count is invalid")

                    End If

                Catch ex As Exception
                    Throw New Exception("Error deserializing DataObject." & Environment.NewLine & _
                                                        "SerializationMethod: Simple" & Environment.NewLine & _
                                                        "Serialization Data: " & tmpSerializedString, ex)

                End Try

            End If

            Return Nothing

        End Function

        Public Function GetInvocationID() As String
            Return Me.InvocationID

        End Function

    End Class

End Namespace