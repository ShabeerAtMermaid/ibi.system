Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Namespace Net.Sockets

    Public Class CommunicationServer
        Implements IDisposable

#Region "Variables"

        Private _Communicator As Communicator
        'Private _Socket As Socket
        Private _Sessions As Hashtable
        Private _SocketAccepters As ArrayList
        Private _UncompatibleClients As ArrayList
        'Private _Port As Integer
        Private _StreamHandler As Interfaces.IStreamHandler
        Private _MaxConnectionQueue As Integer = 500
        Private _IsStarted As Boolean
        Private _IsDisposed As Boolean

        Private _Lock As New Object

        Private _ConnectionsAcceptedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ConnectionsDeniedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ConnectionsEstablishedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ConnectionsReestablishedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ConnectionErrorsPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter

#End Region

#Region "Properties"

        Public ReadOnly Property Communicator() As Communicator
            Get
                Return Me._Communicator

            End Get
        End Property

        'Private Property Socket() As Socket
        '    Get
        '        Return Me._Socket

        '    End Get
        '    Set(ByVal value As Socket)
        '        Me._Socket = value

        '    End Set
        'End Property

        Private Property Sockets() As Dictionary(Of Integer, Socket) = New Dictionary(Of Integer, Socket)()

        Public ReadOnly Property Sessions() As Hashtable
            Get
                Return Me._Sessions

            End Get
        End Property

        Private ReadOnly Property SocketAccepters() As ArrayList
            Get
                If Me._SocketAccepters Is Nothing Then
                    Me._SocketAccepters = New ArrayList

                End If

                Return Me._SocketAccepters

            End Get
        End Property

        Public ReadOnly Property UncompatibleClients() As ArrayList
            Get
                If Me._UncompatibleClients Is Nothing Then
                    Me._UncompatibleClients = New ArrayList

                End If

                Return Me._UncompatibleClients

            End Get
        End Property

        'Public ReadOnly Property Port() As Integer
        '    Get
        '        Return Me._Port

        '    End Get
        'End Property

        Public Property StreamHandler() As Interfaces.IStreamHandler
            Get
                Return Me._StreamHandler

            End Get
            Set(ByVal value As Interfaces.IStreamHandler)
                If IsDirty(Me._StreamHandler, value) Then
                    Me._StreamHandler = value

                    If Not Me._Sessions Is Nothing Then
                        For Each tmpSession As CommunicatorSession In Me._Sessions.Values
                            tmpSession.Communicator.StreamHandler = value

                        Next

                    End If
                End If
            End Set
        End Property

        Public Property MaxConnectionQueue() As Integer
            Get
                Return Me._MaxConnectionQueue

            End Get
            Set(ByVal value As Integer)
                Me._MaxConnectionQueue = value

            End Set
        End Property

        Public Property CompressionEnabled() As Boolean
            Get
                Return Me.Communicator.CompressionEnabled

            End Get
            Set(ByVal value As Boolean)
                Me.Communicator.CompressionEnabled = value

            End Set
        End Property

        Public ReadOnly Property IsStarted() As Boolean
            Get
                Return Me._IsStarted

            End Get
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

        Private Property Lock() As Object
            Get
                Return Me._Lock

            End Get
            Set(ByVal value As Object)
                Me._Lock = value

            End Set
        End Property

        Private Property ConnectionsAcceptedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectionsAcceptedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectionsAcceptedPerformanceCounter = value

            End Set
        End Property

        Private Property ConnectionsDeniedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectionsDeniedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectionsDeniedPerformanceCounter = value

            End Set
        End Property

        Private Property ConnectionsEstablishedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectionsEstablishedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectionsEstablishedPerformanceCounter = value

            End Set
        End Property

        Private Property ConnectionsReestablishedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectionsReestablishedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectionsReestablishedPerformanceCounter = value

            End Set
        End Property

        Private Property ConnectionErrorsPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectionErrorsPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectionErrorsPerformanceCounter = value

            End Set
        End Property

#End Region

#Region "Events"

        Public Event ServerStarted As System.EventHandler(Of EventArgs.CommunicationServerStartedEventArgs)
        Public Event SessionCreated As System.EventHandler(Of EventArgs.CommunicatorSessionCreatedEventArgs)
        Public Event SessionDisposed As System.EventHandler(Of EventArgs.CommunicatorSessionDisposedEventArgs)
        Public Event ConnectionEstablished As System.EventHandler(Of EventArgs.CommunicationServerConnectionEstablishedEventArgs)
        Public Event ConnectionReestablished As System.EventHandler(Of EventArgs.CommunicationServerConnectionReestablishedEventArgs)

#End Region

        Public Sub New()
            Me._Sessions = New Hashtable

            Me.InitializePerformanceCounters()

            Me._Communicator = New Communicator(Me)

        End Sub

        Private Sub InitializePerformanceCounters()
            Try
                Me.ConnectionsAcceptedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicationServer connections accepted")
                Me.ConnectionsDeniedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicationServer connections denied")
                Me.ConnectionsEstablishedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicationServer connections established")
                Me.ConnectionsReestablishedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicationServer connections reestablished")
                Me.ConnectionErrorsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("CommunicationServer connection errors")

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Public Sub Start(ByVal port As Integer)
            Try
                Me.Start(IPAddress.Any, port)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Public Sub Start(ByVal ip As IPAddress, ByVal port As Integer)
            Try
                'Me._Port = port

                Dim tmpEndpoint As New IPEndPoint(ip, port)

                Dim tmpSocket As New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                tmpSocket.Bind(tmpEndpoint)
                tmpSocket.Listen(Me.MaxConnectionQueue)
                tmpSocket.BeginAccept(AddressOf Socket_OnAccept, tmpSocket)

                Me.Sockets.Add(port, tmpSocket)

                Me._IsStarted = True

                Me.OnServerStarted(New EventArgs.CommunicationServerStartedEventArgs(port))

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Private Sub Socket_OnAccept(ByVal ar As System.IAsyncResult)
            Try
                'Dim tmpClient As Socket = Me.Socket.EndAccept(ar)
                Dim tmpClient As Socket = CType(ar.AsyncState, Socket).EndAccept(ar)

                'Me.Socket.BeginAccept(AddressOf Socket_OnAccept, Nothing)
                CType(ar.AsyncState, Socket).BeginAccept(AddressOf Socket_OnAccept, ar.AsyncState)

                Dim tmpAccepter As SocketAccepter = New SocketAccepter(tmpClient)

                AddHandler tmpAccepter.Accepted, AddressOf SocketAccepter_Accepted
                AddHandler tmpAccepter.SyntaxError, AddressOf SocketAccepter_SyntaxError
                AddHandler tmpAccepter.Timeout, AddressOf SocketAccepter_Timeout

                Me.SocketAccepters.Add(tmpAccepter)

                tmpAccepter.Accept()

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                'Me.Socket.BeginAccept(AddressOf Socket_OnAccept, Nothing)
                CType(ar.AsyncState, Socket).BeginAccept(AddressOf Socket_OnAccept, ar.AsyncState)

            End Try
        End Sub

#Region "Session Handling"

        Private Sub CommunicatorSession_CallReceived(ByVal sender As Object, ByVal e As EventArgs.CommunicatorCallReceivedEventArgs)
            Me.Communicator.OnCallReceived(e)

        End Sub

        Private Sub CommunicatorSession_BeforeSend(ByVal sender As Object, ByVal e As EventArgs.CommunicatorBeforeSendEventArgs)
            Me.Communicator.OnBeforeSend(sender, e)

        End Sub

        Private Sub CommunicatorSession_Disposing(ByVal sender As Object, ByVal e As System.EventArgs)
            If Not Me.IsDisposed Then
                Me.Sessions.Remove(CType(sender, CommunicatorSession).Identifier)

                RemoveHandler CType(sender, CommunicatorSession).Disposing, AddressOf CommunicatorSession_Disposing
                RemoveHandler CType(sender, CommunicatorSession).Communicator.CallReceived, AddressOf CommunicatorSession_CallReceived
                RemoveHandler CType(sender, CommunicatorSession).Communicator.BeforeSend, AddressOf CommunicatorSession_BeforeSend

                Me.OnSessionDisposed(New EventArgs.CommunicatorSessionDisposedEventArgs(sender))

            End If

        End Sub

#End Region

#Region "Connection acceptance"

        Private Sub SocketAccepter_Accepted(ByVal sender As Object, ByVal e As EventArgs.SocketAcceptedEventArgs)
            Try
                Using New Threading.Lock(Me.Lock, True)
                    RemoveHandler CType(sender, SocketAccepter).Accepted, AddressOf SocketAccepter_Accepted
                    RemoveHandler CType(sender, SocketAccepter).SyntaxError, AddressOf SocketAccepter_SyntaxError
                    RemoveHandler CType(sender, SocketAccepter).Timeout, AddressOf SocketAccepter_Timeout

                    Me.SocketAccepters.Remove(sender)

                    Dim tmpExistingSession As CommunicatorSession = Me.Sessions(e.IDentifier)

                    If Not tmpExistingSession Is Nothing Then
                        ConnectionsReestablishedPerformanceCounter.Increment()

                        Me.OnConnectionReestablished(New EventArgs.CommunicationServerConnectionReestablishedEventArgs(tmpExistingSession, e.Socket))

                        tmpExistingSession.Communicator.ReplaceSocket(e.Socket)
                        tmpExistingSession._SocketInfo = e.Socket.RemoteEndPoint

                    Else
                        ConnectionsEstablishedPerformanceCounter.Increment()

                        Dim tmpEventArgs As EventArgs.CommunicationServerConnectionEstablishedEventArgs = New EventArgs.CommunicationServerConnectionEstablishedEventArgs(e.Socket, True)

                        Me.OnConnectionEstablished(tmpEventArgs)

                        If tmpEventArgs.Accepted Then
                            ConnectionsAcceptedPerformanceCounter.Increment()

                            Try
                                'tmpClient.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1)

                                Dim tmpClientSession As New CommunicatorSession(e.IDentifier, e.Socket, Me.StreamHandler)

                                AddHandler tmpClientSession.Communicator.CallReceived, AddressOf CommunicatorSession_CallReceived
                                AddHandler tmpClientSession.Communicator.BeforeSend, AddressOf CommunicatorSession_BeforeSend
                                AddHandler tmpClientSession.Disposing, AddressOf CommunicatorSession_Disposing

                                Me.Sessions(tmpClientSession.Identifier) = tmpClientSession

                                tmpClientSession.Initialize()

                                Me.OnSessionCreated(New EventArgs.CommunicatorSessionCreatedEventArgs(tmpClientSession))

                            Catch ex As Exception
                                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                            End Try

                        Else
                            ConnectionsDeniedPerformanceCounter.Increment()

                        End If
                    End If

                End Using

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Private Sub SocketAccepter_SyntaxError(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Using New Threading.Lock(Me.Lock, True)
                    ConnectionErrorsPerformanceCounter.Increment()

                    RemoveHandler CType(sender, SocketAccepter).Accepted, AddressOf SocketAccepter_Accepted
                    RemoveHandler CType(sender, SocketAccepter).SyntaxError, AddressOf SocketAccepter_SyntaxError
                    RemoveHandler CType(sender, SocketAccepter).Timeout, AddressOf SocketAccepter_Timeout

                    Me.SocketAccepters.Remove(sender)

                    Dim tmpIP As String = CType(CType(sender, SocketAccepter).Socket.RemoteEndPoint, IPEndPoint).Address.ToString

                    If Not Me.UncompatibleClients.Contains(tmpIP) Then
                        Me.UncompatibleClients.Add(tmpIP)

                    End If

                    CType(sender, SocketAccepter).Socket.Send(System.Text.Encoding.UTF8.GetBytes("Syntax error - Connections are only accepted by mermaid BaseObjects Communicator."), SocketFlags.None)

                End Using

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Private Sub SocketAccepter_Timeout(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Using New Threading.Lock(Me.Lock, True)
                    ConnectionErrorsPerformanceCounter.Increment()

                    RemoveHandler CType(sender, SocketAccepter).Accepted, AddressOf SocketAccepter_Accepted
                    RemoveHandler CType(sender, SocketAccepter).SyntaxError, AddressOf SocketAccepter_SyntaxError
                    RemoveHandler CType(sender, SocketAccepter).Timeout, AddressOf SocketAccepter_Timeout

                    Me.SocketAccepters.Remove(sender)

                    Dim tmpIP As String = CType(CType(sender, SocketAccepter).Socket.RemoteEndPoint, IPEndPoint).Address.ToString

                    If Not Me.UncompatibleClients.Contains(tmpIP) Then
                        Me.UncompatibleClients.Add(tmpIP)

                    End If

                End Using

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

        Private Class SocketAccepter

#Region "Variables"

            Private _Socket As Socket
            Private Shared _AcceptTimeout As TimeSpan = TimeSpan.FromMinutes(1)

            Private WithEvents _TmrTimeout As Timers.Timer

#End Region

#Region "Properties"

            Public ReadOnly Property Socket() As Socket
                Get
                    Return Me._Socket

                End Get
            End Property

            Public Shared Property AcceptTimeout() As TimeSpan
                Get
                    Return _AcceptTimeout

                End Get
                Set(ByVal value As TimeSpan)
                    _AcceptTimeout = value

                End Set
            End Property

            Private Property TmrTimeout() As Timers.Timer
                Get
                    Return Me._TmrTimeout

                End Get
                Set(ByVal value As Timers.Timer)
                    Me._TmrTimeout = value

                End Set
            End Property

#End Region

#Region "Events"

            Public Event Accepted As System.EventHandler(Of EventArgs.SocketAcceptedEventArgs)
            Public Event Timeout As System.EventHandler
            Public Event SyntaxError As System.EventHandler

#End Region

            Public Sub New(ByVal socket As Socket)
                MyBase.New()

                Me._Socket = socket

                Me.TmrTimeout = New Timers.Timer
                Me.TmrTimeout.Interval = AcceptTimeout.TotalMilliseconds
                Me.TmrTimeout.Enabled = True

            End Sub

            Public Sub Accept()
                Try
                    Dim tmpReceiveBuffer(SocketUtility.IDENTIFIER_LENGTH + SocketUtility.EOIBytes.Length - 1) As Byte

                    Dim tmpBytesReceived As Integer = Me.Socket.Receive(tmpReceiveBuffer, SocketFlags.None)

                    If tmpBytesReceived > 0 Then
                        Dim tmpIDentifier As String = SocketUtility.ExtractIDentifier(tmpReceiveBuffer)

                        If Not String.IsNullOrEmpty(tmpIDentifier) Then
                            Me.OnAccepted(New EventArgs.SocketAcceptedEventArgs(tmpIDentifier, Me.Socket))

                        ElseIf SocketUtility.IndexOfDiag(tmpReceiveBuffer) <> -1 Then
                            'If Communicator.DiagBroadcaster Is Nothing Then
                            '    Communicator.DiagBroadcaster = New TCPBroadcaster(IPAddress.Parse("127.0.0.1"), 8888)
                            '    Communicator.DiagBroadcaster.AutoFlush = True

                            'Else
                            '    Communicator.DiagBroadcaster = Nothing

                            'End If
                            If Communicator.DiagLogWriter Is Nothing Then
                                Communicator.DiagLogWriter = New SocketLogWriter

                            Else
                                Communicator.DiagLogWriter = Nothing

                            End If

                        Else
                            Me.OnSyntaxError(New System.EventArgs)

                        End If
                    End If

                Catch ex As Exception
                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                    Me.OnSyntaxError(New System.EventArgs)

                End Try
            End Sub

            Private Sub TmrTimeout_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrTimeout.Elapsed
                Try
                    Me.OnTimeout(New System.EventArgs)

                Catch ex As Exception
                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                End Try
            End Sub

            Private Sub OnAccepted(ByVal e As EventArgs.SocketAcceptedEventArgs)
                RaiseEvent Accepted(Me, e)

                Me.Dispose(False)

            End Sub

            Private Sub OnTimeout(ByVal e As System.EventArgs)
                RaiseEvent Timeout(Me, e)

                Me.Dispose(True)

            End Sub

            Private Sub OnSyntaxError(ByVal e As System.EventArgs)
                RaiseEvent SyntaxError(Me, e)

                Me.Dispose(True)

            End Sub

            Private Sub Dispose(ByVal disposeSocket As Boolean)
                Try
                    Me.TmrTimeout.Enabled = False
                    Me.TmrTimeout.Dispose()
                    Me.TmrTimeout = Nothing

                    If disposeSocket Then
                        Me.Socket.Close()
                        Me._Socket = Nothing

                    End If

                Catch ex As Exception
                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                End Try
            End Sub

        End Class

#End Region

#Region "Event raisers"

        Private Sub OnServerStarted(ByVal e As EventArgs.CommunicationServerStartedEventArgs)
            RaiseEvent ServerStarted(Me, e)

        End Sub

        Private Sub OnSessionCreated(ByVal e As EventArgs.CommunicatorSessionCreatedEventArgs)
            RaiseEvent SessionCreated(Me, e)

        End Sub

        Private Sub OnSessionDisposed(ByVal e As EventArgs.CommunicatorSessionDisposedEventArgs)
            RaiseEvent SessionDisposed(Me, e)

        End Sub

        Private Sub OnConnectionEstablished(ByVal e As EventArgs.CommunicationServerConnectionEstablishedEventArgs)
            RaiseEvent ConnectionEstablished(Me, e)

        End Sub

        Private Sub OnConnectionReestablished(ByVal e As EventArgs.CommunicationServerConnectionReestablishedEventArgs)
            RaiseEvent ConnectionReestablished(Me, e)

        End Sub

#End Region

#Region "IDisposable Support"

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.DisposeNetworkResources()

                    Me._Communicator = Nothing
                    'Me._Socket = Nothing
                    Me.Sockets = Nothing
                    Me._Sessions.Clear()
                    Me._Sessions = Nothing
                    'Me._Port = Nothing

                End If
            End If

            Me.IsDisposed = True

        End Sub

        Private Sub DisposeNetworkResources()
            Try
                'Me.Socket.Close()
                For Each tmpSocket As Socket In Me.Sockets.Values
                    tmpSocket.Close()
                Next

                Me.Sockets.Clear()

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            End Try
        End Sub

#End Region

    End Class

End Namespace