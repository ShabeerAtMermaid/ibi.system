Imports mermaid.BaseObjects.Net.Sockets.Interfaces
Imports mermaid.BaseObjects.Threading
Imports mermaid.BaseObjects.Diagnostics
Imports System.Net.Sockets
Imports System.Net
Imports System.IO
Imports System.Threading
Imports System.Management

Namespace Net.Sockets

    Public Class Communicator

#Region "Variables"

        Private WithEvents _SocketInfo As SocketInfo
        Private _CreationTime As Date

        Private _RemoteServerIP As IPAddress
        Private _RemoteServerName As String
        Private _RemoteServerPort As Integer
        Private _Identifier As String
        Private _IsClientCommunicator As Boolean
        Private _Server As CommunicationServer
        Private _Socket As Socket
        Private _SocketEndPoint As IPEndPoint
        Private _ReceiveBuffer() As Byte
        Private _Buffer As New Hashtable
        Private _Lock As New Object
        Private _HandleCollection As New Hashtable
        Private _InvocationCounter As Integer
        Private _IsDisposed As Boolean
        Private _StreamHandler As Interfaces.IStreamHandler
        Private _CompressionEnabled As Boolean
        Private _ReconnectDelay As TimeSpan = TimeSpan.FromSeconds(10)

        Private Shared _ActiveCallDetails As New System.Collections.Generic.List(Of CommunicatorDataObject)
        Private Shared _ActiveCallLock As New Object

        'Private _OnReceiveCallback As System.AsyncCallback
        Private _AsyncSocketCallback As System.EventHandler(Of SocketAsyncEventArgs)

        Private _ReceiveData As ReceiveData

        Private _FileProviders As New Hashtable

        Private _Sesssion As CommunicatorSession

        Private _PingsSentPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _PingsSent As Long
        Private _PingsReceivedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _PingsReceived As Long
        Private _BytesSentPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _BytesSent As Long
        Private _BytesReceivedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _BytesReceived As Long
        Private _CallsPerformedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _CallsPerformed As Long
        Private _CallsReceivedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _CallsReceived As Long
        Private _RepliesSentPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _RepliesSent As Long
        Private _RepliesReceivedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _RepliesReceived As Long
        Private _ConnectFailedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ConnectFailed As Long
        Private _SocketShutdownPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _SocketShutDowns As Long
        Private _SerializationErrorsPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _SerializationErrors As Long
        Private _DeserializationErrorsPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _DeserializationErrors As Long
        Private _TotalSerializationTimePerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _TotalSerializationTime As Long
        Private _TotalDeserializationTimePerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _TotalDeserializationTime As Long
        Private _ReplyBufferCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ReplyBufferCount As Long
        Private _ReplyHandleCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ReplyHandleCount As Long

        Private _InternalErrors As Long

        Private _DebugBroadcaster As UDPBroadcaster
        'Private Shared _DiagBroadcaster As TCPBroadcaster
        Private Shared _DiagLogWriter As SocketLogWriter

#End Region

#Region "Properties"

        Public Property SocketInfo() As SocketInfo
            Get
                Return Me._SocketInfo

            End Get
            Set(ByVal value As SocketInfo)
                Me._SocketInfo = value

            End Set
        End Property

        Public ReadOnly Property IPAddress() As String
            Get
                If Not Me.Socket Is Nothing Then
                    Return CType(Me.Socket.LocalEndPoint, IPEndPoint).Address.ToString

                End If

                Return String.Empty

            End Get
        End Property

        Public ReadOnly Property MACAddress() As String
            Get
                Try
                    Dim res As String = String.Empty

                    Dim mc As ManagementClass
                    Dim moc As ManagementObjectCollection
                    Dim mo As ManagementObject

                    mc = New ManagementClass("Win32_NetworkAdapterConfiguration")
                    moc = mc.GetInstances() ' Get all network adapter instances

                    Dim tmpIPAddress As String = Me.IPAddress

                    For Each mo In moc
                        Try
                            If mo.Item("IPEnabled") = True Then
                                Dim tmpMoIPAddress As String = mo("IPAddress")(0)

                                'UNDONE: Doesn't work
                                'If String.Compare(tmpIPAddress, tmpMoIPAddress, True) = 0 Then
                                res = mo.Item("MacAddress").ToString

                                Exit For

                                'End If
                            End If

                            mo.Dispose()

                        Catch ex As Exception
                            'SILENT

                        End Try

                    Next

                    Return res

                Catch ex As Exception
                    'SILENT

                End Try
            End Get
        End Property

        Public ReadOnly Property RemoteServerIP() As IPAddress
            Get
                Return Me._RemoteServerIP

            End Get
        End Property

        Public ReadOnly Property RemoteServerName() As String
            Get
                Return Me._RemoteServerName

            End Get
        End Property

        Public ReadOnly Property RemoteServerPort() As Integer
            Get
                Return Me._RemoteServerPort

            End Get
        End Property

        Public ReadOnly Property Identifier() As String
            Get
                Return Me._Identifier

            End Get
        End Property

        Private Property IsClientCommunicator() As Boolean
            Get
                Return Me._IsClientCommunicator

            End Get
            Set(ByVal value As Boolean)
                Me._IsClientCommunicator = value

            End Set
        End Property

        Private Property Server() As CommunicationServer
            Get
                Return Me._Server

            End Get
            Set(ByVal Value As CommunicationServer)
                Me._Server = Value

            End Set
        End Property

        Private Property Socket() As Socket
            Get
                Return Me._Socket

            End Get
            Set(ByVal value As Socket)
                Me._Socket = value

                If Not Me._Socket Is Nothing Then
                    Me._SocketEndPoint = CType(Me._Socket.RemoteEndPoint, System.Net.IPEndPoint)

                End If
            End Set
        End Property

        Private Property Buffer() As Hashtable
            Get
                Return Me._Buffer

            End Get
            Set(ByVal value As Hashtable)
                Me._Buffer = value

            End Set
        End Property

        Private ReadOnly Property Lock() As Object
            Get
                Return Me._Lock

            End Get
        End Property

        Private ReadOnly Property HandleCollection() As Hashtable
            Get
                Return Me._HandleCollection

            End Get
        End Property

        Private ReadOnly Property InvocationCounter() As String
            Get
                Dim tmpID As Integer = Interlocked.Increment(Me._InvocationCounter)

                Return Me.Identifier & "_" & tmpID

            End Get
        End Property

        Public ReadOnly Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
        End Property

        Friend Property Session() As CommunicatorSession
            Get
                Return Me._Sesssion

            End Get
            Set(ByVal value As CommunicatorSession)
                Me._Sesssion = value

            End Set
        End Property

        Public Property StreamHandler() As Interfaces.IStreamHandler
            Get
                Return Me._StreamHandler

            End Get
            Set(ByVal value As Interfaces.IStreamHandler)
                If IsDirty(Me._StreamHandler, value) Then
                    Me._StreamHandler = value

                    If Not Me._Server Is Nothing Then
                        Me._Server.StreamHandler = value

                    End If
                End If

            End Set
        End Property

        Public Property CompressionEnabled() As Boolean
            Get
                Return _CompressionEnabled

            End Get
            Set(ByVal value As Boolean)
                If IsDirty(Me._CompressionEnabled, value) Then
                    Me._CompressionEnabled = value

                    If Me._CompressionEnabled Then
                        Me.StreamHandler = New CompressionStreamHandler

                    Else
                        Me.StreamHandler = Nothing

                    End If
                End If

            End Set
        End Property

        Public Property ReconnectDelay() As TimeSpan
            Get
                Return Me._ReconnectDelay

            End Get
            Set(ByVal value As TimeSpan)
                Me._ReconnectDelay = value

            End Set
        End Property

        Public Shared ReadOnly Property ActiveCallDetails() As System.Collections.Generic.List(Of CommunicatorDataObject)
            Get
                Return _ActiveCallDetails

            End Get
        End Property

        Private Shared ReadOnly Property ActiveCallLock() As Object
            Get
                Return _ActiveCallLock

            End Get
        End Property

        'Private Property OnReceiveCallback() As System.AsyncCallback
        '    Get
        '        Return Me._OnReceiveCallback

        '    End Get
        '    Set(ByVal value As System.AsyncCallback)
        '        Me._OnReceiveCallback = value

        '    End Set
        'End Property

        Private Property AsyncSocketCallback() As System.EventHandler(Of SocketAsyncEventArgs)
            Get
                Return _AsyncSocketCallback

            End Get
            Set(ByVal value As System.EventHandler(Of SocketAsyncEventArgs))
                _AsyncSocketCallback = value

            End Set
        End Property

        Private ReadOnly Property FileProviders() As Hashtable
            Get
                Return Me._FileProviders

            End Get
        End Property

        Private Property PingsSentPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._PingsSentPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._PingsSentPerformanceCounter = value

            End Set
        End Property

        Private Property PingsReceivedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._PingsReceivedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._PingsReceivedPerformanceCounter = value

            End Set
        End Property

        Private Property BytesSentPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._BytesSentPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._BytesSentPerformanceCounter = value

            End Set
        End Property

        Private Property BytesReceivedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._BytesReceivedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._BytesReceivedPerformanceCounter = value

            End Set
        End Property

        Private Property CallsPerformedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._CallsPerformedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._CallsPerformedPerformanceCounter = value

            End Set
        End Property

        Private Property CallsReceivedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._CallsReceivedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._CallsReceivedPerformanceCounter = value

            End Set
        End Property

        Private Property RepliesSentPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._RepliesSentPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._RepliesSentPerformanceCounter = value

            End Set
        End Property

        Private Property RepliesReceivedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._RepliesReceivedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._RepliesReceivedPerformanceCounter = value

            End Set
        End Property

        Private Property ConnectFailedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ConnectFailedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ConnectFailedPerformanceCounter = value

            End Set
        End Property

        Private Property SocketShutdownPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._SocketShutdownPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._SocketShutdownPerformanceCounter = value

            End Set
        End Property

        Private Property SerializationErrorsPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._SerializationErrorsPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._SerializationErrorsPerformanceCounter = value

            End Set
        End Property

        Private Property DeserializationErrorsPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._DeserializationErrorsPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._DeserializationErrorsPerformanceCounter = value

            End Set
        End Property

        Private Property TotalSerializationTimePerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._TotalSerializationTimePerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._TotalSerializationTimePerformanceCounter = value

            End Set
        End Property

        Private Property TotalDeserializationTimePerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._TotalDeserializationTimePerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._TotalDeserializationTimePerformanceCounter = value

            End Set
        End Property

        Private Property ReplyBufferCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ReplyBufferCountPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ReplyBufferCountPerformanceCounter = value

            End Set
        End Property

        Private Property ReplyHandleCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ReplyHandleCountPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ReplyHandleCountPerformanceCounter = value

            End Set
        End Property

        Private Property ReinitializedReceiveDataObjectsPerformanceCounter As PerformanceCounter

        Private Property PendingAsyncReceiveCallsPerformanceCounter As PerformanceCounter

        Private Property AsyncReceiveCallsSyncCompletionPerformanceCounter As PerformanceCounter

        Private Property SocketReplacementsPerformanceCounter As PerformanceCounter

        Private Property DebugBroadcaster() As UDPBroadcaster
            Get
                Return Me._DebugBroadcaster

            End Get
            Set(ByVal value As UDPBroadcaster)
                Me._DebugBroadcaster = value

            End Set
        End Property

        'Friend Shared Property DiagBroadcaster() As UDPBroadcaster
        '    Get
        '        Return _DiagBroadcaster

        '    End Get
        '    Set(ByVal value As UDPBroadcaster)
        '        _DiagBroadcaster = value

        '    End Set
        'End Property

        'Friend Shared Property DiagBroadcaster() As TCPBroadcaster
        '    Get
        '        Return _DiagBroadcaster

        '    End Get
        '    Set(ByVal value As TCPBroadcaster)
        '        _DiagBroadcaster = value

        '    End Set
        'End Property

        Friend Shared Property DiagLogWriter() As SocketLogWriter
            Get
                Return _DiagLogWriter

            End Get
            Set(ByVal value As SocketLogWriter)
                _DiagLogWriter = value

            End Set
        End Property

        Public Property NetworkLoggingEnabled As Boolean = False

#End Region

#Region "Test Variables"

        Public Shared LogCalls As Boolean = False

#End Region

#Region "Events"

        Public Event Connected As System.EventHandler(Of System.EventArgs)
        Public Event CallReceived As System.EventHandler(Of EventArgs.CommunicatorCallReceivedEventArgs)
        Public Event BeforeSend As System.EventHandler(Of EventArgs.CommunicatorBeforeSendEventArgs)
        Public Event Disposing As System.EventHandler

#End Region

        Friend Sub New(ByVal server As CommunicationServer)
            MyBase.New()

            Me.InitializePerformanceCounters()

            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
            Me._ReceiveData = New ReceiveData(Me.AsyncSocketCallback)

            Me.SocketInfo = New SocketInfo

            Me._Identifier = System.Guid.NewGuid.ToString
            Me._Server = server

            Me._IsClientCommunicator = False

        End Sub

        Friend Sub New(ByVal identifier As String, ByVal socket As Socket)
            MyBase.New()

            Me.InitializePerformanceCounters()

            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
            Me._ReceiveData = New ReceiveData(Me.AsyncSocketCallback)

            Me.SocketInfo = New SocketInfo

            Me._CreationTime = Date.Now

            Me._Identifier = identifier
            Me.Socket = socket

            Me._IsClientCommunicator = False

        End Sub

        Public Sub New(ByVal remoteServer As String, ByVal remoteServerPort As Integer)
            MyBase.New()

            Me.InitializePerformanceCounters()

            Me._IsClientCommunicator = True

            Me._RemoteServerName = remoteServer
            Me._RemoteServerPort = remoteServerPort

            Me._Identifier = System.Guid.NewGuid.ToString

            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
            Me._ReceiveData = New ReceiveData(Me.AsyncSocketCallback)

            Me.SocketInfo = New SocketInfo
            Me.SocketInfo.FriendlyName = My.Computer.Name

            Me.Connect()

        End Sub

        Public Sub New(ByVal remoteServerIP As IPAddress, ByVal remoteServerPort As Integer)
            MyBase.New()

            Me.InitializePerformanceCounters()

            Me._IsClientCommunicator = True

            Me._RemoteServerIP = remoteServerIP
            Me._RemoteServerPort = remoteServerPort

            Me._Identifier = System.Guid.NewGuid.ToString

            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
            Me._ReceiveData = New ReceiveData(Me.AsyncSocketCallback)

            Me.SocketInfo = New SocketInfo
            Me.SocketInfo.FriendlyName = My.Computer.Name

            Me.Connect()

        End Sub

        Friend Sub Initialize()
            Me.Receive()

        End Sub

        Friend Function GetDebugString() As String
            Try
                Dim tmpDebugString As String = ""

                If Not _SocketEndPoint Is Nothing Then
                    tmpDebugString = "<session>" & Me.Identifier & "$" & Me.SocketInfo.FriendlyName & "$" & _CreationTime.ToString & "." & _CreationTime.Millisecond & "$" & _SocketEndPoint.Address.ToString & "$" & _SocketEndPoint.Port & "$" & Me.Session.Timeout.TotalMilliseconds & "$" & _PingsSent & "$" & _PingsReceived & "$" & _BytesSent & "$" & _BytesReceived & "$" & _CallsPerformed & "$" & _CallsReceived & "$" & _RepliesSent & "$" & _RepliesReceived & "$" & _SerializationErrors & "$" & _DeserializationErrors & "$" & _TotalSerializationTime & "$" & _TotalDeserializationTime & "$" & Me.Session.LastActivity.ToString & "$" & Me._InternalErrors & "$" & (Not Me.IsDisposed).ToString & "<@>"

                Else
                    tmpDebugString = "<session>" & Me.Identifier & "$" & Me.SocketInfo.FriendlyName & "$" & _CreationTime.ToString & "." & _CreationTime.Millisecond & "$" & "UNKNOWN" & "$" & 0 & "$" & Me.Session.Timeout.TotalMilliseconds & "$" & _PingsSent & "$" & _PingsReceived & "$" & _BytesSent & "$" & _BytesReceived & "$" & _CallsPerformed & "$" & _CallsReceived & "$" & _RepliesSent & "$" & _RepliesReceived & "$" & _SerializationErrors & "$" & _DeserializationErrors & "$" & _TotalSerializationTime & "$" & _TotalDeserializationTime & "$" & Me.Session.LastActivity.ToString & "$" & Me._InternalErrors & "$" & (Not Me.IsDisposed).ToString & "<@>"

                End If

                Return tmpDebugString

            Catch ex As Exception
                Return ""

            End Try
        End Function

        Private Function GetErrorString(ByVal ex As Exception) As String
            Dim tmpExceptionString As String = ""
            Dim tmpException As Exception = ex

            tmpExceptionString &= "Exception message: " & tmpException.Message
            tmpExceptionString &= Environment.NewLine
            tmpExceptionString &= "Stack trace: " & tmpException.StackTrace
            tmpExceptionString &= Environment.NewLine

            tmpException = tmpException.InnerException

            While tmpException IsNot Nothing
                tmpExceptionString &= "----------------- CAUSED BY -----------------"
                tmpExceptionString &= Environment.NewLine
                tmpExceptionString &= "Exception message: " & tmpException.Message
                tmpExceptionString &= Environment.NewLine
                tmpExceptionString &= "Stack trace: " & tmpException.StackTrace
                tmpExceptionString &= Environment.NewLine

                tmpException = tmpException.InnerException

            End While

            Dim tmpErrorString As String = "<error>" & Me.Identifier & "$" & Date.Now.ToString & "$" & tmpExceptionString & "<@>"

            Return tmpErrorString

        End Function

        Private Sub InitializePerformanceCounters()
            Try
                Me.PingsSentPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator pings sent")
                Me.PingsReceivedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator pings received")
                Me.BytesSentPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator bytes sent")
                Me.BytesReceivedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator bytes received")
                Me.CallsPerformedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator calls performed")
                Me.CallsReceivedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator calls received")
                Me.RepliesSentPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator replies sent")
                Me.RepliesReceivedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator replies received")
                Me.ConnectFailedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator connect failed")
                Me.SocketShutdownPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator sockets shutdown")
                Me.SerializationErrorsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator serialization errors")
                Me.DeserializationErrorsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator deserialization errors")
                Me.TotalSerializationTimePerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator serialization time")
                Me.TotalDeserializationTimePerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator deserialization time")
                Me.ReplyBufferCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator reply buffer count")
                Me.ReplyHandleCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator reply handle count")
                Me.ReinitializedReceiveDataObjectsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator.ReceiveData Objects re-initialized")
                Me.PendingAsyncReceiveCallsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator pending async receive calls")
                Me.AsyncReceiveCallsSyncCompletionPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator async receive calls synchronously completion")
                Me.SocketReplacementsPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator sockets replaced")

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

            End Try
        End Sub

        Private Class ReceiveData

#Region "Variables"

            Private _Socket As Socket
            Private _AsyncEventArgs As SocketAsyncEventArgs
            Private _Buffer() As Byte
            Private _InUse As Boolean

            Private _Callback As System.EventHandler(Of SocketAsyncEventArgs)
            Private _IsDisposed As Boolean

#End Region

#Region "Properties"

            Public Property Socket() As Socket
                Get
                    Return Me._Socket

                End Get
                Set(ByVal value As Socket)
                    Me._Socket = value

                End Set
            End Property

            Public Property AsyncEventArgs() As SocketAsyncEventArgs
                Get
                    Return Me._AsyncEventArgs

                End Get
                Set(ByVal value As SocketAsyncEventArgs)
                    Me._AsyncEventArgs = value

                End Set
            End Property

            Public Property Buffer() As Byte()
                Get
                    Return Me._Buffer

                End Get
                Set(ByVal value As Byte())
                    Me._Buffer = value

                End Set
            End Property

            Public Property InUse() As Boolean
                Get
                    Return Me._InUse

                End Get
                Set(ByVal value As Boolean)
                    Me._InUse = value

                End Set
            End Property

            Public ReadOnly Property IsDisposed() As Boolean
                Get
                    Return Me._IsDisposed

                End Get
            End Property

            Private ReadOnly Property ActiveReceiveDataObjectsPerformanceCounter As PerformanceCounter
                Get
                    Return BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Communicator.ReceiveData Objects active")

                End Get
            End Property

#End Region

            Public Sub New(ByVal callback As System.EventHandler(Of SocketAsyncEventArgs))
                _Callback = callback

                'Me.Buffer = Array.CreateInstance(GetType(Byte), SocketUtility.BufferSize)
                Me.Buffer = SocketBufferPool.GetBuffer("Communicator")
                Me.AsyncEventArgs = New SocketAsyncEventArgs

                AddHandler Me.AsyncEventArgs.Completed, callback

                Me.AsyncEventArgs.SetBuffer(Me.Buffer, 0, Me.Buffer.Length)
                Me.AsyncEventArgs.UserToken = Me

                Me.ActiveReceiveDataObjectsPerformanceCounter.Increment()

            End Sub

            Public Sub Clear()
                Array.Clear(Me.Buffer, 0, Me.Buffer.Length)

                Me.InUse = False

            End Sub

            Public Sub Dispose()
                If Not Me.IsDisposed Then
                    Me._IsDisposed = True

                    Try
                        ''Me.AsyncEventArgs.SetBuffer(Nothing, 0, 0)
                        RemoveHandler Me.AsyncEventArgs.Completed, _Callback

                        _Callback = Nothing

                        'Array.Resize(Me.Buffer, 0)
                        'Array.Resize(Me.AsyncEventArgs.Buffer, 0)

                        ''Me.AsyncEventArgs.SetBuffer(Me.Buffer, 0, 0)
                        Me.AsyncEventArgs.UserToken = Nothing
                        Me.AsyncEventArgs.Dispose()
                        Me.AsyncEventArgs = Nothing
                        Me.Clear()

                        SocketBufferPool.ReuseBuffer("Communicator", Me.Buffer)

                        Me.Buffer = Nothing

                        Me.ActiveReceiveDataObjectsPerformanceCounter.Decrement()

                    Catch ex As Exception
                        Console.WriteLine(ex.Message & Environment.NewLine & ex.StackTrace)
                        Logger.WriteLine("Socket", ex.Message & Environment.NewLine & ex.StackTrace)

                    End Try
                End If

            End Sub

        End Class

        Private Sub Receive()
            Try
                If Not Me.IsDisposed Then
                    If Not Me.Socket Is Nothing Then
                        'If Not Me._ReceiveData Is Nothing Then
                        '    Me._ReceiveData.Dispose()

                        'End If

                        'Me._ReceiveData = New ReceiveData
                        If Me._ReceiveData.IsDisposed Or Me._ReceiveData.InUse Then
                            ReinitializedReceiveDataObjectsPerformanceCounter.Increment()
                            Me._ReceiveData = New ReceiveData(Me.AsyncSocketCallback)

                        End If

                        Me._ReceiveData.Socket = Me.Socket

                        Me._ReceiveData.InUse = True

                        If Me._ReceiveData.Socket.ReceiveAsync(Me._ReceiveData.AsyncEventArgs) Then
                            ' MSDN (http://msdn.microsoft.com/en-us/library/system.net.sockets.socket.receiveasync.aspx): The I/O operation is pending. 
                            ' The SocketAsyncEventArgs.Completed event on the e parameter will be raised upon  completion of the operation. 

                            Me.PendingAsyncReceiveCallsPerformanceCounter.Increment()

                        Else
                            ' MSDN (http://msdn.microsoft.com/en-us/library/system.net.sockets.socket.receiveasync.aspx): The I/O operation completed synchronously. 
                            ' In this case, The SocketAsyncEventArgs.Completed event on the e parameter  will not be raised and the e object passed as a parameter
                            ' may be examined immediately after the method call returns to  retrieve the result of the operation.

                            Me.PendingAsyncReceiveCallsPerformanceCounter.Increment()
                            Me.AsyncReceiveCallsSyncCompletionPerformanceCounter.Increment()

                            Me.Socket_OnReceive(Me, Me._ReceiveData.AsyncEventArgs)

                        End If
                    End If
                End If

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

                Me.DisposeNetworkResources()

            End Try
        End Sub

        Private Sub Socket_OnReceive(ByVal src As Object, ByVal sae As SocketAsyncEventArgs)
            Me.PendingAsyncReceiveCallsPerformanceCounter.Decrement()

            'Dim tmpReceiveData As ReceiveData = ar.AsyncState
            Dim tmpReceiveData As ReceiveData = sae.UserToken

            If Not Me.IsDisposed Then
                Dim tmpReceiveRestarted As Boolean = False

                Using tmpAnalyzer As New Diagnostics.PerformanceAnalyzer("Communicator.Socket_OnReceive")
                    Try
                        tmpAnalyzer.AddPerformanceMark("Start")

                        If Not tmpReceiveData.Socket Is Nothing Then
                            'Dim tmpBytesReceived As Integer = tmpReceiveData.Socket.EndReceive(ar)
                            Dim tmpBytesReceived As Integer = sae.BytesTransferred
                            tmpAnalyzer.AddPerformanceMark("Receiving data")

                            If Not Me.IsDisposed Then
                                If tmpBytesReceived > 0 Then
                                    tmpAnalyzer.AddPerformanceMark(tmpBytesReceived & " bytes received")

                                    If Not Me.DebugBroadcaster Is Nothing Then
                                        Me.DebugBroadcaster.WriteLine(tmpBytesReceived & " bytes received")

                                    End If

                                    Dim tmpCurrentBufferLength As Long = 0

                                    If Not _ReceiveBuffer Is Nothing Then tmpCurrentBufferLength = _ReceiveBuffer.Length

                                    BytesReceivedPerformanceCounter.Increment(tmpBytesReceived)
                                    Interlocked.Add(_BytesReceived, tmpBytesReceived)

                                    tmpAnalyzer.AddPerformanceMark("Updating BytesReceived counters")

                                    Dim tmpReceiveBuffer(tmpCurrentBufferLength + tmpBytesReceived - 1) As Byte

                                    If Not _ReceiveBuffer Is Nothing Then _ReceiveBuffer.CopyTo(tmpReceiveBuffer, 0)
                                    Array.Copy(tmpReceiveData.Buffer, 0, tmpReceiveBuffer, tmpCurrentBufferLength, tmpBytesReceived)

                                    'If Not tmpReceiveData Is Me._ReceiveData Then
                                    '    tmpReceiveData.Dispose()
                                    '    tmpReceiveData = Nothing
                                    'Else
                                    '    tmpReceiveData.Clear()
                                    'End If

                                    If tmpReceiveData Is Me._ReceiveData Then
                                        tmpReceiveData.Clear()
                                    End If

                                    _ReceiveBuffer = tmpReceiveBuffer

                                    tmpAnalyzer.AddPerformanceMark("Moving data to receive buffer")

                                    Dim tmpEOSIndex As Long = SocketUtility.IndexOfEOS(_ReceiveBuffer)

                                    tmpAnalyzer.AddPerformanceMark("Searching for EOS")

                                    If tmpEOSIndex = -1 Then
                                        tmpAnalyzer.AddPerformanceMark("Partial chunk")

                                        If Not Me.DebugBroadcaster Is Nothing Then
                                            Me.DebugBroadcaster.WriteLine("Partial chunk")
                                        End If
                                    End If

                                    Dim tmpReceivedDataList As New ArrayList

                                    While tmpEOSIndex <> -1 And Not Me.IsDisposed
                                        tmpAnalyzer.AddPerformanceMark("Full chunk received")

                                        If Not Me.DebugBroadcaster Is Nothing Then
                                            Me.DebugBroadcaster.WriteLine("Full chunk received")

                                        End If

                                        Dim tmpStreamData As StreamData = GetStream(tmpEOSIndex)

                                        tmpAnalyzer.AddPerformanceMark("Extracting DataObject")

                                        If Not tmpStreamData Is Nothing AndAlso Not tmpStreamData.Stream Is Nothing Then
                                            Dim tmpStream As System.IO.MemoryStream = tmpStreamData.Stream

                                            Dim tmpStreamSize As Long = tmpStream.Length

                                            tmpAnalyzer.AddPerformanceMark("Deserializing of object started")

                                            If Not Me.DebugBroadcaster Is Nothing Then
                                                Me.DebugBroadcaster.WriteLine("Deserializing of object started")

                                            End If

                                            Dim tmpDeserializationStart As Integer = System.Environment.TickCount And Integer.MaxValue

                                            Dim tmpData As CommunicatorDataObject = CommunicatorDataObject.Deserialize(tmpStream)

                                            Dim tmpDeserializationEnd As Integer = System.Environment.TickCount And Integer.MaxValue

                                            tmpAnalyzer.AddPerformanceMark("Deserializing stream")

                                            ' ***
                                            ' We log when buffers  are larger than 85000 bytes since they are stored in the Large Object Heap,
                                            ' that suffers easily from fragmentation leading to OutOfMemory errors.
                                            ' http://www.simple-talk.com/dotnet/.net-framework/the-dangers-of-the-large-object-heap/
                                            ' ***
                                            If tmpEOSIndex >= 85000 Then Logger.WriteLine("LOH", "Communicator buffer: " & tmpEOSIndex & "B | Tag: " & tmpData.Tag)

                                            TotalDeserializationTimePerformanceCounter.Increment(tmpDeserializationEnd - tmpDeserializationStart)
                                            Interlocked.Add(_TotalDeserializationTime, tmpDeserializationEnd - tmpDeserializationStart)

                                            tmpAnalyzer.AddPerformanceMark("Updating Deserializing counters")

                                            tmpStream.Dispose()

                                            tmpAnalyzer.AddPerformanceMark("Closing stream")

                                            If TypeOf tmpData.Data Is SocketInfo Then
                                                Dim tmpSocketInfo As SocketInfo = tmpData.Data

                                                Me.SocketInfo = tmpSocketInfo

                                                tmpAnalyzer.AddPerformanceMark("Data is SocketInfo")

                                                If Not Me.DebugBroadcaster Is Nothing Then
                                                    Me.DebugBroadcaster.WriteLine("Data is SocketInfo")

                                                End If

                                                CallsReceivedPerformanceCounter.Increment()
                                                Interlocked.Increment(_CallsReceived)

                                                'If Not DiagBroadcaster Is Nothing Then
                                                '    Dim tmpDebugString As String = "<call>" & Me.Session.IDentifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                                '    DiagBroadcaster.Write(tmpDebugString)
                                                '    DiagBroadcaster.Write(Me.GetDebugString)

                                                'End If
                                                tmpAnalyzer.AddPerformanceMark("<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>")

                                                If Not DiagLogWriter Is Nothing Then
                                                    Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                                    DiagLogWriter.WriteCallData(Me.Session.Identifier, tmpDebugString)
                                                    DiagLogWriter.UpdateSessionInfo(Me.Session.Identifier, Me.GetDebugString)

                                                End If

                                                tmpAnalyzer.AddPerformanceMark("Updating SocketInfo")

                                            Else
                                                tmpData.SetReceived()

                                                tmpAnalyzer.AddPerformanceMark("Setting received stamp")
                                                tmpAnalyzer.AddPerformanceMark("Deserializing of object finished")
                                                tmpAnalyzer.AddPerformanceMark("DataObject tag is: " & tmpData.Tag)

                                                If Not Me.DebugBroadcaster Is Nothing Then
                                                    Me.DebugBroadcaster.WriteLine("Deserializing of object finished")

                                                    If TypeOf tmpData.Tag Is String Then
                                                        Me.DebugBroadcaster.WriteLine("DataObject tag is: " & tmpData.Tag)

                                                    End If
                                                End If

                                                If tmpData.IsReply Then
                                                    tmpData.ReplyProcessingTime = Date.Now.Subtract(tmpData.Sent)

                                                    tmpAnalyzer.AddPerformanceMark("Data is reply, calculating reply time")
                                                    tmpAnalyzer.AddPerformanceMark("Reply time: " & tmpData.ReplyProcessingTime.ToString)

                                                    If Not Me.DebugBroadcaster Is Nothing Then
                                                        Me.DebugBroadcaster.WriteLine("Data is reply")
                                                        Me.DebugBroadcaster.WriteLine("Reply time: " & tmpData.ReplyProcessingTime.ToString)

                                                    End If

                                                    Monitor.Enter(Me.Lock)

                                                    tmpAnalyzer.AddPerformanceMark("Entering reply buffer lock")

                                                    Me.Buffer(tmpData.InvocationID) = tmpData

                                                    ReplyBufferCountPerformanceCounter.Increment()
                                                    Interlocked.Increment(_ReplyBufferCount)

                                                    RepliesReceivedPerformanceCounter.Increment()
                                                    Interlocked.Increment(_RepliesReceived)

                                                    tmpAnalyzer.AddPerformanceMark("Updating RepliesReceived counters")

                                                    Try
                                                        Dim tmpHandle As ManualResetEvent = Me.HandleCollection(tmpData.InvocationID)
                                                        If Not tmpHandle Is Nothing Then
                                                            tmpAnalyzer.AddPerformanceMark("Setting reply received flag for: " & tmpData.InvocationID)

                                                            If Not Me.DebugBroadcaster Is Nothing Then
                                                                Me.DebugBroadcaster.WriteLine("Setting reply received flag")
                                                                Me.DebugBroadcaster.WriteLine("******************************************************")
                                                                Me.DebugBroadcaster.Flush()

                                                            End If

                                                            tmpHandle.Set()

                                                        End If

                                                    Finally
                                                        If Not Me.Lock Is Nothing Then Monitor.Exit(Me.Lock)

                                                        tmpAnalyzer.AddPerformanceMark("Leaving reply buffer lock")

                                                    End Try

                                                Else
                                                    tmpAnalyzer.AddPerformanceMark("Data is not reply")

                                                    If Not Me.DebugBroadcaster Is Nothing Then
                                                        Me.DebugBroadcaster.WriteLine("Data is not reply")

                                                    End If

                                                    CallsReceivedPerformanceCounter.Increment()
                                                    Interlocked.Increment(_CallsReceived)

                                                    tmpAnalyzer.AddPerformanceMark("Updating CallsReceived counters")

                                                    If Not tmpData.Data Is Nothing AndAlso TypeOf tmpData.Data Is FileProvider Then
                                                        Dim tmpFileProvider As FileProvider = tmpData.Data

                                                        tmpFileProvider.Communicator = Me

                                                    End If

                                                    'Me.Receive()

                                                    'tmpReceiveRestarted = True

                                                    tmpReceivedDataList.Add(tmpData)

                                                    'Me.OnCallReceived(New EventArgs.CommunicatorCallReceivedEventArgs(tmpData))

                                                End If

                                                'If Not DiagBroadcaster Is Nothing Then
                                                '    Dim tmpDebugString As String = "<call>" & Me.Session.IDentifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                                '    DiagBroadcaster.Write(tmpDebugString)
                                                '    DiagBroadcaster.Write(Me.GetDebugString)

                                                'End If
                                                'tmpAnalyzer.AddPerformanceMark("<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>")

                                                If Not DiagLogWriter Is Nothing Then
                                                    Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                                    DiagLogWriter.WriteCallData(Me.Session.Identifier, tmpDebugString)
                                                    DiagLogWriter.UpdateSessionInfo(Me.Session.Identifier, Me.GetDebugString)

                                                End If
                                            End If
                                        End If

                                        If Not tmpStreamData Is Nothing Then tmpStreamData.Dispose()

                                        tmpAnalyzer.AddPerformanceMark("Closing stream")

                                        tmpEOSIndex = SocketUtility.IndexOfEOS(_ReceiveBuffer)

                                        tmpAnalyzer.AddPerformanceMark("Searching for EOS")

                                    End While

                                    If tmpReceivedDataList.Count > 0 Then
                                        Me.Receive()

                                        tmpAnalyzer.AddPerformanceMark("Starting new Receive thread")

                                        tmpReceiveRestarted = True

                                    End If

                                    For Each tmpData As CommunicatorDataObject In tmpReceivedDataList
                                        Me.OnCallReceived(New EventArgs.CommunicatorCallReceivedEventArgs(tmpData))

                                    Next

                                    tmpAnalyzer.AddPerformanceMark("Raising CallReceived events")

                                    tmpReceivedDataList.Clear()
                                    tmpReceivedDataList = Nothing

                                Else
                                    If Not Me.IsClientCommunicator Then
                                        SocketShutdownPerformanceCounter.Increment()
                                        Interlocked.Increment(_SocketShutDowns)

                                        Me.Dispose()

                                        tmpAnalyzer.AddPerformanceMark("Shutting communicator down")

                                    Else
                                        ConnectFailedPerformanceCounter.Increment()
                                        Interlocked.Increment(_ConnectFailed)

                                        Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

                                    End If
                                End If
                            End If
                        End If

                    Catch ex As System.Runtime.Serialization.SerializationException
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))
                        DeserializationErrorsPerformanceCounter.Increment()
                        Interlocked.Increment(_DeserializationErrors)

                        tmpAnalyzer.AddPerformanceMark("Catching SerializationException")

                        Me.OnInternalError(ex)

                        Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

                    Catch ex As System.Net.Sockets.SocketException
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                        Me.DisposeNetworkResources()

                        tmpAnalyzer.AddPerformanceMark("Catching SocketException")

                        Me.OnInternalError(ex)

                        If Not Me.IsClientCommunicator Then
                            'Wait until client reconnects

                        Else
                            Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

                        End If

                    Catch ex As Exception
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                        'Me.DisposeNetworkResources()

                        tmpAnalyzer.AddPerformanceMark("Catching Exception")

                        Me.OnInternalError(ex)

                        Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

                    Finally
                        If Not Me.IsDisposed Then
                            'If Me.IsClientCommunicator Then
                            If Not tmpReceiveRestarted Then
                                Me.Receive()
                                tmpAnalyzer.AddPerformanceMark("Starting new Receive thread")

                            End If
                            'End If
                        End If

                        If tmpAnalyzer.TotalDuration > TimeSpan.FromSeconds(1) Then
                            SocketUtility.SocketPerformanceLog.WriteEntry(tmpAnalyzer.GetDifferentialResult)

                        End If
                    End Try

                    Me.LogNetworkData(tmpAnalyzer.GetTimeStampResult())

                End Using
            End If


            If Not tmpReceiveData Is Nothing AndAlso Not tmpReceiveData Is Me._ReceiveData Then
                tmpReceiveData.Dispose()
            Else
                'tmpReceiveData.Clear()
            End If
        End Sub

        'Private Sub Socket_OnReceive(ByVal ar As IAsyncResult)
        '    If Not Me.IsDisposed Then
        '        Dim tmpReceiveRestarted As Boolean = False

        '        Using tmpAnalyzer As New Diagnostics.PerformanceAnalyzer("Communicator.Socket_OnReceive")
        '            Try
        '                tmpAnalyzer.AddPerformanceMark("Start")

        '                Dim tmpReceiveData As ReceiveData = ar.AsyncState

        '                If Not tmpReceiveData.Socket Is Nothing Then
        '                    Dim tmpBytesReceived As Integer = tmpReceiveData.Socket.EndReceive(ar)

        '                    tmpAnalyzer.AddPerformanceMark("Receiving data")

        '                    If Not Me.IsDisposed Then
        '                        If tmpBytesReceived > 0 Then
        '                            If Not Me.DebugBroadcaster Is Nothing Then
        '                                Me.DebugBroadcaster.WriteLine(tmpBytesReceived & " bytes received")

        '                            End If

        '                            Dim tmpCurrentBufferLength As Long = 0

        '                            If Not _ReceiveBuffer Is Nothing Then tmpCurrentBufferLength = _ReceiveBuffer.Length

        '                            BytesReceivedPerformanceCounter.Increment(tmpBytesReceived)
        '                            Interlocked.Add(_BytesReceived, tmpBytesReceived)

        '                            tmpAnalyzer.AddPerformanceMark("Updating BytesReceived counters")

        '                            Dim tmpReceiveBuffer(tmpCurrentBufferLength + tmpBytesReceived - 1) As Byte

        '                            If Not _ReceiveBuffer Is Nothing Then _ReceiveBuffer.CopyTo(tmpReceiveBuffer, 0)
        '                            Array.Copy(tmpReceiveData.Buffer, 0, tmpReceiveBuffer, tmpCurrentBufferLength, tmpBytesReceived)

        '                            If Not tmpReceiveData Is Me._ReceiveData Then
        '                                tmpReceiveData.Dispose()

        '                            Else
        '                                tmpReceiveData.Clear()

        '                            End If

        '                            _ReceiveBuffer = tmpReceiveBuffer

        '                            tmpAnalyzer.AddPerformanceMark("Moving data to receive buffer")

        '                            Dim tmpEOSIndex As Long = SocketUtility.IndexOfEOS(_ReceiveBuffer)

        '                            tmpAnalyzer.AddPerformanceMark("Searching for EOS")

        '                            If Not Me.DebugBroadcaster Is Nothing Then
        '                                If tmpEOSIndex = -1 Then
        '                                    Me.DebugBroadcaster.WriteLine("Partial chunk")

        '                                End If
        '                            End If

        '                            Dim tmpReceivedDataList As New ArrayList

        '                            While tmpEOSIndex <> -1 And Not Me.IsDisposed
        '                                If Not Me.DebugBroadcaster Is Nothing Then
        '                                    Me.DebugBroadcaster.WriteLine("Full chunk received")

        '                                End If

        '                                Dim tmpStreamData As StreamData = GetStream(tmpEOSIndex)

        '                                tmpAnalyzer.AddPerformanceMark("Extracting DataObject")

        '                                If Not tmpStreamData Is Nothing AndAlso Not tmpStreamData.Stream Is Nothing Then
        '                                    Dim tmpStream As System.IO.MemoryStream = tmpStreamData.Stream

        '                                    Dim tmpStreamSize As Long = tmpStream.Length

        '                                    If Not Me.DebugBroadcaster Is Nothing Then
        '                                        Me.DebugBroadcaster.WriteLine("Deserializing of object started")

        '                                    End If

        '                                    Dim tmpDeserializationStart As Integer = System.Environment.TickCount And Integer.MaxValue

        '                                    Dim tmpData As CommunicatorDataObject = CommunicatorDataObject.Deserialize(tmpStream)

        '                                    Dim tmpDeserializationEnd As Integer = System.Environment.TickCount And Integer.MaxValue

        '                                    tmpAnalyzer.AddPerformanceMark("Deserializing stream")

        '                                    TotalDeserializationTimePerformanceCounter.Increment(tmpDeserializationEnd - tmpDeserializationStart)
        '                                    Interlocked.Add(_TotalDeserializationTime, tmpDeserializationEnd - tmpDeserializationStart)

        '                                    tmpAnalyzer.AddPerformanceMark("Updating Deserializing counters")

        '                                    tmpStream.Dispose()

        '                                    tmpAnalyzer.AddPerformanceMark("Closing stream")

        '                                    If TypeOf tmpData.Data Is SocketInfo Then
        '                                        Dim tmpSocketInfo As SocketInfo = tmpData.Data

        '                                        Me.SocketInfo = tmpSocketInfo

        '                                        If Not Me.DebugBroadcaster Is Nothing Then
        '                                            Me.DebugBroadcaster.WriteLine("Data is SocketInfo")

        '                                        End If

        '                                        CallsReceivedPerformanceCounter.Increment()
        '                                        Interlocked.Increment(_CallsReceived)

        '                                        'If Not DiagBroadcaster Is Nothing Then
        '                                        '    Dim tmpDebugString As String = "<call>" & Me.Session.IDentifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
        '                                        '    DiagBroadcaster.Write(tmpDebugString)
        '                                        '    DiagBroadcaster.Write(Me.GetDebugString)

        '                                        'End If
        '                                        If Not DiagLogWriter Is Nothing Then
        '                                            Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
        '                                            DiagLogWriter.WriteCallData(Me.Session.Identifier, tmpDebugString)
        '                                            DiagLogWriter.UpdateSessionInfo(Me.Session.Identifier, Me.GetDebugString)

        '                                        End If

        '                                        tmpAnalyzer.AddPerformanceMark("Updating SocketInfo")

        '                                    Else
        '                                        tmpData.SetReceived()

        '                                        tmpAnalyzer.AddPerformanceMark("Setting received stamp")

        '                                        If Not Me.DebugBroadcaster Is Nothing Then
        '                                            Me.DebugBroadcaster.WriteLine("Deserializing of object finished")

        '                                            If TypeOf tmpData.Tag Is String Then
        '                                                Me.DebugBroadcaster.WriteLine("DataObject tag is: " & tmpData.Tag)

        '                                            End If
        '                                        End If

        '                                        If tmpData.IsReply Then
        '                                            tmpData.ReplyProcessingTime = Date.Now.Subtract(tmpData.Sent)

        '                                            tmpAnalyzer.AddPerformanceMark("Calculating reply time")

        '                                            If Not Me.DebugBroadcaster Is Nothing Then
        '                                                Me.DebugBroadcaster.WriteLine("Data is reply")
        '                                                Me.DebugBroadcaster.WriteLine("Reply time: " & tmpData.ReplyProcessingTime.ToString)

        '                                            End If

        '                                            Monitor.Enter(Me.Lock)

        '                                            tmpAnalyzer.AddPerformanceMark("Entering reply buffer lock")

        '                                            Me.Buffer(tmpData.InvocationID) = tmpData

        '                                            RepliesReceivedPerformanceCounter.Increment()
        '                                            Interlocked.Increment(_RepliesReceived)

        '                                            tmpAnalyzer.AddPerformanceMark("Updating RepliesReceived counters")

        '                                            Try
        '                                                Dim tmpHandle As ManualResetEvent = Me.HandleCollection(tmpData.InvocationID)
        '                                                If Not tmpHandle Is Nothing Then
        '                                                    If Not Me.DebugBroadcaster Is Nothing Then
        '                                                        Me.DebugBroadcaster.WriteLine("Setting reply received flag")
        '                                                        Me.DebugBroadcaster.WriteLine("******************************************************")
        '                                                        Me.DebugBroadcaster.Flush()

        '                                                    End If

        '                                                    tmpHandle.Set()

        '                                                End If

        '                                            Finally
        '                                                Monitor.Exit(Me.Lock)

        '                                                tmpAnalyzer.AddPerformanceMark("Leaving reply buffer lock")

        '                                            End Try

        '                                        Else
        '                                            If Not Me.DebugBroadcaster Is Nothing Then
        '                                                Me.DebugBroadcaster.WriteLine("Data is not reply")

        '                                            End If

        '                                            CallsReceivedPerformanceCounter.Increment()
        '                                            Interlocked.Increment(_CallsReceived)

        '                                            tmpAnalyzer.AddPerformanceMark("Updating CallsReceived counters")

        '                                            If Not tmpData.Data Is Nothing AndAlso TypeOf tmpData.Data Is FileProvider Then
        '                                                Dim tmpFileProvider As FileProvider = tmpData.Data

        '                                                tmpFileProvider.Communicator = Me

        '                                            End If

        '                                            'Me.Receive()

        '                                            'tmpReceiveRestarted = True

        '                                            tmpReceivedDataList.Add(tmpData)

        '                                            'Me.OnCallReceived(New EventArgs.CommunicatorCallReceivedEventArgs(tmpData))

        '                                        End If

        '                                        'If Not DiagBroadcaster Is Nothing Then
        '                                        '    Dim tmpDebugString As String = "<call>" & Me.Session.IDentifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
        '                                        '    DiagBroadcaster.Write(tmpDebugString)
        '                                        '    DiagBroadcaster.Write(Me.GetDebugString)

        '                                        'End If
        '                                        If Not DiagLogWriter Is Nothing Then
        '                                            Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$RECEIVED$" & tmpData.Tag & "$" & tmpData.InvocationID & "$" & tmpData.IsReply & "$" & tmpStreamSize & "$" & tmpStreamData.WasCompressed & "$" & tmpData.ReplyProcessingTime.TotalMilliseconds & "<@>"
        '                                            DiagLogWriter.WriteCallData(Me.Session.Identifier, tmpDebugString)
        '                                            DiagLogWriter.UpdateSessionInfo(Me.Session.Identifier, Me.GetDebugString)

        '                                        End If
        '                                    End If
        '                                End If

        '                                If Not tmpStreamData Is Nothing Then tmpStreamData.Dispose()

        '                                tmpAnalyzer.AddPerformanceMark("Closing stream")

        '                                tmpEOSIndex = SocketUtility.IndexOfEOS(_ReceiveBuffer)

        '                                tmpAnalyzer.AddPerformanceMark("Searching for EOS")

        '                            End While

        '                            If tmpReceivedDataList.Count > 0 Then
        '                                Me.Receive()

        '                                tmpAnalyzer.AddPerformanceMark("Starting new Receive thread")

        '                                tmpReceiveRestarted = True

        '                            End If

        '                            For Each tmpData As CommunicatorDataObject In tmpReceivedDataList
        '                                Me.OnCallReceived(New EventArgs.CommunicatorCallReceivedEventArgs(tmpData))

        '                            Next

        '                            tmpAnalyzer.AddPerformanceMark("Raising CallReceived events")

        '                            tmpReceivedDataList.Clear()
        '                            tmpReceivedDataList = Nothing

        '                        Else
        '                            If Not Me.IsClientCommunicator Then
        '                                SocketShutdownPerformanceCounter.Increment()
        '                                Interlocked.Increment(_SocketShutDowns)

        '                                Me.Dispose()

        '                                tmpAnalyzer.AddPerformanceMark("Shutting communicator down")

        '                            Else
        '                                ConnectFailedPerformanceCounter.Increment()
        '                                Interlocked.Increment(_ConnectFailed)

        '                                Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

        '                            End If
        '                        End If
        '                    End If
        '                End If

        '            Catch ex As System.Runtime.Serialization.SerializationException
        '                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))
        '                DeserializationErrorsPerformanceCounter.Increment()
        '                Interlocked.Increment(_DeserializationErrors)

        '                tmpAnalyzer.AddPerformanceMark("Catching SerializationException")

        '                Me.OnInternalError(ex)

        '                Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

        '            Catch ex As System.Net.Sockets.SocketException
        '                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

        '                Me.DisposeNetworkResources()

        '                tmpAnalyzer.AddPerformanceMark("Catching SocketException")

        '                Me.OnInternalError(ex)

        '                If Not Me.IsClientCommunicator Then
        '                    'Wait until client reconnects

        '                Else
        '                    Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

        '                End If

        '            Catch ex As Exception
        '                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

        '                'Me.DisposeNetworkResources()

        '                tmpAnalyzer.AddPerformanceMark("Catching Exception")

        '                Me.OnInternalError(ex)

        '                Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

        '            Finally
        '                If Not Me.IsDisposed Then
        '                    'If Me.IsClientCommunicator Then
        '                    If Not tmpReceiveRestarted Then
        '                        Me.Receive()
        '                        tmpAnalyzer.AddPerformanceMark("Starting new Receive thread")

        '                    End If
        '                    'End If
        '                End If

        '                If tmpAnalyzer.TotalDuration > TimeSpan.FromSeconds(1) Then
        '                    SocketUtility.SocketPerformanceLog.WriteEntry(tmpAnalyzer.GetDifferentialResult)

        '                End If
        '            End Try

        '        End Using

        '    End If
        'End Sub

        Private Function GetStream(ByVal endIndex As Long) As StreamData
            Dim tmpBuffer(endIndex - 1) As Byte

            Array.Copy(_ReceiveBuffer, tmpBuffer, endIndex)

            If _ReceiveBuffer.Length > endIndex + SocketUtility.EOSBytes.Length Then
                If Not Me.DebugBroadcaster Is Nothing Then
                    Me.DebugBroadcaster.WriteLine("Extracting stream from buffer: needs split")

                End If

                Dim tmpReceiveBuffer(_ReceiveBuffer.Length - endIndex - SocketUtility.EOSBytes.Length - 1) As Byte

                Array.Copy(_ReceiveBuffer, endIndex + SocketUtility.EOSBytes.Length, tmpReceiveBuffer, 0, tmpReceiveBuffer.Length)

                _ReceiveBuffer = tmpReceiveBuffer

            Else
                If Not Me.DebugBroadcaster Is Nothing Then
                    Me.DebugBroadcaster.WriteLine("Extracting stream from buffer")

                End If

                _ReceiveBuffer = Nothing

            End If

            'If SocketUtility.IndexOfPing(tmpBuffer) = -1 Then
            If True Then
                If tmpBuffer.Length = SocketUtility.DiagBytes.Length Then
                    If SocketUtility.IndexOfDiag(tmpBuffer) <> -1 Then
                        If Not Me.DebugBroadcaster Is Nothing Then
                            Me.DebugBroadcaster.WriteLine("Stream contains DIAG")

                        End If

                        'If DiagBroadcaster Is Nothing Then
                        '    DiagBroadcaster = New TCPBroadcaster(IPAddress.Parse("127.0.0.1"), 8888)
                        '    DiagBroadcaster.AutoFlush = True

                        'Else
                        '    DiagBroadcaster = Nothing

                        'End If
                        If DiagLogWriter Is Nothing Then
                            DiagLogWriter = New SocketLogWriter()

                        Else
                            DiagLogWriter = Nothing

                        End If

                        Return Nothing

                    End If
                End If

                If tmpBuffer.Length = SocketUtility.ShutdownBytes.Length Then
                    If SocketUtility.IndexOfShutdown(tmpBuffer) <> -1 Then
                        If Not Me.DebugBroadcaster Is Nothing Then
                            Me.DebugBroadcaster.WriteLine("Stream contains SHUTDOWN")

                        End If

                        Me.SocketShutdownPerformanceCounter.Increment()
                        Interlocked.Increment(_SocketShutDowns)

                        Me.Dispose()

                        Return Nothing

                    End If
                End If

                Dim tmpStream As New System.IO.MemoryStream(tmpBuffer)

                Dim hasBeenUncompressed As Boolean = False

                If SocketUtility.IsCompressedStream(tmpBuffer) Then
                    Dim tmpFinalBuffer(tmpBuffer.Length - SocketUtility.CompressedContentBytes.Length - 1) As Byte

                    Array.Copy(tmpBuffer, SocketUtility.CompressedContentBytes.Length, tmpFinalBuffer, 0, tmpFinalBuffer.Length)

                    tmpStream.Close()
                    tmpStream = New System.IO.MemoryStream(tmpFinalBuffer)

                    If Not Me.DebugBroadcaster Is Nothing Then
                        Me.DebugBroadcaster.WriteLine("Compression enabled - Deserializing/Decompressing stream")

                    End If

                    If TypeOf Me.StreamHandler Is CompressionStreamHandler Then
                        If Not Me.DebugBroadcaster Is Nothing Then
                            Me.DebugBroadcaster.WriteLine("StreamHandler is CompresseionStreamHandler")

                        End If

                        tmpStream = Me.StreamHandler.DeSerializeStream(tmpStream)

                    Else
                        If Not Me.DebugBroadcaster Is Nothing Then
                            Me.DebugBroadcaster.WriteLine("StreamHandler is not CompressionStreamHandler - creating new")

                        End If

                        Dim tmpCompressionhandler As New CompressionStreamHandler

                        tmpStream = tmpCompressionhandler.DeSerializeStream(tmpStream)

                    End If

                    If Not Me.DebugBroadcaster Is Nothing Then
                        Me.DebugBroadcaster.WriteLine("Stream is uncompressed")

                    End If

                    hasBeenUncompressed = True

                End If

                If Not Me.StreamHandler Is Nothing Then
                    If Not Me.DebugBroadcaster Is Nothing Then
                        Me.DebugBroadcaster.WriteLine("StreamHandler enabled - Deserializing stream")

                    End If

                    If TypeOf Me.StreamHandler Is CompressionStreamHandler Then
                        'If Not hasBeenUncompressed Then
                        '    Return Me.StreamHandler.DeSerializeStream(tmpStream)

                        'Else
                        Return New StreamData(hasBeenUncompressed, tmpStream)

                        'End If

                    Else
                        Return New StreamData(hasBeenUncompressed, Me.StreamHandler.DeSerializeStream(tmpStream))

                    End If

                Else
                    Return New StreamData(hasBeenUncompressed, tmpStream)

                End If

            Else
                If Not Me.DebugBroadcaster Is Nothing Then
                    Me.DebugBroadcaster.WriteLine("Stream contains PING")

                End If

                PingsReceivedPerformanceCounter.Increment()
                Interlocked.Increment(_PingsReceived)

                Return Nothing

            End If

        End Function

        Public Sub SendData(ByVal data As CommunicatorDataObject)
            Using tmpAnalyzer As New Diagnostics.PerformanceAnalyzer("Communicator.SendData")
                If Not Me.IsDisposed Then
                    tmpAnalyzer.AddPerformanceMark("Start")

                    Monitor.Enter(Me.Lock)

                    tmpAnalyzer.AddPerformanceMark("Enter lock")

                    Try
                        Dim tmpEventargs As New EventArgs.CommunicatorBeforeSendEventArgs(data, False)

                        Me.OnBeforeSend(Me, tmpEventargs)

                        tmpAnalyzer.AddPerformanceMark("Raising BeforeSend event")

                        If Not tmpEventargs.Cancel Then
                            Me.Connect()

                            tmpAnalyzer.AddPerformanceMark("Connecting")

                            If Not Me.Socket Is Nothing Then
                                If String.IsNullOrEmpty(data.InvocationID) Then data.InvocationID = Me.InvocationCounter
                                If Not data.IsReply Then data.Sent = Date.Now

                                tmpAnalyzer.AddPerformanceMark("Setting Packet IDs")

                                Dim tmpStream As System.IO.MemoryStream = New System.IO.MemoryStream()

                                Dim tmpSerializationStart As Integer = System.Environment.TickCount And Integer.MaxValue

                                'Write data
                                Dim tmpDataStream As System.IO.MemoryStream = data.Serialize

                                tmpAnalyzer.AddPerformanceMark("Serializing data")

                                Dim tmpSerializationEnd As Integer = System.Environment.TickCount And Integer.MaxValue

                                TotalSerializationTimePerformanceCounter.Increment(tmpSerializationEnd - tmpSerializationStart)
                                Interlocked.Add(_TotalSerializationTime, tmpSerializationEnd - tmpSerializationStart)

                                tmpAnalyzer.AddPerformanceMark("Updating serialization counters")

                                Dim tmpEndPosition As Long

                                Dim hasBeenCompressed As Boolean = False
                                Dim shouldCompress As Boolean = Me.CompressionEnabled

                                If Not shouldCompress Then
                                    shouldCompress = TypeOf Me.StreamHandler Is CompressionStreamHandler

                                End If

                                If data.Compress.HasValue Then
                                    shouldCompress = data.Compress

                                End If

                                tmpAnalyzer.AddPerformanceMark("Determining whether to compress data")

                                If Not Me.StreamHandler Is Nothing Then
                                    If TypeOf Me.StreamHandler Is CompressionStreamHandler Then
                                        If shouldCompress Then
                                            tmpEndPosition = tmpStream.Length
                                            tmpStream.SetLength(tmpStream.Length + SocketUtility.CompressedContentBytes.Length)
                                            tmpStream.Position = tmpEndPosition

                                            tmpStream.Write(SocketUtility.CompressedContentBytes, 0, SocketUtility.CompressedContentBytes.Length)

                                            tmpDataStream = Me.StreamHandler.SerializeStream(tmpDataStream)

                                            hasBeenCompressed = True

                                            tmpAnalyzer.AddPerformanceMark("Compressing data")

                                        End If

                                    Else
                                        tmpDataStream = Me.StreamHandler.SerializeStream(tmpDataStream)

                                        tmpAnalyzer.AddPerformanceMark("Serializing data with handler """ & Me.StreamHandler.GetType.Name & """")

                                    End If
                                End If

                                If shouldCompress And Not hasBeenCompressed Then
                                    tmpEndPosition = tmpStream.Length
                                    tmpStream.SetLength(tmpStream.Length + SocketUtility.CompressedContentBytes.Length)
                                    tmpStream.Position = tmpEndPosition

                                    tmpStream.Write(SocketUtility.CompressedContentBytes, 0, SocketUtility.CompressedContentBytes.Length)

                                    Dim tmpCompressionHandler As New CompressionStreamHandler

                                    tmpDataStream = tmpCompressionHandler.SerializeStream(tmpDataStream)

                                    tmpAnalyzer.AddPerformanceMark("Compressing data")

                                End If

                                Dim tmpDataBytes() As Byte = tmpDataStream.ToArray
                                tmpDataStream.Close()
                                tmpEndPosition = tmpStream.Length
                                tmpStream.SetLength(tmpStream.Length + tmpDataBytes.Length)
                                tmpStream.Position = tmpEndPosition

                                tmpStream.Write(tmpDataBytes, 0, tmpDataBytes.Length)

                                tmpAnalyzer.AddPerformanceMark("Writing data to stream")

                                'Write EOF
                                tmpEndPosition = tmpStream.Length
                                tmpStream.SetLength(tmpStream.Length + SocketUtility.EOSBytes.Length)
                                tmpStream.Position = tmpEndPosition

                                tmpStream.Write(SocketUtility.EOSBytes, 0, SocketUtility.EOSBytes.Length)

                                tmpAnalyzer.AddPerformanceMark("Writing EOF to stream")

                                'If data sent is a fileprovider then save it to serve future chunk request.
                                If Not data.Data Is Nothing Then
                                    If TypeOf data.Data Is FileProvider Then
                                        Dim tmpFileProvider As FileProvider = data.Data

                                        Me.FileProviders(tmpFileProvider.URI) = tmpFileProvider

                                        tmpAnalyzer.AddPerformanceMark("Saving FileProvider")

                                    End If
                                End If

                                Me.Socket.Send(tmpStream.ToArray)
                                Dim tmpBytesSent As Integer = tmpStream.Length

                                tmpAnalyzer.AddPerformanceMark("Sending data")

                                BytesSentPerformanceCounter.Increment(tmpBytesSent)
                                Interlocked.Add(_BytesSent, tmpBytesSent)

                                tmpAnalyzer.AddPerformanceMark("Updating BytesSent counters")

                                CallsPerformedPerformanceCounter.Increment()
                                Interlocked.Increment(_CallsPerformed)

                                tmpAnalyzer.AddPerformanceMark("Updating CallsPerformed counters")

                                If data.IsReply Then
                                    RepliesSentPerformanceCounter.Increment()
                                    Interlocked.Increment(_RepliesSent)

                                    data.ReplyProcessingTime = Date.Now.Subtract(data.Received)

                                    tmpAnalyzer.AddPerformanceMark("Updating RepliesSent counters and calculating reply time")

                                End If

                                'If Not DiagBroadcaster Is Nothing Then
                                '    Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$SENT$" & data.Tag & "$" & data.InvocationID & "$" & data.IsReply & "$" & tmpBytesSent & "$" & hasBeenCompressed & "$" & data.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                '    DiagBroadcaster.Write(tmpDebugString)
                                '    DiagBroadcaster.Write(Me.GetDebugString)

                                '    tmpAnalyzer.AddPerformanceMark("Writing debug info")

                                'End If
                                If Not DiagLogWriter Is Nothing Then
                                    Dim tmpDebugString As String = "<call>" & Me.Session.Identifier & "$" & Date.Now.ToString & "." & Date.Now.Millisecond & "$SENT$" & data.Tag & "$" & data.InvocationID & "$" & data.IsReply & "$" & tmpBytesSent & "$" & hasBeenCompressed & "$" & data.ReplyProcessingTime.TotalMilliseconds & "<@>"
                                    DiagLogWriter.WriteCallData(Me.Session.Identifier, tmpDebugString)
                                    DiagLogWriter.UpdateSessionInfo(Me.Session.Identifier, Me.GetDebugString)

                                    tmpAnalyzer.AddPerformanceMark("Writing debug info")

                                End If

                                tmpStream.Close()
                                tmpStream.Dispose()

                                tmpAnalyzer.AddPerformanceMark("Closing stream")

                            Else
                                Throw New Exceptions.CommunicatorNotConnectedException

                            End If
                        End If


                    Catch ex As System.Runtime.Serialization.SerializationException
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                        SerializationErrorsPerformanceCounter.Increment()
                        Interlocked.Increment(_SerializationErrors)

                        tmpAnalyzer.AddPerformanceMark("Catching SerializationException")

                        Me.OnInternalError(ex)

                        Throw New System.Runtime.Serialization.SerializationException("Error serializing the data.", ex)

                    Catch ex As Exceptions.CommunicatorNotConnectedException
                        tmpAnalyzer.AddPerformanceMark("Catching CommunicatorNotConnectedException")

                        Me.OnInternalError(ex)

                        Throw New Exceptions.CommunicatorNotConnectedException(ex)

                    Catch ex As SocketException
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                        Me.DisposeNetworkResources()

                        tmpAnalyzer.AddPerformanceMark("Catching SocketException")

                        Me.OnInternalError(ex)

                        Throw New Exceptions.CommunicatorNotConnectedException(ex)

                    Catch ex As Exception
                        BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets, "Error sending data."))

                        Me.DisposeNetworkResources()

                        tmpAnalyzer.AddPerformanceMark("Catching Exception")

                        Me.OnInternalError(ex)

                        Throw New Exception("Error sending data.", ex)

                    Finally
                        Monitor.Exit(Me.Lock)

                        tmpAnalyzer.AddPerformanceMark("Leaving lock")

                        If tmpAnalyzer.TotalDuration > TimeSpan.FromSeconds(1) Then
                            SocketUtility.SocketPerformanceLog.WriteEntry(tmpAnalyzer.GetDifferentialResult)

                        End If
                    End Try

                Else
                    Me.OnInternalError(New ObjectDisposedException("Communicator"))

                    Throw New ObjectDisposedException("Communicator")

                End If
            End Using

        End Sub

        Public Sub SendReply(ByVal originalData As CommunicatorDataObject, ByVal reply As CommunicatorDataObject)
            If Not Me.IsDisposed Then
                reply._IsReply = True
                reply.InvocationID = originalData.InvocationID

                If Me._Server Is Nothing OrElse Not Me._Server.IsStarted Then
                    If Not Me.DebugBroadcaster Is Nothing Then
                        If TypeOf reply.Tag Is String Then Me.DebugBroadcaster.WriteLine("Sending reply with tag: " & reply.Tag)
                        Me.DebugBroadcaster.WriteLine("Processing time: " & Date.Now.Subtract(originalData.Received).ToString)
                        Me.DebugBroadcaster.WriteLine("******************************************************")
                        Me.DebugBroadcaster.Flush()

                    End If

                    reply.SetReceived(originalData.Received)
                    reply.Sent = originalData.Sent

                    Me.SendData(reply)

                ElseIf Not originalData.Session Is Nothing Then
                    originalData.Session.Communicator.SendReply(originalData, reply)

                Else
                    Console.WriteLine("!!!!!!!!!!!!!!!! SendReply Session not found !!!!!!!!!!!!!!!!!!!")

                End If

            Else
                Me.OnInternalError(New ObjectDisposedException("Communicator"))

                Throw New ObjectDisposedException("Communicator")

            End If
        End Sub

        Public Function ReceiveReply(ByVal data As CommunicatorDataObject) As CommunicatorDataObject
            Return Me.ReceiveReply(data, TimeSpan.FromMilliseconds(-1))

        End Function

        Private Sub LogNetworkData(data As String)
            Try
                If Me.NetworkLoggingEnabled Then
                    Dim tmpLogFile As String = Path.Combine(My.Application.Info.DirectoryPath, "Logs\Network_" & Me.SocketInfo.FriendlyName & "_" & Date.Now.ToString("yyyyMMdd") & ".log")

                    IO.FileSystem.PrepareFilePath(tmpLogFile)

                    File.AppendAllText(tmpLogFile, data)
                End If

            Catch ex As Exception
                Me.OnInternalError(ex)
            End Try
        End Sub

        Public Function ReceiveReply(ByVal data As CommunicatorDataObject, ByVal timeout As TimeSpan) As CommunicatorDataObject
            Using tmpAnalyser As New PerformanceAnalyzer("Communicator.ReceiveReply")
                tmpAnalyser.AddPerformanceMark("Retrieving reply for: " & data.InvocationID)

                If Not Me.IsDisposed Then
                    Dim tmpHandle As ManualResetEvent = Me.GetHandle(data)

                    If tmpHandle.WaitOne(timeout, False) Then
                        tmpAnalyser.AddPerformanceMark("Reply is ready")

                        Dim tmpReply As CommunicatorDataObject = Me.Buffer(data.InvocationID)
                        Me.HandleCollection.Remove(data.InvocationID)
                        Me.Buffer.Remove(data.InvocationID)

                        ReplyBufferCountPerformanceCounter.Decrement()
                        Interlocked.Decrement(_ReplyBufferCount)

                        tmpHandle.Close()

                        ReplyHandleCountPerformanceCounter.Decrement()
                        Interlocked.Decrement(_ReplyHandleCount)

                        If TypeOf tmpReply.Data Is Exception Then
                            tmpAnalyser.AddPerformanceMark("Reply contains exception; throwing")
                            LogNetworkData(tmpAnalyser.GetTimeStampResult())

                            Throw New Exception("Exception thrown through Socket", CType(tmpReply.Data, Exception))

                        End If

                        Return tmpReply

                        tmpAnalyser.AddPerformanceMark("Returning reply")
                        LogNetworkData(tmpAnalyser.GetTimeStampResult())

                    Else
                        tmpAnalyser.AddPerformanceMark("Timeout waiting for reply")

                        Me.HandleCollection.Remove(data.InvocationID)

                        tmpHandle.Close()

                        ReplyHandleCountPerformanceCounter.Decrement()
                        Interlocked.Decrement(_ReplyHandleCount)

                        Me.OnInternalError(New TimeoutException("Reply has not been received within the specified timeout."))

                        tmpAnalyser.AddPerformanceMark("Throwing TimeoutException")
                        LogNetworkData(tmpAnalyser.GetTimeStampResult())

                        Throw New TimeoutException("Reply has not been received within the specified timeout.")

                    End If
                Else
                    Me.OnInternalError(New ObjectDisposedException("Communicator"))

                    tmpAnalyser.AddPerformanceMark("Communicator is disposed; throwing ObjectDisposedException")
                    LogNetworkData(tmpAnalyser.GetTimeStampResult())

                    Throw New ObjectDisposedException("Communicator")

                End If

            End Using

        End Function

        Public Sub AttachFileProvider(ByVal provider As FileProvider)
            If Not provider Is Nothing Then
                Me.FileProviders(provider.URI) = provider

            End If

        End Sub

        Public Sub DetachFileProvider(ByVal provider As FileProvider)
            If Not provider Is Nothing Then
                If Me.FileProviders.ContainsKey(provider.URI) Then
                    Me.FileProviders.Remove(provider.URI)

                End If
            End If

        End Sub

        Public Sub EnableDebugBroadcaster(ByVal port As Integer)
            Me.DebugBroadcaster = New UDPBroadcaster(port)
            'TEST
            'Me.DebugBroadcaster.AutoFlush = False
            Me.DebugBroadcaster.AutoFlush = True

        End Sub

        Public Sub DisableDebugBroadcaster()
            If Not Me.DebugBroadcaster Is Nothing Then
                Me.DebugBroadcaster.Dispose()

            End If

            Me.DebugBroadcaster = Nothing

        End Sub

        Private Sub Connect()
            Monitor.Enter(Me.Lock)

            Try
                If Me.IsClientCommunicator And Not Me.IsDisposed Then
                    Dim reconnect As Boolean = False

                    If Me.Socket Is Nothing Then
                        reconnect = True

                    Else
                        If Not Me.Socket.Connected Then
                            reconnect = True

                        End If
                    End If

                    If reconnect Then
                        Me.DisposeNetworkResources()

                        Me.Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

                        If Not String.IsNullOrEmpty(Me.RemoteServerName) Then
                            'Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1)
                            Me.Socket.Connect(Me.RemoteServerName, Me.RemoteServerPort)
                        Else
                            'Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1)
                            Me.Socket.Connect(Me.RemoteServerIP, Me.RemoteServerPort)
                        End If

                        Dim tmpStream As System.IO.MemoryStream = New System.IO.MemoryStream()

                        'Send IDentifier
                        If Not String.IsNullOrEmpty(Me.Identifier) Then
                            Dim tmpIDentifierBytes() As Byte = System.Text.Encoding.UTF8.GetBytes(Me.Identifier)
                            Array.Resize(Of Byte)(tmpIDentifierBytes, SocketUtility.IDENTIFIER_LENGTH)

                            tmpStream.SetLength(tmpIDentifierBytes.Length + SocketUtility.EOIBytes.Length)
                            tmpStream.Position = 0
                            tmpStream.Write(tmpIDentifierBytes, 0, tmpIDentifierBytes.Length)
                            tmpStream.Write(SocketUtility.EOIBytes, 0, SocketUtility.EOIBytes.Length)

                            Me.Socket.Send(tmpStream.ToArray)
                            Dim tmpBytesSent As Integer = tmpStream.Length

                            BytesSentPerformanceCounter.Increment(tmpBytesSent)
                            Interlocked.Add(_BytesSent, tmpBytesSent)

                            CallsPerformedPerformanceCounter.Increment()
                            Interlocked.Increment(_CallsPerformed)

                            tmpStream.Close()
                            tmpStream.Dispose()

                            Me.Receive()

                        End If

                        Dim tmpData As New CommunicatorDataObject
                        tmpData.Tag = "SOCKETINFO_CONNECT"
                        tmpData.Data = Me.SocketInfo

                        Me.SendData(tmpData)

                        Me.OnConnected(New System.EventArgs)

                    End If
                End If

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

                Me.DisposeNetworkResources()

                'Throw New Exception("Error in connecting to " & Me.RemoteServerName & vbCrLf & Me.RemoteServerPort & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
            Finally
                Monitor.Exit(Me.Lock)

            End Try
        End Sub

        Private Function GetHandle(ByVal data As CommunicatorDataObject) As ManualResetEvent
            Using tmpAnalyser As New PerformanceAnalyzer("Communicator.GetHandle")
                tmpAnalyser.AddPerformanceMark("Entering lock")

                Monitor.Enter(Me.Lock)

                Try
                    tmpAnalyser.AddPerformanceMark("Searching for handle: " & data.InvocationID)

                    Dim tmpHandle As ManualResetEvent = Me.HandleCollection(data.InvocationID)

                    If tmpHandle Is Nothing Then
                        tmpAnalyser.AddPerformanceMark("Handle not found; creating new")

                        tmpHandle = New ManualResetEvent(False)
                        Me.HandleCollection(data.InvocationID) = tmpHandle

                    Else
                        tmpAnalyser.AddPerformanceMark("Handle found")
                    End If

                    tmpAnalyser.AddPerformanceMark("Searching for existing reply")

                    If Me.Buffer.ContainsKey(data.InvocationID) Then
                        tmpAnalyser.AddPerformanceMark("Reply found; setting handle")
                        tmpHandle.Set()

                    Else
                        tmpAnalyser.AddPerformanceMark("No reply found")
                    End If

                    ReplyHandleCountPerformanceCounter.Increment()
                    Interlocked.Increment(_ReplyHandleCount)

                    tmpAnalyser.AddPerformanceMark("Returning handle")

                    Return tmpHandle

                Finally
                    tmpAnalyser.AddPerformanceMark("Exiting lock")

                    Monitor.Exit(Me.Lock)

                    Me.LogNetworkData(tmpAnalyser.GetTimeStampResult())
                End Try
            End Using

        End Function

        Friend Sub ReplaceSocket(ByVal socket As Socket)
            Me.SocketReplacementsPerformanceCounter.Increment()

            Me.DisposeNetworkResources()

            Me.Socket = socket

            Me.Receive()

        End Sub

        Private Sub OnConnected(ByVal e As System.EventArgs)
            RaiseEvent Connected(Me, e)

        End Sub

        Friend Sub OnCallReceived(ByVal e As EventArgs.CommunicatorCallReceivedEventArgs)
            If LogCalls Then Console.WriteLine("NET CALL> " & Me.Session.SocketInfo.Address.ToString() & " | " & e.Data.Tag)

            If Not e.Data.Data Is Nothing AndAlso TypeOf e.Data.Data Is FileProviderCommand Then
                Dim tmpCommand As FileProviderCommand = e.Data.Data

                If Not Me.DebugBroadcaster Is Nothing Then
                    Me.DebugBroadcaster.WriteLine("FileProvider command received: " & tmpCommand.Command.ToString)

                End If

                Dim tmpFileProvider As FileProvider = Me.FileProviders(tmpCommand.FileProviderURI)

                If Not tmpFileProvider Is Nothing Then
                    Select Case tmpCommand.Command
                        Case FileProviderCommand.Commands.GetNextPortion
                            Dim tmpReplyData As New CommunicatorDataObject

                            Try
                                If Not tmpCommand.FileChunkRequest Is Nothing Then
                                    tmpReplyData.Tag = "FileChunkRequest_REPLY"

                                    tmpReplyData.Data = tmpFileProvider.GetNextPortion(tmpCommand.FileChunkRequest.Offset, tmpCommand.FileChunkRequest.ChunkSize)

                                    Me.SendReply(e.Data, tmpReplyData)

                                Else
                                    tmpReplyData.Tag = "FileChunkRequest_REPLY"

                                    tmpReplyData.Data = New Exception("FileChunkRequest is Nothing.")

                                    Me.SendReply(e.Data, tmpReplyData)

                                End If

                            Catch ex As Exception
                                tmpReplyData.Tag = "FileChunkRequest_REPLY"

                                tmpReplyData.Data = ex

                                Me.OnInternalError(ex)

                                Me.SendReply(e.Data, tmpReplyData)

                            End Try

                        Case FileProviderCommand.Commands.DownloadComplete
                            tmpFileProvider.DownloadComplete()

                            Me.DetachFileProvider(tmpFileProvider)

                        Case FileProviderCommand.Commands.Abort
                            tmpFileProvider.Abort()

                            Me.DetachFileProvider(tmpFileProvider)

                    End Select

                End If

            Else
                SocketUtility.SetCurrentSession(Me.Session)

                If Not e Is Nothing Then
                    SocketUtility.SetCurrentCallData(e.Data)

                End If

                RaiseEvent CallReceived(Me, e)

                If Not e Is Nothing Then
                    Me.RemoveActiveCall(e.Data)

                End If
            End If

            If Not Me.DebugBroadcaster Is Nothing Then
                Me.DebugBroadcaster.WriteLine("******************************************************")
                Me.DebugBroadcaster.Flush()

            End If

        End Sub

        Friend Sub OnBeforeSend(ByVal sender As Object, ByVal e As EventArgs.CommunicatorBeforeSendEventArgs)
            RaiseEvent BeforeSend(sender, e)

        End Sub

        Private Sub OnInternalError(ByVal ex As Exception)
            Interlocked.Increment(_InternalErrors)

            'If Not DiagBroadcaster Is Nothing Then
            '    DiagBroadcaster.Write(Me.GetErrorString(ex))
            '    DiagBroadcaster.Write(Me.GetDebugString)

            'End If
            If Not DiagLogWriter Is Nothing Then
                DiagLogWriter.WriteErrorData(Me.Identifier, Me.GetErrorString(ex))
                DiagLogWriter.UpdateSessionInfo(Me.Identifier, Me.GetDebugString)

            End If

        End Sub

        Private Sub AddActiveCall(ByVal data As CommunicatorDataObject)
            Monitor.Enter(ActiveCallLock)

            Try
                ActiveCallDetails.Add(data)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

            Finally
                Monitor.Exit(ActiveCallLock)

            End Try
        End Sub

        Private Sub RemoveActiveCall(ByVal data As CommunicatorDataObject)
            Monitor.Enter(ActiveCallLock)

            Try
                ActiveCallDetails.Remove(data)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

            Finally
                Monitor.Exit(ActiveCallLock)

            End Try
        End Sub

        Private Sub SocketInfo_Changed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SocketInfo.Changed
            Try
                Dim tmpData As New CommunicatorDataObject
                tmpData.Tag = "SOCKETINFO_UPDATE"
                tmpData.Data = Me.SocketInfo

                Me.SendData(tmpData)

            Catch ex As Exception
                'SILENT

                Me.OnInternalError(ex)

            End Try
        End Sub

        Public Sub Dispose()
            If Not Me.IsDisposed Then
                Me._IsDisposed = True

                Me.Shutdown()
                Me.DisposeNetworkResources()

                If Not Me._ReceiveData Is Nothing Then
                    Me._ReceiveData.Dispose()
                    Me._ReceiveData = Nothing

                End If

                Me.AsyncSocketCallback = Nothing

                Me._RemoteServerIP = Nothing
                Me._RemoteServerPort = Nothing
                Me._Identifier = Nothing
                Me._Server = Nothing
                Me._Buffer = Nothing
                Me._Lock = Nothing
                Me._HandleCollection = Nothing
                Me._InvocationCounter = Nothing

                Me.OnDisposing(New System.EventArgs)

            End If

        End Sub

        Private Sub Shutdown()
            Try
                If Me.IsClientCommunicator Then
                    Dim tmpStream As System.IO.MemoryStream = New System.IO.MemoryStream()
                    tmpStream.SetLength(SocketUtility.ShutdownBytes.Length + SocketUtility.EOSBytes.Length)
                    tmpStream.Position = 0
                    tmpStream.Write(SocketUtility.ShutdownBytes, 0, SocketUtility.ShutdownBytes.Length)
                    tmpStream.Write(SocketUtility.EOSBytes, 0, SocketUtility.EOSBytes.Length)

                    Me.Socket.Send(tmpStream.ToArray)
                    Dim tmpBytesSent As Integer = tmpStream.Length

                    BytesSentPerformanceCounter.Increment(tmpBytesSent)
                    Interlocked.Add(_BytesSent, tmpBytesSent)

                    CallsPerformedPerformanceCounter.Increment()
                    Interlocked.Increment(_CallsPerformed)

                    tmpStream.Close()
                    tmpStream.Dispose()

                End If

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

                Me.OnInternalError(ex)

            End Try
        End Sub

        Private Sub DisposeNetworkResources()
            Try
                If Not Me.Socket Is Nothing Then
                    'Me.Socket.Shutdown(SocketShutdown.Both)
                    Me.Socket.Disconnect(False)
                    Me.Socket.Close()

                End If

            Catch ex As Exception
                'SILENT

                Me.OnInternalError(ex)

            Finally
                Me.Socket = Nothing

            End Try
        End Sub

        Private Sub OnDisposing(ByVal e As System.EventArgs)
            RaiseEvent Disposing(Me, e)

        End Sub

    End Class

End Namespace