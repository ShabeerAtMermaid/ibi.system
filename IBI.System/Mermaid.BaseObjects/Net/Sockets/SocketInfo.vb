﻿Imports System.Runtime.Serialization
Imports System.Reflection

Namespace Net.Sockets

    <System.Serializable()> Public Class SocketInfo
        Implements ISerializable, IDeserializationCallback

#Region "Variables"

        Private _FriendlyName As String

#End Region

#Region "Properties"

        Public Property FriendlyName() As String
            Get
                Return Me._FriendlyName

            End Get
            Set(ByVal value As String)
                If IsDirty(Me._FriendlyName, value) Then
                    Me._FriendlyName = value

                    Me.OnChanged(New System.EventArgs)

                End If

            End Set
        End Property

#End Region

#Region "Events"

        Public Event Changed As System.EventHandler

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            ' Instantiate base class
            MyBase.New()

            ' Record the Info object between the constructor and the deserialization callback
            ' because if we set the values of fields that are declared with
            ' an initializer, the initializer will come along and overwrite the value
            ' after this constructor has been called
            mInfo = info

        End Sub

        Private Sub OnChanged(ByVal e As System.EventArgs)
            RaiseEvent Changed(Me, e)

        End Sub

#Region "Serialization Implementation"

        'This implementation is added since handler to this Objects event is Serialized also by default.
        <NonSerialized()> Private mInfo As SerializationInfo

        Protected Overridable Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext) Implements ISerializable.GetObjectData
            ' Use the shared method to populate the SerializationInfo object
            SocketInfo.SerializeObject(Me, info)

        End Sub

        Protected Overridable Sub OnDeserialization(ByVal sender As Object) Implements System.Runtime.Serialization.IDeserializationCallback.OnDeserialization

            ' Call the shared method to deserialize the object
            SocketInfo.DeserializeObject(Me, mInfo)

            ' Kill the recorded info object
            mInfo = Nothing

        End Sub

        Public Shared Sub SerializeObject(ByVal obj As Object, ByVal info As SerializationInfo)
            ' Local Variables
            Dim aMembersToSerialize As MemberInfo()

            ' Error handler
            Try

                ' Get a list of all fields in this object and derived objects
                ' that are to be serialized
                aMembersToSerialize = SocketInfo.GetSerializableMembers(obj.GetType)

                '  Loop around all fields and save their values
                For Each objMember As MemberInfo In aMembersToSerialize

                    ' It is valid to serialize if it is in this array
                    ' Derived Fields with the same name as base fields
                    ' will automaticall be handled, so we don't need to
                    ' worry about duplicates

                    ' Determine if it is a filed or property
                    If objMember.MemberType = MemberTypes.Field Then

                        Dim objField As FieldInfo = DirectCast(objMember, FieldInfo)
                        info.AddValue(objField.Name, objField.GetValue(obj))

                    ElseIf objMember.MemberType = MemberTypes.Property Then

                        Dim objProperty As PropertyInfo = DirectCast(objMember, PropertyInfo)
                        info.AddValue(objProperty.Name, objProperty.GetValue(obj, Nothing))

                    End If

                Next

            Catch ex As Exception

                ' Trace any errors
                Trace.WriteLine("Error during serialization: " & ex.Message)
                Throw New SerializationException("Error during Serialization. ", ex)

            End Try

        End Sub

        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, Assertion:=True, SerializationFormatter:=True)> _
        Public Shared Sub DeserializeObject(ByVal obj As Object, ByVal info As SerializationInfo)
            ' Local Variables
            Dim aFieldsToDeserialize As MemberInfo()
            Dim aValues As Object()

            ' Error handler
            Try
                ' Get a list of all fields in this object and derived objects
                aFieldsToDeserialize = SocketInfo.GetSerializableMembers(obj.GetType)

                ' Loop around all fields and get their values from the info object
                aValues = DirectCast(Array.CreateInstance(GetType(Object), aFieldsToDeserialize.Length), Object())

                For nCount As Integer = 0 To aFieldsToDeserialize.Length - 1

                    ' Determine if it is a field or property
                    If aFieldsToDeserialize(nCount).MemberType = MemberTypes.Field Then

                        ' get the field
                        Dim objField As FieldInfo = DirectCast(aFieldsToDeserialize(nCount), FieldInfo)

                        ' Get the value of the field from the info object
                        aValues(nCount) = info.GetValue(objField.Name, objField.FieldType)

                    ElseIf aFieldsToDeserialize(nCount).MemberType = MemberTypes.Property Then

                        ' get the property
                        Dim objProperty As PropertyInfo = DirectCast(aFieldsToDeserialize(nCount), PropertyInfo)

                        ' Get the value of the field from the info object
                        aValues(nCount) = info.GetValue(objProperty.Name, objProperty.PropertyType)

                    End If

                Next

                ' Now use formatter services to populate this object's fields
                FormatterServices.PopulateObjectMembers(obj, aFieldsToDeserialize, aValues)

            Catch ex As Exception

                ' Trace errors
                Trace.WriteLine("Error during deserialization: " & ex.Message)
                Throw New SerializationException("Error during deserialization", ex)

            End Try

        End Sub

        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, Assertion:=True, SerializationFormatter:=True)> _
        Private Shared Function GetSerializableMembers(ByVal PersistableType As System.Type) As MemberInfo()

            ' Local Variables
            Dim aAllMembers() As System.Reflection.MemberInfo
            Dim aSerilizableMembers As System.Collections.ArrayList

            '  Get all the serializable members
            aAllMembers = FormatterServices.GetSerializableMembers(PersistableType)

            aSerilizableMembers = New System.Collections.ArrayList

            ' Filter non-serializable fields and event delegates
            For Each objMember As MemberInfo In aAllMembers

                ' We're only interested in fields and properties
                If objMember.MemberType = MemberTypes.Field Then

                    ' get the field
                    Dim objField As FieldInfo = DirectCast(objMember, FieldInfo)

                    ' If it has a NonSerialized Attribute or if it is an event delegate, skip it
                    If (Not GetType(System.Delegate).IsAssignableFrom(objField.FieldType)) Then

                        ' Add the field as it is valid for serialization
                        aSerilizableMembers.Add(objField)

                    End If

                ElseIf objMember.MemberType = MemberTypes.Property Then

                    ' get the property
                    Dim objProperty As PropertyInfo = DirectCast(objMember, PropertyInfo)

                    ' if it is an event delegate, skip it
                    If (Not GetType(System.Delegate).IsAssignableFrom(objProperty.PropertyType)) Then

                        ' Add the field as it is valid for serialization
                        aSerilizableMembers.Add(objProperty)

                    End If

                End If

            Next objMember


            ' Return the array of persistable fields - need to convert to an array first
            Return DirectCast(aSerilizableMembers.ToArray(GetType(MemberInfo)), MemberInfo())

        End Function

#End Region

    End Class

End Namespace