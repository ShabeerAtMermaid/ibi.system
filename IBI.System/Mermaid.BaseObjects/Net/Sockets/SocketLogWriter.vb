﻿Imports mermaid.BaseObjects.Threading
Imports System.IO

Namespace Net.Sockets

    Public Class SocketLogWriter

#Region "Variables"

        Private _LogDirectory As String = Path.Combine(My.Application.Info.DirectoryPath, "Logs")

        Private _Lock As New Object

#End Region

#Region "Properties"

        Public Property LogDirectory() As String
            Get
                Return _LogDirectory

            End Get
            Set(ByVal value As String)
                _LogDirectory = value

                IO.FileSystem.DeleteDirectory(LogDirectory)
                IO.FileSystem.PrepareDirectoryPath(LogDirectory)

            End Set
        End Property

        Private ReadOnly Property Lock() As Object
            Get
                Return _Lock

            End Get
        End Property

#End Region

#Region "Performance Counters"

        Private _QueuePerformanceCounter As Diagnostics.PerformanceCounter

        Private ReadOnly Property QueuePerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return _QueuePerformanceCounter

            End Get
        End Property

#End Region

        Public Sub New()
            MyBase.New()

            IO.FileSystem.DeleteDirectory(LogDirectory)
            IO.FileSystem.PrepareDirectoryPath(LogDirectory)

            InitializePerformanceCounters()

        End Sub

        Private Sub InitializePerformanceCounters()
            _QueuePerformanceCounter = Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Socket LogWriter Queue")

        End Sub

        Public Sub UpdateSessionInfo(ByVal sessionID As String, ByVal data As String)
            On Error Resume Next

            QueuePerformanceCounter.Increment()

            Using New Lock(Lock, True)
                File.WriteAllText(Path.Combine(LogDirectory, "SessionInfo_" & sessionID & ".log"), data)

            End Using

            QueuePerformanceCounter.Decrement()

        End Sub

        Public Sub WriteCallData(ByVal sessionID As String, ByVal data As String)
            On Error Resume Next

            QueuePerformanceCounter.Increment()

            Using New Lock(Lock, True)
                File.AppendAllText(Path.Combine(LogDirectory, "CallData_" & sessionID & ".log"), data)

            End Using

            QueuePerformanceCounter.Decrement()

        End Sub

        Public Sub WriteErrorData(ByVal sessionID As String, ByVal data As String)
            On Error Resume Next

            QueuePerformanceCounter.Increment()

            Using New Lock(Lock, True)
                File.AppendAllText(Path.Combine(LogDirectory, "ErrorData_" & sessionID & ".log"), data)

            End Using

            QueuePerformanceCounter.Decrement()

        End Sub

    End Class

End Namespace