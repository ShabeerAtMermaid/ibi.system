﻿Imports mermaid.BaseObjects.Threading
Imports mermaid.BaseObjects.Diagnostics

Namespace Net.Sockets

    Public Class SocketBufferPool

#Region "Properties"

        Private Shared Property AvailableBuffers As List(Of Byte()) = New List(Of Byte())
       
        Private Shared Property AccessLock As Object = New Object

        Private Shared ReadOnly Property AllocatedBuffersPerformanceCounter As PerformanceCounter
            Get
                Return PerformanceCounterManager.GetPerformanceCounter("Socket buffers allocated")
            End Get
        End Property

        Private Shared ReadOnly Property AvailableBuffersPerformanceCounter As PerformanceCounter
            Get
                Return PerformanceCounterManager.GetPerformanceCounter("Socket buffers available")
            End Get
        End Property

        Private Shared ReadOnly Property ReusedBuffersPerformanceCounter As PerformanceCounter
            Get
                Return PerformanceCounterManager.GetPerformanceCounter("Socket buffers reused")
            End Get
        End Property

#End Region

        Public Shared Sub AllocateBuffers(ByVal count As Integer)
            Using New Lock(AccessLock, True)
                For i As Integer = 1 To count
                    AvailableBuffers.Add(Array.CreateInstance(GetType(Byte), SocketUtility.BufferSize))
                    AllocatedBuffersPerformanceCounter.Increment()
                    AvailableBuffersPerformanceCounter.Increment()
                Next
            End Using

        End Sub

        Public Shared Sub ReuseBuffer(ByVal context As String, ByVal buffer As Byte())
            Using New Lock(AccessLock, True)
                Array.Clear(buffer, 0, buffer.Length)

                AvailableBuffers.Add(buffer)

                ReusedBuffersPerformanceCounter.Increment()
                AvailableBuffersPerformanceCounter.Increment()
            End Using

        End Sub

        Public Shared Function GetBuffer(ByVal context As String) As Byte()
            Using New Lock(AccessLock, True)
                If AvailableBuffers.Count = 0 Then AllocateBuffers(1)

                Dim tmpBuffer As Byte() = AvailableBuffers(0)

                AvailableBuffers.RemoveAt(0)

                AvailableBuffersPerformanceCounter.Decrement()

                Return tmpBuffer

            End Using

        End Function

    End Class

End Namespace