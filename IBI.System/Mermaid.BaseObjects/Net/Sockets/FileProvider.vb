Imports mermaid.BaseObjects.Net.Sockets.Interfaces
Imports mermaid.BaseObjects.EventArgs
Imports mermaid.BaseObjects.Exceptions
Imports mermaid.BaseObjects.IO
Imports System.Runtime.Serialization
Imports System.Reflection
Imports System.Threading
Imports System.Text
Imports System.IO

Namespace Net.Sockets

    ''' <summary>
    ''' Is used to fetch files in small chunks.
    ''' </summary>
    ''' <remarks></remarks>
    <System.Serializable()> Public Class FileProvider
        Implements ISerializable, IDeserializationCallback

#Region "Attributes"

        <NonSerialized()> Private _Communicator As Communicator
        Private _LocalPath As String
        <NonSerialized()> Private _File As FileInfo
        Private _Path As String
        Private _URI As String
        Private _TotalSize As Long
        <NonSerialized()> Private _CurrentOffset As Long
        Private _MD5 As FileHash
        <NonSerialized()> Private _AsyncProgressEvents As Boolean
        <NonSerialized()> Private _Aborted As Boolean
        <NonSerialized()> Private _DownloadFinished As Boolean
        <NonSerialized()> Private _TimedOut As Boolean
        Private _TimeOut As TimeSpan = New TimeSpan(0, 1, 0)
        <NonSerialized()> Private WithEvents _TmrTimeOut As Timers.Timer

        <NonSerialized()> Private _CompletionResetEvent As ManualResetEvent

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Friend Property Communicator() As Communicator
            Get
                Return Me._Communicator

            End Get
            Set(ByVal value As Communicator)
                Me._Communicator = value

            End Set
        End Property

        ''' <summary>
        ''' Information about the file to provide as a download.
        ''' </summary>
        ''' <value><see cref="FileInfo"/> Object of the file.</value>
        ''' <returns><see cref="FileInfo"/> with information about the file.</returns>
        ''' <remarks></remarks>
        Private Property File() As FileInfo
            Get
                Return Me._File

            End Get
            Set(ByVal Value As FileInfo)
                Me._File = Value

            End Set
        End Property

        ''' <summary>
        ''' Relative path to the file.
        ''' </summary>
        ''' <value>String containing the path.</value>
        ''' <returns>Return the relative path.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Path() As String 'Implements IFileProvider.Path
            Get
                Return Me._Path

            End Get
        End Property

        ''' <summary>
        ''' The URI to identify this provider.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property URI() As String
            Get
                Return Me._URI

            End Get
        End Property

        ''' <summary>
        ''' The total size of the file.
        ''' </summary>
        ''' <value>The filesize.</value>
        ''' <returns>Returns the size of the file.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TotalSize() As Long 'Implements IFileProvider.TotalSize
            Get
                Return Me._TotalSize

            End Get
        End Property

        ''' <summary>
        ''' Offset controlling where to read in the file.
        ''' </summary>
        ''' <value><see cref="Long"/> defining the offset.</value>
        ''' <returns>Returns the offset.</returns>
        ''' <remarks></remarks>
        Private Property CurrentOffset() As Integer
            Get
                Return Me._CurrentOffset

            End Get
            Set(ByVal Value As Integer)
                Me._CurrentOffset = Value

            End Set
        End Property

        ''' <summary>
        ''' MD5 checksum of the file.
        ''' </summary>
        ''' <value><see cref="FileHash"/> specifying the MD5.</value>
        ''' <returns>Returns a <see cref="FileHash"/> specifying the MD5.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property MD5() As FileHash 'Implements IFileProvider.MD5
            Get
                Return Me._MD5

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether calls to a registered <see cref="DownloadProgressHandler"/> is done async.
        ''' </summary>
        ''' <value>Boolean defining whether DownloadProgress events are raised async.</value>
        ''' <returns>Return whether DownloadProgress events are raised async.</returns>
        ''' <remarks></remarks>
        Public Property AsyncProgressEvents() As Boolean
            Get
                Return Me._AsyncProgressEvents

            End Get
            Set(ByVal Value As Boolean)
                Me._AsyncProgressEvents = Value

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether the download has been aborted.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has been aborted.</value>
        ''' <returns>Return whether the download has been aborted.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Aborted() As Boolean
            Get
                Return Me._Aborted

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has finished sucessfully.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has finished sucessfully.</value>
        ''' <returns>Return whether the download has finished sucessfully.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DownloadFinished() As Boolean
            Get
                Return Me._DownloadFinished

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has timed out.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TimedOut() As Boolean
            Get
                Return Me._TimedOut

            End Get
        End Property

        ''' <summary>
        ''' The latest <see cref="Date"/> the file has been modified.
        ''' </summary>      
        ''' <value>Last modified date.</value>
        ''' <returns>Returns the last modified date.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Modified() As Date
            Get
                If Me.File Is Nothing Then
                    Throw New FileNotFoundException("The file provided doesn't exist.")

                End If

                If Not Me.File.Exists Then
                    Throw New FileNotFoundException("The file provided doesn't exist.", Me.File.FullName)

                End If

                Return Me.File.LastWriteTime

            End Get
        End Property

        ''' <summary>
        ''' The time the download can be inactive before a DownloadTimeOut Event is raised.
        ''' </summary>
        ''' <value>TimeSpan defining the Time Out.</value>
        ''' <returns>Return the effective Time Out.</returns>
        ''' <remarks></remarks>
        Public Property TimeOut() As TimeSpan
            Get
                Return Me._TimeOut

            End Get
            Set(ByVal value As TimeSpan)
                Me._TimeOut = value

                Me.TmrTimeOut.Interval = value.TotalMilliseconds

            End Set
        End Property

        Public ReadOnly Property CurrentStatus() As String
            Get
                Return Me.CurrentOffset & " of " & Me.TotalSize & " sent"

            End Get
        End Property

        ''' <summary>
        ''' The Timer controlling the Time Out.
        ''' </summary>
        ''' <value><see cref="Timers.Timer" /> controlling the Time Out of the download.</value>
        ''' <returns>Returns the Time Out <see cref="Timers.Timer" />.</returns>
        ''' <remarks>Never return Nothing.</remarks>
        Private ReadOnly Property TmrTimeOut() As Timers.Timer
            Get
                If Me._TmrTimeOut Is Nothing Then
                    Me._TmrTimeOut = New Timers.Timer
                    Me._TmrTimeOut.Interval = Me.TimeOut.TotalMilliseconds

                End If

                Return Me._TmrTimeOut

            End Get
        End Property

        ''' <summary>
        ''' Reset event the user can use to wait for completion (or error or abort or....)
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Property CompletionResetEvent() As ManualResetEvent
            Get
                Return Me._CompletionResetEvent

            End Get
            Set(ByVal value As ManualResetEvent)
                Me._CompletionResetEvent = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

#Region "Events"

        ''' <summary>
        ''' Event that is raised when a portion of a file is downloaded.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadProgress As EventHandler(Of EventArgs.DownloadProgressEventArgs)

        ''' <summary>
        ''' Event that is raised when the download has finished.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadCompleted As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download has timed out.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadTimeOut As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download is aborted.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadAborted As System.EventHandler

#End Region

        ''' <summary>
        ''' Creates a new <see cref="FileProvider"/> with the specified file.
        ''' </summary>
        ''' <param name="localpath">Path to the file to provide.</param>
        ''' <param name="remotePath">Path to save the file when downloaded.</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal localpath As String, ByVal remotePath As String)
            MyBase.New()

            Me._LocalPath = localpath
            Me.File = New FileInfo(localpath)
            Me._Path = remotePath

            If Not Me.File.Exists Then
                Throw New FileNotFoundException("The file provided doesn't exist.", localpath)

            End If

            Me._MD5 = FileHash.FromFile(Me.File, True, False)

            Me._URI = Me.GenerateURI
            Me._TotalSize = Me.File.Length

            Me.TmrTimeOut.Enabled = True

        End Sub

        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            ' Instantiate base class
            MyBase.New()

            ' Record the Info object between the constructor and the deserialization callback
            ' because if we set the values of fields that are declared with
            ' an initializer, the initializer will come along and overwrite the value
            ' after this constructor has been called
            mInfo = info

        End Sub

        Public Sub AttachCommunicator(ByVal communicator As Communicator)
            If Not communicator Is Nothing Then
                Me.Communicator = communicator

            End If
        End Sub

        ''' <summary>
        '''     Returns the next portion or null if the entire file was downloaded.
        ''' </summary>
        ''' <returns>Next portion or null.</returns>
        ''' <remarks>
        ''' </remarks>
        Public Function GetNextPortion(ByVal offset As Long, ByVal portionSize As Integer) As Byte() 'Implements IFileProvider.GetNextPortion
            Me.ResetTimeOutTimer()

            If Me.Aborted Then
                Me.OnDownloadAborted(New System.EventArgs)

                Throw New DownloadAbortedException()

            End If

            If Me.Communicator Is Nothing Then
                If Me.File Is Nothing Then
                    Me.File = New FileInfo(_LocalPath)
                End If

                If offset >= Me.TotalSize Then Return Nothing

                Dim sizeToSend As Integer = Math.Min(Me.TotalSize - offset, portionSize)

                Dim tmpStream As Stream = Me.File.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
                Dim buffer(sizeToSend - 1) As Byte

                Try
                    tmpStream.Position = offset
                    tmpStream.Read(buffer, 0, buffer.Length)

                Finally
                    tmpStream.Dispose()

                End Try

                offset += sizeToSend

                Me.CurrentOffset = offset

                Me.OnDownloadProgress(New EventArgs.DownloadProgressEventArgs(Me, sizeToSend, offset, TotalSize))

                Return buffer

            Else
                Dim tmpCommand As New FileProviderCommand(Me.URI, FileProviderCommand.Commands.GetNextPortion)
                tmpCommand.FileChunkRequest = New FileChunkRequest(offset, portionSize)

                Dim tmpData As New CommunicatorDataObject
                tmpData.Tag = "FileProviderCommand"
                tmpData.Data = tmpCommand

                Me.Communicator.SendData(tmpData)

                Dim tmpReply As CommunicatorDataObject = Me.Communicator.ReceiveReply(tmpData, Me.TimeOut)

                If Not tmpReply Is Nothing Then
                    Return tmpReply.Data

                Else
                    Me.OnDownloadTimeOut(New System.EventArgs)

                    Me.Dispose()

                End If
            End If

        End Function

        ''' <summary>
        ''' Indicates that the download has completed. 
        ''' The FileProvider is disposed and a DownloadComplete Event is raised.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub DownloadComplete() 'Implements IFileProvider.DownloadComplete
            Me._DownloadFinished = True

            Me.Dispose()

            Me.OnDownloadCompleted(New System.EventArgs)

            If Not Me.Communicator Is Nothing Then
                Dim tmpCommand As New FileProviderCommand(Me.URI, FileProviderCommand.Commands.DownloadComplete)

                Dim tmpData As New CommunicatorDataObject
                tmpData.Tag = "FileProviderCommand"
                tmpData.Data = tmpCommand

                Me.Communicator.SendData(tmpData)

            End If

        End Sub

        ''' <summary>
        ''' Aborts the download.
        ''' </summary>
        ''' <remarks>This will cause a <see cref="DownloadAbortedException"/> to be thrown.</remarks>
        Public Sub Abort() 'Implements IFileProvider.Abort
            If Not Me.Aborted Then
                Me._Aborted = True

                Me.ResetTimeOutTimer()

                If Not Me.Communicator Is Nothing Then
                    Dim tmpCommand As New FileProviderCommand(Me.URI, FileProviderCommand.Commands.Abort)

                    Dim tmpData As New CommunicatorDataObject
                    tmpData.Tag = "FileProviderCommand"
                    tmpData.Data = tmpCommand

                    Me.Communicator.SendData(tmpData)

                End If
            End If

        End Sub

        ''' <summary>
        ''' Generates the URI for this provider. 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GenerateURI() As String
            Return System.Guid.NewGuid.ToString

        End Function

        ''' <summary>
        ''' Resets the Time Out Timer and starts it again.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub ResetTimeOutTimer()
            If Not Me.Aborted Then
                Me.TmrTimeOut.Enabled = False

                Me.TmrTimeOut.Interval = Me.TimeOut.TotalMilliseconds

                Me.TmrTimeOut.Enabled = True

            End If

        End Sub

        ''' <summary>
        ''' Disposes the Time Out Timer.
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub DisposeTimeOutTimer()
            Me.TmrTimeOut.Enabled = False

            Me.TmrTimeOut.Dispose()

            Me._TmrTimeOut = Nothing

        End Sub

        ''' <summary>
        ''' Raises the DownloadProgress Event with the specified <see cref="DownloadProgressEventArgs"/>.
        ''' </summary>
        ''' <param name="e"><see cref="DownloadProgressEventArgs"/> to be raised.</param>
        ''' <remarks>If <see cref="AsyncProgressEvents"/> is True, the Event is raised in seperate threads.</remarks>
        Private Sub OnDownloadProgress(ByVal e As EventArgs.DownloadProgressEventArgs)
            If Me.AsyncProgressEvents Then
                If Not Me.DownloadProgressEvent Is Nothing Then
                    For Each tmpHandler As EventHandler(Of EventArgs.DownloadProgressEventArgs) In Me.DownloadProgressEvent.GetInvocationList
                        Dim tmpThreadData As DownloadProgressThreadData = New DownloadProgressThreadData(Me, e, tmpHandler)

                        'THREAD: ThreadManager
                        'Dim tmpThread As New Thread(AddressOf tmpThreadData.NotifyHandler)
                        'tmpThread.Name = "FileDownloader DownloadProgress Thread"

                        Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("FileDownloader DownloadProgress Thread", AddressOf tmpThreadData.NotifyHandler)
                        tmpThread.Start()

                        Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Increment()

                    Next
                End If
            Else
                RaiseEvent DownloadProgress(Me, e)

            End If

        End Sub

        ''' <summary>
        ''' Raises the DownloadCompleted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadCompleted(ByVal e As System.EventArgs)
            RaiseEvent DownloadCompleted(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadTimeOut Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadTimeOut(ByVal e As System.EventArgs)
            RaiseEvent DownloadTimeOut(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadAborted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadAborted(ByVal e As System.EventArgs)
            RaiseEvent DownloadAborted(Me, e)

        End Sub

        Public Sub WaitForCompletion()
            Using New Threading.Lock(Me, True)
                If Not Me.DownloadFinished And Not Me.Aborted And Not Me.IsDisposed Then
                    If Me.CompletionResetEvent Is Nothing Then
                        Me.CompletionResetEvent = New ManualResetEvent(False)

                    End If
                End If

            End Using

            If Not Me.CompletionResetEvent Is Nothing Then
                Me.CompletionResetEvent.WaitOne()

            End If

        End Sub

        <System.Serializable()> Private Class DownloadProgressThreadData

#Region "Attributes"

            <NonSerialized()> Private _FileProvider As FileProvider
            <NonSerialized()> Private _EventArgs As EventArgs.DownloadProgressEventArgs
            <NonSerialized()> Private _Handler As EventHandler(Of EventArgs.DownloadProgressEventArgs)

#End Region

#Region "Properties"

            Public ReadOnly Property FileProvider() As FileProvider
                Get
                    Return Me._FileProvider

                End Get
            End Property

            Public ReadOnly Property EventArgs() As EventArgs.DownloadProgressEventArgs
                Get
                    Return Me._EventArgs

                End Get
            End Property

            Public ReadOnly Property Handler() As EventHandler(Of EventArgs.DownloadProgressEventArgs)
                Get
                    Return Me._Handler

                End Get
            End Property

#End Region

            Public Sub New(ByVal fileProvider As FileProvider, ByVal eventArgs As EventArgs.DownloadProgressEventArgs, ByVal handler As EventHandler(Of EventArgs.DownloadProgressEventArgs))
                Me._FileProvider = fileProvider
                Me._EventArgs = eventArgs
                Me._Handler = handler

            End Sub

            Public Sub NotifyHandler()
                Try
                    Me.Handler.Invoke(Me.FileProvider, Me.EventArgs)

                Catch ex As Exception
                    'SILENT

                Finally
                    Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Decrement()
                End Try
            End Sub

        End Class

        ''' <summary>
        ''' Disposes the Object and it's resources.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub Dispose()
            If Not Me.IsDisposed Then
                Me.IsDisposed = True

                Me.DisposeTimeOutTimer()

            End If

            Using New Threading.Lock(Me, True)
                If Not Me.CompletionResetEvent Is Nothing Then
                    Me.CompletionResetEvent.Set()

                End If

            End Using

        End Sub

        ''' <summary>
        ''' Indicates the there has been no activity for the specified Time Out interval.
        ''' The FileProvider is disposed and a DownloadTimeOut Event is raised.
        ''' </summary>
        ''' <param name="sender">Object raising the Elapsed Event.</param>
        ''' <param name="e">The supplied EventArgs.</param>
        ''' <remarks></remarks>
        Private Sub TmrTimeOut_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrTimeOut.Elapsed
            Me._TimedOut = True

            Me.Dispose()

            Me.OnDownloadTimeOut(New System.EventArgs)

        End Sub

#Region "Serialization Implementation"

        'This implementation is added since handler to this Objects event is Serialized also by default.
        <NonSerialized()> Private mInfo As SerializationInfo

        Protected Overridable Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext) Implements ISerializable.GetObjectData
            ' Use the shared method to populate the SerializationInfo object
            FileProvider.SerializeObject(Me, info)

        End Sub

        Protected Overridable Sub OnDeserialization(ByVal sender As Object) Implements System.Runtime.Serialization.IDeserializationCallback.OnDeserialization

            ' Call the shared method to deserialize the object
            FileProvider.DeserializeObject(Me, mInfo)

            ' Kill the recorded info object
            mInfo = Nothing

        End Sub

        Public Shared Sub SerializeObject(ByVal obj As Object, ByVal info As SerializationInfo)
            ' Local Variables
            Dim aMembersToSerialize As MemberInfo()

            ' Error handler
            Try

                ' Get a list of all fields in this object and derived objects
                ' that are to be serialized
                aMembersToSerialize = FileProvider.GetSerializableMembers(obj.GetType)

                '  Loop around all fields and save their values
                For Each objMember As MemberInfo In aMembersToSerialize

                    ' It is valid to serialize if it is in this array
                    ' Derived Fields with the same name as base fields
                    ' will automaticall be handled, so we don't need to
                    ' worry about duplicates

                    ' Determine if it is a filed or property
                    If objMember.MemberType = MemberTypes.Field Then

                        Dim objField As FieldInfo = DirectCast(objMember, FieldInfo)
                        info.AddValue(objField.Name, objField.GetValue(obj))

                    ElseIf objMember.MemberType = MemberTypes.Property Then

                        Dim objProperty As PropertyInfo = DirectCast(objMember, PropertyInfo)
                        info.AddValue(objProperty.Name, objProperty.GetValue(obj, Nothing))

                    End If

                Next

            Catch ex As Exception

                ' Trace any errors
                Trace.WriteLine("Error during serialization: " & ex.Message)
                Throw New SerializationException("Error during Serialization. ", ex)

            End Try

        End Sub

        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, Assertion:=True, SerializationFormatter:=True)> _
        Public Shared Sub DeserializeObject(ByVal obj As Object, ByVal info As SerializationInfo)
            ' Local Variables
            Dim aFieldsToDeserialize As MemberInfo()
            Dim aValues As Object()

            ' Error handler
            Try
                ' Get a list of all fields in this object and derived objects
                aFieldsToDeserialize = FileProvider.GetSerializableMembers(obj.GetType)

                ' Loop around all fields and get their values from the info object
                aValues = DirectCast(Array.CreateInstance(GetType(Object), aFieldsToDeserialize.Length), Object())

                For nCount As Integer = 0 To aFieldsToDeserialize.Length - 1

                    ' Determine if it is a field or property
                    If aFieldsToDeserialize(nCount).MemberType = MemberTypes.Field Then

                        ' get the field
                        Dim objField As FieldInfo = DirectCast(aFieldsToDeserialize(nCount), FieldInfo)

                        ' Get the value of the field from the info object
                        aValues(nCount) = info.GetValue(objField.Name, objField.FieldType)

                    ElseIf aFieldsToDeserialize(nCount).MemberType = MemberTypes.Property Then

                        ' get the property
                        Dim objProperty As PropertyInfo = DirectCast(aFieldsToDeserialize(nCount), PropertyInfo)

                        ' Get the value of the field from the info object
                        aValues(nCount) = info.GetValue(objProperty.Name, objProperty.PropertyType)

                    End If

                Next

                ' Now use formatter services to populate this object's fields
                FormatterServices.PopulateObjectMembers(obj, aFieldsToDeserialize, aValues)

            Catch ex As Exception

                ' Trace errors
                Trace.WriteLine("Error during deserialization: " & ex.Message)
                Throw New SerializationException("Error during deserialization", ex)

            End Try

        End Sub

        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, Assertion:=True, SerializationFormatter:=True)> _
        Private Shared Function GetSerializableMembers(ByVal PersistableType As System.Type) As MemberInfo()

            ' Local Variables
            Dim aAllMembers() As System.Reflection.MemberInfo
            Dim aSerilizableMembers As System.Collections.ArrayList

            '  Get all the serializable members
            aAllMembers = FormatterServices.GetSerializableMembers(PersistableType)

            aSerilizableMembers = New System.Collections.ArrayList

            ' Filter non-serializable fields and event delegates
            For Each objMember As MemberInfo In aAllMembers

                ' We're only interested in fields and properties
                If objMember.MemberType = MemberTypes.Field Then

                    ' get the field
                    Dim objField As FieldInfo = DirectCast(objMember, FieldInfo)

                    ' If it has a NonSerialized Attribute or if it is an event delegate, skip it
                    If (Not GetType(System.Delegate).IsAssignableFrom(objField.FieldType)) Then

                        ' Add the field as it is valid for serialization
                        aSerilizableMembers.Add(objField)

                    End If

                ElseIf objMember.MemberType = MemberTypes.Property Then

                    ' get the property
                    Dim objProperty As PropertyInfo = DirectCast(objMember, PropertyInfo)

                    ' if it is an event delegate, skip it
                    If (Not GetType(System.Delegate).IsAssignableFrom(objProperty.PropertyType)) Then

                        ' Add the field as it is valid for serialization
                        aSerilizableMembers.Add(objProperty)

                    End If

                End If

            Next objMember


            ' Return the array of persistable fields - need to convert to an array first
            Return DirectCast(aSerilizableMembers.ToArray(GetType(MemberInfo)), MemberInfo())

        End Function

#End Region

    End Class

End Namespace