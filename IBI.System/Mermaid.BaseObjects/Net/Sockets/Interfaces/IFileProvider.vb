'Imports mermaid.BaseObjects.IO

'Namespace Net.Sockets.Interfaces

'    ''' <summary>
'    ''' Is used to fetch files in small chunks.
'    ''' </summary>
'    ''' <remarks></remarks>
'    Public Interface IFileProvider

'#Region "Properties"

'        ''' <summary>
'        ''' Relative path to the file.
'        ''' </summary>
'        ''' <value>String containing the path.</value>
'        ''' <returns>Return the relative path.</returns>
'        ''' <remarks></remarks>
'        ReadOnly Property Path() As String

'        ''' <summary>
'        ''' The total size of the file.
'        ''' </summary>
'        ''' <value>The filesize.</value>
'        ''' <returns>Returns the size of the file.</returns>
'        ''' <remarks></remarks>
'        ReadOnly Property TotalSize() As Long

'        ''' <summary>
'        ''' MD5 checksum of the file.
'        ''' </summary>
'        ''' <value><see cref="FileHash"/> specifying the MD5.</value>
'        ''' <returns>Returns a <see cref="FileHash"/> specifying the MD5.</returns>
'        ''' <remarks></remarks>
'        ReadOnly Property MD5() As FileHash

'#End Region

'        ''' <summary>
'        '''     Returns the next portion or null if the entire file was downloaded.
'        ''' </summary>
'        ''' <returns>Next portion or null.</returns>
'        ''' <remarks>
'        ''' </remarks>
'        Function GetNextPortion(Optional ByVal portionSize As Integer = 32768) As Byte()

'        ''' <summary>
'        ''' Called by the FileDownloader when the download has completed.
'        ''' </summary>
'        ''' <remarks></remarks>
'        Sub DownloadComplete()

'        ''' <summary>
'        ''' Aborts the download.
'        ''' </summary>
'        ''' <remarks></remarks>
'        Sub Abort()

'    End Interface

'End Namespace