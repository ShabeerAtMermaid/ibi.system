Imports System.IO

Namespace Net.Sockets.Interfaces

    Public Interface IStreamHandler

        Function SerializeStream(ByVal input As MemoryStream) As MemoryStream
        Function DeSerializeStream(ByVal input As MemoryStream) As MemoryStream

    End Interface

End Namespace