﻿Namespace Net.Sockets.Interfaces

    Public Interface ISimpleSerializable

        Function Serialize() As String
        Sub Deserialize()

    End Interface

End Namespace