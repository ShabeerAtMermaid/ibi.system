Namespace Net.Sockets.Exceptions

    Public Class CommunicatorNotConnectedException
        Inherits System.ApplicationException

#Region "Constants"

        Private Const DEFAULT_MESSAGE As String = "Communicator not connected."

#End Region

        Public Sub New()
            MyBase.New(DEFAULT_MESSAGE)

        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)

        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As Exception)
            MyBase.New(message, innerException)

        End Sub

        Public Sub New(ByVal innerException As Exception)
            MyBase.New(DEFAULT_MESSAGE, innerException)

        End Sub

    End Class

End Namespace