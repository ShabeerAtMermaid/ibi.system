﻿Imports System.Net.Sockets
Imports System.Net

Namespace Net.Sockets

    Public Class TCPBroadcaster

#Region "Variables"

        Private _IPAddress As IPAddress
        Private _Port As Integer
        Private _Socket As Socket
        Private _AutoFlush As Boolean = True
        Private _FlushBuffer As System.Text.StringBuilder

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public Property Port() As Integer
            Get
                Return Me._Port

            End Get
            Set(ByVal value As Integer)
                Me._Port = value

            End Set
        End Property

        Public Property AutoFlush() As Boolean
            Get
                Return Me._AutoFlush

            End Get
            Set(ByVal value As Boolean)
                Me._AutoFlush = value

            End Set
        End Property

        Private Property FlushBuffer() As System.Text.StringBuilder
            Get
                Return Me._FlushBuffer

            End Get
            Set(ByVal value As System.Text.StringBuilder)
                Me._FlushBuffer = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

        Private ReadOnly Property ThreadPool() As Threading.ThreadPool
            Get
                Dim tmpSettings As New Threading.ThreadPool.ThreadPoolInfo
                tmpSettings.MinThreads = 0
                tmpSettings.MaxThreads = 5
                tmpSettings.ThreadSize = 1 * Threading.ThreadPool.ThreadPoolInfo.MB

                Return Threading.ThreadPoolManager.GetThreadPool("TCPBroadcaster ThreadPool", tmpSettings)

            End Get
        End Property

#End Region

        Public Sub New(ByVal port As Integer)
            '_Port = port

            '_Client = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
            '_Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1)
            Me.New(Converter.ToIPAddress("127.0.0.1"), port)

        End Sub

        Public Sub New(ByVal ipaddress As IPAddress, ByVal port As Integer)
            _IPAddress = ipaddress
            _Port = port

            _Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            _Socket.Connect(ipaddress, port)

        End Sub

        Public Sub WriteLine(ByVal line As String)
            Try
                If Not line.EndsWith(vbCrLf) Then line &= vbCrLf

                Me.Write(line)

            Catch ex As Exception
                'SILENT

            End Try
        End Sub

        Public Sub Write(ByVal text As String)
            Using tmpAnalyzer As New Diagnostics.PerformanceAnalyzer("TCPBroadcaster.Write")
                tmpAnalyzer.AddPerformanceMark("Start")

                If Not String.IsNullOrEmpty(text) And Not Me.IsDisposed Then
                    System.Threading.Monitor.Enter(Me)

                    tmpAnalyzer.AddPerformanceMark("Entering lock")

                    Try
                        If Me.AutoFlush Then
                            Me.SendData(text)

                            tmpAnalyzer.AddPerformanceMark("Sending data")

                        Else
                            If Me.FlushBuffer Is Nothing Then
                                Me.FlushBuffer = New System.Text.StringBuilder

                            End If

                            Me.FlushBuffer.Append(text)

                            tmpAnalyzer.AddPerformanceMark("Append to buffer")

                        End If

                    Catch ex As Exception
                        'SILENT

                    Finally
                        System.Threading.Monitor.Exit(Me)

                        tmpAnalyzer.AddPerformanceMark("Leaving lock")

                    End Try
                End If

                If tmpAnalyzer.TotalDuration > TimeSpan.FromSeconds(1) Then
                    SocketUtility.SocketPerformanceLog.WriteEntry(tmpAnalyzer.GetDifferentialResult)

                End If

            End Using

        End Sub

        Public Sub Flush()
            System.Threading.Monitor.Enter(Me)

            Try
                If Not Me.FlushBuffer Is Nothing Then
                    Me.SendData(Me.FlushBuffer.ToString)

                    Me.FlushBuffer = Nothing

                End If
            Finally
                System.Threading.Monitor.Exit(Me)

            End Try
        End Sub

        Private Sub SendData(ByVal text As String)
            Dim tmpParameters As New Threading.WorkItemCallbackParameterList
            tmpParameters("text") = text

            Dim tmpWorkItem As New Threading.WorkItem(AddressOf SendData_CallBack, tmpParameters)

            Me.ThreadPool.EnqueueWorkItem(tmpWorkItem)

        End Sub

        Private Function SendData_CallBack(ByVal parameters As Threading.WorkItemCallbackParameterList) As Object
            Try
                Dim tmpText As String = parameters("text")

                If _Socket Is Nothing Then
                    _Socket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                    _Socket.Connect(Me._IPAddress, Me.Port)

                End If

                Dim tmpBuffers As System.Collections.Generic.List(Of Byte()) = Me.GetBuffers(tmpText)

                For Each tmpBuffer As Byte() In tmpBuffers
                    Dim tmpBytesSent As Integer = _Socket.Send(tmpBuffer, 0, tmpBuffer.Length, SocketFlags.None)

                Next

            Catch ex As Exception
                'SILENT
                _Socket = Nothing

            End Try
        End Function

        Private Function GetBuffers(ByVal text As String) As System.Collections.Generic.List(Of Byte())
            Dim tmpBuffers As New System.Collections.Generic.List(Of Byte())

            Dim tmpMasterBuffer() As Byte = System.Text.Encoding.UTF8.GetBytes(text)
            Dim tmpBuffer(1024) As Byte
            Dim tmpBufferData As ArrayList

            While tmpMasterBuffer.Length > 0
                tmpBufferData = New ArrayList

                For i As Integer = 0 To Math.Min(tmpMasterBuffer.Length, 1024) - 1
                    tmpBufferData.Add(tmpMasterBuffer(i))

                Next

                If tmpMasterBuffer.Length > 1024 Then
                    Array.Resize(Of Byte)(tmpBuffer, tmpMasterBuffer.Length - 1024 - 1)
                    Array.Clear(tmpBuffer, 0, tmpBuffer.Length)

                    Array.Copy(tmpMasterBuffer, 1025, tmpBuffer, 0, tmpBuffer.Length)

                Else
                    Array.Resize(Of Byte)(tmpBuffer, 0)

                End If

                tmpBuffers.Add(tmpBufferData.ToArray(GetType(Byte)))
                tmpMasterBuffer = tmpBuffer

            End While

            Return tmpBuffers

        End Function

        Public Sub Dispose()
            If Not Me.IsDisposed Then
                Me.IsDisposed = True

                If Not _Socket Is Nothing Then
                    _Socket.Close()
                    _Socket = Nothing

                End If
            End If

        End Sub

    End Class

End Namespace