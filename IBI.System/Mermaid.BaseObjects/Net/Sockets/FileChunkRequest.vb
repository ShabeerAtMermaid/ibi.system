Namespace Net.Sockets

    <System.Serializable()> Friend Class FileChunkRequest

#Region "Variables"

        Private _Offset As Long
        Private _ChunkSize As Integer = 32768

#End Region

#Region "Properties"

        Public Property Offset() As Long
            Get
                Return Me._Offset

            End Get
            Set(ByVal value As Long)
                Me._Offset = value

            End Set
        End Property

        Public Property ChunkSize() As Integer
            Get
                Return Me._ChunkSize

            End Get
            Set(ByVal value As Integer)
                Me._ChunkSize = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal offset As Long, ByVal chunkSize As Integer)
            MyBase.New()

            Me.Offset = offset
            Me.ChunkSize = chunkSize

        End Sub

    End Class

End Namespace