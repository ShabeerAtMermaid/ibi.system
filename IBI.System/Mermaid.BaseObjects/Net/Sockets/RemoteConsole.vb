﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Namespace Net.Sockets

    '    Public Class RemoteConsole

    '#Region "Variables"

    '        Private _Lock As New Object

    '        Private _RemoteServerIP As IPAddress
    '        Private _RemoteServerName As String
    '        Private _Port As Integer

    '        Private _ComSocket As Socket
    '        Private _ReceiveData As Communicator.ReceiveData
    '        Private _AsyncSocketCallback As System.EventHandler(Of SocketAsyncEventArgs)
    '        Private _ReceiveBuffer() As Byte
    '        Private _Buffer As New Hashtable
    '        Private _HandleCollection As New Hashtable

    '        Private _IDCounter As Integer

    '        Private _IsDisposed As Boolean

    '#End Region

    '#Region "Properties"

    '        Private ReadOnly Property Lock() As Object
    '            Get
    '                Return Me._Lock

    '            End Get
    '        End Property

    '        Public ReadOnly Property RemoteServerIP() As IPAddress
    '            Get
    '                Return Me._RemoteServerIP

    '            End Get
    '        End Property

    '        Public ReadOnly Property RemoteServerName() As String
    '            Get
    '                Return Me._RemoteServerName

    '            End Get
    '        End Property

    '        Public ReadOnly Property Port() As Integer
    '            Get
    '                Return Me._Port

    '            End Get
    '        End Property

    '        Private Property ComSocket() As Socket
    '            Get
    '                Return Me._ComSocket

    '            End Get
    '            Set(ByVal value As Socket)
    '                Me._ComSocket = value

    '            End Set
    '        End Property

    '        Private Property AsyncSocketCallback() As System.EventHandler(Of SocketAsyncEventArgs)
    '            Get
    '                Return _AsyncSocketCallback

    '            End Get
    '            Set(ByVal value As System.EventHandler(Of SocketAsyncEventArgs))
    '                _AsyncSocketCallback = value

    '            End Set
    '        End Property

    '        Private Property Buffer() As Hashtable
    '            Get
    '                Return Me._Buffer

    '            End Get
    '            Set(ByVal value As Hashtable)
    '                Me._Buffer = value

    '            End Set
    '        End Property

    '        Private ReadOnly Property HandleCollection() As Hashtable
    '            Get
    '                Return Me._HandleCollection

    '            End Get
    '        End Property

    '        Private ReadOnly Property ReconnectDelay() As TimeSpan
    '            Get
    '                Return TimeSpan.FromSeconds(3)

    '            End Get
    '        End Property

    '        Private ReadOnly Property IDCounter() As Integer
    '            Get
    '                Dim tmpID As Integer = Interlocked.Increment(_IDCounter)

    '                Return tmpID

    '            End Get
    '        End Property

    '        Private Property IsDisposed() As Boolean
    '            Get
    '                Return Me._IsDisposed

    '            End Get
    '            Set(ByVal value As Boolean)
    '                Me._IsDisposed = value

    '            End Set
    '        End Property

    '#End Region

    '        Public Sub New(ByVal remoteConsole As String, ByVal port As Integer)
    '            MyBase.New()

    '            Me._RemoteServerName = remoteConsole
    '            Me._Port = port

    '            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
    '            Me._ReceiveData = New Communicator.ReceiveData(Me.AsyncSocketCallback)

    '            Me.Connect()

    '        End Sub

    '        Public Sub New(ByVal remoteConsoleIP As IPAddress, ByVal port As Integer)
    '            MyBase.New()

    '            Me._RemoteServerIP = remoteConsoleIP
    '            Me._Port = port

    '            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
    '            Me._ReceiveData = New Communicator.ReceiveData(Me.AsyncSocketCallback)

    '            Me.Connect()

    '        End Sub

    '        Private Sub Connect()
    '            Try
    '                Dim reconnect As Boolean = False

    '                If Me.ComSocket Is Nothing Then
    '                    reconnect = True

    '                Else
    '                    If Not Me.ComSocket.Connected Then
    '                        reconnect = True
    '                    End If
    '                End If

    '                If reconnect Then
    '                    Me.DisposeNetworkResources()

    '                    Me.ComSocket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)

    '                    If Not String.IsNullOrEmpty(Me.RemoteServerName) Then
    '                        Me.ComSocket.Connect(Me.RemoteServerName, Me.Port)
    '                    Else
    '                        Me.ComSocket.Connect(Me.RemoteServerIP, Me.Port)
    '                    End If

    '                    Me.Receive()

    '                End If
    '            Catch ex As Exception
    '                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '                Me.DisposeNetworkResources()

    '            End Try
    '        End Sub

    '        Public Function SendCommand(ByVal command As String) As String
    '            Return Me.SendCommand(command, TimeSpan.FromMilliseconds(-1))

    '        End Function

    '        Public Function SendCommand(ByVal command As String, ByVal timeout As TimeSpan) As String
    '            Dim tmpData As New RemoteConsolePacket(command)

    '            Me.SendData(tmpData)

    '            Return Me.ReceiveReply(tmpData, timeout).Data

    '        End Function

    '        Private Function SendData(ByVal data As RemoteConsolePacket) As String
    '            If Not Me.IsDisposed Then
    '                Monitor.Enter(Me.Lock)

    '                Me.Connect()

    '                If Not Me.ComSocket Is Nothing Then
    '                    If String.IsNullOrEmpty(data.ID) Then data.ID = Me.IDCounter

    '                    Dim tmpStream As System.IO.MemoryStream = New System.IO.MemoryStream()

    '                    'Write data
    '                    Dim tmpDataStream As System.IO.MemoryStream = data.Serialize

    '                    Dim tmpEndPosition As Long

    '                    Dim hasBeenCompressed As Boolean = False

    '                    Dim tmpDataBytes() As Byte = tmpDataStream.ToArray
    '                    tmpDataStream.Close()
    '                    tmpEndPosition = tmpStream.Length
    '                    tmpStream.SetLength(tmpStream.Length + tmpDataBytes.Length)
    '                    tmpStream.Position = tmpEndPosition

    '                    tmpStream.Write(tmpDataBytes, 0, tmpDataBytes.Length)

    '                    'Write EOS
    '                    tmpEndPosition = tmpStream.Length
    '                    tmpStream.SetLength(tmpStream.Length + SocketUtility.EOSBytes.Length)
    '                    tmpStream.Position = tmpEndPosition

    '                    tmpStream.Write(SocketUtility.EOSBytes, 0, SocketUtility.EOSBytes.Length)

    '                    Me.ComSocket.Send(tmpStream.ToArray)

    '                    tmpStream.Close()
    '                    tmpStream.Dispose()


    '                Else
    '                    Throw New Exception("RemoteConsole is not connected")

    '                End If
    '            End If

    '        End Function

    '        Private Function ReceiveReply(ByVal data As RemoteConsolePacket) As RemoteConsolePacket
    '            Return Me.ReceiveReply(data, TimeSpan.FromMilliseconds(-1))

    '        End Function

    '        Private Function ReceiveReply(ByVal data As RemoteConsolePacket, ByVal timeout As TimeSpan) As RemoteConsolePacket
    '            If Not Me.IsDisposed Then
    '                Dim tmpHandle As ManualResetEvent = Me.GetHandle(data)

    '                If tmpHandle.WaitOne(timeout, False) Then
    '                    Dim tmpReply As RemoteConsolePacket = Me.Buffer(data.ID)
    '                    Me.HandleCollection.Remove(data.ID)
    '                    Me.Buffer.Remove(data.ID)

    '                    tmpHandle.Close()

    '                    If TypeOf tmpReply.Data Is Exception Then
    '                        Throw New Exception("Exception thrown through Socket", CType(tmpReply.Data, Exception))

    '                    End If

    '                    Return tmpReply

    '                Else
    '                    Me.HandleCollection.Remove(data.ID)

    '                    tmpHandle.Close()

    '                    Throw New TimeoutException("Reply has not been received within the specified timeout.")

    '                End If

    '                Return Nothing

    '            Else
    '                Throw New ObjectDisposedException("RemoteConsole")

    '            End If

    '        End Function

    '        Private Function GetHandle(ByVal data As RemoteConsolePacket) As ManualResetEvent
    '            Monitor.Enter(Me.Lock)

    '            Try
    '                Dim tmpHandle As ManualResetEvent = Me.HandleCollection(data.ID)

    '                If tmpHandle Is Nothing Then
    '                    tmpHandle = New ManualResetEvent(False)
    '                    Me.HandleCollection(data.ID) = tmpHandle

    '                End If

    '                If Me.Buffer.ContainsKey(data.ID) Then
    '                    tmpHandle.Set()

    '                End If

    '                Return tmpHandle

    '            Finally
    '                Monitor.Exit(Me.Lock)

    '            End Try
    '        End Function

    '        Private Sub Receive()
    '            Try
    '                If Not Me.IsDisposed Then
    '                    If Not Me.ComSocket Is Nothing Then
    '                        If Me._ReceiveData.IsDisposed Or Me._ReceiveData.InUse Then
    '                            Me._ReceiveData = New Communicator.ReceiveData(Me.AsyncSocketCallback)

    '                        End If

    '                        Me._ReceiveData.Socket = Me.ComSocket

    '                        Me._ReceiveData.InUse = True

    '                        Me._ReceiveData.Socket.ReceiveAsync(Me._ReceiveData.AsyncEventArgs)

    '                    End If
    '                End If

    '            Catch ex As Exception
    '                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '                Me.DisposeNetworkResources()

    '            End Try
    '        End Sub

    '        Private Sub Socket_OnReceive(ByVal src As Object, ByVal sae As SocketAsyncEventArgs)
    '            If Not Me.IsDisposed Then
    '                Dim tmpReceiveRestarted As Boolean = False

    '                Try
    '                    Dim tmpReceiveData As Communicator.ReceiveData = sae.UserToken

    '                    If Not tmpReceiveData.Socket Is Nothing Then
    '                        Dim tmpBytesReceived As Integer = sae.BytesTransferred

    '                        If Not Me.IsDisposed Then
    '                            If tmpBytesReceived > 0 Then
    '                                Dim tmpCurrentBufferLength As Long = 0

    '                                If Not _ReceiveBuffer Is Nothing Then tmpCurrentBufferLength = _ReceiveBuffer.Length

    '                                Dim tmpReceiveBuffer(tmpCurrentBufferLength + tmpBytesReceived - 1) As Byte

    '                                If Not _ReceiveBuffer Is Nothing Then _ReceiveBuffer.CopyTo(tmpReceiveBuffer, 0)
    '                                Array.Copy(tmpReceiveData.Buffer, 0, tmpReceiveBuffer, tmpCurrentBufferLength, tmpBytesReceived)

    '                                If Not tmpReceiveData Is Me._ReceiveData Then
    '                                    tmpReceiveData.Dispose()

    '                                Else
    '                                    tmpReceiveData.Clear()

    '                                End If

    '                                _ReceiveBuffer = tmpReceiveBuffer

    '                                Dim tmpEOSIndex As Long = SocketUtility.IndexOfEOS(_ReceiveBuffer)

    '                                While tmpEOSIndex <> -1 And Not Me.IsDisposed
    '                                    Dim tmpStreamData As StreamData = Me.GetStream(tmpEOSIndex)

    '                                    If Not tmpStreamData Is Nothing AndAlso Not tmpStreamData.Stream Is Nothing Then
    '                                        Dim tmpStream As System.IO.MemoryStream = tmpStreamData.Stream

    '                                        Dim tmpStreamSize As Long = tmpStream.Length

    '                                        Dim tmpData As RemoteConsolePacket = RemoteConsolePacket.Deserialize(tmpStream)

    '                                        Dim tmpDeserializationEnd As Integer = System.Environment.TickCount And Integer.MaxValue

    '                                        tmpStream.Dispose()

    '                                        Monitor.Enter(Me.Lock)

    '                                        Me.Buffer(tmpData.ID) = tmpData

    '                                        Try
    '                                            Dim tmpHandle As ManualResetEvent = Me.HandleCollection(tmpData.ID)

    '                                            If Not tmpHandle Is Nothing Then
    '                                                tmpHandle.Set()
    '                                            End If

    '                                        Finally
    '                                            Monitor.Exit(Me.Lock)

    '                                        End Try
    '                                    End If

    '                                    If Not tmpStreamData Is Nothing Then tmpStreamData.Dispose()

    '                                    tmpEOSIndex = SocketUtility.IndexOfEOS(_ReceiveBuffer)

    '                                End While


    '                            Else
    '                                Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

    '                            End If
    '                        End If
    '                    End If

    '                Catch ex As System.Runtime.Serialization.SerializationException
    '                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '                    Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

    '                Catch ex As System.Net.Sockets.SocketException
    '                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '                    Me.DisposeNetworkResources()

    '                    Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)


    '                Catch ex As Exception
    '                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '                    Thread.Sleep(Me.ReconnectDelay.TotalMilliseconds)

    '                Finally
    '                    If Not Me.IsDisposed Then
    '                        If Not tmpReceiveRestarted Then
    '                            Me.Receive()
    '                        End If
    '                    End If
    '                End Try
    '            End If
    '        End Sub

    '        Private Function GetStream(ByVal endIndex As Long) As StreamData
    '            Dim tmpBuffer(endIndex - 1) As Byte

    '            Array.Copy(_ReceiveBuffer, tmpBuffer, endIndex)

    '            If _ReceiveBuffer.Length > endIndex + SocketUtility.EOSBytes.Length Then
    '                Dim tmpReceiveBuffer(_ReceiveBuffer.Length - endIndex - SocketUtility.EOSBytes.Length - 1) As Byte

    '                Array.Copy(_ReceiveBuffer, endIndex + SocketUtility.EOSBytes.Length, tmpReceiveBuffer, 0, tmpReceiveBuffer.Length)

    '                _ReceiveBuffer = tmpReceiveBuffer

    '            Else
    '                _ReceiveBuffer = Nothing

    '            End If

    '            If tmpBuffer.Length = SocketUtility.ShutdownBytes.Length Then
    '                If SocketUtility.IndexOfShutdown(tmpBuffer) <> -1 Then
    '                    Me.Dispose()

    '                    Return Nothing

    '                End If
    '            End If

    '            Dim tmpStream As New System.IO.MemoryStream(tmpBuffer)

    '            Dim hasBeenUncompressed As Boolean = False

    '            If SocketUtility.IsCompressedStream(tmpBuffer) Then
    '                Dim tmpFinalBuffer(tmpBuffer.Length - SocketUtility.CompressedContentBytes.Length - 1) As Byte

    '                Array.Copy(tmpBuffer, SocketUtility.CompressedContentBytes.Length, tmpFinalBuffer, 0, tmpFinalBuffer.Length)

    '                tmpStream.Close()
    '                tmpStream = New System.IO.MemoryStream(tmpFinalBuffer)

    '                Dim tmpCompressionhandler As New CompressionStreamHandler

    '                tmpStream = tmpCompressionhandler.DeSerializeStream(tmpStream)

    '                hasBeenUncompressed = True

    '            End If

    '            Return New StreamData(hasBeenUncompressed, tmpStream)

    '        End Function

    '        Private Sub DisposeNetworkResources()
    '            Try
    '                If Not Me.ComSocket Is Nothing Then
    '                    Me.ComSocket.Close()
    '                End If

    '            Catch ex As Exception
    '                'SILENT
    '                BaseUtility.OnExceptionOccurred(Me, New mermaid.BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

    '            Finally
    '                Me.ComSocket = Nothing

    '            End Try
    '        End Sub

    '        Public Sub Dispose()
    '            If Not Me.IsDisposed Then
    '                Me._IsDisposed = True

    '                Me.DisposeNetworkResources()

    '                If Not Me._ReceiveData Is Nothing Then
    '                    Me._ReceiveData.Dispose()
    '                    Me._ReceiveData = Nothing

    '                End If

    '                Me._RemoteServerIP = Nothing
    '                Me._Port = Nothing
    '                Me._Buffer = Nothing
    '                Me._Lock = Nothing
    '                Me._HandleCollection = Nothing

    '            End If

    '        End Sub

    '    End Class

End Namespace