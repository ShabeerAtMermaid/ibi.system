﻿'Imports System.Net.Sockets
'Imports System.Net
'Imports System.Threading

'Namespace Net.Sockets

'    Public Class RemoteConsoleServerSession

'#Region "Variables"

'        Private _Lock As New Object

'        Private _ComSocket As Socket
'        Private _ReceiveData As Communicator.ReceiveData
'        Private _AsyncSocketCallback As System.EventHandler(Of SocketAsyncEventArgs)
'        Private _ReceiveBuffer() As Byte

'        Private _IsDisposed As Boolean

'#End Region

'#Region "Properties"

'        Private ReadOnly Property Lock() As Object
'            Get
'                Return Me._Lock

'            End Get
'        End Property

'        Public ReadOnly Property Port() As Integer
'            Get
'                Return Me._Port

'            End Get
'        End Property

'        Private Property ComSocket() As Socket
'            Get
'                Return Me._ComSocket

'            End Get
'            Set(ByVal value As Socket)
'                Me._ComSocket = value

'            End Set
'        End Property

'        Private Property AsyncSocketCallback() As System.EventHandler(Of SocketAsyncEventArgs)
'            Get
'                Return _AsyncSocketCallback

'            End Get
'            Set(ByVal value As System.EventHandler(Of SocketAsyncEventArgs))
'                _AsyncSocketCallback = value

'            End Set
'        End Property

'        Private Property IsDisposed() As Boolean
'            Get
'                Return Me._IsDisposed

'            End Get
'            Set(ByVal value As Boolean)
'                Me._IsDisposed = value

'            End Set
'        End Property

'#End Region

'        Public Sub New(ByVal socket As Socket)
'            MyBase.New()

'            Me._ComSocket = socket

'            Me.AsyncSocketCallback = New System.EventHandler(Of SocketAsyncEventArgs)(AddressOf Socket_OnReceive)
'            Me._ReceiveData = New Communicator.ReceiveData(Me.AsyncSocketCallback)

'        End Sub

'        Friend Sub Initialize()
'            Me.Receive()

'        End Sub

'        Private Sub Receive()
'            Try
'                If Not Me.IsDisposed Then
'                    If Not Me.ComSocket Is Nothing Then
'                        If Me._ReceiveData.IsDisposed Or Me._ReceiveData.InUse Then
'                            Me._ReceiveData = New Communicator.ReceiveData(Me.AsyncSocketCallback)

'                        End If

'                        Me._ReceiveData.Socket = Me.ComSocket

'                        Me._ReceiveData.InUse = True

'                        Me._ReceiveData.Socket.ReceiveAsync(Me._ReceiveData.AsyncEventArgs)

'                    End If
'                End If

'            Catch ex As Exception
'                BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

'                Me.DisposeNetworkResources()

'            End Try
'        End Sub

'        Private Sub Socket_OnReceive(ByVal src As Object, ByVal sae As SocketAsyncEventArgs)
'            If Not Me.IsDisposed Then
'                Dim tmpReceiveRestarted As Boolean = False

'                Try
'                    Dim tmpReceiveData As Communicator.ReceiveData = sae.UserToken

'                    If Not tmpReceiveData.Socket Is Nothing Then
'                        Dim tmpBytesReceived As Integer = sae.BytesTransferred

'                        If Not Me.IsDisposed Then
'                            Dim tmpCurrentBufferLength As Long = 0

'                            If Not _ReceiveBuffer Is Nothing Then tmpCurrentBufferLength = _ReceiveBuffer.Length

'                            Dim tmpReceiveBuffer(tmpCurrentBufferLength + tmpBytesReceived - 1) As Byte

'                            If Not _ReceiveBuffer Is Nothing Then _ReceiveBuffer.CopyTo(tmpReceiveBuffer, 0)
'                            Array.Copy(tmpReceiveData.Buffer, 0, tmpReceiveBuffer, tmpCurrentBufferLength, tmpBytesReceived)

'                            If Not tmpReceiveData Is Me._ReceiveData Then
'                                tmpReceiveData.Dispose()

'                            Else
'                                tmpReceiveData.Clear()

'                            End If

'                            _ReceiveBuffer = tmpReceiveBuffer

'                            Dim tmpEOSIndex As Long = SocketUtility.IndexOfEOS(_ReceiveBuffer)

'                            While tmpEOSIndex <> -1 And Not Me.IsDisposed
'                                Dim tmpStreamData As StreamData = Me.GetStream(tmpEOSIndex)

'                                If Not tmpStreamData Is Nothing AndAlso Not tmpStreamData.Stream Is Nothing Then
'                                    Dim tmpStream As System.IO.MemoryStream = tmpStreamData.Stream

'                                    Dim tmpStreamSize As Long = tmpStream.Length

'                                    Dim tmpData As RemoteConsolePacket = RemoteConsolePacket.Deserialize(tmpStream)

'                                    Dim tmpDeserializationEnd As Integer = System.Environment.TickCount And Integer.MaxValue

'                                    tmpStream.Dispose()

'                                    Me.OnCommandReceived(tmpData)

'                                End If

'                                If Not tmpStreamData Is Nothing Then tmpStreamData.Dispose()

'                                tmpEOSIndex = SocketUtility.IndexOfEOS(_ReceiveBuffer)

'                            End While


'                        End If
'                    End If

'                Catch ex As System.Runtime.Serialization.SerializationException
'                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

'                Catch ex As System.Net.Sockets.SocketException
'                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

'                    Me.Dispose()

'                Catch ex As Exception
'                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

'                Finally
'                    If Not Me.IsDisposed Then
'                        If Not tmpReceiveRestarted Then
'                            Me.Receive()
'                        End If
'                    End If
'                End Try
'            End If
'        End Sub

'        Private Function GetStream(ByVal endIndex As Long) As StreamData
'            Dim tmpBuffer(endIndex - 1) As Byte

'            Array.Copy(_ReceiveBuffer, tmpBuffer, endIndex)

'            If _ReceiveBuffer.Length > endIndex + SocketUtility.EOSBytes.Length Then
'                Dim tmpReceiveBuffer(_ReceiveBuffer.Length - endIndex - SocketUtility.EOSBytes.Length - 1) As Byte

'                Array.Copy(_ReceiveBuffer, endIndex + SocketUtility.EOSBytes.Length, tmpReceiveBuffer, 0, tmpReceiveBuffer.Length)

'                _ReceiveBuffer = tmpReceiveBuffer

'            Else
'                _ReceiveBuffer = Nothing

'            End If

'            If tmpBuffer.Length = SocketUtility.ShutdownBytes.Length Then
'                If SocketUtility.IndexOfShutdown(tmpBuffer) <> -1 Then
'                    Me.Dispose()

'                    Return Nothing

'                End If
'            End If

'            Dim tmpStream As New System.IO.MemoryStream(tmpBuffer)

'            Dim hasBeenUncompressed As Boolean = False

'            If SocketUtility.IsCompressedStream(tmpBuffer) Then
'                Dim tmpFinalBuffer(tmpBuffer.Length - SocketUtility.CompressedContentBytes.Length - 1) As Byte

'                Array.Copy(tmpBuffer, SocketUtility.CompressedContentBytes.Length, tmpFinalBuffer, 0, tmpFinalBuffer.Length)

'                tmpStream.Close()
'                tmpStream = New System.IO.MemoryStream(tmpFinalBuffer)

'                Dim tmpCompressionhandler As New CompressionStreamHandler

'                tmpStream = tmpCompressionhandler.DeSerializeStream(tmpStream)

'                hasBeenUncompressed = True

'            End If

'            Return New StreamData(hasBeenUncompressed, tmpStream)

'        End Function

'        Private Sub OnCommandReceived(ByVal data As RemoteConsolePacket)

'        End Sub

'        Private Sub DisposeNetworkResources()
'            Try
'                If Not Me.ComSocket Is Nothing Then
'                    Me.ComSocket.Close()
'                End If

'            Catch ex As Exception
'                'SILENT
'                BaseUtility.OnExceptionOccurred(Me, New mermaid.BaseObjects.EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

'            Finally
'                Me.ComSocket = Nothing

'            End Try
'        End Sub

'        Public Sub Dispose()
'            If Not Me.IsDisposed Then
'                Me._IsDisposed = True

'                Me.DisposeNetworkResources()

'                If Not Me._ReceiveData Is Nothing Then
'                    Me._ReceiveData.Dispose()
'                    Me._ReceiveData = Nothing

'                End If

'                Me._RemoteServerIP = Nothing
'                Me._Port = Nothing
'                Me._Buffer = Nothing
'                Me._Lock = Nothing
'                Me._HandleCollection = Nothing

'            End If

'        End Sub

'    End Class

'End Namespace