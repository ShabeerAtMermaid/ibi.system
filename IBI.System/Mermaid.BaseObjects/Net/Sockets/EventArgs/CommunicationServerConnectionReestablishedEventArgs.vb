Imports System.Net.Sockets

Namespace Net.Sockets.EventArgs

    Public Class CommunicationServerConnectionReestablishedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Session As CommunicatorSession
        Private _Socket As Socket

#End Region

#Region "Properties"

        Public ReadOnly Property Session() As CommunicatorSession
            Get
                Return Me._Session

            End Get
        End Property

        Public ReadOnly Property Socket() As Socket
            Get
                Return Me._Socket

            End Get
        End Property

#End Region

        Public Sub New(ByVal session As CommunicatorSession, ByVal socket As Socket)
            MyBase.New()

            Me._Session = session
            Me._Socket = socket

        End Sub

    End Class

End Namespace