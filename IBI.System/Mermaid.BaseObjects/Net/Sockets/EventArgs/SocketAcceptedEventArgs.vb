Imports System.Net.Sockets

Namespace Net.Sockets.EventArgs

    Public Class SocketAcceptedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _IDentifier As String
        Private _Socket As Socket

#End Region

#Region "Properties"

        Public ReadOnly Property IDentifier() As String
            Get
                Return Me._IDentifier

            End Get
        End Property

        Public ReadOnly Property Socket() As Socket
            Get
                Return Me._Socket

            End Get
        End Property

#End Region

        Public Sub New(ByVal identifier As String, ByVal socket As Socket)
            MyBase.New()

            Me._IDentifier = identifier
            Me._Socket = socket

        End Sub

    End Class

End Namespace