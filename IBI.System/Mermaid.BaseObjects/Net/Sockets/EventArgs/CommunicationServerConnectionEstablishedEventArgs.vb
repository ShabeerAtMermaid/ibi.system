Imports System.Net.Sockets

Namespace Net.Sockets.EventArgs

    Public Class CommunicationServerConnectionEstablishedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Socket As Socket
        Private _Accepted As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property Socket() As Socket
            Get
                Return Me._Socket

            End Get
        End Property

        Public Property Accepted() As Boolean
            Get
                Return Me._Accepted

            End Get
            Set(ByVal value As Boolean)
                Me._Accepted = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal socket As Socket, ByVal accepted As Boolean)
            MyBase.New()

            Me._Socket = socket
            Me._Accepted = accepted

        End Sub

    End Class

End Namespace