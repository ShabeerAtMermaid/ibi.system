Namespace Net.Sockets.EventArgs

    Public Class DownloadProgressEventArgs
        Inherits System.EventArgs

#Region "Attributes"

        Private _FileDownloader As FileDownloader
        Private _FileProvider As FileProvider
        Private _DownloadedSinceLastProgress As Long
        Private _DownloadedTotal As Long
        Private _PercentDownloaded As Double
        Private _DownloadSize As Long

#End Region

#Region "Properties"

        Public ReadOnly Property FileDownloader() As FileDownloader
            Get
                Return Me._FileDownloader

            End Get
        End Property

        Public ReadOnly Property FileProvider() As FileProvider
            Get
                Return Me._FileProvider

            End Get
        End Property

        Public ReadOnly Property DownloadedSinceLastProgress() As Long
            Get
                Return Me._DownloadedSinceLastProgress

            End Get
        End Property

        Public ReadOnly Property DownloadedTotal() As Long
            Get
                Return Me._DownloadedTotal

            End Get
        End Property

        Public ReadOnly Property PercentDownloaded() As Double
            Get
                Return (Me.DownloadedTotal / Me.DownloadSize) * 100

            End Get
        End Property

        Public ReadOnly Property DownloadSize() As Long
            Get
                Return Me._DownloadSize

            End Get
        End Property

#End Region

        Public Sub New(ByRef fileDownloader As FileDownloader, ByVal downloadedSinceLastProgress As Long, ByVal downloadedTotal As Long, ByVal downloadSize As Long)
            Me._FileDownloader = fileDownloader
            Me._DownloadedSinceLastProgress = downloadedSinceLastProgress
            Me._DownloadedTotal = downloadedTotal
            Me._DownloadSize = downloadSize

        End Sub

        Public Sub New(ByRef fileProvider As FileProvider, ByVal downloadedSinceLastProgress As Long, ByVal downloadedTotal As Long, ByVal downloadSize As Long)
            Me._FileProvider = fileProvider
            Me._DownloadedSinceLastProgress = downloadedSinceLastProgress
            Me._DownloadedTotal = downloadedTotal
            Me._DownloadSize = downloadSize

        End Sub

    End Class

End Namespace