Namespace Net.Sockets.EventArgs

    Public Class CommunicatorSessionCreatedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _CommunicatorSession As CommunicatorSession

#End Region

#Region "Properties"

        Public ReadOnly Property CommunicatorSession() As CommunicatorSession
            Get
                Return Me._CommunicatorSession

            End Get
        End Property

#End Region

        Public Sub New(ByVal session As CommunicatorSession)
            MyBase.New()

            Me._CommunicatorSession = session

        End Sub

    End Class

End Namespace