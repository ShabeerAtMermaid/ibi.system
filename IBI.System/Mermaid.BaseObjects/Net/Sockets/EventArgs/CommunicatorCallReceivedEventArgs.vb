Namespace Net.Sockets.EventArgs

    Public Class CommunicatorCallReceivedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Data As CommunicatorDataObject

#End Region

#Region "Properties"

        Public ReadOnly Property Data() As CommunicatorDataObject
            Get
                Return Me._Data

            End Get
        End Property

#End Region

        Public Sub New(ByVal data As CommunicatorDataObject)
            MyBase.New()

            Me._Data = data

        End Sub

    End Class

End Namespace