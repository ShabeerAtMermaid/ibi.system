Imports System.Net

Namespace Net.Sockets.EventArgs

    Public Class CommunicationServerStartedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Port As Integer

#End Region

#Region "Properties"

        Public ReadOnly Property Port() As Integer
            Get
                Return Me._Port

            End Get
        End Property

#End Region

        Public Sub New(ByVal port As Integer)
            MyBase.New()

            Me._Port = port

        End Sub

    End Class

End Namespace