Imports System.Net.Sockets

Namespace Net.Sockets.EventArgs

    Public Class CommunicatorBeforeSendEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Data As CommunicatorDataObject
        Private _Cancel As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property Data() As CommunicatorDataObject
            Get
                Return Me._Data

            End Get
        End Property

        Public Property Cancel() As Boolean
            Get
                Return Me._Cancel

            End Get
            Set(ByVal value As Boolean)
                Me._Cancel = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal data As CommunicatorDataObject, ByVal Cancel As Boolean)
            MyBase.New()

            Me._Data = data
            Me._Cancel = Cancel

        End Sub

    End Class

End Namespace