﻿Imports System.Net.Sockets

Namespace Net.Sockets.EventArgs

    Public Class ConnectionEstablishedEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _Socket As Socket
        Private _Cancel As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property Socket() As Socket
            Get
                Return Me._Socket

            End Get
        End Property

        Public ReadOnly Property Cancel() As Boolean
            Get
                Return Me._Cancel

            End Get
        End Property

#End Region

        Public Sub New(ByVal socket As Socket)
            MyBase.New()

            Me._Socket = socket

        End Sub

    End Class

End Namespace