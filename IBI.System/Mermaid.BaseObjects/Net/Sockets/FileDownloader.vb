Imports mermaid.BaseObjects.Net.Sockets.Interfaces
Imports mermaid.BaseObjects.Exceptions
Imports mermaid.BaseObjects.IO
Imports System.Threading
Imports System.Text
Imports System.IO

Namespace Net.Sockets

    ''' <summary>
    ''' Class used to download a file provided by a FileProvider.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class FileDownloader

#Region "Attributes"

        Private _FileProvider As FileProvider
        Private _PortionSize As Integer = 32768
        Private _AsyncProgressEvents As Boolean
        Private _Aborted As Boolean
        Private _DownloadFinished As Boolean
        Private _TimedOut As Boolean
        Private _Timeout As TimeSpan = New TimeSpan(0, 0, 30)
        Private _TimeoutsAccepted As Integer = 2
        Private _OverwriteExistingFiles As Boolean
        Private _SaveFileHash As Boolean = True

#End Region

#Region "Properties"

        ''' <summary>
        ''' <see cref="IFileProvider"/> where the file is downloaded from. 
        ''' </summary>
        ''' <value><see cref="IFileProvider"/> to download from.</value>
        ''' <returns><see cref="IFileProvider"/> to download from.</returns>
        ''' <remarks>If only the URI to the provider has been supplied then the proxy is created.</remarks>
        Public ReadOnly Property FileProvider() As FileProvider
            Get
                Return Me._FileProvider

            End Get
        End Property

        ''' <summary>
        ''' Specified the size of the buffer when downloading.
        ''' </summary>
        ''' <value><see cref="Integer"/> defining the buffer size.</value>
        ''' <returns>Returns the buffer size.</returns>
        ''' <remarks></remarks>
        Public Property PortionSize() As Integer
            Get
                Return Me._PortionSize

            End Get
            Set(ByVal Value As Integer)
                Me._PortionSize = Value

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether calls to a registered <see cref="DownloadProgressHandler"/> is done async.
        ''' </summary>
        ''' <value>Boolean defining whether DownloadProgress events are raised async.</value>
        ''' <returns>Return whether DownloadProgress events are raised async.</returns>
        ''' <remarks></remarks>
        Public Property AsyncProgressEvents() As Boolean
            Get
                Return Me._AsyncProgressEvents

            End Get
            Set(ByVal Value As Boolean)
                Me._AsyncProgressEvents = Value

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether the download has been aborted.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has been aborted.</value>
        ''' <returns>Return whether the download has been aborted.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Aborted() As Boolean
            Get
                Return Me._Aborted

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has finished sucessfully.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether the download has finished sucessfully.</value>
        ''' <returns>Return whether the download has finished sucessfully.</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property DownloadFinished() As Boolean
            Get
                Return Me._DownloadFinished

            End Get
        End Property

        ''' <summary>
        ''' Specifies whether the download has timed out.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property TimedOut() As Boolean
            Get
                Throw New NotImplementedException

                Return Me._TimedOut

            End Get
        End Property

        ''' <summary>
        ''' <see cref="TimeSpan"/> defining the timeout interval for the download.
        ''' </summary>
        ''' <value><see cref="TimeSpan"/> defining the timeout.</value>
        ''' <returns>The timeout for the download.</returns>
        ''' <remarks>Not implemented.</remarks>
        Public Property Timeout() As TimeSpan
            Get
                'Return Me._Timeout
                Throw New NotImplementedException

            End Get
            Set(ByVal value As TimeSpan)
                'Me._Timeout = value
                Throw New NotImplementedException

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether to download files even though the local file has the same <see cref="FileHash"/> as the remote file.
        ''' </summary>
        ''' <value>Overwrite existing files.</value>
        ''' <returns>Returns whether to overwrite existing files.</returns>
        ''' <remarks></remarks>
        Public Property OverwriteExistingFiles() As Boolean
            Get
                Return Me._OverwriteExistingFiles

            End Get
            Set(ByVal value As Boolean)
                Me._OverwriteExistingFiles = value

            End Set
        End Property

        ''' <summary>
        ''' Specifies whether to save the FileHash of the downloaded file on disk.
        ''' </summary>
        ''' <value><see cref="Boolean"/> indicating whether to save the <see cref="FileHash"/>.</value>
        ''' <returns>Returns whether the <see cref="FileHash"/> is saved.</returns>
        ''' <remarks></remarks>
        Public Property SaveFileHash() As Boolean
            Get
                Return Me._SaveFileHash

            End Get
            Set(ByVal value As Boolean)
                Me._SaveFileHash = value

            End Set
        End Property

#End Region

#Region "Events"

        ''' <summary>
        ''' Event that is raised when a portion of a file is downloaded.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadProgress As EventHandler(Of EventArgs.DownloadProgressEventArgs)

        ''' <summary>
        ''' Event that is raised when the download has finished.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadCompleted As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download is aborted.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadAborted As System.EventHandler

        ''' <summary>
        ''' Event that is raised if the download fails.
        ''' </summary>
        ''' <remarks></remarks>
        Public Event DownloadFailed As System.EventHandler

#End Region

        ''' <summary>
        ''' Creates a new <see cref="FileDownloader"/> downloading from the specified <see cref="IFileProvider"/>.
        ''' </summary>
        ''' <param name="fileProvider"><see cref="IFileProvider"/> to download from.</param>
        ''' <remarks></remarks>
        Public Sub New(ByRef fileProvider As FileProvider)
            MyBase.New()

            Me._FileProvider = fileProvider

        End Sub

        ''' <summary>
        ''' Downloads the file from the <see cref="FileProvider"/>.
        ''' </summary>
        ''' <param name="path"><see cref="String"/> containing the folder path to save the downloaded file in.</param>
        ''' <returns>Return the path to the downloaded file.</returns>
        ''' <remarks></remarks>
        ''' <exception cref="DownloadAbortedException">Download was aborted by calling <see cref="Abort"/>() on the Object.</exception>
        ''' <exception cref="InvalidFileHashException">The <see cref="FileHash"/> (MD5) of the file downloaded does not equal the <see cref="FileHash"/> specified by the <see cref="IFileProvider"/>.</exception>
        Public Function DownloadFile(ByVal path As String) As String
            Try
                Dim totalSize As Long = Me.FileProvider.TotalSize
                Dim offset As Long = 0

                If Not path.EndsWith("\") Then path &= "\"
                
                Dim filepath As String = path & Me.FileProvider.Path
                Dim fileDownloadPath As String = System.IO.Path.Combine(Me.GetTempPath(path), System.IO.Path.GetFileName(Me.FileProvider.Path))
                Dim fileExists As Boolean = False
                Dim proceed As Boolean = True

                If Not Me.OverwriteExistingFiles Then
                    Dim tmpFileInfo As New FileInfo(filepath)

                    If tmpFileInfo.Exists Then
                        Dim tmpLocalHash As FileHash = FileHash.FromFile(tmpFileInfo, Me.SaveFileHash)

                        If Not Me.FileProvider.MD5 Is Nothing Then
                            If tmpLocalHash.ToString = Me.FileProvider.MD5.ToString Then
                                fileExists = True
                                proceed = False

                            End If
                        End If
                    End If

                End If

                If proceed Then
                    FileSystem.PrepareFilePath(fileDownloadPath)
                    FileSystem.DeleteFile(fileDownloadPath)

                    Dim tmpStream As Stream = File.Create(fileDownloadPath)
                    Dim tmpTimeouts As Integer = 0

                    Try
                        Dim nextPortion As Byte() = Nothing

                        While True
                            Try
                                nextPortion = Me.FileProvider.GetNextPortion(offset, Me.PortionSize)

                                tmpTimeouts = 0 'reset timeout counter since we are only looking for continuously timeouts

                                If Not nextPortion Is Nothing Then
                                    tmpStream.Write(nextPortion, 0, nextPortion.Length)

                                    offset += nextPortion.Length

                                    Me.OnDownloadProgress(New EventArgs.DownloadProgressEventArgs(Me, nextPortion.Length, offset, totalSize))

                                Else
                                    Me._DownloadFinished = True

                                    Exit While

                                End If

                            Catch ex As TimeoutException
                                tmpTimeouts += 1

                                If tmpTimeouts > Me._TimeoutsAccepted Then
                                    Dim tmpDetailedException As New TimeoutException("Error downloading file """ & filepath & """. " & offset & " of " & totalSize & " bytes received.", ex)

                                    BaseUtility.OnExceptionOccurred(Me, New BaseObjects.EventArgs.ExceptionEventArgs(tmpDetailedException, Enums.ExceptionCategories.Sockets))

                                    Throw tmpDetailedException

                                Else
                                    'DELETE: Console
                                    Console.WriteLine("FileDownloader (" & filepath & ") timeout #" & tmpTimeouts & " of " & Me._TimeoutsAccepted & " accepted")

                                    'Timeout - but continue in while loop to try again since we haven't increased the offset yet

                                End If

                            End Try

                        End While

                    Catch ex As DownloadAbortedException
                        Me._Aborted = True

                    Finally
                        tmpStream.Dispose()

                    End Try

                    If Me.Aborted Then
                        Me.OnDownloadAborted(New System.EventArgs)

                        Throw New BaseObjects.Exceptions.DownloadAbortedException

                    Else
                        Dim tmpFileInfo As New FileInfo(fileDownloadPath)
                        Dim tmpHash As FileHash = FileHash.FromFile(tmpFileInfo, True, Me.SaveFileHash)

                        If Not Me.FileProvider.MD5 Is Nothing Then
                            If tmpHash.ToString <> Me.FileProvider.MD5.ToString Then
                                Me._DownloadFinished = False

                                Me.OnDownloadFailed(New System.EventArgs)

                                Throw New InvalidFileHashException("Expected hash: " & FileProvider.MD5.ToString & " | Downloaded hash: " & tmpHash.ToString)

                            End If
                        End If
                    End If

                ElseIf fileExists Then
                    _DownloadFinished = True

                End If

                If Me.DownloadFinished Then
                    If File.Exists(fileDownloadPath) Then
                        FileSystem.PrepareFilePath(filepath)
                        FileSystem.DeleteFile(filepath)

                        System.IO.File.Move(fileDownloadPath, filepath)

                        If File.Exists(fileDownloadPath & ".md5") Then
                            FileSystem.DeleteFile(filepath & ".md5")

                            System.IO.File.Move(fileDownloadPath & ".md5", filepath & ".md5")

                        End If
                    End If

                    Me.FileProvider.DownloadComplete()
                    Me.OnDownloadCompleted(New System.EventArgs)

                End If

                Return filepath

            Finally
                Try
                    If Me.Aborted Or Not Me.DownloadFinished Then
                        'Dim filepath As String = path & Me.FileProvider.Path

                        'Dim tmpFileInfo As New FileInfo(filepath)

                        'If tmpFileInfo.Exists Then
                        '    tmpFileInfo.Delete()

                        'End If

                        Dim fileDownloadPath As String = System.IO.Path.Combine(Me.GetTempPath(path), System.IO.Path.GetFileName(Me.FileProvider.Path))
                        FileSystem.DeleteFile(fileDownloadPath)

                    End If

                Catch ex As Exception
                    'SILENT

                End Try
            End Try
        End Function

        ''' <summary>
        ''' Aborts the download.
        ''' </summary>
        ''' <remarks>This will cause a <see cref="DownloadAbortedException"/> to be thrown.</remarks>
        Public Sub Abort()
            If Not Me.Aborted Then
                Me._Aborted = True

                Me.FileProvider.Abort()

            End If

        End Sub

        Private Function GetTempPath(ByVal finalPath As String) As String
            Try
                Dim tmpTempFolderInfo As New DirectoryInfo(Path.GetTempPath)
                Dim tmpFinalFolderInfo As New DirectoryInfo(finalPath)

                If String.Compare(tmpTempFolderInfo.Root.FullName, tmpFinalFolderInfo.Root.FullName, True) = 0 Then
                    Return tmpTempFolderInfo.FullName

                Else
                    Dim tmpFinalTempFolderInfo As New DirectoryInfo(Path.Combine(tmpFinalFolderInfo.Root.FullName, "_TEMP"))

                    If Not tmpFinalTempFolderInfo.Exists Then
                        tmpFinalTempFolderInfo.Create()
                        tmpFinalTempFolderInfo.Attributes = tmpFinalTempFolderInfo.Attributes And FileAttributes.Hidden
                    End If

                    Return tmpFinalTempFolderInfo.FullName

                End If

            Catch ex As Exception
                Return Path.GetTempPath

            End Try
        End Function

        ''' <summary>
        ''' Raises the DownloadProgress Event with the specified <see cref="DownloadProgressEventArgs"/>
        ''' </summary>
        ''' <param name="e"><see cref="DownloadProgressEventArgs"/> to be raised.</param>
        ''' <remarks>If <see cref="AsyncProgressEvents"/> is True, the Event is raised in seperate threads.</remarks>
        Private Sub OnDownloadProgress(ByVal e As EventArgs.DownloadProgressEventArgs)
            If Me.AsyncProgressEvents Then
                If Not Me.DownloadProgressEvent Is Nothing Then
                    For Each tmpHandler As EventHandler(Of EventArgs.DownloadProgressEventArgs) In Me.DownloadProgressEvent.GetInvocationList
                        Dim tmpThreadData As DownloadProgressThreadData = New DownloadProgressThreadData(Me, e, tmpHandler)

                        'THREAD: ThreadManager
                        'Dim tmpThread As New Thread(AddressOf tmpThreadData.NotifyHandler)
                        'tmpThread.Name = "FileDownloader DownloadProgress Thread"
                        Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("FileDownloader DownloadProgress Thread", AddressOf tmpThreadData.NotifyHandler)
                        tmpThread.Start()

                        Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Increment()

                    Next
                End If

            Else
                RaiseEvent DownloadProgress(Me, e)

            End If

        End Sub

        ''' <summary>
        ''' Raises the DownloadCompleted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadCompleted(ByVal e As System.EventArgs)
            RaiseEvent DownloadCompleted(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadAborted Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadAborted(ByVal e As System.EventArgs)
            RaiseEvent DownloadAborted(Me, e)

        End Sub

        ''' <summary>
        ''' Raises the DownloadFailed Event with the specified <see cref="System.EventArgs" />.
        ''' </summary>
        ''' <param name="e"><see cref="System.EventArgs" /> to be raised.</param>
        ''' <remarks></remarks>
        Private Sub OnDownloadFailed(ByVal e As System.EventArgs)
            RaiseEvent DownloadFailed(Me, e)

        End Sub

        Private Class DownloadProgressThreadData

#Region "Attributes"

            Private _FileDownloader As FileDownloader
            Private _EventArgs As EventArgs.DownloadProgressEventArgs
            Private _Handler As EventHandler(Of EventArgs.DownloadProgressEventArgs)

#End Region

#Region "Properties"

            Public ReadOnly Property FileDownloader() As FileDownloader
                Get
                    Return Me._FileDownloader

                End Get
            End Property

            Public ReadOnly Property EventArgs() As EventArgs.DownloadProgressEventArgs
                Get
                    Return Me._EventArgs

                End Get
            End Property

            Public ReadOnly Property Handler() As EventHandler(Of EventArgs.DownloadProgressEventArgs)
                Get
                    Return Me._Handler

                End Get
            End Property

#End Region

            Public Sub New(ByVal fileDownloader As FileDownloader, ByVal eventArgs As EventArgs.DownloadProgressEventArgs, ByVal handler As EventHandler(Of EventArgs.DownloadProgressEventArgs))
                Me._FileDownloader = fileDownloader
                Me._EventArgs = eventArgs
                Me._Handler = handler

            End Sub

            Public Sub NotifyHandler()
                Try
                    Me.Handler.Invoke(Me.FileDownloader, Me.EventArgs)

                Catch ex As Exception
                    'SILENT

                Finally
                    Threading.ThreadPoolManager.ThreadCountPerformanceCounter.Decrement()
                End Try
            End Sub

        End Class

    End Class

End Namespace