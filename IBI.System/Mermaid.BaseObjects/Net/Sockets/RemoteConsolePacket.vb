﻿Imports System.Threading

Namespace Net.Sockets

    <System.Serializable()> Friend Class RemoteConsolePacket

#Region "Variables"

        Private _ID As Integer
        Private _Data As Object

#End Region

#Region "Properties"

        Friend Property ID() As Integer
            Get
                Return Me._ID

            End Get
            Set(ByVal value As Integer)
                Me._ID = value

            End Set
        End Property

        Public Property Data() As Object
            Get
                Return Me._Data

            End Get
            Set(ByVal value As Object)
                Me._Data = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal data As String)
            MyBase.New()

            Me._Data = data

        End Sub

        Friend Function Serialize() As System.IO.MemoryStream
            Dim tmpStream As New System.IO.MemoryStream

            Dim tmpFormatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
            tmpFormatter.AssemblyFormat = Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple

            tmpFormatter.Serialize(tmpStream, Me)

            Return tmpStream

        End Function

        Friend Shared Function Deserialize(ByVal stream As System.IO.MemoryStream) As RemoteConsolePacket
            Dim tmpBuffer() As Byte = stream.ToArray

            If Not SocketUtility.IsSimpleSerialized(tmpBuffer) Then
                Array.Clear(tmpBuffer, 0, tmpBuffer.Length)
                Array.Resize(Of Byte)(tmpBuffer, 0)

                Dim tmpFormatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                tmpFormatter.AssemblyFormat = Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple

                Return tmpFormatter.Deserialize(stream)

            Else
                Throw New NotSupportedException("Simple Serialization of RemoteConsolePacket Object not supported")

            End If

            Return Nothing

        End Function

    End Class

End Namespace