Namespace Interfaces

    Public Interface IMBRObject
        Inherits IDisposable

        Property IsDisposed() As Boolean

#Region "Events"

        Event Disposing As System.EventHandler

#End Region

    End Interface

End Namespace