﻿Namespace Diagnostics

    Friend Class PerformanceMark
        Implements IDisposable

#Region "Variables"

        Private _TimeStamp As Date
        Private _Description As String

        Private _IsStamped As Boolean
        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property TimeStamp() As Date
            Get
                Return Me._TimeStamp

            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me._Description

            End Get
        End Property

        Public ReadOnly Property IsStamped() As Boolean
            Get
                Return Me._IsStamped

            End Get
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal description As String)
            MyBase.New()

            Me._Description = description

        End Sub

        Public Sub Stamp()
            Me._TimeStamp = Date.Now
            Me._IsStamped = True

        End Sub

        Public Function Subtract(ByVal mark As PerformanceMark) As String
            If Me.IsStamped Then
                Return Me.TimeStamp.Subtract(mark.TimeStamp).TotalMilliseconds

            ElseIf Not mark.IsStamped Then
                Return "Previous mark not reached"

            Else
                Return "Mark not reached"

            End If

        End Function

#Region " IDisposable Support "

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    ' free managed resources when explicitly called
                    Me._TimeStamp = Nothing
                    Me._Description = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

#End Region

    End Class

End Namespace