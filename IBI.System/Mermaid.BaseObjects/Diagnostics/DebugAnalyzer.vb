﻿Namespace Diagnostics

    Public Class DebugAnalyzer
        Implements IDisposable

#Region "Variables"

        Private _AnalyzerName As String
        Private _DebugMarks As New System.Collections.Generic.List(Of PerformanceMark)

        Private _PerformanceLog As PerformanceLog
        Private _Enabled As Boolean

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property AnalyzerName() As String
            Get
                Return Me._AnalyzerName

            End Get
        End Property

        Private ReadOnly Property DebugMarks() As System.Collections.Generic.List(Of PerformanceMark)
            Get
                Return Me._DebugMarks

            End Get
        End Property

        Public Property PerformanceLog() As PerformanceLog
            Get
                Return Me._PerformanceLog

            End Get
            Set(ByVal value As PerformanceLog)
                Me._PerformanceLog = value

            End Set
        End Property

        Public Property Enabled() As Boolean
            Get
                Return Me._Enabled

            End Get
            Set(ByVal value As Boolean)
                Me._Enabled = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        ''' <summary>
        ''' Creates a new DebugAnalyzer.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal analyzerName As String)
            Me.New(analyzerName, Nothing)

        End Sub

        ''' <summary>
        ''' Creates a new DebugAnalyzer and writes the result to the supplied PerformanceLog.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <param name="performanceLog">PerformanceLog to write the result to</param>
        ''' <remarks>The PerformanceLog is written to when disposing the analyzer.</remarks>
        Public Sub New(ByVal analyzerName As String, ByVal performanceLog As PerformanceLog)
            Me.New(analyzerName, performanceLog, True)

        End Sub

        ''' <summary>
        ''' Creates a new DebugAnalyzer and writes the result to the supplied PerformanceLog.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <param name="performanceLog">PerformanceLog to write the result to</param>
        ''' <param name="enabled">Specifies whether writing to the PerformanceLog is enabled when disposing the analyzer</param>
        ''' <remarks>The PerformanceLog is written to when disposing the analyzer if enabled is True.</remarks>
        Public Sub New(ByVal analyzerName As String, ByVal performanceLog As PerformanceLog, ByVal enabled As Boolean)
            MyBase.New()

            Me._AnalyzerName = analyzerName
            Me._PerformanceLog = performanceLog
            Me._Enabled = True

            Me.AddDebugMark("Start")

        End Sub

        Public Sub AddDebugMark(ByVal markDescription As String)
            Dim tmpMark As New PerformanceMark(markDescription)
            tmpMark.Stamp()

            Me.DebugMarks.Add(tmpMark)

        End Sub

        Public Function GetTimeStampResult() As String
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("Analyzer: " & Me.AnalyzerName)

            For Each tmpMark As PerformanceMark In Me.DebugMarks
                sb.AppendLine(tmpMark.TimeStamp.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & ": " & tmpMark.Description)

            Next

            Return sb.ToString

        End Function

#Region " IDisposable Support "

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    If Not Me.PerformanceLog Is Nothing Then
                        If Me.Enabled Then
                            Me.PerformanceLog.WriteEntry(Me.GetTimeStampResult)

                        End If
                    End If

                    ' free managed resources when explicitly called
                    For Each tmpMark As PerformanceMark In Me.DebugMarks
                        tmpMark.Dispose()

                    Next

                    Me.DebugMarks.Clear()
                    Me._DebugMarks = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

#End Region

    End Class

End Namespace