﻿Imports System.Net.Sockets
Imports System.Net

Namespace Diagnostics

    Public Class PerformanceLogManager

#Region "Variables"

        Private Shared _Enabled As Boolean
        Private Shared _Port As Integer = 8050

        Private Shared _Client As Socket

#End Region

#Region "Properties"

        Public Shared Property Enabled() As Boolean
            Get
                Return _Enabled

            End Get
            Set(ByVal value As Boolean)
                If IsDirty(_Enabled, value) Then
                    _Enabled = value

                    OnEnabledChanged(New System.EventArgs)

                End If

            End Set
        End Property

        Public Shared Property Port() As Integer
            Get
                Return _Port

            End Get
            Set(ByVal value As Integer)
                If IsDirty(_Port, value) Then
                    _Port = value

                    OnPortChanged(New System.EventArgs)

                End If

            End Set
        End Property

#End Region

        Friend Shared Sub SendData(ByVal category As String, ByVal contextData As String, ByVal data As String)
            If Enabled Then
                'Dim tmpStream As System.IO.MemoryStream = New System.IO.MemoryStream()
                'Dim tmpEndPosition As Long

                'Dim tmpCategoryBuffer() As Byte = System.Text.Encoding.UTF8.GetBytes(category)
                'Dim tmpDataBuffer() As Byte = System.Text.Encoding.UTF8.GetBytes(data)

                'tmpEndPosition = tmpStream.Length
                'tmpStream.SetLength(tmpStream.Length + tmpCategoryBuffer.Length)
                'tmpStream.Position = tmpEndPosition
                'tmpStream.Write(tmpCategoryBuffer, 0, tmpCategoryBuffer.Length)

                'tmpEndPosition = tmpStream.Length
                'tmpStream.SetLength(tmpStream.Length + PerformanceLogUtility.ValueSplitBytes.Length)
                'tmpStream.Position = tmpEndPosition
                'tmpStream.Write(PerformanceLogUtility.ValueSplitBytes, 0, PerformanceLogUtility.ValueSplitBytes.Length)

                'tmpEndPosition = tmpStream.Length
                'tmpStream.SetLength(tmpStream.Length + tmpDataBuffer.Length)
                'tmpStream.Position = tmpEndPosition
                'tmpStream.Write(tmpDataBuffer, 0, tmpDataBuffer.Length)

                'tmpEndPosition = tmpStream.Length
                'tmpStream.SetLength(tmpStream.Length + PerformanceLogUtility.EOSBytes.Length)
                'tmpStream.Position = tmpEndPosition
                'tmpStream.Write(PerformanceLogUtility.ValueSplitBytes, 0, PerformanceLogUtility.EOSBytes.Length)

                '_Client.Send(tmpStream.ToArray)

                'tmpStream.Dispose()

                _Client.Send(System.Text.Encoding.UTF8.GetBytes(category & PerformanceLogUtility.ValueSplitString & contextData & PerformanceLogUtility.ValueSplitString & data & PerformanceLogUtility.EOSString))

            End If

        End Sub

        Private Shared Sub ConfigureSocket()
            If Not _Client Is Nothing Then
                _Client.Close()

            End If

            If Enabled Then
                _Client = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
                _Client.Connect(New IPEndPoint(IPAddress.Parse("127.0.0.1"), _Port))

            End If

        End Sub

        Private Shared Sub OnEnabledChanged(ByVal e As System.EventArgs)
            ConfigureSocket()

        End Sub

        Private Shared Sub OnPortChanged(ByVal e As System.EventArgs)
            ConfigureSocket()

        End Sub

    End Class

End Namespace