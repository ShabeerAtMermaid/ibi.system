﻿Namespace Diagnostics

    Public Class StatusRequestEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _ReceivedData As String

#End Region

#Region "Properties"

        Public ReadOnly Property ReceivedData As String
            Get
                Return Me._ReceivedData
            End Get
        End Property

        Public Property StatusCode As MonitorSocket.MonitorStatus = MonitorSocket.MonitorStatus.Running
        Public Property Description As String

#End Region

        Public Sub New(ByVal receivedData As String)
            MyBase.New()

            Me._ReceivedData = receivedData

        End Sub

    End Class

End Namespace