﻿Namespace Diagnostics

    Public Class MemoryInfoEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _PrivateBytes As Long
        Private _VirtualMemory As Long
        Private _WorkingSet As Long

#End Region

#Region "Properties"

        Public ReadOnly Property PrivateBytes As Long
            Get
                Return _PrivateBytes
            End Get
        End Property

        Public ReadOnly Property VirtualMemory As Long
            Get
                Return _VirtualMemory
            End Get
        End Property

        Public ReadOnly Property WorkingSet As Long
            Get
                Return _WorkingSet
            End Get
        End Property

#End Region

        Public Sub New(ByVal privateBytes As Long, ByVal virtualMemory As Long, ByVal workingSet As Long)
            MyBase.New()

            _PrivateBytes = privateBytes
            _VirtualMemory = virtualMemory
            _WorkingSet = workingSet

        End Sub

    End Class

End Namespace