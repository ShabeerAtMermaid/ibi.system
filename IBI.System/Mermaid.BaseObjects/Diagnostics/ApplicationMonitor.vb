﻿Imports System.Threading
Imports System.Diagnostics
Imports System.Management
Imports System.Net.Mail
Imports System.IO
Imports mermaid.BaseObjects.DataTypes

Namespace Diagnostics

    Public Class ApplicationMonitor

#Region "Variables"

        'Private _CPUMonitor As CpuUsage
        'Private _ApplicationCPUCounter As System.Diagnostics.PerformanceCounter
        Private _TotalCPUCounter As System.Diagnostics.PerformanceCounter

        Private _CPUWarningRaised As Boolean
        Private _MemoryWarningRaised As Boolean

        Private WithEvents _CheckTimer As New Timers.Timer

#End Region

#Region "Properties"

        Public Property ApplicationName As String
        Public Property LogsEnabled As Boolean = True

        Public Property CPUWarningLimit As Integer = 100
        Public Property PrivateBytesWarningLimit As Long = 1.4 * GB
        Public Property VirtualMemoryWarningLimit As Long = 1.4 * GB
        Public Property WorkingSetWarningLimit As Long = 1.4 * GB

        Public Property AdditionalMonitoredProcesses As List(Of String) = New List(Of String)
        Private Property AdditionalMonitoredProcessesPerformanceCounters As Hashtable = New Hashtable

        Public Property AutoMailSettings As MailSettings

#End Region

#Region "Constants"

        Public Const B As Long = 1
        Public Const KB As Long = 1024 * B
        Public Const MB As Long = 1024 * KB
        Public Const GB As Long = 1024 * MB

#End Region

#Region "Events"

        Public Event CPUWarning As System.EventHandler(Of CPUInfoEventArgs)
        Public Event MemoryWarning As System.EventHandler(Of MemoryInfoEventArgs)

        Private Sub OnCPUWarning(ByVal e As CPUInfoEventArgs)
            RaiseEvent CPUWarning(Me, e)

        End Sub

        Private Sub OnMemoryWarning(ByVal e As MemoryInfoEventArgs)
            RaiseEvent MemoryWarning(Me, e)

        End Sub

#End Region

        Public Sub New(ByVal applicationName As String, ByVal checkInterval As TimeSpan)
            Me.ApplicationName = applicationName

            '_CPUMonitor = CpuUsage.Create()
            '_ApplicationCPUCounter = New System.Diagnostics.PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess.ProcessName, True)
            '_ApplicationCPUCounter.NextValue()

            _TotalCPUCounter = New System.Diagnostics.PerformanceCounter("Processor", "% Processor Time", "_Total", True)
            _TotalCPUCounter.NextValue()

            Me._CheckTimer.Interval = checkInterval.TotalMilliseconds
            Me._CheckTimer.Enabled = True

        End Sub

        Private Sub CheckTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _CheckTimer.Elapsed
            Try
                Me.PerformSystemCheck()

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Misc))
            End Try
        End Sub

        Public Sub PerformSystemCheck()
            Dim tmpCurrentProcessName As String = Process.GetCurrentProcess.ProcessName

            'CPU Checks
            'Dim currentApplicationCPUUsage As Integer = Me.GetCurrentApplicationCPUUsage
            Dim currentApplicationCPUUsage As Integer = Me.GetCurrentProcessCPUUsage(tmpCurrentProcessName)
            Dim currentTotalCPUUsage As Integer = Me.GetCurrentTotalCPUUsage

            Dim hasCPUWarning As Boolean = currentApplicationCPUUsage > Me.CPUWarningLimit
            Dim isBelow90PercentCPUWarning As Boolean = currentApplicationCPUUsage < (Me.CPUWarningLimit * 0.9)

            Dim hasRaisedCPUWarning As Boolean = False

            'Console.WriteLine("Processor: " & currentApplicationCPUUsage & " | " & currentTotalCPUUsage)

            'CPU Warning disabled
            hasCPUWarning = False

            If hasCPUWarning Then
                If Not _CPUWarningRaised Then
                    Me.OnCPUWarning(New CPUInfoEventArgs(currentApplicationCPUUsage, currentTotalCPUUsage))
                    hasRaisedCPUWarning = True
                End If

                _CPUWarningRaised = True
            Else
                _CPUWarningRaised = False
            End If

            If LogsEnabled Then
                Dim tmpLogLine As String = "Warning: " & hasCPUWarning & "|Total;" & currentTotalCPUUsage
                tmpLogLine &= "|" & tmpCurrentProcessName & ";" & currentApplicationCPUUsage

                If Not Me.AdditionalMonitoredProcesses Is Nothing Then
                    For Each tmpProcessName As String In Me.AdditionalMonitoredProcesses
                        Dim tmpProcessCPUUsage As Integer = Me.GetCurrentProcessCPUUsage(tmpProcessName)

                        tmpLogLine &= "|" & tmpProcessName & ";" & tmpProcessCPUUsage
                    Next
                End If

                Logger.WriteLine("CPU", tmpLogLine)
            End If

            'Memory Checks
            Dim currentPrivateBytesSize As Long = Me.GetCurrentPrivateBytesSize
            Dim currentVirtualMemorySize As Long = Me.GetCurrentVirtualMemorySize
            Dim currentWorkingSetSize As Long = Me.GetCurrentWorkingSetSize

            Dim hasMemoryWarning As Boolean = currentPrivateBytesSize > Me.PrivateBytesWarningLimit Or _
                                              currentVirtualMemorySize > Me.VirtualMemoryWarningLimit Or _
                                              currentWorkingSetSize > Me.WorkingSetWarningLimit
            Dim isBelow90PercentMemoryWarning As Boolean = currentPrivateBytesSize < (Me.PrivateBytesWarningLimit * 0.9) Or _
                                                          currentVirtualMemorySize < (Me.VirtualMemoryWarningLimit * 0.9) Or _
                                                          currentWorkingSetSize < (Me.WorkingSetWarningLimit * 0.9)

            Dim hasRaisedMemoryWarning As Boolean = False

            If hasMemoryWarning Then
                If Not _MemoryWarningRaised Then
                    Me.OnMemoryWarning(New MemoryInfoEventArgs(currentPrivateBytesSize, currentVirtualMemorySize, currentWorkingSetSize))
                    hasRaisedMemoryWarning = True
                End If

                _MemoryWarningRaised = True
            ElseIf isBelow90PercentMemoryWarning Then
                _MemoryWarningRaised = False
            End If

            If LogsEnabled Then
                Dim tmpLogLine As String = "Warning: " & hasMemoryWarning & "|Total;" & 0 & ";" & My.Computer.Info.TotalVirtualMemory - My.Computer.Info.AvailableVirtualMemory & ";" & 0
                tmpLogLine &= "|" & tmpCurrentProcessName & ";" & currentPrivateBytesSize & ";" & currentVirtualMemorySize & ";" & currentWorkingSetSize

                If Not Me.AdditionalMonitoredProcesses Is Nothing Then
                    For Each tmpProcessName As String In Me.AdditionalMonitoredProcesses
                        Dim tmpProcessPrivateBytesSize As Long = Me.GetProcessPrivateBytesSize(tmpProcessName)
                        Dim tmpProcessVirtualMemorySize As Long = Me.GetProcessVirtualMemorySize(tmpProcessName)
                        Dim tmpProcessWorkingSetSize As Long = Me.GetProcessWorkingSetSize(tmpProcessName)

                        tmpLogLine &= "|" & tmpProcessName & ";" & tmpProcessPrivateBytesSize & ";" & tmpProcessVirtualMemorySize & ";" & tmpProcessWorkingSetSize
                    Next
                End If

                Logger.WriteLine("Memory", tmpLogLine)

            End If

            If hasRaisedCPUWarning Or hasRaisedMemoryWarning Then
                If Not Me.AutoMailSettings Is Nothing Then
                    If Me.AutoMailSettings.MailReceivers.Count > 0 Then
                        Dim tmpMailBody As String = BaseUtility.ExtractEmbeddedResource("ApplicationMonitor_MailTemplate.htm")
                        tmpMailBody = tmpMailBody.Replace("{0}", My.Computer.Name)
                        tmpMailBody = tmpMailBody.Replace("{1}", Me.ApplicationName)
                        'tmpMailBody = tmpMailBody.Replace("{2}", My.Application.Info.AssemblyName)
                        tmpMailBody = tmpMailBody.Replace("{2}", Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location))
                        tmpMailBody = tmpMailBody.Replace("{3}", ProgramVersion.CurrentProgramVersion.ToString)
                        tmpMailBody = tmpMailBody.Replace("{4}", Date.Now.Subtract(Process.GetCurrentProcess.StartTime).ToString)

                        tmpMailBody = tmpMailBody.Replace("{10}", currentApplicationCPUUsage & " %")
                        tmpMailBody = tmpMailBody.Replace("{11}", currentTotalCPUUsage & " %")
                        tmpMailBody = tmpMailBody.Replace("{12}", New FileSize(currentPrivateBytesSize).ToString)
                        tmpMailBody = tmpMailBody.Replace("{13}", New FileSize(currentVirtualMemorySize).ToString)
                        tmpMailBody = tmpMailBody.Replace("{14}", New FileSize(currentWorkingSetSize).ToString)

                        Dim tmpSMTP As New SmtpClient(Me.AutoMailSettings.MailServer)

                        Dim tmpMessage As New MailMessage
                        tmpMessage.From = New System.Net.Mail.MailAddress(Me.AutoMailSettings.FromAddress, Me.AutoMailSettings.FromName)

                        For Each tmpReceiver As String In Me.AutoMailSettings.MailReceivers
                            tmpMessage.To.Add(tmpReceiver)
                        Next

                        tmpMessage.Subject = Me.AutoMailSettings.MailSubject
                        tmpMessage.IsBodyHtml = True
                        tmpMessage.Body = tmpMailBody

                        tmpSMTP.SendAsync(tmpMessage, Nothing)

                    End If
                End If
            End If
        End Sub

        'Private Function GetCurrentApplicationCPUUsage() As Integer
        '    'Dim query As New ObjectQuery("SELECT Name,PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process")
        '    'Dim searcher As New ManagementObjectSearcher(Query)
        '    'Dim queryCollection As ManagementObjectCollection

        '    'queryCollection = searcher.Get()

        '    'For Each m As ManagementObject In queryCollection
        '    '    Dim proc_name As String = m("Name").ToString()

        '    '    Dim D1 As ULong = Convert.ToUInt64(m("PercentProcessorTime"))
        '    '    If String.Compare(Process.GetCurrentProcess.ProcessName, proc_name, True) = 0 Then
        '    '        Return D1
        '    '    End If
        '    'Next
        '    Return _ApplicationCPUCounter.NextValue / Environment.ProcessorCount

        'End Function

        Private Function GetCurrentProcessCPUUsage(ByVal processName As String) As Integer
            Try
                Dim tmpPerformanceCounter As System.Diagnostics.PerformanceCounter = Me.AdditionalMonitoredProcessesPerformanceCounters(processName)

                If tmpPerformanceCounter Is Nothing Then
                    tmpPerformanceCounter = New System.Diagnostics.PerformanceCounter("Process", "% Processor Time", processName, True)
                    tmpPerformanceCounter.NextValue()

                    Me.AdditionalMonitoredProcessesPerformanceCounters(processName) = tmpPerformanceCounter
                End If

                Return tmpPerformanceCounter.NextValue / Environment.ProcessorCount

            Catch ex As Exception
                Return 0
            End Try
        End Function

        Private Function GetCurrentTotalCPUUsage() As Integer
            ''Dim query As New ObjectQuery("SELECT Name,PercentProcessorTime FROM Win32_PerfFormattedData_PerfProc_Process")
            'Dim query As New ObjectQuery("SELECT Name,PercentProcessorTime FROM Win32_PerfRawData_PerfOS_Processor")
            'Dim searcher As New ManagementObjectSearcher(query)
            'Dim queryCollection As ManagementObjectCollection

            'queryCollection = searcher.Get()

            'Dim cpuUsage As ULong = 0

            'For Each m As ManagementObject In queryCollection
            '    Dim proc_name As String = m("Name").ToString()

            '    Dim D1 As ULong = Convert.ToUInt64(m("PercentProcessorTime"))
            '    If String.Compare("Idle", proc_name, True) <> 0 And String.Compare("_Total", proc_name, True) <> 0 Then
            '        cpuUsage += D1
            '    End If
            'Next
            'Console.WriteLine("Process: {0} Uses: {1}", "All", cpuUsage)

            'Return cpuUsage

            'Dim cpuCounter As New System.Diagnostics.PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess.ProcessName, True)
            'cpuCounter.NextValue()
            Return _TotalCPUCounter.NextValue ' / Environment.ProcessorCount

        End Function

        Private Function GetCurrentPrivateBytesSize() As Long
            Return Process.GetCurrentProcess.PrivateMemorySize64

        End Function

        Private Function GetCurrentVirtualMemorySize() As Long
            Return Process.GetCurrentProcess.VirtualMemorySize64

        End Function

        Private Function GetCurrentWorkingSetSize() As Long
            Return Process.GetCurrentProcess.WorkingSet64

        End Function

        Private Function GetProcessPrivateBytesSize(ByVal processName As String) As Long
            Dim tmpUsage As Long = 0

            For Each tmpProcess As Process In Process.GetProcessesByName(processName)
                tmpUsage += tmpProcess.PrivateMemorySize64
            Next

            Return tmpUsage

        End Function

        Private Function GetProcessVirtualMemorySize(ByVal processName As String) As Long
            Dim tmpUsage As Long = 0

            For Each tmpProcess As Process In Process.GetProcessesByName(processName)
                tmpUsage += tmpProcess.VirtualMemorySize64
            Next

            Return tmpUsage

        End Function

        Private Function GetProcessWorkingSetSize(ByVal processName As String) As Long
            Dim tmpUsage As Long = 0

            For Each tmpProcess As Process In Process.GetProcessesByName(processName)
                tmpUsage += tmpProcess.WorkingSet64
            Next

            Return tmpUsage

        End Function

        Public Class MailSettings

#Region "Properties"

            Public Property MailServer As String = "mail.mermaid.dk"
            Public Property FromName As String = "System Monitor"
            Public Property FromAddress As String = "noreply@mermaid.dk"
            Public Property MailSubject As String = "System Alert"
            Public Property MailReceivers As New List(Of String)

#End Region

        End Class

    End Class

End Namespace