﻿Namespace Diagnostics

    Public Class PerformanceAnalyzer
        Implements IDisposable

#Region "Variables"

        Private _AnalyzerName As String
        Private _PerformanceMarks As New System.Collections.Generic.List(Of PerformanceMark)

        Private _PerformanceLog As PerformanceLog
        Private _AcceptedDuration As TimeSpan = TimeSpan.FromMilliseconds(-1)

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property AnalyzerName() As String
            Get
                Return Me._AnalyzerName

            End Get
        End Property

        Private ReadOnly Property PerformanceMarks() As System.Collections.Generic.List(Of PerformanceMark)
            Get
                Return Me._PerformanceMarks

            End Get
        End Property

        Public Property PerformanceLog() As PerformanceLog
            Get
                Return Me._PerformanceLog

            End Get
            Set(ByVal value As PerformanceLog)
                Me._PerformanceLog = value

            End Set
        End Property

        Public Property AcceptedDuration() As TimeSpan
            Get
                Return Me._AcceptedDuration

            End Get
            Set(ByVal value As TimeSpan)
                Me._AcceptedDuration = value

            End Set
        End Property

        Public ReadOnly Property TotalDuration() As TimeSpan
            Get
                Using New Threading.Lock(Me.PerformanceMarks, True)
                    If Me.PerformanceMarks.Count > 1 Then
                        Dim tmpFirstMark As PerformanceMark = Me.PerformanceMarks(0)
                        Dim tmpLastMark As PerformanceMark = Me.PerformanceMarks(Me.PerformanceMarks.Count - 1)

                        Return tmpLastMark.TimeStamp.Subtract(tmpFirstMark.TimeStamp)

                    End If

                    Return TimeSpan.Zero

                End Using

            End Get
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        ''' <summary>
        ''' Creates a new PerformanceAnalyzer.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <remarks></remarks>
        Public Sub New(ByVal analyzerName As String)
            Me.New(analyzerName, Nothing, TimeSpan.FromMilliseconds(-1))

        End Sub

        ''' <summary>
        ''' Creates a new PerformanceAnalyzer and writes the result to the supplied PerformanceLog.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <param name="performanceLog">PerformanceLog to write the result to</param>
        ''' <remarks>The PerformanceLog is written to when disposing the analyzer.</remarks>
        Public Sub New(ByVal analyzerName As String, ByVal performanceLog As PerformanceLog)
            Me.New(analyzerName, performanceLog, TimeSpan.FromMilliseconds(-1))

        End Sub

        ''' <summary>
        ''' Creates a new PerformanceAnalyzer and writes the result to the supplied PerformanceLog if the total duration exceeds the accepted duration.
        ''' </summary>
        ''' <param name="analyzerName">Name of the analyzer</param>
        ''' <param name="performanceLog">PerformanceLog to write the result to</param>
        ''' <param name="acceptedDuration">Accepted total duration before writing to the PerformanceLog (TimeSpan.Zero can be used to deactivate the PerformanceLog)</param>
        ''' <remarks>The PerformanceLog is written to when disposing the analyzer.</remarks>
        Public Sub New(ByVal analyzerName As String, ByVal performanceLog As PerformanceLog, ByVal acceptedDuration As TimeSpan)
            MyBase.New()

            Me._AnalyzerName = analyzerName
            Me._PerformanceLog = performanceLog
            Me._AcceptedDuration = acceptedDuration

            Me.AddPerformanceMark("Start")

        End Sub

        Public Sub AddPerformanceMark(ByVal markDescription As String)
            Dim tmpMark As New PerformanceMark(markDescription)
            tmpMark.Stamp()

            Using New Threading.Lock(Me.PerformanceMarks, True)
                Me.PerformanceMarks.Add(tmpMark)
            End Using

        End Sub

        Public Function GetDifferentialResult() As String

            Dim tmpResult As New ArrayList
            tmpResult.Add("Analyzer: " & Me.AnalyzerName)

            Using New Threading.Lock(Me.PerformanceMarks, True)
                If Me.PerformanceMarks.Count > 1 Then
                    Dim tmpPrevMark As PerformanceMark = Me.PerformanceMarks(0)
                    Dim tmpMark As PerformanceMark = Nothing

                    For i As Integer = 1 To Me.PerformanceMarks.Count - 1
                        tmpMark = Me.PerformanceMarks(i)

                        tmpResult.Add(tmpMark.Description & ": " & tmpMark.Subtract(tmpPrevMark))

                        tmpPrevMark = tmpMark

                    Next

                    tmpResult.Add("Total: " & tmpMark.Subtract(Me.PerformanceMarks(0)))

                End If

            End Using

            Dim sb As New System.Text.StringBuilder

            For Each tmpLine As String In tmpResult
                sb.AppendLine(tmpLine)

            Next

            Return sb.ToString

        End Function

        Public Function GetTimeStampResult() As String
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("Analyzer: " & Me.AnalyzerName)

            Using New Threading.Lock(Me.PerformanceMarks, True)
                For Each tmpMark As PerformanceMark In Me.PerformanceMarks
                    'sb.AppendLine(tmpMark.Description & ": " & tmpMark.TimeStamp.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture))
                    sb.AppendLine(tmpMark.TimeStamp.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "> " & tmpMark.Description)

                Next
            End Using

            Return sb.ToString

        End Function

#Region " IDisposable Support "

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    If Not Me.PerformanceLog Is Nothing Then
                        If Me.AcceptedDuration <> TimeSpan.Zero Then
                            If Me.TotalDuration > Me.AcceptedDuration Then
                                Me.PerformanceLog.WriteEntry(Me.GetDifferentialResult)

                            End If
                        End If
                    End If

                    ' free managed resources when explicitly called
                    Using New Threading.Lock(Me.PerformanceMarks, True)
                        For Each tmpMark As PerformanceMark In Me.PerformanceMarks
                            tmpMark.Dispose()
                        Next
                    End Using

                    Me.PerformanceMarks.Clear()
                    Me._PerformanceMarks = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

#End Region

    End Class

End Namespace