#Region "HashTable implementation"

Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.Win32

Namespace Diagnostics

    Public Class PerformanceCounterManager

#Region "Variables"

        Private Shared _PerformanceCounters As PerformanceCounterCollection
        Private Shared _CategoryName As String = String.Empty
        Private Shared _Timer As New Threading.SharedTimer(TimeSpan.FromMilliseconds(500))

        Private Shared _Lock As New Object

#End Region

#Region "Properties"

        Public Shared ReadOnly Property PerformanceCounters() As PerformanceCounterCollection
            Get
                If _PerformanceCounters Is Nothing Then
                    _PerformanceCounters = New PerformanceCounterCollection

                End If

                Return _PerformanceCounters

            End Get
        End Property

        Public Shared Property CategoryName() As String
            Get
                Return _CategoryName

            End Get
            Set(ByVal value As String)
                _CategoryName = value

            End Set
        End Property

        Public Shared ReadOnly Property Timer() As Threading.SharedTimer
            Get
                Return _Timer

            End Get
        End Property

        Private Shared ReadOnly Property Lock() As Object
            Get
                Return _Lock

            End Get
        End Property

        Friend Shared ReadOnly Property ThreadPool() As Threading.ThreadPool
            Get
                Return Threading.ThreadPoolManager.GetThreadPool("PerformanceCounterThreadPool")

            End Get
        End Property

#End Region

#Region "Events"

        Public Shared Event PerformanceCounterValueChanged As System.EventHandler(Of EventArgs.PerformanceCounterValueChangedEventArgs)
       
#End Region

        Private Shared Sub AddPerformanceCounter(ByVal performanceCounter As PerformanceCounter)
            If Not performanceCounter Is Nothing Then
                If Not PerformanceCounters.ContainsKey(performanceCounter.Name) Then
                    PerformanceCounters(performanceCounter.Name) = performanceCounter

                    'Make sure that the shared timer is running

                    Timer.Enabled = True

                ElseIf Not PerformanceCounters(performanceCounter.Name) Is performanceCounter Then
                    Throw New ConstraintException("A PerformanceCounter with name """ & performanceCounter.Name & """ already exists.")

                End If

            Else
                Throw New NullReferenceException("Parameter performanceCounter cannot be null.")

            End If

        End Sub

        Public Shared Function GetPerformanceCounter(ByVal name As String) As PerformanceCounter
            Monitor.Enter(Lock)

            Try
                Dim tmpPerformanceCounter As PerformanceCounter = Nothing

                If PerformanceCounters.ContainsKey(name) Then
                    tmpPerformanceCounter = PerformanceCounters(name)

                End If

                If tmpPerformanceCounter Is Nothing Then
                    tmpPerformanceCounter = New PerformanceCounter(name)

                    AddPerformanceCounter(tmpPerformanceCounter)

                End If

                Return tmpPerformanceCounter

            Finally
                Monitor.Exit(Lock)

            End Try
        End Function

        Public Shared Sub Reset()
            For Each tmpPerformanceCounter As PerformanceCounter In PerformanceCounters.Values
                tmpPerformanceCounter.SetValue(0L)

            Next

            Dim tmpKeySoftware As RegistryKey = My.Computer.Registry.LocalMachine.CreateSubKey("Software")
            Dim tmpKeyCompany As RegistryKey = tmpKeySoftware.CreateSubKey("mermaid technology")
            Dim tmpKeyFinal As RegistryKey = tmpKeyCompany.CreateSubKey("mermaid Performance Counters")
         
            If Not String.IsNullOrEmpty(PerformanceCounterManager.CategoryName) Then
                tmpKeyFinal = tmpKeyFinal.CreateSubKey(PerformanceCounterManager.CategoryName)

            End If

            Dim tmpValues() As String = tmpKeyFinal.GetValueNames

            For Each tmpValueName As String In tmpValues
                'tmpKeyFinal.SetValue(tmpValueName, 0L)
                tmpKeyFinal.DeleteValue(tmpValueName)

            Next

        End Sub

        Friend Shared Sub OnPerformanceCounterValueChanged(ByVal sender As Object, ByVal e As EventArgs.PerformanceCounterValueChangedEventArgs)
            Try
                RaiseEvent PerformanceCounterValueChanged(sender, e)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(sender, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.PerformanceCounters, "PerformanceCounter name: " & e.PerformanceCounter.Name))

            End Try
        End Sub

    End Class

End Namespace

#End Region

#Region "PerformanceCounters implementation"

'Imports System.Diagnostics
'Imports System.Threading

'Namespace Diagnostics

'    Public Class PerformanceCounters

'#Region "Variables"

'        Private Shared _Category As PerformanceCounterCategory
'        Private Shared _ValidatedCounters As New ArrayList
'        Private Shared _Lock As New Object

'#End Region

'#Region "Properties"

'        Private Shared Property Category() As PerformanceCounterCategory
'            Get
'                Return _Category

'            End Get
'            Set(ByVal value As PerformanceCounterCategory)
'                _Category = value

'            End Set
'        End Property

'        Private Shared ReadOnly Property ValidatedCounters() As ArrayList
'            Get
'                Return _ValidatedCounters

'            End Get
'        End Property

'        Private Shared Property Lock() As Object
'            Get
'                Return _Lock

'            End Get
'            Set(ByVal value As Object)
'                _Lock = value

'            End Set
'        End Property

'#End Region

'        Public Shared Function Increment(ByVal counterName As String) As Long
'            Monitor.Enter(Lock)

'            Try
'                ValidateCounter(counterName)

'                Dim tmpCounter As PerformanceCounter = New PerformanceCounter(My.Application.Info.AssemblyName, counterName, False)

'                tmpCounter.Increment()

'                Dim tmpValue As Long = tmpCounter.RawValue

'                Console.WriteLine(counterName & ": " & tmpValue)

'                Return tmpValue

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Function

'        Public Shared Function Decrement(ByVal counterName As String) As Long
'            Monitor.Enter(Lock)

'            Try
'                ValidateCounter(counterName)

'                Dim tmpCounter As PerformanceCounter = New PerformanceCounter(My.Application.Info.AssemblyName, counterName, False)

'                tmpCounter.Decrement()

'                Return tmpCounter.RawValue

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Function

'        Public Shared Function GetValue(ByVal counterName As String) As Long
'            Monitor.Enter(Lock)

'            Try
'                ValidateCounter(counterName)

'                Dim tmpCounter As PerformanceCounter = New PerformanceCounter(My.Application.Info.AssemblyName, counterName, True)

'                Return tmpCounter.RawValue

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Function

'        Public Shared Sub SetValue(ByVal counterName As String, ByVal value As Long)
'            Monitor.Enter(Lock)

'            Try
'                ValidateCounter(counterName)

'                Dim tmpCounter As PerformanceCounter = New PerformanceCounter(My.Application.Info.AssemblyName, counterName, False)

'                tmpCounter.RawValue = value

'            Finally
'                Monitor.Exit(Lock)

'            End Try
'        End Sub

'        'Private Shared Sub ValidateCounterCategory()
'        '    If Not PerformanceCounterCategory.Exists(My.Application.Info.AssemblyName) Then
'        '        Dim ccds As New CounterCreationDataCollection

'        '        Dim tmpCategory As PerformanceCounterCategory = PerformanceCounterCategory.Create(My.Application.Info.AssemblyName, String.Empty, PerformanceCounterCategoryType.Unknown, ccds)

'        '    End If

'        'End Sub

'        Public Shared Sub SetupCategory(ByVal counters As CounterCreationDataCollection)
'            If PerformanceCounterCategory.Exists(My.Application.Info.AssemblyName) Then PerformanceCounterCategory.Delete(My.Application.Info.AssemblyName)
'            Category = PerformanceCounterCategory.Create(My.Application.Info.AssemblyName, "", PerformanceCounterCategoryType.SingleInstance, counters)

'            For Each tmpCounterData As CounterCreationData In counters
'                ValidatedCounters.Add(tmpCounterData.CounterName)

'            Next

'        End Sub

'        Private Shared Sub ValidateCounter(ByVal counterName As String)
'            If Not ValidatedCounters.Contains(counterName) Then
'                Dim tmpCreationData As New CounterCreationData()
'                tmpCreationData.CounterName = counterName
'                tmpCreationData.CounterType = PerformanceCounterType.NumberOfItems64

'                AddPerformanceCounter(tmpCreationData, My.Application.Info.AssemblyName)

'                ValidatedCounters.Add(counterName)

'            End If

'        End Sub

'        Private Shared Sub AddPerformanceCounter(ByVal performanceCounter As CounterCreationData, ByVal categoryName As String)
'            Try
'                Dim tmpPerformanceCounterList As New CounterCreationDataCollection
'                Dim tmpValueList As New Hashtable

'                Dim tmpNewCounter As CounterCreationData
'                tmpPerformanceCounterList.Add(performanceCounter)

'                If PerformanceCounterCategory.Exists(categoryName) Then
'                    Dim tmpCategory As New PerformanceCounterCategory
'                    tmpCategory.CategoryName = categoryName

'                    Dim tmpCounter() As PerformanceCounter = tmpCategory.GetCounters()

'                    For Each tmpPerf As PerformanceCounter In tmpCounter
'                        If Not tmpPerf.CounterName = performanceCounter.CounterName Then
'                            tmpNewCounter = New CounterCreationData
'                            tmpNewCounter.CounterName = tmpPerf.CounterName
'                            tmpNewCounter.CounterType = tmpPerf.CounterType

'                            tmpValueList(tmpPerf.CounterName) = tmpPerf.RawValue
'                            tmpPerformanceCounterList.Add(tmpNewCounter)

'                        End If

'                        tmpPerf.Dispose()

'                    Next

'                    PerformanceCounterCategory.Delete(categoryName)

'                End If

'                'If Not PerformanceCounterCategory.Exists(categoryName) Then
'                Category = PerformanceCounterCategory.Create(categoryName, "", PerformanceCounterCategoryType.SingleInstance, tmpPerformanceCounterList)

'                Dim tmpCounterList() As PerformanceCounter = Category.GetCounters()

'                For Each tmpPerf As PerformanceCounter In tmpCounterList
'                    If Not tmpPerf.CounterName = performanceCounter.CounterName Then
'                        If tmpValueList.ContainsKey(tmpPerf.CounterName) Then
'                            Dim tmpValueCounter As PerformanceCounter = New PerformanceCounter(categoryName, tmpPerf.CounterName, False)
'                            tmpValueCounter.RawValue = tmpValueList(tmpPerf.CounterName)

'                        End If

'                    End If
'                Next

'                'End If

'            Catch ex As Exception
'                ' SILENT

'            End Try
'        End Sub

'    End Class

'End Namespace

#End Region