﻿Imports System.IO

Namespace Diagnostics

    Public Class Logger

        Public Shared Sub WriteLine(ByVal logName As String, ByVal message As String)
            Write(logName, Date.Now.ToString("HH:mm:ss.fff> ") & message & vbCrLf)

        End Sub

        Public Shared Sub Write(ByVal logName As String, ByVal message As String)
            Try
                Dim tmpFileName As String = Path.Combine(My.Application.Info.DirectoryPath, "Logs\" & logName & "_" & Date.Now.ToString("yyyyMMdd") & ".log")

                mermaid.BaseObjects.IO.FileSystem.PrepareFilePath(tmpFileName)

                File.AppendAllText(tmpFileName, message)

            Catch ex As Exception
                'SILENT

            End Try
        End Sub

        Public Shared Sub ClearOldLogs(ByVal logName As String, ByVal expireDays As Integer)
            Try
                Dim tmpLogDirectoryInfo As New DirectoryInfo(Path.Combine(My.Application.Info.DirectoryPath, "Logs"))

                mermaid.BaseObjects.IO.FileSystem.PrepareDirectoryPath(tmpLogDirectoryInfo)

                For Each tmpLogFileInfo As FileInfo In tmpLogDirectoryInfo.GetFiles(logName & "_*.log")
                    Dim tmpYear As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 12, 4)
                    Dim tmpMonth As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 8, 2)
                    Dim tmpDay As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 6, 2)

                    Dim tmpLogDate As Date = New Date(tmpYear, tmpMonth, tmpDay, 0, 0, 0)

                    If tmpLogDate.AddDays(expireDays) < Date.Now Then
                        mermaid.BaseObjects.IO.FileSystem.DeleteFile(tmpLogFileInfo)

                    End If
                Next
            Catch ex As Exception
                'SILENT

            End Try
        End Sub

        Public Shared Sub ClearOldLogs(ByVal expireDays As Integer)
            Try
                Dim tmpLogDirectoryInfo As New DirectoryInfo(Path.Combine(My.Application.Info.DirectoryPath, "Logs"))

                mermaid.BaseObjects.IO.FileSystem.PrepareDirectoryPath(tmpLogDirectoryInfo)

                For Each tmpLogFileInfo As FileInfo In tmpLogDirectoryInfo.GetFiles("*_*.log")
                    Dim tmpYear As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 12, 4)
                    Dim tmpMonth As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 8, 2)
                    Dim tmpDay As Integer = tmpLogFileInfo.Name.Substring(tmpLogFileInfo.Name.Length - 6, 2)

                    Dim tmpLogDate As Date = New Date(tmpYear, tmpMonth, tmpDay, 0, 0, 0)

                    If tmpLogDate.AddDays(expireDays) < Date.Now Then
                        mermaid.BaseObjects.IO.FileSystem.DeleteFile(tmpLogFileInfo)

                    End If
                Next
            Catch ex As Exception
                'SILENT

            End Try
        End Sub

    End Class

End Namespace