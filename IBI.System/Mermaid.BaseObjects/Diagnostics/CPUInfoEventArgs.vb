﻿Namespace Diagnostics

    Public Class CPUInfoEventArgs
        Inherits System.EventArgs

#Region "Variables"

        Private _CurrentProcessUsage As Integer
        Private _TotalUsage As Integer

#End Region

#Region "Properties"

        Public ReadOnly Property CurrentProcessUsage As Integer
            Get
                Return _CurrentProcessUsage
            End Get
        End Property

        Public ReadOnly Property TotalUsage As Integer
            Get
                Return _TotalUsage
            End Get
        End Property

#End Region

        Public Sub New(ByVal currentProcessUsage As Integer, ByVal totalUsage As Integer)
            MyBase.New()

            _CurrentProcessUsage = currentProcessUsage
            _CurrentProcessUsage = totalUsage

        End Sub

    End Class

End Namespace