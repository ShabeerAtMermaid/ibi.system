﻿Namespace Diagnostics

    Public Class PerformanceLogUtility

#Region "Variables"

        Private Shared _ValueSplitBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$>")
        Private Shared _EOSBytes() As Byte = System.Text.Encoding.UTF8.GetBytes("<$EOS$>")

#End Region

#Region "Properties"

        ''' <summary>
        ''' ValueSplit byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property ValueSplitBytes() As Byte()
            Get
                Return _ValueSplitBytes

            End Get
        End Property

        ''' <summary>
        ''' EndOfStream byte sequence.
        ''' </summary>
        Public Shared ReadOnly Property EOSBytes() As Byte()
            Get
                Return _EOSBytes

            End Get
        End Property

#End Region

#Region "Constants"

        Public Const ValueSplitString As String = "<$>"
        Public Const EOSString As String = "<$EOS$>"

#End Region

        Public Shared Function IndexOfEOS(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - PerformanceLogUtility.EOSBytes.Length
                    For y As Integer = 0 To PerformanceLogUtility.EOSBytes.Length - 1
                        If buffer(i + y) = PerformanceLogUtility.EOSBytes(y) Then
                            If y = PerformanceLogUtility.EOSBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfEOS(ByVal buffer As String) As Long
            If Not buffer Is Nothing Then
                Return buffer.IndexOf(EOSString)

            End If

        End Function

        Public Shared Function IndexOfValueSplit(ByVal buffer() As Byte) As Long
            If Not buffer Is Nothing Then
                For i As Integer = 0 To buffer.Length - PerformanceLogUtility.ValueSplitBytes.Length
                    For y As Integer = 0 To PerformanceLogUtility.ValueSplitBytes.Length - 1
                        If buffer(i + y) = PerformanceLogUtility.ValueSplitBytes(y) Then
                            If y = PerformanceLogUtility.ValueSplitBytes.Length - 1 Then
                                Return i

                            End If
                        Else
                            Exit For

                        End If
                    Next
                Next
            End If

            Return -1

        End Function

        Public Shared Function IndexOfValueSplit(ByVal buffer As String) As Long
            If Not buffer Is Nothing Then
                Return buffer.IndexOf(ValueSplitString)

            End If

        End Function

    End Class

End Namespace