﻿Imports System.Net.Sockets
Imports System.Net
Imports System.Threading

Namespace Diagnostics

    Public Class PerformanceLogReader

#Region "Variables"

        Private _Enabled As Boolean
        Private _Port As Integer = DefaultPort

        Private _Socket As Socket
        Private _ReceiveBuffer As String

        Private _Data As DataSet
        Private _ValidatedCategoryPaths As New System.Collections.Generic.List(Of String)

#End Region

#Region "Properties"

        Public Property Enabled() As Boolean
            Get
                Return _Enabled

            End Get
            Set(ByVal value As Boolean)
                If IsDirty(_Enabled, value) Then
                    _Enabled = value

                    OnEnabledChanged(New System.EventArgs)

                End If

            End Set
        End Property

        Public Property Port() As Integer
            Get
                Return _Port

            End Get
            Set(ByVal value As Integer)
                If IsDirty(_Port, value) Then
                    _Port = value

                    OnPortChanged(New System.EventArgs)

                End If

            End Set
        End Property

        Public ReadOnly Property Data() As DataSet
            Get
                Return Me._Data

            End Get
        End Property

#End Region

#Region "Constants"

        Private Const DefaultPort As Integer = 8050

#End Region

        Public Sub New()
            Me.New(DefaultPort)

        End Sub

        Public Sub New(ByVal port As Integer)
            MyBase.New()

            Me.Port = port

            Me._Data = New DataSet("PerformanceLog")

            Dim tmpCategoryDT As DataTable = Me.Data.Tables.Add("Categories")
            tmpCategoryDT.Columns.Add("Path", GetType(String))
            tmpCategoryDT.Columns.Add("Name", GetType(String))
            tmpCategoryDT.Columns.Add("ParentCategory", GetType(String))

            Dim Keys(0) As DataColumn
            Keys(0) = tmpCategoryDT.Columns("Name")
            tmpCategoryDT.PrimaryKey = Keys

            'Me.Data.Relations.Add("SelfReferencing", tmpCategoryDT.Columns("ParentCategory"), tmpCategoryDT.Columns("Name"), False)
            Me._Data.Relations.Add("SelfReferencing", tmpCategoryDT.Columns("Path"), tmpCategoryDT.Columns("ParentCategory"))

        End Sub

        Private Sub ConfigureSocket()
            If Me.Enabled Then
                'THREAD: ThreadManager
                'Dim ListenThread As Thread
                'ListenThread = New Thread(AddressOf BeginListen)

                Dim ListenThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("PerformanceLogReader listen thread", AddressOf BeginListen)
                ListenThread.Start()

            Else
                If Not _Socket Is Nothing Then
                    _Socket.Close()
                   
                End If

                'Kill thread?

            End If
        End Sub

        Private Sub BeginListen()
            _Socket = New Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp)
            Dim _GroupEP As EndPoint = New IPEndPoint(IPAddress.Any, Me.Port)

            Dim tmpBuffer(1024) As Byte

            Dim tmpReceivedBytes As Integer = 0

            _Socket.Bind(_GroupEP)

            While Me.Enabled
                Try
                    tmpReceivedBytes = _Socket.ReceiveFrom(tmpBuffer, _GroupEP)

                    If tmpReceivedBytes > 0 Then
                        _ReceiveBuffer &= System.Text.Encoding.UTF8.GetString(tmpBuffer, 0, tmpReceivedBytes)

                        Dim tmpResult As String = Me.GetString

                        While Not String.IsNullOrEmpty(tmpResult)
                            'Dim tmpDataFields() As String = tmpResult.Split(PerformanceLogUtility.ValueSplitString)
                            Dim tmpDataFields() As String = Me.GetValues(tmpResult)

                            If tmpDataFields.Length = 3 Then
                                Dim tmpCategoryPath As String = tmpDataFields(0).Trim(New Char() {Char.Parse("\")}).ToLower & "\"

                                If Not _ValidatedCategoryPaths.Contains(tmpCategoryPath) Then
                                    Dim tmpCategoryList() As String = tmpDataFields(0).Split(New String() {"\"}, StringSplitOptions.RemoveEmptyEntries)

                                    Dim tmpParentPath As String = Nothing
                                    Dim tmpFullPath As String = String.Empty

                                    For Each tmpCategory As String In tmpCategoryList
                                        tmpFullPath &= tmpCategory.ToLower & "\"

                                        Dim tmpCategoryRow As DataRow = Nothing
                                        Dim tmpCategoryRowMatch() As DataRow = Me.Data.Tables("Categories").Select("Path = '" & tmpFullPath & "'")

                                        If tmpCategoryRowMatch.Length = 1 Then
                                            tmpCategoryRow = tmpCategoryRowMatch(0)

                                        End If

                                        If tmpCategoryRow Is Nothing Then
                                            tmpCategoryRow = Me.Data.Tables("Categories").NewRow
                                            tmpCategoryRow("Path") = tmpFullPath
                                            tmpCategoryRow("Name") = tmpCategory
                                            tmpCategoryRow("ParentCategory") = tmpParentPath

                                            Me.Data.Tables("Categories").Rows.Add(tmpCategoryRow)

                                        End If

                                        tmpParentPath = tmpFullPath

                                    Next

                                    _ValidatedCategoryPaths.Add(tmpCategoryPath)

                                End If

                                Dim tmpLogEntryDT As DataTable = Nothing

                                If Me.Data.Tables.Contains("LogEntries_" & tmpCategoryPath.Replace("\", "_").Replace(" ", "-SPACE-")) Then
                                    tmpLogEntryDT = Me.Data.Tables("LogEntries_" & tmpCategoryPath.Replace("\", "_").Replace(" ", "-SPACE-"))

                                Else
                                    tmpLogEntryDT = Me.Data.Tables.Add("LogEntries_" & tmpCategoryPath.Replace("\", "_").Replace(" ", "-SPACE-"))
                                    tmpLogEntryDT.Columns.Add("Category", GetType(String))
                                    tmpLogEntryDT.Columns.Add("SocketSession", GetType(String))
                                    tmpLogEntryDT.Columns.Add("Context", GetType(String))
                                    tmpLogEntryDT.Columns.Add("Entry", GetType(String))

                                End If

                                Dim tmpContextData() As String = tmpDataFields(1).Split(New String() {"|"}, StringSplitOptions.RemoveEmptyEntries)
                                Dim tmpSocketSession As String = "N/A"
                                Dim tmpContext As String = "N/A"

                                If tmpContextData.Length > 0 Then tmpSocketSession = tmpContextData(0)
                                If tmpContextData.Length > 1 Then tmpContext = tmpContextData(1)

                                Dim tmpLogEntry As String = tmpDataFields(2)

                                tmpLogEntryDT.Rows.Add(New Object() {tmpCategoryPath, tmpSocketSession, tmpContext, tmpLogEntry})

                            End If

                            tmpResult = Me.GetString

                        End While

                    End If

                Catch ex As Exception
                    'SILENT

                End Try

            End While

            _Socket.Close()

        End Sub

        Private Function GetString() As String
            If Not String.IsNullOrEmpty(_ReceiveBuffer) Then
                Dim tmpFirstIndex As Integer = PerformanceLogUtility.IndexOfEOS(_ReceiveBuffer)

                If tmpFirstIndex <> -1 Then
                    Dim tmpResult As String = _ReceiveBuffer.Substring(0, tmpFirstIndex)
                    _ReceiveBuffer = _ReceiveBuffer.Substring(tmpFirstIndex + PerformanceLogUtility.EOSBytes.Length)

                    Return tmpResult

                End If
            End If
        End Function

        Private Function GetValues(ByVal buffer As String) As String()
            Dim tmpResult As New ArrayList

            Dim tmpWorkingBuffer As String = buffer
            Dim tmpIndex As Integer = PerformanceLogUtility.IndexOfValueSplit(tmpWorkingBuffer)

            While tmpIndex > 0
                tmpResult.Add(tmpWorkingBuffer.Substring(0, tmpIndex))

                tmpWorkingBuffer = tmpWorkingBuffer.Substring(tmpIndex + PerformanceLogUtility.ValueSplitString.Length)
                tmpIndex = PerformanceLogUtility.IndexOfValueSplit(tmpWorkingBuffer)

            End While

            tmpResult.Add(tmpWorkingBuffer)

            Return tmpResult.ToArray(GetType(String))

        End Function

        Private Sub OnEnabledChanged(ByVal e As System.EventArgs)
            ConfigureSocket()

        End Sub

        Private Sub OnPortChanged(ByVal e As System.EventArgs)
            ConfigureSocket()

        End Sub

    End Class

End Namespace