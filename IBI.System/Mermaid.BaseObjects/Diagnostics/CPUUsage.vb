﻿Imports System
Imports System.Runtime.InteropServices
Imports System.Diagnostics

'/// <summary>
'/// Defines an abstract base class for implementations of CPU usage counters.
'/// </summary>
Public MustInherit Class CpuUsage
    '/// <summary>
    '/// Creates and returns a CpuUsage instance that can be used to query the CPU time on this operating system.
    '/// </summary>
    '/// <returns>An instance of the CPUUsage class.</returns>
    '/// <exception cref="NotSupportedException">This platform is not supported -or- initialization of the CPUUsage object failed.</exception>
    Public Shared Function Create() As CPUUsage
        If m_CPUUsage Is Nothing Then
            If Environment.OSVersion.Platform = PlatformID.Win32NT Then
                m_CPUUsage = New CpuUsageNt()
            Else
                Throw New NotSupportedException()
            End If
        End If
        Return m_CpuUsage
    End Function
    '/// <summary>
    '/// Determines the current average CPU load.
    '/// </summary>
    '/// <returns>An integer that holds the CPU load percentage.</returns>
    '/// <exception cref="NotSupportedException">One of the system calls fails. The CPU time can not be obtained.</exception>
    Public MustOverride Function Query() As Integer
    '/// <summary>
    '/// Holds an instance of the CPUUsage class.
    '/// </summary>
    Private Shared m_CpuUsage As CpuUsage = Nothing

End Class

Friend NotInheritable Class CpuUsageNt
    Inherits CpuUsage

#Region "Variables"

    '/// <summary>Returns the number of processors in the system in a SYSTEM_BASIC_INFORMATION structure.</summary>
    Private Const SYSTEM_BASICINFORMATION As Integer = 0
    '/// <summary>Returns an opaque SYSTEM_PERFORMANCE_INFORMATION structure.</summary>
    Private Const SYSTEM_PERFORMANCEINFORMATION As Integer = 2
    '/// <summary>Returns an opaque SYSTEM_TIMEOFDAY_INFORMATION structure.</summary>
    Private Const SYSTEM_TIMEINFORMATION As Integer = 3
    '/// <summary>The value returned by NtQuerySystemInformation is no error occurred.</summary>
    Private Const NO_ERROR As Integer = 0
    '/// <summary>Holds the old idle time.</summary>
    Private oldIdleTime As Long
    '/// <summary>Holds the old system time.</summary>
    Private oldSystemTime As Long
    '/// <summary>Holds the number of processors in the system.</summary>
    Private processorCount As Double

#End Region

#Region "API Declarations"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="dwInfoType"></param>
    ''' <param name="lpStructure"></param>
    ''' <param name="dwSize"></param>
    ''' <param name="returnLength"></param>
    ''' <returns>Returns a success NTSTATUS if successful, and an NTSTATUS error code otherwise.</returns>
    ''' <remarks></remarks>
    Private Declare Function NtQuerySystemInformation Lib "ntdll" (ByVal dwInfoType As Integer, ByVal lpStructure() As Byte, ByVal dwSize As Integer, ByVal returnLength As IntPtr) As Integer

#End Region

    Public Sub New()
        Try
            Dim timeInfo(31) As Byte   ' SYSTEM_TIME_INFORMATION structure
            Dim perfInfo(311) As Byte  ' SYSTEM_PERFORMANCE_INFORMATION structure
            Dim baseInfo(43) As Byte ' SYSTEM_BASIC_INFORMATION structure
            Dim ret As Integer
            ' get new system time
            ret = NtQuerySystemInformation(SYSTEM_TIMEINFORMATION, timeInfo, timeInfo.Length, IntPtr.Zero)
            ' get new CPU's idle time
            ret = NtQuerySystemInformation(SYSTEM_PERFORMANCEINFORMATION, perfInfo, perfInfo.Length, IntPtr.Zero)
            ' get number of processors in the system
            ret = NtQuerySystemInformation(SYSTEM_BASICINFORMATION, baseInfo, baseInfo.Length, IntPtr.Zero)
            ' store new CPU's idle and system time and number of processors
            oldIdleTime = BitConverter.ToInt64(perfInfo, 0) ' SYSTEM_PERFORMANCE_INFORMATION.liIdleTime
            oldSystemTime = BitConverter.ToInt64(timeInfo, 8) ' SYSTEM_TIME_INFORMATION.liKeSystemTime
            processorCount = baseInfo(40)
        Catch ex As Exception
        End Try
    End Sub

    Public Overrides Function Query() As Integer
        Dim timeInfo(32) As Byte  ' SYSTEM_TIME_INFORMATION structure
        Dim perfInfo(312) As Byte ' SYSTEM_PERFORMANCE_INFORMATION structure
        Dim dbIdleTime As Double, dbSystemTime As Double
        Dim ret As Integer
        Try

            ' get new system time
            ret = NtQuerySystemInformation(SYSTEM_TIMEINFORMATION, timeInfo, timeInfo.Length, IntPtr.Zero)
            ' get new CPU's idle time
            ret = NtQuerySystemInformation(SYSTEM_PERFORMANCEINFORMATION, perfInfo, perfInfo.Length, IntPtr.Zero)
            ' CurrentValue = NewValue - OldValue
            dbIdleTime = BitConverter.ToInt64(perfInfo, 0) - oldIdleTime
            dbSystemTime = BitConverter.ToInt64(timeInfo, 8) - oldSystemTime
            ' CurrentCpuIdle = IdleTime / SystemTime
            If dbSystemTime <> 0 Then dbIdleTime = dbIdleTime / dbSystemTime
            ' CurrentCpuUsage% = 100 - (CurrentCpuIdle * 100) / NumberOfProcessors
            dbIdleTime = 100.0 - dbIdleTime * 100.0 / processorCount + 0.5
            ' store new CPU's idle and system time
            oldIdleTime = BitConverter.ToInt64(perfInfo, 0) ' SYSTEM_PERFORMANCE_INFORMATION.liIdleTime
            oldSystemTime = BitConverter.ToInt64(timeInfo, 8) ' SYSTEM_TIME_INFORMATION.liKeSystemTime
            Return CType(dbIdleTime, Integer)
        Catch ex As Exception
            Return 0
        End Try
    End Function

End Class