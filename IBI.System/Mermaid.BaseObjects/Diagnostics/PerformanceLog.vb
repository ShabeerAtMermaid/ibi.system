﻿Namespace Diagnostics

    Public Class PerformanceLog

#Region "Variables"

        Private _Enabled As Boolean = True
        Private _Category As String

#End Region

#Region "Properties"

        Public Property Enabled() As Boolean
            Get
                Return Me._Enabled

            End Get
            Set(ByVal value As Boolean)
                Me._Enabled = value

            End Set
        End Property

        Public Property Category() As String
            Get
                Return Me._Category

            End Get
            Set(ByVal value As String)
                Me._Category = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal category As String)
            MyBase.New()

            If String.IsNullOrEmpty(category) Then Throw New ArgumentException("Category cannot be null or empty", "category")

            Me.Category = category

        End Sub

        Public Sub WriteEntry(ByVal entry As String)
            If Me.Enabled Then
                Dim tmpCallData As Net.Sockets.CommunicatorDataObject = Net.Sockets.SocketUtility.FetchCurrentCallData

                Dim tmpContextData As String = "N/A|N/A"

                If Not tmpCallData Is Nothing Then
                    Dim tmpSessionID As String = "N/A"

                    If Not tmpCallData.Session Is Nothing Then
                        tmpSessionID = tmpCallData.Session.IDentifier

                        If Not tmpCallData.Session.Communicator Is Nothing Then
                            If Not tmpCallData.Session.Communicator.SocketInfo Is Nothing Then
                                tmpSessionID = tmpCallData.Session.Communicator.SocketInfo.FriendlyName

                            End If
                        End If
                    End If

                    tmpContextData = tmpSessionID & "|" & tmpCallData.Tag

                End If

                PerformanceLogManager.SendData(Me.Category, tmpContextData, entry)

            End If

        End Sub

    End Class

End Namespace