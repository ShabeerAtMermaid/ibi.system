Namespace Diagnostics

    Public Class PerformanceCounterCollection
        Inherits System.Collections.Generic.Dictionary(Of String, PerformanceCounter)

    End Class

End Namespace