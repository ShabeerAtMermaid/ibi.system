﻿Imports System.Net.Sockets
Imports System.Net

Namespace Diagnostics

    Public Class MonitorSocket

#Region "Properties"

        Private Property ServerSocket As Socket
        Private Property Callback As AsyncCallback

#End Region

#Region "Enums"

        Public Enum MonitorStatus As Integer
            Running = 0
            Warning = 1
            [Error] = 2

        End Enum

#End Region

#Region "Events"

        Public Event StatusRequest As System.EventHandler(Of StatusRequestEventArgs)

        Private Sub OnStatusRequest(ByVal e As StatusRequestEventArgs)
            RaiseEvent StatusRequest(Me, e)

        End Sub

#End Region

        Public Sub New(ByVal port As Integer)
            MyBase.New()

            Dim tmpEndpoint As New IPEndPoint(IPAddress.Any, port)

            Callback = New AsyncCallback(AddressOf ServerSocket_OnAccept)

            ServerSocket = New Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            ServerSocket.Bind(tmpEndpoint)
            ServerSocket.Listen(100)
            ServerSocket.BeginAccept(Callback, Nothing)

        End Sub

        Private Sub ServerSocket_OnAccept(ByVal ar As IAsyncResult)
            Dim listenStarted As Boolean = False

            Try
                Dim clientSocket As Socket = ServerSocket.EndAccept(ar)

                ServerSocket.BeginAccept(Callback, Nothing)
                listenStarted = True

                Dim ClientSession As New ClientSession(Me, clientSocket)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Sockets))

            Finally

                If Not listenStarted Then
                    ServerSocket.BeginAccept(Callback, Nothing)
                End If
            End Try
        End Sub

        Private Function PerformCall(ByVal data As String) As String
            Dim tmpEventArgs As New StatusRequestEventArgs(data)

            Me.OnStatusRequest(tmpEventArgs)

            Return CInt(tmpEventArgs.StatusCode) & ";" & tmpEventArgs.Description

        End Function

        Private Class ClientSession
            Implements IDisposable

#Region "Variables"

            Private MonitorSocket As MonitorSocket
            Private ClientSocket As Socket
            Private AsyncSocketCallback As System.EventHandler(Of SocketAsyncEventArgs)
            Private AsyncEventArgs As SocketAsyncEventArgs

            Private Buffer() As Byte
            Private ReceiveBuffer() As Byte

            Private Shared EOSBytes() As Byte = System.Text.Encoding.UTF8.GetBytes(System.Environment.NewLine)

#End Region

            Public Sub New(ByVal monitor As MonitorSocket, ByVal clientSocket As Socket)
                Me.MonitorSocket = monitor
                Me.ClientSocket = clientSocket

                Me.AsyncSocketCallback = New EventHandler(Of SocketAsyncEventArgs)(AddressOf ClientSocket_OnReceive)

                'Me.Buffer = Array.CreateInstance(GetType(Byte), 32000)
                Me.Buffer = mermaid.BaseObjects.Net.Sockets.SocketBufferPool.GetBuffer("MonitorSocket")
                Me.AsyncEventArgs = New SocketAsyncEventArgs()

                AddHandler Me.AsyncEventArgs.Completed, Me.AsyncSocketCallback

                Me.AsyncEventArgs.SetBuffer(Me.Buffer, 0, Me.Buffer.Length)
                Me.AsyncEventArgs.UserToken = Me

                Me.ClientSocket.ReceiveAsync(Me.AsyncEventArgs)

            End Sub

            Private Sub ClientSocket_OnReceive(ByVal src As Object, ByVal sae As SocketAsyncEventArgs)

                Dim tmpBytesReceived As Integer = sae.BytesTransferred

                If tmpBytesReceived > 0 Then
                    Dim tmpCurrentBufferLength As Long = 0

                    If Not Me.ReceiveBuffer Is Nothing Then tmpCurrentBufferLength = ReceiveBuffer.Length

                    Dim tmpReceiveBuffer(tmpCurrentBufferLength + tmpBytesReceived - 1) As Byte

                    If Not Me.ReceiveBuffer Is Nothing Then Me.ReceiveBuffer.CopyTo(tmpReceiveBuffer, 0)

                    Array.Copy(Me.Buffer, 0, tmpReceiveBuffer, tmpCurrentBufferLength, tmpBytesReceived)

                    Me.ReceiveBuffer = tmpReceiveBuffer

                    Dim tmpEOSIndex As Long = Me.IndexOfEOS(Me.ReceiveBuffer)

                    If tmpEOSIndex <> -1 Then
                        Dim tmpBuffer(tmpEOSIndex - 1) As Byte

                        Array.Copy(Me.ReceiveBuffer, tmpBuffer, tmpEOSIndex)

                        Dim data As String = System.Text.Encoding.UTF8.GetString(tmpBuffer)

                        Me.ClientSocket.Send(System.Text.Encoding.UTF8.GetBytes(Me.MonitorSocket.PerformCall(data)))
                        'Me.ClientSocket.Close()

                        Me.Dispose()

                    Else
                        Me.ClientSocket.ReceiveAsync(Me.AsyncEventArgs)
                    End If
                End If
            End Sub

            Private Function IndexOfEOS(ByVal buffer() As Byte) As Long
                If Not buffer Is Nothing Then
                    For i As Integer = 0 To buffer.Length - EOSBytes.Length
                        For y As Integer = 0 To EOSBytes.Length - 1
                            If buffer(i + y) = EOSBytes(y) Then
                                If (y = EOSBytes.Length - 1) Then
                                    Return i
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Next
                End If

                Return -1

            End Function

#Region "IDisposable Support"

            Private disposedValue As Boolean ' To detect redundant calls

            ' IDisposable
            Protected Overridable Sub Dispose(ByVal disposing As Boolean)
                If Not Me.disposedValue Then
                    If disposing Then
                        Try
                            If Not Me.ClientSocket Is Nothing Then
                                Me.ClientSocket.Close()
                            End If
                        Catch ex As Exception
                            'SILENT
                        End Try

                        If Not Me.AsyncEventArgs Is Nothing Then
                            Me.AsyncEventArgs.Dispose()
                        End If
                        
                        If Not Me.Buffer Is Nothing Then
                            Array.Clear(Me.Buffer, 0, Me.Buffer.Length)
                        End If
                        
                        If Not Me.ReceiveBuffer Is Nothing Then
                            Array.Clear(Me.ReceiveBuffer, 0, Me.ReceiveBuffer.Length)
                        End If
                    End If

                    ' TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.

                    Me.MonitorSocket = Nothing
                    Me.ClientSocket = Nothing
                    Me.AsyncSocketCallback = Nothing
                    Me.AsyncEventArgs = Nothing

                    mermaid.BaseObjects.Net.Sockets.SocketBufferPool.ReuseBuffer("MonitorSocket", Me.Buffer)
                    Me.Buffer = Nothing
                    Me.ReceiveBuffer = Nothing

                End If
                Me.disposedValue = True
            End Sub

            ' TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
            'Protected Overrides Sub Finalize()
            '    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            '    Dispose(False)
            '    MyBase.Finalize()
            'End Sub

            ' This code added by Visual Basic to correctly implement the disposable pattern.
            Public Sub Dispose() Implements IDisposable.Dispose
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)
            End Sub

#End Region

        End Class

    End Class

End Namespace