Imports System.Threading
Imports Microsoft.Win32

Namespace Diagnostics

    Public Class PerformanceCounter

#Region "Variables"

        Private _Name As String
        Private _Value As Long
        Private _WriteToConsole As Boolean = False
        Private _WriteToRegistry As Boolean = True

        Private _RegKeySoftware As RegistryKey
        Private _RegKeyCompany As RegistryKey
        Private _RegKeyProgram As RegistryKey
        Private _RegKeyCategory As RegistryKey

        Private _LastRegValueWritten As Long

        Private _TimerCallback As Threading.SharedTimerCallback

#End Region

#Region "Properties"

        Public ReadOnly Property Name() As String
            Get
                Return Me._Name

            End Get
        End Property

        Public ReadOnly Property Value() As Long
            Get
                Return _Value

            End Get
        End Property

        Public Property WriteToConsole() As Boolean
            Get
                Return _WriteToConsole

            End Get
            Set(ByVal value As Boolean)
                _WriteToConsole = value

            End Set
        End Property

        Public Property WriteToRegistry() As Boolean
            Get
                Return _WriteToRegistry

            End Get
            Set(ByVal value As Boolean)
                _WriteToRegistry = value

            End Set
        End Property

        Private Property TimerCallback() As Threading.SharedTimerCallback
            Get
                Return Me._TimerCallback

            End Get
            Set(ByVal value As Threading.SharedTimerCallback)
                Me._TimerCallback = value

            End Set
        End Property

#End Region

        Friend Sub New(ByVal name As String)
            MyBase.New()

            Me._Value = 0L
            Me._Name = name

            Try
                _RegKeySoftware = My.Computer.Registry.LocalMachine.CreateSubKey("Software")
                _RegKeyCompany = _RegKeySoftware.CreateSubKey("mermaid technology")
                _RegKeyProgram = _RegKeyCompany.CreateSubKey("mermaid Performance Counters")

                If Not String.IsNullOrEmpty(PerformanceCounterManager.CategoryName) Then
                    _RegKeyCategory = _RegKeyProgram.CreateSubKey(PerformanceCounterManager.CategoryName)

                End If

                Me.TimerCallback = New Threading.SharedTimerCallback(AddressOf Timer_Elapsed)
                PerformanceCounterManager.Timer.AttachCallback(Me.TimerCallback)


            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.PerformanceCounters))

            End Try
        End Sub

        Public Function Increment() As Long
            'Monitor.Enter(Lock)

            'Try
            '    SetValue(_Value + 1)

            '    Return Me.Value

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Dim tmpValue As Long = Interlocked.Increment(Me._Value)

            Me.NotifyListeners()

            Return tmpValue

        End Function

        Public Function Increment(ByVal by As Long) As Long
            'Monitor.Enter(Lock)

            'Try
            '    SetValue(_Value + by)

            '    Return Me.Value

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Dim tmpValue As Long = Interlocked.Add(Me._Value, by)

            Me.NotifyListeners()

            Return tmpValue

        End Function

        Public Function Decrement() As Long
            'Monitor.Enter(Lock)

            'Try
            '    SetValue(_Value - 1)

            '    Return Me.Value

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Dim tmpValue As Long = Interlocked.Decrement(Me._Value)

            Me.NotifyListeners()

            Return tmpValue

        End Function

        Public Function Decrement(ByVal by As Long) As Long
            'Monitor.Enter(Lock)

            'Try
            '    SetValue(_Value - by)

            '    Return Me.Value

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Dim tmpValue As Long = Interlocked.Add(Me._Value, by * -1)

            Me.NotifyListeners()

            Return tmpValue

        End Function

        Public Function GetValue() As Long
            'Monitor.Enter(Lock)

            'Try
            '    Return Me.Value

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Return Interlocked.Read(Me._Value)

        End Function

        Public Sub SetValue(ByVal value As Long)
            'Monitor.Enter(Lock)

            'Try
            '    _Value = value

            '    If WriteToConsole Then Console.WriteLine(Me.Name & ": " & Me.Value)

            '    If WriteToRegistry Then
            '        'PerformanceCounterManager.ThreadPool.EnqueueWorkItem(New Threading.WorkItem(New Threading.ParameterLessWorkItemCallback(AddressOf UpdateRegistry)))
            '        Dim tmpThread As New Thread(AddressOf UpdateRegistry)
            '        tmpThread.Name = "PerformanceCounter """ & Me.Name & """ UpdateRegistry thread"
            '        tmpThread.Start()

            '    End If

            '    PerformanceCounterManager.OnPerformanceCounterValueChanged(Me, New EventArgs.PerformanceCounterValueChangedEventArgs(Me, value))

            'Catch ex As Exception
            '    PerformanceCounterManager.OnExceptionOccured(Me, New EventArgs.ExceptionEventArgs(ex, "PerformanceCounter name: " & Me.Name))

            'Finally
            '    Monitor.Exit(Lock)

            'End Try

            Interlocked.Exchange(Me._Value, value)

            Me.NotifyListeners()

        End Sub

        Private Sub NotifyListeners()
            Try
                If WriteToConsole Then Console.WriteLine(Me.Name & ": " & Me.Value)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.PerformanceCounters, "PerformanceCounter name: " & Me.Name))

            End Try
        End Sub

        'Private Sub NotifyListeners_Callback()
        '    Try
        '       If WriteToConsole Then Console.WriteLine(Me.Name & ": " & Me.Value)

        '        If WriteToRegistry Then
        '            If _RegKeyCategory Is Nothing Then
        '                If Not _RegKeyProgram Is Nothing Then
        '                    _RegKeyProgram.SetValue(Me.Name, _Value)

        '                End If

        '            Else
        '                _RegKeyCategory.SetValue(Me.Name, _Value)

        '            End If
        '        End If

        '        PerformanceCounterManager.OnPerformanceCounterValueChanged(Me, New EventArgs.PerformanceCounterValueChangedEventArgs(Me, Value))

        '    Catch ex As Exception
        '        PerformanceCounterManager.OnExceptionOccured(Me, New EventArgs.ExceptionEventArgs(ex, "PerformanceCounter name: " & Me.Name))

        '    End Try
        'End Sub

        Private Sub Timer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                If IsDirty(Interlocked.Read(Me._LastRegValueWritten), Interlocked.Read(Me.Value)) Then
                    Interlocked.Exchange(_LastRegValueWritten, _Value)

                    If WriteToRegistry Then
                        If _RegKeyCategory Is Nothing Then
                            If Not _RegKeyProgram Is Nothing Then
                                _RegKeyProgram.SetValue(Me.Name, Interlocked.Read(_Value))

                            End If

                        Else
                            _RegKeyCategory.SetValue(Me.Name, Interlocked.Read(_Value))

                        End If
                    End If

                    PerformanceCounterManager.OnPerformanceCounterValueChanged(Me, New EventArgs.PerformanceCounterValueChangedEventArgs(Me, Interlocked.Read(Value)))

                End If

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.PerformanceCounters, "PerformanceCounter name: " & Me.Name))

            End Try
        End Sub

    End Class

End Namespace