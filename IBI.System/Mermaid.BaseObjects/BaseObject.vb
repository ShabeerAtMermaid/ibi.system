Imports mermaid.BaseObjects.Remoting
Imports mermaid.BaseObjects.Interfaces
Imports mermaid.BaseObjects.Query
Imports System.ComponentModel

Public Class BaseObject
    Inherits MBRObject
    Implements IBaseObject

#Region "Attributes"

    Private _InstanceID As Integer

#End Region

#Region "Properties"

    Public Overridable Property InstanceID() As Integer Implements Interfaces.IBaseObject.InstanceID
        Get
            Return Me._InstanceId

        End Get
        Set(ByVal Value As Integer)
            Me._InstanceId = Value

        End Set
    End Property

#End Region

#Region "Query"

    'Public Function PerformQuery(ByVal data As QueryDataCollection) As DataSet Implements IBaseObject.PerformQuery
    '    Dim tmpResult As DataSet = New DataSet("QueryResult")

    '    Me.PerformQuery(tmpResult, data)

    '    Return tmpResult

    'End Function

    'Public Sub PerformQuery(ByRef result As DataSet, ByVal data As QueryDataCollection)
    '    Dim tmpResultTable As DataTable = Nothing

    '    If Not result.Tables.Contains(Me.GetType.Name) Then
    '        tmpResultTable = result.Tables.Add(Me.GetType.Name)

    '    Else
    '        tmpResultTable = result.Tables(Me.GetType.Name)

    '    End If

    '    Dim tmpPropertyDescriptors As PropertyDescriptorCollection = TypeDescriptor.GetProperties(Me)
    '    Dim tmpIsFiltered As Boolean = True

    '    If data.QueryFilters.Count > 0 Then
    '        For Each tmpFilter As QueryFilter In data.QueryFilters
    '            Dim tmpFilterProperty As Object = Me.GetPropertyValue(Me, tmpPropertyDescriptors, tmpFilter.PropertyName)

    '            Select Case tmpFilter.Comparer
    '                Case QueryFilter.FilterComparers.Equals
    '                    If Not IsDirty(tmpFilterProperty, tmpFilter.FilterValue) Then
    '                        tmpIsFiltered = False

    '                    End If

    '                Case QueryFilter.FilterComparers.Differs
    '                    If IsDirty(tmpFilterProperty, tmpFilter.FilterValue) Then
    '                        tmpIsFiltered = False

    '                    End If

    '            End Select

    '        Next

    '    Else
    '        tmpIsFiltered = False

    '    End If

    '    If Not tmpIsFiltered Then
    '        Dim tmpObjRow As DataRow = tmpResultTable.NewRow

    '        For Each tmpData As QueryData In data
    '            Dim tmpPropertyName As String = tmpData.PropertyName
    '            Dim tmpPropertyValue As Object = Nothing

    '            If tmpData.QueryData Is Nothing Then
    '                tmpPropertyValue = Me.GetPropertyValue(Me, tmpPropertyDescriptors, tmpPropertyName)

    '                If Not tmpPropertyValue Is Nothing Then
    '                    If Not tmpResultTable.Columns.Contains(tmpData.DisplayName) Then tmpResultTable.Columns.Add(tmpData.DisplayName, tmpPropertyValue.GetType)

    '                    tmpObjRow(tmpData.DisplayName) = tmpPropertyValue

    '                End If
    '            Else
    '                tmpPropertyValue = Me.GetPropertyValue(Me, tmpPropertyDescriptors, tmpPropertyName)

    '                If Not tmpPropertyValue Is Nothing Then
    '                    If Persistence.Broker.IsTypeOfPersistentList(tmpPropertyValue.GetType) Then
    '                        Dim tmpRefTable As DataTable = Nothing

    '                        'UNDONE: Change ref table name i query
    '                        'Dim tmpRefTableName As String = Me.GetType.Name & "_" & tmpPropertyName & "_CREF"
    '                        Dim tmpRefTableName As String = Me.GetType.Name & "_CollectionREF"

    '                        If Not result.Tables.Contains(tmpRefTableName) Then
    '                            tmpRefTable = result.Tables.Add(tmpRefTableName)

    '                        Else
    '                            tmpRefTable = result.Tables(tmpRefTableName)

    '                        End If

    '                        If Not tmpRefTable.Columns.Contains("Container") Then tmpRefTable.Columns.Add("Container", GetType(Integer))
    '                        If Not tmpRefTable.Columns.Contains("InstanceID") Then tmpRefTable.Columns.Add("InstanceID", GetType(Integer))
    '                        If Not tmpRefTable.Columns.Contains("Value") Then tmpRefTable.Columns.Add("Value", GetType(String))

    '                        Dim tmpRefRow As DataRow = Nothing

    '                        For Each tmpValue As Object In tmpPropertyValue
    '                            tmpRefRow = tmpRefTable.NewRow
    '                            tmpRefRow("Container") = Me.InstanceID

    '                            If TypeOf tmpValue Is BaseObject Then
    '                                tmpRefRow("InstanceID") = tmpValue.InstanceID
    '                                tmpRefRow("Value") = String.Empty

    '                                If tmpData.QueryData Is Nothing Then
    '                                    tmpPropertyValue = Me.GetPropertyValue(tmpValue, tmpPropertyDescriptors, tmpPropertyName)

    '                                    If Not tmpPropertyValue Is Nothing Then
    '                                        If Not tmpResultTable.Columns.Contains(tmpData.DisplayName) Then tmpResultTable.Columns.Add(tmpData.DisplayName, tmpPropertyValue.GetType)

    '                                        tmpObjRow(tmpData.DisplayName) = tmpPropertyValue

    '                                    End If
    '                                Else
    '                                    tmpPropertyValue = Me.GetPropertyValue(tmpValue, tmpPropertyDescriptors, tmpPropertyName)

    '                                    If Not tmpPropertyValue Is Nothing Then
    '                                        tmpPropertyValue.PerformQuery(result, tmpData.QueryData)

    '                                    End If
    '                                End If
    '                            Else
    '                                tmpRefRow("InstanceID") = 0
    '                                tmpRefRow("Value") = tmpValue

    '                            End If

    '                            tmpRefTable.Rows.Add(tmpRefRow)

    '                        Next

    '                    ElseIf Persistence.Broker.IsTypeOfPersistentDictionary(tmpPropertyValue.GetType) Then
    '                        Dim tmpRefTable As DataTable = Nothing

    '                        'UNDONE: Change ref table name i query
    '                        'Dim tmpRefTableName As String = Me.GetType.Name & "_" & tmpPropertyName & "_CREF"
    '                        Dim tmpRefTableName As String = Me.GetType.Name & "_CollectionREF"

    '                        If Not result.Tables.Contains(tmpRefTableName) Then
    '                            tmpRefTable = result.Tables.Add(tmpRefTableName)

    '                        Else
    '                            tmpRefTable = result.Tables(tmpRefTableName)

    '                        End If

    '                        If Not tmpRefTable.Columns.Contains("Container") Then tmpRefTable.Columns.Add("Container", GetType(Integer))
    '                        If Not tmpRefTable.Columns.Contains("InstanceID") Then tmpRefTable.Columns.Add("InstanceID", GetType(Integer))
    '                        If Not tmpRefTable.Columns.Contains("Value") Then tmpRefTable.Columns.Add("Value", GetType(String))

    '                        Dim tmpRefRow As DataRow = Nothing

    '                        For Each tmpValue As Object In tmpPropertyValue.Values
    '                            tmpRefRow = tmpRefTable.NewRow
    '                            tmpRefRow("Container") = Me.InstanceID

    '                            If TypeOf tmpValue Is BaseObject Then
    '                                tmpRefRow("InstanceID") = tmpValue.InstanceID
    '                                tmpRefRow("Value") = String.Empty

    '                                If tmpData.QueryData Is Nothing Then

    '                                Else
    '                                    tmpValue.PerformQuery(result, tmpData.QueryData)

    '                                End If
    '                            Else
    '                                tmpRefRow("InstanceID") = 0
    '                                tmpRefRow("Value") = tmpValue

    '                            End If

    '                            tmpRefTable.Rows.Add(tmpRefRow)

    '                        Next

    '                    Else
    '                        If tmpData.QueryData Is Nothing Then
    '                            tmpPropertyValue = Me.GetPropertyValue(Me, tmpPropertyDescriptors, tmpPropertyName)

    '                            If Not tmpPropertyValue Is Nothing Then
    '                                If Not tmpResultTable.Columns.Contains(tmpData.DisplayName) Then tmpResultTable.Columns.Add(tmpData.DisplayName, tmpPropertyValue.GetType)

    '                                tmpObjRow(tmpData.DisplayName) = tmpPropertyValue

    '                            End If
    '                        Else
    '                            tmpPropertyValue = Me.GetPropertyValue(Me, tmpPropertyDescriptors, tmpPropertyName)

    '                            If Not tmpPropertyValue Is Nothing Then
    '                                tmpPropertyValue.PerformQuery(result, tmpData.QueryData)

    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            End If

    '        Next

    '        tmpResultTable.Rows.Add(tmpObjRow)

    '    End If

    'End Sub

    'Private Function GetPropertyValue(ByVal value As Object, ByVal propertyDescriptors As PropertyDescriptorCollection, ByVal propertyName As String) As Object
    '    Dim tmpPropertyNames() As String = propertyName.Split(New String() {"."}, StringSplitOptions.RemoveEmptyEntries)

    '    If tmpPropertyNames.Length = 1 Then
    '        Dim tmpPropertyDescriptor As PropertyDescriptor = propertyDescriptors(propertyName)

    '        If Not tmpPropertyDescriptor Is Nothing Then
    '            Return tmpPropertyDescriptor.GetValue(value)

    '        End If
    '    ElseIf tmpPropertyNames.Length > 1 Then
    '        Dim tmpFirstPropertyValue As Object = Me.GetPropertyValue(value, propertyDescriptors, tmpPropertyNames(0))

    '        If Not tmpFirstPropertyValue Is Nothing Then
    '            Dim tmpPropertyDescriptors As PropertyDescriptorCollection = TypeDescriptor.GetProperties(tmpFirstPropertyValue)

    '            Return Me.GetPropertyValue(tmpFirstPropertyValue, tmpPropertyDescriptors, propertyName.Substring(tmpPropertyNames(0).Length + 1))

    '        End If
    '    End If

    '    Return Nothing

    'End Function

#End Region

    Public Overrides Sub Dispose()
        MyBase.Dispose()

    End Sub

End Class
