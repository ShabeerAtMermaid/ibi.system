Imports mermaid.BaseObjects.Collections
Imports System.Reflection

Public Module Functions

    Public Function IsDirty(ByVal oldValue As Object, ByVal newValue As Object) As Boolean
        If oldValue Is Nothing Then
            If Not newValue Is Nothing Then
                Return True

            End If

        Else
            If oldValue.GetType Is GetType(System.Drawing.Font) Then
                Return True

            End If

            If Not newValue Is Nothing Then
                If newValue.GetType Is GetType(System.Drawing.Font) Then
                    Return True

                End If
            End If

            Return Not oldValue.Equals(newValue)

        End If

    End Function

    Public Function GetCurrentSignature() As String
        Try
            Dim tmpStackLines() As String = System.Environment.StackTrace.Split(New Char() {vbCrLf})
            Dim tmpStackLine As String = tmpStackLines(2) 'Has been 3 until now??

            tmpStackLine = tmpStackLine.Substring(0, tmpStackLine.IndexOf("("))
            tmpStackLine = tmpStackLine.Substring(tmpStackLine.LastIndexOf(" ") + 1)

            Return tmpStackLine

        Catch ex As Exception
            Dim tmpException As New Exception("Error getting current signature: Trace is: " & vbCrLf & _
                                              System.Environment.StackTrace, ex)

            BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Misc))

            Return "UNKNOWN"

        End Try
    End Function

    Public Function ConvertToFixedLength(ByVal fillChar As Char, ByVal fillMethod As Enums.FixedLengthConversionTypes, ByVal length As Integer, ByVal input As String) As String
        Dim sb As New System.Text.StringBuilder

        Select Case fillMethod
            Case Enums.FixedLengthConversionTypes.PrefixFillChar
                If input.Length <= length Then
                    sb.Append(fillChar, length - input.Length)
                    sb.Append(input)

                Else
                    sb.Append(input.Substring(input.Length - length))

                End If

            Case Enums.FixedLengthConversionTypes.PostfixFillChar
                If input.Length <= length Then
                    sb.Append(input)
                    sb.Append(fillChar, length - input.Length)

                Else
                    sb.Append(input.Substring(0, length))

                End If

        End Select

        Return sb.ToString

    End Function

    Public Function JoinListAsString(ByVal list As Array, ByVal splitString As String) As String
        Dim tmpResult As String = String.Empty

        If Not list Is Nothing Then
            For i As Integer = 0 To list.Length - 1
                If i <> 0 Then tmpResult &= splitString

                tmpResult &= list(i)

            Next
        End If

        Return tmpResult

    End Function

    Public Function JoinListAsString(ByVal list As ArrayList, ByVal splitString As String) As String
        Dim tmpResult As String = String.Empty

        If Not list Is Nothing Then
            For i As Integer = 0 To list.Count - 1
                If i <> 0 Then tmpResult &= splitString

                tmpResult &= list(i)

            Next
        End If

        Return tmpResult

    End Function

    Public Function JoinListAsString(ByVal list As List(Of String), ByVal splitString As String) As String
        Dim tmpResult As String = String.Empty

        If Not list Is Nothing Then
            For i As Integer = 0 To list.Count - 1
                If i <> 0 Then tmpResult &= splitString

                tmpResult &= list(i)

            Next
        End If

        Return tmpResult

    End Function

    Public Function JoinListAsString(ByVal list As List(Of Integer), ByVal splitString As String) As String
        Dim tmpResult As String = String.Empty

        If Not list Is Nothing Then
            For i As Integer = 0 To list.Count - 1
                If i <> 0 Then tmpResult &= splitString

                tmpResult &= list(i)

            Next
        End If

        Return tmpResult

    End Function

    Public Function IsTypeOfBaseList(ByVal type As Type) As Boolean
        Return GetBaseListSuperType(type) IsNot Nothing

        'If type.IsGenericType Then
        '    Dim tmpGenericType As Type = type.GetGenericTypeDefinition

        '    If tmpGenericType Is GetType(PersistentList(Of Object)).GetGenericTypeDefinition Then
        '        Return True

        '    End If
        'End If

        'Return False

    End Function

    Private Function GetBaseListSuperType(ByVal type As Type) As Type
        Dim tmpCurrentType As Type = type

        Do
            If tmpCurrentType.IsGenericType Then
                If tmpCurrentType.GetGenericTypeDefinition() Is GetType(BaseList(Of )) Then
                    Return tmpCurrentType

                End If
            End If

            tmpCurrentType = tmpCurrentType.BaseType

        Loop Until tmpCurrentType Is Nothing

        Return Nothing

    End Function

    Public Function GetBaseListValueType(ByVal list As Object) As Type
        Dim tmpListType As Type = list.GetType

        Return tmpListType.GetGenericArguments()(0)

        'Dim tmpValueName As String = tmpListType.FullName
        'tmpValueName = tmpValueName.Substring(tmpValueName.IndexOf("[[") + 2)
        'tmpValueName = tmpValueName.Substring(0, tmpValueName.IndexOf(",")).Trim

        'Dim tmpValueAssemblyName As String = tmpListType.FullName
        'tmpValueAssemblyName = tmpValueAssemblyName.Substring(tmpValueAssemblyName.IndexOf(",") + 1)
        'tmpValueAssemblyName = tmpValueAssemblyName.Substring(0, tmpValueAssemblyName.IndexOf("]"))

        'Dim tmpAssembly As System.Reflection.Assembly = System.Reflection.Assembly.Load(tmpValueAssemblyName)

        'Return tmpAssembly.GetType(tmpValueName, True, True)

    End Function

    Public Function IsTypeOfBaseDictionary(ByVal type As Type) As Boolean
        Return GetBaseDictionarySuperType(type) IsNot Nothing

    End Function

    Private Function GetBaseDictionarySuperType(ByVal type As Type) As Type
        Dim tmpCurrentType As Type = type

        Do
            If tmpCurrentType.IsGenericType Then
                If tmpCurrentType.GetGenericTypeDefinition() Is GetType(BaseDictionary(Of ,)) Then
                    Return tmpCurrentType

                End If
            End If

            tmpCurrentType = tmpCurrentType.BaseType

        Loop Until tmpCurrentType Is Nothing

        Return Nothing

    End Function

    Public Function GetBaseDictionaryKeyType(ByVal dictionary As Object) As Type
        Dim tmpDictionaryType As Type = dictionary.GetType

        Return tmpDictionaryType.GetGenericArguments()(0)

        'Dim tmpKeyName As String = tmpDictionaryType.FullName
        'tmpKeyName = tmpKeyName.Substring(tmpKeyName.IndexOf("[[") + 2)
        'tmpKeyName = tmpKeyName.Substring(0, tmpKeyName.IndexOf(",")).Trim

        'Dim tmpKeyAssemblyName As String = tmpDictionaryType.FullName
        'tmpKeyAssemblyName = tmpKeyAssemblyName.Substring(tmpKeyAssemblyName.IndexOf(",") + 1)
        'tmpKeyAssemblyName = tmpKeyAssemblyName.Substring(0, tmpKeyAssemblyName.IndexOf("]"))

        'Dim tmpKeyAssembly As System.Reflection.Assembly = System.Reflection.Assembly.Load(tmpKeyAssemblyName)

        'Return tmpKeyAssembly.GetType(tmpKeyName, True, True)

    End Function

    Public Function GetBaseDictionaryValueType(ByVal dictionary As Object) As Type
        Dim tmpDictionaryType As Type = dictionary.GetType

        Return tmpDictionaryType.GetGenericArguments()(1)

        'Dim tmpValueName As String = tmpDictionaryType.FullName
        'tmpValueName = tmpValueName.Substring(tmpValueName.IndexOf("]") + 3)
        'tmpValueName = tmpValueName.Substring(0, tmpValueName.IndexOf(",")).Trim

        'Dim tmpValueAssemblyName As String = tmpDictionaryType.FullName
        'tmpValueAssemblyName = tmpValueAssemblyName.Substring(tmpValueAssemblyName.IndexOf("]") + 3)
        'tmpValueAssemblyName = tmpValueAssemblyName.Substring(tmpValueAssemblyName.IndexOf(",") + 1)
        'tmpValueAssemblyName = tmpValueAssemblyName.Substring(0, tmpValueAssemblyName.IndexOf("]"))

        'Dim tmpValueAssembly As System.Reflection.Assembly = System.Reflection.Assembly.Load(tmpValueAssemblyName)

        'Return tmpValueAssembly.GetType(tmpValueName, True, True)

    End Function

End Module