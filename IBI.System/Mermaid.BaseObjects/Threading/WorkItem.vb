Imports System.Threading

Namespace Threading

    Public Class WorkItem
        Implements IDisposable

#Region "Variables"

        Private _ThreadPool As ThreadPool
        Private _Callback As WorkItemCallback
        Private _ParameterLessCallback As ParameterLessWorkItemCallback
        Private _Parameters As WorkItemCallbackParameterList
        Private _ThreadDataSlots As New Hashtable
        Private _WorkerThread As Thread
        Private _WaitHandle As ManualResetEvent
        Private _WaitHandleLock As New Object
        Private _Result As Object
        Private _Exception As Exception
        Private _UseTimeout As Boolean
        Private _EnqueueTime As Date
        Private _Timeout As TimeSpan
        Private WithEvents _TmrTimeout As Timers.Timer
        Private _Completed As Boolean
        Private WithEvents _TmrDispose As Timers.Timer
        Private _Disposed As Boolean

        Public _DebugText As String
        Private _PerformanceAnalyzer As New Diagnostics.PerformanceAnalyzer("ThreadPool WorkItem")

#End Region

#Region "Properties"

        Public Property Name As String

        Friend Property ThreadPool() As ThreadPool
            Get
                Return Me._ThreadPool

            End Get
            Set(ByVal value As ThreadPool)
                Me._ThreadPool = value

            End Set
        End Property

        Private Property Callback() As WorkItemCallback
            Get
                Return Me._Callback

            End Get
            Set(ByVal value As WorkItemCallback)
                Me._Callback = value

            End Set
        End Property

        Private Property ParameterLessCallback() As ParameterLessWorkItemCallback
            Get
                Return Me._ParameterLessCallback

            End Get
            Set(ByVal value As ParameterLessWorkItemCallback)
                Me._ParameterLessCallback = value

            End Set
        End Property

        Private Property Parameters() As WorkItemCallbackParameterList
            Get
                Return Me._Parameters

            End Get
            Set(ByVal value As WorkItemCallbackParameterList)
                Me._Parameters = value

            End Set
        End Property

        Friend ReadOnly Property ThreadDataSlots() As Hashtable
            Get
                Return Me._ThreadDataSlots

            End Get
        End Property

        Private Property WorkerThread() As Thread
            Get
                Return Me._WorkerThread

            End Get
            Set(ByVal value As Thread)
                Me._WorkerThread = value

            End Set
        End Property

        Private Property WaitHandle() As ManualResetEvent
            Get
                Return Me._WaitHandle

            End Get
            Set(ByVal value As ManualResetEvent)
                Me._WaitHandle = value

            End Set
        End Property

        Private ReadOnly Property WaitHandleLock() As Object
            Get
                Return Me._WaitHandleLock

            End Get
        End Property

        Private Property Result() As Object
            Get
                Return Me._Result

            End Get
            Set(ByVal value As Object)
                Me._Result = value

            End Set
        End Property

        Private Property Exception() As Exception
            Get
                Return Me._Exception

            End Get
            Set(ByVal value As Exception)
                Me._Exception = value

            End Set
        End Property

        Friend Property EnqueueTime() As Date
            Get
                Return Me._EnqueueTime

            End Get
            Set(ByVal value As Date)
                Me._EnqueueTime = value

            End Set
        End Property

        Private Property UseTimeout() As Boolean
            Get
                Return Me._UseTimeout

            End Get
            Set(ByVal value As Boolean)
                Me._UseTimeout = value

            End Set
        End Property

        Friend Property Timeout() As TimeSpan
            Get
                Return Me._Timeout

            End Get
            Set(ByVal value As TimeSpan)
                Me._Timeout = value

            End Set
        End Property

        Private ReadOnly Property HasTimedOut() As Boolean
            Get
                If UseTimeout And Date.Now.Subtract(Me.EnqueueTime) > Me.Timeout Or Me.IsTimedOut Then
                    Return True

                End If

                Return False

            End Get
        End Property

        Private Property TmrTimeout() As Timers.Timer
            Get
                Return Me._TmrTimeout

            End Get
            Set(ByVal value As Timers.Timer)
                Me._TmrTimeout = value

            End Set
        End Property

        Public ReadOnly Property Completed() As Boolean
            Get
                Return Me._Completed

            End Get
        End Property

        Public ReadOnly Property TargetName() As String
            Get
                If Not Me.Callback Is Nothing Then
                    If Not Me.Callback.Target Is Nothing Then
                        If Not Me.Callback.Method Is Nothing Then
                            Return Me.Callback.Target.GetType.FullName & "." & Me.Callback.Method.Name & "()"

                        End If
                    End If

                ElseIf Not Me.ParameterLessCallback Is Nothing Then
                    If Not Me.ParameterLessCallback.Target Is Nothing Then
                        If Not Me.ParameterLessCallback.Method Is Nothing Then
                            Return Me.ParameterLessCallback.Target.GetType.FullName & "." & Me.ParameterLessCallback.Method.Name & "()"

                        End If
                    End If
                End If

                Return "[UNKNOWN TARGET]"

            End Get
        End Property

        Private Property TmrDispose() As Timers.Timer
            Get
                Return Me._TmrDispose

            End Get
            Set(ByVal value As Timers.Timer)
                Me._TmrDispose = value

            End Set
        End Property

        Private Property Disposed() As Boolean
            Get
                Return Me._Disposed

            End Get
            Set(ByVal value As Boolean)
                Me._Disposed = value

            End Set
        End Property

        Public ReadOnly Property IsDisposed() As Boolean
            Get
                Return Me.Disposed

            End Get
        End Property

        Private Property HasBeenExecuted As Boolean
        Private Property IsTimedOut As Boolean

#End Region

#Region "Events"

        Public Event TimedOut As System.EventHandler(Of EventArgs.WorkItemEventArgs)

#End Region

        Public Sub New(ByVal callback As WorkItemCallback, ByVal parameters As WorkItemCallbackParameterList)
            MyBase.New()

            _PerformanceAnalyzer.AddPerformanceMark("Start")

            If callback Is Nothing Then Throw New ArgumentNullException("callback")
            If parameters Is Nothing Then Throw New ArgumentNullException("parameters")

            Me.Callback = callback
            Me.Parameters = parameters

        End Sub

        Public Sub New(ByVal callback As WorkItemCallback, ByVal parameters As WorkItemCallbackParameterList, ByVal workTimeout As TimeSpan)
            MyBase.New()

            _PerformanceAnalyzer.AddPerformanceMark("Start")

            If callback Is Nothing Then Throw New ArgumentNullException("callback")
            If parameters Is Nothing Then Throw New ArgumentNullException("parameters")

            Me.Callback = callback
            Me.Parameters = parameters

            Me.UseTimeout = True
            Me.Timeout = workTimeout

        End Sub

        Public Sub New(ByVal callback As ParameterLessWorkItemCallback)
            MyBase.New()

            _PerformanceAnalyzer.AddPerformanceMark("Start")

            If callback Is Nothing Then Throw New ArgumentNullException("callback")

            Me.ParameterLessCallback = callback

        End Sub

        Public Sub New(ByVal callback As ParameterLessWorkItemCallback, ByVal workTimeout As TimeSpan)
            MyBase.New()

            _PerformanceAnalyzer.AddPerformanceMark("Start")

            If callback Is Nothing Then Throw New ArgumentNullException("callback")

            Me.ParameterLessCallback = callback

            Me.UseTimeout = True
            Me.Timeout = workTimeout

        End Sub

        Friend Sub Enqueued()
            Me.EnqueueTime = Date.Now

            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Enqueued")

            Me.SaveThreadDataSlots()

            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Saving ThreadDataSlots")

        End Sub

        Friend Sub Execute()
            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Execute called")

            Try
                Me.LoadThreadDataSlots()

                If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Loading ThreadDataSlots")

                Try
                    If Not Me.Disposed Then
                        If Not Me.HasTimedOut Then
                            Me.WorkerThread = Thread.CurrentThread

                            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Fetching worker thread")

                            If Me.UseTimeout Then Me.StartTimeoutTimer()

                            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Starting timeout timer")

                            If Not Me.Callback Is Nothing Then
                                Me.Result = Me.Callback.Invoke(Me.Parameters)

                            ElseIf Not Me.ParameterLessCallback Is Nothing Then
                                Me.Result = Me.ParameterLessCallback.Invoke()

                            End If

                            If Not Me.IsTimedOut Then
                                Me.ThreadPool.OnWorkItemProcessed(New EventArgs.WorkItemEventArgs(Me))
                            End If

                            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Invoking callback")

                            If Me.UseTimeout Then Me.StopTimeoutTimer()

                            If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Stopping timeout timer")

                        Else
                            Me.IsTimedOut = True

                            Me.Exception = New TimeoutException("WorkItem has timed out before execution started.")

                        End If
                    Else
                        Me.Exception = New ObjectDisposedException("WorkItem")

                    End If

                Catch ex As ThreadAbortException
                    Me.IsTimedOut = True

                    'Me.OnTimedOut(New EventArgs.WorkItemEventArgs(Me))

                    Me.Exception = New Exception("WorkItem hasn't finished within the specified timeout.", ex)

                Catch ex As Exception
                    Me.Exception = ex

                Finally

                End Try

                If Me.IsTimedOut Then
                    If Me.Exception Is Nothing Then Me.Exception = New Exception("WorkItem hasn't finished within the specified timeout.")

                    Me.OnTimedOut(New EventArgs.WorkItemEventArgs(Me))

                End If

                Monitor.Enter(Me.WaitHandleLock)

                If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Entering WaitHandle lock")

                Me._Completed = True

                Dim tmpThreadPool As ThreadPool = Me.ThreadPool

                Try
                    Me.HasBeenExecuted = True

                    If Not Me.WaitHandle Is Nothing Then
                        Me.WaitHandle.Set()

                        If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Signaling WaitHandle")

                    Else 'There is currently no one waiting on the handle, so we start a dispose timer to make sure we get the object disposed.
                        Me.TmrDispose = New Timers.Timer
                        Me.TmrDispose.Interval = TimeSpan.FromSeconds(10).TotalMilliseconds
                        Me.TmrDispose.Enabled = True

                        If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Starting dispose timer")

                    End If

                Catch ex As Exception
                    Me.Exception = ex

                Finally
                    Monitor.Exit(Me.WaitHandleLock)

                    If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Leaving WaitHandle lock")

                End Try

                tmpThreadPool.DequeueWorkItem(Me)

                If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Dequeueing WorkItem")

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me.ThreadPool, "Internal error executing WorkItem.", ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

                If Not Me.IsDisposed Then _PerformanceAnalyzer.AddPerformanceMark("Catching Exception")

            Finally

            End Try
        End Sub

        Private Sub SaveThreadDataSlots()
            Try
                Dim tmpCurrentSession As Net.Sockets.CommunicatorSession = Net.Sockets.SocketUtility.FetchCurrentSession

                If Not tmpCurrentSession Is Nothing Then
                    Me.ThreadDataSlots(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTSESSION) = tmpCurrentSession

                End If

                Dim tmpCurrentCallData As Net.Sockets.CommunicatorDataObject = Net.Sockets.SocketUtility.FetchCurrentCallData

                If Not tmpCurrentCallData Is Nothing Then
                    Me.ThreadDataSlots(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTCALLDATA) = tmpCurrentCallData

                End If

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me.ThreadPool, ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

        Private Sub LoadThreadDataSlots()
            Try
                'For Each tmpEntry As DictionaryEntry In Me.ThreadDataSlots
                '    Thread.SetData(Thread.GetNamedDataSlot(tmpEntry.Key), tmpEntry.Value)

                'Next

                If Me.ThreadDataSlots.ContainsKey(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTSESSION) Then
                    Net.Sockets.SocketUtility.SetCurrentSession(Me.ThreadDataSlots(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTSESSION))

                End If

                If Me.ThreadDataSlots.ContainsKey(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTCALLDATA) Then
                    Net.Sockets.SocketUtility.SetCurrentCallData(Me.ThreadDataSlots(Net.Sockets.SocketUtility.THREADDATASLOTNAME_CURRENTCALLDATA))

                End If

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me.ThreadPool, ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

        Public Function GetResult() As Object
            Monitor.Enter(Me.WaitHandleLock)

            Try
                If Not Me.Completed Then
                    Me.WaitHandle = New ManualResetEvent(False)

                End If

            Finally
                Monitor.Exit(Me.WaitHandleLock)

            End Try

            Try
                If Not Me.Completed Then
                    Me.WaitHandle.WaitOne()

                End If

                If Not Me.Exception Is Nothing Then
                    Throw New Exception("An error occured while executing the WorkItem.", Me.Exception)

                End If

                Return Me.Result

            Finally
                _DebugText &= "GetResult() called successfull - Disposing WorkItem" & vbCrLf

                Me.Dispose()

            End Try
        End Function

        Public Function GetResult(ByVal timeout As TimeSpan) As Object
            Monitor.Enter(Me.WaitHandleLock)

            Try
                If Not Me.Completed Then
                    Me.WaitHandle = New ManualResetEvent(False)

                End If

            Finally
                Monitor.Exit(Me.WaitHandleLock)

            End Try

            Try
                If Not Me.Completed Then
                    If Me.WaitHandle.WaitOne(timeout, True) Then
                        If Not Me.Exception Is Nothing Then
                            Throw New Exception("An error occured while executing the WorkItem.", Me.Exception)

                        End If

                    Else
                        Me.IsTimedOut = True

                        _DebugText &= "The Result has not been received within the specified timeout." & vbCrLf
                        Throw New TimeoutException("The Result has not been received within the specified timeout.")

                    End If

                Else
                    If Not Me.Exception Is Nothing Then
                        Throw New Exception("An error occured while executing the WorkItem.", Me.Exception)

                    End If
                End If

                Return Me.Result

            Finally
                _DebugText &= "GetResult() returned - Disposing WorkItem" & vbCrLf

                Me.Dispose()

            End Try
        End Function

        Private Sub StartTimeoutTimer()
            'UNDONE: Workitem timeout after execution has started is not implemented

            'If Me.TmrTimeout Is Nothing Then Me.TmrTimeout = New Timers.Timer

            'Me.TmrTimeout.Enabled = False
            'Me.TmrTimeout.Interval = Math.Max(1000, Me.EnqueueTime.Add(Me.Timeout).Subtract(Date.Now).TotalMilliseconds)
            'Me.TmrTimeout.Enabled = True

        End Sub

        Private Sub StopTimeoutTimer()
            'If Me.TmrTimeout Is Nothing Then
            '    Me.TmrTimeout.Enabled = False
            '    Me.TmrTimeout.Dispose()

            'End If

        End Sub

        Private Sub TmrTimeout_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrTimeout.Elapsed
            'UNDONE: Workitem timeout after execution has started is not implemented
            'Me.OnTimedOut(New EventArgs.WorkItemEventArgs(Me))

            'Try
            '    Me.TmrTimeout.Enabled = False

            '    'DELETE: Console
            '    Console.WriteLine("WorkItem hasn't finished within the specified timeout.")

            '    Me.Exception = New Exception("WorkItem hasn't finished within the specified timeout.")

            '    If Not Me.WorkerThread Is Nothing Then
            '        Try
            '            Me.WorkerThread.Abort()

            '        Catch ex As Exception
            '            ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me.ThreadPool, New ApplicationException("Error aborting WorkItem thread because of timeout.", ex)))

            '        End Try
            '    End If

            '    If Not Me.WaitHandle Is Nothing Then
            '        Me.WaitHandle.Set()

            '    Else 'There is currently no one waiting on the handle, so we start a dispose timer to make sure we get the object disposed.
            '        Me.TmrDispose = New Timers.Timer
            '        Me.TmrDispose.Interval = TimeSpan.FromSeconds(10).TotalMilliseconds
            '        Me.TmrDispose.Enabled = True

            '    End If

            '    If Not Me.ThreadPool Is Nothing Then Me.ThreadPool.DequeueWorkItem(Me)

            '    Me._Completed = True

            'Catch ex As Exception
            '    ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me.ThreadPool, New ApplicationException("Error in handling WorkItem timeout.", ex)))

            'End Try
        End Sub

        Private Sub TmrDispose_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrDispose.Elapsed
            Try
                Me.TmrDispose.Enabled = False
                _DebugText &= "Dispose timer elapsed - Disposing WorkItem" & vbCrLf

                Me.Dispose()

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me.ThreadPool, "Error in handling WorkItem automatic dispose.", ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

        Private Sub OnTimedOut(ByVal e As EventArgs.WorkItemEventArgs)
            If Not Me.ThreadPool Is Nothing Then
                Me.ThreadPool.WorkItemsTimedOutPerformanceCounter.Increment()

                Me.ThreadPool.OnWorkItemTimedOut(New EventArgs.WorkItemEventArgs(Me))

            End If

            RaiseEvent TimedOut(Me, e)

        End Sub

        Public Overrides Function ToString() As String
            If String.IsNullOrEmpty(Me.Name) Then
                Return Me.TargetName
            Else
                Return Me.Name
            End If

        End Function

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.Disposed Then
                If disposing Then
                    If Not Me.WaitHandle Is Nothing Then Me.WaitHandle.Close()

                    '_PerformanceAnalyzer.AddPerformanceMark("Closing WaitHandle")

                    If Not Me.TimedOutEvent Is Nothing Then
                        For Each tmpDelegate As System.Delegate In Me.TimedOutEvent.GetInvocationList
                            RemoveHandler TimedOut, tmpDelegate

                        Next

                    End If
                End If

                'If Not Me.ThreadPool Is Nothing Then
                '    Me.ThreadPool.DequeueWorkItem(Me)

                'End If

                If Not Me.TmrTimeout Is Nothing Then
                    Me.TmrTimeout.Enabled = False
                    Me.TmrTimeout.Dispose()
                    Me.TmrTimeout = Nothing

                End If

                If Not Me.TmrDispose Is Nothing Then
                    Me.TmrDispose.Enabled = False
                    Me.TmrDispose.Dispose()
                    Me.TmrDispose = Nothing

                End If

                If HasBeenExecuted Then Me.ThreadPool = Nothing

                Me.Callback = Nothing
                Me.ParameterLessCallback = Nothing
                Me.Parameters = Nothing
                Me.Exception = Nothing
                Me.Result = Nothing
                Me.WorkerThread = Nothing
                Me.WaitHandle = Nothing

                If _PerformanceAnalyzer.TotalDuration > TimeSpan.FromSeconds(3) Then
                    ThreadPoolManager.ThreadPoolPerformanceLog.WriteEntry(_PerformanceAnalyzer.GetDifferentialResult)

                End If

                Me.Disposed = True

                _PerformanceAnalyzer.Dispose()
                _PerformanceAnalyzer = Nothing

            End If

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Try
                ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(True)
                GC.SuppressFinalize(Me)

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me.ThreadPool, "Error disposing WorkItem.", ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

    End Class

End Namespace