Imports System.Threading

Namespace Threading

    Friend Class WorkItemQueue
        Inherits System.Collections.Generic.Queue(Of WorkItem)

#Region "Variables"

        Private _ThreadPool As ThreadPool

        'Private _Lock As New Object

#End Region

#Region "Properties"

        Private Property ThreadPool() As ThreadPool
            Get
                Return Me._ThreadPool

            End Get
            Set(ByVal value As ThreadPool)
                Me._ThreadPool = value

            End Set
        End Property

        'Private ReadOnly Property Lock() As Object
        '    Get
        '        Return Me._Lock

        '    End Get
        'End Property

#End Region

        Public Sub New(ByVal threadpool As ThreadPool)
            MyBase.New()

            Me.ThreadPool = threadpool

        End Sub

        'Public Overloads Function Enqueue(ByVal item As WorkItem) As Integer
        '    Monitor.Enter(Me.Lock)

        '    Try
        '        MyBase.Enqueue(item)

        '        Return Me.Count

        '    Finally
        '        Monitor.Exit(Me.Lock)

        '    End Try


        'End Function
        Public Overloads Sub Enqueue(ByVal item As WorkItem)
            MyBase.Enqueue(item)

            Me.ThreadPool.QueuedThreadCountPerformanceCounter.Increment()

        End Sub

        Public Overloads Function Dequeue() As WorkItem
            Try
                Dim tmpWorkItem As WorkItem = MyBase.Dequeue

                Me.ThreadPool.QueuedThreadCountPerformanceCounter.Decrement()

                Return tmpWorkItem

            Catch ex As Exception
                'SILENT

                Return Nothing

            End Try
        End Function

    End Class

End Namespace