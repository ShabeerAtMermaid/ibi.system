#Region "Flush implementation"

'Imports mermaid.BaseObjects.Net.Sockets
'Imports System.Threading

'Namespace Threading

'    Public Class ThreadPool
'        Implements IDisposable

'#Region "Variables"

'        Private _Name As String
'        Private _WorkItemsQueued As New WorkItemQueue
'        Private _WorkItemsActive As New WorkItemCollection
'        Private _MinThreads As Integer = 0
'        Private _MaxThreads As Integer = 5
'        Private _QueueWarningLimit As Integer = 500
'        Private _ThreadSize As Long = 1 * ThreadPoolInfo.MB
'        Private _ThreadIDleTimeout As TimeSpan = TimeSpan.FromSeconds(30)
'        Private _GenerelLock As New Object
'        Private _ActiveItemsLock As New Object
'        Private _QueuedItemsLock As New Object
'        Private _WorkerThreadLock As New Object

'        Private _Disposed As Boolean

'        Private _WorkerThreads As New Generic.List(Of WorkerThreadInfo)
'        Private _AvailableWorkerThreads As New Generic.Stack(Of WorkerThreadInfo)

'        Private _TotalThreadCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
'        Private _ActiveThreadCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter

'        Private _DebugBroadcaster As UDPBroadcaster

'#End Region

'#Region "Properties"

'        Public ReadOnly Property Name() As String
'            Get
'                Return Me._Name

'            End Get
'        End Property

'        Private Property WorkItemsQueued() As WorkItemQueue
'            Get
'                Return Me._WorkItemsQueued

'            End Get
'            Set(ByVal value As WorkItemQueue)
'                Me._WorkItemsQueued = value

'            End Set
'        End Property

'        Public ReadOnly Property QueuedItems() As Integer
'            Get
'                'Me.Lock(Locks.QueuedItemsLock)

'                Try
'                    Return Me.WorkItemsQueued.Count

'                Finally
'                    'Me.Unlock(Locks.QueuedItemsLock)

'                End Try
'            End Get
'        End Property

'        Private Property WorkItemsActive() As WorkItemCollection
'            Get
'                Return Me._WorkItemsActive

'            End Get
'            Set(ByVal value As WorkItemCollection)
'                Me._WorkItemsActive = value

'            End Set
'        End Property

'        Public ReadOnly Property ActiveItems() As Integer
'            Get
'                'Me.Lock(Locks.ActiveItemsLock)

'                Try
'                    Return Me.WorkItemsActive.Count

'                Finally
'                    'Me.Unlock(Locks.ActiveItemsLock)

'                End Try
'            End Get
'        End Property

'        Public ReadOnly Property CurrentThreadCount() As Integer
'            Get
'                Return Me.WorkerThreads.Count

'            End Get
'        End Property

'        Public Property MinThreads() As Integer
'            Get
'                Return Me._MinThreads

'            End Get
'            Set(ByVal value As Integer)
'                If value < 0 Then
'                    Throw New ArgumentException("Value must be 0 or positive", "MinThreads")

'                End If

'                Me._MinThreads = value

'            End Set
'        End Property

'        Public Property MaxThreads() As Integer
'            Get
'                Return Me._MaxThreads

'            End Get
'            Set(ByVal value As Integer)
'                If value <= 0 Then
'                    Throw New ArgumentException("Value must be greater than 0", "MaxThreads")

'                End If

'                Me._MaxThreads = value

'            End Set
'        End Property

'        Public Property QueueWarningLimit() As Integer
'            Get
'                Return Me._QueueWarningLimit

'            End Get
'            Set(ByVal value As Integer)
'                Me._QueueWarningLimit = value

'            End Set
'        End Property

'        Public Property ThreadSize() As Long
'            Get
'                Return Me._ThreadSize

'            End Get
'            Set(ByVal value As Long)
'                Me._ThreadSize = value

'            End Set
'        End Property

'        Public Property ThreadIDleTimeout() As TimeSpan
'            Get
'                Return Me._ThreadIDleTimeout

'            End Get
'            Set(ByVal value As TimeSpan)
'                Me._ThreadIDleTimeout = value

'            End Set
'        End Property

'        Private ReadOnly Property GenerelLock() As Object
'            Get
'                Return Me._GenerelLock

'            End Get
'        End Property

'        Private ReadOnly Property ActiveItemsLock() As Object
'            Get
'                Return Me._ActiveItemsLock

'            End Get
'        End Property

'        Private ReadOnly Property QueuedItemsLock() As Object
'            Get
'                Return Me._QueuedItemsLock

'            End Get
'        End Property

'        Private ReadOnly Property WorkerThreadLock() As Object
'            Get
'                Return Me._WorkerThreadLock

'            End Get
'        End Property

'        Private Property Disposed() As Boolean
'            Get
'                Return Me._Disposed

'            End Get
'            Set(ByVal value As Boolean)
'                Me._Disposed = value

'            End Set
'        End Property

'        Private ReadOnly Property WorkerThreads() As Generic.List(Of WorkerThreadInfo)
'            Get
'                Return Me._WorkerThreads

'            End Get
'        End Property

'        Private ReadOnly Property AvailableWorkerThreads() As Generic.Stack(Of WorkerThreadInfo)
'            Get
'                Return Me._AvailableWorkerThreads

'            End Get
'        End Property

'        Private Property TotalThreadCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
'            Get
'                Return Me._TotalThreadCountPerformanceCounter

'            End Get
'            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
'                Me._TotalThreadCountPerformanceCounter = value

'            End Set
'        End Property

'        Private Property ActiveThreadCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
'            Get
'                Return Me._ActiveThreadCountPerformanceCounter

'            End Get
'            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
'                Me._ActiveThreadCountPerformanceCounter = value

'            End Set
'        End Property

'        Private Property DebugBroadcaster() As UDPBroadcaster
'            Get
'                Return Me._DebugBroadcaster

'            End Get
'            Set(ByVal value As UDPBroadcaster)
'                Me._DebugBroadcaster = value

'            End Set
'        End Property

'#End Region

'#Region "Enums"

'        Private Enum Locks As Integer
'            'GenerelLock = 0
'            ActiveItemsLock = 1
'            QueuedItemsLock = 2
'            WorkerThreadLock = 3

'        End Enum

'#End Region

'#Region "Events"

'        Public Event QueueWarningLimitExceeded As System.EventHandler

'#End Region

'        Friend Sub New(ByVal name As String)
'            MyBase.New()

'            Me._Name = name
'            Me.InitializePerformanceCounters()

'        End Sub

'        Private Sub InitializePerformanceCounters()
'            Try
'                Me.TotalThreadCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ total threads")
'                Me.ActiveThreadCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ active threads")

'            Catch ex As Exception
'                ThreadPoolManager.OnThreadPoolExceptionOccured(New mermaid.BaseObjects.EventArgs.ThreadPoolExceptionEventArgs(Me, ex))

'            End Try
'        End Sub

'        Public Sub InitializeMinThreads()
'            Me.Lock(Locks.WorkerThreadLock)

'            Try
'                For i As Integer = Me.WorkerThreads.Count To Me.MinThreads
'                    WorkerThreadInfo.Create(Me)

'                Next

'            Finally
'                Me.Unlock(Locks.WorkerThreadLock)

'            End Try
'        End Sub

'        Public Sub EnqueueWorkItem(ByVal workItem As WorkItem)
'            Dim tmpStartMark As Integer = System.Environment.TickCount And Integer.MaxValue
'            Dim tmpLockMark As Integer
'            Dim tmpEnqueueMark As Integer
'            Dim tmpQueueWarningMark As Integer
'            Dim tmpFlushMark As Integer

'            Try
'                'Me.Lock(Locks.GenerelLock)

'                Try
'                    If Me.Disposed Then
'                        Throw New ObjectDisposedException("ThreadPool")

'                    End If

'                    If workItem Is Nothing Then Throw New ArgumentNullException("workItem")

'                    Dim tmpQueueCount As Integer

'                    workItem.ThreadPool = Me
'                    workItem.Enqueued()

'                    Me.Lock(Locks.QueuedItemsLock)

'                    tmpLockMark = System.Environment.TickCount And Integer.MaxValue

'                    Try
'                        Me.WorkItemsQueued.Enqueue(workItem)
'                        tmpQueueCount = Me.WorkItemsQueued.Count

'                    Finally
'                        Me.Unlock(Locks.QueuedItemsLock)

'                    End Try

'                    tmpEnqueueMark = System.Environment.TickCount And Integer.MaxValue

'                    If Me.QueueWarningLimit > 0 And tmpQueueCount > Me.QueueWarningLimit Then
'                        Me.OnQueueWarningLimitExceeded(New System.EventArgs)

'                    End If

'                    tmpQueueWarningMark = System.Environment.TickCount And Integer.MaxValue

'                Finally
'                    'Me.Unlock(Locks.GenerelLock)

'                End Try

'                Me.FlushQueue()
'                'Dim tmpThread As New Thread(AddressOf ExecuteFlushQueue)
'                'tmpThread.Name = "ThreadPool """ & Me.Name & """ Flush Thread"
'                'tmpThread.Start()

'                tmpFlushMark = System.Environment.TickCount And Integer.MaxValue

'            Catch ex As Exception
'                'DELETE: Console
'                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
'                Console.WriteLine(ex.Message & " " & ex.StackTrace)

'            Finally
'                If Not Me.DebugBroadcaster Is Nothing Then
'                    Dim sb As New System.Text.StringBuilder
'                    sb.AppendLine("ThreadPool(""" & Me.Name & """).EnqueueWorkItem")
'                    sb.AppendLine("Enter lock:              " & tmpLockMark - tmpStartMark)
'                    sb.AppendLine("Enqueue item:            " & tmpEnqueueMark - tmpLockMark)
'                    sb.AppendLine("Check for queue warning: " & tmpQueueWarningMark - tmpEnqueueMark)
'                    sb.AppendLine("Spawn flush thread:      " & tmpFlushMark - tmpQueueWarningMark)
'                    sb.AppendLine("Total:                   " & tmpFlushMark - tmpStartMark)
'                    sb.AppendLine("----------------------------------------------------------------")

'                    Me.DebugBroadcaster.WriteLine(sb.ToString)

'                End If
'            End Try

'        End Sub

'        Friend Sub DequeueWorkItem(ByVal workItem As WorkItem)
'            Me.Lock(Locks.ActiveItemsLock)

'            Try
'                Me.WorkItemsActive.Remove(workItem)

'            Finally
'                Me.Unlock(Locks.ActiveItemsLock)

'            End Try

'            'Me.FlushQueue()

'        End Sub

'        Public Function GetQueuedItemsNames() As String()
'            Dim tmpWorkItemList() As WorkItem = Nothing
'            Dim tmpResultList As New ArrayList

'            Me.Lock(Locks.QueuedItemsLock)

'            Try
'                tmpWorkItemList = Me.WorkItemsQueued.ToArray

'            Finally
'                Me.Unlock(Locks.QueuedItemsLock)

'            End Try

'            If Not tmpWorkItemList Is Nothing Then
'                For Each tmpWorkItem As WorkItem In tmpWorkItemList
'                    tmpResultList.Add(tmpWorkItem.TargetName)

'                Next
'            End If

'            Return tmpResultList.ToArray(GetType(String))

'        End Function

'        Public Function GetActiveItemsNames() As String()
'            Dim tmpWorkItemList() As WorkItem = Nothing
'            Dim tmpResultList As New ArrayList

'            Me.Lock(Locks.ActiveItemsLock)

'            Try
'                tmpWorkItemList = Me.WorkItemsActive.ToArray

'            Finally
'                Me.Unlock(Locks.ActiveItemsLock)

'            End Try

'            If Not tmpWorkItemList Is Nothing Then
'                For Each tmpWorkItem As WorkItem In tmpWorkItemList
'                    tmpResultList.Add(tmpWorkItem.TargetName)

'                Next
'            End If

'            Return tmpResultList.ToArray(GetType(String))

'        End Function

'        Public Sub EnableDebugBroadcaster(ByVal port As Integer)
'            Me.DebugBroadcaster = New UDPBroadcaster(port)

'        End Sub

'        Public Sub DisableDebugBroadcaster()
'            If Not Me.DebugBroadcaster Is Nothing Then
'                Me.DebugBroadcaster.Dispose()

'            End If

'            Me.DebugBroadcaster = Nothing

'        End Sub

'        Private Sub ExecuteFlushQueue()
'            Try
'                Me.FlushQueue()

'            Catch ex As Exception
'                ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me, ex))

'            End Try
'        End Sub

'        Private Sub FlushQueue()
'            'Dim tmpDebugText As New System.Text.StringBuilder
'            'Dim tmpWriteLog As Boolean = True

'            'Dim tmpStartMark As Integer = System.Environment.TickCount And Integer.MaxValue
'            'Dim tmpLockMark As Integer
'            'Dim tmpStartFlushMark As Integer
'            'Dim tmpWorkerThreadMark As Integer
'            'Dim tmpDequeueMark As Integer
'            'Dim tmpProcessMark As Integer

'            'Me.Lock(Locks.GenerelLock)

'            Try
'                If Not Me.Disposed Then
'                    Dim tmpWorkItem As WorkItem = Nothing

'                    Me.Lock(Locks.QueuedItemsLock)

'                    Try
'                        tmpWorkItem = Me.WorkItemsQueued.Dequeue

'                    Catch ex As Exception
'                        'SILENT

'                    Finally
'                        Me.Unlock(Locks.QueuedItemsLock)

'                    End Try

'                    If Not tmpWorkItem Is Nothing Then
'                        Me.Lock(Locks.ActiveItemsLock)

'                        Try
'                            If Me.ActiveItems < Me.MaxThreads Then
'                                'tmpStartFlushMark = System.Environment.TickCount And Integer.MaxValue

'                                Try
'                                    Me.Lock(Locks.WorkerThreadLock)

'                                    'tmpLockMark = System.Environment.TickCount And Integer.MaxValue

'                                    Dim tmpWorkerThread As WorkerThreadInfo = Nothing

'                                    Try
'                                        If Me.AvailableWorkerThreads.Count > 0 Then
'                                            tmpWorkerThread = Me.AvailableWorkerThreads.Pop()

'                                        ElseIf Me.CurrentThreadCount < Me.MaxThreads Then
'                                            tmpWorkerThread = WorkerThreadInfo.Create(Me)

'                                        End If

'                                    Finally
'                                        Me.Unlock(Locks.WorkerThreadLock)

'                                    End Try

'                                    'tmpWorkerThreadMark = System.Environment.TickCount And Integer.MaxValue

'                                    If Not tmpWorkerThread Is Nothing Then
'                                        'tmpDequeueMark = System.Environment.TickCount And Integer.MaxValue

'                                        If Not tmpWorkItem.IsDisposed Then
'                                            'tmpDebugText.AppendLine("Processing WorkItem")

'                                            tmpWorkerThread.ProcessWorkItem(tmpWorkItem)

'                                            'tmpProcessMark = System.Environment.TickCount And Integer.MaxValue

'                                        Else
'                                            'tmpDebugText.AppendLine("WorkItem is disposed")

'                                            Me.FlushQueue()

'                                        End If
'                                    End If

'                                Catch ex As Exception
'                                    ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me, New ApplicationException("Error dequeuing or starting WorkItem.", ex)))

'                                End Try

'                            Else
'                                'tmpDebugText.AppendLine("Max threads(" & Me.MaxThreads & ") already in use")

'                            End If

'                        Finally
'                            Me.Unlock(Locks.ActiveItemsLock)

'                        End Try

'                    Else
'                        'tmpDebugText.AppendLine("No items in queue")
'                        'tmpWriteLog = False

'                    End If

'                Else
'                    'tmpDebugText.AppendLine("ThreadPool is disposed")

'                End If

'            Finally
'                'If Not Me.DebugBroadcaster Is Nothing And tmpWriteLog Then
'                '    Dim sb As New System.Text.StringBuilder
'                '    sb.AppendLine("ThreadPool(""" & Me.Name & """).FlushQueue")
'                '    sb.AppendLine("Starting flush:          " & tmpStartFlushMark - tmpStartMark)
'                '    sb.AppendLine("Enter lock:              " & tmpLockMark - tmpStartFlushMark)
'                '    sb.AppendLine("Get worker thread:       " & tmpWorkerThreadMark - tmpLockMark)
'                '    sb.AppendLine("Dequeue WorkItem:        " & tmpDequeueMark - tmpWorkerThreadMark)
'                '    sb.AppendLine("Process WorkItem:        " & tmpProcessMark - tmpDequeueMark)
'                '    sb.AppendLine("Total:                   " & tmpProcessMark - tmpStartMark)
'                '    sb.AppendLine()
'                '    sb.Append(tmpDebugText.ToString)
'                '    sb.AppendLine("----------------------------------------------------------------")

'                '    Me.DebugBroadcaster.WriteLine(sb.ToString)

'                'End If

'                'Me.Unlock(Locks.GenerelLock)

'            End Try
'        End Sub

'        'Private Function GetWorkItem() As WorkItem
'        '    If Me.QueueGate.WaitOne(Me._ThreadIDleTimeout, False) Then
'        '        Me.Lock()

'        '        Try
'        '            While True
'        '                If Me.WorkItemsQueued.Count > 0 Then
'        '                    Dim tmpWorkItem As WorkItem = Me.WorkItemsQueued.Dequeue

'        '                    If Not tmpWorkItem.IsDisposed Then
'        '                        Try
'        '                            Return tmpWorkItem

'        '                        Catch ex As Exception
'        '                            ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me, New ApplicationException("Error dequeuing or starting WorkItem.", ex)))

'        '                        End Try

'        '                    End If

'        '                Else
'        '                    Return Nothing

'        '                End If

'        '            End While

'        '            Return Nothing

'        '        Finally
'        '            If Me.WorkItemsQueued.Count > 0 Then
'        '                Me.QueueGate.Set()

'        '            End If

'        '            Me.Unlock()

'        '        End Try

'        '    Else
'        '        Return Nothing

'        '    End If

'        'End Function

'        Private Function DisposeWorkerThread(ByVal threadInfo As WorkerThreadInfo) As Boolean
'            Me.Lock(Locks.WorkerThreadLock)

'            Try
'                If Me.WorkerThreads.Count > Me.MinThreads Then
'                    Me.TotalThreadCountPerformanceCounter.Decrement()

'                    Me.WorkerThreads.Remove(threadInfo)
'                    Me.RemoveAvailableWorkerThread(threadInfo)

'                    Return True

'                Else
'                    Return False

'                End If

'            Catch ex As Exception
'                ThreadPoolManager.OnThreadPoolExceptionOccured(New EventArgs.ThreadPoolExceptionEventArgs(Me, ex))

'            Finally
'                Me.Unlock(Locks.WorkerThreadLock)

'            End Try
'        End Function

'        Private Sub RemoveAvailableWorkerThread(ByVal threadInfo As WorkerThreadInfo)
'            Me.Lock(Locks.WorkerThreadLock)

'            Try
'                Dim tmpWorkerThreadList() As WorkerThreadInfo = Me.AvailableWorkerThreads.ToArray

'                Me.AvailableWorkerThreads.Clear()

'                For i As Integer = 0 To tmpWorkerThreadList.Length - 1
'                    Dim tmpWorkerThread As WorkerThreadInfo = tmpWorkerThreadList(tmpWorkerThreadList.Length - i - 1)

'                    If Not tmpWorkerThread Is threadInfo Then
'                        Me.AvailableWorkerThreads.Push(tmpWorkerThread)

'                    End If
'                Next

'            Finally
'                Me.Unlock(Locks.WorkerThreadLock)

'            End Try
'        End Sub

'        Private Sub Lock(ByVal lock As Locks)
'            'If (lock And Locks.GenerelLock) = Locks.GenerelLock Then
'            '    Monitor.Enter(Me.GenerelLock)

'            'End If

'            If (lock And Locks.ActiveItemsLock) = Locks.ActiveItemsLock Then
'                Monitor.Enter(Me.ActiveItemsLock)

'            End If

'            If (lock And Locks.QueuedItemsLock) = Locks.QueuedItemsLock Then
'                Monitor.Enter(Me.QueuedItemsLock)

'            End If

'            If (lock And Locks.WorkerThreadLock) = Locks.WorkerThreadLock Then
'                Monitor.Enter(Me.WorkerThreadLock)

'            End If

'            Management.LockManager.LockEntered(Me)

'        End Sub

'        Private Sub Unlock(ByVal lock As Locks)
'            'If (lock And Locks.GenerelLock) = Locks.GenerelLock Then
'            '    Monitor.Exit(Me.GenerelLock)

'            'End If

'            If (lock And Locks.ActiveItemsLock) = Locks.ActiveItemsLock Then
'                Monitor.Exit(Me.ActiveItemsLock)

'            End If

'            If (lock And Locks.QueuedItemsLock) = Locks.QueuedItemsLock Then
'                Monitor.Exit(Me.QueuedItemsLock)

'            End If

'            If (lock And Locks.WorkerThreadLock) = Locks.WorkerThreadLock Then
'                Monitor.Exit(Me.WorkerThreadLock)

'            End If

'            Management.LockManager.LockExited(Me)

'        End Sub

'        Private Sub OnQueueWarningLimitExceeded(ByVal e As System.EventArgs)
'            Try
'                RaiseEvent QueueWarningLimitExceeded(Me, e)

'            Catch ex As Exception
'                'SILENT

'            End Try
'        End Sub

'        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
'            If Not Me.Disposed Then
'                If disposing Then

'                End If

'            End If

'            Me.Disposed = True

'        End Sub

'        Public Sub Dispose() Implements IDisposable.Dispose
'            Dispose(True)
'            GC.SuppressFinalize(Me)

'        End Sub

'        Public Class ThreadPoolInfo

'#Region "Variables"

'            Private _MinThreads As Integer = 0
'            Private _MaxThreads As Integer = 5
'            Private _QueueWarningLimit As Integer = 0
'            Private _ThreadSize As Long = 1 * MB
'            Private _ThreadIDleTimeout As TimeSpan = TimeSpan.FromSeconds(30)

'#End Region

'#Region "Properties"

'            Public Property MinThreads() As Integer
'                Get
'                    Return Me._MinThreads

'                End Get
'                Set(ByVal value As Integer)
'                    If value <= 0 Then
'                        Throw New ArgumentException("Value must be 0 or positive", "MaxThreads")

'                    End If

'                    Me._MinThreads = value

'                End Set
'            End Property

'            Public Property MaxThreads() As Integer
'                Get
'                    Return Me._MaxThreads

'                End Get
'                Set(ByVal value As Integer)
'                    If value <= 0 Then
'                        Throw New ArgumentException("Value must be greater than 0", "MaxThreads")

'                    End If

'                    Me._MaxThreads = value

'                End Set
'            End Property

'            Public Property QueueWarningLimit() As Integer
'                Get
'                    Return Me._QueueWarningLimit

'                End Get
'                Set(ByVal value As Integer)
'                    Me._QueueWarningLimit = value

'                End Set
'            End Property

'            Public Property ThreadSize() As Long
'                Get
'                    Return Me._ThreadSize

'                End Get
'                Set(ByVal value As Long)
'                    Me._ThreadSize = value

'                End Set
'            End Property

'            Public Property ThreadIDleTimeout() As TimeSpan
'                Get
'                    Return Me._ThreadIDleTimeout

'                End Get
'                Set(ByVal value As TimeSpan)
'                    Me._ThreadIDleTimeout = value

'                End Set
'            End Property

'#End Region

'#Region "Constants"

'            Public Const B As Long = 1L
'            Public Const KB As Long = 1024L * B
'            Public Const MB As Long = 1024L * KB

'#End Region

'        End Class

'        Private Class WorkerThreadInfo

'#Region "Variables"

'            Private _ThreadPool As ThreadPool
'            Private _WorkerThread As Thread
'            Private _WorkItem As WorkItem
'            Private _Gate As AutoResetEvent

'            Private _IsDisposed As Boolean

'#End Region

'#Region "Properties"

'            Public ReadOnly Property ThreadPool() As ThreadPool
'                Get
'                    Return Me._ThreadPool

'                End Get
'            End Property

'            Private Property WorkerThread() As Thread
'                Get
'                    Return Me._WorkerThread

'                End Get
'                Set(ByVal value As Thread)
'                    Me._WorkerThread = value

'                End Set
'            End Property

'            Private Property WorkItem() As WorkItem
'                Get
'                    Return Me._WorkItem

'                End Get
'                Set(ByVal value As WorkItem)
'                    Me._WorkItem = value

'                End Set
'            End Property

'            Private ReadOnly Property Gate() As AutoResetEvent
'                Get
'                    Return Me._Gate

'                End Get
'            End Property

'            Private Property IsDisposed() As Boolean
'                Get
'                    Return Me._IsDisposed

'                End Get
'                Set(ByVal value As Boolean)
'                    Me._IsDisposed = value

'                End Set
'            End Property

'#End Region

'            Public Sub New(ByVal threadPool As ThreadPool)
'                MyBase.New()

'                Me._Gate = New AutoResetEvent(False)

'                Me._ThreadPool = threadPool

'                Me.ThreadPool.TotalThreadCountPerformanceCounter.Increment()

'                Me.ThreadPool.WorkerThreads.Add(Me)

'                Me.WorkerThread = New Thread(New ThreadStart(AddressOf Execute), Me.ThreadPool.ThreadSize)
'                Me.WorkerThread.Name = "ThreadPool(" & Me.ThreadPool.Name & ") thread"
'                Me.WorkerThread.IsBackground = True
'                Me.WorkerThread.Start()

'            End Sub

'            Private Sub Execute()
'                While Not Me.IsDisposed
'                    If Me.Gate.WaitOne(Me.ThreadPool.ThreadIDleTimeout, False) Then
'                        If Not Me.WorkItem Is Nothing Then
'                            Me.WorkItem.Execute()

'                            Me.ThreadPool.ActiveThreadCountPerformanceCounter.Decrement()
'                            Me.ThreadPool.AvailableWorkerThreads.Push(Me)

'                            Me.ThreadPool.FlushQueue()

'                        End If
'                    Else
'                        'There was no WorkItem to process and the idle timeout has been reached
'                        If Me.Dispose() Then
'                            Exit While

'                        End If
'                    End If

'                End While

'            End Sub

'            Public Sub ProcessWorkItem(ByVal workItem As WorkItem)
'                Me.WorkItem = workItem

'                If Not Me.WorkItem Is Nothing Then
'                    Me.ThreadPool.RemoveAvailableWorkerThread(Me)

'                    Me.ThreadPool.Lock(Locks.ActiveItemsLock)

'                    Try
'                        Me.ThreadPool.WorkItemsActive.Add(Me.WorkItem)

'                    Finally
'                        Me.ThreadPool.Unlock(Locks.ActiveItemsLock)

'                    End Try

'                    Me.ThreadPool.ActiveThreadCountPerformanceCounter.Increment()

'                End If

'                Me.Gate.Set()

'            End Sub

'            Public Shared Function Create(ByVal threadPool As ThreadPool) As WorkerThreadInfo
'                Return New WorkerThreadInfo(threadPool)

'            End Function

'            Public Function Dispose() As Boolean
'                If Not Me.IsDisposed Then
'                    If Me.ThreadPool.DisposeWorkerThread(Me) Then
'                        Me.IsDisposed = True

'                        Me._ThreadPool = Nothing
'                        Me._WorkerThread = Nothing

'                        Me.Gate.Set()
'                        Me.Gate.Close()
'                        Me._Gate = Nothing

'                        Return True

'                    Else
'                        'Reuse thread since threadpool has MinThreads in pool

'                        Return False

'                    End If
'                End If

'            End Function

'        End Class

'    End Class

'End Namespace

#End Region

#Region "Gate implementation"

Imports mermaid.BaseObjects.Diagnostics
Imports mermaid.BaseObjects.Net.Sockets
Imports System.Threading

Namespace Threading

    Public Class ThreadPool
        Implements IDisposable

#Region "Variables"

        Private _Name As String
        Private _WorkItemsQueued As WorkItemQueue
        Private _WorkItemsActive As New ArrayList ' WorkItemCollection
        Private _MinThreads As Integer = 0
        Private _MaxThreads As Integer = 5
        Private _MaxQueue As Integer = Integer.MaxValue
        Private _QueueWarningLimit As Integer = 500
        Private _ThreadSize As Long = 1 * ThreadPoolInfo.MB
        Private _ThreadIdleTimeout As TimeSpan = TimeSpan.FromSeconds(30)
        Private _GenerelLock As New Object
        Private _ActiveItemsLock As New Object
        Private _QueuedItemsLock As New Object
        Private _WorkerThreadLock As New Object

        Private _FlushGate As AutoResetEvent

        Private _Disposed As Boolean

        Private _WorkerThreads As New Generic.List(Of WorkerThreadInfo)
        Private _AvailableWorkerThreads As New Generic.Stack(Of WorkerThreadInfo)

        Private _TotalThreadCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _ActiveThreadCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _QueuedThreadCountPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _TotalWorkItemsProcessedPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter
        Private _WorkItemsTimedOutPerformanceCounter As BaseObjects.Diagnostics.PerformanceCounter

        Private _DebugBroadcaster As UDPBroadcaster

#End Region

#Region "Properties"

        Public ReadOnly Property Name() As String
            Get
                Return Me._Name

            End Get
        End Property

        Private Property WorkItemsQueued() As WorkItemQueue
            Get
                Return Me._WorkItemsQueued

            End Get
            Set(ByVal value As WorkItemQueue)
                Me._WorkItemsQueued = value

            End Set
        End Property

        Public ReadOnly Property QueuedItems() As Integer
            Get
                Return Me.WorkItemsQueued.Count

            End Get
        End Property

        Private Property WorkItemsActive() As ArrayList ' WorkItemCollection
            Get
                Return Me._WorkItemsActive

            End Get
            Set(ByVal value As ArrayList)
                Me._WorkItemsActive = value

            End Set
        End Property

        Public ReadOnly Property ActiveItems() As Integer
            Get
                'Me.Lock(Locks.ActiveItemsLock)

                Try
                    Return Me.WorkItemsActive.Count

                Finally
                    'Me.Unlock(Locks.ActiveItemsLock)

                End Try
            End Get
        End Property

        Public ReadOnly Property CurrentThreadCount() As Integer
            Get
                Return Me.WorkerThreads.Count

            End Get
        End Property

        Public Property MinThreads() As Integer
            Get
                Return Me._MinThreads

            End Get
            Set(ByVal value As Integer)
                If value < 0 Then
                    Throw New ArgumentException("Value must be 0 or positive", "MinThreads")

                End If

                Me._MinThreads = value

            End Set
        End Property

        Public Property MaxThreads() As Integer
            Get
                Return Me._MaxThreads

            End Get
            Set(ByVal value As Integer)
                If value <= 0 Then
                    Throw New ArgumentException("Value must be greater than 0", "MaxThreads")

                End If

                Me._MaxThreads = value

            End Set
        End Property

        Public Property MaxQueue() As Integer
            Get
                Return Me._MaxQueue

            End Get
            Set(ByVal value As Integer)
                Me._MaxQueue = value

            End Set
        End Property

        Public Property QueueWarningLimit() As Integer
            Get
                Return Me._QueueWarningLimit

            End Get
            Set(ByVal value As Integer)
                Me._QueueWarningLimit = value

            End Set
        End Property

        Public Property ThreadSize() As Long
            Get
                Return Me._ThreadSize

            End Get
            Set(ByVal value As Long)
                Me._ThreadSize = value

            End Set
        End Property

        Public Property ThreadIdleTimeout() As TimeSpan
            Get
                Return Me._ThreadIDleTimeout

            End Get
            Set(ByVal value As TimeSpan)
                Me._ThreadIDleTimeout = value

            End Set
        End Property

        Private ReadOnly Property GenerelLock() As Object
            Get
                Return Me._GenerelLock

            End Get
        End Property

        Private ReadOnly Property ActiveItemsLock() As Object
            Get
                Return Me._ActiveItemsLock

            End Get
        End Property

        Private ReadOnly Property QueuedItemsLock() As Object
            Get
                Return Me._QueuedItemsLock

            End Get
        End Property

        Private ReadOnly Property WorkerThreadLock() As Object
            Get
                Return Me._WorkerThreadLock

            End Get
        End Property

        Private Property FlushGate() As AutoResetEvent
            Get
                Return Me._FlushGate

            End Get
            Set(ByVal value As AutoResetEvent)
                Me._FlushGate = value

            End Set
        End Property

        Private Property Disposed() As Boolean
            Get
                Return Me._Disposed

            End Get
            Set(ByVal value As Boolean)
                Me._Disposed = value

            End Set
        End Property

        Private ReadOnly Property WorkerThreads() As Generic.List(Of WorkerThreadInfo)
            Get
                Return Me._WorkerThreads

            End Get
        End Property

        Private ReadOnly Property AvailableWorkerThreads() As Generic.Stack(Of WorkerThreadInfo)
            Get
                Return Me._AvailableWorkerThreads

            End Get
        End Property

        Private Property TotalThreadCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._TotalThreadCountPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._TotalThreadCountPerformanceCounter = value

            End Set
        End Property

        Private Property ActiveThreadCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._ActiveThreadCountPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._ActiveThreadCountPerformanceCounter = value

            End Set
        End Property

        Friend Property QueuedThreadCountPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._QueuedThreadCountPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._QueuedThreadCountPerformanceCounter = value

            End Set
        End Property

        Friend Property TotalWorkItemsProcessedPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._TotalWorkItemsProcessedPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._TotalWorkItemsProcessedPerformanceCounter = value

            End Set
        End Property

        Friend Property WorkItemsTimedOutPerformanceCounter() As BaseObjects.Diagnostics.PerformanceCounter
            Get
                Return Me._WorkItemsTimedOutPerformanceCounter

            End Get
            Set(ByVal value As BaseObjects.Diagnostics.PerformanceCounter)
                Me._WorkItemsTimedOutPerformanceCounter = value

            End Set
        End Property

        Private Property DebugBroadcaster() As UDPBroadcaster
            Get
                Return Me._DebugBroadcaster

            End Get
            Set(ByVal value As UDPBroadcaster)
                Me._DebugBroadcaster = value

            End Set
        End Property

#End Region

#Region "Enums"

        Private Enum Locks As Integer
            'GenerelLock = 0
            ActiveItemsLock = 1
            QueuedItemsLock = 2
            WorkerThreadLock = 3

        End Enum

#End Region

#Region "Events"

        Public Event QueueWarningLimitExceeded As System.EventHandler

        Public Event WorkItemQueued As System.EventHandler(Of EventArgs.WorkItemEventArgs)
        Public Event WorkItemStarted As System.EventHandler(Of EventArgs.WorkItemEventArgs)
        Public Event WorkItemTimedOut As System.EventHandler(Of EventArgs.WorkItemEventArgs)
        Public Event WorkItemProcessed As System.EventHandler(Of EventArgs.WorkItemEventArgs)

        Private Sub OnQueueWarningLimitExceeded(ByVal e As System.EventArgs)
            Try
                RaiseEvent QueueWarningLimitExceeded(Me, e)

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

        Friend Sub OnWorkItemQueued(ByVal e As EventArgs.WorkItemEventArgs)
            Try
                RaiseEvent WorkItemQueued(Me, e)

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

        Friend Sub OnWorkItemStarted(ByVal e As EventArgs.WorkItemEventArgs)
            Try
                RaiseEvent WorkItemStarted(Me, e)

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

        Friend Sub OnWorkItemTimedOut(ByVal e As EventArgs.WorkItemEventArgs)
            Try
                RaiseEvent WorkItemTimedOut(Me, e)

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

        Friend Sub OnWorkItemProcessed(ByVal e As EventArgs.WorkItemEventArgs)
            Try
                RaiseEvent WorkItemProcessed(Me, e)

            Catch ex As Exception
                'SILENT
            End Try
        End Sub

#End Region

        Friend Sub New(ByVal name As String)
            MyBase.New()

            Me._Name = name
            Me._WorkItemsQueued = New WorkItemQueue(Me)

            Me.InitializePerformanceCounters()

            Me.FlushGate = New AutoResetEvent(False)

            'THREAD: ThreadManager
            'Dim tmpThread As New Thread(AddressOf FlushQueue)
            'tmpThread.Name = "ThreadPool(""" & Me.Name & """) Flush thread"

            Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("ThreadPool(""" & Me.Name & """) Flush thread", AddressOf FlushQueue)
            tmpThread.Priority = ThreadPriority.Highest
            tmpThread.Start()

        End Sub

        Private Sub InitializePerformanceCounters()
            Try
                Me.TotalThreadCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ total threads")
                Me.ActiveThreadCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ active threads")
                Me.QueuedThreadCountPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ queued threads")
                Me.TotalWorkItemsProcessedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ total items processed")
                Me.WorkItemsTimedOutPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("ThreadPool """ & Me.Name & """ items timed out")

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me, ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

        Public Sub InitializeMinThreads()
            For i As Integer = Me.WorkerThreads.Count To Me.MinThreads - 1
                Dim tmpThreadInfo As WorkerThreadInfo = WorkerThreadInfo.Create(Me)
                Me.AvailableWorkerThreads.Push(tmpThreadInfo)

            Next

        End Sub

        Public Sub EnqueueWorkItem(ByVal workItem As WorkItem)
            If Me.Disposed Then
                Throw New ObjectDisposedException("ThreadPool")

            End If

            If workItem Is Nothing Then Throw New ArgumentNullException("workItem")

            If Me.QueuedItems >= Me.MaxQueue Then
                Throw New Exception("ThreadPool (" & Me.Name & ") queue is full")

            End If

            workItem.ThreadPool = Me
            workItem.Enqueued()
            workItem._DebugText &= "Enqueued: " & Me.QueuedItems & " in queue and " & Me.ActiveItems & " active items"

            Using New Lock(Me.QueuedItemsLock, True)
                Me.WorkItemsQueued.Enqueue(workItem)

                If Me.QueueWarningLimit > 0 And Me.QueuedItems > Me.QueueWarningLimit Then
                    Me.OnQueueWarningLimitExceeded(New System.EventArgs)

                End If

            End Using

            Me.WriteLog("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Opening FlushGate in EnqueueWorkItem()")
            Me.FlushLog()

            Me.OnWorkItemQueued(New EventArgs.WorkItemEventArgs(workItem))

            Me.FlushGate.Set()

        End Sub

        Friend Sub DequeueWorkItem(ByVal workItem As WorkItem)
            Using New Lock(Me.ActiveItemsLock, True)
                Me.WorkItemsActive.Remove(workItem)
                TotalWorkItemsProcessedPerformanceCounter.Increment()

                'debug
                'workItem.Dispose()
                PerformanceCounterManager.GetPerformanceCounter("ThreadManager WorkItemsActive(debug) count").SetValue(Me.WorkItemsActive.Count)
            End Using

        End Sub

        Public Function GetQueuedItemsNames() As String()
            Using New Lock(Me.ActiveItemsLock, True)
                Dim tmpWorkItemList() As WorkItem = Nothing
                Dim tmpResultList As New ArrayList

                tmpWorkItemList = Me.WorkItemsQueued.ToArray

                If Not tmpWorkItemList Is Nothing Then
                    For Each tmpWorkItem As WorkItem In tmpWorkItemList
                        tmpResultList.Add(tmpWorkItem.TargetName)

                    Next
                End If

                Return tmpResultList.ToArray(GetType(String))

            End Using

        End Function

        Public Function GetActiveItemsNames() As String()
            Using New Lock(Me.ActiveItemsLock, True)
                Dim tmpWorkItemList() As WorkItem = Nothing
                Dim tmpResultList As New ArrayList

                tmpWorkItemList = Me.WorkItemsActive.ToArray(GetType(WorkItem))

                If Not tmpWorkItemList Is Nothing Then
                    For Each tmpWorkItem As WorkItem In tmpWorkItemList
                        tmpResultList.Add(tmpWorkItem.TargetName)

                    Next
                End If

                Return tmpResultList.ToArray(GetType(String))

            End Using

        End Function

        Public Function GetMaxWaitTime() As TimeSpan
            Dim tmpTimeout As TimeSpan = TimeSpan.Zero

            Using New Lock(Me.ActiveItemsLock, True)
                Dim tmpWorkItemList() As WorkItem = Nothing

                tmpWorkItemList = Me.WorkItemsActive.ToArray(GetType(WorkItem))

                If Not tmpWorkItemList Is Nothing Then
                    For Each tmpWorkItem As WorkItem In tmpWorkItemList
                        Dim tmpTimeoutMS As Integer = tmpWorkItem.Timeout.TotalMilliseconds - (Date.Now.Subtract(tmpWorkItem.EnqueueTime).TotalMilliseconds)

                        If tmpTimeoutMS > 0 Then
                            tmpTimeout = tmpTimeout.Add(tmpWorkItem.Timeout)
                        End If
                    Next
                End If
       
                tmpWorkItemList = Me.WorkItemsQueued.ToArray

                If Not tmpWorkItemList Is Nothing Then
                    For Each tmpWorkItem As WorkItem In tmpWorkItemList
                        tmpTimeout = tmpTimeout.Add(tmpWorkItem.Timeout)
                    Next
                End If
            End Using

            Return tmpTimeout

        End Function

        Public Sub EnableDebugBroadcaster(ByVal port As Integer)
            Me.DebugBroadcaster = New UDPBroadcaster(port)
            Me.DebugBroadcaster.AutoFlush = False

        End Sub

        Public Sub DisableDebugBroadcaster()
            If Not Me.DebugBroadcaster Is Nothing Then
                Me.DebugBroadcaster.Dispose()

            End If

            Me.DebugBroadcaster = Nothing

        End Sub

        Private Sub FlushQueue()
            While Not Me.Disposed
                Dim tmpBeforeGateMark As Integer = System.Environment.TickCount And Integer.MaxValue

                Me.FlushGate.WaitOne()

                Using New Lock(Me.WorkerThreadLock, True)
                    Using New Lock(Me.QueuedItemsLock, True)
                        Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
                        sb.AppendLine()

                        Dim tmpAfterGateMark As Integer = System.Environment.TickCount And Integer.MaxValue

                        sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Gate entered - Waited " & tmpAfterGateMark - tmpBeforeGateMark & " ms")

                        If Not Me.Disposed Then
                            Try
                                Dim tmpWorkItem As WorkItem = Nothing
                                Dim tmpWorkerThread As WorkerThreadInfo = Nothing

                                sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Locks entered")

                                Try
                                    If Me.QueuedItems > 0 Then
                                        If Me.AvailableWorkerThreads.Count > 0 Or Me.CurrentThreadCount < Me.MaxThreads Then
                                            If Me.AvailableWorkerThreads.Count > 0 Then
                                                tmpWorkItem = Me.WorkItemsQueued.Dequeue
                                                tmpWorkerThread = Me.AvailableWorkerThreads.Pop()

                                                sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Reusing WorkerThread")

                                            ElseIf Me.CurrentThreadCount < Me.MaxThreads Then
                                                tmpWorkItem = Me.WorkItemsQueued.Dequeue
                                                tmpWorkerThread = WorkerThreadInfo.Create(Me)

                                                sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Creating new WorkerThread")

                                            End If

                                        Else
                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Max threads used: " & Me.CurrentThreadCount & " < " & Me.MaxThreads & " = False (AvailableThreads: " & Me.AvailableWorkerThreads.Count & ")")

                                        End If

                                    Else
                                        sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "No items in queue")

                                    End If

                                Catch ex As Exception
                                    Dim tmpException As New Exceptions.ThreadPoolException(Me, "Error dequeuing WorkItem.", ex)

                                    BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

                                End Try

                                If Not tmpWorkItem Is Nothing Then
                                    sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem Target: " & tmpWorkItem.TargetName)

                                    If Not tmpWorkerThread Is Nothing Then
                                        sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem has been waiting " & Math.Ceiling(Date.Now.Subtract(tmpWorkItem.EnqueueTime).TotalMilliseconds) & " ms")

                                        If Not tmpWorkItem.IsDisposed Then
                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Processing WorkItem")
                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem debug string: " & tmpWorkItem._DebugText)

                                            tmpWorkerThread.ProcessWorkItem(tmpWorkItem)

                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem started")

                                        Else
                                            Me.AvailableWorkerThreads.Push(tmpWorkerThread)

                                            TotalWorkItemsProcessedPerformanceCounter.Increment()

                                            Dim tmpException As New Exceptions.ThreadPoolException(Me, "WorkItem is disposed.")

                                            BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem is disposed")
                                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "WorkItem debug string: " & tmpWorkItem._DebugText)

                                            'UNDONE: Reuse WorkerThread when Workitem disposed
                                            'Using New Lock(Me.WorkerThreadLock, True)
                                            '    Me.AvailableWorkerThreads.Push(tmpWorkerThread)

                                            'End Using

                                        End If

                                    Else
                                        Dim tmpException As New Exceptions.ThreadPoolException(Me, "Could not obtain worker thread.")

                                        BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

                                        sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "No WorkerThread to use")

                                    End If

                                Else
                                    sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "No WorkItem to process")

                                End If

                            Catch ex As Exception
                                Dim tmpException As New Exceptions.ThreadPoolException(Me, "Error flushing or starting WorkItem.", ex)

                                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

                            End Try

                            If (Me.AvailableWorkerThreads.Count > 0 Or Me.CurrentThreadCount < Me.MaxThreads) And Me.QueuedItems > 0 Then
                                sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Further items to process: (" & Me.AvailableWorkerThreads.Count & " > 0 Or " & Me.CurrentThreadCount & " < " & Me.MaxThreads & ") And " & Me.QueuedItems & " > 0 = True")

                                Me.FlushGate.Set()

                            Else
                                sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Further items to process: (" & Me.AvailableWorkerThreads.Count & " > 0 Or " & Me.CurrentThreadCount & " < " & Me.MaxThreads & ") And " & Me.QueuedItems & " > 0 = False")

                            End If

                            Dim tmpEndMark As Integer = System.Environment.TickCount And Integer.MaxValue

                            sb.AppendLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Roundtrip took: " & tmpEndMark - tmpAfterGateMark & " ms")
                            sb.AppendLine("----------------------------------------------------------------------")

                            Me.WriteLog(sb.ToString)

                            Me.FlushLog()

                        End If

                    End Using
                End Using

            End While
        End Sub

        Private Function DisposeWorkerThread(ByVal threadInfo As WorkerThreadInfo) As Boolean
            Try
                Using New Lock(Me.WorkerThreadLock, True)
                    If Me.WorkerThreads.Count > Me.MinThreads Then
                        Me.WorkerThreads.Remove(threadInfo)
                        Me.RemoveAvailableWorkerThread(threadInfo)

                        Me.TotalThreadCountPerformanceCounter.Decrement()

                        Return True

                    Else
                        Return False

                    End If

                End Using

            Catch ex As Exception
                Dim tmpException As New Exceptions.ThreadPoolException(Me, ex)

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Threading))

            End Try
        End Function

        Private Sub RemoveAvailableWorkerThread(ByVal threadInfo As WorkerThreadInfo)
            Using New Lock(Me.WorkerThreadLock, True)
                Dim tmpWorkerThreadList() As WorkerThreadInfo = Me.AvailableWorkerThreads.ToArray

                Me.AvailableWorkerThreads.Clear()

                For i As Integer = 0 To tmpWorkerThreadList.Length - 1
                    Dim tmpWorkerThread As WorkerThreadInfo = tmpWorkerThreadList(tmpWorkerThreadList.Length - i - 1)

                    If Not tmpWorkerThread Is threadInfo Then
                        Me.AvailableWorkerThreads.Push(tmpWorkerThread)

                    End If
                Next
            End Using

        End Sub

        'Private Sub Lock(ByVal lock As Locks)
        '    'If (lock And Locks.GenerelLock) = Locks.GenerelLock Then
        '    '    Monitor.Enter(Me.GenerelLock)

        '    'End If

        '    If (lock And Locks.ActiveItemsLock) = Locks.ActiveItemsLock Then
        '        Monitor.Enter(Me.ActiveItemsLock)

        '    End If

        '    If (lock And Locks.QueuedItemsLock) = Locks.QueuedItemsLock Then
        '        Monitor.Enter(Me.QueuedItemsLock)

        '    End If

        '    If (lock And Locks.WorkerThreadLock) = Locks.WorkerThreadLock Then
        '        Monitor.Enter(Me.WorkerThreadLock)

        '    End If

        '    Management.LockManager.LockEntered(Me)

        'End Sub

        'Private Sub Unlock(ByVal lock As Locks)
        '    'If (lock And Locks.GenerelLock) = Locks.GenerelLock Then
        '    '    Monitor.Exit(Me.GenerelLock)

        '    'End If

        '    If (lock And Locks.ActiveItemsLock) = Locks.ActiveItemsLock Then
        '        Monitor.Exit(Me.ActiveItemsLock)

        '    End If

        '    If (lock And Locks.QueuedItemsLock) = Locks.QueuedItemsLock Then
        '        Monitor.Exit(Me.QueuedItemsLock)

        '    End If

        '    If (lock And Locks.WorkerThreadLock) = Locks.WorkerThreadLock Then
        '        Monitor.Exit(Me.WorkerThreadLock)

        '    End If

        '    Management.LockManager.LockExited(Me)

        'End Sub

        Private Sub WriteLog(ByVal text As String)
            If Not Me.DebugBroadcaster Is Nothing Then
                Me.DebugBroadcaster.WriteLine("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & text)
                Dim str As String = Date.Now.ToString("")

            End If

        End Sub

        Private Sub FlushLog()
            If Not Me.DebugBroadcaster Is Nothing Then
                Me.DebugBroadcaster.Flush()

            End If

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.Disposed Then
                If disposing Then
                    Me.FlushGate.Set()
                    Me.FlushGate.Close()

                End If

            End If

            Me.Disposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

        Public Class ThreadPoolInfo

#Region "Variables"

            Private _MinThreads As Integer = 0
            Private _MaxThreads As Integer = 5
            Private _MaxQueue As Integer = Integer.MaxValue
            Private _QueueWarningLimit As Integer = 0
            Private _ThreadSize As Long = 1 * MB
            Private _ThreadIdleTimeout As TimeSpan = TimeSpan.FromSeconds(30)

#End Region

#Region "Properties"

            Public Property MinThreads() As Integer
                Get
                    Return Me._MinThreads

                End Get
                Set(ByVal value As Integer)
                    If value < 0 Then
                        Throw New ArgumentException("Value must be 0 or positive", "MaxThreads")

                    End If

                    Me._MinThreads = value

                End Set
            End Property

            Public Property MaxThreads() As Integer
                Get
                    Return Me._MaxThreads

                End Get
                Set(ByVal value As Integer)
                    If value <= 0 Then
                        Throw New ArgumentException("Value must be greater than 0", "MaxThreads")

                    End If

                    Me._MaxThreads = value

                End Set
            End Property

            Public Property MaxQueue() As Integer
                Get
                    Return Me._MaxQueue

                End Get
                Set(ByVal value As Integer)
                    Me._MaxQueue = value

                End Set
            End Property

            Public Property QueueWarningLimit() As Integer
                Get
                    Return Me._QueueWarningLimit

                End Get
                Set(ByVal value As Integer)
                    Me._QueueWarningLimit = value

                End Set
            End Property

            Public Property ThreadSize() As Long
                Get
                    Return Me._ThreadSize

                End Get
                Set(ByVal value As Long)
                    Me._ThreadSize = value

                End Set
            End Property

            Public Property ThreadIdleTimeout() As TimeSpan
                Get
                    Return Me._ThreadIDleTimeout

                End Get
                Set(ByVal value As TimeSpan)
                    Me._ThreadIDleTimeout = value

                End Set
            End Property

#End Region

#Region "Constants"

            Public Const B As Long = 1L
            Public Const KB As Long = 1024L * B
            Public Const MB As Long = 1024L * KB

#End Region

        End Class

        Private Class WorkerThreadInfo

#Region "Variables"

            Private _ThreadPool As ThreadPool
            Private _WorkerThread As Thread
            Private _WorkItem As WorkItem
            Private _Gate As AutoResetEvent

            Private _IsExecuteThreadStarted As Boolean
            Private _IsDisposed As Boolean

#End Region

#Region "Properties"

            Public ReadOnly Property ThreadPool() As ThreadPool
                Get
                    Return Me._ThreadPool

                End Get
            End Property

            Private Property WorkerThread() As Thread
                Get
                    Return Me._WorkerThread

                End Get
                Set(ByVal value As Thread)
                    Me._WorkerThread = value

                End Set
            End Property

            Private Property WorkItem() As WorkItem
                Get
                    Return Me._WorkItem

                End Get
                Set(ByVal value As WorkItem)
                    Me._WorkItem = value

                End Set
            End Property

            Private ReadOnly Property Gate() As AutoResetEvent
                Get
                    Return Me._Gate

                End Get
            End Property

            Private Property IsExecuteThreadStarted() As Boolean
                Get
                    Return Me._IsExecuteThreadStarted

                End Get
                Set(ByVal value As Boolean)
                    Me._IsExecuteThreadStarted = value

                End Set
            End Property

            Private Property IsDisposed() As Boolean
                Get
                    Return Me._IsDisposed

                End Get
                Set(ByVal value As Boolean)
                    Me._IsDisposed = value

                End Set
            End Property

#End Region

            Public Sub New(ByVal threadPool As ThreadPool)
                MyBase.New()

                Me._Gate = New AutoResetEvent(False)

                Me._ThreadPool = threadPool

                Me.ThreadPool.TotalThreadCountPerformanceCounter.Increment()

                'THREAD: ThreadManager
                'Me.WorkerThread = New Thread(New ThreadStart(AddressOf Execute), Me.ThreadPool.ThreadSize)
                'Me.WorkerThread.Name = "ThreadPool(" & Me.ThreadPool.Name & ") thread"

                Me.WorkerThread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("ThreadPool(" & Me.ThreadPool.Name & ") thread", AddressOf Execute, Me.ThreadPool.ThreadSize)

                Me.WorkerThread.IsBackground = True
                'Me.WorkerThread.Priority = ThreadPriority.AboveNormal
                Me.WorkerThread.Start()

                Using New Lock(Me.ThreadPool.WorkerThreadLock, True)
                    Me.ThreadPool.WorkerThreads.Add(Me)

                End Using

            End Sub

            Private Sub Execute()
                While Not Me.IsDisposed
                    If Me.Gate.WaitOne(Me.ThreadPool.ThreadIdleTimeout, False) Then
                        If Not Me.IsDisposed Then
                            If Not Me.WorkItem Is Nothing Then
                                Me.ThreadPool.OnWorkItemStarted(New EventArgs.WorkItemEventArgs(Me.WorkItem))

                                Me.WorkItem.Execute()

                                Me.WorkItem = Nothing

                                Me.ThreadPool.ActiveThreadCountPerformanceCounter.Decrement()

                                Using New Lock(Me.ThreadPool.WorkerThreadLock, True)
                                    Me.ThreadPool.AvailableWorkerThreads.Push(Me)

                                End Using

                                Me.ThreadPool.WriteLog("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Opening FlushGate in WorkerThreadInfo.Execute()")
                                Me.ThreadPool.FlushLog()

                                Me.ThreadPool.FlushGate.Set()

                                'UNDONE: Reuse WorkerThread when Workitem disposed
                                'Else
                                '    Me.ThreadPool.ActiveThreadCountPerformanceCounter.Decrement()

                                '    Using New Lock(Me.ThreadPool.WorkerThreadLock, True)
                                '        Me.ThreadPool.AvailableWorkerThreads.Push(Me)

                                '    End Using

                                '    Me.ThreadPool.WriteLog("[" & Date.Now.ToString("HH:mm:ss.FFF", System.Globalization.CultureInfo.InvariantCulture) & "] " & "Opening FlushGate in WorkerThreadInfo.Execute() - WorkItem was Nothing")
                                '    Me.ThreadPool.FlushLog()

                                '    Me.ThreadPool.FlushGate.Set()

                                '    End If

                                'Else
                                '    If Not Me.WorkItem Is Nothing Then
                                '        Me.ThreadPool.DequeueWorkItem(Me.WorkItem)
                            End If
                        End If

                    Else
                        'There was no WorkItem to process and the idle timeout has been reached
                        If Me.Dispose() Then
                            Exit While

                        End If
                    End If

                End While
            End Sub

            Public Sub ProcessWorkItem(ByVal workItem As WorkItem)
                Me.WorkItem = workItem

                If Not Me.WorkItem Is Nothing Then
                    Me.ThreadPool.RemoveAvailableWorkerThread(Me)

                    'Using New Lock(Me.ThreadPool.ActiveItemsLock, True)
                    Me.ThreadPool.WorkItemsActive.Add(Me.WorkItem)

                    'End Using

                    Me.ThreadPool.ActiveThreadCountPerformanceCounter.Increment()

                End If

                Me.Gate.Set()

            End Sub

            Public Shared Function Create(ByVal threadPool As ThreadPool) As WorkerThreadInfo
                Return New WorkerThreadInfo(threadPool)

            End Function

            Public Function Dispose() As Boolean
                If Not Me.IsDisposed Then
                    If Me.ThreadPool.DisposeWorkerThread(Me) Then
                        Me.IsDisposed = True
                        Me.Gate.Set()

                        Me._ThreadPool = Nothing
                        Me._WorkerThread = Nothing
                        Me._WorkItem = Nothing

                        Me.Gate.Close()
                        Me._Gate = Nothing

                        Return True

                    Else
                        'Reuse thread since threadpool has MinThreads in pool

                        Return False

                    End If
                End If

            End Function

        End Class

    End Class

End Namespace

#End Region