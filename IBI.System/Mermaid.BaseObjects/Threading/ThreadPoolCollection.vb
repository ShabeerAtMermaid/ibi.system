Imports mermaid.BaseObjects.Threading

Namespace Threading

    Public Class ThreadPoolCollection
        Inherits System.Collections.Generic.Dictionary(Of String, ThreadPool)

    End Class

End Namespace