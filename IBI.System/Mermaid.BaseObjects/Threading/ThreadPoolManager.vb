Imports System.Threading

Namespace Threading

    Public Class ThreadPoolManager

#Region "Variables"

        Private Shared _ThreadPools As ThreadPoolCollection

        Private Shared _ThreadPoolPerformanceLog As New Diagnostics.PerformanceLog("mermaid BaseObjects\Threading\ThreadPools")
        Private Shared _Lock As New Object

#End Region

#Region "Properties"

        Public Shared ReadOnly Property ThreadPools() As ThreadPoolCollection
            Get
                If _ThreadPools Is Nothing Then
                    _ThreadPools = New ThreadPoolCollection()

                End If

                Return _ThreadPools

            End Get
        End Property

        Public Shared ReadOnly Property ThreadCountPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Threads created")

            End Get
        End Property

        Public Shared ReadOnly Property ThreadPoolPerformanceLog() As Diagnostics.PerformanceLog
            Get
                Return _ThreadPoolPerformanceLog

            End Get
        End Property

        Private Shared ReadOnly Property Lock() As Object
            Get
                Return _Lock

            End Get
        End Property

#End Region

        Private Shared Sub AddThreadPool(ByVal threadPool As ThreadPool)
            If Not threadPool Is Nothing Then
                If Not ThreadPools.ContainsKey(threadPool.Name) Then
                    ThreadPools(threadPool.Name) = threadPool

                ElseIf Not ThreadPools(threadPool.Name) Is threadPool Then
                    Throw New ConstraintException("A ThreadPool with name """ & threadPool.Name & """ already exists.")

                End If

            Else
                Throw New NullReferenceException("Parameter ThreadPool cannot be null.")

            End If

        End Sub

        Public Shared Function GetThreadPool(ByVal name As String) As ThreadPool
            Return GetThreadPool(name, New ThreadPool.ThreadPoolInfo)

        End Function

        Public Shared Function GetThreadPool(ByVal name As String, ByVal defaultSettings As ThreadPool.ThreadPoolInfo) As ThreadPool
            Monitor.Enter(Lock)

            Try
                If ThreadPools.ContainsKey(name) Then
                    Return ThreadPools(name)

                Else
                    Dim tmpThreadPool As New ThreadPool(name)
                    tmpThreadPool.MinThreads = defaultSettings.MinThreads
                    tmpThreadPool.MaxThreads = defaultSettings.MaxThreads
                    tmpThreadPool.MaxQueue = defaultSettings.MaxQueue
                    tmpThreadPool.QueueWarningLimit = defaultSettings.QueueWarningLimit
                    tmpThreadPool.ThreadSize = defaultSettings.ThreadSize
                    tmpThreadPool.ThreadIDleTimeout = defaultSettings.ThreadIDleTimeout

                    AddThreadPool(tmpThreadPool)

                    Return tmpThreadPool

                End If

                Return Nothing

            Finally
                Monitor.Exit(Lock)

            End Try
        End Function

    End Class

End Namespace