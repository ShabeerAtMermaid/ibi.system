﻿'Imports System.Threading

'Namespace Threading

'    Public Class WorkClass

'#Region "Variables"

'        Private _WorkTarget As WorkMethodHandler
'        Private _WorkResultTarget As WorkResultHandler

'#End Region

'#Region "Properties"

'        Public Property WorkTarget() As WorkMethodHandler
'            Get
'                Return Me._WorkTarget

'            End Get
'            Set(ByVal value As WorkMethodHandler)
'                Me._WorkTarget = value

'            End Set
'        End Property

'        Public Property WorkResultTarget() As WorkResultHandler
'            Get
'                Return Me._WorkResultTarget

'            End Get
'            Set(ByVal value As WorkResultHandler)
'                Me._WorkResultTarget = value

'            End Set
'        End Property

'#End Region

'#Region "Delegates"

'        Public Delegate Sub WorkMethodHandler()
'        Public Delegate Sub WorkResultHandler(ByVal result As WorkResult)

'#End Region

'        Public Sub New(ByVal target As WorkMethodHandler, ByVal resultTarget As WorkResultHandler)
'            MyBase.New()

'            Me.WorkTarget = target
'            Me.WorkResultTarget = resultTarget

'        End Sub

'        Public Sub BeginWork()
'            'THREAD: ThreadManager
'            'Dim tmpThread As New Thread(AddressOf BeginWork_Callback)
'            'tmpThread.Name = "WorkClass WorkThread"

'            Dim tmpThread As Thread = mermaid.BaseObjects.Threading.ThreadManager.CreateThread("ThreadPool WorkClass(" & Me.WorkTarget. & ") thread", AddressOf BeginWork_Callback)
'            tmpThread.IsBackground = True
'            tmpThread.Start()

'        End Sub

'        Private Sub BeginWork_Callback()
'            Dim tmpResult As WorkResult = Nothing

'            Try
'                Me.WorkTarget.Invoke()

'                tmpResult = New WorkResult(WorkResult.WorkResults.Succeeded, Nothing)

'            Catch ex As Exception
'                tmpResult = New WorkResult(WorkResult.WorkResults.Failed, ex)

'            End Try

'            Me.WorkResultTarget.Invoke(tmpResult)

'        End Sub

'    End Class

'End Namespace
