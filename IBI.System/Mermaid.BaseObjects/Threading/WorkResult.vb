﻿Namespace Threading

    Public Class WorkResult

#Region "Variables"

        Private _Result As WorkResults
        Private _Exception As Exception

#End Region

#Region "Properties"

        Public ReadOnly Property Result() As WorkResults
            Get
                Return Me._Result

            End Get
        End Property

        Public ReadOnly Property Exception() As Exception
            Get
                Return Me._Exception

            End Get
        End Property

#End Region

#Region "Enums"

        Public Enum WorkResults As Integer
            Unknown = 0
            Failed = 1
            Succeeded = 2
            'Cancelled = 3

        End Enum

#End Region

        Public Sub New(ByVal result As WorkResults, ByVal exception As Exception)
            MyBase.New()

            Me._Result = result
            Me._Exception = exception

        End Sub

    End Class


End Namespace