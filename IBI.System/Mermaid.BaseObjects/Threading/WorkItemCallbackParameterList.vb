Namespace Threading

    Public Class WorkItemCallbackParameterList
        Inherits System.Collections.Generic.Dictionary(Of String, Object)

        Default Public Shadows Property Item(ByVal key As String) As Object
            Get
                If Me.ContainsKey(key) Then
                    Return MyBase.Item(key)

                End If

                Return Nothing

            End Get
            Set(ByVal value As Object)
                MyBase.Item(key) = value

            End Set
        End Property

    End Class

End Namespace