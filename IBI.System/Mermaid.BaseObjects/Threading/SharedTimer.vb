Namespace Threading

    Public Class SharedTimer
        Implements IDisposable

#Region "Variables"

        Private WithEvents _InternalTimer As Timer
        Private _Callbacks As Generic.List(Of SharedTimerCallback)
        Private _IsAsync As Boolean = True

        Private _TotalCallbacksAttachedPerformanceCounter As Diagnostics.PerformanceCounter
        Private _CallbacksAttachedPerformanceCounter As Diagnostics.PerformanceCounter
        Private _CallbacksCalledPerformanceCounter As Diagnostics.PerformanceCounter
        Private _CallbacksFailedPerformanceCounter As Diagnostics.PerformanceCounter

        Private _IsPerformanceCountersInitialized As Boolean
        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public Property Enabled() As Boolean
            Get
                Return Me.InternalTimer.Enabled

            End Get
            Set(ByVal value As Boolean)
                Me.InternalTimer.Enabled = value

            End Set
        End Property

        Public Property Interval() As TimeSpan
            Get
                Return Me._InternalTimer.Interval

            End Get
            Set(ByVal value As TimeSpan)
                Me.InternalTimer.Interval = value

            End Set
        End Property

        Public ReadOnly Property RegisteredCallbacks() As Integer
            Get
                Return Me.Callbacks.Count

            End Get
        End Property

        Private Property InternalTimer() As Timer
            Get
                Return Me._InternalTimer

            End Get
            Set(ByVal value As Timer)
                Me._InternalTimer = value

            End Set
        End Property

        Private Property Callbacks() As Generic.List(Of SharedTimerCallback)
            Get
                Return Me._Callbacks

            End Get
            Set(ByVal value As Generic.List(Of SharedTimerCallback))
                Me._Callbacks = value

            End Set
        End Property

        Public Property IsAsync() As Boolean
            Get
                Return Me._IsAsync

            End Get
            Set(ByVal value As Boolean)
                Me._IsAsync = value

            End Set
        End Property

        Private Property TotalCallbacksAttachedPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Me._TotalCallbacksAttachedPerformanceCounter

            End Get
            Set(ByVal value As Diagnostics.PerformanceCounter)
                Me._TotalCallbacksAttachedPerformanceCounter = value

            End Set
        End Property

        Private Property CallbacksAttachedPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Me._CallbacksAttachedPerformanceCounter

            End Get
            Set(ByVal value As Diagnostics.PerformanceCounter)
                Me._CallbacksAttachedPerformanceCounter = value

            End Set
        End Property

        Private Property CallbacksCalledPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Me._CallbacksCalledPerformanceCounter

            End Get
            Set(ByVal value As Diagnostics.PerformanceCounter)
                Me._CallbacksCalledPerformanceCounter = value

            End Set
        End Property

        Private Property CallbacksFailedPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Me._CallbacksFailedPerformanceCounter

            End Get
            Set(ByVal value As Diagnostics.PerformanceCounter)
                Me._CallbacksFailedPerformanceCounter = value

            End Set
        End Property

        Private Property IsPerformanceCountersInitialized() As Boolean
            Get
                Return Me._IsPerformanceCountersInitialized

            End Get
            Set(ByVal value As Boolean)
                Me._IsPerformanceCountersInitialized = value

            End Set
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        Public Sub New()
            Me.New(TimeSpan.FromSeconds(10))

        End Sub

        Public Sub New(ByVal interval As TimeSpan)
            MyBase.New()

            Me.InternalTimer = New Timer
            Me.Interval = interval
            Me.Callbacks = New Generic.List(Of SharedTimerCallback)

        End Sub

        Public Sub InitializePerformanceCounters(ByVal timerName As String)
            If Not Me.IsPerformanceCountersInitialized Then
                Me.TotalCallbacksAttachedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("SharedTimer """ & timerName & """ total callbacks attached")
                Me.CallbacksAttachedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("SharedTimer """ & timerName & """ callbacks attached")
                Me.CallbacksCalledPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("SharedTimer """ & timerName & """ callbacks called")
                Me.CallbacksFailedPerformanceCounter = BaseObjects.Diagnostics.PerformanceCounterManager.GetPerformanceCounter("SharedTimer """ & timerName & """ callbacks failed")

                Me.IsPerformanceCountersInitialized = True

            End If

        End Sub

        Public Sub AttachCallback(ByVal callback As SharedTimerCallback)
            Me.Callbacks.Add(callback)

            If Not Me.TotalCallbacksAttachedPerformanceCounter Is Nothing Then Me.TotalCallbacksAttachedPerformanceCounter.Increment()
            If Not Me.CallbacksAttachedPerformanceCounter Is Nothing Then Me.CallbacksAttachedPerformanceCounter.Increment()

        End Sub

        Public Sub DetachCallback(ByVal callback As SharedTimerCallback)
            Me.Callbacks.Remove(callback)

            If Not Me.CallbacksAttachedPerformanceCounter Is Nothing Then Me.CallbacksAttachedPerformanceCounter.Decrement()

        End Sub

        Private Sub InternalTimer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InternalTimer.Elapsed
            If Not Me.IsDisposed Then
                Dim tmpInvokeList As New ArrayList
                tmpInvokeList.AddRange(Me.Callbacks)

                For Each tmpCallback As SharedTimerCallback In tmpInvokeList
                    If Me.IsAsync Then
                        tmpCallback.BeginInvoke(sender, e, AddressOf Callback_DelegateCallback, tmpCallback)

                        If Not Me.CallbacksCalledPerformanceCounter Is Nothing Then Me.CallbacksCalledPerformanceCounter.Increment()

                    Else
                        Try
                            tmpCallback.Invoke(sender, e)

                            If Not Me.CallbacksCalledPerformanceCounter Is Nothing Then Me.CallbacksCalledPerformanceCounter.Increment()

                        Catch ex As ObjectDisposedException
                            Me.DetachCallback(tmpCallback)

                        Catch ex As Exception
                            If Not Me.CallbacksFailedPerformanceCounter Is Nothing Then Me.CallbacksFailedPerformanceCounter.Increment()

                            BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Threading))

                        End Try
                    End If

                Next

            End If

        End Sub

        Private Sub Callback_DelegateCallback(ByVal ar As IAsyncResult)
            Dim tmpCallback As SharedTimerCallback = ar.AsyncState

            Try
                tmpCallback.EndInvoke(ar)

                If Not Me.CallbacksCalledPerformanceCounter Is Nothing Then Me.CallbacksCalledPerformanceCounter.Decrement()

            Catch ex As ObjectDisposedException
                Me.DetachCallback(tmpCallback)

            Catch ex As Exception
                If Not Me.CallbacksFailedPerformanceCounter Is Nothing Then Me.CallbacksFailedPerformanceCounter.Increment()

                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Threading))

            End Try
        End Sub

        Public Sub Start()
            Me.InternalTimer.Start()

        End Sub

        Public Sub Restart()
            Me.InternalTimer.Restart()

        End Sub

        Public Sub [Stop]()
            Me.InternalTimer.Stop()

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    ' free managed resources when explicitly called
                    Me.Enabled = False
                    Me.InternalTimer.Dispose()
                    Me.InternalTimer = Nothing

                    Me.Callbacks.Clear()
                    Me.Callbacks = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

    End Class

End Namespace