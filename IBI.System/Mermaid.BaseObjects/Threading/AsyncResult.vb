Imports System.Threading

Namespace Threading

    Public Class AsyncResult
        Implements IAsyncResult, IDisposable

#Region "Variables"

        Private _AsyncState As Object
        Private _AsyncWaitHandle As ManualResetEvent
        Private _Callback As System.AsyncCallback

        Private _WorkCallback As AsyncCallback
        Private _EndCallback As AsyncCallback

        Private _Result As Object

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property AsyncState() As Object Implements System.IAsyncResult.AsyncState
            Get
                Return Me._AsyncState

            End Get
        End Property

        Friend ReadOnly Property AsyncWaitHandle() As System.Threading.WaitHandle Implements System.IAsyncResult.AsyncWaitHandle
            Get
                Return Me._AsyncWaitHandle

            End Get
        End Property

        Private Property Callback() As System.AsyncCallback
            Get
                Return Me._Callback

            End Get
            Set(ByVal value As System.AsyncCallback)
                Me._Callback = value

            End Set
        End Property

        Friend Property WorkCallback() As AsyncCallback
            Get
                Return Me._WorkCallback

            End Get
            Set(ByVal value As AsyncCallback)
                Me._WorkCallback = value

            End Set
        End Property

        Friend Property EndCallback() As AsyncCallback
            Get
                Return Me._EndCallback

            End Get
            Set(ByVal value As AsyncCallback)
                Me._EndCallback = value

            End Set
        End Property

        Public ReadOnly Property CompletedSynchronously() As Boolean Implements System.IAsyncResult.CompletedSynchronously
            Get
                Return False

            End Get
        End Property

        Public ReadOnly Property IsCompleted() As Boolean Implements System.IAsyncResult.IsCompleted
            Get
                Return Me.AsyncWaitHandle.WaitOne(0, False)

            End Get
        End Property

        Public ReadOnly Property Result() As Object
            Get
                Return Me._Result

            End Get
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal callback As System.AsyncCallback, ByVal state As Object)
            MyBase.New()

            Me.Callback = callback
            Me._AsyncState = state

            Me._AsyncWaitHandle = New ManualResetEvent(False)

        End Sub

        Friend Sub Complete()
            Me.Complete(Nothing)

        End Sub

        Friend Sub Complete(ByVal result As Object)
            Me._Result = result

            Me._AsyncWaitHandle.Set()

            If Not Callback Is Nothing Then
                Callback.Invoke(Me)

            End If

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.AsyncWaitHandle.Close()
                    Me._AsyncWaitHandle = Nothing
                    Me._AsyncState = Nothing
                    Me.Callback = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

    End Class

End Namespace