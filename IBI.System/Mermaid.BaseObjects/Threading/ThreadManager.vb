﻿Imports mermaid.BaseObjects.Diagnostics
Imports System.Threading

Namespace Threading

    Public Class ThreadManager

#Region "Variables"

        Private Shared _Lock As New Object
        Private Shared _Initialized As Boolean

        Private Shared _TmrFlush As Timer

#End Region

#Region "Properties"

        Private Shared Property ActiveThreads As List(Of Thread) = New List(Of Thread)
        Private Shared Property ActiveThreadsPerformanceCounter As PerformanceCounter = PerformanceCounterManager.GetPerformanceCounter("ThreadManager: Active Threads")

#End Region

        Public Shared Function CreateThread(ByVal name As String, ByVal start As ThreadStart) As Thread
            Using New Lock(_Lock, True)
                If Not _Initialized Then Initialize()

                Dim tmpThread As New Thread(start)
                tmpThread.Name = name

                ActiveThreads.Add(tmpThread)

                ActiveThreadsPerformanceCounter.Increment()
                PerformanceCounterManager.GetPerformanceCounter("ThreadManager thread count: " & tmpThread.Name).Increment()

                Return tmpThread

            End Using

        End Function

        Public Shared Function CreateThread(ByVal name As String, ByVal start As ThreadStart, ByVal maxStackSize As Long) As Thread
            Using New Lock(_Lock, True)
                If Not _Initialized Then Initialize()

                Dim tmpThread As New Thread(start, maxStackSize)
                tmpThread.Name = name

                ActiveThreads.Add(tmpThread)

                ActiveThreadsPerformanceCounter.Increment()
                PerformanceCounterManager.GetPerformanceCounter("ThreadManager thread count: " & tmpThread.Name).Increment()

                Return tmpThread

            End Using

        End Function

        Private Shared Sub Initialize()
            If Not _Initialized Then
                _TmrFlush = New Timer(TimeSpan.FromSeconds(10))

                AddHandler _TmrFlush.Elapsed, AddressOf TmrFlush_Elapsed

                _TmrFlush.Start()

                _Initialized = True

            End If

        End Sub

        Private Shared Sub TmrFlush_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs)
            Using New Lock(_Lock, True)
                Dim tmpDisposeList As New List(Of Thread)

                For Each tmpThread As Thread In ActiveThreads
                    If Not tmpThread.IsAlive Then
                        tmpDisposeList.Add(tmpThread)
                    End If

                Next

                For Each tmpThread As Thread In tmpDisposeList
                    ActiveThreads.Remove(tmpThread)

                    ActiveThreadsPerformanceCounter.Decrement()
                    PerformanceCounterManager.GetPerformanceCounter("ThreadManager thread count: " & tmpThread.Name).Decrement()
                Next

            End Using
        End Sub

    End Class

End Namespace