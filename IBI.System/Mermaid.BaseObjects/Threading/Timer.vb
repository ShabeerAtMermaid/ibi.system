#Region "Handle Implementation"

'Imports System.Threading

'Namespace Threading

'    Public Class Timer

'#Region "Variables"

'        Private _Enabled As Boolean
'        Private _Interval As TimeSpan
'        Private _UseElapseThreads As Boolean = True

'        Private _InternalThread As Thread
'        Private _InternalThreadGate As AutoResetEvent

'        Private _IsDisposed As Boolean

'#End Region

'#Region "Properties"

'        Public Property Enabled() As Boolean
'            Get
'                Return Me._Enabled

'            End Get
'            Set(ByVal value As Boolean)
'                If IsDirty(Me._Enabled, value) Then
'                    Me._Enabled = value

'                    If Me._Enabled Then
'                        Me.StartInternalThread()

'                    Else
'                        Me.StopInternalThread()

'                    End If
'                End If

'            End Set
'        End Property

'        Public Property Interval() As TimeSpan
'            Get
'                Return Me._Interval

'            End Get
'            Set(ByVal value As TimeSpan)
'                'If IsDirty(Me._Interval, value) Then
'                Me._Interval = value

'                If Me.Enabled Then
'                    Me.Restart()

'                End If
'                'End If

'            End Set
'        End Property

'        Public Property UseElapseThreads() As Boolean
'            Get
'                Return Me._UseElapseThreads

'            End Get
'            Set(ByVal value As Boolean)
'                Me._UseElapseThreads = value

'            End Set
'        End Property

'        Private Property InternalThread() As Thread
'            Get
'                Return Me._InternalThread

'            End Get
'            Set(ByVal value As Thread)
'                Me._InternalThread = value

'            End Set
'        End Property

'        Private Property InternalThreadGate() As AutoResetEvent
'            Get
'                Return Me._InternalThreadGate

'            End Get
'            Set(ByVal value As AutoResetEvent)
'                Me._InternalThreadGate = value

'            End Set
'        End Property

'        Private Property IsDisposed() As Boolean
'            Get
'                Return Me._IsDisposed

'            End Get
'            Set(ByVal value As Boolean)
'                Me._IsDisposed = value

'            End Set
'        End Property

'#End Region

'#Region "Events"

'        Public Event Elapsed As EventHandler

'#End Region

'        Public Sub New()
'            Me.New(TimeSpan.FromSeconds(1))

'        End Sub

'        Public Sub New(ByVal interval As TimeSpan)
'            MyBase.New()

'            Me.Interval = interval
'            Me.InternalThreadGate = New AutoResetEvent(False)

'        End Sub

'        Private Sub StartInternalThread()
'            Me.InternalThread = New Thread(New ThreadStart(AddressOf ProcessInternalThread), 128 * 1024L)
'            Me.InternalThread.Name = "BaseObjects.Threading.Timer Process thread"
'            Me.InternalThread.Start()

'        End Sub

'        Private Sub StopInternalThread()
'            Me.InternalThreadGate.Set()

'        End Sub

'        Private Sub ProcessInternalThread()
'            While Not Me.IsDisposed And Me.Enabled
'                If Not Me.InternalThreadGate.WaitOne(Me.Interval, False) Then
'                    If Me.Enabled Then
'                        Me.OnElapsed()

'                    End If
'                End If

'            End While

'        End Sub

'        Public Sub Start()
'            Me.Enabled = True

'        End Sub

'        Public Sub Restart()
'            Me.Enabled = False
'            Me.Enabled = True

'        End Sub

'        Public Sub [Stop]()
'            Me.Enabled = False

'        End Sub

'        Private Sub OnElapsed()
'            If Me.UseElapseThreads Then
'                'Dim tmpThread As New Thread(New ThreadStart(AddressOf OnElapsed_Callback), 512 * 1024L)
'                Dim tmpThread As New Thread(AddressOf OnElapsed_Callback)
'                tmpThread.Name = "BaseObjects.Threading.Timer Elapsed thread"
'                tmpThread.Start()

'            Else
'                Me.OnElapsed_Callback()

'            End If

'        End Sub

'        Private Sub OnElapsed_Callback()
'            Try
'                RaiseEvent Elapsed(Me, New System.EventArgs)

'            Catch ex As Exception
'                'UNDONE: Log

'            End Try
'        End Sub

'        Public Sub Dispose()
'            If Not Me.IsDisposed Then
'                Me.IsDisposed = True

'                Me.Enabled = False
'                Me.InternalThread = Nothing
'                Me.InternalThreadGate.Close()
'                Me.InternalThreadGate = Nothing

'            End If

'        End Sub

'    End Class

'End Namespace

#End Region

#Region "Timers.Timer Implementation"

Imports System.Threading

Namespace Threading

    Public Class Timer
        Inherits Timers.Timer

#Region "Variables"

#End Region

#Region "Properties"

        Public Overloads Property Interval() As TimeSpan
            Get
                Return TimeSpan.FromMilliseconds(MyBase.Interval)

            End Get
            Set(ByVal value As TimeSpan)
                If IsDirty(MyBase.Interval, value.TotalMilliseconds) Then
                    Dim tmpWasEnabled As Boolean = Me.Enabled

                    Me.Enabled = False

                    MyBase.Interval = value.TotalMilliseconds

                    If tmpWasEnabled Then
                        Me.Enabled = True

                    End If
                End If

            End Set
        End Property

#End Region

#Region "Events"

        Public Shadows Event Elapsed As System.EventHandler

#End Region

        Public Sub New()
            Me.New(TimeSpan.FromSeconds(1))

        End Sub

        Public Sub New(ByVal interval As TimeSpan)
            MyBase.New()

            Me.Interval = interval
           
        End Sub

        Public Overloads Sub Start()
            Me.Enabled = True

        End Sub

        Public Sub Restart()
            Me.Enabled = False
            Me.Enabled = True

        End Sub

        Public Overloads Sub [Stop]()
            Me.Enabled = False

        End Sub

        Private Sub Timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles MyBase.Elapsed
            RaiseEvent Elapsed(Me, New System.EventArgs)

        End Sub

    End Class

End Namespace

#End Region