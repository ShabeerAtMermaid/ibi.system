Namespace Threading

    Public Delegate Function WorkItemCallback(ByVal parameters As WorkItemCallbackParameterList) As Object
    Public Delegate Function ParameterLessWorkItemCallback() As Object

End Namespace