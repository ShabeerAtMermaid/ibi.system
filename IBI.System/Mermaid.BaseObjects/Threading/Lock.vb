Imports System.Threading

Namespace Threading

    Public Class Lock
        Implements IDisposable

#Region "Variables"

        Private Shared _TrackLocks As Boolean
        Private Shared _ActiveLocks As New ArrayList

        Private _LockObject As Object
        Private _IsLocked As Boolean
        Private _StackTrace As String = String.Empty

        Private _IsDisposed As Boolean

#End Region

#Region "Properties"

        Public Shared Property TrackLocks() As Boolean
            Get
                Return _TrackLocks

            End Get
            Set(ByVal value As Boolean)
                _TrackLocks = value

            End Set
        End Property

        Public Shared ReadOnly Property ActiveLocks() As ArrayList
            Get
                Return _ActiveLocks

            End Get
        End Property

        Private Property LockObject() As Object
            Get
                Return Me._LockObject

            End Get
            Set(ByVal value As Object)
                Me._LockObject = value

            End Set
        End Property

        Public ReadOnly Property IsLocked() As Boolean
            Get
                Return Me._IsLocked

            End Get
        End Property

        Public ReadOnly Property StackTrace() As String
            Get
                Return Me._StackTrace

            End Get
        End Property

        Private Property IsDisposed() As Boolean
            Get
                Return Me._IsDisposed

            End Get
            Set(ByVal value As Boolean)
                Me._IsDisposed = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal lockObject As Object, ByVal locked As Boolean)
            MyBase.New()

            If lockObject Is Nothing Then Throw New ArgumentNullException("lockObject")

            Monitor.Enter(ActiveLocks)

            Try
                ActiveLocks.Add(Me)

            Finally
                Monitor.Exit(ActiveLocks)

            End Try

            Me.LockObject = lockObject

            If TrackLocks Then Me._StackTrace &= "Constructing StackTrace:" & vbCrLf & System.Environment.StackTrace & vbCrLf

            If locked Then
                Me.Lock()

            End If

        End Sub

        Public Sub Lock()
            If Not Me.IsLocked Then
                Monitor.Enter(Me.LockObject)
                Me._IsLocked = True

                If TrackLocks Then Me._StackTrace &= vbCrLf & "Locking StackTrace:" & vbCrLf & System.Environment.StackTrace & vbCrLf

            End If

        End Sub

        Public Sub Unlock()
            If Me.IsLocked Then
                Monitor.Exit(Me.LockObject)
                Me._IsLocked = False

            End If

        End Sub

        Protected Overridable Sub Dispose(ByVal disposing As Boolean)
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.Unlock()

                    Monitor.Enter(ActiveLocks)

                    Try
                        ActiveLocks.Remove(Me)

                    Finally
                        Monitor.Exit(ActiveLocks)

                    End Try

                    Me._StackTrace = Nothing
                    Me.LockObject = Nothing

                End If

                ' free shared unmanaged resources
            End If

            Me.IsDisposed = True

        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)

        End Sub

    End Class

End Namespace