Imports System.Threading

Namespace Threading

    Public Class AsyncHelper

        Public Shared Function BeginWork(ByVal workCallback As AsyncCallback, ByVal endCallback As AsyncCallback, ByVal state As Object) As AsyncResult
            If workCallback Is Nothing Then
                Throw New ArgumentNullException("workCallback")

            End If

            Dim tmpAsyncResult As New AsyncResult(AddressOf AsyncResult_Complete, state)
            tmpAsyncResult.WorkCallback = workCallback
            tmpAsyncResult.EndCallback = endCallback

            System.Threading.ThreadPool.QueueUserWorkItem(New WaitCallback(AddressOf Threadpool_StartItem), tmpAsyncResult)

            Return tmpAsyncResult

        End Function

        Public Shared Function EndWork(ByVal ar As IAsyncResult) As Object
            Dim tmpResult As Object = Nothing

            If Not ar Is Nothing Then
                Dim tmpAsyncResult As AsyncResult = ar

                Try
                    tmpAsyncResult.AsyncWaitHandle.WaitOne()

                    tmpResult = tmpAsyncResult.Result

                Finally
                    tmpAsyncResult.Dispose()

                End Try

            End If

            Return tmpResult

        End Function

        Private Shared Sub Threadpool_StartItem(ByVal state As Object)
            Dim tmpAsyncResult As AsyncResult = state

            Try
                tmpAsyncResult.Complete(tmpAsyncResult.WorkCallback.Invoke(tmpAsyncResult))

            Catch ex As Exception
                tmpAsyncResult.Complete(ex)

            End Try
        End Sub

        Private Shared Sub AsyncResult_Complete(ByVal ar As IAsyncResult)
            Dim tmpAsyncResult As AsyncResult = ar

            If Not tmpAsyncResult.EndCallback Is Nothing Then
                tmpAsyncResult.EndCallback.Invoke(tmpAsyncResult)

            End If

        End Sub

    End Class

End Namespace