'Imports System.Globalization
'Imports mermaid.BaseObjects.Persistence.Collections
'Imports mermaid.BaseObjects.Persistence

'Namespace LanguageControlling

'    Public Class LanguageResources

'#Region "Attributes"

'        Private Shared languageControllerValue As LanguageController

'#End Region

'#Region "Properties"

'        Public Shared Property ActiveLanguage() As CultureInfo
'            Get
'                Return LanguageController.ActiveLanguage

'            End Get
'            Set(ByVal Value As CultureInfo)
'                LanguageController.ActiveLanguage = Value

'            End Set
'        End Property

'        Public Shared Property LanguageController() As LanguageController
'            Get
'                Return languageControllerValue

'            End Get
'            Set(ByVal Value As LanguageController)
'                languageControllerValue = Value

'            End Set
'        End Property

'        Public Shared ReadOnly Property AppActiveLanguages() As PersistentList
'            Get
'                Return LanguageController.InnerCollection

'            End Get
'        End Property

'#End Region

'        Friend Shared Sub Instantiate()
'            AddHandler Broker.BrokerInstatiated, AddressOf Broker_BrokerInstantiated

'        End Sub

'        Private Shared Sub Broker_BrokerInstantiated(ByVal sender As Object, ByVal e As System.EventArgs)
'            Dim tmpLanguageControllers As ArrayList = Broker.GetObjectArrayList(GetType(LanguageController), True)

'            For Each tmpLanguageController As LanguageController In tmpLanguageControllers
'                LanguageController = tmpLanguageController

'            Next

'        End Sub

'    End Class

'End Namespace
