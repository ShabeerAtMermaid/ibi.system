'Imports System.Globalization
'Imports mermaid.BaseObjects.Persistence
'Imports mermaid.BaseObjects.Persistence.Interfaces
'Imports mermaid.BaseObjects.Persistence.Collections

'Namespace LanguageControlling

'    <PersistentClass("LanguageControlling")> _
'    Public Class LanguageControlledObject
'        Inherits PersistentHashTableCollection

'#Region "Attributes"

'#End Region

'#Region "Properties"

'        Default Public Overrides Property Item(ByVal key As Object) As Object
'            Get
'                If Not key Is Nothing Then
'                    If TypeOf key Is CultureInfo Then
'                        Return MyBase.Item(CInt(key.LCID))

'                    Else
'                        Return MyBase.Item(CInt(key))

'                    End If
'                End If

'                Return Nothing

'            End Get
'            Set(ByVal Value As Object)
'                If Not key Is Nothing Then
'                    If TypeOf key Is CultureInfo Then
'                        'If IsDirty(MyBase.Item(key.LCID), Value) Then
'                        MyBase.Item(CInt(key.LCID)) = Value
'                        'Me.OnPropertyChanged(Me, New VictorFilm.BaseObjects.EventArgs.PropertyChangedEventArgs(Nothing, Nothing))

'                        'End If

'                    Else
'                        'If IsDirty(MyBase.Item(key), Value) Then
'                        MyBase.Item(CInt(key)) = Value

'                        'Me.OnPropertyChanged(Me, New VictorFilm.BaseObjects.EventArgs.PropertyChangedEventArgs(Nothing, Nothing))

'                        'End If
'                    End If
'                End If

'                If Not Value Is Nothing Then
'                    If Array.IndexOf(Value.GetType.GetInterfaces, GetType(mermaid.BaseObjects.LanguageControlling.Interfaces.ILanguageControlledValue)) <> -1 Then
'                        If TypeOf key Is CultureInfo Then
'                            CType(Value, mermaid.BaseObjects.LanguageControlling.Interfaces.ILanguageControlledValue).Culture = key

'                        Else
'                            CType(Value, mermaid.BaseObjects.LanguageControlling.Interfaces.ILanguageControlledValue).Culture = New CultureInfo(CType(key, Integer))

'                        End If
'                    End If
'                End If

'            End Set
'        End Property

'#End Region

'        Public Sub New(ByVal persistent As Boolean)
'            MyBase.New(persistent)

'            CType(Me.InnerCollection, PersistentHashTable).ForceListChangedEvents = True

'        End Sub

'        Protected Overrides Sub OnInnerCollectionChanged(ByVal sender As Object, ByVal e As EventArgs.HashTableChangedEventArgs)
'            If e.ChangeType = Enums.HashTableChangedTypes.ItemAdded Then
'                If Not e.Value Is Nothing Then
'                    If Array.IndexOf(e.Value.GetType.GetInterfaces, GetType(mermaid.BaseObjects.LanguageControlling.Interfaces.ILanguageControlledValue)) <> -1 Then
'                        CType(e.Value, mermaid.BaseObjects.LanguageControlling.Interfaces.ILanguageControlledValue).Culture = New CultureInfo(CType(e.Key, Integer))

'                    End If
'                End If
'            End If

'            'Me.OnPropertyChanged(Me, New mermaid.BaseObjects.EventArgs.PropertyChangedEventArgs(Nothing, Nothing))

'        End Sub

'        Public Overrides Sub Dispose()
'            MyBase.Dispose()

'            Dim disposeList As New ArrayList

'            Me.LockCollection()

'            For Each tmpObject As Object In Me.Values
'                If Not tmpObject Is Nothing Then
'                    If Array.IndexOf(tmpObject.GetType.GetInterfaces(), GetType(IDisposable)) <> -1 Then
'                        disposeList.Add(tmpObject)

'                    End If
'                End If
'            Next

'            Me.UnlockCollection()

'            For Each tmpObject As IDisposable In disposeList
'                tmpObject.Dispose()

'            Next

'        End Sub

'    End Class

'End Namespace