'Imports System.Globalization
'Imports mermaid.BaseObjects.Persistence
'Imports mermaid.BaseObjects.Persistence.Collections
'Imports mermaid.BaseObjects.Delegates

'Namespace LanguageControlling

'    <PersistentClass("LanguageControlling")> _
'    Public Class LanguageController
'        Inherits PersistentArrayListCollection

'#Region "Attributes"

'        Private activeLanguageValue As CultureInfo

'#End Region

'#Region "Properties"

'        Public Property ActiveLanguage() As CultureInfo
'            Get
'                Return activeLanguageValue

'            End Get
'            Set(ByVal Value As CultureInfo)
'                'If Not IsAppActiveLanguage(Value) Then
'                Me.Add(Value)

'                'End If

'                activeLanguageValue = Value

'            End Set
'        End Property

'#End Region

'#Region "Events"

'        Public Event ActiveLanguageAdded As CultureInfoHandler
'        Public Event ActiveLanguageRemoved As CultureInfoHandler

'#End Region

'        Public Sub New(ByVal persistent As Boolean)
'            MyBase.New(persistent)

'            'Dim tmpCulture As CultureInfo = New CultureInfo("da-DK")

'            'MyBase.Add(tmpCulture)

'            'activeLanguageValue = tmpCulture

'        End Sub

'        Public Shadows Function Add(ByVal language As CultureInfo) As Integer
'            If Not Me.Contains(language) Then
'                Dim index As Integer = MyBase.Add(language)

'                Me.OnActiveLanguageAdded(language)

'                Return index

'            Else
'                Return -1

'            End If

'        End Function

'        Public Shadows Sub Insert(ByVal index As Integer, ByVal language As CultureInfo)
'            If Not Me.Contains(language) Then
'                MyBase.Insert(index, language)

'                Me.OnActiveLanguageAdded(language)

'            End If

'        End Sub

'        Public Shadows Sub Remove(ByVal language As CultureInfo)
'            MyBase.Remove(language)

'            Me.OnActiveLanguageRemoved(language)

'        End Sub

'        'Private Function FindCulture(ByVal cultureInfoID As Integer) As CultureInfo
'        '    Dim count As Integer = 0
'        '    Dim found As Boolean = False
'        '    Dim returnValue As CultureInfo

'        '    Dim tmpArray() As CultureInfo = System.Globalization.CultureInfo.GetCultures(CultureTypes.NeutralCultures)

'        '    While Not found And count < tmpArray.Length
'        '        Dim tmpCulture As CultureInfo = CType(tmpArray(count), CultureInfo)

'        '        If tmpCulture.LCID = cultureInfoID Then
'        '            returnValue = tmpCulture
'        '            found = True

'        '        End If

'        '        count += 1

'        '    End While

'        '    Return returnValue
'        'End Function

'        'Private Function IsAppActiveLanguage(ByVal language As CultureInfo, Optional ByVal index As Integer = 0) As Boolean
'        '    If CType(AppActiveLanguages(index), CultureInfo).LCID = language.LCID Then
'        '        Return True

'        '    ElseIf index < AppActiveLanguages.Count Then
'        '        Return IsAppActiveLanguage(language, index + 1)

'        '    Else
'        '        Return False

'        '    End If

'        'End Function

'        Private Sub OnActiveLanguageAdded(ByVal language As CultureInfo)
'            RaiseEvent ActiveLanguageAdded(Me, New mermaid.BaseObjects.EventArgs.CultureInfoEventArgs(language))

'        End Sub

'        Private Sub OnActiveLanguageRemoved(ByVal language As CultureInfo)
'            RaiseEvent ActiveLanguageRemoved(Me, New mermaid.BaseObjects.EventArgs.CultureInfoEventArgs(language))

'        End Sub

'    End Class

'End Namespace
