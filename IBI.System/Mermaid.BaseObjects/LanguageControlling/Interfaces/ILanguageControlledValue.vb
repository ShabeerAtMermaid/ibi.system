Namespace LanguageControlling.Interfaces

    Public Interface ILanguageControlledValue

#Region "Properties"

        Property Culture() As System.Globalization.CultureInfo

#End Region

    End Interface

End Namespace