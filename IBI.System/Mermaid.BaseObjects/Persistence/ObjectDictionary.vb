﻿Imports mermaid.BaseObjects.Persistence.Interfaces

Namespace Persistence

    Friend Class ObjectDictionary

#Region "Variables"

        Private _Type As Type
        Private _SourceDictionary As Dictionary(Of Integer, ObjectData)
        Private _ObjectDictionary As Dictionary(Of Integer, IPersistentBaseObject)

#End Region

#Region "Properties"

        Public Property Type() As Type
            Get
                Return Me._Type

            End Get
            Set(ByVal value As Type)
                Me._Type = value

            End Set
        End Property

        Public Property SourceDictionary() As Dictionary(Of Integer, ObjectData)
            Get
                Return Me._SourceDictionary

            End Get
            Set(ByVal value As Dictionary(Of Integer, ObjectData))
                Me._SourceDictionary = value

            End Set
        End Property

        Public Property ObjectDictionary() As Dictionary(Of Integer, IPersistentBaseObject)
            Get
                Return Me._ObjectDictionary

            End Get
            Set(ByVal value As Dictionary(Of Integer, IPersistentBaseObject))
                Me._ObjectDictionary = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal type As Type, ByVal source As Dictionary(Of Integer, ObjectData))
            MyBase.New()

            Me._Type = type
            Me._SourceDictionary = source
            Me._ObjectDictionary = New Dictionary(Of Integer, IPersistentBaseObject)

        End Sub

    End Class

End Namespace