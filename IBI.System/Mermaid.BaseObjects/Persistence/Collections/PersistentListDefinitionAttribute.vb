﻿Namespace Persistence.Collections

    <AttributeUsage(AttributeTargets.Property)> _
    Public Class PersistentListDefinitionAttribute
        Inherits Attribute

#Region "Variables"

        Private _ValueType As Type

#End Region

#Region "Properties"

        Public Property ValueType() As Type
            Get
                Return Me._ValueType

            End Get
            Set(ByVal value As Type)
                Me._ValueType = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal valueType As Type)
            MyBase.New()

            Me._ValueType = valueType

        End Sub

    End Class

End Namespace