﻿Namespace Persistence.Collections

    <AttributeUsage(AttributeTargets.Property)> _
    Public Class PersistentDictionaryDefinitionAttribute
        Inherits Attribute

#Region "Variables"

        Private _KeyType As Type
        Private _ValueType As Type

#End Region

#Region "Properties"

        Public Property KeyType() As Type
            Get
                Return Me._KeyType

            End Get
            Set(ByVal value As Type)
                Me._KeyType = value

            End Set
        End Property

        Public Property ValueType() As Type
            Get
                Return Me._ValueType

            End Get
            Set(ByVal value As Type)
                Me._ValueType = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal keyType As Type, ByVal valueType As Type)
            MyBase.New()

            Me._KeyType = keyType
            Me._ValueType = valueType

        End Sub

    End Class

End Namespace