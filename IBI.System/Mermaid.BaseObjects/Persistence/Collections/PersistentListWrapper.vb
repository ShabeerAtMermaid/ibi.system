﻿Imports mermaid.BaseObjects.Persistence.Collections.Interfaces
Imports mermaid.BaseObjects.Persistence.Interfaces
Imports mermaid.BaseObjects.Collections

Namespace Persistence.Collections

    Public Class PersistentListWrapper(Of T)
        Inherits PersistentBaseObject
        Implements IPersistentListWrapper(Of T)

#Region "Variables"

        Private _Collection As New BaseList(Of T)(Me)

#End Region

#Region "Properties"

        <PersistentProperty()> _
        Public Property Collection() As BaseList(Of T) Implements IPersistentListWrapper(Of T).Collection
            Get
                Return Me._Collection

            End Get
            Set(ByVal value As BaseList(Of T))
                Me._Collection = value

            End Set
        End Property

        Public ReadOnly Property SafeCopy() As BaseList(Of T) Implements IPersistentListWrapper(Of T).SafeCopy
            Get
                Return Me.Collection.Clone

            End Get
        End Property

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal persistent As Boolean)
            MyBase.New(persistent)

        End Sub

#Region "List Support"

#Region "Properties"

        Public Property Capacity() As Integer Implements IPersistentListWrapper(Of T).Capacity
            Get
                Return Me.Collection.Capacity

            End Get
            Set(ByVal value As Integer)
                Me.Collection.Capacity = value

            End Set
        End Property

        Public ReadOnly Property Count() As Integer Implements IPersistentListWrapper(Of T).Count, ICollection.Count, ICollection(Of T).Count
            Get
                Return Me.Collection.Count

            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.Generic.ICollection(Of T).IsReadOnly
            Get
                Return False

            End Get
        End Property

        Public ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
            Get
                Return False

            End Get
        End Property

        Default Public Property Item(ByVal index As Integer) As T Implements IPersistentListWrapper(Of T).Item
            Get
                Return Me.Collection.Item(index)

            End Get
            Set(ByVal value As T)
                Me.Collection.Item(index) = value

            End Set
        End Property

        Public ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
            Get
                Return Me

            End Get
        End Property

#End Region

        Public Sub Add(ByVal item As T) Implements IPersistentListWrapper(Of T).Add, ICollection(Of T).Add
            Me.Collection.Add(item)

        End Sub

        Public Sub AddRange(ByVal collection As IEnumerable(Of T)) Implements IPersistentListWrapper(Of T).AddRange
            Me.Collection.AddRange(collection)

        End Sub

        Public Function AsReadOnly() As System.Collections.ObjectModel.ReadOnlyCollection(Of T) Implements IPersistentListWrapper(Of T).AsReadOnly
            Return Me.Collection.AsReadOnly()

        End Function

        Public Function BinarySearch(ByVal index As Integer, ByVal count As Integer, ByVal item As T, ByVal comparer As IComparer(Of T)) As Integer Implements IPersistentListWrapper(Of T).BinarySearch
            Return Me.Collection.BinarySearch(index, count, item, comparer)

        End Function

        Public Function BinarySearch(ByVal item As T) As Integer Implements IPersistentListWrapper(Of T).BinarySearch
            Return Me.Collection.BinarySearch(item)

        End Function

        Public Function BinarySearch(ByVal item As T, ByVal comparer As IComparer(Of T)) As Integer Implements IPersistentListWrapper(Of T).BinarySearch
            Return Me.Collection.BinarySearch(item, comparer)

        End Function

        Public Sub Clear() Implements IPersistentListWrapper(Of T).Clear, ICollection(Of T).Clear
            Me.Collection.Clear()

        End Sub

        Public Function Contains(ByVal item As T) As Boolean Implements IPersistentListWrapper(Of T).Contains, ICollection(Of T).Contains
            Return Me.Collection.Contains(item)

        End Function

        Public Function ConvertAll(Of TOutput)(ByVal converter As Converter(Of T, TOutput)) As List(Of TOutput) Implements IPersistentListWrapper(Of T).ConvertAll
            Return Me.Collection.ConvertAll(converter)

        End Function

        Public Sub CopyTo(ByVal array As T()) Implements IPersistentListWrapper(Of T).CopyTo
            Me.Collection.CopyTo(array)

        End Sub

        Public Sub CopyTo(ByVal index As Integer, ByVal array As T(), ByVal arrayIndex As Integer, ByVal count As Integer) Implements IPersistentListWrapper(Of T).CopyTo
            Me.Collection.CopyTo(index, array, arrayIndex, count)

        End Sub

        Public Sub CopyTo(ByVal array As T(), ByVal arrayIndex As Integer) Implements IPersistentListWrapper(Of T).CopyTo, ICollection(Of T).CopyTo
            Me.Collection.CopyTo(array, arrayIndex)

        End Sub

        Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
            Me.Collection.CopyTo(array, index)
         
        End Sub

        Public Function Exists(ByVal match As Predicate(Of T)) As Boolean Implements IPersistentListWrapper(Of T).Exists
            Return Me.Collection.Exists(match)

        End Function

        Public Function Find(ByVal match As Predicate(Of T)) As T Implements IPersistentListWrapper(Of T).Find
            Return Me.Collection.Find(match)

        End Function

        Public Function FindAll(ByVal match As Predicate(Of T)) As List(Of T) Implements IPersistentListWrapper(Of T).FindAll
            Return Me.Collection.FindAll(match)

        End Function

        Public Function FindIndex(ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindIndex
            Return Me.Collection.FindIndex(match)

        End Function

        Public Function FindIndex(ByVal startIndex As Integer, ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindIndex
            Return Me.Collection.FindIndex(startIndex, match)

        End Function

        Public Function FindIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindIndex
            Return Me.Collection.FindIndex(startIndex, count, match)

        End Function

        Public Function FindLast(ByVal match As Predicate(Of T)) As T Implements IPersistentListWrapper(Of T).FindLast
            Return Me.Collection.FindLast(match)

        End Function

        Public Function FindLastIndex(ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindLastIndex
            Return Me.Collection.FindLastIndex(match)

        End Function

        Public Function FindLastIndex(ByVal startIndex As Integer, ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindLastIndex
            Return Me.Collection.FindLastIndex(startIndex, match)

        End Function

        Public Function FindLastIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).FindLastIndex
            Return Me.Collection.FindLastIndex(startIndex, count, match)

        End Function

        Public Sub ForEach(ByVal action As Action(Of T)) Implements IPersistentListWrapper(Of T).ForEach
            Me.Collection.ForEach(action)

        End Sub

        Public Function GetRange(ByVal index As Integer, ByVal count As Integer) As List(Of T) Implements IPersistentListWrapper(Of T).GetRange
            Return Me.Collection.GetRange(index, count)

        End Function

        Public Function IndexOf(ByVal item As T) As Integer Implements IPersistentListWrapper(Of T).IndexOf
            Return Me.Collection.IndexOf(item)

        End Function

        Public Function IndexOf(ByVal item As T, ByVal index As Integer) As Integer Implements IPersistentListWrapper(Of T).IndexOf
            Return Me.Collection.IndexOf(item, index)

        End Function

        Public Function IndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer Implements IPersistentListWrapper(Of T).IndexOf
            Return Me.Collection.IndexOf(item, index, count)

        End Function

        Public Sub Insert(ByVal index As Integer, ByVal item As T) Implements IPersistentListWrapper(Of T).Insert
            Me.Collection.Insert(index, item)

        End Sub

        Public Sub InsertRange(ByVal index As Integer, ByVal collection As IEnumerable(Of T)) Implements IPersistentListWrapper(Of T).InsertRange
            Me.Collection.InsertRange(index, collection)

        End Sub

        Public Function LastIndexOf(ByVal item As T) As Integer Implements IPersistentListWrapper(Of T).LastIndexOf
            Return Me.Collection.LastIndexOf(item)

        End Function

        Public Function LastIndexOf(ByVal item As T, ByVal index As Integer) As Integer Implements IPersistentListWrapper(Of T).LastIndexOf
            Return Me.Collection.LastIndexOf(item, index)

        End Function

        Public Function LastIndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer Implements IPersistentListWrapper(Of T).LastIndexOf
            Return Me.Collection.LastIndexOf(item, index, count)

        End Function

        Public Function Remove(ByVal item As T) As Boolean Implements IPersistentListWrapper(Of T).Remove, ICollection(Of T).Remove
            Return Me.Collection.Remove(item)

        End Function

        Public Function RemoveAll(ByVal match As Predicate(Of T)) As Integer Implements IPersistentListWrapper(Of T).RemoveAll
            Return Me.Collection.RemoveAll(match)

        End Function

        Public Sub RemoveAt(ByVal index As Integer) Implements IPersistentListWrapper(Of T).RemoveAt
            Me.Collection.RemoveAt(index)

        End Sub

        Public Sub RemoveRange(ByVal index As Integer, ByVal count As Integer) Implements IPersistentListWrapper(Of T).RemoveRange
            Me.Collection.RemoveRange(index, count)

        End Sub

        Public Sub Reverse() Implements IPersistentListWrapper(Of T).Reverse
            Me.Collection.Reverse()

        End Sub

        Public Sub Reverse(ByVal index As Integer, ByVal count As Integer) Implements IPersistentListWrapper(Of T).Reverse
            Me.Collection.Reverse(index, count)

        End Sub

        Public Sub Sort() Implements IPersistentListWrapper(Of T).Sort
            Me.Collection.Sort()

        End Sub

        Public Sub Sort(ByVal comparer As IComparer(Of T)) Implements IPersistentListWrapper(Of T).Sort
            Me.Collection.Sort(comparer)

        End Sub

        Public Sub Sort(ByVal index As Integer, ByVal count As Integer, ByVal comparer As IComparer(Of T)) Implements IPersistentListWrapper(Of T).Sort
            Me.Collection.Sort(index, count, comparer)

        End Sub

        Public Sub Sort(ByVal comparison As Comparison(Of T)) Implements IPersistentListWrapper(Of T).Sort
            Me.Collection.Sort(comparison)

        End Sub

        Public Function ToArray() As T() Implements IPersistentListWrapper(Of T).ToArray
            Return Me.Collection.ToArray()

        End Function

        Public Sub TrimExcess() Implements IPersistentListWrapper(Of T).TrimExcess
            Me.Collection.TrimExcess()

        End Sub

        Public Function TrueForAll(ByVal match As Predicate(Of T)) As Boolean Implements IPersistentListWrapper(Of T).TrueForAll
            Return Me.Collection.TrueForAll(match)

        End Function

#End Region

#Region "IEnumerable Support"

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of T) Implements System.Collections.Generic.IEnumerable(Of T).GetEnumerator
            Return Me.Collection.GetEnumerator

        End Function

        Public Function GetNeutralEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Collection.GetEnumerator()

        End Function

#End Region

        Public Overloads Sub Dispose(ByVal disposeValues As Boolean) Implements IPersistentListWrapper(Of T).Dispose
            If disposeValues Then
                Dim tmpDisposeList As New ArrayList
                tmpDisposeList.AddRange(Me.Collection)

                For Each tmpValue As Object In tmpDisposeList
                    If Array.IndexOf(tmpValue.GetType.GetInterfaces(), GetType(IDisposable)) <> -1 Then
                        tmpValue.Dispose()

                    End If
                Next
            End If

            Me.Clear()

            MyBase.Dispose()

        End Sub

    End Class

End Namespace