﻿Imports mermaid.BaseObjects.Persistence.Collections.Interfaces
Imports mermaid.BaseObjects.Collections
Imports System.Runtime.Serialization

Namespace Persistence.Collections

    Public Class PersistentDictionaryWrapper(Of TKey, TValue)
        Inherits PersistentBaseObject
        Implements IPersistentDictionaryWrapper(Of TKey, TValue)

#Region "Variables"

        Private _Collection As New BaseDictionary(Of TKey, TValue)(Me)

#End Region

#Region "Properties"

        <PersistentProperty()> _
        Public Property Collection() As BaseDictionary(Of TKey, TValue) Implements IPersistentDictionaryWrapper(Of TKey, TValue).Collection
            Get
                Return Me._Collection

            End Get
            Set(ByVal value As BaseDictionary(Of TKey, TValue))
                Me._Collection = value

            End Set
        End Property

        Public ReadOnly Property SafeCopy() As BaseDictionary(Of TKey, TValue) Implements IPersistentDictionaryWrapper(Of TKey, TValue).SafeCopy
            Get
                Return Me.Collection.Clone

            End Get
        End Property

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal persistent As Boolean)
            MyBase.New(persistent)

        End Sub

#Region "Dictionary Support"

#Region "Properties"

        Public ReadOnly Property Comparer() As IEqualityComparer(Of TKey) Implements IPersistentDictionaryWrapper(Of TKey, TValue).Comparer
            Get
                Return Me.Collection.Comparer

            End Get
        End Property

        Public ReadOnly Property Count() As Integer Implements IPersistentDictionaryWrapper(Of TKey, TValue).Count
            Get
                Return Me.Collection.Count

            End Get
        End Property

        Public ReadOnly Property Keys() As Dictionary(Of TKey, TValue).KeyCollection Implements IPersistentDictionaryWrapper(Of TKey, TValue).Keys
            Get
                Return Me.Collection.Keys

            End Get
        End Property

        Public ReadOnly Property Values() As Dictionary(Of TKey, TValue).ValueCollection Implements IPersistentDictionaryWrapper(Of TKey, TValue).Values
            Get
                Return Me.Collection.Values

            End Get
        End Property

        Default Public Property Item(ByVal key As TKey) As TValue Implements IPersistentDictionaryWrapper(Of TKey, TValue).Item
            Get
                Return Me.Collection.Item(key)

            End Get
            Set(ByVal value As TValue)
                Me.Collection.Item(key) = value

            End Set
        End Property

#End Region

        Public Sub Add(ByVal key As TKey, ByVal value As TValue) Implements IPersistentDictionaryWrapper(Of TKey, TValue).Add
            Me.Collection.Add(key, value)

        End Sub

        Public Sub Clear() Implements IPersistentDictionaryWrapper(Of TKey, TValue).Clear
            Me.Collection.Clear()

        End Sub

        Public Function ContainsKey(ByVal key As TKey) As Boolean Implements IPersistentDictionaryWrapper(Of TKey, TValue).ContainsKey
            Return Me.Collection.ContainsKey(key)

        End Function

        Public Function ContainsValue(ByVal value As TValue) As Boolean Implements IPersistentDictionaryWrapper(Of TKey, TValue).ContainsValue
            Return Me.Collection.ContainsValue(value)

        End Function

        'Public Function GetEnumerator() As Dictionary(Of TKey, TValue).Enumerator Implements IPersistentDictionaryWrapper(Of TKey, TValue).GetEnumerator
        '    Return Me.Collection.GetEnumerator()

        'End Function

        Public Function Remove(ByVal key As TKey) As Boolean Implements IPersistentDictionaryWrapper(Of TKey, TValue).Remove
            Return Me.Collection.Remove(key)

        End Function

        Public Function TryGetValue(ByVal key As TKey, ByVal value As TValue) As Boolean Implements IPersistentDictionaryWrapper(Of TKey, TValue).TryGetValue
            Return Me.Collection.TryGetValue(key, value)

        End Function

#End Region

#Region "IEnumerable Support"

        Public Function GetEnumerator() As System.Collections.Generic.IEnumerator(Of TValue) Implements System.Collections.Generic.IEnumerable(Of TValue).GetEnumerator
            Return Me.Collection.Values.GetEnumerator

        End Function

        Public Function GetNeutralEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Collection.GetEnumerator()

        End Function

#End Region

        Public Overloads Sub Dispose(ByVal disposeValues As Boolean) Implements IPersistentDictionaryWrapper(Of TKey, TValue).Dispose
            If disposeValues Then
                Dim tmpDisposeList As New ArrayList
                tmpDisposeList.AddRange(Me.Collection.Values)

                For Each tmpValue As Object In tmpDisposeList
                    If Array.IndexOf(tmpValue.GetType.GetInterfaces(), GetType(IDisposable)) <> -1 Then
                        tmpValue.Dispose()

                    End If
                Next
            End If

            Me.Clear()

            MyBase.Dispose()

        End Sub

    End Class

End Namespace