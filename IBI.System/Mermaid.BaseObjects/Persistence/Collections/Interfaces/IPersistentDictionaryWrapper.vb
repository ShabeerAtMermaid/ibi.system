﻿Imports mermaid.BaseObjects.Persistence.Interfaces
Imports mermaid.BaseObjects.Collections

Namespace Persistence.Collections.Interfaces

    Public Interface IPersistentDictionaryWrapper(Of TKey, TValue)
        Inherits IPersistentBaseObject, IEnumerable(Of TValue)

#Region "Properties"

        Property Collection() As BaseDictionary(Of TKey, TValue)
        ReadOnly Property SafeCopy() As BaseDictionary(Of TKey, TValue)

        ReadOnly Property Comparer() As IEqualityComparer(Of TKey)
        ReadOnly Property Count() As Integer
        ReadOnly Property Keys() As Dictionary(Of TKey, TValue).KeyCollection
        ReadOnly Property Values() As Dictionary(Of TKey, TValue).ValueCollection

        Default Property Item(ByVal key As TKey) As TValue

#End Region

        Sub Add(ByVal key As TKey, ByVal value As TValue)
        Sub Clear()
        Function ContainsKey(ByVal key As TKey) As Boolean
        Function ContainsValue(ByVal value As TValue) As Boolean
        'Function GetEnumerator() As Dictionary(Of TKey, TValue).Enumerator
        Function Remove(ByVal key As TKey) As Boolean
        Function TryGetValue(ByVal key As TKey, ByVal value As TValue) As Boolean

        Overloads Sub Dispose(ByVal disposeValues As Boolean)

    End Interface

End Namespace