﻿Imports mermaid.BaseObjects.Persistence.Interfaces
Imports mermaid.BaseObjects.Collections

Namespace Persistence.Collections.Interfaces

    Public Interface IPersistentListWrapper(Of T)
        Inherits IPersistentBaseObject, IEnumerable(Of T), ICollection, ICollection(Of T)

#Region "Properties"

        Property Collection() As BaseList(Of T)
        ReadOnly Property SafeCopy() As BaseList(Of T)

        Property Capacity() As Integer
        ReadOnly Property Count() As Integer
        Default Property Item(ByVal index As Integer) As T

#End Region

        Sub Add(ByVal item As T)
        Sub AddRange(ByVal collection As IEnumerable(Of T))
        Function AsReadOnly() As System.Collections.ObjectModel.ReadOnlyCollection(Of T)
        Function BinarySearch(ByVal index As Integer, ByVal count As Integer, ByVal item As T, ByVal comparer As IComparer(Of T)) As Integer
        Function BinarySearch(ByVal item As T) As Integer
        Function BinarySearch(ByVal item As T, ByVal comparer As IComparer(Of T)) As Integer
        Sub Clear()
        Function Contains(ByVal item As T) As Boolean
        Function ConvertAll(Of TOutput)(ByVal converter As Converter(Of T, TOutput)) As List(Of TOutput)
        Sub CopyTo(ByVal array As T())
        Sub CopyTo(ByVal index As Integer, ByVal array As T(), ByVal arrayIndex As Integer, ByVal count As Integer)
        Sub CopyTo(ByVal array As T(), ByVal arrayIndex As Integer)
        Function Exists(ByVal match As Predicate(Of T)) As Boolean
        Function Find(ByVal match As Predicate(Of T)) As T
        Function FindAll(ByVal match As Predicate(Of T)) As List(Of T)
        Function FindIndex(ByVal match As Predicate(Of T)) As Integer
        Function FindIndex(ByVal startIndex As Integer, ByVal match As Predicate(Of T)) As Integer
        Function FindIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As Predicate(Of T)) As Integer
        Function FindLast(ByVal match As Predicate(Of T)) As T
        Function FindLastIndex(ByVal match As Predicate(Of T)) As Integer
        Function FindLastIndex(ByVal startIndex As Integer, ByVal match As Predicate(Of T)) As Integer
        Function FindLastIndex(ByVal startIndex As Integer, ByVal count As Integer, ByVal match As Predicate(Of T)) As Integer
        Sub ForEach(ByVal action As Action(Of T))
        Function GetRange(ByVal index As Integer, ByVal count As Integer) As List(Of T)
        Function IndexOf(ByVal item As T) As Integer
        Function IndexOf(ByVal item As T, ByVal index As Integer) As Integer
        Function IndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer
        Sub Insert(ByVal index As Integer, ByVal item As T)
        Sub InsertRange(ByVal index As Integer, ByVal collection As IEnumerable(Of T))
        Function LastIndexOf(ByVal item As T) As Integer
        Function LastIndexOf(ByVal item As T, ByVal index As Integer) As Integer
        Function LastIndexOf(ByVal item As T, ByVal index As Integer, ByVal count As Integer) As Integer
        Function Remove(ByVal item As T) As Boolean
        Function RemoveAll(ByVal match As Predicate(Of T)) As Integer
        Sub RemoveAt(ByVal index As Integer)
        Sub RemoveRange(ByVal index As Integer, ByVal count As Integer)
        Sub Reverse()
        Sub Reverse(ByVal index As Integer, ByVal count As Integer)
        Sub Sort()
        Sub Sort(ByVal comparer As IComparer(Of T))
        Sub Sort(ByVal index As Integer, ByVal count As Integer, ByVal comparer As IComparer(Of T))
        Sub Sort(ByVal comparison As Comparison(Of T))
        Function ToArray() As T()
        Sub TrimExcess()
        Function TrueForAll(ByVal match As Predicate(Of T)) As Boolean

        Overloads Sub Dispose(ByVal disposeValues As Boolean)

    End Interface

End Namespace