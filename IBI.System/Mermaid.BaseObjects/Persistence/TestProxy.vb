﻿Namespace Persistence

    Public Class TestProxy
        Inherits PersistenceLayerProxy

#Region "Properties"

        Public Overrides ReadOnly Property ProxyReady As Boolean
            Get
                Return True

            End Get
        End Property

        Public Overrides Property Version() As Integer
            Get
                Return 1

            End Get
            Set(ByVal value As Integer)
                
            End Set
        End Property

#End Region

        Friend Overloads Overrides Function SaveData(ByVal data As ObjectData) As Integer
            If data.InstanceID > 0 Then
                Return 0
            Else
                Return BaseUtility.IDCounter.GetID
            End If

        End Function

        Friend Overloads Overrides Sub SaveData(ByVal datalist As List(Of ObjectData), Optional ByVal timeoutInSecs As Integer = 900)

        End Sub

        Friend Overrides Sub DeleteData(ByVal instanceID As Integer)

        End Sub

        Friend Overrides Sub DeleteData(ByVal instanceIDs As System.Collections.Generic.List(Of Integer))

        End Sub

        Friend Overrides Sub ClearCollection(ByVal typeName As String, ByVal instanceID As Integer)

        End Sub

        Friend Overrides Function GetObjectData() As System.Collections.Generic.Dictionary(Of String, System.Collections.Generic.Dictionary(Of Integer, ObjectData))

        End Function

        Friend Overrides Function GetObjectData(ByVal typeName As String) As System.Collections.Generic.Dictionary(Of Integer, ObjectData)

        End Function

    End Class

End Namespace