﻿Namespace Persistence

    Public Class TestLayer
        Inherits PersistenceLayer

#Region "Variables"

        Private _Converter As SQLConverter
        Private _Proxy As TestProxy

#End Region

#Region "Properties"

        Public Overrides ReadOnly Property PersistenceLayerReady As Boolean
            Get
                Return True

            End Get
        End Property

        Friend Overrides ReadOnly Property Converter As PersistenceConverter
            Get
                Return _Converter

            End Get
        End Property

        Public Overrides ReadOnly Property Proxy As PersistenceLayerProxy
            Get
                Return _Proxy

            End Get
        End Property

#End Region

        Public Sub New(ByVal key As String)
            MyBase.New(key)

            Me._Converter = New SQLConverter
            Me._Proxy = New TestProxy()

        End Sub

    End Class

End Namespace