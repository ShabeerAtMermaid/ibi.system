Namespace Persistence

    Public MustInherit Class PersistenceLayer

#Region "Attributes"

        Private _Key As String
        Private _Instantiate As Boolean = True
        Private _InstantiateRetries As Integer = 1
        'Private _AutoCreate As Boolean = True

#End Region

#Region "Properties"

        Public Property Key() As String
            Get
                Return Me._Key

            End Get
            Set(ByVal Value As String)
                Me._Key = Value

            End Set
        End Property

        Public MustOverride ReadOnly Property Proxy() As PersistenceLayerProxy

        Friend MustOverride ReadOnly Property Converter() As PersistenceConverter

        Public Property InstantiateRetries() As Integer
            Get
                Return Me._InstantiateRetries

            End Get
            Set(ByVal value As Integer)
                Me._InstantiateRetries = value

            End Set
        End Property

        Public Property Instantiate() As Boolean
            Get
                Return Me._Instantiate

            End Get
            Set(ByVal Value As Boolean)
                Me._Instantiate = Value

            End Set
        End Property

        'Public Property AutoCreate() As Boolean
        '    Get
        '        Return _AutoCreate

        '    End Get
        '    Set(ByVal value As Boolean)
        '        _AutoCreate = value

        '    End Set
        'End Property

        Public Property Version() As Integer
            Get
                Dim tmpConnectRetries As Integer = 0
                Dim tmpPersistenceLayerReady As Boolean = False
                Dim tmpAbortConnect As Boolean = True

                While Not tmpPersistenceLayerReady And tmpConnectRetries < Me.InstantiateRetries
                    tmpPersistenceLayerReady = Me.PersistenceLayerReady

                    tmpConnectRetries += 1

                    If tmpPersistenceLayerReady Then
                        tmpAbortConnect = False

                    Else
                        If tmpConnectRetries < Me.InstantiateRetries Then
                            Dim tmpRetryInterval As TimeSpan = New TimeSpan(0, 1, 0)

                            Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(Me, True, tmpRetryInterval)

                            BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                            System.Threading.Thread.Sleep(tmpRetryInterval)

                        Else
                            Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(Me, False, New TimeSpan(0))

                            BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                        End If
                    End If


                End While

                If Not tmpAbortConnect Then
                    Return Me.Proxy.Version
                Else
                    Throw New Exception("PersistenceLayer not ready - Cannot get Version")
                End If

            End Get
            Set(ByVal value As Integer)
                Me.Proxy.Version = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal key As String)
            Me._Key = key

        End Sub

        Public MustOverride ReadOnly Property PersistenceLayerReady() As Boolean

    End Class

End Namespace