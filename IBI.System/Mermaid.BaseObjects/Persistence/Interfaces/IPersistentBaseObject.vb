Imports mermaid.BaseObjects.Interfaces

Namespace Persistence.Interfaces

    Public Interface IPersistentBaseObject
        Inherits IBaseObject

#Region "Events"

#End Region

#Region "Properties"

        Property InstanceVersion() As Integer
        ReadOnly Property PersistenceEnabled() As Boolean
        ReadOnly Property IsPersistent() As Boolean

#End Region

        Sub Save()
        
    End Interface

End Namespace
