Namespace Persistence

    Friend Class ObjectData

#Region "Attributes"

        Private _Layer As PersistenceLayer
        Private _InstanceID As Integer
        Private _Name As String
        Private _Properties As New Dictionary(Of String, ObjectProperty)
        Private _IsListEntry As Boolean
        Private _IsClearListCommand As Boolean

#End Region

#Region "Properties"

        Public ReadOnly Property Layer() As PersistenceLayer
            Get
                Return Me._Layer

            End Get
        End Property

        Public ReadOnly Property InstanceID() As Integer
            Get
                Return Me._InstanceID

            End Get
        End Property

        Public Property Name() As String
            Get
                Return Me._Name

            End Get
            Set(ByVal Value As String)
                Me._Name = Value

            End Set
        End Property

        Public ReadOnly Property Properties() As Dictionary(Of String, ObjectProperty)
            Get
                Return Me._Properties

            End Get
        End Property

        Public Property IsListEntry() As Boolean
            Get
                Return Me._IsListEntry

            End Get
            Set(ByVal value As Boolean)
                Me._IsListEntry = value

            End Set
        End Property

        Public Property IsClearListCommand() As Boolean
            Get
                Return Me._IsClearListCommand

            End Get
            Set(ByVal value As Boolean)
                Me._IsClearListCommand = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal layer As PersistenceLayer, ByVal instanceID As Integer, ByVal name As String)
            Me._Layer = layer
            Me._InstanceID = instanceID
            Me._Name = name

        End Sub

    End Class

End Namespace