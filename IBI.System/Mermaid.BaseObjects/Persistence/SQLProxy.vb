Imports System.IO
Imports System.Data.SqlClient
Imports System.Text
Imports System.Threading
Imports mermaid.BaseObjects.Diagnostics

Namespace Persistence

    Public Class SQLProxy
        Inherits PersistenceLayerProxy

#Region "Attributes"

        Private _Layer As SQLLayer
        Private _AutoCreateDatabase As Boolean = True

        Private _DatabaseServer As String
        Private _DatabaseName As String
        Private _Login As String
        Private _Password As String

        Private _DatabaseValidated As Boolean
        Private _ValidatedTables As New Dictionary(Of String, Boolean)
        Private _ValidatedColumns As New Dictionary(Of String, Boolean)
        Private _ValidatedColumnObjects As New Dictionary(Of String, Boolean)
        Private _CachedTables As New Dictionary(Of String, Dictionary(Of Integer, ObjectData))

        Private _RequiredObjectsCreated As Boolean

        Private _PrimaryDatabaseConnection As SqlConnection
        Private _BatchDatabaseConnection As SqlConnection
        Private _CommandDatabaseConnection As SqlConnection

        Private _DatabaseConnectionLock As New Object
        Private _PrimaryLock As New Object
        Private _CommandLock As New Object

        Private _PerformanceCountersEnabled As Boolean
        Private _LogEnabled As Boolean

#End Region

#Region "Properties"

        Private ReadOnly Property Layer() As SQLLayer
            Get
                Return Me._Layer

            End Get
        End Property

        Friend Property AutoCreateDatabase() As Boolean
            Get
                Return Me._AutoCreateDatabase

            End Get
            Set(ByVal value As Boolean)
                Me._AutoCreateDatabase = value

            End Set
        End Property

        Friend Property DatabaseServer() As String
            Get
                Return Me._DatabaseServer

            End Get
            Set(ByVal Value As String)
                Me._DatabaseServer = Value

            End Set
        End Property

        Friend Property DatabaseName() As String
            Get
                Return Me._DatabaseName

            End Get
            Set(ByVal Value As String)
                Me._DatabaseName = Value.Replace("-", "")

            End Set
        End Property

        Friend Property Login() As String
            Get
                Return Me._Login

            End Get
            Set(ByVal Value As String)
                Me._Login = Value

            End Set
        End Property

        Friend Property Password() As String
            Get
                Return Me._Password

            End Get
            Set(ByVal Value As String)
                Me._Password = Value

            End Set
        End Property

        Private ReadOnly Property PrimaryDatabaseConnection() As SqlConnection
            Get
                Using New Threading.Lock(Me.DatabaseConnectionLock, True)
                    If Me._PrimaryDatabaseConnection Is Nothing OrElse Not Me._PrimaryDatabaseConnection.State = ConnectionState.Open Then
                        Dim tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                        tmpConnection.Open()

                        DatabaseConnectionsOpenedPerformanceCounter.Increment()

                        Me._PrimaryDatabaseConnection = tmpConnection

                    End If

                    Return Me._PrimaryDatabaseConnection

                End Using

            End Get
        End Property

        Private ReadOnly Property BatchDatabaseConnection() As SqlConnection
            Get
                Using New Threading.Lock(Me.DatabaseConnectionLock, True)
                    If Me._BatchDatabaseConnection Is Nothing OrElse Not Me._BatchDatabaseConnection.State = ConnectionState.Open Then
                        Dim tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                        tmpConnection.Open()

                        DatabaseConnectionsOpenedPerformanceCounter.Increment()

                        Me._BatchDatabaseConnection = tmpConnection

                    End If

                    Return Me._BatchDatabaseConnection

                End Using

            End Get
        End Property

        Private ReadOnly Property CommandDatabaseConnection() As SqlConnection
            Get
                Using New Threading.Lock(Me.DatabaseConnectionLock, True)
                    If Me._CommandDatabaseConnection Is Nothing OrElse Not Me._CommandDatabaseConnection.State = ConnectionState.Open Then
                        Dim tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                        tmpConnection.Open()

                        DatabaseConnectionsOpenedPerformanceCounter.Increment()

                        Me._CommandDatabaseConnection = tmpConnection

                    End If

                    Return Me._CommandDatabaseConnection

                End Using

            End Get
        End Property

        Public ReadOnly Property DatabaseConnectionString() As String
            Get
                If Me.Login = String.Empty Then
                    Return "data source='" & Me.DatabaseServer & "';Trusted_Connection=yes;database=" & Me.DatabaseName & ";Pooling='false'"

                Else
                    Return "data source='" & Me.DatabaseServer & "';Trusted_Connection='no';database='" & Me.DatabaseName & "';Pooling='false';Uid='" & Me.Login & "';Pwd='" & Me.Password & "'"

                End If

            End Get
        End Property

        Private ReadOnly Property DatabaseMasterConnectionString() As String
            Get
                If Me.Login = String.Empty Then
                    Return "data source='" & DatabaseServer & "';Trusted_Connection=yes;database=master;Pooling='false'"

                Else
                    Return "data source='" & DatabaseServer & "';Trusted_Connection='no';database='master';Pooling='false';Uid='" & Me.Login & "';Pwd='" & Me.Password & "'"

                End If
            End Get
        End Property

        Private Property DatabaseValidated() As Boolean
            Get
                Return Me._DatabaseValidated

            End Get
            Set(ByVal Value As Boolean)
                Me._DatabaseValidated = Value

            End Set
        End Property

        Private Property ValidatedTables() As Dictionary(Of String, Boolean)
            Get
                Return Me._ValidatedTables

            End Get
            Set(ByVal Value As Dictionary(Of String, Boolean))
                Me._ValidatedTables = Value

            End Set
        End Property

        Private Property ValidatedColumns() As Dictionary(Of String, Boolean)
            Get
                Return Me._ValidatedColumns

            End Get
            Set(ByVal Value As Dictionary(Of String, Boolean))
                Me._ValidatedColumns = Value

            End Set
        End Property

        Private Property ValidatedColumnObjects() As Dictionary(Of String, Boolean)
            Get
                Return Me._ValidatedColumnObjects

            End Get
            Set(ByVal Value As Dictionary(Of String, Boolean))
                Me._ValidatedColumnObjects = Value

            End Set
        End Property

        Private Property CachedTables() As Dictionary(Of String, Dictionary(Of Integer, ObjectData))
            Get
                Return Me._CachedTables

            End Get
            Set(ByVal Value As Dictionary(Of String, Dictionary(Of Integer, ObjectData)))
                Me._CachedTables = Value

            End Set
        End Property

        Private Property RequiredObjectsCreated() As Boolean
            Get
                Return Me._RequiredObjectsCreated

            End Get
            Set(ByVal value As Boolean)
                Me._RequiredObjectsCreated = value

            End Set
        End Property

        Private ReadOnly Property DatabaseConnectionLock() As Object
            Get
                Return Me._DatabaseConnectionLock

            End Get
        End Property

        Private ReadOnly Property PrimaryLock() As Object
            Get
                Return Me._PrimaryLock

            End Get
        End Property

        Private ReadOnly Property CommandLock() As Object
            Get
                Return Me._CommandLock

            End Get
        End Property

        Public Overrides ReadOnly Property ProxyReady() As Boolean
            Get
                Try
                    'UNDONE: ProxyReady check
                    'If not autocreate then there should be a check whether the db exists and ab appropiate exception raised 
                    Using tmpConnection As New SqlConnection(Me.DatabaseMasterConnectionString)
                        tmpConnection.Open()

                        DatabaseConnectionsOpenedPerformanceCounter.Increment()

                        Dim tmpScript As New StringBuilder

                        tmpScript.AppendLine("IF EXISTS(SELECT * FROM master..sysdatabases WHERE Name = '" & Me.DatabaseName & "')")
                        tmpScript.AppendLine("BEGIN")
                        tmpScript.AppendLine("PRINT 'DB EXISTS'")
                        tmpScript.AppendLine("SELECT [name] FROM " & Me.DatabaseName & "..sysobjects WHERE type = 'U'")
                        tmpScript.AppendLine("END")
                        tmpScript.AppendLine("ELSE")
                        tmpScript.AppendLine("BEGIN")
                        tmpScript.AppendLine("PRINT 'DB DOES NOT EXIST'")
                        tmpScript.AppendLine("END")

                        Using tmpCommand As New SqlCommand(tmpScript.ToString, tmpConnection)
                            tmpCommand.ExecuteNonQuery()

                            Return True

                        End Using
                    End Using

                Catch ex As Exception
                    BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Persistence))

                    Return False

                End Try
            End Get
        End Property

        Public Property PerformanceCountersEnabled() As Boolean
            Get
                Return Me._PerformanceCountersEnabled

            End Get
            Set(ByVal value As Boolean)
                Me._PerformanceCountersEnabled = value

            End Set
        End Property

        Public Property LogEnabled() As Boolean
            Get
                Return Me._LogEnabled

            End Get
            Set(ByVal value As Boolean)
                Me._LogEnabled = value

            End Set
        End Property

        Public Property TransactionsEnabled As Boolean = True

        Public Overrides Property Version() As Integer
            Get
                Dim tmpConnectRetries As Integer = 0
                Dim tmpPersistenceLayerReady As Boolean = False
                Dim tmpAbortConnect As Boolean = True

                While Not tmpPersistenceLayerReady And tmpConnectRetries < Me.Layer.InstantiateRetries
                    tmpPersistenceLayerReady = Me.Layer.PersistenceLayerReady

                    tmpConnectRetries += 1

                    If tmpPersistenceLayerReady Then
                        tmpAbortConnect = False

                    Else
                        If tmpConnectRetries < Me.Layer.InstantiateRetries Then
                            Dim tmpRetryInterval As TimeSpan = New TimeSpan(0, 1, 0)

                            Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(Me.Layer, True, tmpRetryInterval)

                            BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                            Thread.Sleep(tmpRetryInterval)

                        Else
                            Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(Me.Layer, False, New TimeSpan(0))

                            BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                        End If
                    End If


                End While

                If Not tmpAbortConnect Then
                    Dim tmpScript As New StringBuilder

                    tmpScript.AppendLine("IF EXISTS(SELECT * FROM master..sysdatabases WHERE Name = '" & Me.DatabaseName & "')")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("  IF EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & Me.DBINFO_TABLENAME & "')")
                    tmpScript.AppendLine("  BEGIN")
                    tmpScript.AppendLine("      USE [" & Me.DatabaseName & "]")
                    tmpScript.AppendLine("    SELECT [Version] FROM [" & Me.DBINFO_TABLENAME & "]")
                    tmpScript.AppendLine("  END")
                    tmpScript.AppendLine("END")

                    Using tmpConnection As New SqlConnection(Me.DatabaseMasterConnectionString)
                        Using tmpCommand As New SqlCommand(tmpScript.ToString, tmpConnection)
                            Using tmpAdapter As New SqlDataAdapter
                                Dim tmpDataSet As New DataSet

                                If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                                If Me.LogEnabled Then Me.LogCall("Primary_", tmpScript.ToString)

                                tmpAdapter.SelectCommand = tmpCommand
                                tmpAdapter.SelectCommand.Connection = tmpConnection
                                tmpAdapter.Fill(tmpDataSet)

                                If tmpDataSet.Tables.Count > 0 Then
                                    Dim tmpTable As DataTable = tmpDataSet.Tables(0)

                                    If tmpTable.Rows.Count > 0 Then
                                        Return tmpTable.Rows(0)("Version")

                                    End If
                                End If
                            End Using
                        End Using
                    End Using

                End If

            End Get
            Set(ByVal value As Integer)
                Dim tmpScript As New StringBuilder

                tmpScript.AppendLine("IF EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & Me.DBINFO_TABLENAME & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("  UPDATE [" & Me.DBINFO_TABLENAME & "] SET [Version]=" & value)
                tmpScript.AppendLine("END")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                    If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                    If Me.LogEnabled Then Me.LogCall("Primary_", tmpScript.ToString)

                    tmpCommand.ExecuteNonQuery()

                End Using

            End Set
        End Property

#End Region

#Region "Constants"

        Private IDCOUNTER_TABLENAME As String = "InstanceIDCount"
        Private IDCOUNTER_SPNAME As String = "sp_getinstanceid"
        Private PRINTREFERENCES_SPNAME As String = "sp_printreferences"
        Private DBINFO_TABLENAME As String = "DBInfo"
        Public DBUSERTABLES_TABLENAME As String = "DBUserTables"

#End Region

#Region "Performance Counters"

        Friend Shared ReadOnly Property DatabaseCallsPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Database calls")

            End Get
        End Property

        Friend Shared ReadOnly Property DatabaseConnectionsOpenedPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Database connections opened")

            End Get
        End Property

#End Region

        Friend Sub New(ByVal layer As SQLLayer)
            MyBase.New()

            Me._Layer = layer

        End Sub

        Friend Overloads Overrides Function SaveData(ByVal data As ObjectData) As Integer
            Using New Threading.Lock(Me.PrimaryLock, True)
                Dim tmpColumnNames As New StringBuilder
                Dim tmpColumnValues As New StringBuilder

                Dim tmpScript As New StringBuilder

                tmpScript.AppendLine("SET NOCOUNT ON")

                'Check Database exists
                If Not Me.DatabaseValidated Then
                    Me.ValidateDatabase()

                End If

                'Check required objects exists
                If Not Me.RequiredObjectsCreated Then
                    Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

                End If

                'Check Table exists
                If Not Me.ValidatedTables.ContainsKey(data.Name) Then
                    Me.ValidateTable(Me.PrimaryDatabaseConnection, data.Name)

                End If

                'Checks Columns exists
                If Not Me.ValidatedColumns.ContainsKey(data.Name) Then
                    Me.ValidateColumns(Me.PrimaryDatabaseConnection, data)

                End If

                'Check Tablerow exists
                tmpScript.AppendLine("IF " & data.InstanceID & "=0 OR NOT EXISTS (SELECT * FROM [" & data.Name & "] WHERE [" & data.Name & "].InstanceID=" & data.InstanceID & ")")
                tmpScript.AppendLine("BEGIN")
                'Row doesn't exists
                tmpColumnNames.Append("(")
                tmpColumnValues.Append("(")

                For i As Integer = 0 To data.Properties.Count - 1
                    Dim tmpProperty As ObjectProperty = data.Properties.Values(i)

                    Dim tmpAttributes As Dictionary(Of Type, Attribute) = Nothing

                    If tmpProperty.Attributes.Count > 0 Then
                        tmpAttributes = New Dictionary(Of Type, Attribute)

                        For Each tmpAttribute As Attribute In tmpProperty.Attributes
                            tmpAttributes(tmpAttribute.GetType) = tmpAttribute

                        Next
                    End If

                    If Not i = 0 Then
                        tmpColumnNames.Append(", ")
                        tmpColumnValues.Append(", ")

                    End If

                    tmpColumnNames.Append("[" & tmpProperty.PropertyName & "]")
                    tmpColumnValues.Append(Me.Layer.Converter.ConvertToPersistenceValue(tmpProperty.PropertyValue, tmpProperty.PropertyType, tmpAttributes))

                Next

                tmpColumnNames.Append(")")
                tmpColumnValues.Append(")")

                tmpScript.AppendLine("INSERT INTO [" & data.Name & "]" & tmpColumnNames.ToString & " VALUES" & tmpColumnValues.ToString)

                If data.InstanceID = 0 Then
                    tmpScript.AppendLine("DECLARE @newID int")
                    tmpScript.AppendLine("DECLARE @newIDentity int")
                    tmpScript.AppendLine("EXEC " & IDCOUNTER_SPNAME & " @newID OUTPUT")
                    tmpScript.AppendLine("SELECT @newIDentity = IDENT_CURRENT('[" & data.Name & "]')")
                    tmpScript.AppendLine("UPDATE [" & data.Name & "] SET InstanceID=@newID WHERE SQLIdentity=@newIDentity")
                    tmpScript.AppendLine("SET NOCOUNT OFF")
                    tmpScript.AppendLine("SELECT @newID")
                    tmpScript.AppendLine("SET NOCOUNT ON")

                Else
                    tmpScript.AppendLine("SET NOCOUNT OFF")
                    tmpScript.AppendLine("SELECT 0")
                    tmpScript.AppendLine("SET NOCOUNT ON")

                End If

                tmpScript.AppendLine("END")
                tmpScript.AppendLine("ELSE")
                tmpScript.AppendLine("BEGIN")

                'Row exist
                tmpScript.Append("UPDATE [" & data.Name & "] SET ")

                For i As Integer = 0 To data.Properties.Count - 1
                    Dim tmpProperty As ObjectProperty = data.Properties.Values(i)

                    Dim tmpAttributes As Dictionary(Of Type, Attribute) = Nothing

                    If tmpProperty.Attributes.Count > 0 Then
                        tmpAttributes = New Dictionary(Of Type, Attribute)

                        For Each tmpAttribute As Attribute In tmpProperty.Attributes
                            tmpAttributes(tmpAttribute.GetType) = tmpAttribute

                        Next
                    End If

                    If Not i = 0 Then
                        tmpScript.Append(", ")

                    End If

                    tmpScript.Append("[" & tmpProperty.PropertyName & "]=" & Me.Layer.Converter.ConvertToPersistenceValue(tmpProperty.PropertyValue, tmpProperty.PropertyType, tmpAttributes))

                Next

                tmpScript.AppendLine(" WHERE InstanceID=" & data.InstanceID)
                tmpScript.AppendLine("SET NOCOUNT OFF")
                tmpScript.AppendLine("SELECT 0")
                tmpScript.AppendLine("SET NOCOUNT OFF")
                tmpScript.AppendLine("END")

                tmpScript.AppendLine("SET NOCOUNT OFF")

                Try
                    'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                    '    tmpConnection.Open()

                    Using tmpCommandObject As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                        If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()

                        If Me.LogEnabled Then
                            Me.LogCall("Primary_", tmpScript.ToString)
                        End If


                        If data.InstanceID = 0 Then
                            Dim tmpDataSet As New DataSet

                            Using tmpAdapter As New SqlDataAdapter
                                tmpAdapter.SelectCommand = tmpCommandObject
                                tmpAdapter.SelectCommand.Connection = Me.PrimaryDatabaseConnection
                                tmpAdapter.Fill(tmpDataSet)

                            End Using

                            Return tmpDataSet.Tables(0).Rows(0).Item(0)

                        Else
                            tmpCommandObject.ExecuteNonQuery()

                            Return data.InstanceID

                        End If

                    End Using
                    'End Using

                Catch ex As Exception
                    Me.ResetPrimaryDatabaseConnection()

                    Me.LogCall("PrimaryError_", tmpScript.ToString)

                    Throw New Exception("Error saving data", ex)

                End Try

            End Using

        End Function

        Friend Overloads Overrides Sub SaveData(ByVal datalist As List(Of ObjectData), Optional ByVal timeoutInSecs As Integer = 900)
            Dim tmpSaveDataList As New List(Of ObjectData)
            Dim tmpUseTransactions As Boolean = TransactionsEnabled

            If datalist.Count > 0 Then
                tmpSaveDataList.AddRange(datalist.GetRange(0, Math.Min(datalist.Count, 400)))
                datalist.RemoveRange(0, Math.Min(datalist.Count, 400))
            End If

            While tmpSaveDataList.Count > 0
                Dim tmpScript As StringBuilder = New StringBuilder

                Dim tmpProperty As ObjectProperty

                Dim tmpColumnNames As StringBuilder = New StringBuilder
                Dim tmpColumnValues As StringBuilder = New StringBuilder

                tmpScript.AppendLine("SET NOCOUNT ON")

                If tmpUseTransactions Then tmpScript.AppendLine("BEGIN TRANSACTION")

                Dim tmpLastContainerID As Integer = 0
                Dim tmpCurrentContainerID As Integer = 0

                For y As Integer = 0 To tmpSaveDataList.Count - 1
                    Dim tmpObjectData As ObjectData = tmpSaveDataList(y)
                    tmpLastContainerID = tmpCurrentContainerID
                    tmpCurrentContainerID = 0

                    If tmpObjectData.IsClearListCommand Then
                        'Me.ClearCollection(tmpObjectData.Name, tmpObjectData.InstanceID)
                        tmpScript.AppendLine(Me.GenerateClearCollectionScript(tmpObjectData.Name, tmpObjectData.InstanceID))
                        tmpCurrentContainerID = tmpObjectData.InstanceID

                    Else
                        'Check Database exists
                        If Not Me.DatabaseValidated Then
                            Me.ValidateDatabase()

                        End If

                        'Check required objects exists
                        If Not Me.RequiredObjectsCreated Then
                            Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

                        End If

                        'Check Table exists
                        If Not Me.ValidatedTables.ContainsKey(tmpObjectData.Name) Then
                            Me.ValidateTable(Me.BatchDatabaseConnection, tmpObjectData.Name)

                        End If

                        'Checks Columns exists
                        If Not Me.ValidatedColumns.ContainsKey(tmpObjectData.Name) Then
                            Me.ValidateColumns(Me.BatchDatabaseConnection, tmpObjectData)

                        End If

                        'Check Tablerow exists
                        If Not tmpObjectData.IsListEntry Then
                            tmpScript.Append("IF NOT EXISTS (SELECT * FROM [" & tmpObjectData.Name & "] WHERE [InstanceID]=" & tmpObjectData.InstanceID & ")" & vbCrLf)
                            tmpScript.Append("BEGIN" & vbCrLf)

                        End If

                        tmpColumnNames = New StringBuilder
                        tmpColumnValues = New StringBuilder

                        tmpColumnNames.Append("(")
                        tmpColumnValues.Append("(")

                        For i As Integer = 0 To tmpObjectData.Properties.Values.Count - 1
                            tmpProperty = tmpObjectData.Properties.Values(i)

                            Dim tmpAttributes As Dictionary(Of Type, Attribute) = Nothing

                            If tmpProperty.Attributes.Count > 0 Then
                                tmpAttributes = New Dictionary(Of Type, Attribute)

                                For Each tmpAttribute As Attribute In tmpProperty.Attributes
                                    tmpAttributes(tmpAttribute.GetType) = tmpAttribute

                                Next
                            End If

                            If Not i = 0 Then
                                tmpColumnNames.Append(", ")
                                tmpColumnValues.Append(", ")

                            End If

                            tmpColumnNames.Append("[" & tmpProperty.PropertyName & "]")
                            tmpColumnValues.Append(Me.Layer.Converter.ConvertToPersistenceValue(tmpProperty.PropertyValue, tmpProperty.PropertyType, tmpAttributes))

                            If tmpObjectData.IsListEntry And String.Compare("Container", tmpProperty.PropertyName, True) = 0 Then
                                'We want to save the container this entry belongs to
                                tmpCurrentContainerID = tmpProperty.PropertyValue

                            End If
                        Next

                        tmpColumnNames.Append(")")
                        tmpColumnValues.Append(")")

                        If tmpObjectData.IsListEntry Then
                            tmpScript.Append("INSERT INTO [" & tmpObjectData.Name & "]" & tmpColumnNames.ToString & " VALUES" & tmpColumnValues.ToString & vbCrLf)

                        Else
                            tmpScript.Append("INSERT INTO [" & tmpObjectData.Name & "]" & tmpColumnNames.ToString & " VALUES" & tmpColumnValues.ToString & vbCrLf)
                            tmpScript.Append("END" & vbCrLf)
                            tmpScript.Append("ELSE" & vbCrLf)
                            tmpScript.Append("BEGIN" & vbCrLf)
                            'Row exist
                            tmpScript.Append("UPDATE [" & tmpObjectData.Name & "] SET ")

                            For i As Integer = 0 To tmpObjectData.Properties.Values.Count - 1
                                tmpProperty = tmpObjectData.Properties.Values(i)

                                Dim tmpAttributes As Dictionary(Of Type, Attribute) = Nothing

                                If tmpProperty.Attributes.Count > 0 Then
                                    tmpAttributes = New Dictionary(Of Type, Attribute)

                                    For Each tmpAttribute As Attribute In tmpProperty.Attributes
                                        tmpAttributes(tmpAttribute.GetType) = tmpAttribute

                                    Next
                                End If

                                If Not i = 0 Then
                                    tmpScript.Append(", ")

                                End If

                                tmpScript.Append("[" & tmpProperty.PropertyName & "]=" & Me.Layer.Converter.ConvertToPersistenceValue(tmpProperty.PropertyValue, tmpProperty.PropertyType, tmpAttributes))

                            Next

                            tmpScript.Append(" WHERE InstanceID=" & tmpObjectData.InstanceID & vbCrLf)
                            tmpScript.Append("END" & vbCrLf)

                        End If
                    End If

                    If y = tmpSaveDataList.Count - 1 Then
                        'We are at last entry
                        'and need to peek if the next entry belongs to the same container

                        If datalist.Count > 0 Then
                            Dim tmpPeekObject As ObjectData = datalist(0)

                            If tmpPeekObject.IsListEntry Then
                                If tmpPeekObject.Properties.ContainsKey("Container") Then
                                    Dim tmpContainerID As Integer = tmpPeekObject.Properties("Container").PropertyValue

                                    If tmpContainerID = tmpCurrentContainerID Then
                                        'The next item in the queue is for the same container
                                        'Added to current queue to keep entries in the same script.
                                        tmpSaveDataList.Add(datalist(0))
                                        datalist.RemoveAt(0)
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next

                If tmpUseTransactions Then tmpScript.AppendLine("COMMIT TRANSACTION")

                tmpScript.Append("SET NOCOUNT OFF" & vbCrLf)

                Try
                    Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.BatchDatabaseConnection)
                        tmpCommand.CommandTimeout = timeoutInSecs

                        If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                        If Me.LogEnabled Then
                            Me.LogCall("Batch_", tmpScript.ToString)
                        End If

                        tmpCommand.ExecuteNonQuery()

                    End Using

                    tmpSaveDataList.Clear()

                Catch ex As Exception
                    Me.ResetBatchDatabaseConnection()

                    Me.LogCall("BatchError_", tmpScript.ToString)

                    Throw New Exception("Error saving data.", ex)

                End Try

                If datalist.Count > 0 Then
                    tmpSaveDataList.AddRange(datalist.GetRange(0, Math.Min(datalist.Count, 400)))
                    datalist.RemoveRange(0, Math.Min(datalist.Count, 400))
                End If
            End While

        End Sub

        Public Overloads Sub SaveData(ByVal data As System.Data.DataSet)
            Using New Threading.Lock(Me.CommandLock, True)
                If Not data Is Nothing Then
                    If Not Me.DatabaseValidated Then
                        Me.ValidateDatabase()

                    End If

                    Dim tmpScript As New StringBuilder

                    Try
                        For Each tmpTable As DataTable In data.Tables
                            If tmpTable.Rows.Count > 0 Then
                                Me.ValidateTable(Me.CommandDatabaseConnection, tmpTable.TableName)

                                For Each tmpColumn As DataColumn In tmpTable.Columns
                                    Me.ValidateColumn(Me.CommandDatabaseConnection, tmpTable.TableName, tmpColumn.ColumnName, tmpColumn.DataType)

                                Next

                                For Each tmpRow As DataRow In tmpTable.Rows
                                    Dim tmpColumnNames As New StringBuilder
                                    Dim tmpColumnValues As New StringBuilder

                                    tmpColumnNames.Append("(")
                                    tmpColumnValues.Append("(")

                                    For i As Integer = 0 To tmpTable.Columns.Count - 1
                                        Dim tmpColumn As DataColumn = tmpTable.Columns(i)

                                        If Not i = 0 Then
                                            tmpColumnNames.Append(", ")
                                            tmpColumnValues.Append(", ")

                                        End If

                                        tmpColumnNames.Append("[" & tmpColumn.ColumnName & "]")
                                        tmpColumnValues.Append(Me.Layer.Converter.ConvertToPersistenceValue(tmpRow(tmpColumn.ColumnName), tmpColumn.DataType))

                                    Next

                                    tmpColumnNames.Append(")")
                                    tmpColumnValues.Append(")")

                                    tmpScript.Append("INSERT INTO [" & tmpTable.TableName & "]" & tmpColumnNames.ToString & " VALUES" & tmpColumnValues.ToString & vbCrLf)

                                Next

                                'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                                '    tmpConnection.Open()

                                Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.CommandDatabaseConnection)
                                    If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                                    If Me.LogEnabled Then
                                        Me.LogCall("Command_", tmpScript.ToString)

                                    End If

                                    tmpCommand.ExecuteNonQuery()

                                End Using
                                'End Using


                            End If

                        Next

                    Catch ex As Exception
                        Me.ResetCommandDatabaseConnection()

                        Me.LogCall("CommandError_", tmpScript.ToString)

                        Throw New Exception("Error saving data", ex)

                    End Try
                End If

            End Using

        End Sub

        Friend Overrides Sub ClearCollection(ByVal typeName As String, ByVal instanceID As Integer)
            Dim tmpScript As String = Me.GenerateClearCollectionScript(typeName, instanceID)

            Try
                Using tmpCommand As New SqlCommand(tmpScript, Me.BatchDatabaseConnection)
                    If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()

                    If Me.LogEnabled Then
                        Me.LogCall("Batch_", tmpScript.ToString)

                    End If

                    tmpCommand.CommandTimeout = TimeSpan.FromMinutes(10).TotalSeconds
                    tmpCommand.ExecuteNonQuery()

                End Using
                'End Using

            Catch ex As Exception
                Me.ResetBatchDatabaseConnection()

                Me.LogCall("BatchError_", tmpScript.ToString)

                Throw New Exception("Error clearing collection", ex)

            End Try
        End Sub

        Private Function GenerateClearCollectionScript(ByVal typeName As String, ByVal instanceID As Integer) As String
            'Check Database exists
            If Not Me.DatabaseValidated Then
                Me.ValidateDatabase()

            End If

            'Check required objects exists
            If Not Me.RequiredObjectsCreated Then
                Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

            End If

            If Not Me.ValidatedTables.ContainsKey(typeName) Then
                Me.ValidateTable(Me.BatchDatabaseConnection, typeName)

            End If

            Dim tmpScript As New StringBuilder

            tmpScript.AppendLine("IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME='Container' AND TABLE_NAME='" & typeName & "')")
            tmpScript.AppendLine("BEGIN")
            tmpScript.AppendLine("DECLARE @ssql" & instanceID.ToString & " varChar(255)")
            tmpScript.AppendLine("SET @ssql" & instanceID.ToString & " = 'DELETE FROM [" & typeName & "] WHERE Container=" & instanceID & "'")
            tmpScript.AppendLine("EXEC(@ssql" & instanceID.ToString & ")")
            tmpScript.AppendLine("END")

            Return tmpScript.ToString

        End Function

        Friend Overrides Sub DeleteData(ByVal instanceID As Integer)
            Using New Threading.Lock(Me.PrimaryLock, True)
                Dim tmpScript As New StringBuilder

                'Check Database exists
                If Not Me.DatabaseValidated Then
                    Me.ValidateDatabase()

                End If

                'Check required objects exists
                If Not Me.RequiredObjectsCreated Then
                    Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

                End If

                tmpScript.AppendLine("DECLARE @databaseName varchar(255)")
                tmpScript.AppendLine("set @databasename = '" & Me.DatabaseName & "'")
                tmpScript.AppendLine("CREATE TABLE #allTables ([name] varChar(255))")
                tmpScript.AppendLine("DECLARE @ssql varChar(255)")
                tmpScript.AppendLine("DECLARE @ssql2 varChar(255)")
                tmpScript.AppendLine("DECLARE @tablename varChar(200)")
                tmpScript.AppendLine("DECLARE @newTable varChar(255)")
                tmpScript.AppendLine("DECLARE @dropTable varChar(255)")
                tmpScript.AppendLine("SET @ssql = 'INSERT INTO #allTables SELECT [name] FROM ' + @databasename + '..sysobjects WHERE type = ''U'''")
                tmpScript.AppendLine("EXEC (@ssql)")
                tmpScript.AppendLine("DECLARE cnames	CURSOR FOR ")
                tmpScript.AppendLine("SELECT [name] FROM #allTables")
                tmpScript.AppendLine("OPEN cnames")
                tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
                tmpScript.AppendLine("WHILE (@@fetch_status <> -1)")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("IF(@tablename != 'dtproperties' AND @tableName != '" & Me.IDCOUNTER_TABLENAME & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("DECLARE @count int")
                tmpScript.AppendLine("DECLARE @countValidate int")
                tmpScript.AppendLine("SELECT @countValidate = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name='InstanceID' AND table_name=@tableName")
                tmpScript.AppendLine("IF @countValidate = 1")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("SELECT @count = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name='Container' AND table_name=@tableName")
                tmpScript.AppendLine("IF @count=1")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE InstanceID=" & instanceID & " OR Container=" & instanceID & "'")
                tmpScript.AppendLine("END")
                tmpScript.AppendLine("ELSE")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE InstanceID=" & instanceID & "'")
                tmpScript.AppendLine("END")
                tmpScript.AppendLine("EXEC (@ssql2)")
                tmpScript.AppendLine("END")
                tmpScript.AppendLine("END")
                tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
                tmpScript.AppendLine("END")
                tmpScript.AppendLine("DEALLOCATE cnames")
                tmpScript.AppendLine("DROP TABLE #allTables")

                Try
                    'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                    '    tmpConnection.Open()

                    Using tmpCommandObject As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                        If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                        If Me.LogEnabled Then
                            Me.LogCall("Primary_", tmpScript.ToString)

                        End If


                        tmpCommandObject.ExecuteNonQuery()

                    End Using
                    'End Using

                Catch ex As Exception
                    Me.ResetPrimaryDatabaseConnection()

                    Me.LogCall("PrimaryError_", tmpScript.ToString)

                    Throw New Exception("Error deleting data", ex)

                End Try

            End Using

        End Sub

        Friend Overloads Overrides Sub DeleteData(ByVal instanceIDs As List(Of Integer))
            Using New Threading.Lock(Me.PrimaryLock, True)
                Dim tmpDeleteDataList As New List(Of Integer)

                If instanceIDs.Count > 0 Then
                    tmpDeleteDataList.AddRange(instanceIDs.GetRange(0, Math.Min(instanceIDs.Count, 400)))
                    instanceIDs.RemoveRange(0, Math.Min(instanceIDs.Count, 400))
                End If

                While tmpDeleteDataList.Count > 0
                    Dim tmpScript As StringBuilder = New StringBuilder

                    'Check Database exists
                    If Not Me.DatabaseValidated Then
                        Me.ValidateDatabase()

                    End If

                    'Check required objects exists
                    If Not Me.RequiredObjectsCreated Then
                        Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

                    End If

                    tmpScript.AppendLine("DECLARE @databaseName varchar(255)")
                    tmpScript.AppendLine("set @databasename = '" & Me.DatabaseName & "'")
                    tmpScript.AppendLine("CREATE TABLE #allTables ([name] varChar(255))")
                    tmpScript.AppendLine("DECLARE @ssql varChar(8000)")
                    tmpScript.AppendLine("DECLARE @ssql2 varChar(8000)")
                    tmpScript.AppendLine("DECLARE @tablename varChar(200)")
                    tmpScript.AppendLine("DECLARE @newTable varChar(255)")
                    tmpScript.AppendLine("DECLARE @dropTable varChar(255)")
                    tmpScript.AppendLine("SET @ssql = 'INSERT INTO #allTables SELECT [name] FROM ' + @databasename + '..sysobjects WHERE type = ''U'''")
                    tmpScript.AppendLine("EXEC (@ssql)")
                    tmpScript.AppendLine("DECLARE cnames	CURSOR FOR ")
                    tmpScript.AppendLine("SELECT [name] FROM #allTables")
                    tmpScript.AppendLine("OPEN cnames")
                    tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
                    tmpScript.AppendLine("WHILE (@@fetch_status <> -1)")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("IF(@tablename != 'dtproperties' AND @tableName != '" & Me.IDCOUNTER_TABLENAME & "')")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("DECLARE @count int")
                    tmpScript.AppendLine("DECLARE @countValidate int")
                    tmpScript.AppendLine("SELECT @countValidate = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name='InstanceID' AND table_name=@tableName")
                    tmpScript.AppendLine("IF @countValidate = 1")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("SELECT @count = COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name='Container' AND table_name=@tableName")
                    tmpScript.AppendLine("IF @count=1")
                    tmpScript.AppendLine("BEGIN")

                    Dim tmpSSql2 As String = String.Empty ' "SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE "

                    For i As Integer = 0 To tmpDeleteDataList.Count - 1
                        If String.IsNullOrEmpty(tmpSSql2) Then
                            tmpSSql2 &= "SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE "
                        Else
                            tmpSSql2 &= "OR "
                        End If

                        tmpSSql2 &= "InstanceID = " & tmpDeleteDataList(i) & " Or Container = " & tmpDeleteDataList(i) & " "

                        If tmpSSql2.Length > 7000 Then
                            tmpScript.AppendLine(tmpSSql2 & "'")
                            tmpScript.AppendLine("EXEC (@ssql2)")

                            tmpSSql2 = String.Empty
                        End If
                    Next

                    If Not String.IsNullOrEmpty(tmpSSql2) Then
                        tmpScript.AppendLine(tmpSSql2 & "'")
                        tmpScript.AppendLine("EXEC (@ssql2)")

                        tmpSSql2 = String.Empty
                    End If

                    'tmpScript.AppendLine("'")
                    tmpScript.AppendLine("END")
                    tmpScript.AppendLine("ELSE")
                    tmpScript.AppendLine("BEGIN")
                    'tmpScript.Append("SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE ")

                    'For i As Integer = 0 To tmpDeleteDataList.Count - 1
                    '    If i <> 0 Then tmpScript.Append("OR ")

                    '    tmpScript.Append("InstanceID = " & tmpDeleteDataList(i) & " ")
                    'Next

                    tmpSSql2 = String.Empty

                    For i As Integer = 0 To tmpDeleteDataList.Count - 1
                        If String.IsNullOrEmpty(tmpSSql2) Then
                            tmpSSql2 &= "SET @ssql2 = 'DELETE FROM [' + @tablename + '] WHERE "
                        Else
                            tmpSSql2 &= "OR "
                        End If

                        tmpSSql2 &= "InstanceID = " & tmpDeleteDataList(i) & " "

                        If tmpSSql2.Length > 7000 Then
                            tmpScript.AppendLine(tmpSSql2 & "'")
                            tmpScript.AppendLine("EXEC (@ssql2)")

                            tmpSSql2 = String.Empty
                        End If
                    Next

                    If Not String.IsNullOrEmpty(tmpSSql2) Then
                        tmpScript.AppendLine(tmpSSql2 & "'")
                        tmpScript.AppendLine("EXEC (@ssql2)")

                        tmpSSql2 = String.Empty
                    End If

                    'tmpScript.AppendLine("'")
                    tmpScript.AppendLine("END")
                    'tmpScript.AppendLine("EXEC (@ssql2)")
                    tmpScript.AppendLine("END")
                    tmpScript.AppendLine("END")
                    tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
                    tmpScript.AppendLine("END")
                    tmpScript.AppendLine("DEALLOCATE cnames")
                    tmpScript.AppendLine("DROP TABLE #allTables")

                    Try
                        Using tmpCommandObject As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                            If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                            If Me.LogEnabled Then
                                Me.LogCall("Primary_", tmpScript.ToString)
                            End If


                            tmpCommandObject.ExecuteNonQuery()

                        End Using

                        tmpDeleteDataList.Clear()

                    Catch ex As Exception
                        Me.ResetPrimaryDatabaseConnection()

                        Me.LogCall("PrimaryError_", tmpScript.ToString)

                        Throw New Exception("Error deleting data", ex)

                    End Try

                    If instanceIDs.Count > 0 Then
                        tmpDeleteDataList.AddRange(instanceIDs.GetRange(0, Math.Min(instanceIDs.Count, 400)))
                        instanceIDs.RemoveRange(0, Math.Min(instanceIDs.Count, 400))
                    End If

                End While
            End Using

        End Sub

        Private Function GetUserTables() As List(Of String)
            Try
                Dim tmpTableList As New List(Of String)

                'Check required objects exists
                If Not Me.RequiredObjectsCreated Then
                    Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)
                End If

                Dim tmpScript As New StringBuilder
                tmpScript.AppendLine("SELECT * FROM [" & Me.DBUSERTABLES_TABLENAME & "]")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                    Using tmpAdapter As New SqlDataAdapter
                        If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                        If Me.LogEnabled Then Me.LogCall("Primary_", tmpScript.ToString)

                        Dim tmpDataSet As New DataSet

                        tmpAdapter.SelectCommand = tmpCommand
                        tmpAdapter.SelectCommand.Connection = Me.PrimaryDatabaseConnection
                        tmpAdapter.Fill(tmpDataSet)

                        If tmpDataSet.Tables.Count > 0 Then
                            If tmpDataSet.Tables(0).Rows.Count > 0 Then
                                For Each tmpRow As DataRow In tmpDataSet.Tables(0).Rows
                                    Dim tmpTableName As String = tmpRow("TableName")

                                    tmpTableList.Add(tmpTableName)
                                Next
                            End If
                        End If
                    End Using
                End Using

                Return tmpTableList

            Catch ex As Exception
                Me.ResetPrimaryDatabaseConnection()

                Throw New Exception("Error getting UserTables", ex)

            End Try
        End Function

        Public Sub AddUserTableName(ByVal tableName As String)
            Try
                Dim tmpScript As New StringBuilder
                tmpScript.AppendLine("DECLARE @rowCount Int")
                tmpScript.AppendLine("SELECT @rowCount = COUNT(*) FROM [" & Me.DBUSERTABLES_TABLENAME & "] WHERE [TableName] = " & Me.Layer.Converter.ConvertToPersistenceValue(tableName, GetType(String)))
                tmpScript.AppendLine()
                tmpScript.AppendLine("IF (@rowCount = 0)")
                tmpScript.AppendLine("  INSERT INTO [" & Me.DBUSERTABLES_TABLENAME & "]([TableName]) VALUES(" & Me.Layer.Converter.ConvertToPersistenceValue(tableName, GetType(String)) & ")")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                    tmpCommand.ExecuteNonQuery()

                End Using

            Catch ex As Exception
                Me.ResetPrimaryDatabaseConnection()

                Throw New Exception("Error adding UserTable name", ex)

            End Try
        End Sub

        Friend Overrides Function GetObjectData() As Dictionary(Of String, Dictionary(Of Integer, ObjectData))
            Dim tmpResult As New Dictionary(Of String, Dictionary(Of Integer, ObjectData))
            Try

            
            'Check Database exists
            If Not Me.DatabaseValidated Then
                Me.ValidateDatabase()

            End If

            'Check required objects exists
            If Not Me.RequiredObjectsCreated Then
                Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

            End If

            Dim tmpUserTableNames As List(Of String) = Me.GetUserTables

            Dim tmpScript As New StringBuilder

            tmpScript.AppendLine("DECLARE @databaseName varchar(255)")
            tmpScript.AppendLine("SET @databasename = '" & Me.DatabaseName & "'")
            tmpScript.AppendLine("CREATE TABLE #allTables ([name] varChar(255))")
            tmpScript.AppendLine("DECLARE @ssql varChar(255)")
            tmpScript.AppendLine("DECLARE @ssql2 varChar(255)")
            tmpScript.AppendLine("DECLARE @tablename varChar(200)")
            tmpScript.AppendLine("DECLARE @newTable varChar(255)")
            tmpScript.AppendLine("DECLARE @dropTable varChar(255)")
            tmpScript.AppendLine("SET @ssql = 'INSERT INTO #allTables SELECT [name] FROM ' + @databasename + '..sysobjects WHERE type = ''U'''")
            tmpScript.AppendLine("EXEC (@ssql)")
            tmpScript.AppendLine("DECLARE cnames CURSOR FOR ")
            tmpScript.AppendLine("SELECT [name] FROM #allTables")
            tmpScript.AppendLine("OPEN cnames")
            tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
            tmpScript.AppendLine("WHILE (@@fetch_status <> -1)")
            tmpScript.AppendLine("BEGIN")
            tmpScript.Append("IF(@tablename != 'dtproperties' AND @tableName != '" & Me.IDCOUNTER_TABLENAME & "' AND @tableName != '" & Me.DBINFO_TABLENAME & "' AND @tableName != '" & Me.DBUSERTABLES_TABLENAME & "' AND NOT @tableName LIKE '%_CREF'")

            For Each tmpUserTableName As String In tmpUserTableNames
                tmpScript.Append(" AND @tableName != '" & tmpUserTableName & "'")
            Next

            tmpScript.AppendLine(")")
            tmpScript.AppendLine("BEGIN")
            'tmpScript.AppendLine("SET @ssql2 = 'SELECT * FROM [' + @tablename + '] AS [' + @tablename + ']'")
            tmpScript.AppendLine("SET @ssql2 = 'SELECT * FROM [' + @tablename + ']'")
            tmpScript.AppendLine("EXEC (@ssql2)")
            tmpScript.AppendLine("END")
            tmpScript.AppendLine("FETCH NEXT FROM cnames INTO @tablename")
            tmpScript.AppendLine("END")
            tmpScript.AppendLine("DEALLOCATE cnames")
            tmpScript.AppendLine("DROP TABLE #allTables")

            Try
                'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                '    tmpConnection.Open()

                Using tmpCommandObject As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                    Using tmpAdapter As New SqlDataAdapter
                        If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                        If Me.LogEnabled Then Me.LogCall("Primary_", tmpScript.ToString)

                        Dim tmpDataSet As New DataSet

                        tmpAdapter.SelectCommand = tmpCommandObject
                        tmpAdapter.SelectCommand.Connection = Me.PrimaryDatabaseConnection
                        tmpAdapter.Fill(tmpDataSet)

                        For Each tmpDataTable As DataTable In tmpDataSet.Tables
                            'If tmpUserTableNames.Contains(tmpDataTable.TableName) Then
                            '    Continue For
                            'End If

                            Dim tmpObjectName As String = tmpDataTable.TableName 'UNDONE: Always return "Table"???

                            Dim tmpObjectDictionary As Dictionary(Of Integer, ObjectData) = New Dictionary(Of Integer, ObjectData)
                            tmpResult(tmpObjectName) = tmpObjectDictionary

                            For Each tmpRow As DataRow In tmpDataTable.Rows
                                Dim tmpInstanceID As Integer = tmpRow("InstanceID")
                                Dim tmpObjectData As ObjectData = New ObjectData(Me.Layer, tmpInstanceID, tmpObjectName)
                                tmpObjectDictionary(tmpInstanceID) = tmpObjectData

                                For Each tmpColumn As DataColumn In tmpDataTable.Columns
                                    Dim tmpValue As Object = tmpRow(tmpColumn.ColumnName)

                                    If tmpValue Is DBNull.Value Then tmpValue = Nothing

                                    tmpObjectData.Properties(tmpColumn.ColumnName.ToLower) = New ObjectProperty(tmpColumn.ColumnName, tmpValue, tmpColumn.DataType)

                                Next
                            Next

                            Me.CachedTables(tmpObjectName) = tmpObjectDictionary

                        Next

                    End Using
                End Using
                'End Using

            Catch ex As Exception
                Me.ResetPrimaryDatabaseConnection()

                Throw New Exception("Error getting ObjectData", ex)

            End Try
            Catch ex As Exception

            End Try
            Return tmpResult

        End Function

        Friend Overrides Function GetObjectData(ByVal typeName As String) As Dictionary(Of Integer, ObjectData)
            Try

                If Not Me.CachedTables.ContainsKey(typeName) Then
                    'Check Database exists
                    If Not Me.DatabaseValidated Then
                        Me.ValidateDatabase()

                    End If

                    'Check required objects exists
                    If Not Me.RequiredObjectsCreated Then
                        Me.CreateRequiredObjects(Me.PrimaryDatabaseConnection)

                    End If

                    'Check Table exists
                    'If Not Me.ValidatedTables.Contains(tableName) Then
                    '    Me.ValidateTable(tableName)

                    'End If

                    Dim tmpScript As New StringBuilder

                    tmpScript.AppendLine("IF EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & typeName & "')")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("SELECT * FROM [" & typeName & "]")
                    tmpScript.AppendLine("END")

                    Try
                        'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                        '    tmpConnection.Open()

                        Using tmpCommand As New SqlCommand(tmpScript.ToString, Me.PrimaryDatabaseConnection)
                            Using tmpAdapter As New SqlDataAdapter
                                Dim tmpDataSet As New DataSet

                                If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                                If Me.LogEnabled Then Me.LogCall("Primary_", tmpScript.ToString)

                                tmpAdapter.SelectCommand = tmpCommand
                                tmpAdapter.SelectCommand.Connection = Me.PrimaryDatabaseConnection
                                tmpAdapter.Fill(tmpDataSet)

                                If Not tmpDataSet Is Nothing Then
                                    If tmpDataSet.Tables.Count > 0 Then

                                        Dim tmpObjectName As String = typeName ' tmpDataSet.Tables(0).TableName
                                        Dim isReferenceTable As Boolean = tmpObjectName.EndsWith("_CREF")

                                        Dim tmpObjectDictionary As Dictionary(Of Integer, ObjectData) = New Dictionary(Of Integer, ObjectData)

                                        For Each tmpRow As DataRow In tmpDataSet.Tables(0).Rows
                                            Dim tmpID As Integer = 0

                                            If Not isReferenceTable Then
                                                tmpID = tmpRow("InstanceID")

                                            Else
                                                tmpID = tmpRow("SQLIdentity")

                                            End If

                                            Dim tmpObjectData As ObjectData = New ObjectData(Me.Layer, tmpID, tmpObjectName)
                                            tmpObjectDictionary(tmpID) = tmpObjectData

                                            For Each tmpColumn As DataColumn In tmpDataSet.Tables(0).Columns
                                                Dim tmpValue As Object = tmpRow(tmpColumn.ColumnName)

                                                If tmpValue Is DBNull.Value Then tmpValue = Nothing

                                                tmpObjectData.Properties(tmpColumn.ColumnName.ToLower) = New ObjectProperty(tmpColumn.ColumnName, tmpValue, tmpColumn.DataType)

                                            Next
                                        Next

                                        Me.CachedTables(tmpObjectName) = tmpObjectDictionary

                                        Return tmpObjectDictionary

                                    End If
                                End If

                            End Using
                        End Using
                        'End Using

                    Catch ex As Exception
                        Me.ResetPrimaryDatabaseConnection()

                        Throw New Exception("Error getting ObjectData", ex)

                    End Try

                Else
                    Return Me.CachedTables(typeName)

                End If

                Me.CachedTables.Remove(typeName)
            Catch ex As Exception

            End Try

            Return Nothing

        End Function

        'Public Overloads Overrides Function AcquireData(ByVal tableName As String, ByVal queryParameters() As QueryParameter) As DataSet
        '    Try
        '        If Not Me.DatabaseValidated Then
        '            Me.ValidateDatabase()
        '            Me.CreateRequiredObjects()

        '        End If

        '        If Not Me.ValidatedTables.Contains(tableName) Then
        '            Me.ValidateTable(tableName)

        '        End If

        '        Dim sb As New StringBuilder
        '        Dim result As New DataSet

        '        sb.Append("SELECT * FROM [" & tableName & "] ")

        '        If queryParameters.Length > 0 Then
        '            sb.Append("WHERE ")

        '            For i As Integer = 0 To queryParameters.Length - 1
        '                Dim tmpParameter As QueryParameter = queryParameters(i)

        '                Dim comparerSymbolString As String = ""

        '                If Not i = 0 Then
        '                    sb.Append("AND ")

        '                End If

        '                sb.Append("(")

        '                Dim comparerString As String = [Enum].Format(GetType(mermaid.BaseObjects.Enums.QueryComparer), tmpParameter.Comparer, "f")

        '                If comparerString.IndexOf(mermaid.BaseObjects.Enums.QueryComparer.LessThan.ToString) <> -1 Then comparerSymbolString += "<"
        '                If comparerString.IndexOf(mermaid.BaseObjects.Enums.QueryComparer.GreaterThan.ToString) <> -1 Then comparerSymbolString += ">"
        '                If comparerString.IndexOf(mermaid.BaseObjects.Enums.QueryComparer.Equals.ToString) <> -1 Then comparerSymbolString += "="

        '                For y As Integer = 0 To tmpParameter.CompareValues.Length - 1
        '                    Dim tmpCompareValue As Object = tmpParameter.CompareValues(y)

        '                    If Not tmpCompareValue Is Nothing Then
        '                        If Not y = 0 Then
        '                            sb.Append("OR ")

        '                        End If

        '                        Me.ValidateColumn(tableName, tmpParameter.Parameter, tmpCompareValue.GetType)

        '                        'UNDONE: Commented
        '                        'sb.Append("[" & tmpParameter.Parameter & "] " & comparerSymbolString & " " & PersistenceConverter.ConvertToPersistenceValue(tmpCompareValue, tmpCompareValue.GetType) & " ")

        '                    End If

        '                Next

        '                sb.Append(") ")

        '            Next

        '        End If

        '        Dim databaseConnection As New SqlConnection(Me.DatabaseConnectionString)
        '        databaseConnection.Open()

        '        Dim commandObject As SqlCommand = New SqlCommand(sb.ToString, databaseConnection)

        '        Dim converter As New SqlDataAdapter
        '        converter.SelectCommand = commandObject

        '        If Me.PerformanceCountersEnabled Then
        '            DatabaseCallsPerformanceCounter.Increment()

        '        End If

        '        Dim tmpDataSet As New DataSet
        '        converter.Fill(tmpDataSet)
        '        converter.Dispose()
        '        databaseConnection.Close()
        '        commandObject.Dispose()
        '        databaseConnection.Dispose()


        '        Return tmpDataSet

        '    Finally

        '    End Try
        'End Function

        Public Function SendCommand(ByVal script As String) As DataSet
            Return Me.SendCommand(script, 300)

        End Function

        Public Function SendCommand(ByVal script As String, ByVal timeoutInSecs As Integer) As DataSet
            Using New Threading.Lock(Me.CommandLock, True)
                If Not Me.DatabaseValidated Then
                    Me.ValidateDatabase()
                    'Me.CreateRequiredObjects(Me.CommandDatabaseConnection)

                End If

                Try
                    'Using tmpConnnection As New SqlConnection(Me.DatabaseConnectionString)
                    '    tmpConnnection.Open()

                    Using tmpCommand As New SqlCommand(script, Me.CommandDatabaseConnection)
                        tmpCommand.CommandTimeout = timeoutInSecs

                        Using tmpAdapter As New SqlDataAdapter
                            Dim tmpDataSet As New DataSet

                            tmpAdapter.SelectCommand = tmpCommand

                            If Me.PerformanceCountersEnabled Then DatabaseCallsPerformanceCounter.Increment()
                            If Me.LogEnabled Then Me.LogCall("Command_", script)

                            tmpAdapter.Fill(tmpDataSet)

                            Return tmpDataSet

                        End Using
                    End Using
                    'End Using

                Catch ex As Exception
                    Me.ResetCommandDatabaseConnection()

                    Throw New Exception("Error sending command", ex)

                End Try

            End Using

        End Function

        Private Sub ValidateDatabase()
            If Me.AutoCreateDatabase Then
                Using tmpConnection As New SqlConnection(Me.DatabaseMasterConnectionString)
                    tmpConnection.Open()

                    DatabaseConnectionsOpenedPerformanceCounter.Increment()

                    'Check Database exists and create if not
                    Dim tmpScript As New StringBuilder

                    tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM master..sysdatabases WHERE Name = '" & Me.DatabaseName & "')")
                    tmpScript.AppendLine("BEGIN")
                    tmpScript.AppendLine("DECLARE @ssql varChar(255)")
                    tmpScript.AppendLine("SET @ssql = 'CREATE DATABASE " & Me.DatabaseName & "'")
                    tmpScript.AppendLine("EXEC(@ssql)")
                    tmpScript.AppendLine("EXEC sp_dboption '" & Me.DatabaseName & "', 'autoclose', 'FALSE'")
                    tmpScript.AppendLine("END")

                    Using tmpCommandObject As New SqlCommand(tmpScript.ToString, tmpConnection)
                        tmpCommandObject.ExecuteNonQuery()

                        Me.DatabaseValidated = True

                    End Using
                End Using

            Else
                Me.DatabaseValidated = True

            End If

        End Sub

        Private Sub CreateRequiredObjects(ByVal connection As SqlConnection)
            If Me.AutoCreateDatabase Then
                'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                '    tmpConnection.Open()

                Dim tmpScript As New StringBuilder

                'IDCOUNTER_SPNAME procedure
                tmpScript.AppendLine("IF EXISTS (SELECT name FROM sysobjects WHERE name = '" & IDCOUNTER_SPNAME & "' AND type = 'P')")
                tmpScript.AppendLine("DROP PROCEDURE " & IDCOUNTER_SPNAME)

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using

                tmpScript = New StringBuilder

                tmpScript.AppendLine("CREATE PROCEDURE " & IDCOUNTER_SPNAME & " (@assignedID int OUTPUT)")
                tmpScript.AppendLine("AS")
                tmpScript.AppendLine("SET NOCOUNT ON")
                tmpScript.AppendLine("INSERT INTO [" & IDCOUNTER_TABLENAME & "] VALUES('')")
                tmpScript.AppendLine()
                tmpScript.AppendLine("SELECT @assignedID = MAX(IDCount) FROM [" & IDCOUNTER_TABLENAME & "]")
                tmpScript.AppendLine("DELETE FROM [" & IDCOUNTER_TABLENAME & "] WHERE IDCount != @assignedID")
                tmpScript.AppendLine("SET NOCOUNT OFF")
                tmpScript.AppendLine("SELECT @assignedID")
                tmpScript.AppendLine("SET NOCOUNT ON")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using

                'IDCOUNTER_TABLENAME table
                tmpScript = New StringBuilder

                tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & Me.IDCOUNTER_TABLENAME & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("CREATE TABLE [" & Me.IDCOUNTER_TABLENAME & "] ([IDCount] int IDENTITY(" & 1000 & ",1), [Remarks] varChar(255))")
                tmpScript.AppendLine("END")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using
                'End Using

                'DBINFO_TABLENAME table
                tmpScript = New StringBuilder

                tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & Me.DBINFO_TABLENAME & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("  CREATE TABLE [" & Me.DBINFO_TABLENAME & "] ([Version] Int)")
                tmpScript.AppendLine("END")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using

                'DBUSERTABLES_TABLENAME table
                tmpScript = New StringBuilder

                tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & Me.DBUSERTABLES_TABLENAME & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("  CREATE TABLE [" & Me.DBUSERTABLES_TABLENAME & "] ([TableName] varChar(1000))")
                tmpScript.AppendLine("END")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using

                tmpScript = New StringBuilder

                tmpScript.AppendLine("DECLARE @rowCount Int")
                tmpScript.AppendLine("SELECT @rowCount = COUNT(*) FROM [" & Me.DBINFO_TABLENAME & "]")
                tmpScript.AppendLine()
                tmpScript.AppendLine("IF (@rowCount = 0)")
                tmpScript.AppendLine("  INSERT INTO [" & Me.DBINFO_TABLENAME & "]([Version]) VALUES(0)")

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                End Using

                Me.RequiredObjectsCreated = True

            Else
                Me.RequiredObjectsCreated = True

            End If

        End Sub

        Private Sub ValidateTable(ByVal connection As SqlConnection, ByVal tableName As String)
            'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
            '    tmpConnection.Open()

            'Check Table exists and create if not
            Dim tmpScript As New StringBuilder

            tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" & tableName & "')")
            tmpScript.AppendLine("BEGIN")
            tmpScript.AppendLine("CREATE TABLE [" & tableName & "]")
            tmpScript.AppendLine("(")
            tmpScript.AppendLine("SQLIdentity int IDENTITY PRIMARY KEY")
            tmpScript.AppendLine(")")
            tmpScript.AppendLine("END")

            Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                tmpCommand.ExecuteNonQuery()

            End Using

            Me.ValidatedTables(tableName) = True

            'End Using

        End Sub

        Private Sub ValidateColumns(ByVal connection As SqlConnection, ByVal data As ObjectData)
            'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
            '    tmpConnection.Open()

            Dim tmpScript As New StringBuilder

            For Each tmpProperty As ObjectProperty In data.Properties.Values
                Dim tmpAttributes As Dictionary(Of Type, Attribute) = Nothing

                If tmpProperty.Attributes.Count > 0 Then
                    tmpAttributes = New Dictionary(Of Type, Attribute)

                    For Each tmpAttribute As Attribute In tmpProperty.Attributes
                        tmpAttributes(tmpAttribute.GetType) = tmpAttribute

                    Next
                End If

                tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME='" & tmpProperty.PropertyName & "' and TABLE_NAME='" & data.Name & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("ALTER TABLE [" & data.Name & "] ADD [" & tmpProperty.PropertyName & "] " & Me.Layer.Converter.ConvertToPersistenceFormat(tmpProperty.PropertyType, tmpAttributes))
                tmpScript.AppendLine("END")

            Next

            Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                tmpCommand.ExecuteNonQuery()

            End Using

            Me.ValidatedColumns(data.Name) = True

            'End Using

        End Sub

        Private Sub ValidateColumn(ByVal connection As SqlConnection, ByVal tableName As String, ByVal columnName As String, ByVal columnType As Type)
            If Not Me.ValidatedColumns.ContainsKey(tableName & "|" & columnName) Then
                Dim tmpScript As New StringBuilder

                tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM [" & Me.DatabaseName & "].INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME='" & columnName & "' and TABLE_NAME='" & tableName & "')")
                tmpScript.AppendLine("BEGIN")
                tmpScript.AppendLine("ALTER TABLE [" & tableName & "] ADD [" & columnName & "] " & Me.Layer.Converter.ConvertToPersistenceFormat(columnType))
                tmpScript.AppendLine("END")

                'Using tmpConnection As New SqlConnection(Me.DatabaseConnectionString)
                '    tmpConnection.Open()

                Using tmpCommand As New SqlCommand(tmpScript.ToString, connection)
                    tmpCommand.ExecuteNonQuery()

                    Me.ValidatedColumnObjects(tableName & "|" & columnName) = True

                End Using
                'End Using

            End If

        End Sub

        Friend Sub ReleaseMemory()
            Me.CachedTables.Clear()

        End Sub

        Private Sub ResetPrimaryDatabaseConnection()
            Me._PrimaryDatabaseConnection = Nothing

        End Sub

        Private Sub ResetBatchDatabaseConnection()
            Me._BatchDatabaseConnection = Nothing

        End Sub

        Private Sub ResetCommandDatabaseConnection()
            Me._CommandDatabaseConnection = Nothing

        End Sub

        Private Sub LogCall(ByVal filePrefix As String, ByVal content As String)
            Try
                Dim tmpFileName As String = filePrefix & Date.Now.ToString("yyyyMMdd_HHmmss.fff") & ".sql"
                'tmpFileName &= Date.Now.Year
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Month)
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Day)
                'tmpFileName &= "_"
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Hour)
                ''tmpFileName &= ":"
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Minute)
                ''tmpFileName &= ":"
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 2, Date.Now.Second)
                ''tmpFileName &= "."
                'tmpFileName &= ConvertToFixedLength("0", Enums.FixedLengthConversionTypes.PrefixFillChar, 5, Date.Now.Millisecond)
                'tmpFileName &= ".sql"

                Dim tmpFilePath As String = Path.Combine(Path.Combine(My.Application.Info.DirectoryPath, "SQL Logs"), tmpFileName)

                IO.FileSystem.PrepareFilePath(tmpFilePath)

                File.WriteAllText(tmpFilePath, content)

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Me, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Persistence))

            End Try
        End Sub

    End Class

End Namespace

