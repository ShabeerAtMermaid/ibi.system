Imports System.ComponentModel
Imports System.Reflection
Imports System.Threading
Imports System.IO
Imports System.Text
Imports mermaid.BaseObjects.Persistence
Imports mermaid.BaseObjects.Persistence.Interfaces
Imports mermaid.BaseObjects.Persistence.Collections

Namespace Persistence

    Public Class Broker

#Region "Variables"

        Private Shared _ObjectIDDictionary As New Dictionary(Of Integer, IPersistentBaseObject)
        Private Shared _ObjectTypeDictionary As New Dictionary(Of Type, ObjectDictionary)
        Private Shared _Lock As New Object
        Private Shared _ThrowExceptions As Boolean = True
        Private Shared _InstantiationComplete As Boolean = True
        'Private Shared _CachedReferenceTables As New Hashtable

        Private Shared _FlushQueue As New Dictionary(Of Integer, IPersistentBaseObject)
        Private Shared _DisposeQueue As New Dictionary(Of Integer, IPersistentBaseObject)
        Private Shared _FlushLock As New Object

        Private Shared _FlushTimerStarted As Boolean
        Private Shared WithEvents _TmrFlush As Threading.Timer = New Threading.Timer(TimeSpan.FromSeconds(10))
        Private Shared WithEvents _TmrTestTransactions As Threading.Timer = New Threading.Timer(TimeSpan.FromSeconds(10))

        Private Shared _CachedPropertyDescriptors As New Dictionary(Of String, List(Of PropertyDescriptor))
        Private Shared _CachedPropertyDescriptorCollections As New Dictionary(Of Type, PropertyDescriptorCollection)
        Private Shared _CachedAssemblies As New Dictionary(Of String, Assembly)

        Private Shared _PerformanceCountersEnabled As Boolean

#End Region

#Region "Properties"

        Private Shared ReadOnly Property ObjectIDDictionary() As Dictionary(Of Integer, IPersistentBaseObject)
            Get
                Return _ObjectIDDictionary

            End Get
        End Property

        Private Shared ReadOnly Property ObjectTypeDictionary() As Dictionary(Of Type, ObjectDictionary)
            Get
                Return _ObjectTypeDictionary

            End Get
        End Property

        Private Shared ReadOnly Property Lock() As Object
            Get
                Return _Lock

            End Get
        End Property

        Public Shared Property ThrowExceptions() As Boolean
            Get
                Return _ThrowExceptions

            End Get
            Set(ByVal Value As Boolean)
                _ThrowExceptions = Value

            End Set
        End Property

        Public Shared ReadOnly Property InstantiationComplete() As Boolean
            Get
                Return _InstantiationComplete

            End Get
        End Property

        'Private Shared ReadOnly Property CachedReferenceTables() As Hashtable
        '    Get
        '        Return _CachedReferenceTables

        '    End Get
        'End Property

        Private Shared ReadOnly Property CachedPropertyDescriptors() As Dictionary(Of String, List(Of PropertyDescriptor))
            Get
                Return _CachedPropertyDescriptors

            End Get
        End Property

        Private Shared ReadOnly Property CachedPropertyDescriptorCollections() As Dictionary(Of Type, PropertyDescriptorCollection)
            Get
                Return _CachedPropertyDescriptorCollections

            End Get
        End Property

        Private Shared ReadOnly Property CachedAssemblies() As Dictionary(Of String, Assembly)
            Get
                Return _CachedAssemblies

            End Get
        End Property

        Private Shared ReadOnly Property FlushQueue() As Dictionary(Of Integer, IPersistentBaseObject)
            Get
                Return _FlushQueue

            End Get
        End Property

        Private Shared ReadOnly Property DisposeQueue() As Dictionary(Of Integer, IPersistentBaseObject)
            Get
                Return _DisposeQueue

            End Get
        End Property

        Private Shared ReadOnly Property FlushLock() As Object
            Get
                Return _FlushLock

            End Get
        End Property

        Public Shared Property FlushInterval() As TimeSpan
            Get
                Return TmrFlush.Interval

            End Get
            Set(ByVal value As TimeSpan)

                TmrFlush.Interval = value

            End Set
        End Property

        Private Shared ReadOnly Property TmrFlush() As Threading.Timer
            Get
                Return _TmrFlush

            End Get
        End Property

        Public Shared Property PerformanceCountersEnabled() As Boolean
            Get
                Return _PerformanceCountersEnabled

            End Get
            Set(ByVal value As Boolean)
                _PerformanceCountersEnabled = value

            End Set
        End Property

        Public Shared ReadOnly Property PersistenceObjectsCreatedPerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Persistent Objects created")

            End Get
        End Property

        Public Shared ReadOnly Property PersistenceFlushQueueSizePerformanceCounter() As Diagnostics.PerformanceCounter
            Get
                Return Diagnostics.PerformanceCounterManager.GetPerformanceCounter("Persistent Flush Queue size")

            End Get
        End Property

#End Region

#Region "Events"

        Public Shared Event BrokerInstatiated As EventHandler
        Public Shared Event BeforeInstantiating As System.EventHandler

#End Region

        Public Shared Sub SaveObject(ByVal saveValue As IPersistentBaseObject)
            If PersistenceSettings.PersistenceEnabled And saveValue.PersistenceEnabled And Not saveValue.IsDisposed Then
                If saveValue.InstanceID > 0 Then
                    Using New Threading.Lock(FlushLock, True)
                        If Not _FlushTimerStarted Then
                            TmrFlush.Enabled = True
                            _FlushTimerStarted = True
                            'TEST
                            _TmrTestTransactions.Enabled = True

                        End If

                        FlushQueue(saveValue.InstanceID) = saveValue

                        If PerformanceCountersEnabled Then
                            PersistenceFlushQueueSizePerformanceCounter.Increment()
                        End If

                        'TmrFlush.Enabled = True

                    End Using

                Else
                    SendToProxy(saveValue)

                End If
            End If

        End Sub

        Private Shared Sub SendToProxy(ByVal saveValue As IPersistentBaseObject)
            Try
                Using New Threading.Lock(Lock, True)
                    Dim tmpSaveList As List(Of ObjectData) = GetObjectData(saveValue)

                    Dim tmpInstanceID As Integer = saveValue.InstanceID

                    If tmpInstanceID = 0 Then
                        If tmpSaveList.Count > 0 Then
                            Dim tmpObjectdata As ObjectData = tmpSaveList(0)

                            'Save the ObjectData and gets the InstanceID of the value
                            tmpInstanceID = tmpObjectdata.Layer.Proxy.SaveData(tmpObjectdata)

                            tmpSaveList.RemoveAt(0)

                        End If
                    End If

                    'For Each tmpObjectData As ObjectData In tmpSaveList
                    '    ''If a collection is to be saved
                    '    'If tmpCollectionObjectDataList.Count > 0 Then
                    '    '    'If new object, update container columns in collections
                    '    '    If saveValue.InstanceID = 0 Then
                    '    '        'Iterate through the items from the collection to be added
                    '    '        For Each tmpCollectionObjectData As ObjectData In tmpCollectionObjectDataList
                    '    '            tmpCollectionObjectData.Properties("Container").PropertyValue = tmpInstanceID

                    '    '        Next
                    '    '    End If

                    '    '    tmpLayer.Proxy.SaveData(tmpCollectionObjectDataList)

                    '    'End If

                    'Next

                    'UNDONE: Only support default layer
                    If tmpSaveList.Count > 0 Then
                        PersistenceSettings.DefaultPersistenceLayer.Proxy.SaveData(tmpSaveList)

                    End If

                    'If the value has no InstanceID, assign the id returned from the Bridge
                    If saveValue.InstanceID = 0 Then
                        If tmpInstanceID <> 0 Then
                            saveValue.InstanceID = tmpInstanceID

                            If PerformanceCountersEnabled Then
                                PersistenceObjectsCreatedPerformanceCounter.Increment()

                            End If
                        Else
                            Throw New Exception("Could not save object. No InstanceID returned from Proxy")
                        End If
                    End If

                End Using

            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                If ThrowExceptions Then Throw tmpException

            End Try
        End Sub

        Private Shared Function GetObjectData(ByVal saveValue As IPersistentBaseObject) As List(Of ObjectData)

            Dim tmpResult As New List(Of ObjectData)

            Try


                Dim tmpLayer As PersistenceLayer = Nothing

                Dim tmpPersistentClassAttribute As PersistentClassAttribute = TypeDescriptor.GetAttributes(saveValue).Item(GetType(PersistentClassAttribute))

                If Not tmpPersistentClassAttribute Is Nothing Then tmpLayer = PersistenceSettings.GetPersistenceLayer(tmpPersistentClassAttribute.PersistenceLayerKey)

                If tmpLayer Is Nothing Then tmpLayer = PersistenceSettings.DefaultPersistenceLayer

                If Not tmpLayer Is Nothing Then
                    Dim src_pdc As PropertyDescriptorCollection = GetPropertyDescriptorCollection(saveValue)

                    'Gets the class name
                    Dim objectName As String = TypeDescriptor.GetClassName(saveValue)

                    'Declare temp variables
                    Dim tmpObjectProperty As ObjectProperty
                    Dim tmpPersistentPropertyAttribute As PersistentPropertyAttribute
                    Dim tmpName As String
                    Dim tmpValue As Object
                    Dim tmpType As Type
                    Dim tmpAttributes As List(Of Attribute)

                    'Create new ObjectData to store all the information to be saved in
                    Dim tmpObjectData As New ObjectData(tmpLayer, saveValue.InstanceID, objectName)

                    tmpResult.Add(tmpObjectData)

                    'Gets the name of the assembly the value belongs to
                    Dim tmpAssemblyName As String = Path.GetFileName(saveValue.GetType.Assembly.Location)

                    'Create ObjectProperty for the AssemblyName
                    tmpObjectProperty = New ObjectProperty("Assembly", tmpAssemblyName, GetType(String))
                    tmpObjectData.Properties("assembly") = tmpObjectProperty

                    'Create ObjectProperty for the Class name
                    tmpObjectProperty = New ObjectProperty("ObjectName", objectName, GetType(String))
                    tmpObjectData.Properties("objectname") = tmpObjectProperty

                    'Iterate through all public properties of the value
                    For Each tmpPropertyDescriptor As System.ComponentModel.PropertyDescriptor In src_pdc
                        Thread.Sleep(10)
                        'Looks for the CustomAttribute PersistentAttribute
                        tmpPersistentPropertyAttribute = tmpPropertyDescriptor.Attributes(GetType(PersistentPropertyAttribute))

                        'If the attribure is found
                        If Not tmpPersistentPropertyAttribute Is Nothing Then
                            'Gets the name, value and type of the property
                            tmpName = tmpPropertyDescriptor.Name()
                            tmpValue = tmpPropertyDescriptor.GetValue(saveValue)
                            tmpType = tmpPropertyDescriptor.PropertyType

                            'If the attribute specifies the type to save the property as
                            'If Not tmpPersistentPropertyAttribute.SaveType Is Nothing Then
                            '    tmpType = tmpPersistentPropertyAttribute.SaveType

                            'End If

                            'Get all attributes of the property
                            Dim tmpAttributeSource(tmpPropertyDescriptor.Attributes.Count - 1) As Attribute
                            tmpPropertyDescriptor.Attributes.CopyTo(tmpAttributeSource, 0)

                            tmpAttributes = New List(Of Attribute)
                            tmpAttributes.AddRange(tmpAttributeSource)

                            If Array.IndexOf(tmpType.GetInterfaces(), GetType(mermaid.BaseObjects.Persistence.Interfaces.IPersistentBaseObject)) <> -1 Then
                                'If not the value of the property is Nothing
                                If Not tmpValue Is Nothing Then
                                    'Only save the InstanceID of the value
                                    tmpValue = CType(tmpValue, IPersistentBaseObject).InstanceID

                                Else
                                    tmpValue = 0

                                End If

                                tmpType = GetType(Integer)

                                'Create ObjectProperty for the InstanceID and add "_REF" to the property name to indicate it's a reference
                                tmpObjectProperty = New ObjectProperty(tmpName & "_REF", tmpValue, tmpType, tmpAttributes)

                                'Add the ObjectProperty to the ObjectData
                                tmpObjectData.Properties(tmpObjectProperty.PropertyName) = tmpObjectProperty

                            ElseIf IsTypeOfBaseList(tmpType) Then
                                If Not tmpValue Is Nothing Then
                                    Dim tmpValueType As Type = GetBaseListValueType(tmpValue)

                                    Dim tmpDefinition As PersistentListDefinitionAttribute = tmpPropertyDescriptor.Attributes(GetType(PersistentListDefinitionAttribute))

                                    If Not tmpDefinition Is Nothing Then
                                        tmpValueType = tmpDefinition.ValueType

                                    End If

                                    Dim tmpSafeList As Object = tmpValue.Clone

                                    'If the collection isn't created but updated
                                    If saveValue.InstanceID > 0 Then
                                        'Deletes all entries in the database
                                        'tmpLayer.Proxy.ClearCollection(objectName & "_" & tmpName & "_CREF", saveValue.InstanceID)
                                        Dim tmpClearCollectionData As New ObjectData(tmpLayer, saveValue.InstanceID, objectName & "_" & tmpName & "_CREF")
                                        tmpClearCollectionData.IsClearListCommand = True

                                        tmpResult.Add(tmpClearCollectionData)

                                    End If

                                    For i As Integer = 0 To tmpSafeList.Count - 1
                                        Thread.Sleep(10)
                                        Dim tmpCollectionItem As Object = tmpSafeList(i)

                                        'Gets the class name of the item
                                        Dim collObjectName As String = TypeDescriptor.GetClassName(tmpCollectionItem)

                                        'Create ObjectData to store the item in
                                        Dim tmpCollObjectData As ObjectData = New ObjectData(tmpLayer, -1, objectName & "_" & tmpName & "_CREF")
                                        tmpCollObjectData.IsListEntry = True

                                        Dim tmpCollObjectProperty As ObjectProperty

                                        'Gets the name of the assembly the item belongs to
                                        Dim collAssemblyName As String = Path.GetFileName(tmpCollectionItem.GetType.Assembly.Location)

                                        'Create ObjectProperty for the AssemblyName
                                        tmpCollObjectProperty = New ObjectProperty("Assembly", collAssemblyName, GetType(String))
                                        tmpCollObjectData.Properties("assembly") = tmpCollObjectProperty

                                        'Create ObjectProperty for the ClassName
                                        tmpCollObjectProperty = New ObjectProperty("ObjectName", collObjectName, GetType(String))
                                        tmpCollObjectData.Properties("objectname") = tmpCollObjectProperty

                                        'Create ObjectProperty for the InstanceID of the collection that holds the item
                                        tmpCollObjectProperty = New ObjectProperty("Container", saveValue.InstanceID, GetType(Integer))
                                        tmpCollObjectData.Properties("container") = tmpCollObjectProperty

                                        'The item in the collection is a persistentBaseObject 
                                        If Array.IndexOf(tmpValueType.GetInterfaces(), GetType(mermaid.BaseObjects.Persistence.Interfaces.IPersistentBaseObject)) <> -1 Then
                                            'Create ObjectProperty for the InstanceID of the item
                                            tmpCollObjectProperty = New ObjectProperty("InstanceID", tmpCollectionItem.InstanceID, GetType(Integer))
                                            tmpCollObjectData.Properties("instanceid") = tmpCollObjectProperty

                                            'Create ObjectProperty for the item (Only used if the item is a simple datatype)
                                            tmpCollObjectProperty = New ObjectProperty("Value", "", GetType(String))
                                            tmpCollObjectData.Properties("value") = tmpCollObjectProperty

                                        Else
                                            'Create ObjectProperty for the InstanceID of the item
                                            tmpCollObjectProperty = New ObjectProperty("InstanceID", 0, GetType(Integer))
                                            tmpCollObjectData.Properties("instanceid") = tmpCollObjectProperty

                                            'Create ObjectProperty for the item (Only used if the item is a simple datatype)
                                            tmpCollObjectProperty = New ObjectProperty("Value", tmpCollectionItem, tmpValueType)
                                            tmpCollObjectData.Properties("value") = tmpCollObjectProperty

                                        End If

                                        'Create ObjectProperty for the key
                                        tmpCollObjectProperty = New ObjectProperty("Key", "", GetType(String))
                                        tmpCollObjectData.Properties("key") = tmpCollObjectProperty

                                        'Create ObjectProperty for the Index
                                        tmpCollObjectProperty = New ObjectProperty("Index", i, GetType(Integer))
                                        tmpCollObjectData.Properties("index") = tmpCollObjectProperty

                                        'Adds the ObjectData to the list of items to be saved
                                        tmpResult.Add(tmpCollObjectData)

                                    Next

                                    tmpSafeList.Clear()

                                End If

                            ElseIf IsTypeOfBaseDictionary(tmpType) Then
                                If Not tmpValue Is Nothing Then
                                    Dim tmpKeyType As Type = GetBaseDictionaryKeyType(tmpValue)
                                    Dim tmpValueType As Type = GetBaseDictionaryValueType(tmpValue)

                                    Dim tmpDefinition As PersistentDictionaryDefinitionAttribute = tmpPropertyDescriptor.Attributes(GetType(PersistentDictionaryDefinitionAttribute))

                                    If Not tmpDefinition Is Nothing Then
                                        tmpKeyType = tmpDefinition.KeyType
                                        tmpValueType = tmpDefinition.ValueType

                                    End If

                                    Dim tmpSafeDictionary As Object = tmpValue.Clone

                                    'If the collection isn't created but updated
                                    If saveValue.InstanceID > 0 Then
                                        'Deletes all entries in the database
                                        'tmpLayer.Proxy.ClearCollection(objectName & "_" & tmpName & "_CREF", saveValue.InstanceID)
                                        Dim tmpClearCollectionData As New ObjectData(tmpLayer, saveValue.InstanceID, objectName & "_" & tmpName & "_CREF")
                                        tmpClearCollectionData.IsClearListCommand = True

                                        tmpResult.Add(tmpClearCollectionData)

                                    End If

                                    For Each tmpDictionaryEntry As Object In tmpSafeDictionary
                                        Thread.Sleep(10)


                                        'Gets the class name of the item
                                        Dim collObjectName As String = TypeDescriptor.GetClassName(tmpDictionaryEntry.Value)

                                        'Create ObjectData to store the item in
                                        Dim tmpCollObjectData As ObjectData = New ObjectData(tmpLayer, -1, objectName & "_" & tmpName & "_CREF")
                                        tmpCollObjectData.IsListEntry = True

                                        Dim tmpCollObjectProperty As ObjectProperty

                                        'Gets the name of the assembly the item belongs to
                                        Dim collAssemblyName As String = Path.GetFileName(tmpDictionaryEntry.Value.GetType.Assembly.Location)

                                        'Create ObjectProperty for the AssemblyName
                                        tmpCollObjectProperty = New ObjectProperty("Assembly", collAssemblyName, GetType(String))
                                        tmpCollObjectData.Properties("assembly") = tmpCollObjectProperty

                                        'Create ObjectProperty for the ClassName
                                        tmpCollObjectProperty = New ObjectProperty("ObjectName", collObjectName, GetType(String))
                                        tmpCollObjectData.Properties("objectname") = tmpCollObjectProperty

                                        'Create ObjectProperty for the InstanceID of the collection that holds the item
                                        tmpCollObjectProperty = New ObjectProperty("Container", saveValue.InstanceID, GetType(Integer))
                                        tmpCollObjectData.Properties("container") = tmpCollObjectProperty

                                        'The item in the collection is a persistentBaseObject 
                                        If Array.IndexOf(tmpValueType.GetInterfaces(), GetType(mermaid.BaseObjects.Persistence.Interfaces.IPersistentBaseObject)) <> -1 Then
                                            'Create ObjectProperty for the InstanceID of the item
                                            tmpCollObjectProperty = New ObjectProperty("InstanceID", tmpDictionaryEntry.Value.InstanceID, GetType(Integer))
                                            tmpCollObjectData.Properties("instanceid") = tmpCollObjectProperty

                                            'Create ObjectProperty for the item (Only used if the item is a simple datatype)
                                            tmpCollObjectProperty = New ObjectProperty("Value", "", GetType(String))
                                            tmpCollObjectData.Properties("value") = tmpCollObjectProperty

                                        Else
                                            'Create ObjectProperty for the InstanceID of the item
                                            tmpCollObjectProperty = New ObjectProperty("InstanceID", 0, GetType(Integer))
                                            tmpCollObjectData.Properties("instanceid") = tmpCollObjectProperty

                                            If tmpValueType Is GetType(String) Then
                                                Dim tmpValueAttributeList As List(Of Attribute) = New List(Of Attribute)
                                                tmpValueAttributeList.Add(New StringLengthAttribute(5000))

                                                'Create ObjectProperty for the item (Only used if the item is a simple datatype)
                                                tmpCollObjectProperty = New ObjectProperty("Value", tmpDictionaryEntry.Value, tmpValueType, tmpValueAttributeList)
                                                tmpCollObjectData.Properties("value") = tmpCollObjectProperty

                                            Else
                                                'Create ObjectProperty for the item (Only used if the item is a simple datatype)
                                                tmpCollObjectProperty = New ObjectProperty("Value", tmpDictionaryEntry.Value, tmpValueType)
                                                tmpCollObjectData.Properties("value") = tmpCollObjectProperty

                                            End If
                                        End If

                                        'Create ObjectProperty for the key
                                        tmpCollObjectProperty = New ObjectProperty("Key", tmpDictionaryEntry.Key, tmpKeyType)
                                        tmpCollObjectData.Properties("key") = tmpCollObjectProperty

                                        'Create ObjectProperty for the Index
                                        tmpCollObjectProperty = New ObjectProperty("Index", 0, GetType(Integer))
                                        tmpCollObjectData.Properties("index") = tmpCollObjectProperty

                                        'Adds the ObjectData to the list of items to be saved
                                        tmpResult.Add(tmpCollObjectData)

                                    Next

                                    tmpSafeDictionary.Clear()

                                End If

                            Else
                                'Check whether the type is supported by the layer
                                If tmpLayer.Converter.IsTypeSupported(tmpType) Then
                                    'Add the ObjectProperty to the ObjectData
                                    tmpObjectProperty = New ObjectProperty(tmpName, tmpValue, tmpType, tmpAttributes)
                                    tmpObjectData.Properties(tmpName) = tmpObjectProperty

                                Else
                                    Dim tmpException As New Exception("Type """ & tmpType.Name & """ is not supported by the " & tmpLayer.GetType().Name)

                                    BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                                End If
                            End If

                        End If
                    Next

                Else
                    Dim tmpException As New Exception("No PersistenceLayer defined for type """ & saveValue.GetType.FullName & """ and no Default PersistenceLayer defined")

                    BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                    If ThrowExceptions Then Throw tmpException

                End If
            Catch ex As Exception
                Dim tmpException As New Exception("No PersistenceLayer defined for type """ & saveValue.GetType.FullName & """ and no Default PersistenceLayer defined")
                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))
                If ThrowExceptions Then Throw ex
            End Try

            Return tmpResult

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Instantiates all PersistentObjects from the database.
        ''' </summary>
        ''' <remarks>
        '''     Object references is also instantiated and linked.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Shared Sub InstantiateAll()
            _InstantiationComplete = False

            Try
                'Temporarely save whether Persistence is enabled
                Dim tmpPersistenceEnabled As Boolean = PersistenceSettings.PersistenceEnabled

                'Deactivate persistence while objects are being instantiated
                PersistenceSettings.PersistenceEnabled = False

                Dim tmpInstantiateRetries As Integer
                Dim tmpPersistenceLayerReady As Boolean
                Dim tmpAbortInstantiate As Boolean

                For Each tmpPersistenceLayer As PersistenceLayer In PersistenceSettings.PersistenceLayers
                    tmpInstantiateRetries = 0
                    tmpPersistenceLayerReady = False
                    tmpAbortInstantiate = True

                    While Not tmpPersistenceLayerReady And tmpInstantiateRetries < tmpPersistenceLayer.InstantiateRetries And tmpPersistenceLayer.Instantiate
                        tmpPersistenceLayerReady = tmpPersistenceLayer.PersistenceLayerReady

                        tmpInstantiateRetries += 1

                        If tmpPersistenceLayerReady Then
                            tmpAbortInstantiate = False

                        Else
                            If tmpInstantiateRetries < tmpPersistenceLayer.InstantiateRetries Then
                                Dim tmpRetryInterval As TimeSpan = New TimeSpan(0, 1, 0)

                                Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(tmpPersistenceLayer, True, tmpRetryInterval)

                                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                                Thread.Sleep(tmpRetryInterval)

                            Else
                                Dim tmpException As New Exceptions.PersistenceLayerNotReadyException(tmpPersistenceLayer, False, New TimeSpan(0))

                                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                            End If
                        End If


                    End While

                    If Not tmpAbortInstantiate Then
                        OnBeforeInstantiating()

                        'Get all Object data from the Proxy
                        Dim tmpData As Dictionary(Of String, Dictionary(Of Integer, ObjectData)) = tmpPersistenceLayer.Proxy.GetObjectData()

                        'Iterate through all the data
                        For Each tmpObjectTypeEntry As KeyValuePair(Of String, Dictionary(Of Integer, ObjectData)) In tmpData
                            Dim tmpAssemblyName As String
                            Dim tmpTypeName As String
                            Dim tmpAssembly As [Assembly]

                            If tmpObjectTypeEntry.Value.Count > 0 Then
                                Dim tmpObjectData As ObjectData = tmpObjectTypeEntry.Value.Values.First

                                tmpAssemblyName = tmpObjectData.Properties("assembly").PropertyValue
                                tmpTypeName = tmpObjectData.Properties("objectname").PropertyValue

                                Try
                                    'Gets the assembly and type/class name
                                    'Load the assembly that stores the class
                                    tmpAssembly = GetAssembly(tmpAssemblyName)

                                    'Gets the type of the object to instantiate
                                    Dim tmpType As Type = tmpAssembly.GetType(tmpTypeName)

                                    If Not tmpType Is Nothing Then
                                        'Create a Dictionary to save the objects in
                                        Dim tmpObjectDictionary As ObjectDictionary = Nothing

                                        If ObjectTypeDictionary.ContainsKey(tmpType) Then tmpObjectDictionary = ObjectTypeDictionary(tmpType)

                                        If tmpObjectDictionary Is Nothing Then
                                            tmpObjectDictionary = New ObjectDictionary(tmpType, tmpObjectTypeEntry.Value)
                                            ObjectTypeDictionary(tmpType) = tmpObjectDictionary

                                        End If

                                        'Iterate through all data in the datatype
                                        For Each tmpObjectDataEntry As KeyValuePair(Of Integer, ObjectData) In tmpObjectTypeEntry.Value
                                            tmpObjectData = tmpObjectDataEntry.Value

                                            Try
                                                Dim tmpObject As IPersistentBaseObject = Nothing

                                                'Check which constructor to use
                                                If HasPrivatePersistenceConstructor(tmpType) Then
                                                    'Dim args(0) As Object
                                                    ''Create an instance of the object
                                                    'args(0) = New PersistentObjectData

                                                    tmpObject = Activator.CreateInstance(tmpType, True)

                                                ElseIf HasPublicPersistenceConstructor(tmpType) Then
                                                    Dim args(0) As Object
                                                    'Create an instance of the object
                                                    args(0) = True

                                                    tmpObject = Activator.CreateInstance(tmpType, args)

                                                Else
                                                    'We could not find neither a private or public constructor that matches the requirements
                                                    BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(New Exception("Could not find a mathing constructor for type """ & tmpType.FullName & """."), Enums.ExceptionCategories.PerformanceCounters))

                                                End If

                                                If Not tmpObject Is Nothing Then
                                                    'Gets a PropertyDescriptor for the object
                                                    Dim src_pdc As PropertyDescriptorCollection = GetPropertyDescriptorCollection(tmpObject)

                                                    'Iterate through all the public properties of the object
                                                    For i As Integer = 0 To src_pdc.Count - 1
                                                        Try
                                                            'Gets the property type and name
                                                            Dim tmpPropertyType As Type = src_pdc(i).PropertyType
                                                            Dim tmpPropertyName As String = src_pdc(i).Name

                                                            If tmpObjectData.Properties.ContainsKey(tmpPropertyName.ToLower) Then
                                                                Dim tmpValue As Object = tmpPersistenceLayer.Converter.ConvertFromPersistenceValue(tmpObjectData.Properties(tmpPropertyName.ToLower).PropertyValue, tmpPropertyType)

                                                                If Not tmpValue Is Nothing Then
                                                                    src_pdc(i).SetValue(tmpObject, tmpValue)

                                                                End If
                                                            End If
                                                        Catch ex As Exception
                                                            'SILENT

                                                        End Try

                                                    Next
                                                    'Adds the object to the ObjectDictionary
                                                    Dim id As Integer = tmpObject.InstanceID

                                                    If id > 0 Then
                                                        tmpObjectDictionary.ObjectDictionary(id) = tmpObject
                                                        ObjectIDDictionary(id) = tmpObject

                                                    End If
                                                End If

                                            Catch ex As Exception
                                                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                                                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                                            End Try

                                        Next
                                    End If

                                Catch ex As Exception
                                    Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                                    BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                                End Try

                            End If

                        Next
                    End If

                Next
                'Sets all object references in the instantiated objects (non simple datatypes)
                SetObjectReferences()

                OnBrokerInstantiated()

                ''Clears all temp data used for instantiation
                'ObjectTypeDictionary.Clear()
                'ObjectIDDictionary.Clear()

                'Sets the PersistenceEnabled property back to its original value
                PersistenceSettings.PersistenceEnabled = tmpPersistenceEnabled
            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)
                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))
                If ThrowExceptions Then Throw tmpException

            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Sets all object references on the instantiated objects.
        ''' </summary>
        ''' <remarks>
        '''     This method is called from InstantiateAll().
        ''' </remarks>
        ''' <history>
        ''' 	[peter]	20-12-2004	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Shared Sub SetObjectReferences()
            Using tmpAnalyzer As New Diagnostics.PerformanceAnalyzer("Broker.SetObjectReferences")
                Try
                    'List of Properties that is IPersistentList and must be filled
                    Dim tmpLists As New Dictionary(Of Type, ListData)
                    'Dim tmpDictionaries As New Dictionary(Of Type, ListData)

                    Dim tmpAssemblyName As String
                    Dim tmpAssembly As [Assembly]
                    Dim tmpPropertyType As Type
                    Dim tmpPropertyName As String = String.Empty

                    'Iterate through all the ObjectLists instantiated
                    For Each tmpObjectTypeDictionaryEntry As KeyValuePair(Of Type, ObjectDictionary) In ObjectTypeDictionary
                        Dim tmpObjectDictionary As ObjectDictionary = tmpObjectTypeDictionaryEntry.Value

                        'If objects has been instantiated
                        If tmpObjectDictionary.ObjectDictionary.Values.Count > 0 Then
                            Try

                                'Iterate through the data of the datatype
                                For Each tmpObject As IPersistentBaseObject In tmpObjectDictionary.ObjectDictionary.Values
                                    Try
                                        'Gets the InstanceID of the object
                                        Dim tmpID As Integer = tmpObject.InstanceID

                                        'Gets a PropertyDescriptor for the public properties of the object
                                        Dim tmpPropertyList As System.Collections.Generic.List(Of PropertyDescriptor) = GetPropertyDescriptors(tmpObject.GetType)

                                        'Iterate through the objects properties
                                        For Each tmpPropertyDescriptor As PropertyDescriptor In tmpPropertyList
                                            Dim tmpReferenceID As Integer = 0
                                            Try
                                                If tmpPropertyDescriptor.IsReferenceProperty Then
                                                    'Gets the property type and name
                                                    tmpPropertyType = tmpPropertyDescriptor.PropertyType
                                                    tmpPropertyName = tmpPropertyDescriptor.PropertyName

                                                    Dim tmpPersistentPropertyAttribute As PersistentPropertyAttribute = tmpPropertyDescriptor.PersistentPropertyAttribute

                                                    'If the property is a persistenBaseObject
                                                    If Array.IndexOf(tmpPropertyType.GetInterfaces(), GetType(mermaid.BaseObjects.Persistence.Interfaces.IPersistentBaseObject)) <> -1 Then
                                                        'Gets the InstanceID of the referenced object

                                                        If tmpObjectDictionary.SourceDictionary.ContainsKey(tmpID) Then
                                                            If tmpObjectDictionary.SourceDictionary(tmpID).Properties.ContainsKey(tmpPropertyName.ToLower & "_ref") Then
                                                                tmpReferenceID = 0

                                                                'Sets the value from the data source
                                                                tmpReferenceID = tmpObjectDictionary.SourceDictionary(tmpID).Properties(tmpPropertyName.ToLower & "_ref").PropertyValue

                                                                If tmpReferenceID > 0 Then
                                                                    Dim tmpReferenceValue As Object = Nothing

                                                                    'Sets the value of the property
                                                                    If ObjectIDDictionary.ContainsKey(tmpReferenceID) Then tmpReferenceValue = ObjectIDDictionary(tmpReferenceID)

                                                                    tmpPropertyDescriptor.PropertyInfo.SetValue(tmpObject, tmpReferenceValue)

                                                                End If
                                                            End If
                                                        End If

                                                        'If the property is a PersistentList
                                                    ElseIf IsTypeOfBaseList(tmpPropertyType) Then
                                                        'This is a PersistentList and must be filled as the last property
                                                        If Not tmpLists.ContainsKey(tmpObject.GetType) Then tmpLists(tmpObject.GetType) = New ListData(tmpObject.GetType, tmpPropertyDescriptor)

                                                        'Else if the property is a PersistentDictionary
                                                    ElseIf IsTypeOfBaseDictionary(tmpPropertyType) Then
                                                        'This is a PersistentDictionary and must be filled as the last property
                                                        If Not tmpLists.ContainsKey(tmpObject.GetType) Then tmpLists(tmpObject.GetType) = New ListData(tmpObject.GetType, tmpPropertyDescriptor)

                                                    End If
                                                End If

                                            Catch ex As Exception
                                                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                                                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence, tmpObject.GetType.ToString & "." & tmpPropertyName & " RefID: " & tmpReferenceID))

                                            End Try
                                        Next

                                    Catch ex As Exception
                                        Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                                        BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                                    End Try
                                Next

                            Catch ex As Exception
                                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                            End Try

                        End If
                    Next

                    tmpAnalyzer.AddPerformanceMark("Settings references")

                    'Fill all lists
                    For Each tmpListData As ListData In tmpLists.Values

                        Dim tmpLayer As PersistenceLayer = Nothing

                        Dim tmpPersistentClassAttribute As PersistentClassAttribute = TypeDescriptor.GetAttributes(tmpListData.ContainerType).Item(GetType(PersistentClassAttribute))

                        If Not tmpPersistentClassAttribute Is Nothing Then tmpLayer = PersistenceSettings.GetPersistenceLayer(tmpPersistentClassAttribute.PersistenceLayerKey)

                        If tmpLayer Is Nothing Then tmpLayer = PersistenceSettings.DefaultPersistenceLayer

                        If Not tmpLayer Is Nothing Then

                            Thread.Sleep(20)
                            Dim tmpListName As String = tmpListData.ContainerType.FullName & "_" & tmpListData.PropertyDescriptor.PropertyName & "_CREF"

                            Dim tmpListEntryDictionary As Dictionary(Of Integer, ObjectData) = tmpLayer.Proxy.GetObjectData(tmpListName)

                            tmpPropertyType = tmpListData.PropertyDescriptor.PropertyType
                            tmpPropertyName = tmpListData.PropertyDescriptor.PropertyName

                            Dim tmpListObject As Object

                            If Not tmpListEntryDictionary Is Nothing Then
                                'Iterate through the entries in the collection
                                For Each tmpObjectData As ObjectData In tmpListEntryDictionary.Values
                                    Dim tmpListTypeName As String

                                    Try
                                        tmpListObject = Nothing

                                        'Gets the assembly name and class name
                                        tmpAssemblyName = tmpObjectData.Properties("assembly").PropertyValue
                                        tmpListTypeName = tmpObjectData.Properties("objectname").PropertyValue

                                        'Gets the InstanceID of the object containing the collection
                                        Dim tmpContainerID As Integer = tmpObjectData.Properties("container").PropertyValue
                                        Dim tmpContainer As Object = ObjectIDDictionary(tmpContainerID)

                                        'If the entry isn't a simple datatype
                                        Dim tmpListType As Type = Type.GetType(tmpListTypeName)

                                        If tmpListType Is Nothing Then
                                            If File.Exists(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, tmpAssemblyName)) Then
                                                tmpAssembly = [Assembly].LoadFrom(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, tmpAssemblyName))

                                                tmpListType = tmpAssembly.GetType(tmpListTypeName)

                                            End If

                                        End If

                                        If Not tmpListType Is Nothing Then
                                            If Array.IndexOf(tmpListType.GetInterfaces(), GetType(mermaid.BaseObjects.Persistence.Interfaces.IPersistentBaseObject)) <> -1 Then
                                                'Gets the type of the object to reference
                                                Dim tmpListObjectType As Type = tmpAssembly.GetType(tmpListTypeName)

                                                Dim tmpInstanceID As Integer = tmpObjectData.Properties("instanceid").PropertyValue

                                                'Gets the object of the specified type with the specified InstanceID
                                                If ObjectIDDictionary.ContainsKey(tmpInstanceID) Then tmpListObject = ObjectIDDictionary(tmpInstanceID)

                                                'If the object has been found
                                                If Not tmpListObject Is Nothing Then
                                                    'If the property is a PersistentList
                                                    If IsTypeOfBaseList(tmpPropertyType) Then
                                                        Dim tmpList As Object = tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer)

                                                        'Insert the entry in the ArrayList at the specified index
                                                        If tmpObjectData.Properties("index").PropertyValue < tmpList.Count Then
                                                            tmpList.Insert(tmpObjectData.Properties("index").PropertyValue, tmpListObject)

                                                        Else
                                                            tmpList.Add(tmpListObject)

                                                        End If

                                                        'Else if the property is a PersistentDictionary
                                                    ElseIf IsTypeOfBaseDictionary(tmpPropertyType) Then
                                                        Dim tmpKeyType As Type = GetBaseDictionaryKeyType(tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer))

                                                        Dim tmpKey As Object = tmpLayer.Converter.ConvertFromPersistenceValue(tmpObjectData.Properties("key").PropertyValue, tmpKeyType)

                                                        'Insert the item into the HashTable
                                                        tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer).Item(tmpKey) = tmpListObject

                                                    End If
                                                End If

                                                'Else if the item is a simple datatype
                                            Else
                                                tmpListObject = tmpObjectData.Properties("value").PropertyValue

                                                'If the property is a PersistentList
                                                If IsTypeOfBaseList(tmpPropertyType) Then
                                                    Dim tmpList As Object = tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer)

                                                    'Insert the entry in the List at the specified index
                                                    If tmpObjectData.Properties("index").PropertyValue < tmpList.Count Then
                                                        tmpList.Insert(tmpObjectData.Properties("index").PropertyValue, tmpLayer.Converter.ConvertFromPersistenceValue(tmpListObject, tmpListType))

                                                    Else
                                                        tmpList.Add(tmpLayer.Converter.ConvertFromPersistenceValue(tmpListObject, tmpListType))

                                                    End If

                                                ElseIf IsTypeOfBaseDictionary(tmpPropertyType) Then
                                                    Dim tmpKeyType As Type = GetBaseDictionaryKeyType(tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer))

                                                    Dim tmpKey As Object = tmpLayer.Converter.ConvertFromPersistenceValue(tmpObjectData.Properties("key").PropertyValue, tmpKeyType)

                                                    'Insert the item into the PersistentDictionary
                                                    tmpListData.PropertyDescriptor.PropertyInfo.GetValue(tmpContainer).Item(tmpKey) = tmpLayer.Converter.ConvertFromPersistenceValue(tmpListObject, tmpListType)

                                                End If

                                            End If
                                        End If

                                    Catch ex As Exception
                                        Dim tmpException As New Exception("An exception occurred in Broker. ListName: " & tmpListName, ex)
                                        BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))
                                    End Try

                                Next

                            End If
                        End If
                    Next


                    tmpAnalyzer.AddPerformanceMark("Filling lists")

                Catch ex As Exception

                    Dim tmpException As New Exception("An exception occurred in Broker.", ex)
                    BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))
                    If ThrowExceptions Then Throw tmpException

                Finally
                    PersistenceSettings.PersistencePerformanceLog.WriteEntry(tmpAnalyzer.GetDifferentialResult)
                End Try
            End Using

        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Returns the object with the specified ID.
        ''' </summary>
        ''' <param name="instanceID">ID of the object to return.</param>
        ''' <returns>Returns the object with the specified ID.</returns>
        ''' <remarks>
        '''     If no Object is found, Nothing is returned.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Shared Function GetObject(ByVal instanceID As Integer) As Object
            Try
                If ObjectIDDictionary.ContainsKey(instanceID) Then Return ObjectIDDictionary(instanceID)

            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                If ThrowExceptions Then Throw tmpException

                Return Nothing

            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Returns an ArrayList containing all objects of the specified Type.
        ''' </summary>
        ''' <param name="type">Type of Objects to return in the list.</param>
        ''' <returns>Returns an ArrayList containing all objects of the specified Type.</returns>
        ''' <remarks>
        '''     If no Objects is found, an empty ArrayList is returned.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Shared Function GetObjectArrayList(ByVal type As Type, Optional ByVal canBeInherited As Boolean = False) As ArrayList
            Try
                Dim tmpResult As New ArrayList
                Dim tmpObjectDictionary As ObjectDictionary = Nothing

                If ObjectTypeDictionary.ContainsKey(type) Then tmpObjectDictionary = ObjectTypeDictionary(type)

                If Not tmpObjectDictionary Is Nothing Then
                    tmpResult.AddRange(tmpObjectDictionary.ObjectDictionary.Values)

                End If

                Return tmpResult

            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                If ThrowExceptions Then Throw tmpException

            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Returns an ArrayList containing all objects of the specified Type.
        ''' </summary>
        ''' <param name="type">Type of Objects to return in the HashTable.</param>
        ''' <returns>Returns a HashTable containing all objects of the specified Type.</returns>
        ''' <remarks>
        '''     If no Objects is found, an empty Dictionary is returned.
        '''     The IDs of the Objects is used as the Key.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Shared Function GetObjectDictionary(ByVal type As Type) As Dictionary(Of Integer, IPersistentBaseObject)
            Try
                Dim tmpObjectDictionary As ObjectDictionary = ObjectTypeDictionary(type)

                If Not tmpObjectDictionary Is Nothing Then
                    Return tmpObjectDictionary.ObjectDictionary

                End If

                Return New Dictionary(Of Integer, IPersistentBaseObject)

            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                If ThrowExceptions Then Throw tmpException

                Return Nothing

            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Deletes the Object from the database.
        ''' </summary>
        ''' <param name="obj">The Object to delete.</param>
        ''' <remarks>
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Shared Sub DeleteObject(ByVal obj As IPersistentBaseObject)
            Try
                If obj.InstanceID > 0 Then
                    Using New Threading.Lock(FlushLock, True)
                        If Not _FlushTimerStarted Then
                            TmrFlush.Enabled = True
                            _FlushTimerStarted = True

                        End If

                        DisposeQueue(obj.InstanceID) = obj

                        'TmrFlush.Enabled = True

                    End Using

                End If

            Catch ex As Exception
                Dim tmpException As New Exception("An exception occurred in Broker.", ex)

                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(tmpException, Enums.ExceptionCategories.Persistence))

                If ThrowExceptions Then Throw tmpException

            End Try
        End Sub

        ''' <summary>
        ''' Removes all pointers to the objects instantiated so they can be Garbage Collected if necessary.
        ''' REMARK: GetObject(), GetObjectArrayList() and GetObjectHashTable will not be able to return data after this call.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub ReleaseMemory()
            ObjectIDDictionary.Clear()

            For Each tmpObjectDictionary As ObjectDictionary In ObjectTypeDictionary.Values
                tmpObjectDictionary.ObjectDictionary.Clear()
                tmpObjectDictionary.SourceDictionary.Clear()

            Next

            ObjectTypeDictionary.Clear()

            For Each tmpPropertyDescriptorList As List(Of PropertyDescriptor) In CachedPropertyDescriptors.Values
                tmpPropertyDescriptorList.Clear()

            Next

            CachedPropertyDescriptors.Clear()
            CachedPropertyDescriptorCollections.Clear()
            CachedAssemblies.Clear()

            For Each tmpLayer As PersistenceLayer In PersistenceSettings.PersistenceLayers
                If TypeOf tmpLayer Is SQLLayer Then
                    CType(tmpLayer.Proxy, SQLProxy).ReleaseMemory()

                End If
            Next

        End Sub

        Private Shared Sub OnBeforeInstantiating()
            RaiseEvent BeforeInstantiating(Nothing, New System.EventArgs)

        End Sub

        Private Shared Sub OnBrokerInstantiated()
            _InstantiationComplete = True

            RaiseEvent BrokerInstatiated(Nothing, New System.EventArgs)

        End Sub

        Private Shared Function GetPropertyDescriptors(ByVal type As Type) As List(Of PropertyDescriptor)
            If Not CachedPropertyDescriptors.ContainsKey(type.FullName) Then
                Dim tmpList As New List(Of PropertyDescriptor)

                Dim src_pc As PropertyDescriptorCollection = TypeDescriptor.GetProperties(type)
                Dim tmpProperty As System.ComponentModel.PropertyDescriptor

                Dim tmpPersistentPropertyAttribute As PersistentPropertyAttribute

                For i As Integer = 0 To src_pc.Count - 1
                    tmpProperty = src_pc(i)
                    tmpPersistentPropertyAttribute = src_pc(i).Attributes(GetType(PersistentPropertyAttribute))

                    If Not tmpPersistentPropertyAttribute Is Nothing Then
                        tmpList.Add(New PropertyDescriptor(tmpProperty, tmpPersistentPropertyAttribute))

                    End If
                Next

                CachedPropertyDescriptors(type.FullName) = tmpList

                Return tmpList

            Else
                Return CachedPropertyDescriptors(type.FullName)

            End If

            Return Nothing

        End Function

        Private Shared Function GetPropertyDescriptorCollection(ByVal obj As Object) As System.ComponentModel.PropertyDescriptorCollection
            If Not CachedPropertyDescriptorCollections.ContainsKey(obj.GetType) Then
                Dim tmpCollection As PropertyDescriptorCollection = TypeDescriptor.GetProperties(obj)

                CachedPropertyDescriptorCollections(obj.GetType) = tmpCollection

                Return tmpCollection

            Else
                Return CachedPropertyDescriptorCollections(obj.GetType)

            End If

            Return Nothing

        End Function

        Private Shared Function GetAssembly(ByVal name As String) As Assembly
            If Not CachedAssemblies.ContainsKey(name) Then
                Dim tmpAssembly As Assembly = [Assembly].LoadFrom(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, name))
                'tmpAssembly = [Assembly].Load(tmpAssemblyName)

                CachedAssemblies(name) = tmpAssembly

                Return tmpAssembly

            Else
                Return CachedAssemblies(name)

            End If

            Return Nothing

        End Function

        Private Shared Sub TmrFlush_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TmrFlush.Elapsed
            Try
                TmrFlush.Enabled = False

                Dim tmpFlushQueue As Dictionary(Of Integer, IPersistentBaseObject)
                Dim tmpDisposeQueue As Dictionary(Of Integer, IPersistentBaseObject)

                Using New Threading.Lock(FlushLock, True)
                    tmpFlushQueue = FlushQueue
                    tmpDisposeQueue = DisposeQueue

                    _FlushQueue = New Dictionary(Of Integer, IPersistentBaseObject)
                    _DisposeQueue = New Dictionary(Of Integer, IPersistentBaseObject)

                End Using

                Dim tmpSaveList As New List(Of ObjectData)

                For Each tmpObject As IPersistentBaseObject In tmpFlushQueue.Values
                    tmpSaveList.AddRange(GetObjectData(tmpObject))

                Next

                Try
                    'UNDONE: Only support default layer
                    If tmpSaveList.Count > 0 Then
                        PersistenceSettings.DefaultPersistenceLayer.Proxy.SaveData(tmpSaveList)

                    End If

                    'Flush Dispose Queue
                    If tmpDisposeQueue.Count > 0 Then
                        Dim tmpDisposeList As New List(Of Integer)

                        For Each tmpObject As IPersistentBaseObject In tmpDisposeQueue.Values
                            tmpDisposeList.Add(tmpObject.InstanceID)

                        Next

                        'UNDONE: Only support default layer
                        PersistenceSettings.DefaultPersistenceLayer.Proxy.DeleteData(tmpDisposeList)

                    End If

                    If PerformanceCountersEnabled Then
                        PersistenceFlushQueueSizePerformanceCounter.Decrement(tmpFlushQueue.Count)
                    End If

                Catch ex As Exception
                    'Failed - returning objects to the flushQueues
                    Using New Threading.Lock(FlushLock, True)
                        For Each tmpEntry As KeyValuePair(Of Integer, IPersistentBaseObject) In tmpFlushQueue
                            FlushQueue(tmpEntry.Key) = tmpEntry.Value

                        Next

                        For Each tmpEntry As KeyValuePair(Of Integer, IPersistentBaseObject) In tmpDisposeQueue
                            DisposeQueue(tmpEntry.Key) = tmpEntry.Value

                        Next
                    End Using

                    tmpFlushQueue.Clear()
                    tmpDisposeQueue.Clear()

                    Throw New Exception("Error flushing queue", ex)

                End Try

                tmpFlushQueue.Clear()
                tmpDisposeQueue.Clear()

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Persistence))

            Finally
                TmrFlush.Enabled = True

            End Try
        End Sub

#Region "Helper functions"

        Friend Shared Function HasPrivatePersistenceConstructor(ByVal type As Type) As Boolean
            Dim tmpConstructorList() As System.Reflection.ConstructorInfo = type.GetConstructors(BindingFlags.Instance Or BindingFlags.NonPublic)

            For Each tmpConstructor As System.Reflection.ConstructorInfo In tmpConstructorList
                Dim tmpParameterList() As ParameterInfo = tmpConstructor.GetParameters

                Return tmpParameterList.Count = 0

                'If tmpParameterList.Count = 1 Then
                '    If tmpParameterList(0).ParameterType Is GetType(Persistence.PersistentObjectData) Then
                '        Return True

                '    End If
                'End If
            Next

            'If Not type.BaseType Is Nothing Then
            '    Return HasPrivatePersistenceConstructor(type.BaseType)

            'End If

            Return False

        End Function

        Friend Shared Function HasPublicPersistenceConstructor(ByVal type As Type) As Boolean
            Dim tmpConstructorList() As System.Reflection.ConstructorInfo = type.GetConstructors

            For Each tmpConstructor As System.Reflection.ConstructorInfo In tmpConstructorList
                Dim tmpParameterList() As System.Reflection.ParameterInfo = tmpConstructor.GetParameters

                If tmpParameterList.Count = 1 Then
                    If tmpParameterList(0).ParameterType Is GetType(Boolean) Then
                        Return True

                    End If
                End If
            Next

            Return False

        End Function

#End Region

        ''TEST
        'Private Shared Sub ExecuteTestQuery()
        '    Using tmpConnection As New System.Data.SqlClient.SqlConnection("data source='W2K3TV1';Trusted_Connection=yes;database=vTouchProData_W2K3TV1;Pooling='false'")
        '        tmpConnection.Open()
        '        Dim tmpScript As StringBuilder = New StringBuilder
        '        tmpScript.AppendLine("SET NOCOUNT ON")
        '        tmpScript.AppendLine("BEGIN TRANSACTION")

        '        tmpScript.AppendLine("SELECT * FROM [vTouch.Server.Model.Computer]")
        '        tmpScript.AppendLine("SELECT * FROM [vTouch.Server.Model.ComputerCollection]")
        '        tmpScript.AppendLine("SELECT * FROM [vTouch.Server.Model.Display]")

        '        tmpScript.AppendLine("COMMIT TRANSACTION")

        '        Using tmpCommand As New System.Data.SqlClient.SqlCommand(tmpScript.ToString, tmpConnection)
        '            tmpCommand.CommandTimeout = 60

        '            tmpCommand.ExecuteNonQuery()

        '        End Using
        '    End Using

        'End Sub

        'Private Shared Sub _TmrTestTransactions_Elapsed(sender As Object, e As System.EventArgs) Handles _TmrTestTransactions.Elapsed
        '    Try
        '        _TmrTestTransactions.Enabled = False
        '        ExecuteTestQuery()

        '    Catch ex As Exception

        '    Finally
        '        _TmrTestTransactions.Enabled = True

        '    End Try
        'End Sub

    End Class

    Friend Class PropertyDescriptor

#Region "Variables"

        Private _PropertyName As String
        Private _PropertyType As Type
        Private _PropertyInfo As System.ComponentModel.PropertyDescriptor
        Private _IsReferenceProperty As Boolean
        Private _PersistentPropertyAttribute As PersistentPropertyAttribute

#End Region

#Region "Properties"

        Public ReadOnly Property PropertyName() As String
            Get
                Return Me._PropertyName

            End Get
        End Property

        Public ReadOnly Property PropertyType() As Type
            Get
                Return Me._PropertyType

            End Get
        End Property

        Public ReadOnly Property PropertyInfo() As System.ComponentModel.PropertyDescriptor
            Get
                Return Me._PropertyInfo

            End Get
        End Property

        Public ReadOnly Property IsReferenceProperty() As Boolean
            Get
                Return Me._IsReferenceProperty

            End Get
        End Property

        Public ReadOnly Property PersistentPropertyAttribute() As PersistentPropertyAttribute
            Get
                Return Me._PersistentPropertyAttribute

            End Get
        End Property

#End Region

        Public Sub New(ByVal propertyInfo As System.ComponentModel.PropertyDescriptor, ByVal persistentPropertyAttribute As PersistentPropertyAttribute)
            MyBase.New()

            Me._PropertyInfo = propertyInfo
            Me._PersistentPropertyAttribute = persistentPropertyAttribute

            Me._PropertyName = propertyInfo.Name
            Me._PropertyType = propertyInfo.PropertyType

            If Array.IndexOf(Me.PropertyInfo.PropertyType.GetInterfaces(), GetType(IPersistentBaseObject)) <> -1 Then
                Me._IsReferenceProperty = True

            ElseIf IsTypeOfBaseList(Me.PropertyInfo.PropertyType) Then
                Me._IsReferenceProperty = True

            ElseIf IsTypeOfBaseDictionary(Me.PropertyInfo.PropertyType) Then
                Me._IsReferenceProperty = True

            Else
                Me._IsReferenceProperty = False

            End If

        End Sub

    End Class

End Namespace