Namespace Persistence

    Public MustInherit Class PersistenceConverter

        Public MustOverride Function ConvertFromPersistenceValue(ByVal value As Object, ByVal type As Type) As Object
        Public MustOverride Function ConvertToPersistenceValue(ByVal value As Object, ByVal type As Type, Optional ByVal attributes As Dictionary(Of Type, Attribute) = Nothing) As Object
        Public MustOverride Function ConvertToPersistenceFormat(ByVal type As System.Type, Optional ByVal attributes As Dictionary(Of Type, Attribute) = Nothing) As Object
        Public MustOverride Function IsTypeSupported(ByVal type As System.Type) As Boolean

    End Class

End Namespace