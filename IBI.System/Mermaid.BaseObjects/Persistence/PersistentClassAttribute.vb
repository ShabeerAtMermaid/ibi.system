Namespace Persistence

    <AttributeUsage(AttributeTargets.Class)> _
    Public Class PersistentClassAttribute
        Inherits Attribute

#Region "Attributes"

        Private _UseDefaultPersistenceLayer As Boolean
        Private _PersistenceLayerKey As Object

#End Region

#Region "Properties"

        Public ReadOnly Property UseDefaultPersistenceLayer() As Boolean
            Get
                Return Me._UseDefaultPersistenceLayer

            End Get
        End Property

        Public ReadOnly Property PersistenceLayerKey() As Object
            Get
                Return Me._PersistenceLayerKey

            End Get
        End Property

#End Region

        Public Sub New()
            MyBase.New()

            Me._UseDefaultPersistenceLayer = True

        End Sub

        Public Sub New(ByVal persistenceLayerKey As String)
            MyBase.New()

            Me._UseDefaultPersistenceLayer = False
            Me._PersistenceLayerKey = persistenceLayerKey

        End Sub

    End Class

End Namespace