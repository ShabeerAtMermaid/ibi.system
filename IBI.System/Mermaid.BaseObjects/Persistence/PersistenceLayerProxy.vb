Namespace Persistence

    Public MustInherit Class PersistenceLayerProxy

#Region "Properties"

        Public MustOverride ReadOnly Property ProxyReady() As Boolean

        Public MustOverride Property Version() As Integer

#End Region

        Friend MustOverride Function SaveData(ByVal objectData As ObjectData) As Integer
        Friend MustOverride Sub SaveData(ByVal datalist As List(Of ObjectData), Optional ByVal timeoutInSecs As Integer = 900)
        'Public MustOverride Sub SaveData(ByVal data As DataSet)
        Friend MustOverride Sub DeleteData(ByVal instanceID As Integer)
        Friend MustOverride Sub DeleteData(ByVal instanceIDs As List(Of Integer))
        Friend MustOverride Sub ClearCollection(ByVal typeName As String, ByVal instanceID As Integer)
        Friend MustOverride Function GetObjectData() As Dictionary(Of String, Dictionary(Of Integer, ObjectData))
        Friend MustOverride Function GetObjectData(ByVal typeName As String) As Dictionary(Of Integer, ObjectData)
        'Public MustOverride Function AcquireData(ByVal tableName As String, ByVal queryParameters() As QueryParameter) As DataSet
        'Public MustOverride Function AcquireData(ByVal sqlScript As String) As DataSet
        'Public MustOverride Function AcquireData(ByVal sqlScript As String, ByVal timeoutInSecs As Integer) As DataSet

    End Class

End Namespace