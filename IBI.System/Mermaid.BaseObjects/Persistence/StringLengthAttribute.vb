﻿Namespace Persistence

    <AttributeUsage(AttributeTargets.Property)> _
    Public Class StringLengthAttribute
        Inherits Attribute

#Region "Variables"

        Private _Length As Integer

#End Region

#Region "Properties"

        Public ReadOnly Property Length() As Integer
            Get
                Return Me._Length

            End Get
        End Property

#End Region

        Public Sub New(ByVal length As Integer)
            MyBase.New()

            Me._Length = length

        End Sub

    End Class

End Namespace