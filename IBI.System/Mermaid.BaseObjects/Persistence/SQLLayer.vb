Namespace Persistence

    Public Class SQLLayer
        Inherits PersistenceLayer

#Region "Attributes"

        Private _Proxy As SQLProxy
        Private _Converter As SQLConverter

#End Region

#Region "Properties"

        Public Property DatabaseServer() As String
            Get
                Return Me._Proxy.DatabaseServer

            End Get
            Set(ByVal Value As String)
                Me._Proxy.DatabaseServer = Value

            End Set
        End Property

        Public Property DatabaseName() As String
            Get
                Return Me._Proxy.DatabaseName

            End Get
            Set(ByVal Value As String)
                Me._Proxy.DatabaseName = Value

            End Set
        End Property

        Public Property Login() As String
            Get
                Return Me._Proxy.Login

            End Get
            Set(ByVal Value As String)
                Me._Proxy.Login = Value

            End Set
        End Property

        Public Property Password() As String
            Get
                Return Me._Proxy.Password

            End Get
            Set(ByVal Value As String)
                Me._Proxy.Password = Value

            End Set
        End Property

        Public Overrides ReadOnly Property Proxy() As PersistenceLayerProxy
            Get
                Return Me._Proxy

            End Get
        End Property

        Friend Overrides ReadOnly Property Converter() As PersistenceConverter
            Get
                Return Me._Converter

            End Get
        End Property

        Public Overrides ReadOnly Property PersistenceLayerReady() As Boolean
            Get
                Return Me.Proxy.ProxyReady

            End Get
        End Property

        Public Property PerformanceCountersEnabled() As Boolean
            Get
                Return CType(Me.Proxy, SQLProxy).PerformanceCountersEnabled

            End Get
            Set(ByVal value As Boolean)
                CType(Me.Proxy, SQLProxy).PerformanceCountersEnabled = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal key As String)
            MyBase.New(key)

            Me._Proxy = New SQLProxy(Me)
            Me._Converter = New SQLConverter

        End Sub

    End Class

End Namespace