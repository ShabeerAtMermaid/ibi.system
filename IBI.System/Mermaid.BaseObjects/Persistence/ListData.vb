﻿Namespace Persistence

    Friend Class ListData

#Region "Variables"

        Private _ContainerType As Type
        Private _PropertyDescriptor As PropertyDescriptor

#End Region

#Region "Properties"

        Public Property ContainerType() As Type
            Get
                Return Me._ContainerType

            End Get
            Set(ByVal value As Type)
                Me._ContainerType = value

            End Set
        End Property

        Public Property PropertyDescriptor() As PropertyDescriptor
            Get
                Return Me._PropertyDescriptor

            End Get
            Set(ByVal value As PropertyDescriptor)
                Me._PropertyDescriptor = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal containerType As Type, ByVal propertyDescriptor As PropertyDescriptor)
            MyBase.New()

            Me._ContainerType = containerType
            Me._PropertyDescriptor = propertyDescriptor

        End Sub

    End Class

End Namespace