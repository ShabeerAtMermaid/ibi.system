Imports mermaid.BaseObjects.Remoting
Imports mermaid.BaseObjects.Persistence.Interfaces
Imports System.ComponentModel
Imports System.Reflection

Namespace Persistence

    Public Class PersistentBaseObject
        Inherits BaseObject
        Implements IPersistentBaseObject

#Region "Attributes"

        Private _InstanceVersion As Integer
        Protected _PersistenceEnabled As Boolean

#End Region

#Region "Properties"

        <PersistentProperty()> _
        Public Overrides Property InstanceID() As Integer
            Get
                Return MyBase.InstanceID

            End Get
            Set(ByVal Value As Integer)
                MyBase.InstanceID = Value

            End Set
        End Property

        <PersistentProperty()> _
        Public Property InstanceVersion() As Integer Implements IPersistentBaseObject.InstanceVersion
            Get
                Return Me._InstanceVersion

            End Get
            Set(ByVal Value As Integer)
                Me._InstanceVersion = Value

            End Set
        End Property

        Protected Overridable ReadOnly Property PersistenceEnabled() As Boolean Implements IPersistentBaseObject.PersistenceEnabled
            Get
                Return Me._PersistenceEnabled

            End Get
        End Property

        Public ReadOnly Property IsPersistent() As Boolean Implements IPersistentBaseObject.IsPersistent
            Get
                If Broker.InstantiationComplete Then
                    If Not Me.IsDisposed Then
                        If Me.InstanceID < (Integer.MaxValue / 2) And Me.InstanceID > 0 Then
                            Return True

                        End If
                    End If
                End If

                Return False

            End Get
        End Property

#End Region

#Region "Events"

#End Region

        Public Sub New(ByVal persistent As Boolean)
            MyBase.New()

            Me._PersistenceEnabled = persistent

            If Me.PersistenceEnabled Then
                Me.Save()

            Else
                Me.InstanceID = BaseUtility.IDCounter.GetID

            End If

        End Sub

        Protected Sub New()
            MyBase.new()

            Me._PersistenceEnabled = True

        End Sub

        Public Overridable Sub Save() Implements IPersistentBaseObject.Save
            If PersistenceSettings.PersistenceEnabled And Me.PersistenceEnabled And Not Me.IsDisposed Then
                Me.InstanceVersion += 1

                Broker.SaveObject(Me)

            End If

        End Sub

        Public Overrides Sub Dispose()
            If Not Me.IsDisposed Then
                If Me.IsPersistent Then
                    PersistenceSettings.PersistenceThreadPool.EnqueueWorkItem(New Threading.WorkItem(AddressOf Dispose_CallBack))

                End If

                MyBase.Dispose()

            End If

        End Sub

        Private Function Dispose_CallBack() As Object
            Broker.DeleteObject(Me)

        End Function

    End Class

End Namespace