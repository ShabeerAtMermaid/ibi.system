﻿Namespace Persistence

    Public Class SQLConverter
        Inherits PersistenceConverter

        Public Overrides Function ConvertFromPersistenceValue(ByVal value As Object, ByVal type As Type) As Object
           If type.IsEnum Then
                If Not value Is DBNull.Value Then
                    Return [Enum].Parse(type, value, True)

                Else
                    Return [Enum].Parse(type, 0, True)

                End If
            ElseIf type Is GetType(Boolean) Then
                If Not value Is DBNull.Value Then
                    If TypeOf value Is Integer Then
                        If value > 0 Then
                            Return True

                        Else
                            Return False

                        End If

                    ElseIf TypeOf value Is Boolean Then
                        Return value

                    Else
                        Return False

                    End If

                Else
                    Return False

                End If

            ElseIf type Is GetType(String) Then
                If Not value Is DBNull.Value Then
                    Return value

                Else
                    Return String.Empty

                End If

            ElseIf type Is GetType(Integer) Then
                Return Converter.ToInt32(value)
                
            ElseIf type Is GetType(Double) Then
                Return Converter.ToDouble(value)

            ElseIf type Is GetType(Long) Then
                Return Converter.ToLong(value)

            ElseIf type Is GetType(TimeSpan) Then
                If Not value Is DBNull.Value Then
                    Return New TimeSpan(0, 0, value)

                Else
                    Return New TimeSpan(0, 0, 0)

                End If

            ElseIf type Is GetType(System.Globalization.CultureInfo) Then
                If Not value Is DBNull.Value Then
                    Dim tmpLCID As Integer = CInt(value)

                    If tmpLCID > 0 Then
                        Return New System.Globalization.CultureInfo(tmpLCID)

                    Else
                        Return Nothing

                    End If

                Else
                    Return Nothing

                End If

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.Fontos) Then
                If Not value Is DBNull.Value Then
                    Return New DataTypes.Fontos(CType(value, String))

                Else
                    Return New DataTypes.Fontos("Times New Roman;10;0")

                End If

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.FileSize) Then
                If Not value Is DBNull.Value Then
                    Return New DataTypes.FileSize(Val(value))

                Else
                    Return New DataTypes.FileSize(0)

                End If

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.ProgramVersion) Then
                If Not value Is DBNull.Value Then
                    Return New DataTypes.ProgramVersion(CType(value, String))

                Else
                    Return New DataTypes.ProgramVersion("0.0.0.0")

                End If

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.FileInformation) Then
                If Not value Is DBNull.Value Then
                    Return DataTypes.FileInformation.DeSerialize(value)

                Else
                    Return Nothing

                End If

            ElseIf type Is GetType(Drawing.Color) Then
                If Not value Is DBNull.Value Then
                    Return Drawing.Color.FromArgb(CInt(value))

                Else
                    Return Drawing.Color.Red

                End If

            ElseIf type Is GetType(System.Net.IPAddress) Then
                Return Converter.ToIPAddress(value)

            Else
                If Not value Is DBNull.Value Then
                    Return value

                Else
                    Return Nothing

                End If
            End If
        End Function

        Public Overrides Function ConvertToPersistenceValue(ByVal value As Object, ByVal type As Type, Optional ByVal attributes As Dictionary(Of Type, Attribute) = Nothing) As Object
            If type.IsEnum Then
                If Not value Is Nothing Then
                    Return CInt(value)

                Else
                    Return 0

                End If
            End If

            Select Case type.ToString
                Case GetType(System.String).ToString
                    Dim tmpValue As String = String.Empty

                    If Not value Is Nothing AndAlso TypeOf value Is String Then
                        tmpValue = "'" & value.Replace("'", "''") & "'"

                    Else
                        tmpValue = "'" & value & "'"

                    End If

                    Dim tmpMaxLength As Integer = 256

                    If Not attributes Is Nothing Then
                        Dim tmpAttribute As StringLengthAttribute = Nothing

                        If attributes.ContainsKey(GetType(StringLengthAttribute)) Then tmpAttribute = attributes(GetType(StringLengthAttribute))

                        If Not tmpAttribute Is Nothing Then
                            tmpMaxLength = tmpAttribute.Length

                        End If
                    End If

                    If tmpValue.Length > tmpMaxLength Then
                        Return tmpValue.Substring(0, tmpMaxLength - 1) & "'"

                    Else
                        Return "N" & tmpValue

                    End If

                Case GetType(Date).ToString
                    Dim tmpDate As Date = value

                    If tmpDate.Year > 1753 Then
                        Return "'" & tmpDate.Month & "-" & tmpDate.Day & "-" & tmpDate.Year & " " & tmpDate.Hour & ":" & tmpDate.Minute & ":" & tmpDate.Second & "'"

                    Else
                        Return "'01-01-1754 00:00:00'"

                    End If

                Case GetType(System.DateTime).ToString
                    Dim tmpDate As DateTime = value

                    If tmpDate.Year > 1753 Then
                        Return "'" & tmpDate.Month & "-" & tmpDate.Day & "-" & tmpDate.Year & " " & tmpDate.Hour & ":" & tmpDate.Minute & ":" & tmpDate.Second & "'"

                    Else
                        Return "'01-01-1754 00:00:00'"

                    End If
                Case GetType(System.Boolean).ToString
                    If value Then
                        Return 1

                    Else
                        Return 0

                    End If

                Case GetType(System.Double).ToString
                    Dim tmpString As String = value

                    Return tmpString.Replace(",", ".")

                Case GetType(System.Single).ToString
                    Dim tmpString As String = value

                    Return tmpString.Replace(",", ".")

                Case GetType(System.TimeSpan).ToString
                    If Not value Is Nothing Then
                        Return CType(value, TimeSpan).TotalSeconds

                    Else
                        Return 0

                    End If

                Case GetType(System.Globalization.CultureInfo).ToString
                    If Not value Is Nothing Then
                        Return CType(value, System.Globalization.CultureInfo).LCID

                    Else
                        Return 0

                    End If

                Case GetType(mermaid.BaseObjects.DataTypes.Fontos).ToString
                    If Not value Is Nothing Then
                        Return "'" & CType(value, DataTypes.Fontos).FontString & "'"

                    Else
                        Return "'Times New Roman;10;0'"

                    End If

                Case GetType(mermaid.BaseObjects.DataTypes.FileSize).ToString
                    If Not value Is Nothing Then
                        Return CType(value, DataTypes.FileSize).Size

                    Else
                        Return 0

                    End If

                Case GetType(mermaid.BaseObjects.DataTypes.ProgramVersion).ToString
                    If Not value Is Nothing Then
                        Return "'" & CType(value, DataTypes.ProgramVersion).ToString & "'"

                    Else
                        Return "'0.0.0.0'"

                    End If

                Case GetType(mermaid.BaseObjects.DataTypes.FileInformation).ToString
                    If Not value Is Nothing Then
                        Return CType(value, DataTypes.FileInformation).Serialize

                    Else
                        Return String.Empty

                    End If

                Case GetType(Drawing.Color).ToString
                    Return CType(value, Drawing.Color).ToArgb

                Case GetType(System.Net.IPAddress).ToString
                    If Not value Is Nothing Then
                        Return "'" & value.ToString & "'"

                    Else
                        Return "'127.0.0.1'"

                    End If

                Case Else
                    Return value

            End Select

        End Function

        Public Overrides Function ConvertToPersistenceFormat(ByVal type As System.Type, Optional ByVal attributes As Dictionary(Of Type, Attribute) = Nothing) As Object
            If type.IsEnum Then
                Return "Int"

            End If

            Select Case type.ToString
                Case GetType(System.String).ToString
                    If Not attributes Is Nothing Then
                        Dim tmpAttribute As StringLengthAttribute = Nothing

                        If attributes.ContainsKey(GetType(StringLengthAttribute)) Then tmpAttribute = attributes(GetType(StringLengthAttribute))

                        If Not tmpAttribute Is Nothing Then
                            Return "varChar(" & tmpAttribute.Length & ")"

                        End If
                    End If

                    Return "varChar(256)"

                Case GetType(System.Int16).ToString
                    Return "Int"

                Case GetType(System.Int32).ToString
                    Return "Int"

                Case GetType(System.Int64).ToString
                    Return "BigInt"

                Case GetType(Integer).ToString
                    Return "Int"

                Case GetType(System.Double).ToString
                    Return "Decimal(18,9)"

                Case GetType(System.Single).ToString
                    Return "Decimal(18,9)"

                Case GetType(System.Boolean).ToString
                    Return "Bit"

                Case GetType(Date).ToString
                    Return "DateTime"

                Case GetType(System.DateTime).ToString
                    Return "DateTime"

                Case GetType(System.TimeSpan).ToString
                    Return "Int"

                Case GetType(System.Globalization.CultureInfo).ToString
                    Return "Int"

                Case GetType(mermaid.BaseObjects.DataTypes.Fontos).ToString
                    Return "varChar(60)"

                Case GetType(mermaid.BaseObjects.DataTypes.FileSize).ToString
                    Return "Int"

                Case GetType(mermaid.BaseObjects.DataTypes.ProgramVersion).ToString
                    Return "varChar(50)"

                Case GetType(mermaid.BaseObjects.DataTypes.FileInformation).ToString
                    Return "varChar(2000)"

                Case GetType(Drawing.Color).ToString
                    Return "Int"

                Case GetType(System.Net.IPAddress).ToString
                    Return "varChar(30)"

            End Select

            Return Nothing

        End Function

        Public Overrides Function IsTypeSupported(ByVal type As System.Type) As Boolean
            If type.IsEnum Then
                Return True

            ElseIf type Is GetType(Boolean) Then
                Return True

            ElseIf type Is GetType(String) Then
                Return True

            ElseIf type Is GetType(Date) Then
                Return True

            ElseIf type Is GetType(DateTime) Then
                Return True

            ElseIf type Is GetType(Double) Then
                Return True

            ElseIf type Is GetType(Single) Then
                Return True

            ElseIf type Is GetType(Short) Then
                Return True

            ElseIf type Is GetType(Integer) Then
                Return True

            ElseIf type Is GetType(Long) Then
                Return True

            ElseIf type Is GetType(TimeSpan) Then
                Return True

            ElseIf type Is GetType(System.Globalization.CultureInfo) Then
                Return True

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.Fontos) Then
                Return True

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.FileSize) Then
                Return True

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.ProgramVersion) Then
                Return True

            ElseIf type Is GetType(mermaid.BaseObjects.DataTypes.FileInformation) Then
                Return True

            ElseIf type Is GetType(Drawing.Color) Then
                Return True

            ElseIf type Is GetType(System.Net.IPAddress) Then
                Return True

            End If

            Return False

        End Function

    End Class

End Namespace