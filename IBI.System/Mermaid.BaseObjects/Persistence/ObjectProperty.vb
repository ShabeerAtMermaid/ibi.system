Namespace Persistence

    Friend Class ObjectProperty

#Region "Attributes"

        Private _PropertyName As String
        Private _PropertyValue As Object
        Private _PropertyType As Type
        Private _Attributes As List(Of Attribute)

#End Region

#Region "Properties"

        Public Property PropertyName() As String
            Get
                Return Me._PropertyName

            End Get
            Set(ByVal Value As String)
                Me._PropertyName = Value

            End Set
        End Property

        Public Property PropertyValue() As Object
            Get
                Return Me._PropertyValue

            End Get
            Set(ByVal Value As Object)
                Me._PropertyValue = Value

            End Set
        End Property

        Public Property PropertyType() As Type
            Get
                Return Me._PropertyType

            End Get
            Set(ByVal Value As Type)
                Me._PropertyType = Value

            End Set
        End Property

        Public Property Attributes() As List(Of Attribute)
            Get
                If Me._Attributes Is Nothing Then
                    Me._Attributes = New List(Of Attribute)

                End If

                Return Me._Attributes

            End Get
            Set(ByVal Value As List(Of Attribute))
                Me._Attributes = Value

            End Set
        End Property

#End Region

        Public Sub New(ByVal name As String, ByVal value As Object, ByVal type As Type, ByVal attributes As List(Of Attribute))
            Me.PropertyName = name
            Me.PropertyValue = value
            Me.PropertyType = type
            Me.Attributes = attributes

        End Sub

        Public Sub New(ByVal name As String, ByVal value As Object, ByVal type As Type)
            Me.New(name, value, type, Nothing)

        End Sub

    End Class

End Namespace