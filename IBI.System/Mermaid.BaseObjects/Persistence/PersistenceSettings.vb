Imports mermaid.BaseObjects.Threading

Namespace Persistence

    Public Class PersistenceSettings

#Region "Variables"

        Private Shared _PersistenceEnabled As Boolean
        Private Shared _PersistenceLayers As New List(Of PersistenceLayer)
        Private Shared _DefaultPersistenceLayer As PersistenceLayer
        Private Shared _PersistencePerformanceLog As New Diagnostics.PerformanceLog("mermaid BaseObjects\Persistence")

#End Region

#Region "Properties"

        Public Shared Property PersistenceEnabled() As Boolean
            Get
                Return _PersistenceEnabled

            End Get
            Set(ByVal Value As Boolean)
                _PersistenceEnabled = Value

            End Set
        End Property

        Public Shared ReadOnly Property PersistenceLayers() As List(Of PersistenceLayer)
            Get
                Return _PersistenceLayers

            End Get
        End Property

        Public Shared Property DefaultPersistenceLayer() As PersistenceLayer
            Get
                Return _DefaultPersistenceLayer

            End Get
            Set(ByVal value As PersistenceLayer)
                _DefaultPersistenceLayer = value

            End Set
        End Property

        Public Shared ReadOnly Property PersistenceThreadPool() As BaseObjects.Threading.ThreadPool
            Get
                Return ThreadPoolManager.GetThreadPool("PersistencePool")

            End Get
        End Property

        Public Shared ReadOnly Property PersistencePerformanceLog() As Diagnostics.PerformanceLog
            Get
                Return _PersistencePerformanceLog

            End Get
        End Property

#End Region

        Public Shared Function GetPersistenceLayer(ByVal key As String) As PersistenceLayer
            For Each tmpLayer As PersistenceLayer In PersistenceLayers
                If String.Compare(tmpLayer.Key, key, True) = 0 Then
                    Return tmpLayer

                End If
            Next

            Return Nothing

        End Function

    End Class

End Namespace