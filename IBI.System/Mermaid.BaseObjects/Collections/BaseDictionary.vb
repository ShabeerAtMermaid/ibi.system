﻿Imports mermaid.BaseObjects.Persistence.Interfaces
Imports System.Threading

Namespace Collections

    <System.Serializable()> Public Class BaseDictionary(Of TKey, TValue)
        Inherits Dictionary(Of TKey, TValue)

#Region "Variables"

        Private _SyncLock As New Object
        Private _Owner As IPersistentBaseObject

#End Region

#Region "Properties"

        Default Public Shadows Property Item(ByVal key As TKey) As TValue
            Get
                Using New Threading.Lock(Me.SyncLock, True)
                    If MyBase.ContainsKey(key) Then
                        Return MyBase.Item(key)

                    End If

                    Return Nothing

                End Using

            End Get
            Set(ByVal value As TValue)
                Using New Threading.Lock(Me.SyncLock, True)
                    MyBase.Item(key) = value

                    Me.AddHandlers(value)

                End Using

            End Set
        End Property

        Public ReadOnly Property [SyncLock]() As Object
            Get
                Return Me._SyncLock

            End Get
        End Property

        Private Property Owner() As IPersistentBaseObject
            Get
                Return Me._Owner

            End Get
            Set(ByVal value As IPersistentBaseObject)
                Me._Owner = value

            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal owner As IPersistentBaseObject)
            MyBase.New()

            Me.Owner = owner

        End Sub

        Private Sub New(ByVal source As BaseDictionary(Of TKey, TValue))
            MyBase.New(source)

        End Sub

        Private Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)

        End Sub

        Public Shadows Sub Add(ByVal key As TKey, ByVal value As TValue)
            Using New Threading.Lock(Me.SyncLock, True)
                MyBase.Add(key, value)

                Me.AddHandlers(value)

            End Using

        End Sub

        Public Shadows Sub Clear()
            Using New Threading.Lock(Me.SyncLock, True)
                For Each tmpItem As Object In Me.Values
                    Me.RemoveHandlers(tmpItem)

                Next

                MyBase.Clear()

            End Using

        End Sub

        Public Function Clone() As BaseDictionary(Of TKey, TValue)
            Using New Threading.Lock(Me.SyncLock, True)
                Return New BaseDictionary(Of TKey, TValue)(Me)

            End Using

        End Function

        Public Shadows Function Remove(ByVal key As TKey) As Boolean
            Using New Threading.Lock(Me.SyncLock, True)
                Me.RemoveHandlers(Me(key))

                Return MyBase.Remove(key)

            End Using

        End Function

        'Private Sub Lock()
        '    Monitor.Enter(Me)

        'End Sub

        'Private Sub Unlock()
        '    Monitor.Exit(Me)

        'End Sub

#Region "Element Diposing Support"

        Private Sub AddHandlers(ByVal element As Object)
            If Not element Is Nothing Then
                If Array.IndexOf(element.GetType.GetInterfaces, GetType(IPersistentBaseObject)) <> -1 Then
                    AddHandler CType(element, IPersistentBaseObject).Disposing, AddressOf Element_Disposing

                End If
            End If

        End Sub

        Private Sub RemoveHandlers(ByVal element As Object)
            If Not element Is Nothing Then
                If Array.IndexOf(element.GetType.GetInterfaces, GetType(IPersistentBaseObject)) <> -1 Then
                    RemoveHandler CType(element, IPersistentBaseObject).Disposing, AddressOf Element_Disposing

                End If
            End If

        End Sub

        Private Sub Element_Disposing(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.Remove(sender.InstanceID)

            If Not Me.Owner Is Nothing Then
                Me.Owner.Save()

            End If

        End Sub

#End Region

    End Class

End Namespace