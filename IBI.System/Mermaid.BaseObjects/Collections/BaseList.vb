﻿Imports mermaid.BaseObjects.Persistence.Interfaces
Imports mermaid.BaseObjects.Query

Namespace Collections

    <System.Serializable()> Public Class BaseList(Of T)
        Inherits List(Of T)

#Region "Variables"

        Private _SyncLock As New Object
        Private _Owner As IPersistentBaseObject

#End Region

#Region "Properties"

        Default Public Shadows Property Item(ByVal index As Integer) As T
            Get
                Using New Threading.Lock(Me.SyncLock, True)
                    Return MyBase.Item(index)

                End Using

            End Get
            Set(ByVal value As T)
                Using New Threading.Lock(Me.SyncLock, True)
                    MyBase.Item(index) = value

                    Me.AddHandlers(value)

                End Using

            End Set
        End Property

        Public ReadOnly Property [SyncLock]() As Object
            Get
                Return Me._SyncLock

            End Get
        End Property

        Private Property Owner() As IPersistentBaseObject
            Get
                Return Me._Owner

            End Get
            Set(ByVal value As IPersistentBaseObject)
                Me._Owner = value

            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal owner As IPersistentBaseObject)
            MyBase.New()

            Me.Owner = owner

        End Sub

        Private Sub New(ByVal source As BaseList(Of T))
            MyBase.New(source)

        End Sub

        'Private Sub New(info As System.Runtime.Serialization.SerializationInfo, context As System.Runtime.Serialization.StreamingContext))
        '    MyBase.New(info, context)

        'End Sub

        Public Shadows Sub Add(ByVal item As T)
            Using New Threading.Lock(Me.SyncLock, True)
                MyBase.Add(item)

                Me.AddHandlers(item)

            End Using

        End Sub

        Public Shadows Sub AddRange(ByVal collection As IEnumerable(Of T))
            Using New Threading.Lock(Me.SyncLock, True)
                MyBase.AddRange(collection)

                For Each tmpItem As Object In collection
                    Me.AddHandlers(tmpItem)

                Next

            End Using

        End Sub

        Public Shadows Sub Clear()
            Using New Threading.Lock(Me.SyncLock, True)
                For Each tmpItem As Object In Me
                    Me.RemoveHandlers(tmpItem)

                Next

                MyBase.Clear()

            End Using

        End Sub

        Public Function Clone() As BaseList(Of T)
            Using New Threading.Lock(Me.SyncLock, True)
                Return New BaseList(Of T)(Me)

            End Using

        End Function

        Public Shadows Sub Insert(ByVal index As Integer, ByVal item As T)
            Using New Threading.Lock(Me.SyncLock, True)
                MyBase.Insert(index, item)

                Me.AddHandlers(item)

            End Using

        End Sub

        Public Shadows Sub InsertRange(ByVal index As Integer, ByVal collection As IEnumerable(Of T))
            Using New Threading.Lock(Me.SyncLock, True)
                MyBase.InsertRange(index, collection)

                For Each tmpItem As Object In collection
                    Me.AddHandlers(tmpItem)

                Next

            End Using

        End Sub

        Public Shadows Function Remove(ByVal item As T) As Boolean
            Using New Threading.Lock(Me.SyncLock, True)
                Me.RemoveHandlers(item)

                Return MyBase.Remove(item)

            End Using

        End Function

        Public Shadows Function RemoveAll(ByVal match As Predicate(Of T)) As Integer
            Using New Threading.Lock(Me.SyncLock, True)
                For Each tmpItem As Object In Me
                    Me.RemoveHandlers(tmpItem)

                Next

                Return MyBase.RemoveAll(match)

            End Using

        End Function

        Public Shadows Sub RemoveAt(ByVal index As Integer)
            Using New Threading.Lock(Me.SyncLock, True)
                Dim tmpItem As Object = Me(index)

                MyBase.RemoveAt(index)

                Me.RemoveHandlers(tmpItem)

            End Using

        End Sub

        Public Shadows Sub RemoveRange(ByVal index As Integer, ByVal count As Integer)
            Using New Threading.Lock(Me.SyncLock, True)
                For i As Integer = index To index + count
                    Me.RemoveHandlers(Me(i))

                Next

                MyBase.RemoveRange(index, count)

            End Using

        End Sub

#Region "Element Diposing Support"

        Private Sub AddHandlers(ByVal element As Object)
            If Not element Is Nothing Then
                If Array.IndexOf(element.GetType.GetInterfaces, GetType(IPersistentBaseObject)) <> -1 Then
                    AddHandler CType(element, IPersistentBaseObject).Disposing, AddressOf Element_Disposing

                End If
            End If

        End Sub

        Private Sub RemoveHandlers(ByVal element As Object)
            If Not element Is Nothing Then
                If Array.IndexOf(element.GetType.GetInterfaces, GetType(IPersistentBaseObject)) <> -1 Then
                    RemoveHandler CType(element, IPersistentBaseObject).Disposing, AddressOf Element_Disposing

                End If
            End If

        End Sub

        Private Sub Element_Disposing(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.Remove(sender)

            If Not Me.Owner Is Nothing Then
                Me.Owner.Save()

            End If

        End Sub

#End Region

        '#Region "Query"

        '        Public Sub PerformQuery(ByVal data As QueryDataCollection)

        '        End Sub

        '        Friend Sub PerformQuery(ByRef result As DataSet, ByVal data As QueryDataCollection, Optional ByVal includeReferenceData As Boolean = True)
        '            For Each tmpValue As Object In Me
        '                If TypeOf tmpValue Is BaseObject Then
        '                    If includeReferenceData Then

        '                    End If

        '                    CType(tmpValue, BaseObject).PerformQuery(result, data)

        '                End If
        '            Next

        '        End Sub

        '#End Region

    End Class

End Namespace