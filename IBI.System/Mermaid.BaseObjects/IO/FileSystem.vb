Imports System.IO

Namespace IO

    Public Class FileSystem

#Region "Enums"

        Public Enum CleanDirectoryOptions As Integer
            IgnoreSubDirectories = 0
            CleanSubDirectories = 1
            DeleteSubDirectories = 2

        End Enum

#End Region

        Public Shared Sub PrepareDirectoryPath(ByVal path As String)
            Dim tmpDirectoryInfo As New DirectoryInfo(path)

            FileSystem.PrepareDirectoryPath(tmpDirectoryInfo)

        End Sub

        Public Shared Sub PrepareDirectoryPath(ByVal directoryInfo As DirectoryInfo)
            If Not directoryInfo Is Nothing Then
                If Not directoryInfo.Exists Then
                    directoryInfo.Create()

                    'Dim tmpFullDirectoryPath As String = directoryInfo.FullName.Replace(directoryInfo.Root.FullName, String.Empty)

                    'Dim tmpDirectoryList As String() = tmpFullDirectoryPath.Split(New Char() {Char.Parse("\"), Char.Parse("/")})
                    'Dim tmpPartialPath As String = directoryInfo.Root.FullName

                    'For i As Integer = 0 To tmpDirectoryList.Length - 1
                    '    Dim tmpDirectoryName As String = tmpDirectoryList(i)

                    '    If Not tmpDirectoryName = String.Empty Then
                    '        Dim tmpPartialDirectoryInfo As DirectoryInfo = New DirectoryInfo(Path.Combine(tmpPartialPath, tmpDirectoryName))

                    '        If Not tmpPartialDirectoryInfo.Exists Then
                    '            SetAttr(tmpPartialDirectoryInfo.Parent.FullName, FileAttribute.Normal)

                    '            tmpPartialDirectoryInfo.Create()

                    '        End If

                    '        tmpPartialPath = Path.Combine(tmpPartialPath, tmpDirectoryName)

                    '    End If
                    'Next
                End If
            End If

        End Sub

        Public Shared Sub PrepareFilePath(ByVal path As String)
            Dim tmpFileInfo As New FileInfo(path)

            FileSystem.PrepareDirectoryPath(tmpFileInfo.Directory)

        End Sub

        Public Shared Sub PrepareFilePath(ByVal fileInfo As FileInfo)
            FileSystem.PrepareDirectoryPath(fileInfo.Directory)

        End Sub

        Public Shared Sub DeleteFile(ByVal filePath As String)
            FileSystem.DeleteFile(New FileInfo(filePath))

        End Sub

        Public Shared Sub DeleteFile(ByVal fileInfo As FileInfo)
            Try

                If fileInfo.Exists Then
                    Dim isDirectory As Boolean = (File.GetAttributes(fileInfo.FullName) And FileAttributes.Directory) = FileAttributes.Directory

                    If Not isDirectory Then
                        fileInfo.Delete()

                        Dim tmpMD5FileInfo As New FileInfo(fileInfo.FullName & ".md5")

                        If tmpMD5FileInfo.Exists Then
                            tmpMD5FileInfo.Delete()

                        End If

                    Else
                        Dim tmpDirectory As New DirectoryInfo(fileInfo.FullName)

                        FileSystem.DeleteDirectory(tmpDirectory)

                    End If
                End If

            Catch ex As Exception
                'SILENT: Catch

            End Try
        End Sub

        Public Shared Sub DeleteDirectory(ByVal directoryPath As String, Optional ByVal deleteContent As Boolean = True)
            Try
                Dim tmpDirectoryInfo As New DirectoryInfo(directoryPath)

                If tmpDirectoryInfo.Exists Then
                    tmpDirectoryInfo.Delete(deleteContent)

                End If

            Catch ex As Exception
                'SILENT: Catch

            End Try
        End Sub

        Public Shared Sub DeleteDirectory(ByVal directoryInfo As DirectoryInfo, Optional ByVal deleteContent As Boolean = True)
            Try
                If directoryInfo.Exists Then
                    directoryInfo.Delete(deleteContent)

                End If

            Catch ex As Exception
                'SILENT: Catch

            End Try
        End Sub

        Public Shared Sub CleanDirectory(ByVal directoryPath As String, ByVal cleanOption As CleanDirectoryOptions)
            Try
                Dim tmpDirectoryInfo As New DirectoryInfo(directoryPath)

                If tmpDirectoryInfo.Exists Then
                    CleanDirectory(tmpDirectoryInfo, cleanOption)

                End If

            Catch ex As Exception
                'SILENT: Catch

            End Try
        End Sub

        Public Shared Sub CleanDirectory(ByVal directoryInfo As DirectoryInfo, ByVal cleanOption As CleanDirectoryOptions)
            Try
                For Each tmpFile As FileInfo In directoryInfo.GetFiles
                    DeleteFile(tmpFile)

                Next

                If cleanOption = CleanDirectoryOptions.CleanSubDirectories Or cleanOption = CleanDirectoryOptions.DeleteSubDirectories Then
                    For Each tmpDirectory As DirectoryInfo In directoryInfo.GetDirectories
                        If cleanOption = CleanDirectoryOptions.CleanSubDirectories Then
                            CleanDirectory(tmpDirectory, CleanDirectoryOptions.CleanSubDirectories)

                        ElseIf cleanOption = CleanDirectoryOptions.DeleteSubDirectories Then
                            DeleteDirectory(tmpDirectory)

                        End If
                    Next
                End If

            Catch ex As Exception
                'SILENT: Catch

            End Try
        End Sub

    End Class

End Namespace