Imports System
Imports System.Xml
Imports System.Xml.Schema
Imports System.Xml.Serialization
Imports System.Text
Imports System.Globalization
Imports System.IO
Imports System.Threading
Imports System.Security.Cryptography

Namespace IO

    ''' <summary>
    ''' MD5 hash of a file. Holds hash data.
    ''' </summary>
    ''' <remarks></remarks>
    <Serializable()> Public Class FileHash

#Region "Variables"

        Private Shared _ComputeLock As New Object

#End Region

#Region "Properties"

        Private Shared ReadOnly Property ComputeLock() As Object
            Get
                Return _ComputeLock

            End Get
        End Property

#End Region

#Region "Fields"

        Private _Data As Byte()

#End Region

#Region "Consts"

        Private Shared ReadOnly Hasher As HashAlgorithm = New MD5CryptoServiceProvider()
        Public Shared ReadOnly Empty As FileHash = FileHash.FromString("00000000000000000000000000000000")

#End Region

#Region "Constructors"

        ''' <summary>
        ''' Initializes a new instance of the <see cref="FileHash"/> class.
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()

        End Sub

        ''' <summary>
        ''' Initializes a new instance of the <see cref="FileHash"/> class.
        ''' </summary>
        ''' <param name="data">The raw hash data.</param>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">Data of the hash is null.</exception>
        ''' <exception cref="ArgumentException">Data length is invalid.</exception>
        Public Sub New(ByVal data As Byte())
            If data Is Nothing Then
                Throw New ArgumentNullException("data")

            End If

            If data.Length <> 16 Then
                Throw New ArgumentException("Data of the hash is invalid.", "data")

            End If

            Me._Data = data

        End Sub

#End Region

#Region "Implementation"

        ''' <summary>
        ''' Returns a <see cref="String"></see> that represents the current <see cref="Object"></see>.
        ''' </summary>
        ''' <returns>A <see cref="String"></see> that represents the current <see cref="Object"></see>.</returns>
        ''' <remarks></remarks>
        Public Overrides Function ToString() As String
            Dim hashBuilder As New StringBuilder(128)

            For Each b As Byte In Me._Data
                hashBuilder.AppendFormat(b.ToString("x2", CultureInfo.InvariantCulture.NumberFormat))

            Next

            Return hashBuilder.ToString()

        End Function

        ''' <summary>
        ''' Parses the specified input string.
        ''' </summary>
        ''' <param name="input">The input string.</param>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">Input string is null or empty.</exception>
        ''' <exception cref="ArgumentException">Input string has invalid format. Length of the input string must be 32 characters.</exception>
        Private Sub Parse(ByVal input As String)
            If String.IsNullOrEmpty(input) Then
                Throw New ArgumentNullException("input")

            End If

            Dim length As Integer = input.Length / 2

            If length <> 16 Then
                Throw New ArgumentException("Input string is not formated properly.", "input")

            End If

            Dim buffer(length - 1) As Byte

            For i As Integer = 0 To length - 1
                buffer(i) = Byte.Parse(input.Substring(i * 2, 2), NumberStyles.HexNumber, CultureInfo.InvariantCulture.NumberFormat)

            Next

            Me._Data = buffer

        End Sub

        ''' <summary>
        ''' Determines whether the specified <see cref="Object"></see> is equal to the current <see cref="Object"></see>.
        ''' </summary>
        ''' <param name="obj">The <see cref="Object"></see> to compare with the current <see cref="Object"></see>.</param>
        ''' <returns><c>True</c> if the specified <see cref="Object"></see> is equal to the current <see cref="Object"></see>; otherwise, false.</returns>
        ''' <remarks></remarks>
        Public Overrides Function Equals(ByVal obj As Object) As Boolean
            Dim hash As FileHash = obj

            If Not hash Is Nothing Then
                If hash._Data.Length <> Me._Data.Length Then
                    Return False

                End If

                Dim length As Integer = Me._Data.Length

                For i As Integer = 0 To length - 1
                    If hash._Data(i) <> Me._Data(i) Then
                        Return False

                    End If
                Next

                Return True

            End If

            Return MyBase.Equals(obj)

        End Function

        ''' <summary>
        ''' Serves as a hash function for a particular type. <see cref="M:System.Object.GetHashCode"></see> is suitable for use in hashing algorithms and data structures like a hash table.
        ''' </summary>
        ''' <returns>A hash code for the current <see cref="Object"></see>.</returns>
        ''' <remarks></remarks>
        ''' <exception cref="InvalidOperationException">Data in the hash is invalid. In most cases this occure when data length is wrong.</exception>
        Public Overrides Function GetHashCode() As Integer
            Dim length As Integer = Me._Data.Length

            If length Mod 4 <> 0 Then
                Throw New InvalidOperationException("Data of the hash is invalid.")

            End If

            Dim result As Integer = 0

            Dim i As Integer = 0

            While i < length
                Dim a As Integer = Me._Data(i)
                a = a << 8
                a += Me._Data(i + 1)
                a = a << 8
                a += Me._Data(i + 2)
                a = a << 8
                a += Me._Data(i + 3)

                result ^= a

                i += 4

            End While

            Return result

        End Function

        ''' <summary>
        ''' Computes the hash from specified <see cref="Stream"/>
        ''' </summary>
        ''' <param name="input">The input <see cref="Stream"/>.</param>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">Input stream is not initilized yet.</exception>

        Private Sub ComputeHash(ByVal input As Stream)
            Monitor.Enter(ComputeLock)

            Try
                If input Is Nothing Then
                    Throw New ArgumentNullException("input")

                End If

                Me._Data = FileHash.Hasher.ComputeHash(input)

            Finally
                Monitor.Exit(ComputeLock)

            End Try
        End Sub

        ''' <summary>
        ''' Saves the <see cref="FileHash"/> using the <see cref="FileInfo"/> <see cref="Object"/>.
        ''' </summary>
        ''' <param name="fileInfo"><see cref="FileInfo"/> <see cref="Object"/> defining where to save the <see cref="FileHash"/>.</param>
        ''' <remarks></remarks>
        Public Sub Save(ByVal fileInfo As FileInfo)
            Dim writer As StreamWriter = fileInfo.CreateText()

            Try
                writer.WriteLine(Me.ToString())

            Finally
                writer.Dispose()

            End Try

        End Sub

#End Region

#Region "Implementation - Static"

        ''' <summary>
        ''' Creates hash from hex encoded string.
        ''' </summary>
        ''' <param name="input">The hex encoded string.</param>
        ''' <returns><see cref="FileHash"/> creted from specified string.</returns>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">Input string is null or empty.</exception>
        ''' <exception cref="ArgumentException">Input string not formated properly. Length of the input string must be 36 characters.</exception>
        Public Shared Function FromString(ByVal input As String) As FileHash
            If String.IsNullOrEmpty(input) Then
                Throw New ArgumentNullException("input")

            End If

            If input.Length <> 32 Then
                Throw New ArgumentException("Input string is not formated properly.", "input")

            End If

            Dim hash As FileHash = New FileHash()
            hash.Parse(input)
            Return hash

        End Function

        ''' <summary>
        ''' Creates hash from specified <see cref="FileInfo"/>. Please note that this method loads spcified file, so it may be accessible. Also possible that this function will take time.
        ''' </summary>
        ''' <param name="info">The file information.</param>
        ''' <returns><see cref="FileHash"/> creted from specified <see cref="FileInfo"/>.</returns>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">File information is null.</exception>
        ''' <exception cref="FileNotFoundException">Phisical file does not exists.</exception>
        Public Shared Function FromFile(ByVal info As FileInfo, ByVal save As Boolean) As FileHash
            Try
                If info Is Nothing Then
                    Throw New ArgumentNullException("info")

                End If

                If Not info.Exists Then
                    Throw New FileNotFoundException()

                End If

                Dim md5File As New FileInfo(info.FullName & ".md5")

                If md5File.Exists Then
                    Return FileHash.FromMd5File(md5File)

                Else
                    Dim stream As FileStream = info.OpenRead()

                    Try
                        Dim hash As FileHash = FileHash.FromStream(stream)

                        If save Then
                            hash.Save(md5File)

                        End If

                        Return hash

                    Finally
                        stream.Dispose()

                    End Try
                End If

                Return Nothing

            Catch ex As Exception
                BaseUtility.OnExceptionOccurred(Nothing, New EventArgs.ExceptionEventArgs(ex, Enums.ExceptionCategories.Misc))

                Return Nothing

            End Try
        End Function

        ''' <summary>
        ''' Creates hash from specified <see cref="FileInfo"/>. Please note that this method loads spcified file, so it may be accessible. Also possible that this function will take time.
        ''' </summary>
        ''' <param name="info">The file inforamtion.</param>
        ''' <param name="force"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function FromFile(ByVal info As FileInfo, ByVal force As Boolean, ByVal save As Boolean) As FileHash
            Try
                If Not force Then
                    Return FromFile(info, save)

                Else
                    If info Is Nothing Then
                        Throw New ArgumentNullException("info")

                    End If

                    If Not info.Exists Then
                        Throw New FileNotFoundException()

                    End If

                    Dim md5File As New FileInfo(info.FullName & ".md5")

                    If md5File.Exists Then
                        md5File.Delete()

                    End If

                    Dim stream As FileStream = info.OpenRead()

                    Try
                        Dim hash As FileHash = FileHash.FromStream(stream)

                        If save Then
                            hash.Save(md5File)

                        End If

                        Return hash

                    Finally
                        stream.Dispose()

                    End Try

                End If

                Return Nothing

            Catch ex As Exception
                'SILENT

                Return Nothing

            End Try
        End Function

        Public Shared Function FromMd5File(ByVal md5File As FileInfo) As FileHash
            If md5File Is Nothing Then
                Throw New ArgumentNullException("md5File")

            End If

            If Not md5File.Exists Then
                Throw New FileNotFoundException()

            End If

            Dim md5Definition As String = String.Empty

            Dim reader As StreamReader = md5File.OpenText()

            Try
                md5Definition = reader.ReadLine()

            Finally
                reader.Dispose()

            End Try

            Return FileHash.FromString(md5Definition)

        End Function

        ''' <summary>
        ''' Creates hash from specified file path. Please note that this method loads spcified file, so it may be accessible. Also possible that this function will take time.
        ''' </summary>
        ''' <param name="filePath">The file path.</param>
        ''' <returns><see cref="FileHash"/> creted from specified file path.</returns>
        ''' <remarks></remarks>'
        ''' <exception cref="ArgumentNullException">File path is null or empty.</exception>
        ''' <exception cref="FileNotFoundException">Physical file does not exists.</exception>
        Public Shared Function FromFile(ByVal filePath As String, ByVal save As Boolean) As FileHash
            If String.IsNullOrEmpty(filePath) Then
                Throw New ArgumentNullException("filePath")

            End If

            Return FileHash.FromFile(New FileInfo(filePath), save)

        End Function

        ''' <summary>
        ''' Creates hash from specified stream. Possible that this function will take time.
        ''' </summary>
        ''' <param name="input">The input stream.</param>
        ''' <returns><see cref="FileHash"/> created from specified <see cref="FileInfo"/>.</returns>
        ''' <remarks></remarks>
        ''' <exception cref="ArgumentNullException">Input stream is not initialized yet.</exception>
        Public Shared Function FromStream(ByVal input As Stream) As FileHash
            If input Is Nothing Then
                Throw New ArgumentNullException("input")

            End If

            Dim hash As FileHash = New FileHash()
            hash.ComputeHash(input)
            Return hash

        End Function

#End Region

    End Class

End Namespace