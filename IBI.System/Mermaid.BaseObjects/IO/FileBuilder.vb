Imports System.IO

Namespace IO

    Public Class FileBuilder

#Region "Variables"

        Private _IsInitialized As Boolean
        Private _FileData As FileData
        Private _Stream As FileStream
        Private _OverwriteExistingFiles As Boolean
        Private _TempPath As String
        
#End Region

#Region "Properties"

        Private Property IsInitialized() As Boolean
            Get
                Return Me._IsInitialized

            End Get
            Set(ByVal value As Boolean)
                Me._IsInitialized = value

            End Set
        End Property

        Public ReadOnly Property FileData() As FileData
            Get
                Return Me._FileData

            End Get
        End Property

        Private ReadOnly Property Stream() As FileStream
            Get
                If Me._Stream Is Nothing Then
                    'Me._Stream = File.Create(Me.FileData.Path)
                    Me._Stream = File.Create(Me.TempPath)

                End If

                Return Me._Stream

            End Get
        End Property

        Public Property OverwriteExistingFiles() As Boolean
            Get
                Return Me._OverwriteExistingFiles

            End Get
            Set(ByVal value As Boolean)
                Me._OverwriteExistingFiles = value

            End Set
        End Property

        Private Property TempPath() As String
            Get
                Return Me._TempPath

            End Get
            Set(ByVal value As String)
                Me._TempPath = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal fileData As FileData)
            MyBase.New()

            If fileData Is Nothing Then Throw New NullReferenceException("FileData cannot be null")

            Me._FileData = fileData

            Me.TempPath = Path.Combine(Path.GetTempPath, Path.GetFileName(fileData.Path))

        End Sub

        Public Function AppendNextPortion(ByVal data As Byte()) As Boolean
            If Not Me.IsInitialized Then
                Me.Initialize()

            End If

            If data IsNot Nothing Then
                Me.Stream.Write(data, 0, data.Length)

                If Me.Stream.Length = Me.FileData.Size Then
                    Me.Stream.Dispose()

                    FileSystem.DeleteFile(Me.FileData.Path)

                    File.Move(Me.TempPath, Me.FileData.Path)

                    Return True

                Else
                    Return False

                End If

            Else
                Throw New NullReferenceException("Data cannot be null")

            End If
        End Function

        Private Sub Initialize()
            Me.IsInitialized = True

            Dim fileExists As Boolean = False

            FileSystem.DeleteFile(Me.TempPath)

            Dim tmpFileInfo As New FileInfo(Me.FileData.Path)

            If tmpFileInfo.Exists Then
                If Me.OverwriteExistingFiles Then
                    Dim tmpLocalHash As FileHash = FileHash.FromFile(tmpFileInfo, False)
                    fileExists = True

                    If Not Me.FileData.MD5 Is Nothing Then
                        If tmpLocalHash.ToString = Me.FileData.MD5.ToString Then
                            fileExists = True

                        Else
                            'tmpFileInfo.Delete()

                        End If

                    Else
                        Throw New ArgumentException("FileData.MD5 cannot be null.")

                    End If

                Else
                    Throw New Exception("File already exists")

                End If
            End If

            FileSystem.PrepareFilePath(Me.FileData.Path)

        End Sub

    End Class

End Namespace