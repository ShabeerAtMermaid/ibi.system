Namespace IO

    <Serializable()> _
    Public Class FileData

#Region "Variables"

        Private _Path As String
        Private _Size As Long
        Private _MD5 As FileHash

#End Region

#Region "Properties"

        Public Property Path() As String
            Get
                Return Me._Path

            End Get
            Set(ByVal value As String)
                Me._Path = value

            End Set
        End Property

        Public Property Size() As Long
            Get
                Return Me._Size

            End Get
            Set(ByVal value As Long)
                Me._Size = value

            End Set
        End Property

        Public Property MD5() As FileHash
            Get
                Return Me._MD5

            End Get
            Set(ByVal value As FileHash)
                Me._MD5 = value

            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub New(ByVal path As String, ByVal size As Long, ByVal md5 As FileHash)
            MyBase.New()

            Me.Path = path
            Me.Size = size
            Me.MD5 = md5

        End Sub

        Public Sub New(ByVal path As String, ByVal size As Long)
            MyBase.New()

            Me.Path = path
            Me.Size = size
            
        End Sub

        Public Sub Dispose()
            Me._Path = Nothing
            Me._Size = Nothing
            Me._MD5 = Nothing

        End Sub

    End Class


End Namespace