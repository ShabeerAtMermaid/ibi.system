Imports System.IO

Namespace IO

    Public Class FileSplitter

#Region "Variables"

        Private _FileData As FileData
        Private _Offset As Long

#End Region

#Region "Properties"

        Public ReadOnly Property FileData() As FileData
            Get
                Return Me._FileData

            End Get
        End Property

        Private Property Offset() As Long
            Get
                Return Me._Offset

            End Get
            Set(ByVal value As Long)
                Me._Offset = value

            End Set
        End Property

#End Region

        Public Sub New(ByVal fileData As FileData)
            MyBase.New()

            Me._FileData = fileData

        End Sub

        Public Function GetNextPortion(Optional ByVal portionSize As Integer = 32768) As Byte()
            If Me.Offset >= Me.FileData.Size Then Return Nothing

            Dim sizeToSend As Integer = Math.Min(Me.FileData.Size - Offset, portionSize)

            Dim tmpFileInfo As New FileInfo(Me.FileData.Path)

            Dim buffer(sizeToSend - 1) As Byte
            Dim tmpStream As Stream = tmpFileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite)

            Try
                tmpStream.Position = Me.Offset
                tmpStream.Read(buffer, 0, buffer.Length)

            Finally
                tmpStream.Dispose()

            End Try

            Me.Offset += sizeToSend

            Return buffer

        End Function

    End Class

End Namespace