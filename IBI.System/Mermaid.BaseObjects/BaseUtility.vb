Imports Microsoft.Win32
Imports System.IO

Public Class BaseUtility

#Region "Variables"

    <ThreadStatic()> Private Shared _ContextID As String = New Guid().ToString

#End Region

#Region "Properties"

    Friend Shared Property ContextID() As String
        Get
            Return _ContextID

        End Get
        Set(ByVal value As String)
            _ContextID = value

        End Set
    End Property

#End Region

#Region "Events"

    Public Shared Event ExceptionOccurred As System.EventHandler(Of EventArgs.ExceptionEventArgs)

#End Region

    Friend Shared Sub OnExceptionOccurred(ByVal sender As Object, ByVal e As EventArgs.ExceptionEventArgs)
        RaiseEvent ExceptionOccurred(sender, e)

    End Sub

    Friend Shared Function ExtractEmbeddedResource(ByVal resourceName As String) As String
        Dim tmpStream As Stream = System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream(GetType(BaseUtility).Namespace & "." & resourceName)

        Dim tmpBuffer(tmpStream.Length - 1) As Byte
        tmpStream.Read(tmpBuffer, 0, tmpBuffer.Length)

        Return System.Text.Encoding.UTF8.GetString(tmpBuffer)

    End Function

    Public Class IDCounter

#Region "Variables"

        Private Shared _CurrentID As Integer = 0
        Private Shared _Lock As New Object
        Private Shared WithEvents _TmrSave As Timers.Timer
        
#End Region

#Region "Properties"

        Private Shared Property CurrentID() As Integer
            Get
                Try
                    If _CurrentID = 0 Then
                        Dim tmpKeySoftware As RegistryKey = My.Computer.Registry.LocalMachine.CreateSubKey("Software")
                        Dim tmpKeyCompany As RegistryKey = tmpKeySoftware.CreateSubKey("mermaid technology")
                        Dim tmpKeyProgram As RegistryKey = tmpKeyCompany.CreateSubKey("mermaid BaseObjects")

                        _CurrentID = tmpKeyProgram.GetValue("LocalIDCount")

                    End If

                    Return _CurrentID

                Catch ex As Exception
                    'SILENT

                End Try
            End Get
            Set(ByVal value As Integer)
                _CurrentID = value

                TmrSave.Enabled = True

            End Set
        End Property

        Private Shared Property Lock() As Object
            Get
                Return _Lock

            End Get
            Set(ByVal value As Object)
                _Lock = value

            End Set
        End Property

        Private Shared ReadOnly Property TmrSave() As Timers.Timer
            Get
                If _TmrSave Is Nothing Then
                    _TmrSave = New Timers.Timer
                    _TmrSave.Interval = TimeSpan.FromSeconds(10).TotalMilliseconds

                End If

                Return _TmrSave

            End Get
        End Property

#End Region

        Public Shared Function GetID() As Integer
            Using New Threading.Lock(Lock, True)
                Dim tmpID As Integer = CurrentID

                If tmpID < Integer.MaxValue Then
                    If tmpID < Integer.MaxValue / 2 Then
                        tmpID = Integer.MaxValue / 2

                    Else
                        tmpID += 1

                    End If

                Else
                    tmpID = Integer.MaxValue / 2

                End If

                CurrentID = tmpID

                Return tmpID

            End Using

        End Function

        Public Shared Sub SetID(ByVal ID As Integer)
            Using New Threading.Lock(Lock, True)
                CurrentID = ID

            End Using

        End Sub

        Private Shared Sub TmrSave_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles _TmrSave.Elapsed
            Try
                TmrSave.Enabled = False

                Dim tmpKeySoftware As RegistryKey = My.Computer.Registry.LocalMachine.CreateSubKey("Software")
                Dim tmpKeyCompany As RegistryKey = tmpKeySoftware.CreateSubKey("mermaid technology")
                Dim tmpKeyProgram As RegistryKey = tmpKeyCompany.CreateSubKey("mermaid BaseObjects")

                tmpKeyProgram.SetValue("LocalIDCount", CurrentID)

            Catch ex As Exception
                'SILENT

            End Try
        End Sub

    End Class

End Class
