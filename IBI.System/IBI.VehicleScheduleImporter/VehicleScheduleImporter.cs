﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.Shared.VehicleSchedules.Interfaces;
using IBI.Shared.Interfaces.VehicleSchedules;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using IBI.DataAccess.DBModels;
using IBI.Cache;
using System.Data.Entity.Spatial;
using IBI.Shared;
using IBI.Shared.Diagnostics;

namespace IBI.VehicleScheduleImporter
{
    public class VehicleScheduleImporter : IVehicleScheduleImporter
    {
        #region Attributes
        private int _customerId;
        private string _url;

        private StringBuilder sb = new StringBuilder();
        private string logFileName = DateTime.Now.ToString("yyyy-mm-dd HHmmss") + ".log";
        int journeyCount = 0;
        #endregion

        private Hashtable ParseCalendarEntries(JObject calendar)
        {
            Hashtable table = new Hashtable();
            try
            {
                foreach (var entry in calendar.Children<JProperty>().ToList())
                {
                    if (!table.ContainsKey((string)entry.Value))
                    {
                        ArrayList list = new ArrayList();
                        list.Add(entry.Name);
                        table[(string)entry.Value] = list;
                    }
                    else
                    {
                        ArrayList list = (ArrayList)table[(string)entry.Value];
                        list.Add(entry.Name);
                        table[(string)entry.Value] = list;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return table;
        }

        private void parseJourneyJsonData()
        {

        }

        private static string ExternalReferencePrefix
        {
            get { return System.Configuration.ConfigurationSettings.AppSettings["ExternalReferencePrefix"].ToString(); }
            
        }
        public void ImportVehicleSchedules()
        {
            try
            {
                //sb.AppendLine(DateTime.Now + " - ImportVehicleSchedules started");
                JArray weekDays = JArray.Parse(@"[1,2,3,4,5]");
                JArray saturday = JArray.Parse(@"[6]");
                JArray sunday = JArray.Parse(@"[7]");
                JArray newYear = JArray.Parse(@"[8]");
                JArray xMas = JArray.Parse(@"[9]");

                DateTime startJourneyTime = DateTime.Now.Date;
                DateTime endJourneyTime = startJourneyTime.AddDays(AppSettings.GetNorgebussFutureDays());
                AppUtility.Log2File("NG_Journeys", DateTime.Now + " - Loading data from " + startJourneyTime + " TO " + endJourneyTime);

                Hashtable vehicleScheduleMap = new Hashtable();

                JObject vehicleScheduleData = downloadJsonData();
                DateTime startDB = DateTime.Now;
                ArrayList vScheduleList = new ArrayList();
                var lines = vehicleScheduleData["lines"].Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "number") != null).ToList();
                //foreach (var line in lines)
                {
                    //string lineValue = (string)line["number"];
                    //}
                    //var v344 = vehicleScheduleData["lines"].Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "number") != null).FirstOrDefault();
                    //{
                    /*
                    JToken stopList;
                    JToken stopSequences;
                    JToken calendar;
                    JToken trips;
                    

                    stopList = line["stoplist"];
                    stopSequences = line["stopsequence"];
                    calendar = vehicleScheduleData["calendar"];
                    var timetable = line["timetable"];
                    trips = line["trips"];
                     
                    vehicleworkings = vehicleScheduleData["vehicleworkings"];
                    //InitialiseDataElements(vehicleScheduleData, line, out stopList, out stopSequences, out calendar, out trips, out vehicleworkings);
                    Hashtable directionOneStopsList = new Hashtable();
                    ArrayList directionOneStopIdList = new ArrayList();
                    Hashtable directionTwoStopsList = new Hashtable();
                    ArrayList directionTwoStopIdList = new ArrayList();
                    loadStopsInfo(stopList, stopSequences, directionOneStopsList, "1", directionOneStopIdList);
                    loadStopsInfo(stopList, stopSequences, directionTwoStopsList, "2", directionTwoStopIdList);

                    var weekDayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, weekDays)) != null).FirstOrDefault();
                    var saturdayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, saturday)) != null).FirstOrDefault();
                    var sundayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, sunday)) != null).FirstOrDefault();
                    var newYearTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, newYear)) != null).FirstOrDefault();
                    var xMasTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, xMas)) != null).FirstOrDefault();
                    
                    var weekDayTripsPlannedData = trips.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, weekDays)) != null).FirstOrDefault();
                    var saturdayTripsPlannedData = trips.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, saturday)) != null).FirstOrDefault();
                    var sundayTripsPlannedData = trips.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, sunday)) != null).FirstOrDefault();
                    var newYearTripsPlannedData = trips.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, newYear)) != null).FirstOrDefault();
                    var xMasTripsPlannedData = trips.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, xMas)) != null).FirstOrDefault();
                    */
                    JToken vehicleworkings;
                    JToken calendar;

                    vehicleworkings = vehicleScheduleData["vehicleworkings"];
                    calendar = vehicleScheduleData["calendar"];
                    Hashtable calendarEntries = ParseCalendarEntries((JObject)calendar);
                    var weekDayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, weekDays)) != null).FirstOrDefault();
                    var saturdayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, saturday)) != null).FirstOrDefault();
                    var sundayTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, sunday)) != null).FirstOrDefault();
                    var newYearTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, newYear)) != null).FirstOrDefault();
                    var xMasTripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, xMas)) != null).FirstOrDefault();


                    ArrayList mondayEntries = (ArrayList)calendarEntries["1"];
                    ArrayList tuesdayEntries = (ArrayList)calendarEntries["2"];
                    ArrayList wednesdayEntries = (ArrayList)calendarEntries["3"];
                    ArrayList thursdayEntries = (ArrayList)calendarEntries["4"];
                    ArrayList fridayEntries = (ArrayList)calendarEntries["5"];
                    ArrayList saturdayEntries = (ArrayList)calendarEntries["6"];
                    ArrayList sundayEntries = (ArrayList)calendarEntries["7"];
                    ArrayList newYearEntries = (ArrayList)calendarEntries["8"];
                    ArrayList xMasEntries = (ArrayList)calendarEntries["9"];

                    //sb.AppendLine(DateTime.Now + " - Initial data read");
                    int vehicleScheduleIndex = 1000;
                    //sb.AppendLine(DateTime.Now + " - Starting weekdays import");
                    foreach (var weekDayTrip in weekDayTripsData)
                    {
                        if (string.Compare(weekDayTrip.Key, "vehicleworking", true) != 0) continue;
                        Console.WriteLine(DateTime.Now + " - Monday import started");
                        parseTripData(mondayEntries, vScheduleList, weekDayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, weekDays);
                        Console.WriteLine(DateTime.Now + " - Tuesday import started");
                        parseTripData(tuesdayEntries, vScheduleList, weekDayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, weekDays);
                        Console.WriteLine(DateTime.Now + " - Wednesday import started");
                        parseTripData(wednesdayEntries, vScheduleList, weekDayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, weekDays);
                        Console.WriteLine(DateTime.Now + " - Thursday import started");
                        parseTripData(thursdayEntries, vScheduleList, weekDayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, weekDays);
                        Console.WriteLine(DateTime.Now + " - Friday import started");
                        parseTripData(fridayEntries, vScheduleList, weekDayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, weekDays);
                    }
                    foreach (var saturdayTrip in saturdayTripsData)
                    {
                        if (string.Compare(saturdayTrip.Key, "vehicleworking", true) != 0) continue;
                        Console.WriteLine(DateTime.Now + " - Saturday import started");
                        parseTripData(saturdayEntries, vScheduleList, saturdayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, saturday);
                    }
                    foreach (var sundayTrip in sundayTripsData)
                    {
                        if (string.Compare(sundayTrip.Key, "vehicleworking", true) != 0) continue;
                        Console.WriteLine(DateTime.Now + " - Sunday import started");
                        parseTripData(sundayEntries, vScheduleList, sundayTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, sunday);
                    }
                    foreach (var newYearTrip in newYearTripsData)
                    {
                        if (string.Compare(newYearTrip.Key, "vehicleworking", true) != 0) continue;
                        Console.WriteLine(DateTime.Now + " - New Year Eve import started");
                        parseTripData(newYearEntries, vScheduleList, newYearTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, newYear);
                    }
                    foreach (var xMasTrip in xMasTripsData)
                    {
                        if (string.Compare(xMasTrip.Key, "vehicleworking", true) != 0) continue;
                        Console.WriteLine(DateTime.Now + " - XMAS import started");
                        parseTripData(xMasEntries, vScheduleList, xMasTrip, vehicleScheduleIndex, startJourneyTime, endJourneyTime, lines, xMas);
                    }
                }
                //sb.AppendLine(DateTime.Now + " - Data import completed");
                TimeSpan diff = DateTime.Now - startDB;
                Console.WriteLine("It took {0} minutes to complete parsing data", diff.TotalMinutes);
                startDB = DateTime.Now;
                IBI.Cache.JourneyCache.FillRouteDirections(_customerId);
                //IBI.Cache.JourneyCache.FillSchedules(_customerId);
                IBI.Cache.JourneyCache.FillSchedulesWithAddons(_customerId);
                //using (IBIDataModel dbContext = new IBIDataModel())
                {
                    int vehicleScheduleProcessed = 1;
                    List<ScheduleVehicleJourney> listToProcess = new List<ScheduleVehicleJourney>();
                    foreach (VehicleSchedule tmpVehicleSchedule in vScheduleList)
                    {
                        listToProcess.AddRange(tmpVehicleSchedule.Journeys);
                        if (vehicleScheduleProcessed % 2 != 0 && vehicleScheduleProcessed < vScheduleList.Count)
                        {
                            vehicleScheduleProcessed++;
                            continue;
                        }

                        DateTime a = DateTime.Now;
                        Console.WriteLine("Journeys to progress are: {0}", listToProcess.Count);
                        ProcessJourney(listToProcess);
                        TimeSpan b = DateTime.Now - a;
                        vehicleScheduleProcessed += 1;
                        Console.WriteLine("It took {0} seconds to sync journeys {1}", b.TotalSeconds, vehicleScheduleProcessed);
                        System.Threading.Thread.Sleep(500);
                        listToProcess.Clear();
                    }
                }
                diff = DateTime.Now - startDB;
                Console.WriteLine("It took {0} minutes to complete database sync", diff.TotalMinutes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                //sb.AppendLine(DateTime.Now + " - Message: " + ex.Message);
                //sb.AppendLine(DateTime.Now + " - Stacktrace: " + ex.StackTrace);
            }
            //File.AppendAllText(logFileName, sb.ToString());
            sb.Clear();
        }

        private static void InitialiseDataElements(JObject vehicleScheduleData, JObject line, out JToken stopList, out JToken stopSequences, out JToken calendar, out JToken trips, out JToken vehicleworkings)
        {
            stopList = line["stoplist"];
            stopSequences = line["stopsequence"];
            calendar = vehicleScheduleData["calendar"];
            var timetable = line["timetable"];
            trips = line["trips"];
            vehicleworkings = vehicleScheduleData["vehicleworkings"];
        }
        ArrayList tripIds = new ArrayList();
        Hashtable cacheList = new Hashtable();
        private void parseTripData(ArrayList dayEntries, ArrayList vScheduleList, KeyValuePair<string, JToken> dayTrip, int vehicleScheduleIndex_1, DateTime startJourneyTime, DateTime endJourneyTime, List<JObject> lines, JArray weekday)
        {
            IList<JToken> vehicleSchedules = dayTrip.Value.Children().ToList();
            int number = 1;
            foreach (JToken vehicleSchedule in vehicleSchedules)
            {
                int vehicleScheduleIndex = int.Parse(vehicleSchedule.Value<JProperty>().Name);
                foreach (string date in dayEntries)
                {
                    DateTime validDate = DateTime.Parse(date);
                    if (validDate >= endJourneyTime || validDate < startJourneyTime)
                    {
                        continue;
                    }
                    string vehicleScheduleExternalRef = String.Format("{0}-{1}-{2}", ExternalReferencePrefix, DateTime.Parse(date).ToString("yyyyMMdd"), vehicleScheduleIndex);
                     
                    //var directionOne = weekDayTripsPlannedData["direction"]["1"];

                    VehicleSchedule vs = new VehicleSchedule();
                    vs.Journeys = new List<ScheduleVehicleJourney>();
                    vs.VehicleScheduleExternalRef = vehicleScheduleExternalRef;
                    int jIndex = 1;
                    foreach (JObject trip in vehicleSchedule.First.Children())
                    {
                        string tripLine = (string)trip["line"];
                        /*if (string.Compare(tripLine, "0") != 0 && string.Compare(tripLine, "TODO") != 0)
                        {
                            continue;
                        }*/
                        string tripId = (string)trip["trip"];
                        string dc = (string)trip["dc"];
                        int n;
                        bool isNumeric = int.TryParse(tripId, out n);
                        ScheduleVehicleJourney svj = null;
                        if (!isNumeric)
                        {
                            svj = new ScheduleVehicleJourney();
                            svj.TripType = tripId;
                            svj.DC = dc;
                            svj.Line = tripLine;
                            svj.TripId = String.Format("{0}-{1}-{2}-{3}", ExternalReferencePrefix, DateTime.Parse(date).ToString("yyyyMMdd"), vehicleScheduleIndex.ToString() + tripId + dc + tripLine, jIndex);
                            svj.Line = "";
                            svj.FromName = "";
                            svj.Destination = dc;
                            svj.ViaName = "";
                            svj.PlannedStartTime = svj.PlannedEndTime = DateTime.Now;
                            svj.VehicleScheduleExternalRef = vehicleScheduleExternalRef;
                        }
                        else
                        {
                            JToken line = null;
                            foreach (var l in lines)
                            {
                                if (string.Compare(tripLine, (string)l["number"], true) == 0)
                                {
                                    line = l;
                                }
                            }
                            if (line == null)
                                return;

                            Hashtable directionOneStopsList = new Hashtable();
                            ArrayList directionOneStopIdList = new ArrayList();
                            Hashtable directionTwoStopsList = new Hashtable();
                            ArrayList directionTwoStopIdList = new ArrayList();
                            if (cacheList.ContainsKey(tripLine + "onestoplist"))
                            {
                                directionOneStopsList = (Hashtable)cacheList[tripLine + "onestoplist"];
                                directionOneStopIdList = (ArrayList)cacheList[tripLine + "onestopidlist"];
                                directionTwoStopsList = (Hashtable)cacheList[tripLine + "twostoplist"];
                                directionTwoStopIdList = (ArrayList)cacheList[tripLine + "twostopidlist"];
                            }
                            else
                            {
                                loadStopsInfo(line["stoplist"], line["stopsequence"], directionOneStopsList, "1", directionOneStopIdList);
                                loadStopsInfo(line["stoplist"], line["stopsequence"], directionTwoStopsList, "2", directionTwoStopIdList);
                                cacheList[tripLine + "onestoplist"] = directionOneStopsList;
                                cacheList[tripLine + "onestopidlist"] = directionOneStopIdList;
                                cacheList[tripLine + "twostoplist"] = directionTwoStopsList;
                                cacheList[tripLine + "twostopidlist"] = directionTwoStopIdList;
                            }

                            var weekDayTripsPlannedData = line["trips"].Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, weekday)) != null).FirstOrDefault();

                            string direction = "1";
                            var tmpTripData = weekDayTripsPlannedData["direction"][direction][tripId] ?? null;
                            if (tmpTripData == null)
                            {
                                direction = "2";
                                tmpTripData = weekDayTripsPlannedData["direction"][direction][tripId] ?? null;
                            }
                            if (tmpTripData == null)
                            {
                                Console.WriteLine("-------------------------------------------------------------------");
                                Console.WriteLine(DateTime.Now + " - The trip not found {0} {1}", trip, date);
                                Console.WriteLine("-------------------------------------------------------------------");
                                continue;
                            }
                            //sb.AppendLine(DateTime.Now + " - processing trip: " + trip);
                            svj = new ScheduleVehicleJourney();
                            svj.TripType = tripId;
                            svj.VehicleScheduleExternalRef = vehicleScheduleExternalRef;
                            svj.StopsInfo = new List<StopInfo>();

                            int stopIndex = 0;
                            bool first = false;
                            bool last = false;
                            string lastStopName = string.Empty;
                            DateTime previousStopTime = DateTime.MinValue;
                            foreach (string plannedTime in tmpTripData.Children())
                            {
                                //sb.AppendLine(DateTime.Now + " - processing stop number: " + stopIndex + ", plannedTime: " + plannedTime);
                                if (!string.IsNullOrEmpty(plannedTime))
                                {
                                    string stopId = string.Empty;
                                    StopInfo info = null;

                                    if (direction == "1")
                                    {
                                        stopId = (string)directionOneStopIdList[stopIndex];
                                        info = new StopInfo((StopInfo)directionOneStopsList[stopId]);
                                    }
                                    else
                                    {
                                        stopId = (string)directionTwoStopIdList[stopIndex];
                                        info = new StopInfo((StopInfo)directionTwoStopsList[stopId]);
                                    }

                                    info.StopId = IBI.Shared.ScheduleHelper.TransformStopNo(stopId, ScheduleHelper.StopPrefix.NORGBUS);

                                    info.PlannedDeparturetime = DateTime.Parse(date + " " + plannedTime);

                                    if (info.PlannedDeparturetime < previousStopTime)
                                    {
                                        info.PlannedDeparturetime = info.PlannedDeparturetime.AddDays(1);
                                    }
                                    previousStopTime = info.PlannedDeparturetime;
                                    if (!first)
                                    {
                                        first = true; ;
                                        svj.FromName = info.LongName;
                                    }
                                    lastStopName = info.LongName;
                                    svj.StopsInfo.Add(info);
                                }
                                stopIndex++;
                            }
                            if (!last)
                            {
                                last = true;
                                svj.Destination = lastStopName;
                            }
                            svj.Line = "";
                            svj.TripId = String.Format("{0}-{1}-{2}-{3}", vehicleScheduleExternalRef.ToString(), direction, tripLine, tripId);
                            if (tripIds.Contains(svj.TripId))
                            {
                                Console.WriteLine("-------------------------------------------------------------------");
                                Console.WriteLine(DateTime.Now + " - The trip already loaded {0} {1}", trip, date);
                                Console.WriteLine("-------------------------------------------------------------------");
                                continue;
                            }
                            svj.PlannedStartTime = svj.StopsInfo[0].PlannedDeparturetime;
                            svj.PlannedEndTime = svj.StopsInfo[svj.StopsInfo.Count - 1].PlannedDeparturetime;
                        }
                        vs.Journeys.Add(svj); //must do something about JourneySequence HERE
                        //ProcessJourney("SAS", trip, svj.StopsInfo);
                        //sb.AppendLine(DateTime.Now + " - Added journey to list for vehicleSchedule: " + vehicleScheduleIndex);
                        journeyCount++;
                        jIndex++;
                        System.Threading.Thread.Sleep(10);
                    }
                    vScheduleList.Add(vs);
                    vehicleScheduleIndex++;
                    //Console.WriteLine(DateTime.Now + " - Journeys processed for date {0} are {1}, new added {2}", date, journeyCount, vs.Journeys.Count);
                    //File.AppendAllText(logFileName, sb.ToString());
                    sb.Clear();
                }
                //sb.AppendLine(DateTime.Now + " - Added vehicle Schedule: " + vehicleScheduleIndex + " to list");
                number++;
            }
        }

        private void loadStopsInfo(JToken stopList, JToken stopSequences, Hashtable directionOneStopsList, string direction, ArrayList stopIdList)
        {
            foreach (string stopid in stopSequences[direction].Children())
            {
                StopInfo sInfo = new StopInfo();
                sInfo.StopId = stopid;
                JToken currentStop = null;
                if (stopList[direction] != null)
                {
                    currentStop = stopList[direction][stopid];
                }

                for (int directid = 1; currentStop == null && directid < 10; directid++)
                {
                    currentStop = stopList[directid.ToString()][stopid];
                }
                sInfo.LongName = (string)currentStop["long name"];
                sInfo.ShortName = (string)currentStop["short name"];
                //sInfo.Latitude = stopList[direction][stopid]["latitude"]==null ? 0 : float.Parse(stopList[direction][stopid]["latitude"].ToString());
                //sInfo.Longitude =stopList[direction][stopid]["longitude"]==null ? 0 : float.Parse(stopList[direction][stopid]["longitude"].ToString());
                sInfo.Latitude = (string)currentStop["latitude"];
                sInfo.Longitude = (string)currentStop["longitude"];
                sInfo.HasAnnouncement = string.Compare((string)currentStop["infotainment"]["announcement"], "1") == 0;
                if (sInfo.HasAnnouncement)
                {
                    sInfo.AnnouncementRadius = int.Parse((string)currentStop["infotainment"]["announcement_radius"] ?? "0");
                    //string announcementTextTag = (string)currentStop["infotainment"]["announcementtext"];
                    //if (string.Compare(announcementTextTag, "long name") == 0)
                    //{
                    //    sInfo.AnnouncementText = sInfo.LongName;
                    //}
                    //else
                    //{
                    //    sInfo.AnnouncementText = sInfo.ShortName;
                    //}
                    sInfo.AnnouncementText = "True";

                }
                directionOneStopsList[stopid] = sInfo;
                stopIdList.Add(stopid);
            }
        }


        //planned
        private void ProcessJourney(List<ScheduleVehicleJourney> journeys)
        {
            int customerId = _customerId;
            string line = string.Empty;
            string tripId = string.Empty;

            StringBuilder log = new StringBuilder();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedule = dbContext.Schedules.Where(s => s.Line == "DEAD" && s.Destination == "DEAD" && s.CustomerId == CustomerId).FirstOrDefault();
                    foreach (ScheduleVehicleJourney tmpJourney in journeys)
                    {

                        line = tmpJourney.Line;
                        tripId = tmpJourney.TripId;
                        string externalRef = tripId;
                        bool exists = dbContext.Journeys.Where(j => j.ExternalReference == externalRef && j.CustomerId == customerId).Count() > 0;

                        if (exists)
                        {
                            Console.WriteLine("#############################################################################");
                            Console.WriteLine("Journey Already exists: " + externalRef);
                            Console.WriteLine("#############################################################################");
                            log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} already exists / imported to Journeys table ##\n", tripId));
                            continue;
                        }
                        int nTripType = 0;
                        bool isNumeric = int.TryParse(tmpJourney.TripType, out nTripType);
                        Journey journey = null; ;
                        try
                        {
                            if (!isNumeric)
                            {
                                ArrayList addons = new ArrayList();
                                addons.Add("dc:" + tmpJourney.DC);
                                addons.Add("line:" + tmpJourney.Line);
                                addons.Add("trip:" + tmpJourney.TripType);

                                journey = new Journey()
                                {
                                    JourneyStateId = 0,
                                    CustomerId = CustomerId,
                                    BusNumber = "0",
                                    PlannedStartTime = null,
                                    PlannedEndTime = null,
                                    ExternalReference = externalRef,
                                    ScheduleId = JourneyCache.GetScheduleNumber(customerId, string.Empty, string.Empty, tmpJourney.DC, null, null, ref log, true, dbContext, addons, ExternalReferencePrefix),
                                    LastUpdated = DateTime.Now
                                };
                            }
                            else
                            {

                                List<StopInfo> stops = tmpJourney.StopsInfo;
                                DateTime plannedStartTime = stops.FirstOrDefault().PlannedDeparturetime;
                                DateTime plannedendTime = stops.LastOrDefault().PlannedDeparturetime;
                                string destination = stops.LastOrDefault().LongName;

                                decimal firstStopId = 0;

                                journey = new Journey();
                                log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} Checking if there is an existing future journey to be changed ##\n", tripId));
                                var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, line, destination, plannedStartTime, firstStopId).FirstOrDefault();
                                //int existingJourneyId = dbContext.GetExistingFutureJourney(customerId, externalRef, line, destination, plannedStartTime, firstStopId);
                                //var existingJourney = dbContext.Journeys.Where(j => j.JourneyId == existingJourneyId).FirstOrDefault();
                                log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} Checked ##\n", tripId));

                                if (existingJourney != null)
                                {
                                    journey = existingJourney;
                                    journey.JourneyStops.Clear();
                                }

                                journey.JourneyStops = new List<JourneyStop>();

                                journey.JourneyStateId = (int)IBI.Shared.Common.Types.JourneyStates.FUTURE;
                                journey.BusNumber = "0";
                                //journey.ClientRef = null;
                                journey.CustomerId = customerId;
                                //journey.EndTime = null;
                                journey.ExternalReference = externalRef;
                                journey.LastUpdated = DateTime.Now;
                                //journey.StartTime = null;

                                StringBuilder jsLog = new StringBuilder();
                                int stopSequence = 1;
                                foreach (var st in stops)
                                {
                                    JourneyStop jStop = new JourneyStop();
                                    jStop.Timestamp = DateTime.Now;

                                    jStop.StopId = decimal.Parse(st.StopId);


                                    jStop.PlannedDepartureTime = st.PlannedDeparturetime;

                                    jStop.PlannedArrivalTime = st.PlannedDeparturetime;
                                    string sqlPoint;

                                    jStop.StopName = st.LongName;

                                    sqlPoint = string.Format("POINT({0} {1})", st.Longitude.ToString(), st.Latitude.ToString());
                                    jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                                    //jStop.StopGPS = st.StopGPS;
                                    //TODO: Commented because zone does not exist
                                    //log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} Checking Zone ##\n", tripId));
                                    //jStop.Zone = JourneyCache.GetZoneName(jStop.StopGPS.Latitude.ToString().Replace(',', '.'), jStop.StopGPS.Longitude.ToString().Replace(',', '.'), true, dbContext);
                                    //jStop.Zone = "-1";

                                    bool stopNameChanged=false;
                                    SyncStop(jStop.StopId , jStop.StopName, jStop.StopGPS, "Geography", ref stopNameChanged, "NB");

                                    jStop.StopSequence = stopSequence;
                                    journey.JourneyStops.Add(jStop);
                                    stopSequence++;

                                    log.AppendLine(DateTime.Now + " - " + string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5} GPS: {6} {7}",
                                                jStop.StopId, jStop.StopName, stopSequence, jStop.Zone, jStop.PlannedDepartureTime.ToString(), jStop.PlannedArrivalTime.ToString(), jStop.StopGPS.Latitude.ToString(), jStop.StopGPS.Longitude.ToString()));

                                }

                                journey.PlannedStartTime = journey.JourneyStops.FirstOrDefault().PlannedDepartureTime;
                                journey.PlannedEndTime = journey.JourneyStops.LastOrDefault().PlannedArrivalTime;
                                string fromName = journey.JourneyStops.FirstOrDefault().StopName;
                                destination = journey.JourneyStops.LastOrDefault().StopName;



                                List<ScheduleStop> scheduleStops = journey.JourneyStops.Select(js => new ScheduleStop
                                {
                                    StopGPS = js.StopGPS,
                                    StopName = js.StopName,
                                    StopId = js.StopId,
                                    StopSequence = js.StopSequence,
                                    Zone = js.Zone,
                                    ScheduleStopsAddons = UpdateScheduleStopAdds(stops, js.StopSequence)
                                }).ToList();

                                //determine schedules
                                log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} Calling GetScheduleNumber ##\n", tripId));
                                journey.ScheduleId = JourneyCache.GetScheduleNumber(customerId, line, fromName, destination, null, scheduleStops, ref log, true, dbContext);
                                log.AppendLine(DateTime.Now + " - " + String.Format("## Trip # {0} GetScheduleNumber completed ##\n", tripId));
                                log.AppendLine(DateTime.Now + " - " + String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, Total Stops: {8}",
                                    (journey.JourneyId > 0 ? "UPDATED" : "INSERTED"), journey.JourneyId, journey.CustomerId, journey.ExternalReference, journey.ScheduleId, line, fromName, destination, journey.JourneyStops.Count()));
                            }
                            bool vehicleScheduleExists = dbContext.VehicleSchedules.Where(vs => vs.ExternalReference == tmpJourney.VehicleScheduleExternalRef).Count() > 0;
                            IBI.DataAccess.DBModels.VehicleSchedule vSchedule = null;
                            vSchedule = dbContext.VehicleSchedules.Where(jvs => jvs.ExternalReference == tmpJourney.VehicleScheduleExternalRef).FirstOrDefault();

                            //if (!vehicleScheduleExists)
                            if (vSchedule == null)
                            {
                                vSchedule = new IBI.DataAccess.DBModels.VehicleSchedule();
                                vSchedule.ExternalReference = tmpJourney.VehicleScheduleExternalRef;
                                vSchedule.DateAdded = DateTime.Now;
                                vSchedule.CustomerId = journey.CustomerId;
                                dbContext.VehicleSchedules.Add(vSchedule);
                            }

                            if (vSchedule.PlannedStartTime == null && journey.PlannedStartTime != null)
                            {
                                vSchedule.PlannedStartTime = journey.PlannedStartTime;
                            }
                            
                            if (journey.PlannedEndTime != null)
                            {
                                vSchedule.PlannedEndTime = journey.PlannedEndTime;
                            }
                            //else
                            //{
                            //    vSchedule = dbContext.VehicleSchedules.Where(jvs => jvs.VehicleScheduleId == tmpJourney.VehicleScheduleId).FirstOrDefault();
                            //}
                            if (journey.JourneyId == 0) //otherwise dbContext will update that existing Journey
                            {
                                dbContext.Journeys.Add(journey);
                                dbContext.SaveChanges();
                                journey.VehicleScheduleJourneys.Add(new VehicleScheduleJourney {  
                                  VehicleSchedule = vSchedule
                                });
                                dbContext.SaveChanges();
                            }
                            //}

                            AppUtility.Log2File("NG_Journeys", log.ToString());
                            log.Clear();


                            //bool vehicleScheduleExists = dbContext.VehicleSchedules.Where(j => j.VehicleScheduleId == tmpJourney.VehicleScheduleId).Count() > 0;

                            log.AppendLine(DateTime.Now + " - " + String.Format("Journey Added: {0}", journey.JourneyId));
                            AppUtility.Log2File("NG_Journeys", log.ToString());
                            log.Clear();

                        }
                        catch (Exception jException)
                        {
                            string errorMessage = "";
                            Exception innerException = jException;

                            while (innerException != null)
                            {
                                errorMessage += innerException.Message + "::" + innerException.StackTrace + Environment.NewLine;

                                innerException = innerException.InnerException;
                            }

                            log.AppendLine(DateTime.Now + " - " + String.Format("Failed to save this journey. {0}\n", errorMessage));
                        }
                        System.Threading.Thread.Sleep(10);
                    }
                    //dbContext.SaveChanges();
                }//end using ibiDataModel


                AppUtility.Log2File("NG_Journeys", log.ToString() + "\n\n Journeys imported successfully");

            }
            catch (Exception ex)
            {
                string errorMessage = "";
                Exception innerException = ex;

                while (innerException != null)
                {
                    errorMessage += innerException.Message + "::" + innerException.StackTrace + Environment.NewLine;

                    innerException = innerException.InnerException;
                }
                AppUtility.Log2File("NG_Journeys", "Failure : " + errorMessage);
                Console.WriteLine(errorMessage);
            }
        }

        //private void SyncStopWithDB(IBIDataModel dbContext, JourneyStop jStop)
        //{
        //    throw new NotImplementedException();
        //}

        private string SyncStop(decimal stopNo, string originalStopName, DbGeography stopGps, string gpsProjection, ref bool nameChanged, string stopSource)
        {
            try
            {

                using (new CallCounter("ScheduleServiceController.GetStopName"))
                {
                    nameChanged = false;
                    string stopName = originalStopName; //.Contains('(') ? originalStopName.Substring(0, originalStopName.IndexOf('(')-1).Trim() : originalStopName;

                    if (string.IsNullOrEmpty(stopName))
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.MoviaWSData, new Exception("StopName for StopNumber:[" + stopNo.ToString() + "] is null from Movia WS"));
                        //return "";
                    }


                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        Stop stop = dbContext.Stops.Where(s => s.GID == stopNo).FirstOrDefault();
                         
                        if (stop != null)
                        {

                            if (!(String.Equals(stop.StopName, stopName)))
                            {
                                nameChanged = true;
                                stop.StopName = AppUtility.SafeSqlLiteral(stopName); 
                                stop.LastUpdated = DateTime.Now; 
                                stop.StopGPS = stopGps;  // DbGeography.FromText(sqlPoint, 4326);

                                stop.LastUpdated = DateTime.Now;
                                stop.StopSource = stopSource;

                                stopName = stop.StopName;

                                dbContext.SaveChanges();
                            }

                            //dont update stop in any case

                        }

                        else
                        {
                            nameChanged = true;

                            Stop newStop = new Stop();
                            newStop.GID = stopNo;
                            newStop.StopName = AppUtility.SafeSqlLiteral(stopName);
                            newStop.StopGPS = stopGps; // DbGeography.FromText(sqlPoint, 4326);
                            newStop.OriginalGPS = newStop.StopGPS;

                            //stop.OriginalGPSCoordinateNS = newStop.OriginalGPSCoordinateNS; //[KHI: Comment this line if Geography is introduced]
                            //stop.OriginalGPSCoordinateEW = newStop.OriginalGPSCoordinateEW; //[KHI: Comment this line if Geography is introduced]

                            newStop.OriginalName = AppUtility.SafeSqlLiteral(originalStopName);
                            newStop.OriginalCreated = newStop.LastUpdated = DateTime.Now;
                            newStop.StopSource = stopSource;
                            newStop.LastUpdated = DateTime.Now;

                            stopName = newStop.StopName;

                            dbContext.Stops.Add(newStop);

                            dbContext.SaveChanges();

                            //stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            //stop.OriginalGPS = stop.StopGPS;

                            //dbContext.SaveChanges();
                        }


                        return stopName;
                    }

                }
            }

            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.MoviaWS, string.Format("StopId:[{0}] - originalStopName: {1}, gpsCoordinates: {2} {3}, gpsProjection: {4} \n {5}", stopNo, originalStopName, stopGps.Latitude, stopGps.Longitude, gpsProjection, Logger.GetDetailedError(ex)));
            }

            return "";

        } 


        private ICollection<ScheduleStopsAddon> UpdateScheduleStopAdds(List<StopInfo> stops, int p)
        {
            ICollection<ScheduleStopsAddon> stopAddons = new List<ScheduleStopsAddon>();
            StopInfo sInfo = stops[p - 1];//.Where((x, i) => i % p == 0).FirstOrDefault();
            if (sInfo != null && sInfo.HasAnnouncement)
            {
                if (sInfo.AnnouncementRadius > 0)
                {
                    ScheduleStopsAddon radius = new ScheduleStopsAddon();
                    radius.StopSequence = p;
                    radius.Key = "radius";
                    radius.Value = sInfo.AnnouncementRadius.ToString();
                    stopAddons.Add(radius);
                }
                if (!String.IsNullOrEmpty(sInfo.AnnouncementText)) {
                    ScheduleStopsAddon announcement = new ScheduleStopsAddon();
                    announcement.StopSequence = p;
                    announcement.Key = "announcement";
                    announcement.Value = sInfo.AnnouncementText.ToString();
                    stopAddons.Add(announcement);
                }
            }
            return stopAddons;
        }

        private void createDeadJourney(StringBuilder log, IBIDataModel dbContext, ScheduleVehicleJourney tmpJourney, string externalRef)
        {
            var schedule = dbContext.Schedules.Where(s => s.Line == "DEAD" && s.Destination == "DEAD" && s.CustomerId == CustomerId).FirstOrDefault();
            Journey journey = new Journey()
            {
                JourneyStateId = 0,
                BusNumber = "0",
                PlannedStartTime = tmpJourney.PlannedStartTime,
                PlannedEndTime = tmpJourney.PlannedEndTime,
                ExternalReference = externalRef,
                ScheduleId = schedule.ScheduleId
            };


            bool vehicleScheduleExists = dbContext.VehicleSchedules.Where(vs => vs.ExternalReference == tmpJourney.VehicleScheduleExternalRef).Count() > 0;
            IBI.DataAccess.DBModels.VehicleSchedule vSchedule = null;
            if (!vehicleScheduleExists)
            {
                vSchedule = new IBI.DataAccess.DBModels.VehicleSchedule();
                vSchedule.ExternalReference = tmpJourney.VehicleScheduleExternalRef;
                vSchedule.DateAdded = DateTime.Now;
                dbContext.VehicleSchedules.Add(vSchedule);
            }
            else
            {
                vSchedule = dbContext.VehicleSchedules.Where(jvs => jvs.ExternalReference == tmpJourney.VehicleScheduleExternalRef).FirstOrDefault();
            }

            dbContext.SaveChanges();

            if (journey.JourneyId == 0) //otherwise dbContext will update that existing Journey
            {
                dbContext.Journeys.Add(journey);
                dbContext.SaveChanges();            
            }

            AppUtility.Log2File("NG_Journeys", log.ToString());
            log.Clear();


            //bool vehicleScheduleExists = dbContext.VehicleSchedules.Where(j => j.VehicleScheduleId == tmpJourney.VehicleScheduleId).Count() > 0;

            log.AppendLine(DateTime.Now + " - " + String.Format("Journey Added: {0}", journey.JourneyId));
            AppUtility.Log2File("NG_Journeys", log.ToString());
            log.Clear();

            dbContext.SaveChanges();
            journey = null;
        }


        private JObject downloadJsonData()
        {
            System.Net.WebClient webClient = new System.Net.WebClient();

            string jsonText = webClient.DownloadString(_url);
            JObject vehicleScheduleData = JObject.Parse(jsonText);
            return vehicleScheduleData;
        }


        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
            }
        }
    }
}


//var directions = from p in vehicleScheduleData["stoplist"]["1"].Values<string>()
//                 into g select new {Name=g.Key};


//foreach (var item in directions)
//{
//    //sb.AppendLine(item);
//}


//if (vehicleScheduleData != null)
//{
//    ArrayList stopData = new ArrayList();
//    var stopsList = vehicleScheduleData["stoplist"];
//    if (stopsList != null && stopsList.HasValues)
//    {
//        if (stopsList.HasValues)
//        {
//            Hashtable stopListMap = new Hashtable();
//            JEnumerable<JToken> children = stopsList.Children();
//            foreach (JToken token in children)
//            {
//                ArrayList stopListInformation = new ArrayList();
//                string tokenPath = token.Path;
//                if (token.HasValues)
//                {
//                    JEnumerable<JToken> tokenChildren = token.Children();
//                    foreach (JToken tokenChild in tokenChildren)
//                    {
//                        string[] pathArray = tokenPath.Split(new char[] { '.' });
//                        ArrayList mapStops = new ArrayList();
//                        foreach (JToken stopInfo in tokenChild.Children())
//                        {
//                            string[] arr = stopInfo.Path.Split(new char[] { '.' });
//                            if (stopInfo.HasValues)
//                            {
//                                foreach (JToken info1 in stopInfo.Children())
//                                {
//                                    string stopName = info1.Value<String>("long name") ?? "";
//                                    string stopShortName = info1.Value<String>("short name") ?? "";
//                                    JToken infotainment = info1.Value<JToken>("infotainment") ?? "";
//                                    int announcement = infotainment.Value<int>("announcement");
//                                    string announcementText = string.Empty;
//                                    if (announcement == 1)
//                                    {
//                                        announcementText = infotainment.Value<string>("announcementtext") ?? "";
//                                        if (string.Compare(announcementText, "long name", true) == 0)
//                                        {
//                                            announcementText = stopName;
//                                        }
//                                        else if (string.Compare(announcementText, "short name", true) == 0)
//                                        {
//                                            announcementText = stopShortName;
//                                        }
//                                    }
//                                    mapStops.Add(new VehicleScheduleStop(arr[arr.Length - 1], stopName, stopShortName, new VehicleScheduleStopInfotainment(announcement == 1, announcementText)));
//                                }
//                            }
//                        }
//                        stopListMap[pathArray[pathArray.Length - 1]] = mapStops;
//                    }
//                }
//            }
//            vehicleScheduleMap["stoplist"] = stopListMap;
//        }
//    }
//}
//var vehicleworkingsArray = vehicleworkings.Children<JObject>().ToList();
//                foreach (JObject vehicleWorking in vehicleworkingsArray)
//                {
//                    JArray daytype = (JArray)vehicleWorking.Children<JProperty>().FirstOrDefault(x => x.Name == "daytype").Value;

//                    var tripsData = vehicleworkings.Children<JObject>().Where(x => x.Children<JProperty>().FirstOrDefault(y => y.Name == "daytype" && JToken.DeepEquals(y.Value, weekDays)) != null).FirstOrDefault();
//                    var vw = tripsData["vehicleworking"];

//                    bool b = JToken.DeepEquals(weekDays, daytype);
//                    var vehicleworkingTrips = vehicleWorking.Children<JProperty>().FirstOrDefault(x => x.Name == "vehicleworking").Value.Children<JProperty>();
//                    foreach (var vehicleWorkingTrip in vehicleworkingTrips)
//                    {
//                        var tripList = vehicleWorkingTrip.Value;
//                        foreach (var item in tripList.Children())
//                        {

//                        }
//                        int i = 0;
//                    }

//                }