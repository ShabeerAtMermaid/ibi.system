﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Collections;

namespace IBI.Web.Common
{
    public static class ImageGeneratorSettings
    {

        public static List<KeyValuePair<string, string>> TemplatesAndRuleIds
        {
            get;
            set;
        }

        public static List<KeyValuePair<string, List<string>>> LineFontAndSizes
        {
            get;
            set;
        }

        public static List<KeyValuePair<string, List<string>>> MainFontAndSizes
        {
            get;
            set;
        }

        public static List<KeyValuePair<string, List<string>>> SubFontAndSizes
        {
            get;
            set;
        }

        public static void ImageSettingParsing(int customerId)
        {
            try
            {
                List<string> Templates = new List<string>();
                string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\ImageGeneratorSettings.xml");
                XDocument xdoc = XDocument.Load(filePath);

                var settings =  (from s in xdoc.Descendants("Setting")
                                where (int.Parse(s.Attribute("settingid").Value) ==
                                          (from c in xdoc.Descendants("Customer")
                                           where int.Parse(c.Attribute("customerid").Value) == customerId
                                           select int.Parse(c.Attribute("settingid").Value)
                                          ).FirstOrDefault()
                                       )
                                select new
                                {
                                    Templates = s.Descendants("Template"),
                                    LineFonts = s.Descendants("LineFonts"),
                                    MainFonts = s.Descendants("MainFonts"),
                                    SubFonts = s.Descendants("SubFonts")
                                }
                                ).FirstOrDefault();

                if (settings == null)
                {
                    settings = (from s in xdoc.Descendants("Setting")
                                where (int.Parse(s.Attribute("settingid").Value) ==
                                          (from c in xdoc.Descendants("Customer")
                                           where int.Parse(c.Attribute("customerid").Value) == 0
                                           select int.Parse(c.Attribute("settingid").Value)
                                          ).FirstOrDefault()
                                       )
                                select new
                                {
                                    Templates = s.Descendants("Template"),
                                    LineFonts = s.Descendants("LineFonts"),
                                    MainFonts = s.Descendants("MainFonts"),
                                    SubFonts = s.Descendants("SubFonts")
                                }
                                 ).FirstOrDefault();
                }

                if (TemplatesAndRuleIds == null) TemplatesAndRuleIds = new List<KeyValuePair<string, string>>();
                TemplatesAndRuleIds.Clear();
                foreach (var Template in settings.Templates)
                {
                    string TempName = Template.Attribute("templatename").Value;
                    string graphicalruleid = Template.Attribute("graphicalruleid").Value;
                    KeyValuePair<string, string> keyValuePair = new KeyValuePair<string, string>(TempName, graphicalruleid);
                    if (!TemplatesAndRuleIds.Contains(keyValuePair))
                    {
                        TemplatesAndRuleIds.Add(keyValuePair);
                    }
                }

                if (LineFontAndSizes == null) LineFontAndSizes = new List<KeyValuePair<string, List<string>>>();
                LineFontAndSizes.Clear();
                foreach (var LineFont in settings.LineFonts.Descendants("Font"))
                {
                    var FontName = LineFont.Attribute("name").Value;
                    List<string> FontSize = (from si in LineFont.Descendants("size")
                                          select si.Attribute("size").Value).ToList();
                    KeyValuePair<string, List<string>> keyValuePair = new KeyValuePair<string, List<string>>(FontName, FontSize);

                    if (!LineFontAndSizes.Contains(keyValuePair))
                    {
                        LineFontAndSizes.Add(keyValuePair);
                    }
                }



                /*
                 
                if (MainFontAndSizes == null) MainFontAndSizes = new Hashtable();
                MainFontAndSizes.Clear();
                foreach (var MainFont in settings.MainFonts.Descendants("Font"))
                {
                    var FontName = MainFont.Attribute("name").Value;
                    List<string> FontSize = (from si in MainFont.Descendants("size")
                                          select si.Attribute("size").Value).ToList();
                    if (!MainFontAndSizes.ContainsKey(FontName))
                    {
                        MainFontAndSizes.Add(FontName, FontSize);
                    }
                }
                 
                */


                if (MainFontAndSizes == null) MainFontAndSizes = new List<KeyValuePair<string, List<string>>>();
                MainFontAndSizes.Clear();
                foreach (var MainFont in settings.MainFonts.Descendants("Font"))
                {
                    var FontName = MainFont.Attribute("name").Value;
                    List<string> FontSize = (from si in MainFont.Descendants("size")
                                             select si.Attribute("size").Value).ToList();
                    KeyValuePair<string, List<string>> keyValuePair = new KeyValuePair<string, List<string>>(FontName, FontSize);

                    if (!MainFontAndSizes.Contains(keyValuePair))
                    {
                        MainFontAndSizes.Add(keyValuePair);
                    }
                }

                /*
                 * 
                if (SubFontAndSizes == null) SubFontAndSizes = new Hashtable();
                SubFontAndSizes.Clear();
                foreach (var SubFont in settings.SubFonts.Descendants("Font"))
                {
                    var FontName = SubFont.Attribute("name").Value;
                    List<string> FontSize = (from si in SubFont.Descendants("size")
                                          select si.Attribute("size").Value).ToList();
                    if (!SubFontAndSizes.ContainsKey(FontName))
                    {
                        SubFontAndSizes.Add(FontName, FontSize);
                    }
                }
                 * 
                 */

                if (SubFontAndSizes == null) SubFontAndSizes = new List<KeyValuePair<string, List<string>>>();
                SubFontAndSizes.Clear();
                foreach (var SubFont in settings.SubFonts.Descendants("Font"))
                {
                    var FontName = SubFont.Attribute("name").Value;
                    List<string> FontSize = (from si in SubFont.Descendants("size")
                                             select si.Attribute("size").Value).ToList();
                    KeyValuePair<string, List<string>> keyValuePair = new KeyValuePair<string, List<string>>(FontName, FontSize);

                    if (!SubFontAndSizes.Contains(keyValuePair))
                    {
                        SubFontAndSizes.Add(keyValuePair);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public static List<SelectListItem> GetTemplates(string graphicsRuleId)
        {
            List<SelectListItem> Templates = new List<SelectListItem>();
            foreach (KeyValuePair<string,string> keyValuePair in TemplatesAndRuleIds)
            {
                if (keyValuePair.Value == graphicsRuleId)
                    Templates.Add(new SelectListItem { Value = keyValuePair.Value, Text = keyValuePair.Key , Selected = true });
                else
                    Templates.Add(new SelectListItem { Value = keyValuePair.Value, Text = keyValuePair.Key });
            }
            return Templates;
        }

        public static List<SelectListItem> GetTemplates()
        {
            List<SelectListItem> Templates = new List<SelectListItem>();
            foreach (KeyValuePair<string, string> keyValuePair in TemplatesAndRuleIds)
            {
                Templates.Add(new SelectListItem { Value = keyValuePair.Key, Text = keyValuePair.Key  });
            }
            return Templates;
        }

        public static SelectList LineFonts()
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in LineFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key);
            }
            return new SelectList(Fonts, Fonts[0]);
        }

        public static SelectList LineFonts(string lineFont)
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in LineFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key );
            }
            if (!Fonts.Contains(lineFont)) Fonts.Add(lineFont);
            return new SelectList(Fonts, lineFont);
        }

        public static SelectList LineFontsSizes(string fontName)
        {
            List<string> FontSizes = null ;
            foreach (KeyValuePair<string, List<string>> keyValuePair in LineFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    return new SelectList(FontSizes);
                }
            }
            return new SelectList(FontSizes);
        }

        //public static SelectList LineFontsSizes(string fontName, string fontsize)
        //{
        //    List<string> FontSizes = (List<string>)LineFontAndSizes[fontName];
        //    if (FontSizes == null || FontSizes.Count() <= 0)
        //    {
        //        FontSizes = new List<string>();
        //        FontSizes.Add(fontsize);
        //        LineFontAndSizes.Add(fontName, new List<string>() { fontsize });
        //    }
        //    return new SelectList(FontSizes, fontsize);
        //}

        public static SelectList LineFontsSizes(string fontName, string fontsize)
        {
            List<string> FontSizes = null;
            foreach (KeyValuePair<string, List<string>> keyValuePair in LineFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    break;
                }
            }
            if (FontSizes == null || FontSizes.Count() <= 0)
            {
                FontSizes = new List<string>();
                FontSizes.Add(fontsize);

                KeyValuePair<string, List<string>> NewkeyValuePair = new KeyValuePair<string, List<string>>(fontName, new List<string>() { fontsize });
                LineFontAndSizes.Add(NewkeyValuePair);
            }
            return new SelectList(FontSizes, fontsize);
        }

        /*
        public static SelectList MainFonts()
        {
            List<string> Fonts = new List<string>();

            foreach (string key in MainFontAndSizes.Keys)
            {
                Fonts.Add(key);
            }
            return new SelectList(Fonts, Fonts[0]);
        }
         */
        public static SelectList MainFonts()
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in MainFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key);
            }
            return new SelectList(Fonts, Fonts[0]);
        } 
         
        /*
        public static SelectList MainFonts(string mainFont)
        {
            List<string> Fonts = new List<string>();

            foreach (string key in MainFontAndSizes.Keys)
            {
                Fonts.Add(key);
            }
            if (!Fonts.Contains(mainFont)) Fonts.Add(mainFont);
            return new SelectList(Fonts, mainFont);
        }
        */

        public static SelectList MainFonts(string mainFont)
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in MainFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key);
            }
            if (!Fonts.Contains(mainFont)) Fonts.Add(mainFont);
            return new SelectList(Fonts, mainFont);
        }
        /*
        public static SelectList MainFontsSizes(string fontName)
        {
            List<string> FontSizes = (List<string>)MainFontAndSizes[fontName];

            return new SelectList(FontSizes);
        }
        */

        public static SelectList MainFontsSizes(string fontName)
        {
            List<string> FontSizes = null;
            foreach (KeyValuePair<string, List<string>> keyValuePair in MainFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    return new SelectList(FontSizes);
                }
            }
            return new SelectList(FontSizes);
        }


        /*
        public static SelectList MainFontsSizes(string fontName, string fontsize)
        {
            List<string> FontSizes = (List<string>)MainFontAndSizes[fontName];
            if (FontSizes == null || FontSizes.Count() <= 0)
            {
                FontSizes = new List<string>();
                FontSizes.Add(fontsize);
                MainFontAndSizes.Add(fontName, new List<string>() { fontsize });
            }
            return new SelectList(FontSizes, fontsize);
        }
        */

        public static SelectList MainFontsSizes(string fontName, string fontsize)
        {
            List<string> FontSizes = null;
            foreach (KeyValuePair<string, List<string>> keyValuePair in MainFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    break;
                }
            }
            if (FontSizes == null || FontSizes.Count() <= 0)
            {
                FontSizes = new List<string>();
                FontSizes.Add(fontsize);

                KeyValuePair<string, List<string>> NewkeyValuePair = new KeyValuePair<string, List<string>>(fontName, new List<string>() { fontsize });
                MainFontAndSizes.Add(NewkeyValuePair);
            }
            return new SelectList(FontSizes, fontsize);
        }


        /*

        public static SelectList SubFonts()
        {
            List<string> Fonts = new List<string>();

            foreach (string key in SubFontAndSizes.Keys)
            {
                Fonts.Add(key);
            }
            return new SelectList(Fonts, Fonts[0]);
        }
        */

        public static SelectList SubFonts()
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in SubFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key);
            }
            return new SelectList(Fonts, Fonts[0]);
        } 

        /*
        public static SelectList SubFonts(string subFont)
        {
            List<string> Fonts = new List<string>();

            foreach (string key in SubFontAndSizes.Keys)
            {
                Fonts.Add(key);
            }
            if (!Fonts.Contains(subFont)) Fonts.Add(subFont);
            return new SelectList(Fonts, subFont);
        }
        */

        public static SelectList SubFonts(string subFont)
        {
            List<string> Fonts = new List<string>();

            foreach (KeyValuePair<string, List<string>> keyValuePair in SubFontAndSizes)
            {
                Fonts.Add(keyValuePair.Key);
            }
            if (!Fonts.Contains(subFont)) Fonts.Add(subFont);
            return new SelectList(Fonts, subFont);
        }

        /*
        public static SelectList SubFontsSizes(string fontName)
        {
            List<string> FontSizes = (List<string>)SubFontAndSizes[fontName];

            return new SelectList(FontSizes);
        }
         */

        public static SelectList SubFontsSizes(string fontName)
        {
            List<string> FontSizes = null;
            foreach (KeyValuePair<string, List<string>> keyValuePair in SubFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    return new SelectList(FontSizes);
                }
            }
            return new SelectList(FontSizes);
        }

        /*
        public static SelectList SubFontsSizes(string fontName, string fontsize)
        {
            List<string> FontSizes = (List<string>)SubFontAndSizes[fontName];
            if (FontSizes == null || FontSizes.Count() <= 0)
            {
                FontSizes = new List<string>();
                FontSizes.Add(fontsize);
                SubFontAndSizes.Add(fontName, new List<string>() { fontsize });
            }
            return new SelectList(FontSizes, fontsize);
        }
         */

        public static SelectList SubFontsSizes(string fontName, string fontsize)
        {
            List<string> FontSizes = null;
            foreach (KeyValuePair<string, List<string>> keyValuePair in SubFontAndSizes)
            {
                if (keyValuePair.Key == fontName)
                {
                    FontSizes = keyValuePair.Value;
                    break;
                }
            }
            if (FontSizes == null || FontSizes.Count() <= 0)
            {
                FontSizes = new List<string>();
                FontSizes.Add(fontsize);

                KeyValuePair<string, List<string>> NewkeyValuePair = new KeyValuePair<string, List<string>>(fontName, new List<string>() { fontsize });
                SubFontAndSizes.Add(NewkeyValuePair);
            }
            return new SelectList(FontSizes, fontsize);
        }


    }
}






















//namespace IBI.Web.Common
//{
//    public static class ImageGeneratorSettings
//    {
//        public static Hashtable TemplatesAndRuleIds
//        {
//            get;
//            set;
//        }

//        public static Hashtable LineFontAndSizes
//        {
//            get;
//            set;
//        }

//        public static Hashtable MainFontAndSizes
//        {
//            get;
//            set;
//        }
        
//        public static Hashtable SubFontAndSizes
//        {
//            get;
//            set;
//        }

//        public static void ImageSettingParsing(int customerId)
//        {
//            try
//            {
//                List<string> Templates = new List<string>();
//                string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Config\ImageGeneratorSettings.xml");
//                XDocument xdoc = XDocument.Load(filePath);

//                var settings = (from s in xdoc.Descendants("Setting")
//                                    where (int.Parse(s.Attribute("settingid").Value) ==
//                                              (from c in xdoc.Descendants("Customer")
//                                               where int.Parse(c.Attribute("customerid").Value) == customerId
//                                               select int.Parse(c.Attribute("settingid").Value)
//                                              ).FirstOrDefault()
//                                           )
//                                    select new 
//                                        {
//                                            Templates = s.Descendants("Template"),
//                                            LineFonts = s.Descendants("LineFonts"),
//                                            MainFonts = s.Descendants("MainFonts"),
//                                            SubFonts = s.Descendants("SubFonts")
//                                        }
//                                ).FirstOrDefault();

//                if (settings == null)
//                {
//                    settings = (from s in xdoc.Descendants("Setting")
//                                    where (int.Parse(s.Attribute("settingid").Value) ==
//                                              (from c in xdoc.Descendants("Customer")
//                                               where int.Parse(c.Attribute("customerid").Value) == 0
//                                               select int.Parse(c.Attribute("settingid").Value)
//                                              ).FirstOrDefault()
//                                           )
//                                    select new
//                                    {
//                                        Templates = s.Descendants("Template"),
//                                        LineFonts = s.Descendants("LineFonts"),
//                                        MainFonts = s.Descendants("MainFonts"),
//                                        SubFonts = s.Descendants("SubFonts")
//                                    }
//                                 ).FirstOrDefault();
//                }

//                if (TemplatesAndRuleIds == null)  TemplatesAndRuleIds = new Hashtable();
//                foreach (var Template in settings.Templates)
//                {
//                    var TempName = Template.Attribute("templatename").Value;
//                    var graphicalruleid = Template.Attribute("graphicalruleid").Value;
//                    if (!TemplatesAndRuleIds.ContainsKey(TempName))
//                    {
//                        TemplatesAndRuleIds.Add(TempName, graphicalruleid);
//                    }
//                }

//                if (LineFontAndSizes == null)  LineFontAndSizes = new Hashtable();
//                foreach (var LineFont in settings.LineFonts.Descendants("Font"))
//                {
//                    var FontName = LineFont.Attribute("name").Value;
//                    List<int> FontSize = (from si in LineFont.Descendants("size")
//                                    select int.Parse(si.Attribute("size").Value)).ToList();
//                    if (!LineFontAndSizes.ContainsKey(FontName))
//                    {
//                        LineFontAndSizes.Add(FontName, FontSize);
//                    }
//                }

//                if (MainFontAndSizes == null) MainFontAndSizes = new Hashtable();
//                foreach (var MainFont in settings.MainFonts.Descendants("Font"))
//                {
//                    var FontName = MainFont.Attribute("name").Value;
//                    List<int> FontSize = (from si in MainFont.Descendants("size")
//                                          select int.Parse(si.Attribute("size").Value)).ToList();
//                    if (!MainFontAndSizes.ContainsKey(FontName))
//                    {
//                        MainFontAndSizes.Add(FontName, FontSize);
//                    }
//                }

//                if (SubFontAndSizes == null) SubFontAndSizes = new Hashtable();
//                foreach (var SubFont in settings.SubFonts.Descendants("Font"))
//                {
//                    var FontName = SubFont.Attribute("name").Value;
//                    List<int> FontSize = (from si in SubFont.Descendants("size")
//                                          select int.Parse(si.Attribute("size").Value)).ToList();
//                    if (!SubFontAndSizes.ContainsKey(FontName))
//                    {
//                        SubFontAndSizes.Add(FontName, FontSize);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//            }
//        }

//        public static List<SelectListItem> GetTemplates(string graphicsRuleId)
//        {
//            List<SelectListItem> Templates = new List<SelectListItem>();
//            foreach (string key in TemplatesAndRuleIds.Keys)
//            {
//                if (TemplatesAndRuleIds[key].ToString() == graphicsRuleId)
//                    Templates.Add(new SelectListItem { Value = TemplatesAndRuleIds[key].ToString(), Text = key, Selected = true });
//                else
//                    Templates.Add(new SelectListItem { Value = TemplatesAndRuleIds[key].ToString(), Text = key });
//            }
//            return Templates; 
//        }

//        public static List<SelectListItem> GetTemplates()
//        {
//            List<SelectListItem> Templates = new List<SelectListItem>();

//            ////Templates.Add(new SelectListItem { Value = "SELECT", Text = "SELECT", Selected = true });
//            foreach (string key in TemplatesAndRuleIds.Keys)
//            {
//                //Templates.Add(new SelectListItem { Value = TemplatesAndRuleIds[key].ToString(), Text = key });    
//                Templates.Add(new SelectListItem { Value = key, Text = key });
//            }
//            return Templates;
//        }

//        public static SelectList LineFonts()
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in LineFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            return new SelectList(Fonts, Fonts[0]);
//        }

//        public static SelectList LineFonts(string lineFont)
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in LineFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            if (!Fonts.Contains(lineFont)) Fonts.Add(lineFont);
//            return new SelectList(Fonts, lineFont);
//        }

//        public static SelectList LineFontsSizes(string fontName)
//        {
//            List<int> FontSizes = (List<int>)LineFontAndSizes[fontName];

//            return new SelectList(FontSizes);
//        }

//        public static SelectList LineFontsSizes(string fontName,int fontsize)
//        {
//            List<int> FontSizes = (List<int>)LineFontAndSizes[fontName];
//            if (FontSizes.Count() <= 0) FontSizes.Add(fontsize);
//            return new SelectList(FontSizes, fontsize);
//        }

//        public static SelectList MainFonts()
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in MainFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            return new SelectList(Fonts,Fonts[0]);
//        }

//        public static SelectList MainFonts(string mainFont)
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in MainFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            if (!Fonts.Contains(mainFont)) Fonts.Add(mainFont);
//            return new SelectList(Fonts, mainFont);
//        }

//        public static SelectList MainFontsSizes(string fontName)
//        {
//            List<int> FontSizes = (List<int>)MainFontAndSizes[fontName];
            
//            return new SelectList(FontSizes);
//        }

//        public static SelectList MainFontsSizes(string fontName, int fontsize)
//        {
//            List<int> FontSizes = (List<int>)MainFontAndSizes[fontName];
//            if (FontSizes.Count() <= 0) FontSizes.Add(fontsize);
//            return new SelectList(FontSizes, fontsize);
//        }

//        public static SelectList SubFonts()
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in SubFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            return new SelectList(Fonts, Fonts[0]);
//        }

//        public static SelectList SubFonts(string subFont)
//        {
//            List<string> Fonts = new List<string>();

//            foreach (string key in SubFontAndSizes.Keys)
//            {
//                Fonts.Add(key);
//            }
//            if (!Fonts.Contains(subFont)) Fonts.Add(subFont);
//            return new SelectList(Fonts, subFont);
//        }

//        public static SelectList SubFontsSizes(string fontName)
//        {
//            List<int> FontSizes = (List<int>)SubFontAndSizes[fontName];

//            return new SelectList(FontSizes);
//        }

//        public static SelectList SubFontsSizes(string fontName, int fontsize)
//        {
//            List<int> FontSizes = (List<int>)SubFontAndSizes[fontName];
//            if (FontSizes.Count() <= 0) FontSizes.Add(fontsize);
//            return new SelectList(FontSizes, fontsize);
//        }
//    }
//}