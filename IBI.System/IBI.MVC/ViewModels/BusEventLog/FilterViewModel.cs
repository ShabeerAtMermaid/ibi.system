﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace IBI.Web.ViewModels.BusEventLog
{
    public class FilterViewModel
    {   
         
            [DataMember]
            [Required(ErrorMessage = "Bus number required")]
            public string BusNumber { get; set; }

            [DataMember]
            [Required(ErrorMessage = "Date is required")]
            public DateTime? Date { get; set; }
                            
            public void Fill()
            {
                 //do nothing yet
            }

            
        }

             
}