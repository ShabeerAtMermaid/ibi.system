﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.DataClasses
{
    [DataContract]
    public class SignFileContent
    {
        [DataMember]
        public string Value
        {
            get;
            set;
        }
        [DataMember]
        public string base64Image
        {
            get;
            set;
        }
    }
}