﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using MvcContrib.Pagination;
using MvcContrib.Filters;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;


namespace IBI.Web.ViewModels.Messaging.Outgoing

{

    public class ListContainerViewModel
    {
        public IPagination<IBI.Shared.Models.Messages.Message> PagedList { get; set; }
        public FilterViewModel FilterViewModel { get; set; }
        public GridSortOptions GridSortOptions { get; set; } 
    }
}