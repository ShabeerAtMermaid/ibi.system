﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    [DataContract]
    public class SignAddonImageGeneratorTextSettings
    {
        [DataMember]
        private SignAddonImageTextSettings LineText { get; set; }
        [DataMember]
        private SignAddonImageTextSettings MainText { get; set; }
        [DataMember]
        private SignAddonImageTextSettings SubText { get; set; }
        [DataMember]
        private SignAddonImageAdditionalSettings Additional { get; set; }
    }
}