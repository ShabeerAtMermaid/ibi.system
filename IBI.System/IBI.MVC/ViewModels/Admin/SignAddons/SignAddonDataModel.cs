﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Collections;
using IBI.Shared.Models.Signs;

namespace IBI.Web.ViewModels.Admin.SignAddons
{
    public class SignAddonDataModel
    {
        [DataMember]
        public int CustomerId { get; set; }
        [DataMember]
        public string MainText { get; set; }
        [DataMember]
        public string SubText { get; set; }
        [DataMember]
        public string Line { get; set; }
        [DataMember]
        public List<SignXmlLayout> SignXmlLayout
        {
            get;
            set;
        }
        public Hashtable Base64Images { get; set; }

    }
}