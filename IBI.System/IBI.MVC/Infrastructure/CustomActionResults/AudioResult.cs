﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IBI.Web.Infrastructure.CustomActionResults
{
    public class AudioResult : ActionResult
    {
        private readonly byte[] Audio;

        public AudioResult(byte[] audio)
        {
            this.Audio = audio;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            
            //var response = context.RequestContext.HttpContext.Response;
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "audio/mp3";
            HttpContext.Current.Response.BinaryWrite(Audio);
            HttpContext.Current.Response.End();

            //var response = HttpContext.Current.Response;
            //byte[] image = C1File.ReadAllBytes(PathUtil.Resolve("~/Composite/images/about.png"));
            //response.Clear();
            //response.ContentType = "image/png";
            //response.BinaryWrite(image);
            //response.End();
        }
    }
}