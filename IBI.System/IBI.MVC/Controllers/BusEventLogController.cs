﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using MvcContrib.UI.Grid;
using IBI.Web.Infrastructure;
using IBI.Web.ViewModels.BusEventLog; 
using System.Globalization;
using IBI.Web.Infrastructure.CustomAttributes;

namespace IBI.Web.Controllers
{ 
    [AuthorizeLogin]
    public class BusEventLogController : Controller
    {        
        public ActionResult Index(
            string busNumber,
            DateTime? date,
            GridSortOptions gridSortOptions,
            int? page)
        {
             
            if (date == null || busNumber == "")
            {
                ListContainerViewModel emptyModel = new ListContainerViewModel();
                FilterViewModel filters = new FilterViewModel();
                filters.BusNumber = "";
                filters.Date = date ?? DateTime.Now;
                emptyModel.FilterViewModel = filters;

                var pagedListx = new List<IBI.Shared.Models.BusTree.BusEventLog>()
                    .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                    .AsPagination(page ?? 1, 100);


                emptyModel.PagedList = pagedListx;
                return View(emptyModel);
            }


            //try
            //{
            //    date = DateTime.ParseExact(dateFrom, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            //}
            //catch (Exception ex)
            //{
            //    return RedirectToAction("BusTree", "Bus", new { errorMessage = "date=" + dateFrom + " - " + ex.Message });
            //}

                //Response.Write(date.ToString());

                //Response.End();


            //return RedirectToAction("BusTree", "Bus", new { errorMessage = "No error" });
            var list = ServiceManager.GetBusEventLog(busNumber, date.Value);

            //Response.Write(list.Count());

            //Response.End();

            // Set default sort column
            if (string.IsNullOrWhiteSpace(gridSortOptions.Column))
            {
                gridSortOptions.Column = "TimeStamp";
                gridSortOptions.Direction = SortDirection.Descending;
            }


            // Filter on subject 
            if (busNumber != null && date != null)
            {
                list = list.Where(a => a.BusNumber == busNumber).ToList();
            }


            // Create all filter data and set current values if any
            // These values will be used to set the state of the select list and textbox
            // by sending it back to the view.
            var filterViewModel = new FilterViewModel();
            filterViewModel.BusNumber = busNumber;
            filterViewModel.Date = date;
            filterViewModel.Fill();

            // Order and page the product list
            var pagedList = list
                .OrderBy(gridSortOptions.Column, gridSortOptions.Direction)
                   .AsPagination(page ?? 1, 10000);


            var listContainer = new ListContainerViewModel
            {
                PagedList = pagedList,
                FilterViewModel = filterViewModel,
                GridSortOptions = gridSortOptions
            };

            return View(listContainer);

            return null;
        }

    }
}
