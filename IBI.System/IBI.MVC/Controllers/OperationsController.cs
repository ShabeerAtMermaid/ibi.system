﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;

namespace IBI.Web.Controllers
{
    public class OperationsController : Controller
    {
        //
        // GET: /Operations/

        public ActionResult ActiveJourneys()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetCustomerList()
        {
           List<IBI.Shared.Models.Customer> customers = IBI.Web.Common.CurrentUser.CustomerList;
           return Json(customers, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetActiveJourneys(int customerId, string sDate, string eDate, int pageIndex, int pageSize)
        {
            var data = RESTManager.GetActiveJourneys(customerId, sDate, eDate, pageIndex, pageSize);
            return Json(data, JsonRequestBehavior.AllowGet);

        }

    }
}
