﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IBI.Web.Infrastructure;
using IBI.Web.Infrastructure.CustomAttributes;

namespace IBI.Web.Controllers
{
     

    [AuthorizeLogin]
    public class StatisticsController :  Controller
    {
        //
        // GET: /Example/

        public ActionResult Statistics()
        {
            return View("Journey");
        }

        public ActionResult StatisticsPunctuality(
            string busNumber,
            DateTime? journeyDate,
            int? journeyId)
        {
            ViewData["busNumber"] = busNumber;
            ViewData["journeyDate"] = journeyDate ?? DateTime.Now;
            ViewData["journeyId"] = journeyId;
            return View("Punctuality");
        }

        public JsonResult Journey(
           string busNumber,
           DateTime? journeyDate
          )
        {
            IBI.Shared.Models.BusJourney.Journey[] list = ServiceManager.GetJourneys(
                journeyDate ?? DateTime.MinValue,
                busNumber
                ).ToArray();
            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


        public JsonResult Punctuality(
            string busNumber,
            DateTime? journeyDate,
            int? journeyId
          )
        {
            List<IBI.Shared.Models.BusJourney.Punctuality> list = ServiceManager.GetPunctuality(
                 journeyDate ?? DateTime.MinValue,
                 busNumber,
                 journeyId ?? 0
                );

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }       
    }
}