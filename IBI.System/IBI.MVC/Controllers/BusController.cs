﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;   
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.IO;
using System.Text;
using System.Net;
using System.Web.Script.Serialization;
using IBI.Shared.Models.BusTree;         
using IBI.Web.Infrastructure.Ajax;
using IBI.Shared.Models;
using IBI.Shared.Models.Messages;
using IBI.Shared.Models.BusTree;
using IBI.Web.ViewModels.BusStatusLog;
using IBI.Web.Common;
using IBI.Web.Infrastructure;
using MvcContrib.UI.Grid;
using MvcContrib.Pagination;
using MvcContrib.Sorting;
using IBI.Web.Infrastructure.Keys;
using IBI.Web.Infrastructure.CustomAttributes;
using IBI.Web.Infrastructure.Logger;
using IBI.Web.ViewModels.BusTree.BusGroup;


namespace IBI.Web.Controllers
{ 
    [AuthorizeLogin]
    public class BusController : Controller
    {
        
        private List<BusStatusColor> Filters
        {
            get
            {

                List<BusStatusColor> filters = SessionManager.Get<List<BusStatusColor>>(SessionKeys.BusTreeFilter);
                if (filters == null)
                {
                    filters = new List<BusStatusColor>
                    { 
                        BusStatusColor.GREEN,
                        BusStatusColor.GREY,
                        BusStatusColor.LIGHT_GREY,
                        BusStatusColor.ORANGE,
                        BusStatusColor.RED,
                        BusStatusColor.YELLOW
                    };
                    SessionManager.Store(SessionKeys.BusTreeFilter, filters);                    
                } 

                return filters;                
            }
            set
            {
                SessionManager.Store(SessionKeys.BusTreeFilter, value);
            }
        }
        //
        // GET: /Bus/

        #region Index Controller

        public ActionResult Index()
        {
            return View();            
        }  
         
        #endregion


        #region BusTree
        [HttpGet]        
        [ActionName("BusTree")]
        public ActionResult BusTree(bool? importLiveDB)
        {           
            return View("BusTree");            
        }

        [HttpPost]
        [ActionName("BusTree")]
        public ActionResult BusTree()
        {
            List<BusStatusColor> filters = new List<BusStatusColor>();

            if (Request.Form["chkGreen"] == "on")
                filters.Add(BusStatusColor.GREEN);

            if (Request.Form["chkGrey"] == "on")
                filters.Add(BusStatusColor.GREY);
            if (Request.Form["chkLightGrey"] == "on")
                filters.Add(BusStatusColor.LIGHT_GREY);
            if (Request.Form["chkOrange"] == "on")
                filters.Add(BusStatusColor.ORANGE);
            if (Request.Form["chkRed"] == "on")
                filters.Add(BusStatusColor.RED);
            if (Request.Form["chkYellow"] == "on")
                filters.Add(BusStatusColor.YELLOW);

            this.Filters = filters;

            return View("BusTree");
        }

        // BusReloading functionality is handled in post method of BusTree writen above.  
        //public ActionResult ReloadBusTree() 
        //{
        //    List<BusStatusColor> filters =  new List<BusStatusColor>();

        //    if (Request.Form["chkGreen"] == "on")
        //        filters.Add(BusStatusColor.GREEN);

        //    if (Request.Form["chkGrey"] == "on")
        //        filters.Add(BusStatusColor.GREY);
        //    if (Request.Form["chkLightGrey"] == "on")
        //        filters.Add(BusStatusColor.LIGHT_GREY);
        //    if (Request.Form["chkOrange"] == "on")
        //        filters.Add(BusStatusColor.ORANGE);
        //    if (Request.Form["chkRed"] == "on")
        //        filters.Add(BusStatusColor.RED);
        //    if (Request.Form["chkYellow"] == "on")
        //        filters.Add(BusStatusColor.YELLOW);

        //    this.Filters = filters;

        //   return View("BusTree");
        //}
         
        public JsonResult TreeDataByREST()
        {
            
            
            IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);

            if (usr != null && usr.UserId > 0)
            {
                WebLogger.Log("TreeDataByREST - start" , new object[]{usr.UserId});
                Tree tree = ServiceManager.GetBusTree(usr.UserId, this.Filters);
                WebLogger.Log("TreeDataByREST - end" );

                List<BusNode> groups = tree.Groups;

                string clientInformation = "";

                //set color counters in ClientInformation

                Dictionary<string, int> ColorCounter = tree.Counters ; 
                
                foreach (string key in ColorCounter.Keys)
                {
                    int val = 0;
                    ColorCounter.TryGetValue(key, out val); 
                    clientInformation += val.ToString() + ",";
                }

                clientInformation = clientInformation.TrimEnd(',');
                clientInformation += "|";


                //set filters in ClientInformation
                clientInformation+=Filters.Contains(BusStatusColor.GREEN) + ",";
                clientInformation+=Filters.Contains(BusStatusColor.GREY) + ",";
                clientInformation+=Filters.Contains(BusStatusColor.LIGHT_GREY) + ",";
                clientInformation+=Filters.Contains(BusStatusColor.ORANGE) + ",";
                clientInformation+=Filters.Contains(BusStatusColor.RED) + ",";
                clientInformation+=Filters.Contains(BusStatusColor.YELLOW);
                

                if (groups.Count > 0)
                {
                    groups[0].key = "0";
                    groups[0].ClientInformation = clientInformation; //JsonConvert.SerializeObject(ColorCounter.ToList());
                }
                 
                //return Json(groups, JsonRequestBehavior.AllowGet);

                var jsonResult = Json(groups, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            else
            {
                RedirectToAction("Login", "Account");
                
                List<GroupNode> nodeList = new List<GroupNode>();
                nodeList.Add(new GroupNode());
                return Json(nodeList, JsonRequestBehavior.AllowGet);
            }
        }
         
        public ActionResult BusDetail(int customerId, string busNo) {

            WebLogger.Log("BusDetail - start" , new object[]{customerId, busNo});
            IBI.Shared.Models.BusTree.Bus b = ServiceManager.GetBus(customerId, busNo);
            WebLogger.Log("BusDetail - end" );

            return View(b);
        }
         
        public ActionResult GroupDetail(int groupId) {

            WebLogger.Log("GroupDetail - start" , new object[]{groupId});
            IBI.Shared.Models.BusTree.Group model = ServiceManager.GetGroup(groupId);
            WebLogger.Log("GroupDetail - end");
            return View(model);
        }

        
        #endregion

        #region Simple Bus Tree

         /// <summary>
         /// 
         /// </summary>
         /// <param name="groupId"></param>
         /// <param name="customerId"></param>
         /// <param name="inOperation">if inOperation == true then only those buses will be fetched which are InOperation, otherwise all the buses. By Default all the buses</param>
         /// <returns></returns>
        [HttpGet]         
        public JsonResult GetSimpleBusTree(string groupId = "0", int customerId = 0, bool inOperation = false)
        {

            //if(customerId==0)
            //{
            //    if(String.IsNullOrEmpty(Request["CustomerId"])!=false)
            //    {
            //        customerId = int.Parse(Request["CustomerId"]);
            //    }
            //}

            customerId = Infrastructure.CacheManager.Get<int>("CurrentPageCustomerId");
            WebLogger.Log("GetSimpleBusTree - start", new object[] { CurrentUser.UserId.ToString(), groupId });

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();

            Tree t = srv.GetSimpleBusTree(CurrentUser.UserId.ToString(), groupId, customerId.ToString(), inOperation.ToString().ToLower());
            Infrastructure.CacheManager.Destroy("CurrentPageCustomerId");
            WebLogger.Log("GetSimplebusTree - end");

            SessionManager.Store(SessionKeys.BusMapTree, t);
            var tResult = Json(t.Groups, JsonRequestBehavior.AllowGet);
            tResult.MaxJsonLength = int.MaxValue;
            return tResult;

        }

        #endregion

        [HttpGet]
        public JsonResult GetBusGroupTree(string groupId = "0")
        {
            WebLogger.Log("GetBusGroupTree - start", new object[] { CurrentUser.UserId.ToString(), groupId });

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();

            WebLogger.Log("GetBusGroupTree - end");

            Tree t = srv.GetBusGroupTree(CurrentUser.UserId.ToString(), groupId);
            return Json(t.Groups, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public JsonResult GetBusGroupTreeForCustomer(int customerId, string groupId = "0")
        {
            WebLogger.Log("GetBusGroupTree - start", new object[] { CurrentUser.UserId.ToString(), groupId });

            BusTreeService.BusTreeServiceClient srv = new BusTreeService.BusTreeServiceClient();

            WebLogger.Log("GetBusGroupTree - end");

            Tree t = srv.GetBusGroupTree(CurrentUser.UserId.ToString(), groupId);
            t.Groups = t.Groups.Where(n => n.CustomerID == customerId).ToList();

            return Json(t.Groups, JsonRequestBehavior.AllowGet);

        }



        [HttpGet]
        public ActionResult ModifyBusAlias(string busNumber, int customerId, string busAlias)
        {
            //IBI.Shared.Models.BusTree.Bus bus = ServiceManager.GetBus(customerId, busNumber);
            ViewModels.BusTree.BusStatus.BusStatusModel b = new ViewModels.BusTree.BusStatus.BusStatusModel()
            {
                BusNumber = busNumber,
                CustomerId = customerId,
                BusAlias = busAlias

            };
            ViewBag.ShouldClose = false;
            return View("ModifyBusAlias", b);

        }

        [HttpPost]
        public JsonResult ModifyBusAlias(ViewModels.BusTree.BusStatus.BusStatusModel b)
        {
            if (string.IsNullOrEmpty(b.BusAlias))
            {
                b.LoadAfterSave =  false;
                b.ErrorMessage = "Please Enter BusAlias";
            }
            else
            {
                b.LoadAfterSave = ServiceManager.ModifyBusAlias(b.CustomerId, b.BusNumber, b.BusAlias);
                b.ErrorMessage = b.LoadAfterSave ? "" : "This BusAlias is already selected";
            }
            
            
            return Json(b, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusStatus(string busNumber, int customerId)
        {
            IBI.Shared.Models.BusTree.Bus bus = ServiceManager.GetBus(customerId, busNumber);
            ViewModels.BusTree.BusStatus.BusStatusModel b = new ViewModels.BusTree.BusStatus.BusStatusModel()
            {
                BusNumber = bus.BusNumber,
                CustomerId = bus.CustomerId??Common.Settings.DefaultCustomerId ,
                InOperation = bus.InOperation??false
            };
            ViewBag.ShouldClose = false;
            return View("BusStatus", b);
        
        }

        [HttpPost]
        //[Ajax(true)]
        public ActionResult ChangeBusStatus(ViewModels.BusTree.BusStatus.BusStatusModel b)
        {
            ServiceManager.ChangeBusStatus(b.CustomerId, b.BusNumber, b.InOperation);
            ViewBag.ShouldClose = true ;
            b.LoadAfterSave = true;
            return View("BusStatus", b);
        }

        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusMap()
        {
            ViewBag.Username = Common.CurrentUser.Username;
            ViewBag.Fullname = Common.CurrentUser.Fullname;
            return View("BusMap");
        }


        [HttpPost]
        public ActionResult GetUserSpecificBusesData(string filter, string customerIds, bool offline, bool online)
        {
            string busesData = ServiceManager.GetUserSpecificBuses(filter,customerIds, offline, online);
            Response.ContentType = "application/json";
            Response.Write(busesData);
            return null;
        }

        [HttpPost]
        public ActionResult GetBusClientsLastPings(string busNumber,int customerId)
        {
            string clientsData = ServiceManager.GetBusClientsLastPings(busNumber, customerId);
            Response.ContentType = "application/json";
            Response.Write(clientsData);
            return null;
        }

        [HttpPost]
        public JsonResult GetBusServiceTickets(string busNumber, int customerId)
        {
            string clientsData = ServiceManager.GetBusServiceTickets(busNumber, customerId);
            Response.ContentType = "application/json";
            Response.Write(clientsData);
            return null;
        }


        [HttpGet]
        public JsonResult GetUserSchedule(int scheduleId)
        {
            IBI.Shared.Models.UserScheduleModels.Schedule userSchedule = ServiceManager.GetUserSchedule(scheduleId);
            var jsonResult = Json(userSchedule, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }


        [HttpGet]
        public JsonResult SetUserActivityConfiguration(string propertyName, string propertyValue)
        {
            IBI.Web.Common.CurrentUser.SetUserActivityConfiguration(propertyName,propertyValue);
            return null;
        }


        [HttpGet]
        //[Ajax(true)]
        public ActionResult BusGroup(string busNumber, int customerId = 0, int groupId = 0)
        {
            BusGroupModel sendModel = new BusGroupModel();
            sendModel.BusNumber = busNumber;
            sendModel.CustomerId = customerId;
            sendModel.GroupId = groupId;
            return View("BusGroup", sendModel);
        }

        [HttpPost]
        //[Ajax(true)]
        public ActionResult MoveGroup(BusGroupModel model)
        {
            ServiceManager.ChangeBusGroup(model.CustomerId, model.BusNumber, model.GroupId);
            model.loadAfterSave = true;
            return View("BusGroup", model);
        }

        [HttpGet]
        public ActionResult IndexBusGroup(string busNumber, int customerId = 0, int groupId = 0)
        {
            ViewData["customerId"] = customerId;
            ViewData["busNumber"] = busNumber;
            ViewData["groupId"] = groupId;

            return View();
        }
        

        [HttpGet]
        public JsonResult DeleteBus(string busNumber, int customerId)
        {

            if (CurrentUser.UserId != null && CurrentUser.UserId > 0) 
            {
                ServiceResponse<bool> response = ServiceManager.DeleteBus(busNumber, customerId, CurrentUser.Username);
                return Json(response.Data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult PwdConfirmation(string busNumber, int customerId) {

            ViewBag.BusNumber = busNumber;
            ViewBag.CustomerId = customerId;

            return View();
        }

        [HttpPost]
        public JsonResult AdminPwdConfirmation(string password)
        {
            //confirm password
            bool result = ServiceManager.ConfirmAdminPassword(CurrentUser.Username, password);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

            #region Helper Functions
       
        #endregion

        #region Client Configurations

        public JsonResult GetClientConfigurationsTree()
        {


            IBI.Shared.Models.Accounts.User usr = SessionManager.Get<IBI.Shared.Models.Accounts.User>(SessionKeys.LoggedInUser);

            if (usr != null && usr.UserId > 0)
            {
                WebLogger.Log("GetClientConfigurationsTree - start", new object[] { usr.UserId });
                Tree tree = ServiceManager.GetClientConfigurationsTree(usr.UserId, this.Filters);
                WebLogger.Log("GetClientConfigurationsTree - end");

                List<BusNode> groups = tree.Groups;

                //string clientInformation = "";

                ////set color counters in ClientInformation

                //Dictionary<string, int> ColorCounter = tree.Counters;

                //foreach (string key in ColorCounter.Keys)
                //{
                //    int val = 0;
                //    ColorCounter.TryGetValue(key, out val);
                //    clientInformation += val.ToString() + ",";
                //}

                //clientInformation = clientInformation.TrimEnd(',');
                //clientInformation += "|";


                ////set filters in ClientInformation
                //clientInformation += Filters.Contains(BusStatusColor.GREEN) + ",";
                //clientInformation += Filters.Contains(BusStatusColor.GREY) + ",";
                //clientInformation += Filters.Contains(BusStatusColor.LIGHT_GREY) + ",";
                //clientInformation += Filters.Contains(BusStatusColor.ORANGE) + ",";
                //clientInformation += Filters.Contains(BusStatusColor.RED) + ",";
                //clientInformation += Filters.Contains(BusStatusColor.YELLOW);


                if (groups.Count > 0)
                {
                    groups[0].key = "0";
                    //groups[0].ClientInformation = clientInformation; //JsonConvert.SerializeObject(ColorCounter.ToList());
                }

                //return Json(groups, JsonRequestBehavior.AllowGet);

                var jsonResult = Json(groups, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            else
            {
                RedirectToAction("Login", "Account");

                List<GroupNode> nodeList = new List<GroupNode>();
                nodeList.Add(new GroupNode());
                return Json(nodeList, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //[Ajax(true)]
        public JsonResult SetClientConfigurations(int ConfigurationPropertyId, string PropertyValue, int GroupId, int CustomerID, string BusNumber)
        {
            ServiceResponse<bool> response = ServiceManager.SetClientConfigurations(ConfigurationPropertyId, PropertyValue, GroupId, CustomerID, BusNumber);
            return Json(response.Data, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }

}
