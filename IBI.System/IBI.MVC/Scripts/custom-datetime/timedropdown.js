﻿(function ($) {
    $.fn.timedropdown = function () {
        return this.each(function () {
            var txt = $(this);
            with (txt) {
                // Create a ul after the textbox containing the time values
                // for the dropdown (this list could be made dynamic)
                parent().append('<ul id="ul' + txt.attr('id') + '"><li>00:00</li><li>00:30</li><li>01:00</li><li>01:30</li><li>02:00</li><li>02:30</li><li>03:00</li><li>03:30</li><li>04:00</li><li>04:30</li><li>05:00</li><li>05:30</li><li>06:00</li><li>06:30</li><li>07:00</li><li>07:30</li><li>08:00</li><li>08:30</li><li>09:00</li><li>09:30</li><li>10:00</li><li>10:30</li><li>11:00</li><li>11:30</li><li>12:00</li><li>12:30</li><li>13:00</li><li>13:30</li><li>14:00</li><li>14:30</li><li>15:00</li><li>15:30</li><li>16:00</li><li>16:30</li><li>17:00</li><li>17:30</li><li>18:00</li><li>18:30</li><li>19:00</li><li>19:30</li><li>20:00</li><li>20:30</li><li>21:00</li><li>21:30</li><li>22:00</li><li>22:30</li><li>23:00</li><li>23:30</li></ul>');

                // Build the dropdown, attach a callback
                txtdropdown(timedropdown_shown);
            }
        });
    };
})(jQuery);

// Callback function to auto-scroll the dropdown to the nearest time
function timedropdown_shown(txt, ddl) {
    // If unable to parse time, default position is...
    var timeIndex = 0;

    // Parse the time value in the textbox
    var time = new Date('1/1/2010 ' + txt.val());

    if (!isNaN(time)) {
        // Determine the index of the li with the nearest time (round down)
        // We assume the times are static, every half-hour, starting with 12:00
        timeIndex = (time.getHours() * 2) + (time.getMinutes() / 30);
    }

    // Select the li at the matching index and scroll to it
    ddl.scrollTo(ddl.children('li').eq(timeIndex));
}
