﻿namespace IBI.JourneyImporter.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseFPlan = new System.Windows.Forms.Button();
            this.fplanfilepath = new System.Windows.Forms.TextBox();
            this.stopnamesfilepath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.stopcoordfilepath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.operationdaysfilepath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.journeydurationfilepath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnImport = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.gvJourney = new System.Windows.Forms.DataGridView();
            this.Line = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TotalJourneys = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.CustomerID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnBrowseStopNames = new System.Windows.Forms.Button();
            this.btnBrowseStopCoord = new System.Windows.Forms.Button();
            this.btnBrowseDaysFile = new System.Windows.Forms.Button();
            this.btnBrowseDuration = new System.Windows.Forms.Button();
            this.chkBxSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gvJourney)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "FPlan File";
            // 
            // btnBrowseFPlan
            // 
            this.btnBrowseFPlan.Location = new System.Drawing.Point(517, 29);
            this.btnBrowseFPlan.Name = "btnBrowseFPlan";
            this.btnBrowseFPlan.Size = new System.Drawing.Size(75, 23);
            this.btnBrowseFPlan.TabIndex = 1;
            this.btnBrowseFPlan.Text = "Browse";
            this.btnBrowseFPlan.UseVisualStyleBackColor = true;
            this.btnBrowseFPlan.Click += new System.EventHandler(this.btnBrowseFPlan_Click);
            // 
            // fplanfilepath
            // 
            this.fplanfilepath.Location = new System.Drawing.Point(95, 31);
            this.fplanfilepath.Name = "fplanfilepath";
            this.fplanfilepath.Size = new System.Drawing.Size(416, 20);
            this.fplanfilepath.TabIndex = 2;
            // 
            // stopnamesfilepath
            // 
            this.stopnamesfilepath.Location = new System.Drawing.Point(95, 68);
            this.stopnamesfilepath.Name = "stopnamesfilepath";
            this.stopnamesfilepath.Size = new System.Drawing.Size(144, 20);
            this.stopnamesfilepath.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Stop Names File";
            // 
            // stopcoordfilepath
            // 
            this.stopcoordfilepath.Location = new System.Drawing.Point(95, 103);
            this.stopcoordfilepath.Name = "stopcoordfilepath";
            this.stopcoordfilepath.Size = new System.Drawing.Size(144, 20);
            this.stopcoordfilepath.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Stop Coord File";
            // 
            // operationdaysfilepath
            // 
            this.operationdaysfilepath.Location = new System.Drawing.Point(95, 140);
            this.operationdaysfilepath.Name = "operationdaysfilepath";
            this.operationdaysfilepath.Size = new System.Drawing.Size(144, 20);
            this.operationdaysfilepath.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Op. Days File";
            // 
            // journeydurationfilepath
            // 
            this.journeydurationfilepath.Location = new System.Drawing.Point(95, 177);
            this.journeydurationfilepath.Name = "journeydurationfilepath";
            this.journeydurationfilepath.Size = new System.Drawing.Size(144, 20);
            this.journeydurationfilepath.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Duration File";
            // 
            // btnImport
            // 
            this.btnImport.Enabled = false;
            this.btnImport.Location = new System.Drawing.Point(689, 518);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(114, 41);
            this.btnImport.TabIndex = 15;
            this.btnImport.Text = "Import Journeys";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // gvJourney
            // 
            this.gvJourney.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvJourney.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.chkBxSelect});
            this.gvJourney.Location = new System.Drawing.Point(19, 223);
            this.gvJourney.Name = "gvJourney";
            this.gvJourney.Size = new System.Drawing.Size(784, 271);
            this.gvJourney.TabIndex = 19;
            // 
            // Line
            // 
            this.Line.Location = new System.Drawing.Point(367, 68);
            this.Line.Name = "Line";
            this.Line.Size = new System.Drawing.Size(144, 20);
            this.Line.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(284, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Line";
            // 
            // TotalJourneys
            // 
            this.TotalJourneys.Location = new System.Drawing.Point(367, 99);
            this.TotalJourneys.Name = "TotalJourneys";
            this.TotalJourneys.Size = new System.Drawing.Size(144, 20);
            this.TotalJourneys.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(284, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Total Journeys";
            // 
            // CustomerID
            // 
            this.CustomerID.Location = new System.Drawing.Point(646, 68);
            this.CustomerID.Name = "CustomerID";
            this.CustomerID.Size = new System.Drawing.Size(144, 20);
            this.CustomerID.TabIndex = 25;
            this.CustomerID.Text = "2164";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(563, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Customer ID";
            // 
            // btnBrowseStopNames
            // 
            this.btnBrowseStopNames.Location = new System.Drawing.Point(245, 68);
            this.btnBrowseStopNames.Name = "btnBrowseStopNames";
            this.btnBrowseStopNames.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseStopNames.TabIndex = 26;
            this.btnBrowseStopNames.Text = "...";
            this.btnBrowseStopNames.UseVisualStyleBackColor = true;
            this.btnBrowseStopNames.Click += new System.EventHandler(this.btnBrowseStopNames_Click);
            // 
            // btnBrowseStopCoord
            // 
            this.btnBrowseStopCoord.Location = new System.Drawing.Point(245, 101);
            this.btnBrowseStopCoord.Name = "btnBrowseStopCoord";
            this.btnBrowseStopCoord.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseStopCoord.TabIndex = 27;
            this.btnBrowseStopCoord.Text = "...";
            this.btnBrowseStopCoord.UseVisualStyleBackColor = true;
            this.btnBrowseStopCoord.Click += new System.EventHandler(this.btnBrowseStopCoord_Click);
            // 
            // btnBrowseDaysFile
            // 
            this.btnBrowseDaysFile.Location = new System.Drawing.Point(245, 138);
            this.btnBrowseDaysFile.Name = "btnBrowseDaysFile";
            this.btnBrowseDaysFile.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseDaysFile.TabIndex = 28;
            this.btnBrowseDaysFile.Text = "...";
            this.btnBrowseDaysFile.UseVisualStyleBackColor = true;
            this.btnBrowseDaysFile.Click += new System.EventHandler(this.btnBrowseDaysFile_Click);
            // 
            // btnBrowseDuration
            // 
            this.btnBrowseDuration.Location = new System.Drawing.Point(245, 174);
            this.btnBrowseDuration.Name = "btnBrowseDuration";
            this.btnBrowseDuration.Size = new System.Drawing.Size(33, 23);
            this.btnBrowseDuration.TabIndex = 29;
            this.btnBrowseDuration.Text = "...";
            this.btnBrowseDuration.UseVisualStyleBackColor = true;
            this.btnBrowseDuration.Click += new System.EventHandler(this.btnBrowseDuration_Click);
            // 
            // chkBxSelect
            // 
            this.chkBxSelect.HeaderText = "";
            this.chkBxSelect.Name = "chkBxSelect";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(824, 610);
            this.Controls.Add(this.btnBrowseDuration);
            this.Controls.Add(this.btnBrowseDaysFile);
            this.Controls.Add(this.btnBrowseStopCoord);
            this.Controls.Add(this.btnBrowseStopNames);
            this.Controls.Add(this.CustomerID);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TotalJourneys);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.Line);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.gvJourney);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.journeydurationfilepath);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.operationdaysfilepath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.stopcoordfilepath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.stopnamesfilepath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fplanfilepath);
            this.Controls.Add(this.btnBrowseFPlan);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Hafas Importer";
            ((System.ComponentModel.ISupportInitialize)(this.gvJourney)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowseFPlan;
        private System.Windows.Forms.TextBox fplanfilepath;
        private System.Windows.Forms.TextBox stopnamesfilepath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox stopcoordfilepath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox operationdaysfilepath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox journeydurationfilepath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.DataGridView gvJourney;
        private System.Windows.Forms.TextBox Line;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TotalJourneys;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox CustomerID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnBrowseStopNames;
        private System.Windows.Forms.Button btnBrowseStopCoord;
        private System.Windows.Forms.Button btnBrowseDaysFile;
        private System.Windows.Forms.Button btnBrowseDuration;
        private System.Windows.Forms.DataGridViewCheckBoxColumn chkBxSelect;
    }
}

