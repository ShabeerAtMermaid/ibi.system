﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemLog.aspx.cs" Inherits="CMS_Aspx.SystemLog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="LblMessage" runat="server" 
        Text="You do not have access to this module" ForeColor="Red"></asp:Label>
    <asp:GridView ID="GridLog" runat="server" AutoGenerateColumns="False" GridLines="None"
            AllowSorting="True" BorderColor="Black" EnableViewState="False">
        <AlternatingRowStyle BackColor="LightGray" />
        <Columns>
            <asp:ImageField Visible="False">
                <HeaderStyle Width="15px" />
            </asp:ImageField>
            <asp:BoundField DataField="Timestamp" HeaderText="Timestamp" 
                SortExpression="Timestamp">
            <HeaderStyle Width="140px" />
            </asp:BoundField>
            <asp:BoundField DataField="EntryCategory" HeaderText="Category" 
                SortExpression="EntryCategory">
            <HeaderStyle Width="140px" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description" 
                SortExpression="Description">
            <HeaderStyle Width="640px" />
            </asp:BoundField>
        </Columns>
        <HeaderStyle ForeColor="White" HorizontalAlign="Left" Height="20px" CssClass="TableHeader" />
        <RowStyle BackColor="WhiteSmoke" VerticalAlign="Top" />
    </asp:GridView>
</asp:Content>
