﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using System.Drawing;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class BusHistory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridHistory.RowDataBound += new GridViewRowEventHandler(GridHistory_RowDataBound);

            if (!this.IsPostBack)
            {
                this.CtrlHistoryDate.SelectedDate = DateTime.Now;
                this.TxtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");

                this.loadHistory();
            }
        }

        private void GridHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    headerCell.HorizontalAlign = HorizontalAlign.Left;
                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }

                e.Row.Cells[0].Width = 177;

                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    //int columnName = int.Parse(e.Row.Cells[i].Text);

                    //if (columnName % 60 == 0)
                    //    e.Row.Cells[i].Text = TimeSpan.FromMinutes(columnName).Hours.ToString();
                    //else
                    e.Row.Cells[i].Text = String.Empty;

                    e.Row.Cells[i].Width = 8;
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int busNumber = int.Parse(e.Row.Cells[0].Text);

                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    int columnName = (i - 1) * 15;
                    String[] cellValues = e.Row.Cells[i].Text.Split('|');

                    String cellValue = cellValues[0];
                    String remarks = cellValues.Length == 1 ? "" : cellValues[1];

                    if (String.Compare("Yes", cellValue, true) == 0)
                        e.Row.Cells[i].BackColor = Color.Green;
                    else if (String.Compare("No", cellValue, true) == 0)
                        e.Row.Cells[i].BackColor = Color.Red;
                    else if (String.Compare("SERVICE", cellValue, true) == 0)
                        e.Row.Cells[i].BackColor = Color.FromArgb(255, 201, 14);
                    else if (String.Compare("NA", cellValue, true) == 0)
                        e.Row.Cells[i].BackColor = Color.LightGreen;
                    //else
                    //    e.Row.Cells[i].BackColor = Color.White;

                    e.Row.Cells[i].Text = String.Empty;

                    TimeSpan logTime = TimeSpan.FromMinutes(columnName);

                    String mouseOverText = "Bus " + busNumber + " - " + logTime.Hours + ":" + (logTime.Minutes < 10 ? "0" + logTime.Minutes.ToString() : logTime.Minutes.ToString()) + Environment.NewLine +
                                           remarks;

                    if (String.Compare("SERVICE", cellValue, true) == 0)
                        mouseOverText = "Has Service case" + Environment.NewLine + mouseOverText;

                    e.Row.Cells[i].Attributes.Add("alt", mouseOverText);
                    e.Row.Cells[i].Attributes.Add("title", mouseOverText);
                }
            }
        }

        private void loadHistory()
        {
            DataTable historyTable = this.generateHistoryTable();
            //DateTime historyDate = this.CtrlHistoryDate.SelectedDate.Value;
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "dd-MM-yyyy";
            dateFormat.LongDatePattern = "dd-MM-yyyy";
            dateFormat.FullDateTimePattern = "dd-MM-yyyy";
            dateFormat.DateSeparator = "-";

            DateTime historyDate = DateTime.Parse(this.TxtDate.Text, dateFormat);

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();
                List<int> busNumbers = new List<int>();

                String script = "SELECT DISTINCT [BusNumber] FROM [Clients]" + Environment.NewLine +
                                    "WHERE [InOperation]=1" + Environment.NewLine +
                                    "ORDER BY [BusNumber] ASC";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                busNumbers.Add(int.Parse(dataReader.GetString(dataReader.GetOrdinal("BusNumber"))));
                            }
                        }
                    }
                }

                DateTime baseTime = new DateTime(historyDate.Year, historyDate.Month, historyDate.Day, 0, 0, 0);

                script = "SELECT [Timestamp], [BusNumber], [IsOK], [LastSchedule], [Remarks], [MarkedForService], [ServiceLevel] FROM [BusStatusLog]" + Environment.NewLine +
                            "WHERE [Timestamp] BETWEEN '" + baseTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '" + baseTime.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                int busNumber = dataReader.GetInt32(dataReader.GetOrdinal("BusNumber"));

                                if (busNumbers.Contains(busNumber))
                                {
                                    DateTime logTimestamp = dataReader.GetDateTime(dataReader.GetOrdinal("Timestamp"));
                                    DateTime scheduleTimestamp = dataReader.IsDBNull(dataReader.GetOrdinal("LastSchedule")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("LastSchedule"));
                                    String remarks = dataReader.IsDBNull(dataReader.GetOrdinal("Remarks")) ? "" : dataReader.GetString(dataReader.GetOrdinal("Remarks"));
                                    Boolean markedForService = dataReader.IsDBNull(dataReader.GetOrdinal("MarkedForService")) ? false : dataReader.GetBoolean(dataReader.GetOrdinal("MarkedForService"));
                                    int serviceLevel = dataReader.IsDBNull(dataReader.GetOrdinal("ServiceLevel")) ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("ServiceLevel"));

                                    Boolean isOK = dataReader.GetBoolean(dataReader.GetOrdinal("IsOK"));

                                    DataRow row = this.getRow(historyTable, busNumber);

                                    row["Bus"] = busNumber;
                                    //int columnName = (24 * 4) / ((logTimestamp.Hour * 4) + logTimestamp.Minute);
                                    int columnName = (logTimestamp.Hour * 60) + logTimestamp.Minute;

                                    String columnValue = "NA";

                                    if (scheduleTimestamp >= logTimestamp.AddMinutes(-15))
                                        columnValue = isOK ? "Yes" : "No";

                                    if (markedForService || serviceLevel > 0)
                                        columnValue = "SERVICE";

                                    row[columnName.ToString()] = columnValue + "|" + remarks;
                                }
                            }
                        }
                    }
                }
            }

            historyTable.DefaultView.Sort = "Bus ASC";

            this.GridHistory.DataSource = historyTable;
            this.GridHistory.DataBind();

            this.CtrlHistoryDate.SelectedDate = historyDate;
        }

        private DataTable generateHistoryTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Bus", typeof(int));

            for (int i = 0; i < (24 * 60); i += 15)
            {
                table.Columns.Add(i.ToString(), typeof(String));
            }

            return table;
        }

        private DataRow getRow(DataTable source, int busNumber)
        {
            DataRow[] rows = source.Select("Bus=" + busNumber);

            if (rows.Length > 0)
            {
                return rows[0];
            }
            else
            {
                DataRow row = source.NewRow();
                source.Rows.Add(row);

                return row;
            }
        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.loadHistory();
        }
    }
}