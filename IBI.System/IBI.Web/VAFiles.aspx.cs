﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class VAFiles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridVAFiles.RowDataBound += new GridViewRowEventHandler(GridVAFiles_RowDataBound);
        }

        private void GridVAFiles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    if (String.Compare("Approved", headerCell.Text) != 0 && String.Compare("O", headerCell.Text) != 0)
                        headerCell.HorizontalAlign = HorizontalAlign.Left;

                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }              
            }

            if (e.Row.DataItem != null)
            {               
                    DataRow dataRow = ((DataRowView)e.Row.DataItem).Row;

                    String fileName = dataRow["FileName"].ToString();

                    ((ImageButton)e.Row.FindControl("BtnDownloadFile")).Attributes.Add("onclick", "openAudioFile('" + fileName + "')");
            }
        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.loadData();
        }

        private void loadData()
        {
            DataTable dataSource = this.GenerateDataTable();

            if (!String.IsNullOrEmpty(this.TxtStopName.Text))
            {
                DataRow dataRow;

                using (SqlConnection connection = AppSettings.GetTTSDatabaseConnection())
                {
                    connection.Open();

                    String script = "SELECT [Text], [LastModified], [Approved], [FileName], [Version]" + Environment.NewLine +
                                    "FROM [AudioFiles]" + Environment.NewLine +
                                    "WHERE [Text] like '%" + this.TxtStopName.Text.Replace("'", "''") + "%'";

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataReader dataReader = command.ExecuteReader())
                        {
                            while (dataReader.Read())
                            {
                                String text = dataReader.GetString(dataReader.GetOrdinal("Text"));
                                DateTime lastModified = dataReader.GetDateTime(dataReader.GetOrdinal("LastModified"));
                                Boolean approved = dataReader.GetBoolean(dataReader.GetOrdinal("Approved"));
                                String filename = dataReader.GetString(dataReader.GetOrdinal("FileName"));
                                int version = dataReader.GetInt32(dataReader.GetOrdinal("Version"));

                                dataRow = dataSource.NewRow();
                                dataRow["StopName"] = text;
                                dataRow["LastModified"] = lastModified;
                                dataRow["Approved"] = approved;
                                dataRow["FileName"] = filename;
                                dataRow["Version"] = version;
                             
                                dataSource.Rows.Add(dataRow);
                            }
                        }
                    }
                }
            }

            dataSource.DefaultView.Sort = "StopName ASC";

            this.GridVAFiles.DataSource = dataSource;
            this.GridVAFiles.DataBind();
        }

        private DataTable GenerateDataTable()
        {
            DataTable dataSource = new DataTable("VAFiles");
            dataSource.Columns.Add("StopName", typeof(String));
            dataSource.Columns.Add("LastModified", typeof(DateTime));
            dataSource.Columns.Add("Approved", typeof(Boolean));
            dataSource.Columns.Add("FileName", typeof(String));
            dataSource.Columns.Add("Version", typeof(String));

            return dataSource;
        }
    }
}