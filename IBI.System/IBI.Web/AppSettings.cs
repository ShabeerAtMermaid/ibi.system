﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace IBI.CMS
{
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {            
            String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetHotspotDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["HotspotDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetvTouchDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["vTouchDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static SqlConnection GetTTSDatabaseConnection()
        {
            String connectionString = ConfigurationManager.AppSettings["TTSDatabaseConnection"];

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }

        public static TimeSpan GetOfflineTolerance()
        {
            String tolerance = ConfigurationManager.AppSettings["OfflineTolerance"];

            return TimeSpan.Parse(tolerance);
        }

        public static Boolean ShouldValidateSchedules()
        {
            String value = ConfigurationManager.AppSettings["ValidateSchedules"];

            if (!String.IsNullOrEmpty(value))
                return Boolean.Parse(value);

            return true;
        }
    }
}