﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Schedules.aspx.cs" Inherits="CMS_Aspx.Schedules" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <script language="javascript" type="text/javascript">
     function openSchedule(scheduleID) {
         window.open("http://ibi.mermaid.dk/REST/Schedule/Schedule.xml?scheduleid=" + scheduleID);

         return true;
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView ID="GridList" runat="server" AutoGenerateColumns="False" GridLines="None"
            AllowSorting="True" BorderColor="Black" EnableViewState="False">
        <AlternatingRowStyle BackColor="LightGray" />
        <Columns>
            <asp:TemplateField Visible="False">
                <ItemTemplate>
                    <asp:CheckBox ID="ChkInOperation" runat="server" Checked="false" />
                </ItemTemplate>
                <HeaderStyle Width="20px" />
            </asp:TemplateField>
            <asp:BoundField DataField="Line" HeaderText="Line" SortExpression="Line">
            <HeaderStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="FromName" HeaderText="From" 
                SortExpression="FromName">
            <HeaderStyle Width="250px" />
            </asp:BoundField>
            <asp:BoundField DataField="Destination" HeaderText="Destination" 
                SortExpression="Destination">
            <HeaderStyle Width="300px" />
            </asp:BoundField>
            <asp:BoundField DataField="ViaName" HeaderText="Via" SortExpression="ViaName">
            <HeaderStyle Width="250px" />
            </asp:BoundField>
            <asp:TemplateField>
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:ImageButton ID="BtnViewSchedule" runat="server" ImageUrl="~/Images/UnorderedList_16.png"
                            CausesValidation="False" />
                </ItemTemplate>
                <HeaderStyle Width="30px" />
            </asp:TemplateField>
        </Columns>
        <HeaderStyle ForeColor="White" HorizontalAlign="Left" Height="20px" CssClass="TableHeader" />
        <RowStyle BackColor="WhiteSmoke" />
    </asp:GridView>
    <br />
    <br />
    <center>
        <table cellpadding="1" cellspacing="0" width="900px">
        <tr>
            <td class="TableHeader" align="left" colspan="2">
                &nbsp; Upload Schedule</td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" align="left" style="width: 200px">
                &nbsp; Customer ID:
            </td>
            <td class="TableBackground" align="left" style="width: 700pX">
                <asp:TextBox ID="txtCustomerID" runat="server" Width="142px"></asp:TextBox>
            &nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="txtCustomerID" ErrorMessage="CustomerID is invalid" 
                    ForeColor="Red" ValidationExpression="^(\d{4})$"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtCustomerID" ErrorMessage="CustomerID is missing" 
                    ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" align="left">
                &nbsp; Schedule:
            </td>
            <td class="TableBackground" align="left">
                <asp:FileUpload ID="FileSchedule" runat="server" Width="536px" />
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="TableBackground" align="center" colspan="2">
                <asp:ImageButton ID="BtnUploadSchedule" runat="server" 
                    ImageUrl="~/Images/ButtonSend.png" onclick="BtnUploadSchedule_Click" />
            </td>
        </tr>
        <tr>
            <td class="TableBackground" colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    </center>
</asp:Content>
