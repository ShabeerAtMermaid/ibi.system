﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Globalization;
using System.Data;
using System.Drawing;
using System.IO;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class InfotainmentAnalyze : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.GridHistory.RowDataBound += new GridViewRowEventHandler(GridHistory_RowDataBound);

            if (!this.IsPostBack)
            {
                this.CtrlHistoryDate.SelectedDate = DateTime.Now;
                this.TxtDate.Text = DateTime.Now.ToString("dd-MM-yyyy");

                this.loadHistory();
            }
        }

        private void GridHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell headerCell in e.Row.Cells)
                {
                    headerCell.HorizontalAlign = HorizontalAlign.Left;
                    headerCell.VerticalAlign = VerticalAlign.Middle;
                }

                e.Row.Cells[0].Width = 177;

                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].Text = String.Empty;

                    e.Row.Cells[i].Width = 8;
                }
            }
            else if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
                dateFormat.ShortDatePattern = "dd-MM-yyyy";
                dateFormat.LongDatePattern = "dd-MM-yyyy";
                dateFormat.FullDateTimePattern = "dd-MM-yyyy";
                dateFormat.DateSeparator = "-";

                DateTime historyDate = DateTime.Parse(this.TxtDate.Text, dateFormat);

                int busNumber = int.Parse(e.Row.Cells[0].Text);
                DateTime baseTime = new DateTime(historyDate.Year, historyDate.Month, historyDate.Day, 0, 0, 0);

                String folderName = "Resources/Screenshots/VTC/" + busNumber + "/";

                for (int i = 1; i < e.Row.Cells.Count; i++)
                {
                    int columnName = (i - 1) * 15;
                    DateTime columnTime = baseTime.AddMinutes(columnName);

                    Color cellColor = e.Row.Cells[i].BackColor;
                    String cellValue = e.Row.Cells[i].Text;

                    if (String.Compare("NA", cellValue, true) == 0)
                        cellColor = Color.LightGreen;

                    if (Directory.Exists(Server.MapPath(folderName)))
                    {
                        foreach (String imagePath in Directory.GetFiles(Server.MapPath(folderName), "Screenshot_" + busNumber + "_VTC_" + columnTime.ToString("yyyyMMdd_HHmm") + "*.jpg"))
                        {
                            e.Row.Cells[i].Style["cursor"] = "hand";

                            cellColor = Color.Green;

                            if (!isImageValid(imagePath))
                            {
                                e.Row.Cells[i].Attributes["onclick"] = "openScreenshots('" + busNumber + "', '" + Path.GetFileNameWithoutExtension(imagePath) + "')";

                                cellColor = Color.Red;

                                break;
                            }

                            e.Row.Cells[i].Attributes["onclick"] = "openScreenshots('" + busNumber + "', '" + Path.GetFileNameWithoutExtension(imagePath) + "')";
                        }
                    }

                    e.Row.Cells[i].BackColor = cellColor;

                    e.Row.Cells[i].Text = String.Empty;

                    TimeSpan logTime = TimeSpan.FromMinutes(columnName);

                    String mouseOverText = "Bus " + busNumber + " - " + logTime.Hours + ":" + (logTime.Minutes < 10 ? "0" + logTime.Minutes.ToString() : logTime.Minutes.ToString());

                    e.Row.Cells[i].Attributes.Add("alt", mouseOverText);
                    e.Row.Cells[i].Attributes.Add("title", mouseOverText);
                }
            }
        }

        private Boolean isImageValid(String imagePath)
        {
            try
            {
                int colorTolerance = 10;

                using (System.Drawing.Bitmap image = new System.Drawing.Bitmap(imagePath))
                {
                    Color upperLeftColor = image.GetPixel(25, 25);
                    Color leftColor = image.GetPixel(25, 125);
                    Color topColor = image.GetPixel(125, 25);

                    if (upperLeftColor.R < colorTolerance && upperLeftColor.G < colorTolerance && upperLeftColor.B < colorTolerance)
                        return false;

                    if (leftColor.R < colorTolerance && leftColor.G < colorTolerance && leftColor.B < colorTolerance)
                        return false;

                    if (topColor.R < colorTolerance && topColor.G < colorTolerance && topColor.B < colorTolerance)
                        return false;

                    Boolean contentBlack = true;

                    for (int x = 60; x < image.Width; x += 25)
                    {
                        for (int y = 60; y < image.Height; y += 25)
                        {
                            Color contentColor = image.GetPixel(x, y);

                            if (contentColor.R > colorTolerance || contentColor.G > colorTolerance || contentColor.B > colorTolerance)
                                contentBlack = false;
                            
                            if (!contentBlack)
                                break;
                        }

                        if (!contentBlack)
                            break;
                    }

                    if (contentBlack)
                        return false;

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void loadHistory()
        {
            DataTable historyTable = this.generateHistoryTable();
            //DateTime historyDate = this.CtrlHistoryDate.SelectedDate.Value;
            DateTimeFormatInfo dateFormat = (DateTimeFormatInfo)new System.Globalization.CultureInfo("da-DK").DateTimeFormat.Clone();
            dateFormat.ShortDatePattern = "dd-MM-yyyy";
            dateFormat.LongDatePattern = "dd-MM-yyyy";
            dateFormat.FullDateTimePattern = "dd-MM-yyyy";
            dateFormat.DateSeparator = "-";

            DateTime historyDate = DateTime.Parse(this.TxtDate.Text, dateFormat);

            using (SqlConnection connection = AppSettings.GetIBIDatabaseConnection())
            {
                connection.Open();
                List<int> busNumbers = new List<int>();

                String script = "SELECT DISTINCT [BusNumber] FROM [Clients]" + Environment.NewLine +
                                "WHERE [InOperation]=1 AND [ClientType]='VTC'" + Environment.NewLine +
                                "ORDER BY [BusNumber] ASC";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                busNumbers.Add(int.Parse(dataReader.GetString(dataReader.GetOrdinal("BusNumber"))));
                            }
                        }
                    }
                }

                DateTime baseTime = new DateTime(historyDate.Year, historyDate.Month, historyDate.Day, 0, 0, 0);

                script = "SELECT [Timestamp], [BusNumber], [IsOK], [LastSchedule] FROM [BusStatusLog]" + Environment.NewLine +
                         "WHERE [Timestamp] BETWEEN '" + baseTime.ToString("yyyy-MM-dd HH:mm:ss") + "' AND '" + baseTime.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss") + "'";

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader dataReader = command.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {
                            while (dataReader.Read())
                            {
                                int busNumber = dataReader.GetInt32(dataReader.GetOrdinal("BusNumber"));

                                if (busNumbers.Contains(busNumber))
                                {
                                    DateTime logTimestamp = dataReader.GetDateTime(dataReader.GetOrdinal("Timestamp"));
                                    DateTime scheduleTimestamp = dataReader.IsDBNull(dataReader.GetOrdinal("LastSchedule")) ? DateTime.MinValue : dataReader.GetDateTime(dataReader.GetOrdinal("LastSchedule"));

                                    Boolean isOK = dataReader.GetBoolean(dataReader.GetOrdinal("IsOK"));

                                    DataRow row = this.getRow(historyTable, busNumber);

                                    row["Bus"] = busNumber;
                                    //int columnName = (24 * 4) / ((logTimestamp.Hour * 4) + logTimestamp.Minute);
                                    int columnName = (logTimestamp.Hour * 60) + logTimestamp.Minute;

                                    String columnValue = "NA";

                                    if (scheduleTimestamp >= logTimestamp.AddMinutes(-15))
                                        columnValue = isOK ? "Yes" : "No";

                                    row[columnName.ToString()] = columnValue;
                                }
                            }
                        }
                    }
                }
            }

            historyTable.DefaultView.Sort = "Bus ASC";

            this.GridHistory.DataSource = historyTable;
            this.GridHistory.DataBind();

            this.CtrlHistoryDate.SelectedDate = historyDate;
        }

        private DataRow getRow(DataTable source, int busNumber)
        {
            DataRow[] rows = source.Select("Bus=" + busNumber);

            if (rows.Length > 0)
            {
                return rows[0];
            }
            else
            {
                DataRow row = source.NewRow();
                source.Rows.Add(row);

                return row;
            }
        }

        private DataTable generateHistoryTable()
        {
            DataTable table = new DataTable();
            table.Columns.Add("Bus", typeof(int));

            for (int i = 0; i < (24 * 60); i += 15)
            {
                table.Columns.Add(i.ToString(), typeof(String));
            }

            return table;
        }

        protected void BtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            this.loadHistory();
        }
    }
}