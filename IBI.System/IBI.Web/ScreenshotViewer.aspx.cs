﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using IBI.CMS;

namespace CMS_Aspx
{
    public partial class FrmScreenshotViewer : System.Web.UI.Page
    {
        private List<String> screenshotList = new List<string>();
        private int currentImageIndex = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            String busNumber = Request["busnumber"];
            String clientType = Request["clienttype"];
            int imageIndex = Request["imageindex"] == null ? -1 : int.Parse(Request["imageindex"]);
            String wantedImage = Request["imagename"];

            String folderName = "Resources/Screenshots/" + clientType + "/" + busNumber + "/";

            //this.CllImageName.InnerText = Server.MapPath(folderName);

            if (Directory.Exists(Server.MapPath(folderName)))
            {
                String[] imageList = Directory.GetFiles(Server.MapPath(folderName), "*.jpg");

                for (int i = 0; i < imageList.Length; i++)
                {
                    String imagePath = imageList[i];
                    
                    screenshotList.Add(folderName + Path.GetFileName(imagePath));

                    if (!String.IsNullOrEmpty(wantedImage))
                    {
                        if (String.Compare(wantedImage, Path.GetFileNameWithoutExtension(imagePath), true) == 0)
                            imageIndex = i;
                    }
                }
            }

            if (screenshotList.Count > 0)
            {
                if (imageIndex == -1)
                    loadImage(screenshotList.Count - 1);
                else
                    loadImage(imageIndex);
            }

            this.LnkVTC.NavigateUrl = "ScreenshotViewer.aspx?clienttype=VTC&busnumber=" + busNumber;
            this.LnkDCU.NavigateUrl = "ScreenshotViewer.aspx?clienttype=DCU&busnumber=" + busNumber;
        }

        private void loadImage(int imageNumber)
        {
            this.ImgScreenshot.ImageUrl = screenshotList[imageNumber];

            this.CllImageName.InnerText = Path.GetFileNameWithoutExtension(screenshotList[imageNumber]);
            this.CllImageCount.InnerText = (imageNumber + 1) + " of " + screenshotList.Count;

            this.currentImageIndex = imageNumber;
        }

        protected void BtnPrevious_Click(object sender, ImageClickEventArgs e)
        {
            //this.loadImage(Math.Max(0, this.currentImageIndex - 1));
            String busNumber = Request["busnumber"];
            String clientType = Request["clienttype"];
            Response.Redirect("ScreenshotViewer.aspx?busnumber=" + busNumber + "&clienttype=" + clientType + "&imageindex=" + Math.Max(0, this.currentImageIndex - 1));
        }

        protected void BtnNext_Click(object sender, ImageClickEventArgs e)
        {
            //this.loadImage(Math.Min(screenshotList.Count - 1, this.currentImageIndex + 1));
            String busNumber = Request["busnumber"];
            String clientType = Request["clienttype"];
            Response.Redirect("ScreenshotViewer.aspx?busnumber=" + busNumber + "&clienttype=" + clientType + "&imageindex=" + Math.Min(screenshotList.Count - 1, this.currentImageIndex + 1));
        }
    }
}