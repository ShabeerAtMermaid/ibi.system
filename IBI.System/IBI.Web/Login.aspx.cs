﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.DirectoryServices.AccountManagement;
using System.Configuration;

namespace CMS_Aspx
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.UserName.Attributes.Add("onkeypress", "return clickButton(event,'" + this.BtnLogin.ClientID + "')");
            //this.Password.Attributes.Add("onkeypress", "return clickButton(event,'" + this.BtnLogin.ClientID + "')");

            if (this.IsPostBack)
                this.CheckLogin(true);
            else
                this.CheckLogin(false);
        }

        private void CheckLogin(bool interactiveCheck)
        {
            bool authorized = false;
            bool hasAccess = false;
            bool isAdmin = false;

            string tmpUsername = "";
            Session["IsAdmin"] = isAdmin;

            //if (Session["User"] != null)
            //{
            //    authorized = true;
            //    hasAccess = true;

            //    tmpUsername = Session["User"].ToString();
            //}
            //else if (interactiveCheck)
            if (interactiveCheck)
            {
                string domain = ConfigurationManager.AppSettings["Domain"];

                using (PrincipalContext context = new PrincipalContext(ContextType.Domain, domain))
                {
                    authorized = context.ValidateCredentials(this.UserName.Text, this.Password.Text);
                    tmpUsername = this.UserName.Text;

                    if (authorized)
                    {
                        using (GroupPrincipal adminGroupPrincipal = GroupPrincipal.FindByIdentity(context, IdentityType.Name, "Administrators"))
                        {
                            using (GroupPrincipal arrivaGroupPrincipal = GroupPrincipal.FindByIdentity(context, IdentityType.Name, "IBI_Arriva"))
                            {
                                using (GroupPrincipal cityTrafikGroupPrincipal = GroupPrincipal.FindByIdentity(context, IdentityType.Name, "IBI_CityTrafik"))
                                {
                                    using (UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, this.UserName.Text))
                                    {
                                        if (userPrincipal != null)
                                        {
                                            tmpUsername = userPrincipal.DisplayName;

                                            Boolean isMemberOfAcceptedGroup = false;
                                            
                                            if (adminGroupPrincipal != null && userPrincipal.IsMemberOf(adminGroupPrincipal))
                                                isAdmin = true;

                                            if (!isMemberOfAcceptedGroup && arrivaGroupPrincipal != null && userPrincipal.IsMemberOf(arrivaGroupPrincipal))
                                                isMemberOfAcceptedGroup = true;

                                            if (!isMemberOfAcceptedGroup && cityTrafikGroupPrincipal != null && userPrincipal.IsMemberOf(cityTrafikGroupPrincipal))
                                                isMemberOfAcceptedGroup = true;

                                            if (isAdmin || isMemberOfAcceptedGroup)
                                                hasAccess = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (!interactiveCheck)
            {
                string[] acceptedIPs = ConfigurationManager.AppSettings["AcceptedIPs"].Split(new string[] { ";" }, StringSplitOptions.None);

                foreach (string tmpIP in acceptedIPs)
                {
                    if (String.Compare(tmpIP, Request.UserHostAddress, true) == 0)
                    {
                        authorized = true;
                        hasAccess = true;
                        tmpUsername = "Mermaid";
                        isAdmin = true;

                        break;
                    }
                }
            }

            //if (authorized)
            //{
            //    hasAccess = true;
            //}

            if (authorized)
            {
                if (hasAccess)
                {
                    Session["User"] = tmpUsername;
                    Session["IsAdmin"] = isAdmin;

                    FormsAuthentication.RedirectFromLoginPage(tmpUsername, false);
                }
                else
                {
                    Session["User"] = null;
                    this.FailureText.Text = "You do not have access";
                }
            }
            else
            {
                Session["User"] = null;

                if (interactiveCheck)
                    this.FailureText.Text = "Login failed";
            }
        }

        protected void BtnLogin_Click(object sender, ImageClickEventArgs e)
        {
            //Do nothing since CheckLogin is already called
        }
    }
}