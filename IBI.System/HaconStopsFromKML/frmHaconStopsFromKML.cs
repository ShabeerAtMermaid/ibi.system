﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Spatial;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace HaconStopsFromKML
{
    public partial class frmHaconStopsFromKML : Form
    {
        public frmHaconStopsFromKML()
        {
            InitializeComponent();
        }
         

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtPath.Text = openFileDialog1.FileName;
                ProcessStops();
            }
        }

        private void ProcessStops()
        {
            string filePath = txtPath.Text;

            try
            {
                
                if (File.Exists(filePath))
                {
                    string fileText = File.ReadAllText(filePath, System.Text.Encoding.UTF8);
                    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    MemoryStream memStream = new MemoryStream(encoding.GetBytes(fileText));

                    XDocument xDoc = XDocument.Load(memStream);

                    //String name = (from p in xDoc.Descendants("name")
                                                     // select p.Value); 
                
                    using( IBIDataModel dbContext = new IBIDataModel())
                    {

                        foreach (var st in xDoc.Descendants("Placemark"))
                        {
                            var name = st.Descendants("name").FirstOrDefault().Value; //.ToString(SaveOptions.DisableFormatting);
                            string stopId = name.Substring(0, name.IndexOf(' '));
                            string stopName = name.Substring(name.IndexOf(' ') + 1);
                            string cords = st.Descendants("coordinates").FirstOrDefault().Value;
                            string lat = cords.Substring(cords.IndexOf(',') + 1);
                            string lon = cords.Substring(0, cords.IndexOf(','));

                            txtResults.Text += stopId + " : " + stopName + " : " + lat + " : " + lon + Environment.NewLine;

                            string sqlPoint = string.Format("POINT({0} {1})", lon, lat);

                            decimal gid = decimal.Parse(IBI.Shared.ScheduleHelper.TransformStopNo(stopId, IBI.Shared.ScheduleHelper.StopPrefix.HACON));

                            bool exists = true;
                            IBI.DataAccess.DBModels.Stop stop = dbContext.Stops.Where(s => s.GID == gid).FirstOrDefault();


                            

                            if(stop == null)
                            {
                                stop = new IBI.DataAccess.DBModels.Stop();

                                stop.OriginalName = stopName;
                                stop.OriginalGPS = DbGeography.FromText(sqlPoint, 4326);
                                stop.OriginalCreated = DateTime.Now;                                
                                exists = false;
                            }
                            
                            stop.GID = gid;
                            if(stopName.Contains('('))
                            {
                                stopName = stopName.Substring(0, stopName.IndexOf('(')).Trim();
                            }
                            stop.StopName = stopName;
                            stop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            stop.LastUpdated = DateTime.Now;
                            stop.StopSource = "HACON";

                            if(!exists)
                            {
                                dbContext.Stops.Add(stop);
                            } 
                            
                        }

                        dbContext.SaveChanges();   


                    }


                }
                
                MessageBox.Show("All stops imported successfully" );
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failure : " + ex.Message + " Stack : " + ex.StackTrace.ToString());
            }
        }
    }
}
