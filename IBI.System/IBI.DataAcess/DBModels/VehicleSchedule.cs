//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class VehicleSchedule
    {
    	 
        public VehicleSchedule()
        {
            this.VehicleScheduleJourneys = new HashSet<VehicleScheduleJourney>();
        }
    
        public int VehicleScheduleId { get; set; }
        public string ExternalReference { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<System.DateTime> PlannedStartTime { get; set; }
        public Nullable<System.DateTime> PlannedEndTime { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
    
        public virtual ICollection<VehicleScheduleJourney> VehicleScheduleJourneys { get; set; }
    }
}
