//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class Customer
    {
    	 
        public Customer()
        {
            this.CustomerAudioFiles = new HashSet<CustomerAudioFile>();
            this.DataUpdateStatuses = new HashSet<DataUpdateStatus>();
            this.Groups = new HashSet<Group>();
            this.SignContents = new HashSet<SignContent>();
            this.SignDatas = new HashSet<SignData>();
            this.SignItems = new HashSet<SignItem>();
            this.SignLayouts = new HashSet<SignLayout>();
            this.Schedules = new HashSet<Schedule>();
            this.Land2BusMessages = new HashSet<Land2BusMessages>();
            this.Bus2LandMessageTypes = new HashSet<Bus2LandMessageTypes>();
            this.Land2BusMessageCategories = new HashSet<Land2BusMessageCategories>();
            this.Buses = new HashSet<Bus>();
            this.SignAddonItems = new HashSet<SignAddonItem>();
        }
    
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> DateAdded { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public string AnnouncementVoice { get; set; }
        public string ScheduleRef { get; set; }
    
        public virtual ICollection<CustomerAudioFile> CustomerAudioFiles { get; set; }
        public virtual ICollection<DataUpdateStatus> DataUpdateStatuses { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
        public virtual ICollection<SignContent> SignContents { get; set; }
        public virtual ICollection<SignData> SignDatas { get; set; }
        public virtual ICollection<SignItem> SignItems { get; set; }
        public virtual ICollection<SignLayout> SignLayouts { get; set; }
        public virtual ICollection<Schedule> Schedules { get; set; }
        public virtual ICollection<Land2BusMessages> Land2BusMessages { get; set; }
        public virtual ICollection<Bus2LandMessageTypes> Bus2LandMessageTypes { get; set; }
        public virtual ICollection<Land2BusMessageCategories> Land2BusMessageCategories { get; set; }
        public virtual ICollection<Bus> Buses { get; set; }
        public virtual ICollection<SignAddonItem> SignAddonItems { get; set; }
    }
}
