//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class App
    {
    	 
        public App()
        {
            this.Auths = new HashSet<Auth>();
        }
    
        public int ApiId { get; set; }
        public string ApiKey { get; set; }
        public string ApplicationName { get; set; }
    
        public virtual ICollection<Auth> Auths { get; set; }
    }
}
