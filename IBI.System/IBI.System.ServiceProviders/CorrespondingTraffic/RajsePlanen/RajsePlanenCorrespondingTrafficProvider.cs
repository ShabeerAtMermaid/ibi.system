﻿using IBI.Shared.Diagnostics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using IBI.DataAccess;
using IBI.DataAccess.DBModels;
using System.Xml.Xsl;
using System.IO;
using System.Xml.Linq;

namespace IBI.ServiceProviders.CorrespondingTraffic.RajsePlanen
{
    class RajsePlanenCorrespondingTrafficProvider : CorrespondingTrafficProvider
    {

        static XslCompiledTransform XsltTransform = new XslCompiledTransform(false);

        private static Int64 _counter;
        private static Int64 Counter
        {
            get
            {
                if (_counter != null && _counter >= 0)
                {
                    if (_counter == Int64.MaxValue - 1)
                        _counter = 0;

                    _counter += 1;
                }
                else
                {
                    _counter = 1;
                }


                return _counter;
            }
            set
            {
                _counter = value;
            }
        }

        public override string GetDepartureBoard(List<KeyValuePair<string, object>> parameters, ref string logEntry)
        {            
            if(Counter==1)
            { 
                XsltTransform.Load(typeof(XSLT_CT_Rejseplanen)); //picking XSLT from compiled DLL 
            }

            IFormatProvider provider = System.Globalization.CultureInfo.InvariantCulture;

            KeyValuePair<string, object> tmpBusNumber = parameters.Where(p=>p.Key=="BusNumber").FirstOrDefault();
            string busNumber = tmpBusNumber.Value == null ? "" : tmpBusNumber.Value.ToString();

            KeyValuePair<string, object> tmpStopNumber = parameters.Where(p => p.Key == "StopNumber").FirstOrDefault();
            string dbStopNumber = tmpStopNumber.Value == null ? "" : tmpStopNumber.Value.ToString();

            KeyValuePair<string, object> tmpBusLine = parameters.Where(p => p.Key == "BusLine").FirstOrDefault();
            string busline = tmpBusLine.Value == null ? "" : tmpBusLine.Value.ToString();

            KeyValuePair<string, object> tmpShowJourneys = parameters.Where(p => p.Key == "ShowJourneys").FirstOrDefault();
            int showJourneys= tmpShowJourneys.Value==null ? 0 : int.Parse(tmpShowJourneys.Value.ToString());

            KeyValuePair<string, object> tmpDateTime = parameters.Where(p => p.Key == "DateTime").FirstOrDefault();
            DateTime dateTime = tmpDateTime.Value == null ? DateTime.Now : DateTime.Parse(tmpDateTime.Value.ToString());
            //DateTime dateTime = tmpDateTime.Value == null ? DateTime.Now : DateTime.ParseExact(tmpDateTime.Value.ToString(), "dd.MM.yyyyTHH:mm", provider);
         

            //e.g date: 23.05.13
            //e.g time: 13:30
            //string dbStopNumber = stopNumber;
            string stopName = "";
            bool excludeLowPriorityMedia = false;
            int lastChanceToShow = 0;
            bool isCheckPoint = false;
            string stopNumber = dbStopNumber;
            string rejseplanenStopNumber = "";

            string result = "";

            try
            {

                //untransform Movia's StopNumber

                if (stopNumber.Length == 16)//it's a movia's transformed stop number
                {
                    stopNumber = rejseplanenStopNumber = Convert.ToInt32(stopNumber.Substring(7)).ToString();
                    
                }

                try
                {
                    logEntry += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> Checkpoint info from DB starts" + Environment.NewLine;

                    isCheckPoint = GetCheckPoint( dbStopNumber, ref stopName, ref lastChanceToShow, ref excludeLowPriorityMedia, ref rejseplanenStopNumber, ref logEntry);

                    logEntry += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> Checkpoint info from DB ends" + Environment.NewLine;
                }
                catch (Exception ex)
                {
                      logEntry += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " Error while getting Stop Info - StopNumber: " + dbStopNumber + "exception:" + ex.Message + Environment.NewLine;
                }

             
                WebClient webClient = new WebClient();
                ICredentials credentials = null;
                webClient.Credentials = new NetworkCredential(this.Username, this.Password);

                
                string date = dateTime.ToString("dd.MM.yy");
                string time = dateTime.ToString("HH:mm");

                string rUrl = String.Format(this.Url + "?id={0}&showJourneys={1}&date={2}&time={3}", rejseplanenStopNumber, showJourneys.ToString(), date, time);
                webClient.Encoding = System.Text.Encoding.UTF8;
              

                DateTime serverStartTime = DateTime.Now; 
                
                logEntry += String.Format(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " ----> [StopNumber: {0} , Departures: {1}], Rejseplanen call started.", stopNumber, showJourneys.ToString()) + Environment.NewLine;


                string xmlData = webClient.DownloadString(rUrl);
                 


                StringReader rd = new StringReader(xmlData);
                XDocument xDoc = XDocument.Load(rd);


                logEntry += String.Format(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " ----> [StopNumber: {0} , Departures: {1}], Rejseplanen call Completed in {2} seconds", stopNumber, showJourneys.ToString(), DateTime.Now.Subtract(serverStartTime).Seconds.ToString()) + Environment.NewLine;

                logEntry += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation starts" + Environment.NewLine;

              

                XsltArgumentList argsList = new XsltArgumentList();
                argsList.AddParam("FetchTime", "", DateTime.Now.ToString("dd.MM.yyyyTHH:mm"));
                argsList.AddParam("FetchFromTime", "", dateTime.ToString("dd.MM.yyyyTHH:mm"));
                argsList.AddParam("StopNumber", "", rejseplanenStopNumber);
                argsList.AddParam("TransformedStopNumber", "", stopNumber);
                argsList.AddParam("StopName", "", stopName);
                argsList.AddParam("CheckPoint", "", isCheckPoint.ToString().ToLower());
                argsList.AddParam("LastChanceToPlay", "", lastChanceToShow.ToString());
                argsList.AddParam("ExcludeLowPriorityMedia", "", excludeLowPriorityMedia.ToString().ToLower());

                //XslCompiledTransform transform = new XslCompiledTransform(true);
                //string xsltFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data\\CT.xslt");

                //transform.Load(xsltFilePath);

                //using (StringWriter writer = new StringWriter())
                //{
                //    transform.Transform(doc.CreateNavigator(), argsList, writer);

                //    result = writer.ToString();

                //    writer.Dispose();
                //}

                using (StringWriter writer = new StringWriter())
                {
                    XsltTransform.Transform(xDoc.CreateReader(), argsList, writer);
                    result = writer.ToString();
                }


                rd.Dispose();
                rd = null;                
                webClient.Dispose();
                webClient = null;
                xDoc = null;
                xmlData = string.Empty;


                logEntry += DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + " --> XSLT Transformation ends" + Environment.NewLine;
                           
            }
            catch (Exception ex)
            { 
                logEntry += ex.Message + "-->" + ex.StackTrace;
            }


            return result;
        }

       

        private static bool GetCheckPoint(string stopNumber, ref string stopName, ref int lastChanceeToShow, ref bool excludeLowPriorityMedia, ref string rejsePlanenStopNumber, ref string logEntry)
        {
          
            stopName = "";
            bool isCheckPoint = false;
            lastChanceeToShow = 30;
            excludeLowPriorityMedia = false;
            rejsePlanenStopNumber = Convert.ToInt32(stopNumber.Substring(7)).ToString();

            //if (true)
            //    return true;

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    decimal gId = decimal.Parse(stopNumber);

                    var stop = dbContext.GetStopCheckPointInformation(gId, rejsePlanenStopNumber).FirstOrDefault();
                    if (stop != null)
                    {
                        stopName = stop.StopName;
                        isCheckPoint = stop.IsCheckPoint;
                        lastChanceeToShow = stop.LastTimeToShow;
                        excludeLowPriorityMedia = stop.ExcludeLowPriorityMedia;
                        rejsePlanenStopNumber = stop.RejseplanenStopNumber;
                    }

                    //dbContext.Dispose();
                }


            }
            catch (Exception ex)
            {
                logEntry += "------> Exception while getting Checkpoint info for stop. " + ex.Message + Environment.NewLine;

            }


            return isCheckPoint;
        }



    }
}
