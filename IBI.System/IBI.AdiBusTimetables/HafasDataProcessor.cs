﻿using IBI.DataAccess.DBModels;
using IBI.Shared;
using IBI.Shared.ConfigSections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.AdiBusTimetables
{
    public class CorrectedStop
    {

        public int StopNumber { get; set; }
        public int MainStop { get; set; }
        public int Radius { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public int LaneManagement { get; set; }
        public int CorrespondanceStop { get; set; }
        public string StopName { get; set; }

        public static List<CorrectedStop> CacheCorrectedStopsData(string[] lines)
        {

            List<CorrectedStop> data = new List<CorrectedStop>();

            foreach (string line in lines)
            {
                CorrectedStop stopData = new CorrectedStop();
                stopData.StopNumber = int.Parse(line.Substring(0, 9).Trim());
                stopData.MainStop = int.Parse(line.Substring(11, 1).Trim());
                stopData.Radius = int.Parse(line.Substring(14, 4).Trim());

                //if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "da-DK")
                //{

                //    stopData.Longitude = decimal.Parse(line.Substring(20, 9).Trim().Replace('.', ','));
                //    stopData.Latitude = decimal.Parse(line.Substring(31, 9).Trim().Replace('.', ','));
                //}
                //else 
                { 
                    stopData.Longitude = line.Substring(20, 9).Trim();
                    stopData.Latitude = line.Substring(31, 9).Trim();
                }

                stopData.LaneManagement = int.Parse(line.Substring(42, 1).Trim());
                stopData.CorrespondanceStop = int.Parse(line.Substring(45, 1).Trim());
                stopData.StopName = line.Substring(50).Trim();

                data.Add(stopData);
                 
                //Console.WriteLine("Corrected Long:" + stopData.Longitude);
                
                ////if(stopData.StopName == "Skanderborg St.")
                //{
                //    Console.WriteLine(string.Format("StopName: {0}, Latitude: {1}, Longitude: {2}", stopData.StopName,line.Substring(31, 9).Trim(), line.Substring(20, 9).Trim()));
                //}
            }

            return data;
        }
    }


    public partial class HafasDataProcessor
    {

        #region Variables
        StringBuilder sb = new StringBuilder();
        Log ParentLog { get; set; }
        Log CustomerLog { get; set; }



        private string CustomerLogFileName { get; set; }
        private string ParentLogFileName { get; set; }

        public string GetCustomerLogFileName(int counter, int customerid)
        {
            return string.Format("Timetables_{0}_{1}", counter, customerid);
        }

        private int Counter { get; set; }
        /*
        public string GetLogFileName(int counter, int customerid)
        {
            return string.Format("Timetables_{0}_{1}", counter, customerid);
        }
       */
        Hashtable hexaToBinary;

        List<Journey> Journeys { get; set; }
        List<KeyValuePair<string, string>> StopNamesCache { get; set; } //value: stopname
        List<KeyValuePair<string, string>> StopCoordCache { get; set; } //value: lat|lon
        List<CorrectedStop> CorrectedStopsCache { get; set; } //value: lat|lon

        List<String> Lines { get; set; }

        //public int CustomerId { get; set; }

        public string TargetDirectory { get; set; }
        public string FPlanFilepath { get; set; }
        public string StopCoordFilepath { get; set; }
        public string StopNamesFilepath { get; set; }
        public string OperationDaysFilepath { get; set; }
        public string JourneyDurationFilepath { get; set; }
        public string CorrectedStopsFilepath { get; set; }

        string[] FPlanData { get; set; }
        string[] StopNamesData { get; set; }
        string[] StopCoordData { get; set; }
        string[] JourneyDurationData { get; set; }
        string[] OperationDaysData { get; set; }
        string[] CorrectedStopsData { get; set; }


        VdvSubscriptionConfiguration Config { get; set; }
        #endregion



        public HafasDataProcessor(string targetDirectory, VdvSubscriptionConfiguration config, string parentLogFileName, int counter)
        {
            this.ParentLogFileName = parentLogFileName;
            this.Counter = counter;

            ParentLog = new Log(parentLogFileName);
            ParentLog.WriteLog(string.Format("Hafas Data Processor initiated to read data from files under [{0}]", targetDirectory));

            this.Config = config;
            this.TargetDirectory = targetDirectory;

            ////Clear existing Cache elements
            //IBI.Cache.JourneyCache.ClearZonesCache();
            //IBI.Cache.JourneyCache.ClearSchedulesCache();
            //IBI.Cache.JourneyCache.ClearRouteCache();

            //////Fill cache elements from DB
            //IBI.Cache.JourneyCache.FillRouteDirections();
            //IBI.Cache.JourneyCache.FillSchedules();


            Journeys = new List<Journey>();
            StopNamesCache = new List<KeyValuePair<string, string>>();
            StopCoordCache = new List<KeyValuePair<string, string>>();
            CorrectedStopsCache = new List<CorrectedStop>();

            Lines = new List<String>();



        }



        public void DoProcess()
        {

            if (Directory.Exists(TargetDirectory))
            {

                FPlanFilepath = System.IO.Path.Combine(TargetDirectory, "fplan");
                StopCoordFilepath = System.IO.Path.Combine(TargetDirectory, "bfkoord");
                StopNamesFilepath = System.IO.Path.Combine(TargetDirectory, "bahnhof");
                OperationDaysFilepath = System.IO.Path.Combine(TargetDirectory, "bitfeld");
                JourneyDurationFilepath = System.IO.Path.Combine(TargetDirectory, "eckdaten");
                CorrectedStopsFilepath = System.IO.Path.Combine(TargetDirectory, "correctedstops");
            }

            ProcessFPlanFile();
        }


        #region Hexa2Binary Functions

        public string HexToBinary(string hexValue)
        {

            ulong number = UInt64.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
            return Convert.ToString((int)number, 2);
        }

        private void initializeHexToBinary()
        {
            hexaToBinary = new Hashtable();
            hexaToBinary["0"] = HexToBinary("0");
            hexaToBinary["1"] = HexToBinary("1");
            hexaToBinary["2"] = HexToBinary("2");
            hexaToBinary["3"] = HexToBinary("3");
            hexaToBinary["4"] = HexToBinary("4");
            hexaToBinary["5"] = HexToBinary("5");
            hexaToBinary["6"] = HexToBinary("6");
            hexaToBinary["7"] = HexToBinary("7");
            hexaToBinary["8"] = HexToBinary("8");
            hexaToBinary["9"] = HexToBinary("9");
            hexaToBinary["A"] = HexToBinary("A");
            hexaToBinary["B"] = HexToBinary("B");
            hexaToBinary["C"] = HexToBinary("C");
            hexaToBinary["D"] = HexToBinary("D");
            hexaToBinary["E"] = HexToBinary("E");
            hexaToBinary["F"] = HexToBinary("F");
        }


        #endregion


        private void ProcessFPlanFile()
        {
            //ParentLog.WriteLog("Extracting data from fplan file");

            StringBuilder log = new StringBuilder();

            if (String.IsNullOrEmpty(FPlanFilepath) || !File.Exists(FPlanFilepath))
            {
                throw new Exception("Please provide future plan path");
            }
            if (String.IsNullOrEmpty(StopNamesFilepath) || !File.Exists(StopNamesFilepath))
            {
                throw new Exception("Please provide stop name file path");
            }
            if (String.IsNullOrEmpty(StopCoordFilepath) || !File.Exists(StopCoordFilepath))
            {
                throw new Exception("Please provide stop coord file path");
            }
            if (String.IsNullOrEmpty(OperationDaysFilepath) || !File.Exists(OperationDaysFilepath))
            {
                throw new Exception("Please provide operationdays file path");
            }
            if (String.IsNullOrEmpty(JourneyDurationFilepath) || !File.Exists(JourneyDurationFilepath))
            {
                throw new Exception("Please provide journey duration file path");
            }
            if (String.IsNullOrEmpty(CorrectedStopsFilepath) || !File.Exists(CorrectedStopsFilepath))
            {
                throw new Exception("Please provide correctedstops file path");
            }



            ////Clear existing Cache elements
            //IBI.Cache.JourneyCache.ClearZonesCache();
            //IBI.Cache.JourneyCache.ClearSchedulesCache();
            //IBI.Cache.JourneyCache.ClearRouteCache();

            //////Fill cache elements from DB
            //IBI.Cache.JourneyCache.FillRouteDirections(this.CustomerId);
            //IBI.Cache.JourneyCache.FillSchedules(this.CustomerId);


            FPlanData = File.ReadAllLines(FPlanFilepath, Encoding.Default);

            List<String> lstplan = FPlanData.ToList();
            lstplan.Add("*Z");
            FPlanData = lstplan.ToArray();

            ParentLog.WriteLog("Reading & caching data from HAFAS files");

            StopNamesData = File.ReadAllLines(StopNamesFilepath, Encoding.Default);
            StopCoordData = File.ReadAllLines(StopCoordFilepath, Encoding.Default);
            JourneyDurationData = File.ReadAllLines(JourneyDurationFilepath, Encoding.Default);
            OperationDaysData = File.ReadAllLines(OperationDaysFilepath, Encoding.Default);
            CorrectedStopsData = File.ReadAllLines(CorrectedStopsFilepath, Encoding.Default);

            CorrectedStopsCache = CorrectedStop.CacheCorrectedStopsData(CorrectedStopsData);

            CultureInfo provider = CultureInfo.InvariantCulture;

            initializeHexToBinary();


            bool journeyEnds = false;
            string operationDaysReference = string.Empty;
            DateTime journeyDurationStartTime = DateTime.ParseExact(JourneyDurationData[2].Split(' ')[0], "dd.MM.yyyy", provider);
            DateTime journeyDurationEndTime = DateTime.ParseExact(JourneyDurationData[3].Split(' ')[0], "dd.MM.yyyy", provider);

            string journeyStartTime = string.Empty;
            string journeyEndTime = string.Empty;

            ArrayList journeyOptions = new ArrayList();
            //List<JourneyStop> journeyStops = new List<JourneyStop>();// new ArrayList();
            ArrayList journeyStops = new ArrayList();

            Lines.Clear();

            string jLine = "";

            int stopSequence = 1;


            ParentLog.WriteLog("Extracting journeys from fplan files");

            try
            {
                string currentTripId = string.Empty;


                foreach (string line in FPlanData)
                {


                    string lineCode = line.Length > 0 ? line.Substring(0, 2) : "";

                    if (lineCode == "")
                        continue;


                    switch (lineCode)
                    {
                        case "*G":
                        //TODO
                        case "*R":
                            //TODO
                            break;
                        case "*F":
                            //TODO - check if it is valid line
                            break;
                        case "*I":
                            //TODO
                            break;
                        case "*C":
                            //TODO
                            break;
                        case "*A":
                            journeyOptions.Add(line);
                            break;
                        case "*L":
                            string[] lineJourneyElements = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            jLine = lineJourneyElements[1];

                            if (String.IsNullOrEmpty(Lines.Find(l => l == jLine)))
                                Lines.Add(jLine);

                            break;
                        case "*Z":
                            if (journeyEnds)
                            {
                                // journey ends
                                foreach (string optionLine in journeyOptions)
                                {
                                    string[] lineElements = optionLine.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                    operationDaysReference = lineElements[4];
                                    bool bAlldays = operationDaysReference.Equals("000000");
                                    string bitValues = string.Empty; //365 char string to hold indexes of days
                                    if (!bAlldays)
                                    {
                                        var opDaysLine = Array.Find(OperationDaysData, s => s.StartsWith(operationDaysReference));
                                        if (opDaysLine != null)
                                        {
                                            // 3E7CF8000000000000300000000000000000000000000000000000000000000000000000000000000000000000000
                                            string[] opDataColumns = opDaysLine.ToString().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                            string daysHexValue = opDataColumns[1];//.Substring(4);

                                            //DateTime journeyStartDateTime = DateTime.Parse(journeyStartTime, "dd.mm.yyyy");
                                            
                                            //for (int i = 0; i < daysHexValue.Length; i++)
                                            //{
                                            //    bitValues += hexaToBinary[daysHexValue.Substring(i, 1)];
                                            //    if (bitValues.Length >= (journeyDurationEndTime - journeyDurationStartTime).TotalDays)
                                            //    {
                                            //        break;
                                            //    }
                                            //}

                                            bitValues = String.Join(String.Empty,
                                              daysHexValue.Select(
                                                c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')
                                              )
                                            );

                                            //we need to discard first 2 and last 2 digits (according to hafas documentation) 
                                            bitValues = bitValues.Length>5 ? bitValues.Substring(2, bitValues.Length - 2) : "0"; 
                                            
                                        }
                                    }

                                    journeyStartTime = lineElements[5].Substring(1);
                                    journeyEndTime = lineElements[6].Substring(1);
                                    int jDays2AddStart = 0;
                                    int jDays2AddEnd = 0;

                                    journeyStartTime = Adjust24HourTime(journeyStartTime, ref jDays2AddStart);
                                    journeyEndTime = Adjust24HourTime(journeyEndTime, ref jDays2AddEnd);

                                    // enter journey into DB/List/Grid
                                    int dateIndex = 0;
                                    DateTime journeyDate = journeyDurationStartTime;
                                    //for (DateTime dtStart = journeyDurationStartTime; dateIndex < (journeyDurationEndTime - journeyDurationStartTime).TotalDays; dateIndex++)
                                    for (DateTime dtStart = journeyDurationStartTime; dateIndex < bitValues.Length; dateIndex++)
                                    {
                                        if (bAlldays || bitValues[dateIndex] == '1')
                                        {


                                            Journey journey = new Journey();

                                            journey.JourneyDate = journeyDurationStartTime.AddDays(dateIndex);

                                            string jStartPoint = GetStopName(lineElements[2]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[2]).Split(' ')[2]; 
                                            string jDestination = GetStopName(lineElements[3]); //StopNamesData.First(s => s.Split(' ')[0] == lineElements[3]).Split(' ')[2];

                                            //Console.WriteLine(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyStartTime);
                                            //22-10-2015 00:00:00 0505
                                            DateTime jPlannedDeparture = String.IsNullOrEmpty(journeyStartTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyStartTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);
                                            DateTime jPlannedArrival = String.IsNullOrEmpty(journeyEndTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + journeyEndTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);

                                            if (jPlannedDeparture != DateTime.MinValue)
                                            {
                                                jPlannedDeparture = jPlannedDeparture.AddDays(jDays2AddStart);
                                            }

                                            if (jPlannedArrival != DateTime.MinValue)
                                            {
                                                jPlannedArrival = jPlannedArrival.AddDays(jDays2AddEnd);
                                            }

                                            //DateTime jPlannedDeparture = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " +  journeyStartTime, "MM/dd/yyyy HHmm", provider);
                                            //DateTime jPlannedArrival = DateTime.ParseExact(journey.JourneyDate.ToString("MM/dd/yyyy") + " " + journeyEndTime, "MM/dd/yyyy HHmm", provider);



                                            journey.BusNumber = "0";
                                            journey.CustomerId = 0;
                                            journey.JourneyNumber = 0;
                                            journey.StartTime = null;
                                            journey.EndTime = null;

                                            journey.StartPoint = jStartPoint;
                                            journey.Line = jLine;
                                            journey.Destination = jDestination;
                                            journey.PlannedDepartureTime = jPlannedDeparture;
                                            journey.PlannedArrivalTime = jPlannedArrival;
                                            journey.ScheduleNumber = 0;

                                            journey.TripId = currentTripId; //line.Substring(3, 6).Trim();
                                            

                                            //Add Stops to journey
                                            journey.JourneyStops.AddRange(journeyStops);


                                            if (journey.PlannedDepartureTime > DateTime.Now && journey.PlannedDepartureTime <= DateTime.Now.AddDays(Config.FutureDaysBuffer))
                                            {
                                                this.Journeys.Add(journey);
                                            }

                                        }
                                        dtStart = dtStart.AddDays(1);

                                    }
                                }
                                //TODO
                                // initialize journey variables
                                journeyStartTime = string.Empty;
                                journeyEndTime = string.Empty;
                                journeyOptions.Clear();
                                journeyStops.Clear();
                                
                            }
                            else
                            {
                                journeyEnds = true;                              

                            }

                            if (line.Length >= 10)
                            {
                                currentTripId = line.Substring(3, 6).Trim();
                            }


                            break;
                        default:
                            {
                                if (line.StartsWith("*"))
                                    continue;


                                // hop line
                                string stopNumber = line.Substring(0, 9); // stop length can be less than 9 - check it based on setting(PDF)

                                string stopName = GetStopName(stopNumber); //StopNamesData.First(s => s.Split(' ')[0] == stopNumber).Split(' ')[2];
                                string arrivalTime = line.Substring(32, 5).Substring(1).Trim();
                                string departureTime = line.Substring(39, 5).Substring(1).Trim();
                                int day2addArrival = 0;
                                int day2addDeparture = 0;

                                arrivalTime = Adjust24HourTime(arrivalTime, ref day2addArrival);
                                departureTime = Adjust24HourTime(departureTime, ref day2addDeparture);

                                string coord;
                                string[] arrCoord;
                                string lon;
                                string lat;
                                string zone = "0";
                                string isCheckpoint = "false";

                                CorrectedStop correctedStop = CorrectedStopsCache.Where(s => s.StopNumber == int.Parse(stopNumber)).FirstOrDefault();
                                if (correctedStop != null)
                                {

                                    lon = correctedStop.Longitude;  //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                                    lat = correctedStop.Latitude;   //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];                                
                                    isCheckpoint = correctedStop.MainStop == 1 ? "true" : "false";

                                    
                                }
                                else
                                {

                                    coord = GetStopCoord(stopNumber);
                                    arrCoord = coord.Split('|');
                                    lon = arrCoord.Length > 0 ? arrCoord[0] : "";  //StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1];
                                    lat = arrCoord.Length > 1 ? arrCoord[1] : "";//StopCoordData.First(s => s.Split(' ')[0] == stopNumber).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[2];                                                                

                                    //if (System.Threading.Thread.CurrentThread.CurrentCulture.Name == "da-DK")
                                    //{ 
                                    //    lon = lon.Replace('.', ',');
                                    //    lat = lat.Replace('.', ',');

                                    //    if (lon.StartsWith("9"))
                                    //    {
                                    //        Console.WriteLine("Long:" + lon);
                                    //    }
                                    //}
                                }

                                journeyStops.Add(new StopHop(stopNumber, stopName, arrivalTime, departureTime, lat, lon, zone, isCheckpoint, day2addArrival, day2addDeparture));

                                break;
                            }
                    }//end swtich


                }//end foreach
            }
            catch (Exception ex)
            {
               // Console.WriteLine("Problem parsing fPlan data lines. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                ParentLog.WriteLog("Problem parsing fPlan data lines. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
            }
            finally
            {
                ParentLog.WriteLog(string.Format("Total {0} journeys found for now onwards to next {1} days", Journeys.Count(), Config.FutureDaysBuffer));
                SaveJourneys2DB();
            }



        }

                              
        private string GetStopName(string stopNumber)
        {
            stopNumber = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopNumber, Shared.ScheduleHelper.StopPrefix.HAFAS);

            //search in cache first
            var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
            string stopName = "";

            if (!String.IsNullOrEmpty(stop.Value))
            {
                stopName = stop.Value;
            }
            else
            {
                //var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
                
                CorrectedStop correctedStop = CorrectedStopsCache.Where(s => s.StopNumber == int.Parse(stopNumber)).FirstOrDefault();
                if (correctedStop != null)  
                    stopName = correctedStop.StopName;

                if (String.IsNullOrEmpty(stopName)){
                    stopName = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
                    if(stopName!=null)
                        stopName =stopName.Substring(14);
                }

                if (stopName != null)
                {
                    //stopName = sstop.Split(' ')[2];
                    
                    int endIndex = stopName.IndexOf('(');
                    if (endIndex > 0)
                        stopName = stopName.Substring(0, endIndex).Trim();

                    int dolloarIndex = stopName.IndexOf('$');
                    if (dolloarIndex > 0)
                        stopName = stopName.Substring(0, dolloarIndex).Trim();

                    StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
                }
                else
                {
                    stopName = "";
                }

            }

            return stopName;
        }

        //private string GetStopName(string stopNumber)
        //{
        //    stopNumber = IBI.Shared.ScheduleHelper.UnTransformStopNo(stopNumber, Shared.ScheduleHelper.StopPrefix.HAFAS);

        //    //search in cache first
        //    var stop = StopNamesCache.FirstOrDefault(s => s.Key == stopNumber);
        //    string stopName = "";

        //    if (!String.IsNullOrEmpty(stop.Value))
        //    {
        //        stopName = stop.Value;
        //    }
        //    else
        //    {
        //        var sstop = StopNamesData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();

        //        if (sstop != null)
        //        {
        //            //stopName = sstop.Split(' ')[2];
        //            stopName = sstop.Substring(14);

        //            int endIndex = stopName.IndexOf('(');
        //            if (endIndex > 0)
        //                stopName = stopName.Substring(0, endIndex).Trim();

        //            int dolloarIndex = stopName.IndexOf('$');
        //            if (dolloarIndex > 0)
        //                stopName = stopName.Substring(0, dolloarIndex).Trim();

        //            StopNamesCache.Add(new KeyValuePair<string, string>(stopNumber, stopName));
        //        }
        //        else
        //        {
        //            stopName = "";
        //        }

        //    }

        //    return stopName;
        //}

        private string GetStopCoord(string stopNumber)
        {
            //search in cache first
            var stop = StopCoordCache.FirstOrDefault(s => s.Key == stopNumber);
            string stopCoord = "|";
            if (!String.IsNullOrEmpty(stop.Value))
            {
                stopCoord = stop.Value;
            }
            else
            {
                stopCoord = StopCoordData.Where(s => s.Split(' ')[0] == stopNumber).FirstOrDefault();
                if (!String.IsNullOrEmpty(stopCoord))
                {
                    string[] arrCoord = stopCoord.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    string lat = arrCoord.Length > 1 ? arrCoord[1] : "";
                    string lon = arrCoord.Length > 2 ? arrCoord[2] : "";

                    stopCoord = lat + "|" + lon;
                    StopCoordCache.Add(new KeyValuePair<string, string>(stopNumber, stopCoord));
                }
                else
                {
                    stopCoord = "|";
                }

            }


            return stopCoord;

        }

        private string Adjust24HourTime(string time, ref int day2Add)
        {

            if (!String.IsNullOrEmpty(time))
            {
                int t = 0;
                bool tResult = int.TryParse(time, out t);

                if (tResult)
                {
                    if (t >= 2400)
                    {
                        day2Add = 1;
                        time = (t - 2400).ToString();
                        time = time.Length < 4 ? "00000" + time : time;
                        time = time.Substring(time.Length - 4);

                    }
                }
            }

            return time;
        }

        private bool SaveJourneys2DB()
        {

            StringBuilder log = new StringBuilder();
            CultureInfo provider = CultureInfo.InvariantCulture;
            int index = 0;

            ParentLog.WriteLog("Process of saving journeys into IBI System started ... ");

            try
            {
                using (DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
                {

                    //global list of All stops from database
                    var allDBStops = dbContext.Stops.Where(s => s.StopSource == "HAFAS").ToList();

                    int jCounter = 1;
                    foreach (Journey journey in Journeys)
                    {

                        bool doSave = false;
                        int customerId = 0;



                        foreach (VdvCustomerConfiguration customerConfig in Config.Customers)
                        {
                            //----------------
                            if ( (customerConfig.LineFilter.Count()==0 || customerConfig.LineFilter.Any(l => l.ToString() == journey.Line))  && customerConfig.SaveDataInDB == true)
                            {
                                doSave = true;
                                customerId = customerConfig.CustomerId;
                                break;
                            }
                            //----------------

                        }

                        if (!doSave)
                        {
                            //jCounter++;
                            continue;
                        }

                        StringBuilder sbJourney = new StringBuilder();
                        StringBuilder sbStops = new StringBuilder();


                        //Log custLog = new Log(GetCustomerLogFileName(Counter, customerId), sbJourney);

                        VdvCustomerConfiguration custConfig = Config.Customers.Where(c => c.CustomerId == customerId).FirstOrDefault();

                        string externalRef = String.Format("{0}-{1}-{2}", Config.ExternalReferencePrefix, journey.JourneyDate.ToString("yyyyMMdd"), journey.TripId); //logic of external ref from MAR

                        sbJourney.AppendLine(String.Format("## Journey # {0} ##", jCounter.ToString()));

                        bool exists = dbContext.Journeys.Where(j => j.ExternalReference == externalRef).Count() > 0;

                        //----------------
                        if (exists)
                        {
                            sbJourney.AppendLine(String.Format("## Trip # {0} already exists / imported to Journeys table ##\n", journey.TripId));
                            jCounter++;
                            continue;
                        }
                        //----------------



                        IBI.DataAccess.DBModels.Journey dbJourney = new DataAccess.DBModels.Journey();


                        int stopSequence = 1;
                        List<JourneyStop> jStops = new List<JourneyStop>();

                        foreach (StopHop stop in journey.JourneyStops)
                        {
                            JourneyStop jStop = new JourneyStop();
                            jStop.Timestamp = DateTime.Now;

                            decimal ibiStopNumber = IBI.Shared.AppUtility.IsNullDec(ScheduleHelper.TransformStopNo(stop.StopNumber, ScheduleHelper.StopPrefix.HAFAS));

                           
                            //fix for DK Server
                            //if (!stop.Latitude.Contains('.'))
                            //{
                            //    stop.Latitude = stop.Latitude.Insert(2, ".");
                            //    stop.Longitude = stop.Longitude.Insert(2, ".");
                            //}


                            bool getLiveZone = bool.Parse(ConfigurationManager.AppSettings["GetLiveZonesForStop"].ToString());

                            if (getLiveZone && stop.Zone == "0" && String.IsNullOrEmpty(stop.StopName) == false)
                            {
                                stop.Zone = IBI.Cache.JourneyCache.GetZoneName(stop.Latitude, stop.Longitude, true, dbContext);
                            }

                            if (stop.Zone == "0") {
                                stop.Zone = "";
                            }

                            //Console.Write(stop.Latitude + ", " + stop.Longitude);

                            //var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude.Replace(',', '.'), stop.Latitude.Replace(',', '.'));
                            var sqlPoint = string.Format("POINT({0} {1})", stop.Longitude, stop.Latitude);
                            jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                            jStop.StopId = ibiStopNumber;
                            jStop.StopName = stop.StopName;
                            jStop.StopSequence = stopSequence++;

                            DateTime plannedDepartureTime = String.IsNullOrEmpty(stop.DepartureTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.DepartureTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);
                            DateTime plannedArrivalTime = String.IsNullOrEmpty(stop.ArrivalTime) ? DateTime.MinValue : DateTime.ParseExact(journey.JourneyDate.ToString("MMddyyyy") + " " + stop.ArrivalTime, "MMddyyyy HHmm", CultureInfo.InvariantCulture);

                            if (plannedDepartureTime != DateTime.MinValue)
                            {
                                jStop.PlannedDepartureTime = plannedDepartureTime.AddDays(stop.Day2addDeparture);
                            }

                            if (plannedArrivalTime != DateTime.MinValue)
                            {
                                jStop.PlannedArrivalTime = plannedArrivalTime.AddDays(stop.Day2addArrival);
                            }

                            jStop.Zone = stop.Zone;
                            jStop.IsOnline = false;
                            jStop.Timestamp = DateTime.Now;

                            //Console.WriteLine(plannedArrivalTime.ToString("yyyy-MM-ddTHH:mm") + " || " +plannedDepartureTime.ToString("yyyy-MM-ddTHH:mm"));

                            ////SYNC STOPS *******************************************************************
                            bool syncStops = bool.Parse(ConfigurationManager.AppSettings["SyncStops"]);

                            if (syncStops)
                            {
                                try
                                {
                                    var st = allDBStops.Where(s => s.GID == ibiStopNumber).FirstOrDefault();
                                    if (st == null)
                                    {
                                        DataAccess.DBModels.Stop newStop = new DataAccess.DBModels.Stop()
                                        {
                                            StopName = jStop.StopName,
                                            GID = jStop.StopId,
                                            StopGPS = jStop.StopGPS,
                                            StopSource = "HAFAS",
                                            OriginalCreated = DateTime.Now,
                                            OriginalName = jStop.StopName,
                                            OriginalGPS = jStop.StopGPS,
                                            LastUpdated = DateTime.Now
                                        };

                                        dbContext.Stops.Add(newStop);
                                        allDBStops.Add(newStop);//added to global list for better next comparison.
                                        dbContext.SaveChanges();
                                    }
                                    else
                                    {
                                        if(st.StopName != jStop.StopName)
                                        {
                                        //update
                                            st.StopName = jStop.StopName;
                                            st.StopGPS = jStop.StopGPS;
                                            st.StopSource = "HAFAS";
                                            st.LastUpdated = DateTime.Now;

                                            dbContext.SaveChanges();
                                        }
                                    }


                                    jStops.Add(jStop);
                                    sbStops.AppendLine(string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5} GPS: {6} {7}",
                                                jStop.StopId, jStop.StopName, stopSequence, jStop.Zone, jStop.PlannedDepartureTime.ToString(), jStop.PlannedArrivalTime.ToString(), jStop.StopGPS.Latitude.ToString(), jStop.StopGPS.Longitude.ToString()));

                                }
                                catch (Exception exStops)
                                {
                                    //throw new Exception(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
                                    sbStops.AppendLine(String.Format("Failed to save Stop in Database: + StopNumber: {0}, StopName: {1}", stop.StopNumber, stop.StopName));
                                }
                            }
                            ////*********************************************************************************************

                        }// end stops loop

                        string destinationAfterRewrite = journey.Destination;

                        int scheduleId = 0;
                        if (journey.ScheduleNumber == 0)
                        {
                            //dbJourney.ScheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(CustomerId, journey.Line, journey.StartPoint, ref destinationAfterRewrite, journey.ViaName, dbJourney.JourneyStops, ref log, true);
                            scheduleId = IBI.Cache.JourneyCache.GetScheduleNumber(custConfig, 0, customerId, journey.Line, journey.StartPoint, journey.Destination, journey.ViaName, jStops, ref sbJourney, ref destinationAfterRewrite, true);
                        }

                        ////existing future journey CHECK ***********************
                        bool updateExistingJourneys = bool.Parse(ConfigurationManager.AppSettings["UpdateExistingJourneys"]);
                        if (updateExistingJourneys)
                        {
                            decimal firstStopId = jStops.FirstOrDefault().StopId;
                            var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, journey.Line, destinationAfterRewrite, journey.PlannedDepartureTime, firstStopId).FirstOrDefault();

                            if (existingJourney != null)
                            {
                                dbJourney = existingJourney;
                                dbJourney.JourneyStops.Clear();
                            }
                        }
                        ////*******************************************************

                        dbJourney.JourneyStops = jStops;
                        dbJourney.ScheduleId = scheduleId;
                        dbJourney.BusNumber = journey.BusNumber;
                        dbJourney.CustomerId = customerId;
                        dbJourney.JourneyStateId = 0;
                        dbJourney.PlannedEndTime = journey.PlannedArrivalTime;
                        dbJourney.PlannedStartTime = journey.PlannedDepartureTime;
                        dbJourney.LastUpdated = DateTime.Now;
                        dbJourney.ExternalReference = externalRef;


                        sbJourney.AppendLine(String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, RewriteDestination: {8}, Total Stops: {9}",
                                       (dbJourney.JourneyId > 0 ? "UPDATED" : "INSERTED"), dbJourney.JourneyId, dbJourney.CustomerId, dbJourney.ExternalReference, dbJourney.ScheduleId, journey.Line, journey.StartPoint, journey.Destination, destinationAfterRewrite, jStops.Count()));
                        sbJourney.AppendLine(sbStops.ToString());

                        if (dbJourney.JourneyId == 0) 
                        { 
                            dbContext.Journeys.Add(dbJourney);
                        }

                        try
                        {
                            //Save new Journey to DB                    
                            dbContext.SaveChanges();
                            sbJourney.AppendLine(string.Format("Journey {0} saved successfully to DB", dbJourney.JourneyId));

                        }
                        catch (Exception exJourney)
                        {
                            sbJourney.AppendLine(string.Format("ERROR !!! Failed to save Journey [{0}]", IBI.Shared.Diagnostics.Logger.GetDetailedError(exJourney)));
                        }


                        Log custLog = new Log(GetCustomerLogFileName(Counter, customerId), sbJourney);
                        custLog.WriteLog(sbJourney.ToString());

                        custLog = null;

                        sbJourney.Clear();
                        sbStops.Clear();

                        System.Threading.Thread.Sleep(20);
                    }

                    ParentLog.WriteLog("All journeys successfully imported to the database");
                    //MessageBox.Show("All journeys successfully imported to the database");
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                //Console.WriteLine("Problem Saving Journeys to Database. [" + IBI.Shared.Diagnostics.Logger.GetDetailedError(ex) + "]");
                ParentLog.WriteLog(ex);

            }


            return true;
        }


        #region Private Helper



        #endregion

    }

}
