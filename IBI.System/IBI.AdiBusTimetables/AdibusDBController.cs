using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.DataAccess.DBModels;
using System.Data.Entity.Spatial;
using System.Configuration;
using IBI.Shared.ConfigSections;
using System.Collections.Generic;
using System;
using System.IO;
using IBI.Implementation.Schedular;

namespace IBI.AdiBusTimetables
{

    public class AdibusDBController
    {

        //public HaConCustomerConfiguration HCustConfig { get; set; }

        private static string GetJourneyLogFileName(int customerId)
        {
            return string.Format("VDV_Journeys_{0}", customerId.ToString());
        }

        private static string GetSubscriptionLogFileName()
        {
            return string.Format("VDV_{0}", "Adibus");
        }

        private static List<KeyValuePair<string, string>> _RouteCache = null;
        private static List<KeyValuePair<string, string>> _ScheduleCache = null;
        private static List<KeyValuePair<string, string>> _ZoneCache = null;


        static List<KeyValuePair<string, string>> RouteCache
        {
            get
            {
                if (_RouteCache == null)
                    _RouteCache = new List<KeyValuePair<string, string>>();

                return _RouteCache;
            }
            set
            {
                _RouteCache = value;
            }  //value: scheduleId|stopSequenceMD5
        }

        static List<KeyValuePair<string, string>> ScheduleCache
        {
            get
            {
                if (_ScheduleCache == null)
                    _ScheduleCache = new List<KeyValuePair<string, string>>();

                return _ScheduleCache;
            }
            set
            {
                _ScheduleCache = value;
            }  //value: scheduleId|stopSequenceMD5
        }

        static List<KeyValuePair<string, string>> ZoneCache
        {
            get
            {
                if (_ZoneCache == null)
                    _ZoneCache = new List<KeyValuePair<string, string>>();

                return _ZoneCache;
            }
            set
            {
                _ZoneCache = value;
            }
        }

        public static string GetZoneName(string lat, string lon, bool genearateZoneInDB)
        {
            //search in cache first
            string zoneName = "";
            string key = lat + "," + lon;

            var zone = ZoneCache.FirstOrDefault(s => s.Key == key);


            if (!String.IsNullOrEmpty(zone.Value))
            {
                zoneName = zone.Value;
            }
            else
            {
                if (genearateZoneInDB)
                {
                    try
                    {
                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            var dbZone = IBI.REST.Schedule.ScheduleServiceController.GetZone(lat, lon);

                            if (dbZone != null)
                            {
                                zoneName = dbZone.Zone;
                                ZoneCache.Add(new KeyValuePair<string, string>(key, zoneName));
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        zoneName = string.Empty;
                    }
                }

                else
                {
                    zoneName = "0";
                }
            }

            return zoneName;
        }

        public static void FillRouteDirections()
        {
            RouteCache = new List<KeyValuePair<string, string>>();
            RouteCache.Clear();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                //string custId = customerID.ToString();
                var routes = dbContext.RouteDirections.AsQueryable<RouteDirection>(); //Where(r => r.Name.StartsWith(customerId.ToString()))
                foreach (var route in routes)
                {
                    string key = route.RouteDirectionId.ToString();
                    string value = route.Name;
                    RouteCache.Add(new KeyValuePair<string, string>(key, value));
                }

            }
        }

        public static void FillSchedules()
        {
            ScheduleCache = new List<KeyValuePair<string, string>>();
            ScheduleCache.Clear();

            using (IBIDataModel dbContext = new IBIDataModel())
            {
                //int custId = Convert.ToInt32(CustomerID);
                var schedules = dbContext.Schedules.AsQueryable<Schedule>(); //.Where(s => s.CustomerId == customerID);
                foreach (var schedule in schedules)
                {
                    string key = schedule.CustomerId.ToString() + "|" + schedule.Line + "|" + schedule.FromName + "|" + schedule.Destination + "|" + schedule.ViaName;

                    string scheduleStopSequence = string.Empty;

                    if (schedule.ScheduleStops.Count() > 0)
                    {
                        scheduleStopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(IBI.Shared.AppUtility.ListToSingleLineText(schedule.ScheduleStops.Select(js => js.StopId.ToString()).ToList()));
                    }
                    string value = schedule.ScheduleId.ToString() + "|" + scheduleStopSequence;
                    ScheduleCache.Add(new KeyValuePair<string, string>(key, value));
                }

            }
        }

        public static void ClearSchedulesCache()
        {
            ScheduleCache.Clear();
            ScheduleCache = null;
        }

        public static void ClearZonesCache()
        {
            ZoneCache.Clear();
            ZoneCache = null;
        }

        public static void ClearRouteCache()
        {
            RouteCache.Clear();
            RouteCache = null;
        }

        public static int GetScheduleNumber(VdvCustomerConfiguration hCustConfig, Int64 subId, int customerId, string line, string fromName, string destination, string viaName, ICollection<JourneyStop> journeyStops, ref StringBuilder log, ref string destinationAfterRewrite, bool generateScheduleInDB = true)
        {
            int scheduleId = 0;
            string scheduleStopSequence = string.Empty;
            string journeyStopSequence = string.Empty;

            /****/

            foreach (var vdvSchedule in hCustConfig.ScheduleRewriteDefinitions.Schedules.Where(s => s.Line == line && s.Destination == destination))
            {
                decimal sId = 0;
                bool isMissing = true;

                foreach (string stopId in vdvSchedule.MissingStops)
                {
                    sId = decimal.Parse(stopId);
                    if (journeyStops.Where(js => js.StopId == sId).Count() > 0)
                    {
                        isMissing = false;
                        break;
                    }
                }

                string rScheduleSequence = IBI.Shared.AppUtility.ListToCommaValues(vdvSchedule.ContainsStops.ToList());
                string vScheduleSequence = string.Empty;

                int lastStopSequence = 0;
                foreach (string stopId in vdvSchedule.ContainsStops)
                {
                    sId = decimal.Parse(stopId);
                    var jStop = journeyStops.Where(js => js.StopId == sId).FirstOrDefault();

                    if (jStop != null && jStop.StopSequence > lastStopSequence)
                    {
                        lastStopSequence = jStop.StopSequence;
                        vScheduleSequence += stopId + ",";
                    }
                }

                vScheduleSequence = vScheduleSequence.TrimEnd(',');

                if (isMissing && rScheduleSequence == vScheduleSequence)
                {
                    destination = destinationAfterRewrite = string.IsNullOrEmpty(vdvSchedule.RewriteDestination) ? destination : vdvSchedule.RewriteDestination;
                    viaName = string.IsNullOrEmpty(vdvSchedule.RewriteVia) ? viaName : vdvSchedule.RewriteVia;

                    log.AppendLine(String.Format("Schedule after Rewrite: Line: {0} , Destination: {1} , Via: {2}", line, destination, viaName));
                    break;
                }
                else
                {
                    continue;
                }

            }


            /****/

            //journeyStopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(journeyStops.Aggregate((current, next) => current.StopId.ToString() + next.StopId.ToString()));
            journeyStopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(IBI.Shared.AppUtility.ListToSingleLineText(journeyStops.Select(js => js.StopId.ToString()).ToList()));

            string key = customerId.ToString() + "|" + line + "|" + fromName + "|" + destination + "|" + viaName;

            try
            {
                // var scheduleItems = ScheduleCache.Where(s => s.Key == key);

                var scheduleItems = ScheduleCache.Where(s => s.Key == key);
                if (scheduleItems != null && scheduleItems.Count() > 0)
                {
                    var scheduleItem = scheduleItems.FirstOrDefault();

                    //foreach (KeyValuePair<string, string> scheduleItem in scheduleItems)
                    //{                        
                    //if (scheduleItem.Value != null)
                    //{
                    scheduleId = Convert.ToInt32(scheduleItem.Value.Split('|')[0]);
                    scheduleStopSequence = scheduleItem.Value.Split('|')[1];

                    if (journeyStopSequence == scheduleStopSequence)
                    {
                        return scheduleId;
                    }
                    else
                    {
                        //schedule needs to be updated.
                        //in db
                        ScheduleCache.Remove(scheduleItem);

                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            var dbSchedule = dbContext.Schedules.Where(s => s.ScheduleId == scheduleId).FirstOrDefault();
                            dbSchedule.ScheduleStops.Clear();

                            dbSchedule.ScheduleStops = new List<ScheduleStop>();

                            foreach (JourneyStop jStop in journeyStops)
                            {
                                ScheduleStop sStop = new ScheduleStop();
                                sStop.StopGPS = jStop.StopGPS;
                                sStop.StopId = jStop.StopId;
                                sStop.StopName = jStop.StopName;
                                sStop.StopSequence = jStop.StopSequence;
                                sStop.Zone = jStop.Zone;
                                sStop.ScheduleId = scheduleId;

                                dbSchedule.ScheduleStops.Add(sStop);
                            }

                            dbSchedule.LastUpdated = DateTime.Now; //fixed bug

                            dbContext.SaveChanges();

                            //in cached schedules
                            ScheduleCache.Add(new KeyValuePair<string, string>(key, scheduleId.ToString() + "|" + journeyStopSequence));

                            return scheduleId;
                        }


                    }

                    //}

                    //}
                }

                scheduleId = 0; //reset
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var schedules = dbContext.Schedules.Where(s => s.CustomerId == customerId &&
                                                    s.Line == line &&
                                                    s.FromName == fromName &&
                                                    s.Destination == destination &&
                                                    (s.ViaName == viaName || (s.ViaName == null && viaName == ""))
                                                    );

                    foreach (var schedule in schedules)
                    {
                        if (schedule.ScheduleStops.Count() > 0)
                        {
                            scheduleStopSequence = IBI.Shared.AppUtility.ConvertStringtoMD5(IBI.Shared.AppUtility.ListToSingleLineText(schedule.ScheduleStops.Select(js => js.StopId.ToString()).ToList()));
                        }

                        if (scheduleStopSequence == journeyStopSequence)
                        {
                            scheduleId = schedule.ScheduleId;
                        }


                    }

                    if (scheduleId == 0 && generateScheduleInDB)
                    {
                        Schedule sch = new IBI.DataAccess.DBModels.Schedule
                        {
                            CustomerId = customerId,
                            Destination = destination,
                            FromName = fromName,
                            Line = line,
                            LastUpdated = DateTime.Now,
                            ViaName = viaName,
                            RouteDirectionId = GetRouteDirectionId(subId, customerId, line, destination, viaName)
                        };


                        sch.ScheduleStops = new List<ScheduleStop>();

                        foreach (JourneyStop jStop in journeyStops)
                        {
                            ScheduleStop sStop = new ScheduleStop();
                            sStop.StopGPS = jStop.StopGPS;
                            sStop.StopId = jStop.StopId;
                            sStop.StopName = jStop.StopName;
                            sStop.StopSequence = jStop.StopSequence;
                            sStop.Zone = jStop.Zone;
                            //sStop.ScheduleId = sch.ScheduleId;

                            sch.ScheduleStops.Add(sStop);
                        }
                        dbContext.Schedules.Add(sch);

                        dbContext.SaveChanges();

                        //generate sign for new schedule
                        try
                        {
                            IBI.Implementation.Schedular.SignController.CheckSignForNewSchedule(dbContext, sch, "HaCon VDV");
                        }
                        catch (Exception signException)
                        {
                            //error creating sign. check SystemLogs
                            throw new Exception("Error Creating Sign for new schedule [" + signException.Message + "]");
                        }
                        finally
                        {
                            scheduleId = sch.ScheduleId;

                            //add this schedule to ScheduleCache.
                            ScheduleCache.Add(new KeyValuePair<string, string>(key, scheduleId.ToString() + "|" + journeyStopSequence));
                        }
                        // dbContext.SaveChanges();

                    }
                    //else
                    //{
                    //    scheduleId = 0;
                    //}

                }

            }

            catch (Exception ex)
            {
                AppUtility.Log2File(GetJourneyLogFileName(customerId), "Failed to get schedule for current journey. info: " + key + " [" + ex.Message + "]\n" + ex.StackTrace, true, true);
            }

            log.AppendLine(string.Format("Schedule ID assigned to this Journey: {0}", scheduleId));

            return scheduleId;
        }

        private static int GetRouteDirectionId(Int64 subId, int customerId, string line, string destination, string viaName, bool generateRouteInDB = true)
        {
            int routeDirectionId = 0;

            try
            {
                string name = customerId.ToString() + "-" + line + "-" + destination + "-" + viaName;
                name = name.Trim('-');

                var routeItems = RouteCache.Where(s => s.Value == name);

                if (routeItems != null)
                {

                    foreach (KeyValuePair<string, string> scheduleItem in routeItems)
                    {
                        if (scheduleItem.Value != null && scheduleItem.Value != "0")
                        {
                            routeDirectionId = Convert.ToInt32(scheduleItem.Key);

                            return routeDirectionId;
                        }
                    }
                }

                routeDirectionId = 0; //reset
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var routes = dbContext.RouteDirections.Where(s => s.Name == name);

                    foreach (var route in routes)
                    {
                        routeDirectionId = route.RouteDirectionId;
                    }

                    if (routeDirectionId == 0 && generateRouteInDB)
                    {
                        RouteDirection rd = new RouteDirection
                        {
                            Name = name

                        };

                        dbContext.RouteDirections.Add(rd);

                        dbContext.SaveChanges();

                        routeDirectionId = rd.RouteDirectionId;

                        //add this schedule to ScheduleCache.
                        RouteCache.Add(new KeyValuePair<string, string>(routeDirectionId.ToString(), name));

                    }
                    //else
                    //{
                    //    routeDirectionId = 0;
                    //}

                }

            }

            catch (Exception ex)
            {
                AppUtility.Log2File(GetJourneyLogFileName(customerId), "Failed to get RouteDirectionId for current journey [" + ex.Message + "]", true, true);
            }

            return routeDirectionId;
        }


        public static void ProcessPollData(XDocument xDoc, Int64 subId, int chunkNo, VdvSubscriptionConfiguration config)
        {

            if (config.SaveDataInFiles)
                xDoc.Save(Path.Combine(AppSettings.GetResourceDirectory(), "Hafas\\data_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_sub_" + subId.ToString() + "_0" + chunkNo.ToString() + ".xml"));

            //if (config.SaveDataInDB == false)
            //    return;

            StringBuilder log = new StringBuilder();

            try
            {

                if (xDoc == null)
                {
                    return;
                }

                string acknowledgement = xDoc.Descendants("Acknowledge").FirstOrDefault().Attribute("Result").Value.ToLower();
                int totJourneys = xDoc.Descendants("LineSchedule").Count();

                if (acknowledgement == "ok" && totJourneys > 0)
                {
                    //string fileText = File.ReadAllText(filePath, System.Text.Encoding.Default);
                    //System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                    //MemoryStream memStream = new MemoryStream(encoding.GetBytes(fileText));

                    //XDocument xDoc = XDocument.Load(memStream);

                    //String name = (from p in xDoc.Descendants("name")
                    // select p.Value); 

                    using (IBIDataModel dbContext = new IBIDataModel())
                    {

                        //get all Hacon Stops in memory to avoid repeated queries
                        //dbContext.Stops.Where(s => s.GID == "DEC");
                        var stops = (from s in dbContext.Stops
                                     //where s.GID.ToString().StartsWith("91")
                                     where s.StopSource.ToUpper() == "HACON"
                                     select s);


                        AppUtility.AppendLog(ref log, String.Format("/******** Total journeys in this data chunk: {0} ********/", xDoc.Descendants("LineSchedule").Count().ToString()));

                        int jCounter = 1;
                        foreach (var jr in xDoc.Descendants("LineSchedule"))
                        {

                            string tripId = jr.Descendants("TripName").FirstOrDefault().Value;

                            string line = jr.Descendants("LineID").FirstOrDefault().Value;
                            string destination = jr.Descendants("DirectionText").FirstOrDefault().Value;

                            string jpsString = jr.Descendants("DepartureTime").FirstOrDefault().Value;
                            string jpeString = jr.Descendants("ArrivalTime").LastOrDefault().Value;


                            DateTime plannedStartTime = AppSettings.FixTimeZone(jpsString); //DateTime.ParseExact(jpsString.Substring(0, jpsString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            DateTime plannedEndTime = AppSettings.FixTimeZone(jpsString); //DateTime.ParseExact(jpeString.Substring(0, jpeString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                            bool doSave = false;
                            int customerId = 0;

                            foreach (VdvCustomerConfiguration customerConfig in config.Customers)
                            {
                                //----------------
                                if (customerConfig.LineFilter.Any(l => l.ToString() == line) && customerConfig.SaveDataInDB == true)
                                {
                                    doSave = true;
                                    customerId = customerConfig.CustomerId;
                                    break;
                                }
                                //----------------

                            }

                            if (!doSave)
                            {
                                //jCounter++;
                                continue;
                            }

                            VdvCustomerConfiguration custConfig = config.Customers.Where(c => c.CustomerId == customerId).FirstOrDefault();

                            DateTime operatingDate = DateTime.ParseExact(jr.Descendants("DayType").FirstOrDefault().Value, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                            string externalRef = String.Format("{0}-{1}-{2}", config.ExternalReferencePrefix, operatingDate.ToString("yyyyMMdd"), tripId); //logic of external ref from MAR

                            AppUtility.AppendLog(ref log, String.Format("## Journey # {0} ##", jCounter.ToString()));

                            bool exists = dbContext.Journeys.Where(j => j.ExternalReference == externalRef).Count() > 0;

                            //----------------
                            if (exists)
                            {
                                AppUtility.AppendLog(ref log, String.Format("## Trip # {0} already exists / imported to Journeys table ##\n", tripId));
                                jCounter++;
                                continue;
                            }
                            //----------------

                            decimal firstStopId = decimal.Parse(IBI.Shared.ScheduleHelper.TransformStopNo(jr.Descendants("StopID").FirstOrDefault().Value.ToString(), IBI.Shared.ScheduleHelper.StopPrefix.HACON));
                            decimal lastStopId = decimal.Parse(IBI.Shared.ScheduleHelper.TransformStopNo(jr.Descendants("StopID").LastOrDefault().Value.ToString(), IBI.Shared.ScheduleHelper.StopPrefix.HACON));
                            var lastStop = stops.Where(s => s.GID == lastStopId).FirstOrDefault();

                            if (lastStop != null)
                            {
                                destination = lastStop.StopName;
                            }

                            try
                            {

                                List<JourneyStop> journeyStops = new List<JourneyStop>();

                                StringBuilder jsLog = new StringBuilder();
                                int invalidStops = 0;
                                int stopSequence = 1;
                                foreach (var st in jr.Descendants("ScheduleStop"))
                                {
                                    JourneyStop jStop = new JourneyStop();

                                    jStop.StopId = decimal.Parse(IBI.Shared.ScheduleHelper.TransformStopNo(st.Descendants("StopID").FirstOrDefault().Value.ToString(), IBI.Shared.ScheduleHelper.StopPrefix.HACON));

                                    var dTime = st.Descendants("DepartureTime").FirstOrDefault();
                                    var aTime = st.Descendants("ArrivalTime").FirstOrDefault();

                                    string pdString = dTime != null ? dTime.Value : "";
                                    string paString = aTime != null ? aTime.Value : "";

                                    if (!(string.IsNullOrEmpty(pdString)))
                                        jStop.PlannedDepartureTime = AppSettings.FixTimeZone(pdString); //DateTime.ParseExact(pdString.Substring(0, pdString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                                    if (!(string.IsNullOrEmpty(paString)))
                                        jStop.PlannedArrivalTime = AppSettings.FixTimeZone(paString); //DateTime.ParseExact(paString.Substring(0, paString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                                    if (jStop.PlannedArrivalTime == null)
                                        jStop.PlannedArrivalTime = jStop.PlannedDepartureTime;

                                    if (jStop.PlannedDepartureTime == null)
                                        jStop.PlannedDepartureTime = jStop.PlannedArrivalTime;


                                    //DbGeography stopGPS = new DbGeography();

                                    var stop = stops.Where(s => s.GID == jStop.StopId).FirstOrDefault();


                                    string lat;
                                    string lon;
                                    string sqlPoint;
                                    bool addToJourney = true;

                                    if (stop != null)
                                    {
                                        jStop.StopName = stop.StopName;
                                        //lat = stop.GPSCoordinateNS.ToString().Trim();
                                        //lon = stop.GPSCoordinateEW.ToString().Trim(); 
                                        //sqlPoint = string.Format("POINT({0} {1})", lon, lat);
                                        //jStop.StopGPS = DbGeography.FromText(sqlPoint, 4326);
                                        jStop.StopGPS = stop.StopGPS;
                                        jStop.Zone = GetZoneName(jStop.StopGPS.Latitude.ToString().Replace(',', '.'), jStop.StopGPS.Longitude.ToString().Replace(',', '.'), true);

                                    }
                                    else
                                    {
                                        jStop.StopName = "";

                                        if (jStop.PlannedDepartureTime.Value.ToString("HH:mm:ss") == "00:00:00")
                                        {
                                            AppUtility.AppendLog(ref jsLog, string.Format(" ----- StopId: {0} INVALID STOP", jStop.StopId));
                                        }
                                        else
                                        {
                                            AppUtility.AppendLog(ref jsLog, string.Format(" ----- StopId: {0} INVALID STOP | Missing from Stops Repository | PlannedTimestamp: {1}", jStop.StopId, jStop.PlannedDepartureTime));
                                        }


                                        addToJourney = false;
                                        invalidStops++;

                                    }

                                    if (addToJourney)
                                    {
                                        jStop.StopSequence = stopSequence;
                                        journeyStops.Add(jStop);
                                        stopSequence++;

                                        AppUtility.AppendLog(ref jsLog, string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5} GPS: {6} {7}",
                                                 jStop.StopId, jStop.StopName, stopSequence, jStop.Zone, jStop.PlannedDepartureTime.ToString(), jStop.PlannedArrivalTime.ToString(), jStop.StopGPS.Latitude.ToString(), jStop.StopGPS.Longitude.ToString()));

                                    }

                                }

                                string fromName = journeyStops.FirstOrDefault().StopName;
                                destination = journeyStops.LastOrDefault().StopName;
                                string destinationAfterRewrite = destination;
                                //determine schedules
                                int scheduleId = GetScheduleNumber(custConfig, subId, customerId, line, fromName, destination, null, journeyStops, ref log, ref destinationAfterRewrite, true);


                                Journey journey = new Journey();

                                var existingJourney = dbContext.GetExistingFutureJourney(customerId, externalRef, line, destinationAfterRewrite, plannedStartTime, firstStopId).FirstOrDefault();

                                if (existingJourney != null)
                                {
                                    journey = existingJourney;
                                    journey.JourneyStops.Clear();
                                }

                                journey.JourneyStops = journeyStops;
                                journey.ScheduleId = scheduleId;
                                DateTime? journeyPlannedStartTime = null;
                                DateTime? journeyPlannedEndTime = null;

                                DateTime? stopPlannedDeparture = null;

                                journey.JourneyStateId = (int)IBI.Shared.Common.Types.JourneyStates.FUTURE;
                                journey.BusNumber = "0";
                                //journey.ClientRef = null;
                                journey.CustomerId = customerId;
                                //journey.EndTime = null;
                                journey.ExternalReference = externalRef;
                                journey.LastUpdated = DateTime.Now;
                                //journey.StartTime = null;


                                journey.PlannedStartTime = journey.JourneyStops.FirstOrDefault().PlannedDepartureTime;
                                journey.PlannedEndTime = journey.JourneyStops.LastOrDefault().PlannedArrivalTime;
                                



                                //if (exists == false)
                                {

                                    AppUtility.AppendLog(ref log, String.Format("Journey:: ({0}) JourneyId={1} CustomerId={2} TripNumber={3}, ScheduleNumber: {4} Line:{5}, From:{6}, Destination:{7}, Total Stops: {8}",
                                        (journey.JourneyId > 0 ? "UPDATED" : "INSERTED"), journey.JourneyId, journey.CustomerId, journey.ExternalReference, journey.ScheduleId, line, fromName, destination, journey.JourneyStops.Count()));

                                    if (journey.JourneyId == 0) //otherwise dbContext will update that existing Journey
                                    {
                                        dbContext.Journeys.Add(journey);
                                    }

                                    AppUtility.AppendLog(ref log, String.Format("Stops without GPS, Name and Time: {0}", invalidStops));

                                    log.AppendLine(jsLog.ToString());
                                    //foreach (var stop in journey.JourneyStops)
                                    //{
                                    //    log.AppendLine(string.Format(" ----- StopId: {0} StopName: {1} StopSequence: {2} Zone: {3} Departure: {4} Arrival: {5}",
                                    //         stop.StopId, stop.StopName, stop.StopSequence, stop.Zone, stop.PlannedDepartureTime.ToString(), stop.PlannedArrivalTime.ToString()));
                                    //}

                                    AppUtility.Log2File(GetJourneyLogFileName(customerId), log.ToString());
                                    log.Clear();

                                    dbContext.SaveChanges();

                                    AppUtility.AppendLog(ref log, String.Format("Journey Added: {0}", journey.JourneyId));
                                    AppUtility.Log2File(GetJourneyLogFileName(customerId), log.ToString());
                                    log.Clear();
                                }

                                journey = null;
                            }
                            catch (Exception jException)
                            {
                                string errorMessage = "";
                                Exception innerException = jException;

                                while (innerException != null)
                                {
                                    errorMessage += innerException.Message + "::" + innerException.StackTrace + Environment.NewLine;

                                    innerException = innerException.InnerException;
                                }

                                AppUtility.AppendLog(ref log, String.Format("Failed to save this journey. {0}\n", errorMessage), true);
                            }

                            AppUtility.Log2File(GetJourneyLogFileName(customerId), log.ToString(), true);

                            System.Threading.Thread.Sleep(10);

                            jCounter++;


                        }
                    }
                }

                // AppUtility.Log2File("Hacon_" + config.CustomerId.ToString() + "_Journeys_sub_" + subId.ToString(), log.ToString() + "\n\nAll Journeys imported successfully");

            }
            catch (Exception ex)
            {
                AppUtility.Log2File(GetSubscriptionLogFileName(), "Failure : " + ex.Message + " Stack : " + ex.StackTrace.ToString(), true, true);
                string test = string.Empty;
            }
        }

    }



    #region Private



    #endregion
}
