﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IBI.HaConHRXPost
{
    public partial class DataService : ServiceBase
    {
        private HRXDataPost serverInstance;

        public DataService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.serverInstance = new HRXDataPost();
        }

        protected override void OnStop()
        {
            this.serverInstance.Dispose();
        }
    }
}
