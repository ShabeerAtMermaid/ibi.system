﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace IBI.HaConHRXPost
{
    public class AppSettings
    {
        public static SqlConnection GetIBIDatabaseConnection()
        {
            String connectionString = ConfigurationManager.ConnectionStrings["IBIDatabaseConnection"].ConnectionString;

            return new SqlConnection(connectionString);
        }

        public static String GetResourceDirectory()
        {
            String resourceDirectory = ConfigurationManager.AppSettings["ResourceDirectory"];

            return resourceDirectory;
        }

         
        #region "HaCon HRX Post"

        public static String HaConHRXPost_EndPoint_Url()
        {
            String result = ConfigurationManager.AppSettings["HaConHRXPost_EndPoint_Url"];

            return result;
        }

        public static String HaConHRXPost_EndPoint_Username()
        {
            String result = ConfigurationManager.AppSettings["HaConHRXPost_EndPoint_Username"];

            return result;
        }

        public static String HaConHRXPost_EndPoint_Password()
        {
            String result = ConfigurationManager.AppSettings["HaConHRXPost_EndPoint_Password"];

            return result;
        }

         public static bool HaConHRXPostEnabled()
        {
            bool retVal = true;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost"]))
            {
                retVal = bool.Parse(ConfigurationManager.AppSettings["HaConHRXPost"]);
            }

            return retVal;
        }

         public static int PostIntervalInSeconds()
         {
             int retVal = 60;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost_PostIntervalInSeconds"]))
             {
                 retVal = int.Parse(ConfigurationManager.AppSettings["HaConHRXPost_PostIntervalInSeconds"]);
             }

             return retVal;
         }

         public static int PingToleranceInSeconds()
         {
             int retVal = 60;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost_PingToleranceInSeconds"]))
             {
                 retVal = int.Parse(ConfigurationManager.AppSettings["HaConHRXPost_PingToleranceInSeconds"]);
             }

             return retVal;
         }
         public static int[] HaConHRXPostCustomerIds()
         {
             int[] retVal = new int[]{};
             string list = "";
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost_CustomerIds"]))
             {
                 list =  ConfigurationManager.AppSettings["HaConHRXPost_CustomerIds"];

                 retVal = list.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(n => int.Parse(n)).ToArray(); 

             }

             return retVal;
         }

         public static string[] HaConHRXPostSenders()
         {
             string[] retVal = new string[] { };
             string list = "";
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost_Senders"]))
             {
                 list = ConfigurationManager.AppSettings["HaConHRXPost_Senders"];

                 retVal = list.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

             }

             return retVal;
         }

         public static string[] HaConHRXPostLines()
         {
             string[] retVal = new string[]{};
             string list = "";
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["HaConHRXPost_Lines"]))
             {
                 list =  ConfigurationManager.AppSettings["HaConHRXPost_Lines"];

                 retVal = list.ToString().Split(new char[]{','}, StringSplitOptions.RemoveEmptyEntries);

             }

             return retVal;
         }

        #endregion


         public static bool LogAllMessages()
         {
             bool retVal = true;
             if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogAllMessages"]))
             {
                 retVal = bool.Parse(ConfigurationManager.AppSettings["LogAllMessages"]);
             }

             return retVal;
         }

    }
}