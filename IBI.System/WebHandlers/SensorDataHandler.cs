﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.Text;

/// <summary>
/// Summary description for MapDataHandler
/// </summary>
/// 
namespace WebHandlers
{
    public class SensorDataHandler : IHttpHandler
    {
        private HttpContext context;

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            this.context = context;

            String json = String.Empty;

            String requestFile = context.Request.Url.AbsolutePath.ToLower();

            if (requestFile.Contains("/"))
                requestFile = requestFile.Substring(requestFile.LastIndexOf("/") + 1);

            switch (requestFile)
            {
                case "sensorhistory.json":
                    json = GetSensorHistory();
                    break;

                case "clientlist.json":
                    json = GetClientList();
                    break;

            }

            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //context.Response.ContentType = "application/json";
            context.Response.ContentType = "text/plain";
            context.Response.ContentEncoding = Encoding.UTF8;

            context.Response.Write(json);
        }

        private String GetSensorHistory()
        {
            DateTime historyDate = DateTime.ParseExact(context.Request["date"], "dd-MM-yyyy", null);
            String clientID = context.Request["clientid"];
            String[] sensors = context.Request["sensors"].Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            String json = "{" + Environment.NewLine;

            json += "\"cols\": [{\"id\": \"Timestamp\", \"label\": \"Time\", \"type\": \"timeofday\"}" + Environment.NewLine;

            foreach (var sensor in sensors)
            {
                string sensorName = sensor.ResolveSensorName();

                json += "       ,{\"id\": \"" + sensorName + "\", \"label\": \"" + sensorName + "\", \"type\": \"number\"}" + Environment.NewLine;
            }

            json += "]," + Environment.NewLine;
            json += "\"rows\": [";

            String connectionString = ConfigurationManager.AppSettings["OperationsDBConnectionString"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = @"SELECT [Timestamp]" + Environment.NewLine;

                foreach (var sensor in sensors)
                {
                    //script += ", [" + sensor + "].data.value('(text())[1]', 'varchar(260)') AS " + sensor + Environment.NewLine;
                    string sensorValue = sensor.Replace("'", "''");
                    string sensorName = sensor.ResolveSensorName();

                    bool isAttribute = sensor.IsAttribute();

                    if (isAttribute)
                        script += ", [PingData_Xml].value('(" + sensorValue + ")[1]', 'varchar(260)') AS [" + sensorName + "]" + Environment.NewLine;
                    else if (String.Compare("BootCount", sensor, StringComparison.OrdinalIgnoreCase) == 0)
                        script += ", [PingData_Xml].value('(/PingData/System/BootCount/node())[1]', 'varchar(260)') AS [" + sensorName + "]" + Environment.NewLine;
                    else if (String.Compare("HDDFails", sensor, StringComparison.OrdinalIgnoreCase) == 0)
                        script += ", [PingData_Xml].value('(/PingData/Diagnostics/Observation[@id=''hdd_fails'']/node())[1]', 'varchar(260)') AS [" + sensorName + "]" + Environment.NewLine;
                    else if (sensor.StartsWith("count("))
                        script += ", [PingData_Xml].query('" + sensorValue + "') AS [" + sensorName + "]" + Environment.NewLine;
                    else if (!sensor.StartsWith("/"))
                        script += ", [PingData_Xml].value('(/PingData/System/Sensors/Sensor[@id=''" + sensorValue + "'']/node())[1]', 'varchar(260)') AS [" + sensorName + "]" + Environment.NewLine;
                    else
                        script += ", [PingData_Xml].value('(" + sensorValue + "/node())[1]', 'varchar(260)') AS [" + sensorName + "]" + Environment.NewLine;
                }

                script += "FROM [ClientPings]" + Environment.NewLine;

                //foreach (var sensor in sensors)
                //{               
                //        script += "CROSS APPLY [ClientPings].[PingData_Xml].nodes('/PingData/System/Sensors/Sensor') AS [" + sensor + "](data)" + Environment.NewLine;
                //}

                script += "WHERE [Client]='" + clientID + "'" + Environment.NewLine;
                script += "AND [Timestamp] BETWEEN '" + historyDate.ToString("yyyy-MM-dd") + "' AND '" + historyDate.AddDays(1).ToString("yyyy-MM-dd") + "'" + Environment.NewLine;

                //foreach (var sensor in sensors)
                //{
                //    script += "AND	[" + sensor + "].data.value('(@id)[1]', 'varchar(260)') = '" + sensor + "'" + Environment.NewLine;
                //}

                script += "ORDER BY [Timestamp]" + Environment.NewLine;

                DataTable data = new DataTable();

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    command.CommandTimeout = 300;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int rowCounter = 0;
                        DateTime lastTimestamp = new DateTime(historyDate.Year, historyDate.Month, historyDate.Day, 0, 0, 0);
                        int maxGap = 20;

                        while (reader.Read())
                        {
                            DateTime timestamp = reader.GetDateTime(0);

                            while (lastTimestamp.AddMinutes(maxGap) < timestamp)
                            {
                                json += (rowCounter == 0 ? "" : ",") + "{\"c\":[";
                                json += "{\"v\": " + lastTimestamp.ToString("'['H', 'm', 's']'") + "}";

                                for (int i = 0; i < sensors.Length; i++)
                                {
                                    json += ",{\"v\": 0.0}";
                                }

                                json += "]}" + Environment.NewLine;

                                lastTimestamp = lastTimestamp.AddMinutes(maxGap);

                                rowCounter++;
                            }

                            lastTimestamp = timestamp;

                            json += (rowCounter == 0 ? "" : ",") + "{\"c\":[";
                            json += "{\"v\": " + timestamp.ToString("'['H', 'm', 's']'") + "}";

                            for (int i = 0; i < sensors.Length; i++)
                            {
                                string rawValue = "";
                                double value = 0;

                                if (!reader.IsDBNull(i + 1))
                                {
                                    rawValue = reader.GetString(i + 1);

                                    double.TryParse(rawValue, out value);
                                }

                                json += ",{\"v\": " + value.ToString(CultureInfo.InvariantCulture) + "}";

                            }

                            json += "]}" + Environment.NewLine;

                            rowCounter++;
                        }
                    }
                }
            }

            json += "]" + Environment.NewLine;
            json += "}";

            return json;
        }

        private String GetClientList()
        {
            String json = "{" + Environment.NewLine;

            String connectionString = ConfigurationManager.AppSettings["OperationsDBConnectionString"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = @"
SELECT [ClientID], [Name]
FROM [MermaidOperationsServer].[dbo].[Clients]";
                ;

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int rowCounter = 0;

                        while (reader.Read())
                        {
                            json += (rowCounter == 0 ? "" : ",") + "{\"ClientID\":\"" + reader["ClientID"].ToString() + "\",\"Name\":\"" + reader["Name"].ToString() + "\"}";
                            json += Environment.NewLine;

                            rowCounter++;
                        }
                    }
                }
            }

            json += "}";

            return json;
        }
        }

    public static class MyExtensions
    {
        public static string RemoveInvalidChars(this string source)
        {
            char[] validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-".ToCharArray();
            string output = "";

            for (int i = 0; i < source.Length; i++)
            {
                char item = source[i];
                char prevItem = '_';

                if (output.Length > 0)
                    prevItem = output[output.Length - 1];

                if (validChars.Contains(item))
                    output += item.ToString();
                else if (prevItem != '_')
                    output += "_";
            }

            if (output[output.Length - 1] == '_')
                output = output.Substring(0, output.Length - 1);

            //foreach (char item in source.ToCharArray())
            //{
            //    if (validChars.Contains(item))
            //        output += item.ToString();
            //    else
            //        output += "_";
            //}

            return output;
            //return new String(source.Where(x => validChars.Contains(x)).ToArray());
        }

        public static string ResolveSensorName(this string source)
        {
            bool isAttributeValue = false;
            bool isAttribute = source.IsAttribute();

            int lastIndex = source.LastIndexOfAny(new char[] { '/' });

            if (lastIndex > -1)
                isAttributeValue = source.Substring(lastIndex + 1).IndexOf("@") > -1;

            if (isAttribute)
            {
                string name = source.Substring(source.LastIndexOf("@") + 1);

                return name.RemoveInvalidChars();
            }
            else if (isAttributeValue)
            {
                // /PingData/System/Sensors/Sensor[@id='sensora']
                int nameIndex = source.LastIndexOfAny(new char[] { '@' }) + 1;
                int valueIndex = nameIndex + source.Substring(nameIndex).IndexOfAny(new char[] { '\'', '\"' }) + 1;
                int valueLength = source.LastIndexOfAny(new char[] { '\'', '\"' }) - valueIndex;

                string name = source.Substring(valueIndex, valueLength);

                return name.RemoveInvalidChars();
            }
            else
            {
                int nameIndex = source.LastIndexOfAny(new char[] { '/' }) + 1;
                int nameEndIndex = source.IndexOfAny(new char[] { '[' });
                int nameLength = nameEndIndex > -1 ? nameEndIndex - nameIndex : source.Length - nameIndex;

                string name = source.Substring(nameIndex, nameLength);

                return name.RemoveInvalidChars();
            }
        }

        public static bool IsAttribute(this string source)
        {
            bool isAttribute = false;

            int lastIndex = source.LastIndexOfAny(new char[] { ']', '/' });

            if (lastIndex > -1)
                isAttribute = source.Substring(lastIndex + 1).IndexOf("@") > -1;

            return isAttribute;
        }
    }
}