﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace WebHandlers
{
    class OperationsHandler : IHttpHandler
    {
        private HttpContext context;

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            this.context = context;

            XElement data = null;

            String requestFile = context.Request.Url.AbsolutePath.ToLower();

            if (requestFile.Contains("/"))
                requestFile = requestFile.Substring(requestFile.LastIndexOf("/") + 1);

            switch (requestFile)
            {
                case "pingdata.xml":
                    String clientID = context.Request["clientid"];

                    data = GetPingData(clientID);
                    break;
            }

            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            //context.Response.ContentType = "application/json";
            context.Response.ContentType = "text/xml";
            context.Response.ContentEncoding = Encoding.UTF8;

            context.Response.Write(data);
        }

        private XElement GetPingData(String clientId)
        {
             String connectionString = ConfigurationManager.AppSettings["OperationsDBConnectionString"];

             using (SqlConnection connection = new SqlConnection(connectionString))
             {
                 connection.Open();

                 String script = 
@"SELECT c.ClientId, c.LastPing, p.PingData
FROM [Clients] c
	LEFT JOIN
[ClientPings] p
ON c.[ClientID]=p.Client AND c.LastPing=p.[Timestamp]
WHERE c.ClientID = '{0}'";

                 DataTable data = new DataTable();

                 using (SqlCommand command = new SqlCommand(String.Format(script, clientId), connection))
                 {
                     command.CommandTimeout = 300;

                     using (SqlDataReader reader = command.ExecuteReader())
                     {
                         if (!reader.HasRows)
                             return null;

                         reader.Read();

                         return new XElement("Data",
                                             new XElement("ClientId", reader.GetGuid(0)),
                                             new XElement("LastPing", reader.GetDateTime(1)),
                                             new XElement("PingData", reader.GetString(2))
                                            );
                     }

                 }
             }
        }
    }
}
