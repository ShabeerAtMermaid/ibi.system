﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace WebHandlers
{
    class SiriusImportHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                XElement siriusDocument = LoadSiriusData(context);
                List<BusData> busList = ResolveBusList();

                this.UpdateSiriusLog(siriusDocument, busList);

                context.Response.StatusCode = 200;
            }
            catch (Exception ex)
            {
                String error = "Error importing Sirius data:" + Environment.NewLine;

                Exception exception = ex;

                while (exception != null)
                {
                    error += exception.Message + Environment.NewLine;
                    error += exception.StackTrace + Environment.NewLine;
                    error += "--" + Environment.NewLine;

                    exception = exception.InnerException;
                }

                LogSiriusError(context, error);

                throw new Exception("Error importing Sirius data", ex);
            }
        }

        private List<BusData> ResolveBusList()
        {
            List<int> customerIDs = new List<int>() { 2140, 2214 };
            List<BusData> busList = new List<BusData>();

            String connectionString = ConfigurationManager.AppSettings["IBIDatabaseConnection"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = String.Format(@"SELECT [CustomerId], [BusNumber]
                                                FROM [Buses]
                                                WHERE
	                                                [CustomerID] IN ({0})
	                                                AND [InOperation]=1", String.Join(",", customerIDs));

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            busList.Add(new BusData()
                            {
                                CustomerID = reader.GetInt32(reader.GetOrdinal("CustomerId"))
                                ,
                                BusNumber = reader.GetString(reader.GetOrdinal("BusNumber"))
                            });
                        }
                    }
                }
            }

            return busList;
        }

        private XElement LoadSiriusData(HttpContext context)
        {
            using (WebClient client = new WebClientEx())
            {
                String user = "Mermaid";
                String pass = "Arriva.2013";
                //String user = "Vest";
                //String pass = "bus1234";

                //Authorize
                //String authReply = client.DownloadString(String.Format("http://80.63.58.83/AssetLocationsRoute/locationajax.asmx/Logon?UserName={0}&Password={1}", user, pass));
                String authReply = client.DownloadString(String.Format("http://www.traffilog.com/mamws/locationajax.asmx/Logon?UserName={0}&Password={1}", user, pass));
                    
                //Get all busses
                //String vehicleReply = client.DownloadString("http://80.63.58.83/AssetLocationsRoute/locationajax.asmx/getInstalledVehicles?");
                String vehicleReply = client.DownloadString("http://www.traffilog.com/mamws/locationajax.asmx/getInstalledVehicles?");

                //Logoff
                //String disconnectReply = client.DownloadString("http://80.63.58.83/AssetLocationsRoute/locationajax.asmx/Logoff?");
                String disconnectReply = client.DownloadString("http://www.traffilog.com/mamws/locationajax.asmx/Logoff?");

                LogSiriusResponse(context, vehicleReply);

                return XElement.Parse(vehicleReply);
            }
        }

        private void UpdateSiriusLog(XElement siriusDocument, List<BusData> busList)
        {
            //String insertTemplate = @"INSERT INTO [SiriusHistory]([CustomerID],[BusNumber],[Timestamp],[IgnitionOn]) VALUES({0},{1},'" + DateTime.Now.ToString("yyyy-MM-dd HH:" + (DateTime.Now.Minute - (DateTime.Now.Minute % 15)).ToString("00") + ":00") + "',{2});" + Environment.NewLine;
            String insertLogTemplate = @"INSERT INTO [SiriusHistory]([CustomerID],[BusNumber],[Timestamp],[IgnitionOn]) VALUES({0},'{1}','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',{2});" + Environment.NewLine;
            String insertStatusTemplate = @"DELETE FROM [SiriusStatus] WHERE [CustomerID]={0} AND [BusNumber]='{1}'
                                            INSERT INTO [SiriusStatus]([CustomerID],[BusNumber],[Timestamp],[IgnitionOn]) VALUES({0},'{1}','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',{2});" + Environment.NewLine;

            String connectionString = ConfigurationManager.AppSettings["TempDBConnectionString"];

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                String script = "";

                foreach (BusData busData in busList)
                {
                    var vehicleElement = siriusDocument.Descendants()
                                            .Where(v => v.Name == "VEHICLE" && v.Attributes("VEHICLE_REG").Single().Value == busData.BusNumber)
                                            .FirstOrDefault();

                    if (vehicleElement == null)
                        continue;

                    String lastTimestampString = vehicleElement.Attributes("LAST_TIMESTAMP").Single().Value;

                    DateTime lastTimestamp = System.Xml.XmlConvert.ToDateTime(lastTimestampString + "Z");
                    int lastStatus = int.Parse(vehicleElement.Attributes("LAST_STATUS").Single().Value);

                    script += String.Format(insertLogTemplate, new object[] { busData.CustomerID, busData.BusNumber, ((lastTimestamp.AddMinutes(5) > DateTime.Now && lastStatus > 0) ? 1 : 0) });
                    script += String.Format(insertStatusTemplate, new object[] { busData.CustomerID, busData.BusNumber, ((lastTimestamp.AddMinutes(5) > DateTime.Now && lastStatus > 0) ? 1 : 0) });
                }

                using (SqlCommand command = new SqlCommand(script, connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private void LogSiriusResponse(HttpContext context, String text)
        {
            //String logEntry = text;

            //try
            //{
            //    String logPath = Path.Combine(context.Request.PhysicalApplicationPath, @"Logs\siriuslog_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml");

            //    File.WriteAllText(logPath, logEntry + Environment.NewLine);
            //}
            //catch (Exception ex)
            //{
            //    //SILENT
            //}
        }

        private void LogSiriusError(HttpContext context, String text)
        {
            String logEntry = text;

            try
            {
                String logPath = Path.Combine(context.Request.PhysicalApplicationPath, @"Logs\siriuslog_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".log");

                File.WriteAllText(logPath, logEntry + Environment.NewLine);
            }
            catch (Exception ex)
            {
                //SILENT
            }
        }

        public class WebClientEx : WebClient
        {
            public WebClientEx(CookieContainer container)
            {
                this.container = container;
            }

            public WebClientEx()
            {

            }

            private readonly CookieContainer container = new CookieContainer();

            protected override WebRequest GetWebRequest(Uri address)
            {
                WebRequest r = base.GetWebRequest(address);
                var request = r as HttpWebRequest;
                if (request != null)
                {
                    request.CookieContainer = container;
                }
                return r;
            }

            protected override WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
            {
                WebResponse response = base.GetWebResponse(request, result);
                ReadCookies(response);
                return response;
            }

            protected override WebResponse GetWebResponse(WebRequest request)
            {
                WebResponse response = base.GetWebResponse(request);
                ReadCookies(response);
                return response;
            }

            private void ReadCookies(WebResponse r)
            {
                var response = r as HttpWebResponse;
                if (response != null)
                {
                    CookieCollection cookies = response.Cookies;
                    container.Add(cookies);
                }
            }
        }

        private class BusData
        {
            public int CustomerID { get; set; }
            public String BusNumber { get; set; }
        }
    }
}
