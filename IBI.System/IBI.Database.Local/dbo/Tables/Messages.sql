﻿CREATE TABLE [dbo].[Messages] (
    [MessageId]         INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]        INT           NULL,
    [MessageCategoryId] INT           NULL,
    [Subject]           VARCHAR (255) NULL,
    [MessageText]       VARCHAR (MAX) NULL,
    [ValidFrom]         DATETIME      CONSTRAINT [DF_Messages_ValidFrom] DEFAULT (getdate()) NOT NULL,
    [ValidTo]           DATETIME      CONSTRAINT [DF_Messages_ValidTo] DEFAULT (getdate()+(2)) NOT NULL,
    [Sender]            INT           NULL,
    [DateAdded]         DATETIME      CONSTRAINT [DF_Messages_DateAdded] DEFAULT (getdate()) NULL,
    [LastModifiedBy]    INT           NULL,
    [DateModified]      DATETIME      CONSTRAINT [DF_Messages_DateModified] DEFAULT (getdate()) NULL,
    [IsArchived]        BIT           CONSTRAINT [DF_Messages_IsArchived] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED ([MessageId] ASC),
    CONSTRAINT [FK_Messages_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]),
    CONSTRAINT [FK_Messages_MessageCategories] FOREIGN KEY ([MessageCategoryId]) REFERENCES [dbo].[MessageCategories] ([MessageCategoryId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_Messages_Users] FOREIGN KEY ([Sender]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE ON UPDATE CASCADE
);

