﻿CREATE TABLE [dbo].[Stops] (
    [GID]             DECIMAL (19)  NOT NULL,
    [StopName]        VARCHAR (500) NOT NULL,
    [GPSCoordinateNS] VARCHAR (50)  NULL,
    [GPSCoordinateEW] VARCHAR (50)  NULL,
    [GPSProjection]   VARCHAR (50)  CONSTRAINT [DF_Stops_GPSProjection] DEFAULT ('UTM32N') NOT NULL,
    CONSTRAINT [PK_Stops] PRIMARY KEY CLUSTERED ([GID] ASC)
);

