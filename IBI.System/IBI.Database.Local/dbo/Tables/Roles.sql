﻿CREATE TABLE [dbo].[Roles] (
    [RoleId]   INT          NOT NULL,
    [RoleName] VARCHAR (50) NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

