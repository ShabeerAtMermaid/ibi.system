﻿CREATE TABLE [dbo].[SignLayouts] (
    [LayoutId]       INT          IDENTITY (1, 1) NOT NULL,
    [GraphicsRuleId] INT          NULL,
    [Type]           VARCHAR (50) NULL,
    [Width]          INT          NULL,
    [Height]         INT          NULL,
    [Manufacturer]   VARCHAR (50) NULL,
    [Technology]     VARCHAR (50) NULL,
    [CustomerId]     INT          NULL,
    CONSTRAINT [PK_SignLayouts] PRIMARY KEY CLUSTERED ([LayoutId] ASC),
    CONSTRAINT [FK_SignLayouts_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]),
    CONSTRAINT [FK_SignLayouts_SignGraphicsRules] FOREIGN KEY ([GraphicsRuleId]) REFERENCES [dbo].[SignGraphicsRules] ([GraphicsRuleId])
);

