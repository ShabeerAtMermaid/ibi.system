﻿CREATE TABLE [dbo].[DataUpdateStatuses] (
    [DataKey]    VARCHAR (100) NOT NULL,
    [Timestamp]  DATETIME      NOT NULL,
    [CustomerId] INT           NOT NULL,
    [BusNumber]  INT           NOT NULL,
    [Value]      VARCHAR (255) NULL,
    CONSTRAINT [PK_DataUpdateStatuses] PRIMARY KEY CLUSTERED ([DataKey] ASC, [CustomerId] ASC, [BusNumber] ASC),
    CONSTRAINT [FK_DataUpdateStatuses_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

