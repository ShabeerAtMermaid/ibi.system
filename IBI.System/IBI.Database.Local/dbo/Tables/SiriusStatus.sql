﻿CREATE TABLE [dbo].[SiriusStatus] (
    [ID]         INT          IDENTITY (1, 1) NOT NULL,
    [CustomerID] INT          NOT NULL,
    [BusNumber]  VARCHAR (50) NOT NULL,
    [Timestamp]  DATETIME     NOT NULL,
    [IgnitionOn] BIT          NOT NULL,
    CONSTRAINT [PK_SiriusStatus] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_SiriusStatus_CustomerID_BusNumber]
    ON [dbo].[SiriusStatus]([CustomerID] ASC, [BusNumber] ASC);

