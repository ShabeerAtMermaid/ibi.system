﻿CREATE TABLE [dbo].[Destinations] (
    [CustomerID]  INT           NOT NULL,
    [Line]        VARCHAR (50)  NOT NULL,
    [FromName]    VARCHAR (500) NOT NULL,
    [Destination] VARCHAR (500) NOT NULL,
    [ViaName]     VARCHAR (500) NOT NULL,
    [MoviaEntry]  BIT           CONSTRAINT [DF_Destinations_MoviaEntry] DEFAULT ((1)) NOT NULL,
    [LastSeen]    DATETIME      NULL,
    [Excluded]    BIT           NULL
);

