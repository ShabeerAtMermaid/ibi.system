﻿CREATE TABLE [dbo].[MessageBuses] (
    [MessageId]  INT          NOT NULL,
    [BusNumber]  VARCHAR (50) NOT NULL,
    [CustomerId] INT          NOT NULL,
    CONSTRAINT [PK_MessageBuses] PRIMARY KEY CLUSTERED ([MessageId] ASC, [BusNumber] ASC, [CustomerId] ASC),
    CONSTRAINT [FK_MessageBuses_Buses] FOREIGN KEY ([BusNumber], [CustomerId]) REFERENCES [dbo].[Buses] ([BusNumber], [CustomerId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_MessageBuses_Messages] FOREIGN KEY ([MessageId]) REFERENCES [dbo].[Messages] ([MessageId]) ON DELETE CASCADE ON UPDATE CASCADE
);

