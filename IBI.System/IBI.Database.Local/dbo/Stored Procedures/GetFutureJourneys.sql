﻿-- =============================================
-- Author:           nak
-- Create date: 16 may 2013
-- =============================================
CREATE PROCEDURE [dbo].[GetFutureJourneys]
       @lat nvarchar(20) = '',
       @lng nvarchar(20) = '',
       @radius int = 50,
       @line varchar(255) = '',
       @starttime varchar(50) = '',
       @endtime varchar(50) = '',
       @period int = 24,
       @customerid int
AS
BEGIN
              -- CONVERT(Datetime, '2011-09-28 18:01:00')
              IF ( @lat <> '' AND @lng <> '' AND @period <> -1 )
              BEGIN
                     DECLARE @p GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + @lat + ' ' + @lng + ')', 4326);
                     SELECT
                           CustomerId, Line, StartPoint as FromName, Destination, ScheduleNumber, JourneyNumber, JourneyInfo.data.value('(md5/text())[1]', 'varchar(260)') as md5
                           FROM [Journey_Journeys] journeys
                           CROSS APPLY JourneyData.nodes('/Journey') AS JourneyInfo(data)
                     WHERE
                           GEOGRAPHY::STGeomFromText('POINT('+ convert(nvarchar(20), JourneyInfo.data.value('(GPSCoordinateNS/text())[1]', 'varchar(260)'))+' '+ convert( nvarchar(20), JourneyInfo.data.value('(GPSCoordinateEW/text())[1]', 'varchar(260)'))+')', 4326).STBuffer(@radius).STIntersects(@p) = 1 AND BusNumber=0
                           AND PlannedDepartureTime <= DATEADD(HOUR, @period, GETDATE()) AND CustomerId=@customerid
              END
 
              IF ( @lat <> '' AND @lng <> '' AND @starttime <> '' AND @endtime <> '' )
              BEGIN
                     DECLARE @p1 GEOGRAPHY = GEOGRAPHY::STGeomFromText('POINT(' + @lat + ' ' + @lng + ')', 4326);
                     SELECT
                           CustomerId, Line, StartPoint as FromName, Destination, ScheduleNumber, JourneyNumber, JourneyInfo.data.value('(md5/text())[1]', 'varchar(260)') as md5
                           FROM [Journey_Journeys] journeys
                           CROSS APPLY JourneyData.nodes('/Journey') AS JourneyInfo(data)
                     WHERE
                           GEOGRAPHY::STGeomFromText('POINT('+ convert(nvarchar(20), JourneyInfo.data.value('(GPSCoordinateNS/text())[1]', 'varchar(260)'))+' '+ convert( nvarchar(20), JourneyInfo.data.value('(GPSCoordinateEW/text())[1]', 'varchar(260)'))+')', 4326).STBuffer(@radius).STIntersects(@p1) = 1 AND BusNumber=0
                           AND PlannedDepartureTime >= CONVERT(Datetime, @starttime) AND PlannedDepartureTime <= CONVERT(Datetime, @endtime) AND CustomerId=@customerid
              END
 
              IF ( @line <> '' AND @period <> -1 )
              BEGIN
                     SELECT
                           CustomerId, Line, StartPoint as FromName, Destination, ScheduleNumber, JourneyNumber, JourneyInfo.data.value('(md5/text())[1]', 'varchar(260)') as md5
                           FROM [Journey_Journeys] journeys
                           CROSS APPLY JourneyData.nodes('/Journey') AS JourneyInfo(data)
                     WHERE
                           Line=@line AND PlannedDepartureTime <= DATEADD(HOUR, @period, GETDATE()) AND BusNumber=0  AND CustomerId=@customerid
              END
 
              IF ( @line <> '' AND @starttime <> '' AND @endtime <> '' )
              BEGIN
                     SELECT
                           CustomerId, Line, StartPoint as FromName, Destination, ScheduleNumber, JourneyNumber, JourneyInfo.data.value('(md5/text())[1]', 'varchar(260)') as md5
                           FROM [Journey_Journeys] journeys
                           CROSS APPLY JourneyData.nodes('/Journey') AS JourneyInfo(data)
                     WHERE
                           PlannedDepartureTime >= CONVERT(Datetime, @starttime) AND PlannedDepartureTime <= CONVERT(Datetime, @endtime) AND BusNumber=0  AND CustomerId=@customerid
              END
END
