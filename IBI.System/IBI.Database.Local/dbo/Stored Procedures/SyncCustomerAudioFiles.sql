﻿CREATE PROC [dbo].[SyncCustomerAudioFiles]
	 
AS
	BEGIN

truncate table CustomerAudioFiles
DBCC CHECKIDENT('CustomerAudioFiles', RESEED, 0)
	
DECLARE @CustomerId INT
DECLARE @cur CURSOR
SET @cur = CURSOR FOR
	SELECT CustomerId FROM Customers

OPEN @cur
FETCH NEXT FROM @cur INTO @CustomerId

WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @CustomerId;
		 
		 WITH allstops([StopName], [CustomerId], [AnnouncementVoice])
		 AS
		 (
		 SELECT
		  ss.StopName AS StopName,
		  Customers.CustomerID, Customers.AnnouncementVoice
		 FROM [Schedules] schedules
		  INNEr JOIN ScheduleStops ss on schedules.ScheduleId = ss.ScheduleId
		  inner join Customers on schedules.CustomerID = Customers.CustomerId
		 WHERE ISNULL(ss.StopName, '') <> ''
			AND Customers.CustomerId = @CustomerId
		  
		  )
		
		
		insert into CustomerAudioFiles (CustomerId, StopName, Voice)
		 	 	  
			SELECT @CustomerId, allstops.[StopName] COLLATE Danish_Norwegian_CI_AS, allstops.[AnnouncementVoice]  COLLATE Danish_Norwegian_CI_AS
			 FROM allstops
			 intersect
			 SELECT @CustomerId, tts.[Text], tts.[Voice]
			 FROM [TTSServer_GRANDPA].[dbo].[AudioFiles] tts 
		
		FETCH NEXT FROM @cur INTO @CustomerId
	END

CLOSE @cur
DEALLOCATE @cur
END
