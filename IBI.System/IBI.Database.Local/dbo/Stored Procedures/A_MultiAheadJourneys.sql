﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE [dbo].[A_MultiAheadJourneys] 
	@ScheduleNumber int = 0,
	@BusNumber int = 0,
	@StartTime datetime = NULL,
	@EndTime datetime = NULL
AS
BEGIN
	DECLARE @scheduleData xml
	DECLARE @myStopName varchar(100)
	DECLARE @missing bit
	DECLARE @JS varchar(MAX)
	DECLARE @tmpJourneyNumber int
	DECLARE @tmpBusNumber varchar(100)
	DECLARE @tmpJourneyDate datetime
	DECLARE @tmpJourneyData xml
	
	DECLARE @CompareTable table
	(
		currentjourney int,
		busnumber varchar(100),
		aheadjourney varchar(100),
		stopname varchar(100)
	)
	DECLARE jcursor CURSOR FOR
		select top 100 JourneyNumber,BusNumber,JourneyDate,JourneyData
		from Journeys WHERE IsLastStop=1 
		AND (@BusNumber=0 OR @BusNumber=BusNumber)
		AND (@ScheduleNumber=0 OR ScheduleNumber=@ScheduleNumber)
		AND (@EndTime IS NULL OR (@EndTime IS NOT NULL AND JourneyDate <= @EndTime)) 
		AND  (@StartTime IS NULL OR (@StartTime IS NOT NULL AND JourneyDate >= @StartTime)) 
		ORDER BY JourneyDate DESC
	OPEN jcursor
	FETCH NEXT FROM jcursor INTO @tmpJourneyNumber,@tmpBusNumber,@tmpJourneyDate,@tmpJourneyData
	WHILE @@FETCH_STATUS = 0   
	BEGIN
		DECLARE @currentJourney varchar(100)
		SET @currentJourney = ''
    	DECLARE cur CURSOR FOR
		-- SELECT top 1 JourneyData FROM Journeys WHERe IsLastStop=1 and JourneyAhead is not null ORDER BY JourneyDate DESC
			SELECT JData.Stops.value('(JourneyNumber/text())[1]', 'varchar(100)') JourneyNumber,
			JData.Stops.value('(StopName/text())[1]', 'varchar(100)') StopName
			FROM 
				@tmpJourneyData.nodes('/Journey/JourneyStops/StopInfo/Ahead/Stop') AS JData(Stops)

			OPEN cur
				FETCH NEXT FROM cur INTO @currentJourney,@myStopName
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						INSERT INTO @CompareTable
						SELECT @tmpJourneyNumber,@tmpBusNumber,@currentJourney,@myStopName
				FETCH NEXT FROM cur INTO @currentJourney,@myStopName		
					END
			CLOSE cur   
			DEALLOCATE cur
	FETCH NEXT FROM jcursor INTO @tmpJourneyNumber,@tmpBusNumber,@tmpJourneyDate,@tmpJourneyData
	END
	CLOSE jcursor   
	DEALLOCATE jcursor

	SELECT currentjourney,busnumber,aheadjourney,stopname from @CompareTable order by currentjourney
END
