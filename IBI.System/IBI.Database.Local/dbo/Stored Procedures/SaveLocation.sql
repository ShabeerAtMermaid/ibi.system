﻿CREATE PROC [dbo].[SaveLocation]
@location geography,
@CustomerId int,
@BusNumber int
AS
	BEGIN
		UPDATE [Buses] set LastKnownLocation = @location where CustomerId=@CustomerId and BusNumber = @BusNumber
	END
