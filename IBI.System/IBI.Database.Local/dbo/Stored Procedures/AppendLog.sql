﻿CREATE PROC [dbo].[AppendLog]
@EntryType varchar(255),
@EntryCategory varchar(255),
@Description text
AS
	BEGIN
		INSERT INTO [SystemLog]([Timestamp], [EntryType], [EntryCategory], [Description]) 
		VALUES(CONVERT(VARCHAR(20), GETDATE(), 20), @EntryType, @EntryCategory, @Description)
	END
