﻿USE [IBI_System]
GO
/****** Object:  StoredProcedure [dbo].[xml_UpdateStop]    Script Date: 10/22/2014 17:34:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--USE [IBI_System]
--GO
--/****** Object:  StoredProcedure [dbo].[xml_UpdateStop]    Script Date: 07/24/2014 12:13:01 ******/
--SET ANSI_NULLS ON
--GO
-- EXEC xml_UpdateStop 0, '2014-08-08 06:10:000', '2014-08-08 06:30:000', '2014-08-08 06:11:000', '2014-08-08 06:11:000','2014-08-08 06:12:000', '2014-08-08 06:12:000', 0,  '<Stop><StopId>9025200000000402</StopId><PlanDifference>-47</PlanDifference><JourneyNumber></JourneyNumber><ClientRef>938fa35e-c4bc-49f2-831f-a4b7d368b2xx</ClientRef><Status>LATE</Status><BusNumber>1112</BusNumber><ActualArrival>2014-08-07T12:46:28</ActualArrival><ActualDeparture>2014-08-07T12:46:37</ActualDeparture><ScheduleNumber>915</ScheduleNumber><PlannedArrival>2014-08-07T11:59:00</PlannedArrival><PlannedDeparture>2014-08-07T11:59:00</PlannedDeparture><FromName>R?dovrehallen</FromName><ViaName/><IsLastStop>false</IsLastStop><StopSequence>5</StopSequence><CustomerId>2140</CustomerId><ExternalReference>281</ExternalReference></Stop>'

--SET QUOTED_IDENTIFIER ON
--GO


ALTER PROC [dbo].[xml_UpdateStop]
	@JourneyId int,
	@JourneyPlannedStartTime datetime,
	@JourneyPlannedEndTime datetime,
	--@StopId decimal(19,0),
	@ActualArrival datetime,
	@ActualDeparture datetime,
	@PlannedArrivalTime datetime,
	@PlannedDepartureTime datetime,
	@IsLastStop bit,
	--@ExternalReference varchar(100),
	--@JourneyStops text,
	@StopData text,
	@logEnabled bit = 0
AS
	/*
	DECLARE	@JourneyId int
	DECLARE	@JourneyPlannedStartTime datetime
	DECLARE	@JourneyPlannedEndTime datetime
	DECLARE @StopId decimal(19,0)
	DECLARE	@ActualArrival datetime
	DECLARE	@ActualDeparture datetime
	DECLARE	@IsLastStop bit
	DECLARE	@ExternalReference varchar(100)
	DECLARE	@JourneyStops varchar(max)
	DECLARE	@StopData varchar(max)
	--AS

			SET @JourneyId = 1960100
			SET @JourneyPlannedStartTime = N'7/24/2014 6:58:00 AM'
			SET @JourneyPlannedEndTime = N'7/24/2014 8:05:00 AM'
			SET @StopId = 9025200000028315
			SET @ActualArrival = N'7/24/2014 10:32:59 AM'
			SET @ActualDeparture = N'7/24/2014 10:33:03 AM'
			SET @IsLastStop = 0
			SET @ExternalReference = N'179'
			SET @JourneyStops = N'<JourneyStops><StopInfo StopNumber="9025200000007217"><StopName>Buddinge St.</StopName><ArrivalTime>2014-07-24T06:58:00</ArrivalTime><DepartureTime>2014-07-24T06:58:00</DepartureTime><GPSCoordinateNS>55.7469525444862</GPSCoordinateNS><GPSCoordinateEW>12.4938884820151</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T06:58:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T06:59:43</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T06:59:43</ADDT_DepartureTime><LMDT_Time>2014-07-24T06:59:48</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000010"><StopName>Rådhuspassagen</StopName><ArrivalTime>2014-07-24T07:00:31</ArrivalTime><DepartureTime>2014-07-24T07:00:32</DepartureTime><GPSCoordinateNS>55.7439384917845</GPSCoordinateNS><GPSCoordinateEW>12.4939274282837</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T06:59:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:00:27</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:00:27</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:00:30</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001724"><StopName>Buddinge, Gladsaxe Rådhus</StopName><ArrivalTime>2014-07-24T07:02:27</ArrivalTime><DepartureTime>2014-07-24T07:02:27</DepartureTime><GPSCoordinateNS>55.7422875837883</GPSCoordinateNS><GPSCoordinateEW>12.4955343020235</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:00:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:01:56</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:01:56</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:01:58</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007218"><StopName>Buddinge Skole</StopName><ArrivalTime>2014-07-24T07:02:41</ArrivalTime><DepartureTime>2014-07-24T07:03:28</DepartureTime><GPSCoordinateNS>55.7409095443832</GPSCoordinateNS><GPSCoordinateEW>12.5017672138974</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:01:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:03:28</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:03:28</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:03:30</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000292"><StopName>Vandtårnsvej</StopName><ArrivalTime>2014-07-24T07:04:00</ArrivalTime><DepartureTime>2014-07-24T07:04:42</DepartureTime><GPSCoordinateNS>55.7386997356173</GPSCoordinateNS><GPSCoordinateEW>12.5062004016556</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:02:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:04:42</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:04:42</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:04:44</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007219"><StopName>Søborg Torv</StopName><ArrivalTime>2014-07-24T07:05:06</ArrivalTime><DepartureTime>2014-07-24T07:05:12</DepartureTime><GPSCoordinateNS>55.7372953096367</GPSCoordinateNS><GPSCoordinateEW>12.5086967784467</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:03:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:05:38</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:05:38</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:06:12</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007113"><StopName>Søborg Skole</StopName><ArrivalTime>2014-07-24T07:06:01</ArrivalTime><DepartureTime>2014-07-24T07:06:11</DepartureTime><GPSCoordinateNS>55.7348659956117</GPSCoordinateNS><GPSCoordinateEW>12.5131843443555</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:04:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:06:41</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:06:41</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:06:46</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007114"><StopName>Erik Bøghs Allé</StopName><ArrivalTime>2014-07-24T07:06:38</ArrivalTime><DepartureTime>2014-07-24T07:07:42</DepartureTime><GPSCoordinateNS>55.7337867310104</GPSCoordinateNS><GPSCoordinateEW>12.5153072823998</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:05:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:07:42</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:07:42</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:07:48</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007115"><StopName>Dyssegårdsvej</StopName><ArrivalTime>2014-07-24T07:08:23</ArrivalTime><DepartureTime>2014-07-24T07:09:14</DepartureTime><GPSCoordinateNS>55.7310404406282</GPSCoordinateNS><GPSCoordinateEW>12.5202789102939</GPSCoordinateEW><Zone>31</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:06:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:09:14</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:09:14</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:09:31</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000335"><StopName>Gladsaxevej</StopName><ArrivalTime>2014-07-24T07:10:14</ArrivalTime><DepartureTime>2014-07-24T07:10:26</DepartureTime><GPSCoordinateNS>55.726873046084</GPSCoordinateNS><GPSCoordinateEW>12.5252707918253</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:08:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:10:17</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:10:17</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:10:20</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000336"><StopName>Emdrup Torv</StopName><ArrivalTime>2014-07-24T07:11:22</ArrivalTime><DepartureTime>2014-07-24T07:11:15</DepartureTime><GPSCoordinateNS>55.722721857278</GPSCoordinateNS><GPSCoordinateEW>12.5293478536993</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:10:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:11:30</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:11:30</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:11:53</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000337"><StopName>Bispebjerg Parkalle</StopName><ArrivalTime>2014-07-24T07:11:57</ArrivalTime><DepartureTime>2014-07-24T07:12:18</DepartureTime><GPSCoordinateNS>55.7199339596117</GPSCoordinateNS><GPSCoordinateEW>12.5315370922136</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:11:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:12:23</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:12:23</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000338"><StopName>Bispebjerg Torv</StopName><ArrivalTime>2014-07-24T07:13:15</ArrivalTime><DepartureTime>2014-07-24T07:13:36</DepartureTime><GPSCoordinateNS>55.7159246371222</GPSCoordinateNS><GPSCoordinateEW>12.5319700596418</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:12:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:13:13</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:13:13</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007039"><StopName>Tuborgvej</StopName><ArrivalTime>2014-07-24T07:14:25</ArrivalTime><DepartureTime>2014-07-24T07:14:55</DepartureTime><GPSCoordinateNS>55.7128404113858</GPSCoordinateNS><GPSCoordinateEW>12.5356445722089</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:14:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:14:55</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:14:55</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:14:59</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007040"><StopName>Bispebjerg Hospital</StopName><ArrivalTime>2014-07-24T07:15:40</ArrivalTime><DepartureTime>2014-07-24T07:15:55</DepartureTime><GPSCoordinateNS>55.7100965092474</GPSCoordinateNS><GPSCoordinateEW>12.5380996034329</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:15:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:15:55</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:15:55</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:16:01</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007041"><StopName>Lygten</StopName><ArrivalTime>2014-07-24T07:16:30</ArrivalTime><DepartureTime>2014-07-24T07:17:16</DepartureTime><GPSCoordinateNS>55.7079733394967</GPSCoordinateNS><GPSCoordinateEW>12.5400777999363</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:16:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:17:16</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:17:16</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:17:18</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002770"><StopName>Bispebjerg St.</StopName><ArrivalTime>2014-07-24T07:17:20</ArrivalTime><DepartureTime>2014-07-24T07:17:50</DepartureTime><GPSCoordinateNS>55.7068446858672</GPSCoordinateNS><GPSCoordinateEW>12.5415302095699</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:17:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:18:02</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:18:02</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007042"><StopName>Rovsingsgade</StopName><ArrivalTime>2014-07-24T07:18:34</ArrivalTime><DepartureTime>2014-07-24T07:18:55</DepartureTime><GPSCoordinateNS>55.7046493469917</GPSCoordinateNS><GPSCoordinateEW>12.5454561853864</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:18:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:18:59</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:18:59</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007043"><StopName>Haraldsgade</StopName><ArrivalTime>2014-07-24T07:19:32</ArrivalTime><DepartureTime>2014-07-24T07:19:47</DepartureTime><GPSCoordinateNS>55.7028955052806</GPSCoordinateNS><GPSCoordinateEW>12.5484823253466</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:19:00</PlannedDepartureTime><AADT_ArrivalTime>2014-07-24T07:20:07</AADT_ArrivalTime><ADDT_DepartureTime>2014-07-24T07:20:07</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:21:26</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007044"><StopName>Ægirsgade</StopName><ArrivalTime>2014-07-24T07:20:18</ArrivalTime><DepartureTime>2014-07-24T07:20:35</DepartureTime><GPSCoordinateNS>55.7017001735538</GPSCoordinateNS><GPSCoordinateEW>12.5506193097941</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:20:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:17:18</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007045"><StopName>Jagtvej</StopName><ArrivalTime>2014-07-24T07:21:43</ArrivalTime><DepartureTime>2014-07-24T07:22:03</DepartureTime><GPSCoordinateNS>55.6989105929221</GPSCoordinateNS><GPSCoordinateEW>12.5547170130292</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:21:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:17:18</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007046"><StopName>Arresøgade</StopName><ArrivalTime>2014-07-24T07:22:29</ArrivalTime><DepartureTime>2014-07-24T07:22:42</DepartureTime><GPSCoordinateNS>55.69776298154</GPSCoordinateNS><GPSCoordinateEW>12.5574089698253</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:22:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:17:18</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000006017"><StopName>Nørre Campus</StopName><ArrivalTime>2014-07-24T07:23:47</ArrivalTime><DepartureTime>2014-07-24T07:23:24</DepartureTime><GPSCoordinateNS>55.6960317428531</GPSCoordinateNS><GPSCoordinateEW>12.5622532898703</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:23:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000006018"><StopName>Rigshospitalet Syd</StopName><ArrivalTime>2014-07-24T07:25:21</ArrivalTime><DepartureTime>2014-07-24T07:25:09</DepartureTime><GPSCoordinateNS>55.6932626900155</GPSCoordinateNS><GPSCoordinateEW>12.5668769749579</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:25:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007047"><StopName>Sortedam Dossering</StopName><ArrivalTime>2014-07-24T07:25:33</ArrivalTime><DepartureTime>2014-07-24T07:25:47</DepartureTime><GPSCoordinateNS>55.6918132235585</GPSCoordinateNS><GPSCoordinateEW>12.5695779191373</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:26:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000028274"><StopName>Sølvtorvet</StopName><ArrivalTime>2014-07-24T07:27:51</ArrivalTime><DepartureTime>2014-07-24T07:28:05</DepartureTime><GPSCoordinateNS>55.6890682354841</GPSCoordinateNS><GPSCoordinateEW>12.5751242886434</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:28:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:09:23</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000006020"><StopName>Statens Museum for Kunst</StopName><ArrivalTime>2014-07-24T07:28:47</ArrivalTime><DepartureTime>2014-07-24T07:28:58</DepartureTime><GPSCoordinateNS>55.6871337759689</GPSCoordinateNS><GPSCoordinateEW>12.5776513869554</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:29:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000006584"><StopName>Nørreport St.</StopName><ArrivalTime>2014-07-24T07:30:41</ArrivalTime><DepartureTime>2014-07-24T07:32:09</DepartureTime><GPSCoordinateNS>55.6840559065104</GPSCoordinateNS><GPSCoordinateEW>12.5726233490513</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:32:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001667"><StopName>Larslejsstræde</StopName><ArrivalTime>2014-07-24T07:33:42</ArrivalTime><DepartureTime>2014-07-24T07:33:53</DepartureTime><GPSCoordinateNS>55.680581522434</GPSCoordinateNS><GPSCoordinateEW>12.5678381675333</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:34:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001668"><StopName>Jarmers Plads</StopName><ArrivalTime>2014-07-24T07:34:57</ArrivalTime><DepartureTime>2014-07-24T07:35:07</DepartureTime><GPSCoordinateNS>55.678282810159</GPSCoordinateNS><GPSCoordinateEW>12.5665203122561</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:36:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001420"><StopName>Rådhuspladsen</StopName><ArrivalTime>2014-07-24T07:36:59</ArrivalTime><DepartureTime>2014-07-24T07:37:06</DepartureTime><GPSCoordinateNS>55.6765094498206</GPSCoordinateNS><GPSCoordinateEW>12.5675620773117</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:37:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001670"><StopName>Vesterport St.</StopName><ArrivalTime>2014-07-24T07:38:40</ArrivalTime><DepartureTime>2014-07-24T07:39:08</DepartureTime><GPSCoordinateNS>55.6758470231164</GPSCoordinateNS><GPSCoordinateEW>12.5636135462533</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:39:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000050501"><StopName>Vesterport St.</StopName><ArrivalTime>2014-07-24T07:38:40</ArrivalTime><DepartureTime>2014-07-24T07:39:08</DepartureTime><GPSCoordinateNS>55.6749925162928</GPSCoordinateNS><GPSCoordinateEW>12.5611104760839</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:40:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000050502"><StopName>Vesterbros Torv</StopName><ArrivalTime>2014-07-24T07:42:01</ArrivalTime><DepartureTime>2014-07-24T07:42:26</DepartureTime><GPSCoordinateNS>55.6726539409287</GPSCoordinateNS><GPSCoordinateEW>12.5546993663347</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:41:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001529"><StopName>Frederiksberg Allé</StopName><ArrivalTime>2014-07-24T07:43:02</ArrivalTime><DepartureTime>2014-07-24T07:43:26</DepartureTime><GPSCoordinateNS>55.6725306872462</GPSCoordinateNS><GPSCoordinateEW>12.5494749811478</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:42:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001530"><StopName>Enghavevej</StopName><ArrivalTime>2014-07-24T07:44:35</ArrivalTime><DepartureTime>2014-07-24T07:44:57</DepartureTime><GPSCoordinateNS>55.6718126592654</GPSCoordinateNS><GPSCoordinateEW>12.5465275420703</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:43:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:11:53</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001531"><StopName>Platanvej</StopName><ArrivalTime>2014-07-24T07:45:16</ArrivalTime><DepartureTime>2014-07-24T07:45:36</DepartureTime><GPSCoordinateNS>55.6702627322851</GPSCoordinateNS><GPSCoordinateEW>12.5387922749376</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:44:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000001532"><StopName>Barfods Skole</StopName><ArrivalTime>2014-07-24T07:46:04</ArrivalTime><DepartureTime>2014-07-24T07:46:33</DepartureTime><GPSCoordinateNS>55.6706501952867</GPSCoordinateNS><GPSCoordinateEW>12.533569826438</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:45:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002307"><StopName>De Små Haver</StopName><ArrivalTime>2014-07-24T07:47:15</ArrivalTime><DepartureTime>2014-07-24T07:47:32</DepartureTime><GPSCoordinateNS>55.670921219622</GPSCoordinateNS><GPSCoordinateEW>12.5300044881863</GPSCoordinateEW><Zone>1</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:46:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002308"><StopName>Zoologisk Have</StopName><ArrivalTime>2014-07-24T07:48:29</ArrivalTime><DepartureTime>2014-07-24T07:48:35</DepartureTime><GPSCoordinateNS>55.6715626857672</GPSCoordinateNS><GPSCoordinateEW>12.5224508169285</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:48:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000028666"><StopName>Søndre Fasanvej</StopName><ArrivalTime>2014-07-24T07:49:20</ArrivalTime><DepartureTime>2014-07-24T07:49:36</DepartureTime><GPSCoordinateNS>55.6720112243589</GPSCoordinateNS><GPSCoordinateEW>12.5176795223403</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:48:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002310"><StopName>Solbjerg Kirkegård</StopName><ArrivalTime>2014-07-24T07:50:46</ArrivalTime><DepartureTime>2014-07-24T07:50:51</DepartureTime><GPSCoordinateNS>55.6721721761359</GPSCoordinateNS><GPSCoordinateEW>12.5099390511922</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:49:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:16:01</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002311"><StopName>Dalgas Boulevard</StopName><ArrivalTime>2014-07-24T07:51:08</ArrivalTime><DepartureTime>2014-07-24T07:51:50</DepartureTime><GPSCoordinateNS>55.6722810158663</GPSCoordinateNS><GPSCoordinateEW>12.5057983957114</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:50:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:19:27</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000004495"><StopName>Domus Vista</StopName><ArrivalTime>2014-07-24T07:52:33</ArrivalTime><DepartureTime>2014-07-24T07:52:50</DepartureTime><GPSCoordinateNS>55.6723767309398</GPSCoordinateNS><GPSCoordinateEW>12.5002230153491</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:51:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000004496"><StopName>Skellet</StopName><ArrivalTime>2014-07-24T07:53:30</ArrivalTime><DepartureTime>2014-07-24T07:53:38</DepartureTime><GPSCoordinateNS>55.6724839522657</GPSCoordinateNS><GPSCoordinateEW>12.4947713165411</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:52:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000029655"><StopName>Ålholm St.</StopName><ArrivalTime>2014-07-24T07:53:54</ArrivalTime><DepartureTime>2014-07-24T07:54:34</DepartureTime><GPSCoordinateNS>55.6725243391861</GPSCoordinateNS><GPSCoordinateEW>12.4931331158554</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:52:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:16:01</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000007965"><StopName>Ålholm Plads</StopName><ArrivalTime>2014-07-24T07:54:57</ArrivalTime><DepartureTime>2014-07-24T07:55:15</DepartureTime><GPSCoordinateNS>55.6726190976313</GPSCoordinateNS><GPSCoordinateEW>12.4879661938917</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:53:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000002088"><StopName>Peter Bangs Vej</StopName><ArrivalTime>2014-07-24T07:55:36</ArrivalTime><DepartureTime>2014-07-24T07:55:41</DepartureTime><GPSCoordinateNS>55.6726793020495</GPSCoordinateNS><GPSCoordinateEW>12.483973083964</GPSCoordinateEW><Zone>2</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:54:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000004514"><StopName>Damhustorvet</StopName><ArrivalTime>2014-07-24T07:57:20</ArrivalTime><DepartureTime>2014-07-24T07:57:37</DepartureTime><GPSCoordinateNS>55.6725927194957</GPSCoordinateNS><GPSCoordinateEW>12.4721079708576</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:56:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:13:16</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000004136"><StopName>Randrupvej</StopName><ArrivalTime>2014-07-24T07:57:52</ArrivalTime><DepartureTime>2014-07-24T07:58:00</DepartureTime><GPSCoordinateNS>55.6721502946088</GPSCoordinateNS><GPSCoordinateEW>12.4670635539639</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:57:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000004137"><StopName>Ørbygård</StopName><ArrivalTime>2014-07-24T07:59:04</ArrivalTime><DepartureTime>2014-07-24T07:59:11</DepartureTime><GPSCoordinateNS>55.6717709194545</GPSCoordinateNS><GPSCoordinateEW>12.4627243978183</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:57:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000432"><StopName>Roskildevej</StopName><ArrivalTime>2014-07-24T08:00:23</ArrivalTime><DepartureTime>2014-07-24T08:00:37</DepartureTime><GPSCoordinateNS>55.6716347722622</GPSCoordinateNS><GPSCoordinateEW>12.4563736840967</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T07:59:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:12:28</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000433"><StopName>Valhøjs Allé</StopName><ArrivalTime>2014-07-24T08:00:41</ArrivalTime><DepartureTime>2014-07-24T08:00:46</DepartureTime><GPSCoordinateNS>55.6737425227065</GPSCoordinateNS><GPSCoordinateEW>12.4558030055563</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T08:00:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000434"><StopName>Rødager Allé</StopName><ArrivalTime>2014-07-24T08:01:28</ArrivalTime><DepartureTime>2014-07-24T08:01:34</DepartureTime><GPSCoordinateNS>55.6765281609228</GPSCoordinateNS><GPSCoordinateEW>12.4550971693989</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T08:01:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000000435"><StopName>Rødovre Centrum</StopName><ArrivalTime>2014-07-24T08:02:26</ArrivalTime><DepartureTime>2014-07-24T08:02:49</DepartureTime><GPSCoordinateNS>55.6794165975342</GPSCoordinateNS><GPSCoordinateEW>12.4542209710576</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T08:02:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000028314"><StopName>Tårnvej</StopName><ArrivalTime>2014-07-24T08:03:49</ArrivalTime><DepartureTime>2014-07-24T08:03:56</DepartureTime><GPSCoordinateNS>55.6806825659245</GPSCoordinateNS><GPSCoordinateEW>12.4516030149699</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>false</IsCheckpoint><PlannedDepartureTime>2014-07-24T08:03:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:18:04</LMDT_Time></StopInfo><StopInfo StopNumber="9025200000028387"><StopName>Rødovrehallen</StopName><ArrivalTime>2014-07-24T08:05:02</ArrivalTime><DepartureTime>2014-07-24T08:05:52</DepartureTime><GPSCoordinateNS>55.6809360168927</GPSCoordinateNS><GPSCoordinateEW>12.444811615197</GPSCoordinateEW><Zone>32</Zone><IsCheckpoint>true</IsCheckpoint><PlannedDepartureTime>2014-07-24T08:05:00</PlannedDepartureTime><AADT_ArrivalTime>0001-01-01T00:00:00</AADT_ArrivalTime><ADDT_DepartureTime>0001-01-01T00:00:00</ADDT_DepartureTime><LMDT_Time>2014-07-24T07:21:26</LMDT_Time></StopInfo></JourneyStops>'
			SET @StopData = N'<Stop><StopId>9025200000028315</StopId><PlanDifference>-16</PlanDifference><JourneyNumber>1960100</JourneyNumber><Status>LATE</Status><BusNumber>1501</BusNumber><StopName></StopName><ActualArrival>2014-07-24T10:32:59</ActualArrival><ActualDeparture>2014-07-24T10:33:03</ActualDeparture><ScheduleNumber>915</ScheduleNumber><PlannedArrival>2014-07-24T10:46:00</PlannedArrival><PlannedDeparture>2014-07-24T10:46:00</PlannedDeparture><FromName>Rødovrehallen</FromName><ViaName></ViaName><IsLastStop>false</IsLastStop><StopSequence>2</StopSequence><ExternalReference>179</ExternalReference></Stop>'
	*/

BEGIN
    --SET @logEnabled=0
	DECLARE @StopDataXml xml
	SET @StopDataXml = @StopData	

	DECLARE @RouteDirectionId int
	DECLARE @ScheduleId int 
	DECLARE @StopSequence int

	DECLARE @JourneyAhead int
	DECLARE @JourneyBehind int

	DECLARE @ClientRef varchar(50)
	DECLARE @CustomerId int
	DECLARE @BusNumber varchar(50)

	DECLARE @StopId decimal(19,0)
	DECLARE @ExternalReference varchar(100)
	
	DECLARE @StartTime datetime
								
	DECLARE @JourneyLog TABLE( [JourneyId] [int] NOT NULL,[Timestamp] [datetime] NOT NULL,[Description] [text] NULL)

	SELECT @StopSequence = StopInfo.data.value('(StopSequence/text())[1]', 'int'),		  
			@ClientRef = StopInfo.data.value('(ClientRef/text())[1]', 'varchar(50)'),
			@ScheduleId = StopInfo.data.value('(ScheduleNumber/text())[1]', 'int'),
			@StopId = StopInfo.data.value('(StopId/text())[1]', 'decimal'),
			@CustomerId = StopInfo.data.value('(CustomerId/text())[1]', 'int'),
			@BusNumber = StopInfo.data.value('(BusNumber/text())[1]', 'varchar(50)'),		
			@ExternalReference = StopInfo.data.value('(ExternalReference/text())[1]', 'varchar(100)'),
			@StartTime = StopInfo.data.value('(StartTime/text())[1]', 'datetime')
			FROM @StopDataXml.nodes('/Stop') AS StopInfo(data)  		 

	print 'Schedule Number = ' + Cast(@ScheduleId as VARCHAR(max))	 	
	print 'Stop Sequence = ' + Cast(@StopSequence as VARCHAR(max))	 
	print 'Customer Id = ' + Cast(@CustomerId as VARCHAR(max))	 
	print 'Client Ref = ' + @ClientRef	 

	SET @RouteDirectionId = (Select TOP 1 s.RouteDirectionId From Schedules s WHERE s.ScheduleId = @ScheduleId) 
	--print 'RouteDirectionId = ' + Cast(@RouteDirectionid as VARCHAR(max))
	print '@RouteDirectionId = ' + CAST(@RouteDirectionId as varchar(max)) 	 
	DECLARE @tmpJourneys Table (JourneyId int, StartTime datetime, Active bit, ScheduleId int, BusNumber varchar(50), ClientRef varchar(100), PlannedStartTime datetime)

	;WITH journey_CTE (JourneyId, StartTime, Active, ScheduleId, BusNumber, ClientRef, PlannedStartTime)
	AS
	(
		 Select j.JourneyId, j.StartTime, j.Active, j.ScheduleId, j.BusNumber, j.ClientRef, j.PlannedStartTime from Journeys j 
		 where j.Active=1 -- AND j.PlannedStartTime between GETDATE()-1 and GETDATE()
	)

	INSERT INTO @tmpJourneys (JourneyId, StartTime, Active, ScheduleId, BusNumber, ClientRef, PlannedStartTime)
	SELECT JourneyId, StartTime, Active, ScheduleId, BusNumber, ClientRef, PlannedStartTime FROM journey_CTE
	
	SELECT Top 1 @JourneyId = JourneyId FROM @tmpJourneys WHERE JourneyId = @JourneyId OR ClientRef = @ClientRef	
	-- IF JOURNEY doesn't exists
		IF @JourneyId=0
		BEGIN
		
			IF @logEnabled = 1
			 BEGIN
				INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] -  JourneyId = 0')
			 END
			print '@JourneyId=0'
				--EXEC xml_GetActiveJourney  @ScheduleId, @CustomerId, @BusNumber, @JourneyPlannedStartTime, @JourneyPlannedEndTime, @ClientRef
				INSERT INTO [Journeys]
				   ([BusNumber]
				   ,[ScheduleId]
				   ,[CustomerId]
				   ,[PlannedStartTime]
				   ,[PlannedEndTime]
				   ,[StartTime]
				   ,[EndTime]
				   ,[Active]
				   ,[ExternalReference]
				   ,[ClientRef])
			 VALUES
				   (@BusNumber
				   ,@ScheduleId
				   ,@CustomerId
				   ,@JourneyPlannedStartTime
				   ,@JourneyPlannedEndTime
				   ,@StartTime
				   ,null
				   ,1
				   ,@ExternalReference
				   ,@ClientRef)
			print '[Journeys] inert'
		 
			SET @JourneyId = SCOPE_IDENTITY()
			IF @logEnabled = 1 BEGIN
				INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - New Journey Created with JourneyId: ' + CAST(ISNULL(@JourneyId, '') as Varchar(MAX)))
			END

			--print '@JourneyId ' + @JourneyId
			INSERT INTO JourneyStops
				SELECT @JourneyId,
					   ss.StopSequence,
					   ss.StopId,
					   ss.StopName,
					   ss.StopGPS,
					   ss.Zone,
					   null,
					   null,
					   null,
					   null,
					   null,
					   null		   
					FROM ScheduleStops ss WHERE ScheduleId = @ScheduleId 
				print 'JourneyStops Inserted ' + CAST(@JourneyId as varchar(max)) 	  
		END

	--	SELECT Top 1 @JourneyId = JourneyId FROM @tmpJourneys WHERE JourneyId = @JourneyId OR ClientRef = @ClientRef	
		--------------------------------------------------------------------------------------------------
		print 'SELECT Top 1 @JourneyId ' + CAST(@JourneyId as varchar(max))
		
		DECLARE @Active bit

		-- check if actualdeparture time passed, set the JourneyStops table and return
		Update JourneyStops
			SET ActualDepartureTime = @ActualDeparture,
			PlannedArrivalTime = @PlannedArrivalTime,
			PlannedDepartureTime = @PlannedDepartureTime
			WHERE JourneyId = @JourneyId AND StopId = @StopId AND StopSequence = @StopSequence AND ActualArrivalTime IS NOT NULL

			IF @logEnabled = 1 BEGIN
				INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - Stop Updated, StopId: ' + CAST(ISNULL(@StopId, '') as Varchar(MAX))  + ' ,StopSequence: ' + CAST(ISNULL(@StopSequence, '') as Varchar(MAX)) + ' ,ActualArrival: ' + Replace(CAST(ISNULL(@ActualArrival, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','')  + ' ,ActualDeparture: ' + Replace(CAST(ISNULL(@ActualDeparture, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','')  + ' ,PlannedArrivalTime: ' + Replace(CAST(ISNULL(@PlannedArrivalTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' ,PlannedDepartureTime: ' + Replace(CAST(ISNULL(@PlannedDepartureTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' , ClientRef: ' + IsNull(@ClientRef, '') + ' for JourneyId: ' + CAST(ISNULL(@JourneyId, '') as Varchar(MAX)))
			END
		
		print 'ActualDepartureTime updated RowCount = ' + Cast(@@ROWCOUNT as varchar(10))

		IF @@ROWCOUNT>0
			return; 

		IF(@IsLastStop = 1)
			SET @Active = 0
		ELSE		
			SET @Active = 1
		 
		print 'Active = ' + Cast(@Active as varchar(10))
		 
		/**** CODE OBSOLETE due to implementation of <StartTime> node in StopData xml
		DECLARE @JourneyStartTime datetime
		SET @JourneyStartTime = (SELECT StartTime FROM Journeys WHERE JourneyId = @JourneyId)
		 
		IF @JourneyStartTime IS NULL
			SET @JourneyStartTime = @StartTime --GETDATE()
		*/
		 
		--UPDATE Journeys
		UPDATE Journeys
			SET 
				StartTime = @StartTime, -- @JourneyStartTime,
				PlannedStartTime =  Case  WHEN @JourneyPlannedStartTime IS NULL THEN (Select TOP 1 PlannedStartTime from Journeys Where JourneyId = @JourneyId) ElSE @JourneyPlannedStartTime END, --@JourneyPlannedStartTime,  
				PlannedEndTime = Case  WHEN @JourneyPlannedEndTime IS NULL THEN (Select TOP 1 PlannedEndTime from Journeys Where JourneyId = @JourneyId) ElSE @JourneyPlannedEndTime END, --@JourneyPlannedEndTime,
				Active = 1,
				LastUpdated = GETDATE(),
				BusNumber = @BusNumber,
				ClientRef = @ClientRef,
				--ExternalReference = @ExternalReference,
				EndTime = Case @Active WHEN 1 THEN null WHEN 0 THEN GETDATE() END
			WHERE JourneyId = @JourneyId 
		print 'Update Journeys RowCount = ' + Cast(@@ROWCOUNT as varchar(10))
		IF @logEnabled = 1 BEGIN
			INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - Journey Updated, StartTime: ' + Replace(CAST(ISNULL(@StartTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','')  + ' ,PlannedStartTime: ' + Replace(CAST(ISNULL(@JourneyPlannedStartTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','')  + ' ,PlannedEndTime: ' +  Replace(CAST(ISNULL(@JourneyPlannedEndTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','')  + ' ,BusNumber: ' + CAST(ISNULL(@BusNumber, '') as Varchar(MAX)) + ' for JourneyId: ' + CAST(ISNULL(@JourneyId, '') as Varchar(MAX)))
		END
		
		DECLARE @AheadBusNumber varchar(20)
		DECLARE @BehindBusNumber varchar(20)
	 
	 	----Get Ahead Journey
		--SELECT TOP 1
		--	@JourneyAhead =  j.JourneyId,	
		--	@AheadBusNumber = j.BusNumber
		--FROM
		--	[JourneyStops] js
		--	LEFT JOIN	
		--	@tmpJourneys j
		--	ON j.JourneyId=js.JourneyId
		--	INNER JOIN
		--	 [Schedules] s
		--	ON j.ScheduleId=s.ScheduleId 
		--WHERE s.RouteDirectionId=@RouteDirectionId
		--	AND j.JourneyId != @JourneyId
		--AND j.Active=1  -- it may raise some problems if ahead bus has reached last stop ... (we won't get ahead journey for last 2,3 stops)
		--	AND js.StopSequence = @StopSequence -- we need to get rid of this ... bcoz same route direction may have different schedules.
		--	AND js.StopId = @StopId
		--	AND js.ActualArrivalTime IS NOT NULL	
		--	AND j.BusNumber <> @BusNumber
		--ORDER BY js.ActualArrivalTime DESC

		--Get Ahead Journey
		 
		SELECT TOP 1
			@JourneyAhead =  j.JourneyId,	
			@AheadBusNumber = j.BusNumber
		FROM
			[JourneyStops] js
			LEFT JOIN	
			@tmpJourneys j
			ON j.JourneyId=js.JourneyId
		WHERE j.ScheduleId IN (select ScheduleId from Schedules where RouteDirectionId = @RouteDirectionId)
			AND j.JourneyId != @JourneyId
			AND j.Active=1  -- it may raise some problems if ahead bus has reached last stop ... (we won't get ahead journey for last 2,3 stops)
			AND js.StopSequence = @StopSequence -- we need to get rid of this ... bcoz same route direction may have different schedules.
			AND js.StopId = @StopId
			AND js.ActualArrivalTime IS NOT NULL	
			AND j.BusNumber <> @BusNumber
		ORDER BY js.ActualArrivalTime DESC	
		print 'Journey Ahead: ' + CAST(@JourneyAhead as varchar(max))
	 
		--IF @JourneyAhead != NULL
		--BEGIN
			--Update JourneyStops
			Update JourneyStops 
				Set ActualArrivalTime = @ActualArrival,
					ActualDepartureTime = @ActualDeparture,
					JourneyAhead = @JourneyAhead,
					PlannedArrivalTime = @PlannedArrivalTime,
					PlannedDepartureTime = @PlannedDepartureTime			
				WHERE JourneyId = @JourneyId AND StopSequence = @StopSequence AND StopId = @StopId
			print 'Update JourneysStops RowCount = ' + Cast(@@ROWCOUNT as varchar(10))
			IF @logEnabled = 1 BEGIN
				INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - Stop and AheadJourney Updated, StopId: ' + CAST(ISNULL(@StopId, '') as Varchar(MAX)) + ' ,StopSequence: ' + CAST(ISNULL(@StopSequence, '') as Varchar(MAX)) + ' ,ActualArrival: ' + Replace(CAST(ISNULL(@ActualArrival, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' ,ActualDeparture: ' + Replace(CAST(ISNULL(@ActualDeparture, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' ,PlannedArrivalTime: ' + Replace(CAST(ISNULL(@PlannedArrivalTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' ,PlannedDepartureTime: ' + Replace(CAST(ISNULL(@PlannedDepartureTime, '') as Varchar(MAX)),'Jan  1 1900 12:00AM','') + ' and Ahead JourneyId: ' + CAST(ISNULL(@JourneyAhead, '') as Varchar(MAX))+ ' for JourneyId: ' + CAST(ISNULL(@JourneyId, '') as Varchar(MAX)))
			END

			--Update Ahead Journey
			UPDATE JourneyStops 
				SET JourneyBehind = @JourneyId
				WHERE JourneyId = @JourneyAhead AND StopSequence = @StopSequence AND StopId = @StopId 

			IF @logEnabled = 1 BEGIN
				INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - Updating JourneyBehind, StopId: ' + CAST(ISNULL(@StopId, '') as Varchar(MAX)) + ' ,StopSequence: ' + CAST(ISNULL(@StopSequence, '') as Varchar(MAX)) + ' ,JourneyBehind: ' + CAST(ISNULL(@JourneyId, '') as Varchar(MAX))  + ' for JourneyId: ' + CAST(ISNULL(@JourneyAhead, '') as Varchar(MAX)))
			END

			IF(@IsLastStop = 1)
			BEGIN
				UPDATE Journeys SET Active = 0 --WHERE JourneyId = @JourneyAhead 
				WHERE 
				JourneyId != @JourneyId
				AND Active = 1  -- it may raise some problems if ahead bus has reached last stop ... (we won't get ahead journey for last 2,3 stops)
				AND ScheduleId IN (SELECT ScheduleId FROM Schedules WHERE RouteDirectionId = @RouteDirectionId)
				AND EndTime IS NOT NULL
				AND DATEADD(MINUTE,10,LastUpdated) < GETDATE()
				
				IF @logEnabled = 1 BEGIN
					INSERT INTO @JourneyLog VALUES(@JourneyId,GETDATE(),'@[SaveActiveJourney] - Updating Journeys for last stop - setting Active to 0 for JourneyId: ' + CAST(ISNULL(@JourneyAhead, '') as Varchar(MAX))  + ', Updated row count: ' + CAST(ISNULL(@@ROWCOUNT, '') as Varchar(MAX)))
				END

				
			END
		--END
	--todo: Make entry in predictionstable for estimated timestamps from XML in parameter ()
	--select '<Data><JourneyId>' + CAST(@JourneyId AS VARCHAR(20)) + '</JourneyId></Data>'

	print 'JourneyStops Updated'

	IF @logEnabled = 1 BEGIN
		INSERT INTO SystemJourneyLog([JourneyId],[Timestamp],[Description])
		SELECT [JourneyId],[Timestamp],[Description] FROM @JourneyLog
	END

	--@AheadBusNumber
	DECLARE @AheadXML xml
	DECLARE @BehindXML xml
	DECLARE @resultXML xml

	SET @AheadXML = (SELECT @AheadBusNumber AS BusNumber, js.JourneyId AS JourneyNumber, js.StopName, js.StopId, js.StopSequence, js.ActualArrivalTime AS ActualArrival, js.ActualDepartureTime AS ActualDeparture
	FROM JourneyStops js  
	   WHERE js.JourneyId = @JourneyAhead AND js.StopId = @StopId AND js.StopSequence = @StopSequence
	--FOR XML RAW ('Ahead'), ELEMENTS)
	FOR XML PATH('Stop') , ROOT('Ahead'))

	SELECT TOP 1 @JourneyBehind = js.JourneyBehind FROM JourneyStops js  WHERE js.JourneyId = @JourneyId AND js.JourneyBehind is not null ORDER BY js.ActualArrivalTime DESC

	SET @BehindXML = (SELECT TOP 1 jb.BusNumber AS BusNumber, jb.JourneyId AS JourneyNumber, js.StopName, js.StopId, js.StopSequence, js.ActualArrivalTime AS ActualArrival, js.ActualDepartureTime AS ActualDeparture
	FROM JourneyStops js  inner join @tmpJourneys jb ON jb.JourneyId = js.JourneyBehind  
	   WHERE js.JourneyId = @JourneyBehind AND js.JourneyBehind is not null ORDER BY ActualArrival DESC
	--FOR XML RAW ('Behind'), ELEMENTS)
	FOR XML PATH('Stop') , ROOT('Behind'))

	SET @resultXML =  CAST(ISNULL(@AheadXML, '<Ahead></Ahead>') as Varchar(max)) + CAST(ISNULL(@BehindXML, '<Behind></Behind>') as Varchar(max))

	SELECT @resultXML FOR XML RAW ('Data')
	
	/*
	select '<Data>
				<Ahead>
					<BusNumber><BusNumber>
					<JourneyNumber></JourneyNumber>
					<StopName></StopName>
					<ActualArrival<</ActualArrival>
					<ActualDeparture></ActualDeparture>
					<PlanDifference></PlanDifference>
					<Status></Status>
					<ScheduleNumber></ScheduleNumber>
					<FromName></FromName>
					<ViaName></ViaName>
					<IsLastStop></IsLastStop>
				</Ahead>
				<Behind>
					<BusNumber><BusNumber>
					<JourneyNumber></JourneyNumber>
					<StopName></StopName>
					<ActualArrival<</ActualArrival>
					<ActualDeparture></ActualDeparture>
					<PlanDifference></PlanDifference>
					<Status></Status>
					<ScheduleNumber></ScheduleNumber>
					<FromName></FromName>
					<ViaName></ViaName>
					<IsLastStop></IsLastStop>
				</Behind>
			</Data>'
	*/
END

