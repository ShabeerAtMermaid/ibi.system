﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace IBI.Web.VDV.Handlers
{
    public class DataReadyPingHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        #endregion
        //public string JourneyId { get; set; }
         
        public void ProcessRequest(HttpContext context)
        {

            //string sender = string.Empty;

            string url = context.Request.RawUrl; //http://ibi.mermaid.dk/vdv/guldborgsund/SmartVMS/sisref/dataready.xml
            string response = string.Empty;
            //sender = url.Substring("vdv/"

            //string response = string.Empty;


            StringBuilder log = new StringBuilder();
            var postData = new System.IO.StreamReader(context.Request.InputStream).ReadToEnd();

            

            string[] validSenders = System.Configuration.ConfigurationManager.AppSettings["Senders"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim().ToLower()).ToArray();
            string sender;
            bool isValidSender = false;
            foreach (string validSender in validSenders)
            {
               
                //if (sender.ToLower() == validSender)
                if(url.ToLower().Contains(string.Format("/{0}/", validSender.ToLower())))
                {
                    Logger.Log(string.Format("Call received for Sender \"{0}\". Posted Data: {1} ", validSender, postData));

                    isValidSender = true;
                    response = ProcessRequest(context, validSender);
                    break;
                }
            }

            if (!isValidSender)
            {
                Logger.Log(string.Format("No response sent back to {0}", url));
                response = string.Empty;
            }

            //string mimeType = "text/xml";
            //return this.Content(response, mimeType, Encoding.Default);

            SendResponse(context, response);

        }


        #region "Private Helper"

        private void SendResponse(HttpContext context, string data)
        {
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            //sb.Append(GetDestinationTree(int.Parse(customerID)));
            sb.Append(data);

            Byte[] fileContent = Encoding.UTF8.GetBytes(sb.ToString());

            //Clear all content output from the buffer stream 
            context.Response.Clear();

            //Add a HTTP header to the output stream that specifies the filename 
            //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

            //Add a HTTP header to the output stream that contains the content length(File Size)
            context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

            //Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "text/xml";

            //Write the data out to the client. 
            context.Response.BinaryWrite(fileContent);
        }

        private string ProcessRequest(HttpContext context, string sender)
        {

            //Logger.Log(string.Format("Call received for Sender \"{0}\"", sender));

            try
            {

                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><DataReadyAnswer><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></DataReadyAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")));
                // sb.Append(string.Format("<DataReadyAnswer><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></DataReadyAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")));

                //IBI.Shared.MSMQ.VDVQueueManager queue = new Shared.MSMQ.VDVQueueManager(sender);
                //queue.AppendToQueue(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));



                System.IO.File.WriteAllText(Path.Combine(context.Server.MapPath("~"), string.Format(ConfigurationManager.AppSettings["DataReadyStatusFile"].ToString().Replace("{SENDER}", "{0}"), sender)),
                        DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                        Encoding.Default);

                return sb.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);

            }

            return string.Empty;
        }

        #endregion


    }
}