﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class Bus
    {
        public Bus() {
            
            //setting some defulat properties.

            LastDCUPing = DateTime.MinValue;
            LastHotspotPing = DateTime.MinValue;
            LastInfotainmentPing = DateTime.MinValue;
            LastScheduleTime = DateTime.MinValue;
            LastVTCPing = DateTime.MinValue;
        }

        [XmlAttribute]
        [DataMember(Order = 0)]
        public int BusNumber
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(Order = 1)]
        public Boolean IsOK
        {
            get;
            set;
        }

        [DataMember(Order = 0)]
        public Boolean InOperation
        {
            get;
            set;
        }

        [DataMember(Order = 1)]
        public Boolean IsOnline
        {
            get;
            set;
        }

        [DataMember(Order = 2)]
        public long ScheduledJourney
        {
            get;
            set;
        }

        [DataMember(Order = 3)]
        public long CurrentJourney
        {
            get;
            set;
        }

        [DataMember(Order = 4)]
        public Nullable<DateTime> LastScheduleTime
        {
            get;
            set;
        }

        [DataMember(Order = 5)]
        public Nullable<DateTime> LastDCUPing
        {
            get;
            set;
        }

        [DataMember(Order = 6)]
        public Nullable<DateTime> LastVTCPing
        {
            get;
            set;
        }

        [DataMember(Order = 7)]
        public Nullable<DateTime> LastInfotainmentPing
        {
            get;
            set;
        }

        [DataMember(Order = 8)]
        public Nullable<DateTime> LastHotspotPing
        {
            get;
            set;
        }

        [DataMember(Order = 9)]
        public String Latitude
        {
            get;
            set;
        }

        [DataMember(Order = 10)]
        public String Longitude
        {
            get;
            set;
        }

        [DataMember(Order = 11)]
        public String Remarks
        {
            get;
            set;
        }

        [DataMember(Order = 12)]
        public Boolean MarkedForService
        {
            get;
            set;
        }

        [DataMember(Order = 13)]
        public int ServiceLevel
        {
            get;
            set;
        }

        public String InfotainmentMacAddress
        {
            get;
            set;
        }
        
    }
}