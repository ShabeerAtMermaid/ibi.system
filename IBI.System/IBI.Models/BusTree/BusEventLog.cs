﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace IBI.Models.BusTree
{
    public class BusEventLog
    {

        [DataMember]
        [ScaffoldColumn(true)]
        public int SQLIdentity { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public string BusNumber{ get; set; }

        [DataMember]
        public String ClientType { get; set; }
        
        [DataMember]
        public bool? IsMaster { get; set; }
        
        [DataMember]
        public bool? IsOverwritten { get; set; }
        
        [DataMember]
        public String Source { get; set; }

        [DataMember]
        public String Line { get; set; }
        
        [DataMember]
        public String From { get; set; }
        
        [DataMember]
        public String Destination { get; set; }
        
        [DataMember]
        public String JourneyNumber { get; set; }
        
        [DataMember]
        public Int64? StopNumber { get; set; }

        [DataMember]
        public int? Zone { get; set; }

        [DataMember]
        public String GPS1 { get; set; }

        [DataMember]
        public String GPS2 { get; set; }

        [DataMember]
        public String EventSource { get; set; }

        [DataMember]
        public String EventId { get; set; }

        [DataMember]
        public String EventData { get; set; }

        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public String ExtraData { get; set; }
    }
}
