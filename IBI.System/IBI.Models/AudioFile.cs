﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IBI.Models
{
    [DataContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public class AudioFile
    {
        [XmlAttribute]
        [DataMember(Order = 0)]
        public int Version
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(Order = 1)]
        public String File
        {
            get;
            set;
        }

        [XmlAttribute]
        [DataMember(IsRequired = true, Order = 2)]
        public String Hash
        {
            get;
            set;
        }
    }
}