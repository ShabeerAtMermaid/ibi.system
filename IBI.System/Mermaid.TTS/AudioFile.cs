﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mermaid.TTS
{
    public class AudioFile
    {
        #region Properties

        public String Text
        {
            get;
            set;
        }

        public String Voice
        {
            get;
            set;
        }

        public Boolean Approved
        {
            get;
            set;
        }

        public Boolean Rejected
        {
            get;
            set;
        }

        public DateTime LastModified
        {
            get;
            set;
        }

        public String FileName
        {
            get;
            set;
        }

        public Byte[] FileContent
        {
            get;
            set;
        }

        public int Version
        {
            get;
            set;
        }

        #endregion
    }
}
