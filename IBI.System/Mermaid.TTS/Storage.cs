﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;

namespace Mermaid.TTS
{
    public class Storage
    {
        #region Properties

        private Boolean DatabaseInitialized
        {
            get;
            set;
        }

        public String ConnectionString
        {
            get;
            set;
        }

        #endregion

        public Storage(String connectionString)
        {
            this.ConnectionString = connectionString;

            //TODO: Initialize database
            this.DatabaseInitialized = true;
        }

        private void InitializeDatabase()
        {
            if (!this.DatabaseInitialized)
            {
                using (SqlConnection tmpConnection = this.GetConnection())
                {
                    tmpConnection.Open();

                    StringBuilder tmpScript = new StringBuilder();

                    tmpScript.AppendLine("IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'AudioFiles')");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine("CREATE TABLE [AudioFiles] ([LastModified] datetime, [Text] varChar(255), [Voice] varChar(255), [FileName] varChar(255), [FileHash] varChar(55), [Approved] bit, [Rejected] bit, [Version] int, [FileContent] Image, SourceVoice varchar(50), SourceText varchar(1000))");
                    tmpScript.AppendLine("END");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SaveEntry' AND type = 'P')");
                    tmpScript.AppendLine("DROP PROCEDURE SaveEntry");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("IF EXISTS (SELECT name FROM sysobjects WHERE name = 'RetrieveEntry' AND type = 'P')");
                    tmpScript.AppendLine("DROP PROCEDURE RetrieveEntry");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("IF EXISTS (SELECT name FROM sysobjects WHERE name = 'SaveAudioEntry' AND type = 'P')");
                    tmpScript.AppendLine("DROP PROCEDURE SaveAudioEntry");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("IF EXISTS (SELECT name FROM sysobjects WHERE name = 'TTSData' AND type = 'P')");
                    tmpScript.AppendLine("DROP PROCEDURE TTSData");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("CREATE PROCEDURE SaveEntry (@text varChar(255), @voice varChar(255), @filename varChar(255), @filehash varChar(55), @approved bit, @rejected bit, @version int, @filecontent Image)");
                    tmpScript.AppendLine("AS");
                    tmpScript.AppendLine("IF NOT EXISTS(SELECT [Text] FROM [AudioFiles] WHERE [Text] like @text AND [Voice] like @voice)");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine("  INSERT INTO [AudioFiles]([LastModified], [Text], [Voice], [FileName], [FileHash], [Approved], [Rejected], [Version], [FileContent]) VALUES(GETDATE(), @text, @voice, @filename, @filehash, @approved, @rejected, @version, @filecontent)");
                    tmpScript.AppendLine("END");
                    tmpScript.AppendLine("ELSE");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine("  UPDATE [AudioFiles] SET [LastModified]=GETDATE(), [FileName]=@filename, [FileHash]=@filehash, [Approved]=@approved, [Rejected]=@rejected, [Version]=@version, [FileContent]=@filecontent WHERE [Text] like @text AND [Voice] like @voice");
                    tmpScript.AppendLine("END");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("CREATE PROCEDURE RetrieveEntry (@text varChar(255), @voice varChar(255))");
                    tmpScript.AppendLine("AS");
                    tmpScript.AppendLine("  SELECT * FROM [AudioFiles] WHERE [Text]=@text AND [VOICE]=@voice");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("CREATE PROCEDURE SaveAudioEntry (@text varChar(255), @sourcetext varChar(255), @sourcevoice varChar(255), @voice varChar(255), @filename varChar(255), @filehash varChar(55), @approved bit, @rejected bit, @version int, @filecontent Image)");
                    tmpScript.AppendLine("AS");
                    tmpScript.AppendLine("IF NOT EXISTS(SELECT [Text] FROM [AudioFiles] WHERE [Text] like @text AND [Voice] like @voice)");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine("  INSERT INTO [AudioFiles]([LastModified], [Text], [SourceText], [SourceVoice], [Voice], [FileName], [FileHash], [Approved], [Rejected], [Version], [FileContent]) VALUES(GETDATE(), @text, @sourcetext, @sourcevoice, @voice, @filename, @filehash, @approved, @rejected, @version, @filecontent)");
                    tmpScript.AppendLine("END");
                    tmpScript.AppendLine("ELSE");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine("  UPDATE [AudioFiles] SET [LastModified]=GETDATE(), [FileName]=@filename, [FileHash]=@filehash, [Approved]=@approved, [Rejected]=@rejected, [Version]=@version, [FileContent]=@filecontent, [SourceText] = @sourcetext, [SourceVoice] = @sourcevoice WHERE [Text] like @text AND [Voice] like @voice");
                    tmpScript.AppendLine("END");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }

                    tmpScript = new StringBuilder();

                    tmpScript.AppendLine("CREATE PROCEDURE TTSData(@customerid varChar(255))"); 
                    tmpScript.AppendLine("AS");
                    tmpScript.AppendLine("BEGIN");
                    tmpScript.AppendLine(" WITH allstops([StopName])");
                    tmpScript.AppendLine("AS");
                    tmpScript.AppendLine("(");
                    tmpScript.AppendLine(" SELECT");
                    tmpScript.AppendLine("  StopInfo.data.value('(StopName/text())[1]', 'varchar(260)') AS StopName");
                    tmpScript.AppendLine(" FROM [IBI_Data].[dbo].[Schedules] schedules");
                    tmpScript.AppendLine("  CROSS APPLY [ScheduleXML].nodes('/Schedule/JourneyStops/StopInfo') AS StopInfo(data)"); 
                    tmpScript.AppendLine(" WHERE schedules.CustomerID = @customerid and StopInfo.data.value('(StopName/text())[1]', 'varchar(260)') IS NOT NULL");
                    tmpScript.AppendLine(")");
                    tmpScript.AppendLine("SELECT *");
                    tmpScript.AppendLine(" FROM [TTSServer_Staging].[dbo].[AudioFiles] tts");
                    tmpScript.AppendLine(" WHERE tts.Text in (SELECT [StopName] FROM allstops) and tts.Approved=1");
                    tmpScript.AppendLine("END");

                    using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                    {
                        tmpCommand.ExecuteNonQuery();
                    }


                    this.DatabaseInitialized = true;
                }
            }
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(this.ConnectionString);
        }

        public void SaveAudioData(String text, String voice, Boolean approved, Boolean rejected, String filepath, String filehash, int version)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("SaveEntry", tmpConnection))
                {
                    tmpCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter tmpParamText = new SqlParameter("@text", System.Data.SqlDbType.VarChar, 255);
                    tmpParamText.Value = text;
                    tmpCommand.Parameters.Add(tmpParamText);

                    SqlParameter tmpParamVoice = new SqlParameter("@voice", System.Data.SqlDbType.VarChar, 255);
                    tmpParamVoice.Value = voice;
                    tmpCommand.Parameters.Add(tmpParamVoice);

                    SqlParameter tmpParamApproved = new SqlParameter("@approved", System.Data.SqlDbType.Bit);
                    tmpParamApproved.Value = approved;
                    tmpCommand.Parameters.Add(tmpParamApproved);

                    SqlParameter tmpParamRejected = new SqlParameter("@rejected", System.Data.SqlDbType.Bit);
                    tmpParamRejected.Value = rejected;
                    tmpCommand.Parameters.Add(tmpParamRejected);

                    SqlParameter tmpParamFileName = new SqlParameter("@filename", System.Data.SqlDbType.VarChar, 255);
                    tmpParamFileName.Value = System.IO.Path.GetFileName(filepath);
                    tmpCommand.Parameters.Add(tmpParamFileName);

                    SqlParameter tmpParamFileHash = new SqlParameter("@filehash", System.Data.SqlDbType.VarChar, 255);
                    tmpParamFileHash.Value = filehash;
                    tmpCommand.Parameters.Add(tmpParamFileHash);

                    SqlParameter tmpParamFileContent = new SqlParameter("@filecontent", System.Data.SqlDbType.Image);
                    tmpParamFileContent.Value = File.ReadAllBytes(filepath);
                    tmpCommand.Parameters.Add(tmpParamFileContent);

                    SqlParameter tmpParamVersion = new SqlParameter("@version", System.Data.SqlDbType.Int);
                    tmpParamVersion.Value = version;
                    tmpCommand.Parameters.Add(tmpParamVersion);

                    tmpCommand.ExecuteNonQuery();
                }
            }
        }

        public void SaveAudioData(String text, String voice, Boolean approved, Boolean rejected, String filepath, String filehash, int version, string sourcetext, string sourcevoice, byte[] fileContent)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("SaveAudioEntry", tmpConnection))
                {
                    tmpCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter tmpParamText = new SqlParameter("@text", System.Data.SqlDbType.VarChar, 255);
                    tmpParamText.Value = text;
                    tmpCommand.Parameters.Add(tmpParamText);

                    SqlParameter tmpParamSourceText = new SqlParameter("@sourcetext", System.Data.SqlDbType.VarChar, 255);
                    tmpParamSourceText.Value = sourcetext ?? "";
                    tmpCommand.Parameters.Add(tmpParamSourceText);

                    SqlParameter tmpParamSourceVoice = new SqlParameter("@sourcevoice", System.Data.SqlDbType.VarChar, 255);
                    tmpParamSourceVoice.Value = sourcevoice;
                    tmpCommand.Parameters.Add(tmpParamSourceVoice);

                    SqlParameter tmpParamVoice = new SqlParameter("@voice", System.Data.SqlDbType.VarChar, 255);
                    tmpParamVoice.Value = voice;
                    tmpCommand.Parameters.Add(tmpParamVoice);

                    SqlParameter tmpParamApproved = new SqlParameter("@approved", System.Data.SqlDbType.Bit);
                    tmpParamApproved.Value = approved;
                    tmpCommand.Parameters.Add(tmpParamApproved);

                    SqlParameter tmpParamRejected = new SqlParameter("@rejected", System.Data.SqlDbType.Bit);
                    tmpParamRejected.Value = rejected;
                    tmpCommand.Parameters.Add(tmpParamRejected);

                    SqlParameter tmpParamFileName = new SqlParameter("@filename", System.Data.SqlDbType.VarChar, 255);
                    tmpParamFileName.Value = System.IO.Path.GetFileName(filepath);
                    tmpCommand.Parameters.Add(tmpParamFileName);

                    SqlParameter tmpParamFileHash = new SqlParameter("@filehash", System.Data.SqlDbType.VarChar, 255);
                    tmpParamFileHash.Value = filehash;
                    tmpCommand.Parameters.Add(tmpParamFileHash);

                    SqlParameter tmpParamFileContent = new SqlParameter("@filecontent", System.Data.SqlDbType.Image);
                    tmpParamFileContent.Value = fileContent != null ? fileContent : File.ReadAllBytes(filepath);
                    tmpCommand.Parameters.Add(tmpParamFileContent);

                    SqlParameter tmpParamVersion = new SqlParameter("@version", System.Data.SqlDbType.Int);
                    tmpParamVersion.Value = version;
                    tmpCommand.Parameters.Add(tmpParamVersion);

                    tmpCommand.ExecuteNonQuery();
                }
            }
        }

        public void ApproveAudioFile(String text, String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                ///tmpScript.AppendLine("UPDATE [AudioFiles] SET [Approved]=1 WHERE [Text]='" + text.Replace("'","''") + "' AND [Voice]='" + voice + "'");
                tmpScript.AppendLine("UPDATE [AudioFiles] SET [Approved]=1 WHERE [Text]='" + text.Replace("'", "''") + "'");

                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    tmpCommand.ExecuteNonQuery();
                }
            }
        }

        public void RejectAudioFile(String text, String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                //tmpScript.AppendLine("UPDATE [AudioFiles] SET [Approved]=0, [Rejected]=1 WHERE [Text]='" + text.Replace("'", "''") + "' AND [Voice]='" + voice + "'");
                tmpScript.AppendLine("UPDATE [AudioFiles] SET [Approved]=0, [Rejected]=1 WHERE [Text]='" + text.Replace("'", "''") + "'");

                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    tmpCommand.ExecuteNonQuery();
                }
            }
        }

        public AudioFile GetAudioData(String text, String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("RetrieveEntry", tmpConnection))
                {
                    tmpCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter tmpParamText = new SqlParameter("@text", System.Data.SqlDbType.VarChar, 255);
                    tmpParamText.Value = text;
                    tmpCommand.Parameters.Add(tmpParamText);

                    // SP does not consider voice variable
                    SqlParameter tmpParamVoice = new SqlParameter("@voice", System.Data.SqlDbType.VarChar, 255);
                    tmpParamVoice.Value = voice;
                    tmpCommand.Parameters.Add(tmpParamVoice);

                    SqlDataReader tmpDataReader = tmpCommand.ExecuteReader();

                    if (tmpDataReader.HasRows)
                    {
                        tmpDataReader.Read();

                        Hashtable tmpColumnNames = new Hashtable();

                        for (int i = 0; i < tmpDataReader.FieldCount; i++)
                        {
                            tmpColumnNames[tmpDataReader.GetName(i)] = i;
                        }

                        AudioFile tmpAudioFile = new AudioFile();
                        tmpAudioFile.LastModified = (DateTime)tmpDataReader.GetValue((int)tmpColumnNames["LastModified"]);
                        tmpAudioFile.Text = (String)tmpDataReader.GetValue((int)tmpColumnNames["Text"]);
                        tmpAudioFile.Voice = (String)tmpDataReader.GetValue((int)tmpColumnNames["Voice"]);
                        tmpAudioFile.FileName = (String)tmpDataReader.GetValue((int)tmpColumnNames["FileName"]);
                        tmpAudioFile.Approved = (Boolean)tmpDataReader.GetValue((int)tmpColumnNames["Approved"]);
                        tmpAudioFile.Rejected = (Boolean)tmpDataReader.GetValue((int)tmpColumnNames["Rejected"]);
                        tmpAudioFile.FileContent = (Byte[])tmpDataReader.GetValue((int)tmpColumnNames["FileContent"]);
                        tmpAudioFile.Version = (int)tmpDataReader.GetValue((int)tmpColumnNames["Version"]);

                        return tmpAudioFile;
                    }
                }
            }

            return null;
        }

        public AudioFile GetAudioData(String filename)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("SELECT * FROM [AudioFiles] WHERE [FileName] like '" + filename + "%'", tmpConnection))
                {
                    SqlDataReader tmpDataReader = tmpCommand.ExecuteReader();

                    if (tmpDataReader.HasRows)
                    {
                        tmpDataReader.Read();

                        Hashtable tmpColumnNames = new Hashtable();

                        for (int i = 0; i < tmpDataReader.FieldCount; i++)
                        {
                            tmpColumnNames[tmpDataReader.GetName(i)] = i;
                        }

                        AudioFile tmpAudioFile = new AudioFile();
                        tmpAudioFile.LastModified = (DateTime)tmpDataReader.GetValue((int)tmpColumnNames["LastModified"]);
                        tmpAudioFile.Text = (String)tmpDataReader.GetValue((int)tmpColumnNames["Text"]);
                        tmpAudioFile.Voice = (String)tmpDataReader.GetValue((int)tmpColumnNames["Voice"]);
                        tmpAudioFile.FileName = (String)tmpDataReader.GetValue((int)tmpColumnNames["FileName"]);
                        tmpAudioFile.Approved = (Boolean)tmpDataReader.GetValue((int)tmpColumnNames["Approved"]);
                        tmpAudioFile.Rejected = (Boolean)tmpDataReader.GetValue((int)tmpColumnNames["Rejected"]);
                        tmpAudioFile.FileContent = (Byte[])tmpDataReader.GetValue((int)tmpColumnNames["FileContent"]);
                        tmpAudioFile.Version = (int)tmpDataReader.GetValue((int)tmpColumnNames["Version"]);

                        return tmpAudioFile;
                    }
                }
            }

            return null;
        }


        public Byte[] GetAudioFileByStopName(String stopname, String voice)
        {
            return GetAudioFileInQueryByStopName(stopname, voice);
        }

        public Byte[] GetAudioFile(String filename, String voice)
        {
            return GetAudioFileInQuery(filename, voice);
        }

        private Byte[] GetAudioFileInQueryByStopName(String stopname, String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("SELECT * FROM [AudioFiles] WHERE [Voice] = '" + voice + "' and [Text] = '" + stopname + "'", tmpConnection))
                {
                    SqlDataReader tmpDataReader = tmpCommand.ExecuteReader();

                    if (tmpDataReader.HasRows)
                    {
                        tmpDataReader.Read();

                        Hashtable tmpColumnNames = new Hashtable();

                        for (int i = 0; i < tmpDataReader.FieldCount; i++)
                        {
                            tmpColumnNames[tmpDataReader.GetName(i)] = i;
                        }

                        return (Byte[])tmpDataReader.GetValue((int)tmpColumnNames["FileContent"]);
                    }
                }
            }

            return null;
 
        }

        private Byte[] GetAudioFileInQuery(String filename, String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                using (SqlCommand tmpCommand = new SqlCommand("SELECT * FROM [AudioFiles] WHERE [Voice] = '" + voice + "' and [FileName] like '" + filename + "%'", tmpConnection))
                {
                    SqlDataReader tmpDataReader = tmpCommand.ExecuteReader();

                    if (tmpDataReader.HasRows)
                    {
                        tmpDataReader.Read();

                        Hashtable tmpColumnNames = new Hashtable();

                        for (int i = 0; i < tmpDataReader.FieldCount; i++)
                        {
                            tmpColumnNames[tmpDataReader.GetName(i)] = i;
                        }

                        return (Byte[])tmpDataReader.GetValue((int)tmpColumnNames["FileContent"]);
                    }
                }
            }

            return null;
        }

        public Byte[] GetAudioFile(String filename)
        {
            return GetAudioFileInQuery(filename, ConfigurationManager.AppSettings["AudioFileDefaultVoice"]);
        }

        public DataTable GetCustomerApprovedAudioData(String voice)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                tmpScript.AppendLine("SELECT [Text],[LastModified],[FileName],[FileHash],[Approved],[Rejected],[Version] FROM [AudioFiles] WHERE [Approved]=1 and [Voice]='" + voice + "'");

                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    DataSet result = new DataSet();

                    using (SqlDataAdapter tmpAdapter = new SqlDataAdapter(tmpCommand))
                    {
                        tmpAdapter.Fill(result);

                        return result.Tables[0];
                    }
                }
            }

            return null;
        }
        public DataTable GetAllApprovedAudioData()
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                tmpScript.AppendLine("SELECT [Text],[LastModified],[FileName],[FileHash],[Approved],[Rejected],[Version] FROM [AudioFiles] WHERE [Approved]=1");
              
                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    DataSet result = new DataSet();

                    using (SqlDataAdapter tmpAdapter = new SqlDataAdapter(tmpCommand))
                    {
                        tmpAdapter.Fill(result);

                        return result.Tables[0];
                    }
                }
            }

            return null;
        }

        public DataTable GetAllUnapprovedAudioData(Boolean includeRejected)
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                if (!includeRejected)
                    tmpScript.AppendLine("SELECT [Text],[LastModified],[FileName],[FileHash],[Approved],[Rejected],[Version] FROM [AudioFiles] WHERE [Approved]=0 AND [Rejected]=0");
                else
                    tmpScript.AppendLine("SELECT [Text],[LastModified],[FileName],[FileHash],[Approved],[Rejected],[Version] FROM [AudioFiles] WHERE [Approved]=0");

                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    DataSet result = new DataSet();

                    using (SqlDataAdapter tmpAdapter = new SqlDataAdapter(tmpCommand))
                    {
                        tmpAdapter.Fill(result);

                        return result.Tables[0];
                    }
                }
            }

            return null;
        }

        public DataTable GetAllRejectedAudioData()
        {
            this.InitializeDatabase();

            using (SqlConnection tmpConnection = this.GetConnection())
            {
                tmpConnection.Open();

                StringBuilder tmpScript = new StringBuilder();
                tmpScript.AppendLine("SELECT [Text],[LastModified],[FileName],[FileHash],[Approved],[Rejected],[Version] FROM [AudioFiles] WHERE [Rejected]=1");

                using (SqlCommand tmpCommand = new SqlCommand(tmpScript.ToString(), tmpConnection))
                {
                    DataSet result = new DataSet();

                    using (SqlDataAdapter tmpAdapter = new SqlDataAdapter(tmpCommand))
                    {
                        tmpAdapter.Fill(result);

                        return result.Tables[0];
                    }
                }
            }

            return null;
        }
    }
}
