﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mermaid.TTS
{
    public interface ITTSEngine : IDisposable
    {
        void GenerateAudioFile(String text, String targetFile);
        void GenerateAudioFile(String text, String targetFile, ref String fixedText);

    }
}
