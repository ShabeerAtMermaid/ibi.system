﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.DataAccess;
using System.ServiceModel.Web;
using IBI.Shared;
using System.Data;
using System.Data.SqlClient;
using IBI.Shared.Diagnostics;
using IBI.REST.Shared;
using System.Text;
using System.Collections;
//using IBI.DataAccess.UserManagement;
using IBI.DataAccess.DBModels;
using IBI.Shared.Models;
using IBI.Shared.Models.Accounts;
using IBI.Shared.Common.Types;

namespace IBI.REST.UserManagement
{
    public class UserManagementController
    {

        public static List<UserPage> GetUserPages(int userId)
        {
            List<UserPage> list = new List<UserPage>();
            List<UserPage> finalList = new List<UserPage>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    IBI.DataAccess.DBModels.User usr = dbContext.Users.Where(u => u.UserId == userId).FirstOrDefault();

                    if (usr != null)
                    {
                        foreach (Role role in usr.Roles)
                        {

                            foreach (RolePage rolePage in role.RolePages)
                            {
                                //start - logic for admin
                                if (rolePage.PageId == 0)
                                {
                                    list.Clear();
                                    foreach (Page p in dbContext.Pages.Where(p => p.PageId > 0))
                                    {

                                        list.Add(new UserPage
                                        {
                                            PageId = p.PageId,
                                            ParentPageId = p.ParentPageId ?? 0,
                                            ParentPageName = (p.ParentPage != null) ? p.ParentPage.Name : "",
                                            PageName = p.Name,
                                            MenuInitials = p.MenuInitials,
                                            Controller = p.Controller,
                                            Action = p.Action,
                                            ShowMenu = p.ShowMenu ?? false,
                                            Path = p.Path,
                                            Access = "1111",
                                            Read = true,
                                            Write = true,
                                            Delete = true,
                                            Update = true
                                        });
                                    }
                                    return list;
                                }
                                //end - logic for admin

                                list.Add(new UserPage
                                {
                                    PageId = rolePage.PageId,
                                    ParentPageId = rolePage.Page.ParentPageId ?? 0,
                                    ParentPageName = (rolePage.Page.ParentPage != null) ? rolePage.Page.ParentPage.Name : "",
                                    PageName = rolePage.Page.Name,
                                    MenuInitials = rolePage.Page.MenuInitials,
                                    Controller = rolePage.Page.Controller,
                                    Action = rolePage.Page.Action,
                                    Path = rolePage.Page.Path,
                                    ShowMenu = rolePage.Page.ShowMenu ?? false,
                                    Access = rolePage.Access,
                                    Read = rolePage.Read ?? false,
                                    Write = rolePage.Write ?? false,
                                    Delete = rolePage.Delete ?? false,
                                    Update = rolePage.Update ?? false
                                });
                            }
                        }
                    }
                }


                foreach (UserPage uPage in list)
                {
                    if (uPage.ParentPageId == 0)
                    {
                        var firstChild = list.Where(p => p.ParentPageId == uPage.PageId && p.ShowMenu == true && p.Read).FirstOrDefault();

                        if (firstChild != null)
                        {
                            uPage.Controller = firstChild.Controller;
                            uPage.Action = firstChild.Action;
                            uPage.Path = firstChild.Path;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, ex);
                throw ex;
            }

            return list;
        }


        #region "UserActivity Configuration"

        internal static List<IBIKeyValuePair> GetUserActivityConfigurations(int userId)
        {
            List<IBIKeyValuePair> list = new List<IBIKeyValuePair>();

            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    foreach(var p in dbContext.GetUserActivityConfigurations(userId))
                    {
                        list.Add(new IBIKeyValuePair(p.Key, p.Value));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.USER_MGMT, ex);
            }

            return list;
        }

        internal static void SetUserActivityConfiguration(int userId, string property, string value)
        {
            try
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    dbContext.SetUserActivityConfiguration(userId, property, value);
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.USER_MGMT, ex);
            }
        }

        #endregion



       
    }

}