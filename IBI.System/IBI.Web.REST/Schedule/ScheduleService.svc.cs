﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared.Models;
using System.ServiceModel.Activation;
using IBI.Shared.Models.Destinations;
using IBI.Shared.Models.Signs;
using System.Xml;
using IBI.Shared.Models.SignAddons;
using IBI.Shared.Models.ScheduleTreeItems;

namespace IBI.REST.Schedule
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ScheduleService : IScheduleService
    {
        public XmlDocument GetCurrentSchedule(String busNo, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetCurrentSchedule"))
            {
                return ScheduleServiceController.GetCurrentSchedule(busNo, customerId);
            }
            
        }

        public XmlDocument DownloadCurrentSchedule(String busNo, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.DownloadCurrentSchedule"))
            {
                return ScheduleServiceController.DownloadCurrentSchedule(busNo, customerId);
            }
            
        }

        public ActiveBusReply GetActiveBus()
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetActiveBus"))
            {
                return ScheduleServiceController.GetActiveBus();
            }
            
        }

        public ActiveBusReply GetActiveBusFiltered(int searchSeed, Boolean preferVia)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetActiveBusFiltered"))
            {
                return ScheduleServiceController.GetActiveBus(searchSeed, preferVia);
            }
            
        }

        public ZoneLookup GetZone(String latitude, String longitude)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetZone"))
            {
                return ScheduleServiceController.GetZone(latitude, longitude);
            }
            
        }


        //public List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string plannedDeparture, string plannedArrival, string actualDeparture, string actualArrival)
        //{
        //    return ScheduleServiceController.GetJourneys(
        //        line,
        //        busNumber,
        //        journeyNumber,
        //        journeyDate,
        //        from,
        //        to,
        //        plannedDeparture,
        //        plannedArrival,
        //        actualDeparture,
        //        actualArrival
        //        );

        //}

        //public List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(string line, string busNumber, string journeyNumber, DateTime journeyDate, string from, string to, string stopName, string plannedDeparture, string actualDeparture, string actualArrival, string planDifference, string status)
        //{
        //    return ScheduleServiceController.GetPunctuality(
        //         line,
        //         busNumber,
        //         journeyNumber,
        //         journeyDate,
        //         from,
        //         to,
        //         stopName,
        //         plannedDeparture,
        //         actualDeparture,
        //         actualArrival,
        //         planDifference,
        //         status
        //        );
        //}

        public List<IBI.Shared.Models.BusJourney.Journey> GetJourneys(DateTime startDate, string busNumber)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetJourneys"))
            {
                return ScheduleServiceController.GetJourneys(
                startDate, busNumber
                );
            }
            

        }

        public List<IBI.Shared.Models.BusJourney.Punctuality> GetPunctuality(DateTime startDate, string busNumber, int journeyNumber)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetPunctuality"))
            {
                return ScheduleServiceController.GetPunctuality(
                startDate, busNumber, journeyNumber
                );
            }
            
        }


        #region Signs

        public List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsData(int signId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignsData"))
            {
                return ScheduleServiceController.GetSignsXmlData(signId);
            }
            
        }


        public bool SaveSign(Sign sign, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSign"))
            {
                return ScheduleServiceController.SaveSign(sign, userName);
            }
            
        }

        public bool DeleteSign(int signId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.DeleteSign"))
            {
                return ScheduleServiceController.DeleteSign(signId, userName);
            }
            
        }

        //public IBI.Shared.Models.Destinations.Destination FindSpecialDestination(int customerId, string line, string destinationName, string selectionStructure) {
        //    return ScheduleServiceController.FindSpecialDestination(customerId, line, destinationName, selectionStructure);
        //}

        public SignTree GetSignTree(SignTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping)
        {

            using (new IBI.Shared.CallCounter("ScheduleService.GetSignTree"))
            {
                SignTree t = ScheduleServiceController.GetSignTree(tree, includeLeaf, showInactive, setAlphNumericGrouping);
                return t;
            }
            
        }


        public Sign GetSign(int signId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSign"))
            {
                return ScheduleServiceController.GetSign(signId);
            }
            
        }

        public Group GetGroup(int groupId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetGroup"))
            {
                return ScheduleServiceController.GetGroup(groupId);
            }
            
        }

        public bool SaveSignGroup(IBI.Shared.Models.Signs.Group group, int? oldParentId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignGroup"))
            {
                return ScheduleServiceController.SaveSignGroup(group, oldParentId, userName);
            }
            
        }

        public bool SaveSignGroupSimple(IBI.Shared.Models.Signs.Group group, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignGroupSimple"))
            {
                return ScheduleServiceController.SaveSignGroup(group, userName);
            }
            
        }


        public Sign FindSign(int customerId, int? groupId, string line, string name, string mainText, string subText)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.FindSign"))
            {
                return ScheduleServiceController.FindSign(customerId, groupId, line, name, mainText, subText);
            }
            
        }
        public Group FindSignGroup(int customerId, int? parentGroupId, string groupName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.FindSignGroup"))
            {
                return ScheduleServiceController.FindSignGroup(customerId, parentGroupId, groupName);
            }
            
        }

        public bool DeleteSignGroup(int groupId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.DeleteSignGroup"))
            {
                return ScheduleServiceController.DeleteSignGroup(groupId, userName);
            }
            
        }

        public bool HasChildSignGroup(int groupId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.HasChildSignGroup"))
            {
                return ScheduleServiceController.HasChildSignGroup(groupId);
            }
            
        }

        public bool FindScheduleId(int scheduleId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.FindScheduleId"))
            {
                return ScheduleServiceController.FindScheduleId(scheduleId);
            }
            
        }

        public SignGraphicRule GetGraphicRule(int graphicRuleId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetGraphicRule"))
            {
                return ScheduleServiceController.GetGraphicRule(graphicRuleId, userName);
            }
            
        }

        public int SaveGraphicRule(SignGraphicRule graphicRule, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveGraphicRule"))
            {
                return ScheduleServiceController.SaveGraphicRule(graphicRule, userName);
            }
        }

        public bool SaveSignData(int signItemId, int signRuleId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignData"))
            {
                return ScheduleServiceController.SaveSignData(signItemId, signRuleId, userName);
            }
            
        }

        public List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignPreview(int signItemId, int signRuleId, int layoutId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignPreview"))
            {
                return ScheduleServiceController.GetSignPreview(signItemId, signRuleId, layoutId, userName);
            }
            
        }


        public IBI.Shared.Models.Signs.SignXmlLayout GetSignLayout(int signLayoutId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignLayout"))
            {
                return ScheduleServiceController.GetSignLayout(signLayoutId);
            }
            
        }


        public IBI.Shared.Models.Signs.SignGraphicRule GetSignGraphicRule(int signGraphicRuleId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignGraphicRule"))
            {
                return ScheduleServiceController.GetSignGraphicRule(signGraphicRuleId);
            }
            
        }

        public List<SignDataImageItem> GetSignImages(int customerId, int layoutid, int contentid)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignImages"))
            {
                return ScheduleServiceController.GetSignImages(customerId, layoutid, contentid);
            }
            
        }

        public bool SaveSignDataImages(int layoutId, SignXmlData signData, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignDataImages"))
            {
                return ScheduleServiceController.SaveSignDataImages(layoutId, signData, userName);
            }
            
        }

        public List<string> GetImagePreview(int signItemId, SignGraphicRule signGraphicRule, int layoutId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetImagePreview"))
            {
                return ScheduleServiceController.GetImagePreview(signItemId, signGraphicRule, layoutId, userName);
            }
            
        }

        public bool SaveImageGeneratorData(int layoutId, int signItemId, int signRuleId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveImageGeneratorData"))
            {
                return ScheduleServiceController.SaveImageGeneratorData(layoutId, signItemId, signRuleId, userName);
            }
            
        }

        #endregion



        #region SignAddons

        public List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonsData(int signAddonItemId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddonsData"))
            {
                return ScheduleServiceController.GetSignAddonsXmlData(signAddonItemId);
            }
            
        }


        public bool SaveSignAddon(SignAddon signAddon, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignAddon"))
            {
                return ScheduleServiceController.SaveSignAddon(signAddon, userName);
            }
            
        }

        public bool DeleteSignAddon(int signAddonItemId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.DeleteSignAddon"))
            {
                return ScheduleServiceController.DeleteSignAddon(signAddonItemId, userName);
            }
            
        }


        public SignAddonTree GetSignAddonTree(SignAddonTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddonTree"))
            {
                SignAddonTree t = ScheduleServiceController.GetSignAddonTree(tree, includeLeaf, showInactive, setAlphNumericGrouping);
                return t;
            }
            
        }


        public SignAddon GetSignAddon(int signAddonItemId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddon"))
            {
                return ScheduleServiceController.GetSignAddon(signAddonItemId);
            }
            
        }


        public SignAddon FindSignAddon(int customerId, int? groupId, string line, string name, string signText, string udpText)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.FindSignAddon"))
            {
                return ScheduleServiceController.FindSignAddon(customerId, groupId, line, name, signText, udpText);
            }
            
        }


        public bool SaveSignAddonData(int signAddonItemId, int signRuleId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignAddonData"))
            {
                return ScheduleServiceController.SaveSignAddonData(signAddonItemId, signRuleId, userName);
            }
            
        }

        public List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignAddonPreview(int signAddonItemId, int signRuleId, int layoutId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddonPreview"))
            {
                return ScheduleServiceController.GetSignAddonPreview(signAddonItemId, signRuleId, layoutId, userName);
            }
            
        }



        public List<SignDataImageItem> GetSignAddonImages(int customerId, int layoutid, int signAddonItemId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddonImages"))
            {
                return ScheduleServiceController.GetSignAddonImages(customerId, layoutid, signAddonItemId);
            }
            
        }

        public bool SaveSignAddonDataImages(int layoutId, SignXmlData signAddonData, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignAddonDataImages"))
            {
                return ScheduleServiceController.SaveSignAddonDataImages(layoutId, signAddonData, userName);
            }
            
        }

        public List<string> GetSignAddonImagePreview(int signAddonItemId, SignGraphicRule signGraphicRule, int layoutId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSignAddonImagePreview"))
            {
                return ScheduleServiceController.GetSignAddonImagePreview(signAddonItemId, signGraphicRule, layoutId, userName);
            }
            
        }

        public bool SaveSignAddonImageGeneratorData(int layoutId, int signAddonItemId, int signRuleId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSignAddonImageGeneratorData"))
            {
                return ScheduleServiceController.SaveSignAddonImageGeneratorData(layoutId, signAddonItemId, signRuleId, userName);
            }
            
        }

        #endregion


        #region "Schedule Tree"

        public ScheduleTree GetScheduleTree(ScheduleTree tree, bool includeLeaf, bool showInactive, bool setAlphNumericGrouping)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetScheduleTree"))
            {
                return ScheduleServiceController.GetScheduleTree(tree, includeLeaf, showInactive, setAlphNumericGrouping);
            }
            
        }

        public bool CheckExistingSchedule(int scheduleId, int customerId, string line, string fromName, string destination, string viaName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.CheckExistingSchedule"))
            {
                return ScheduleServiceController.CheckExistingSchedule(scheduleId, customerId, line, fromName, destination, viaName);
            }
            
        }

        public IBI.Shared.Models.ScheduleTreeItems.Schedule GetSchedule(int scheduleId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetSchedule"))
            {
                return ScheduleServiceController.GetSchedule(scheduleId);
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sch"></param>
        /// <returns>
        ///  > 0 : new schedule created
        ///  0 : error, no schedule saved
        /// -1 : schedule already exists - no changes made (may not occur)
        /// -2 : schedule already exists - with changes
        /// -3 : schedule already exists - no changes
        /// -4 : schedule already exists - updated successfully
        /// </returns>
        public int SaveSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule sch, string forcedOption)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.SaveSchedule"))
            {
                int schId = ScheduleServiceController.SaveSchedule(sch, forcedOption);
                return schId;
            }
            
        }

        public int UpdateSchedule(IBI.Shared.Models.ScheduleTreeItems.Schedule schedule, string forcedOption)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.UpdateSchedule"))
            {
                return ScheduleServiceController.UpdateSchedule(schedule, forcedOption);
            }
            
        }

        public bool DeleteSchedule(int scheduleId) {
            using (new IBI.Shared.CallCounter("ScheduleService.DeleteSchedule"))
            {
                return ScheduleServiceController.DeleteSchedule(scheduleId);
            }
            
        }

        public int UpdateScheduleStops(IBI.Shared.Models.ScheduleTreeItems.Schedule sch) {
            using (new IBI.Shared.CallCounter("ScheduleService.UpdateScheduleStops"))
            {
                return ScheduleServiceController.UpdateScheduleStops(sch);
            }
            
        }

        #endregion



        /// <summary>
        /// Get a schedule and it's stops based on schedule Id but if that user doesn't have access to that customer then return no schedule
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public IBI.Shared.Models.UserScheduleModels.Schedule GetUserSchedule(int userId, int scheduleId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.GetUserSchedule"))
            {
                IBI.Shared.Models.UserScheduleModels.Schedule userSchedule = ScheduleServiceController.GetUserSchedule(userId, scheduleId);
                return userSchedule;
            }
            
        }
    }
}