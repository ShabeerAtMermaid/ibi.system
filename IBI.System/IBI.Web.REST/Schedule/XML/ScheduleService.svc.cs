﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using System.ServiceModel.Activation;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;

namespace IBI.REST.Schedule.XML
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ScheduleService : IScheduleService
    {
        public XmlDocument GetCurrentSchedule(String busNo, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetCurrentSchedule"))
            {
                XmlDocument xDoc = ScheduleServiceController.GetCurrentSchedule(busNo, customerId);
                return xDoc;
            }
            

        }

        public XmlDocument DownloadCurrentSchedule(String busNo, String customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.DownloadCurrentSchedule"))
            {
                XmlDocument xDoc = ScheduleServiceController.DownloadCurrentSchedule(busNo, customerId);
                return xDoc;
            }
            
        }

        public String GetInServiceStatus(String busNo, String customerId) {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetInServiceStatus"))
            {
                string val = ScheduleServiceController.GetInServiceStatus(busNo, customerId);
                return val;
            }
            
        }

        public ActiveBusReply GetActiveBus()
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetActiveBus"))
            {
                return ScheduleServiceController.GetActiveBus();
            }
            
        }

        public ActiveBusReply GetActiveBusFiltered(int searchSeed, Boolean preferVia)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetActiveBusFiltered"))
            {
                return ScheduleServiceController.GetActiveBus(searchSeed, preferVia);
            }
            
        }

        public ZoneLookup GetZone(String latitude, String longitude)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetZone"))
            {
                return ScheduleServiceController.GetZone(latitude, longitude);
            }
            
        }


        public string SyncDestinationImages(int customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.SyncDestinationImages"))
            {
                return ScheduleServiceController.SyncDestinationImages(customerId, null, "Browser");
            }
            
            
        }

        public string SyncDestinationContentImages(string signItems, string signGroups, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.SyncDestinationContentImages"))
            {
                return ScheduleServiceController.SyncDestinationContentImages(signItems, signGroups, userName);
            }
            
        }

        public string SyncDestinationContent(int customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.SyncDestinationContent"))
            {

            }
            throw new NotImplementedException();
        }

        public string GetDestinationImagesData(int customerId, string mainText, string subText, string line)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetDestinationImagesData"))
            {
                return ScheduleServiceController.GetDestinationImagesData(customerId, mainText, subText, line);
            }
            
        }

        public List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsData(int signId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetSignsData"))
            {
                return ScheduleServiceController.GetSignsXmlData(signId);
            }
            
        }

        public IBI.Shared.Models.Signs.SignXml GetSignContentData(int customerId, int layoutid, int contentid)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.GetSignContentData"))
            {
                return ScheduleServiceController.GetSignContentXmlData(customerId, layoutid, contentid);
            }
            
        }

        public string CreateDestinationXml(int customerId, string mainText, string subText, string line, string selectionStructure)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.CreateDestinationXml"))
            {

            }
            throw new NotImplementedException();
        }

        public string UpdateDestinationXml(int customerId, string mainText, string subText, string line, string oldMainText, string oldSubText, string oldLine, string selectionStructure, int priority)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.UpdateDestinationXml"))
            {

            }
            throw new NotImplementedException();
        }

        public string ForceMD5Recalculation(int customerId)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.ForceMD5Recalculation"))
            {
                return ScheduleServiceController.ForceMD5Recalculation(customerId);
            }
            
        }

        public string ForceManualRule(int customerId, int ruleid)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.ForceManualRule"))
            {
                return ScheduleServiceController.ForceManualRule(customerId, ruleid);
            }
            
        }

        public string SyncSignData(int customerId, int signId, string userName)
        {
            using (new IBI.Shared.CallCounter("ScheduleService.Xml.SyncSignData"))
            {
                return ScheduleServiceController.SyncSignData(signId, userName);
            }
            
        }
    }
}
