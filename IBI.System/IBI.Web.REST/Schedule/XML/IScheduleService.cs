﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using IBI.Shared.Models;
using System.Xml;
using System.Xml.Linq;

namespace IBI.REST.Schedule.XML
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    [XmlSerializerFormat]
    public interface IScheduleService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CurrentSchedule?busno={busno}&customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        XmlDocument GetCurrentSchedule(String busNo, String customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "DownloadCurrentSchedule?busno={busno}&customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        XmlDocument DownloadCurrentSchedule(String busNo, String customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetInServiceStatus?busno={busno}&customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        String GetInServiceStatus(String busNo, String customerId);
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ActiveBus", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        ActiveBusReply GetActiveBus();
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ActiveBusFiltered?searchseed={searchseed}&prefervia={prefervia}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        ActiveBusReply GetActiveBusFiltered(int searchSeed, Boolean preferVia);
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetZone?lat={latitude}&lon={longitude}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        ZoneLookup GetZone(String latitude, String longitude);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SyncDestinationImages?customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string SyncDestinationImages(int customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SyncDestinationContentImages?signItems={signItems}&signGroups={signGroups}&userName={userName}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string SyncDestinationContentImages(string signItems, string signGroups, string userName);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SyncDestinationContent?customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string SyncDestinationContent(int customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetDestinationImagesData?customerId={customerId}&maintext={mainText}&subtext={subText}&line={line}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string GetDestinationImagesData(int customerId, string mainText, string subText, string line);

        [OperationContract]
        [WebGet(
            UriTemplate = "GetSignsData?signId={signId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        [AspNetCacheProfile("GetSignsDataCache")]
        //[WebInvoke(Method = "GET", UriTemplate = "GetSignsData?signId={signId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<IBI.Shared.Models.Signs.SignXmlLayout> GetSignsData(int signId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetSignContentData?customerId={customerId}&layoutid={layoutid}&contentid={contentid}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        IBI.Shared.Models.Signs.SignXml GetSignContentData(int customerId, int layoutid, int contentid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CreateDestination?customerId={customerId}&mainText={mainText}&subText={subText}&line={line}&selectionStructure={selectionStructure}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string CreateDestinationXml(int customerId, string mainText, string subText, string line, string selectionStructure);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "UpdateDestination?customerId={customerId}&mainText={mainText}&subText={subText}&line={line}&oldMainText={oldMainText}&oldSubText={oldSubText}&oldLine={oldLine}&selectionStructure={selectionStructure}&priority={priority}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string UpdateDestinationXml(int customerId, string mainText, string subText, string line, string oldMainText, string oldSubText, string oldLine, string selectionStructure, int priority);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ForceMD5Recalculation?customerId={customerId}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string ForceMD5Recalculation(int customerId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ForceManualRule?customerId={customerId}&ruleid={ruleid}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string ForceManualRule(int customerId, int ruleid);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "SyncSignData?customerId={customerId}&signcontentid={signcontentid}&userName={userName}", RequestFormat = WebMessageFormat.Xml, ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        string SyncSignData(int customerId, int signcontentid, string userName);

    }
}