﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Net;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared.Diagnostics;

namespace IBI.REST.Schedule
{
    public class ZoneHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("ZoneHandler"))
            {
                String latitude = context.Request["lat"];
                String longitude = context.Request["lon"];

                string kmlFileContents = "";

                String requestString = "http://geo.oiorest.dk/takstzoner/" + latitude + "," + longitude;

                try
                {
                    if (!String.IsNullOrEmpty(latitude) && !String.IsNullOrEmpty(longitude))
                    {
                        kmlFileContents = ScheduleServiceController.GetKML(latitude, longitude);
                    }
                }
                catch (Exception ex)
                {
                    string clientIP = Shared.RestUtil.GetClientIP();
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.OIORest, new ApplicationException("Zone request failed from [" + clientIP + "]." + Environment.NewLine +
                                                                                                                          "Request: " + requestString + Environment.NewLine, ex));
                    //throw ex;

                    context.Response.Clear();
                    context.Response.StatusCode = 404;
                    context.Response.End();
                }


                Byte[] fileContent = Encoding.UTF8.GetBytes(kmlFileContents);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=Zone" + zone + ".kml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

       
        #endregion
    }
}
