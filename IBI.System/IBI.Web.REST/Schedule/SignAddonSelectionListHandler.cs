﻿using System;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using IBI.REST.Shared;
using IBI.Shared;
using IBI.DataAccess.DBModels;
using System.Xml.Linq;
using System.Data.Entity;
using System.Linq;
using IBI.Shared.Models;
using IBI.Shared.Models.Schedules;
using IBI.REST.Common;


namespace IBI.REST.Schedule
{
    public class SignAddonSelectionListHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }



        public void ProcessRequest(HttpContext context)
        {
            using (new IBI.Shared.CallCounter("SignAddOnSelectionListHandler"))
            {
                String customerID = context.Request["customerid"];

                if (String.IsNullOrEmpty(customerID))
                    customerID = "2140";

                String content = string.Empty;
                object customerIDCache = context.Cache[customerID + "signaddonselectionlistexpiry"];
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.SignAddOnSelectionListCache) > DateTime.Now)
                {
                    content = (String)context.Cache[customerID + "signaddonselectionlistcontent"];
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    //sb.Append(GetDestinationTree(int.Parse(customerID)));
                    sb.Append(GetSignAddonTree(int.Parse(customerID)));
                    content = sb.ToString();
                    context.Cache[customerID + "signaddonselectionlistexpiry"] = DateTime.Now;
                    context.Cache[customerID + "signaddonselectionlistcontent"] = content;
                }

                Byte[] fileContent = Encoding.UTF8.GetBytes(content);

                //Clear all content output from the buffer stream 
                context.Response.Clear();

                //Add a HTTP header to the output stream that specifies the filename 
                //context.Response.AddHeader("Content-Disposition", "attachment; filename=DestinationList.xml");

                //Add a HTTP header to the output stream that contains the content length(File Size)
                context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                //Set the HTTP MIME type of the output stream 
                context.Response.ContentType = "text/xml";

                //Write the data out to the client. 
                context.Response.BinaryWrite(fileContent);
            }
            
        }

        #endregion


        #region Helper

        private static void AppendSigns(List<IBI.Shared.Models.SignAddons.SignAddon> signAddons, ref StringBuilder xmlData)
        {
            if (signAddons == null || signAddons.Count() == 0)
            {
                return;
            }

            if (signAddons.FirstOrDefault() != null)
            {
                foreach (IBI.Shared.Models.SignAddons.SignAddon s in signAddons.FirstOrDefault().children)
                {
                    //SignAddonListModel tmpSign = new SignAddonListModel();
                    //tmpSign.Name = tmpSign.UdpText = s.Name;
                    //tmpSign.NodeText = s.SignText;
                    //tmpSign.Priority = (s.Priority == null || s.Priority == 0) ? 99999 : s.Priority;
                    //items.Add(tmpSign);

                    if (string.IsNullOrEmpty(s.UdpText))
                    {
                        s.UdpText = s.Name;
                    }
                    //xmlData.Append("<Destination dest=\"" + dest + "\" via=\"" + via + "\" line=\"" + scheduleLine + "\" destination=\"" + scheduleDestination + "\" schedule=\"" + scheduleId + "\" signitemid=\"" + s.SignId + "\" linetext=\"" + s.Line + "\" maintext=\"" + s.MainText + "\" subtext=\"" + s.SubText + "\">" + s.Name + "</Destination>");
                    xmlData.Append(string.Format("<SignAddon itemid=\"{0}\" signtext=\"{1}\" >{2}</SignAddon>", s.SignAddonItemId, HttpContext.Current.Server.HtmlEncode(s.SignText), HttpContext.Current.Server.HtmlEncode(s.Name)));
                }
            }
        }


        public static string GetSignAddonTree(int customerId)
        {
            IBI.Shared.Models.SignAddons.SignAddonTree signAddonTree = new IBI.Shared.Models.SignAddons.SignAddonTree();
            signAddonTree.CustomerID = customerId;
            signAddonTree = ScheduleServiceController.GetSignAddonTree(signAddonTree, true, false, false);
            StringBuilder sb = new StringBuilder();
            if (signAddonTree != null)
            {
                sb.Append("<Data>");
                List<DestListModel> schedules = new List<DestListModel>();

                StringBuilder sbdata = new StringBuilder();
                //flat sign addon list

                sb.Append("<SignAddons>");

                AppendSigns(signAddonTree.SignAddons, ref sbdata);
                sb.Append(sbdata.ToString());
                sb.Append("</SignAddons>");
                sb.Append("</Data>");
            }

            return sb.ToString();
        }



        #endregion



    }






}
