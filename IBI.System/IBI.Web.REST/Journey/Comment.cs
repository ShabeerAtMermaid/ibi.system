﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IBI.REST.Journey
{
    [DataContract(Namespace = "")]
    public class Comment
    {
        [DataMember]
        public string Body { get; set; }
    }
}