﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using IBI.Shared.Diagnostics;
using IBI.Shared;

namespace IBI.REST.Journey
{
    [XmlSerializerFormat]
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBIJourney", ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class JourneyPost : IJourneyPost
    {
        public Stream Ping(Stream request)
        {
            using (new IBI.Shared.CallCounter("JourneyPost.Ping"))
            {
                StreamReader reader = new StreamReader(request);
                string text = "Ping received: " + reader.ReadToEnd();
                System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                MemoryStream ms = new MemoryStream(encoding.GetBytes(text));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms;
            }
            
        }



        public Stream SubmitJourney(Stream request)
        {
            using (new IBI.Shared.CallCounter("JourneyPost.SubmitJourney"))
            {
                string result = "Failure";

                StreamReader reader = new StreamReader(request, Encoding.UTF8);

                string requestData = reader.ReadToEnd();

                requestData = System.Web.HttpContext.Current.Server.UrlDecode(requestData);

                //Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.JOURNEY, requestData);

                if (!String.IsNullOrEmpty(requestData))
                {
                    try
                    {
                        string[] arr = requestData.Split(new string[] { "@~@" }, StringSplitOptions.RemoveEmptyEntries);

                        int customerId = arr.Length > 0 ? int.Parse(arr[0]) : 0;
                        String xmlData = arr.Length > 0 ? arr[1] : "";


                        XmlDocument journeyDoc = new XmlDocument();
                        journeyDoc.LoadXml(xmlData);


                        bool isValidJourneyData = journeyDoc.SelectSingleNode("//Journey/BusNumber") != null;

                        if (!isValidJourneyData)
                        {
                            //invalid Journey data is posted from CLIENT to JourneySubmit()
                            //disregard post
                            //simply send back success message to client, so it should not SUBMIT that journey again.

                            string sender = "Unknown Source";
                            if (System.Web.HttpContext.Current != null)
                            {
                                sender = System.Web.HttpContext.Current.Request.UserHostAddress;
                            }

                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, " ::: Invalid Journey Data Submited (no bus number) @ " + sender + " ::: " + requestData);
                            result = "Success";
                            goto ReturnLabel;
                        }

                        int journeyNumber;

                        using (IBIDataModel dbContext = new IBIDataModel())
                        {

                            dbContext.xml_SubmitJourney(customerId, journeyDoc.DocumentElement.OuterXml, AppSettings.MoviaJourneySynLogEnabled());

                            result = "Success";
                        }

                        result = "Success";

                    }
                    catch (Exception ex)
                    {
                        Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY, ex + " ::: Journey Data ::: " + requestData);
                        result = "Failure";
                    }
                }

            ReturnLabel:
                System.Text.ASCIIEncoding rEncoding = new System.Text.ASCIIEncoding();
                MemoryStream ms = new MemoryStream(rEncoding.GetBytes(result));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms;

            }
            
        }
    }
}
