﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Xml.Linq;
using System.IO;
using System.Data.Entity;

namespace IBI.REST.Shared
{
    public class RestUtil
    {

        public static Hashtable ExtractPingValues(String pingData)
        {
            Hashtable pingValueTable = new Hashtable();

            if (!String.IsNullOrEmpty(pingData))
            {
                String[] pingValues = pingData.Split(';');

                for (int i = 0; i < pingValues.Length; i++)
                {
                    String valueData = pingValues[i];

                    //String[] valuePair = valueData.Split('=');

                    //avoiding '=' in value

                    if (!valueData.Contains('='))
                        continue;

                    String[] valuePair =  new String[2];

                    valuePair[0] = valueData.Substring(0, valueData.IndexOf('='));
                    valuePair[1] = valueData.Substring(valueData.IndexOf('=')+1);
                   

                    String valueName = valuePair[0];
                    String value = "";

                    if (valuePair.Length == 2)
                        value = valuePair[1];

                    pingValueTable[valueName] = value;
                }
            }

            return pingValueTable;
        }

        public static String GetClientIP()
        {
            try
            {

                String ip = HttpContext.Current.Request.UserHostAddress;
                //HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(ip))
                {
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }

                return ip;


            }
            catch { return ""; }
        }

        //public static string FixScheduleStopAddonsXML(string scheduleXml)
        //{
        //    StringReader rd = new StringReader(scheduleXml);
        //    XDocument xDoc = XDocument.Load(rd);

        //    foreach (var stopInfo in xDoc.Descendants("StopInfo"))
        //     {
        //        foreach (var addon in stopInfo.Descendants("Addon"))
        //        {
        //            var key = addon.Descendants("Key").FirstOrDefault();
        //            var val = addon.Descendants("Value").FirstOrDefault();

                    
        //            if (key != null) {
        //                XElement customElement = null;
        //                if(val!=null)
        //                {
        //                   string elementName =  key.Value.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
        //                   customElement  = new XElement(elementName, new XCData(val.Value.ToString()));         
        //                   XAttribute attr = new XAttribute("isAddOn", "true");
        //                   customElement.Add(attr);
        //                }

        //                stopInfo.Add(customElement);
        //            }
        //        }
        //        stopInfo.Descendants("ScheduleStopAddons").Remove();
        //     }

        //    return xDoc.ToString();
        //}

        //public static string FixScheduleAddonsXML(string scheduleXml, string rootNode)
        //{
        //    StringReader rd = new StringReader(scheduleXml);
        //    XDocument xDoc = XDocument.Load(rd);

        //    foreach (var scheduleInfo in xDoc.Descendants(rootNode))
        //    {
        //        foreach (var addon in scheduleInfo.Descendants("Addon"))
        //        {
        //            var key = addon.Descendants("Key").FirstOrDefault();
        //            var val = addon.Descendants("Value").FirstOrDefault();


        //            if (key != null)
        //            {
        //                XElement customElement = null;
        //                if (val != null)
        //                {
        //                    string elementName = key.Value.Replace(" ", "").Replace("<", "").Replace(">", "").Replace("&amp;", "").Replace("&", "");
        //                    customElement = new XElement(elementName, new XCData(val.Value.ToString()));
        //                    XAttribute attr = new XAttribute("isAddOn", "true");
        //                    customElement.Add(attr);
        //                }

        //                scheduleInfo.Add(customElement);
        //            }
        //        }
        //        scheduleInfo.Descendants("ScheduleAddons").Remove();
        //    }

        //    return xDoc.ToString();
        //}
    }
}