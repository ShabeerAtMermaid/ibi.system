﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Globalization;
using IBI.Shared.Models;
using IBI.REST.Shared;
using IBI.Shared;
using System.ServiceModel.Activation;

namespace IBI.REST.TTS.XML
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class TTSService : ITTSService
    {
        public TTSData GetTTSData(String customerId)
        {
            using (new IBI.Shared.CallCounter("TTSService.Xml.GetTTSData"))
            {
                return TTSServiceController.GetTTSData(customerId);
            }
            
        }

        public TTSData GetTTSDataFromDate(String fromDate)
        {
            using (new IBI.Shared.CallCounter("TTSService.Xml.GetTTSDataFromDate"))
            {
                return TTSServiceController.GetTTSDataFromDate(DateTime.Parse(fromDate, AppUtility.getDateFormatProvider()));       
            }
            
         }

        public TTSData GetUnfilteredTTSData()
        {
            using (new IBI.Shared.CallCounter("TTSService.Xml.GetUnfilteredTTSData"))
            {
                return TTSServiceController.GetUnfilteredTTSData();
            }
            
        }
    }
}
