﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mermaid.TTS;
using IBI.Shared;

namespace IBI.REST.TTS
{
    public class TTSFileHandler : IHttpHandler
    {
        #region Properties

        public bool IsReusable
        {
            get { return false; }
        }

        #endregion

        public void ProcessRequest(HttpContext context)
        {
            String filename = (String)context.Request["filename"];
            String customerid = (String)context.Request["customerid"];
            String voice = (String)context.Request["voice"];
            String stopname = (String)context.Request["stop"];

            Byte[] fileContent = null;

            if (!String.IsNullOrEmpty(filename) || !String.IsNullOrEmpty(stopname))
            {
                String key = string.Concat(filename, customerid, voice, stopname);

                object customerIDCache = context.Cache[key + "ttsfileexpiry"];
                if (customerIDCache != null && ((DateTime)customerIDCache).AddMinutes(AppSettings.TTSFileCache) > DateTime.Now)
                {
                    fileContent = (Byte[])context.Cache[key + "ttsfilecontent"];
                }
                else
                {
                    if (!(String.IsNullOrEmpty(voice)))
                    {
                        if (!String.IsNullOrEmpty(filename))
                        {
                            fileContent = TTSServiceController.GetTTSFileByVoice(filename, voice);
                        }
                        else
                        {
                            fileContent = TTSServiceController.GetTTSFileByStopName(stopname, voice);
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(filename))
                        {
                            fileContent = TTSServiceController.GetTTSFile(filename, customerid);
                        }
                        else
                        {
                            fileContent = TTSServiceController.GetTTSFileByStopName(stopname, String.IsNullOrEmpty(customerid) ? 0 : int.Parse(customerid));
                        }

                    }
                    if (fileContent != null)
                    {
                        context.Cache[key + "ttsfileexpiry"] = DateTime.Now;
                        context.Cache[key + "ttsfilecontent"] = fileContent;
                    }
                }

                if (fileContent != null)
                {
                    //Clear all content output from the buffer stream 
                    context.Response.Clear();

                    //Add a HTTP header to the output stream that specifies the filename 
                    //context.Response.AddHeader("Content-Disposition", "attachment; filename=" & tmpFileInfo.Name)

                    //Add a HTTP header to the output stream that contains the content length(File Size)
                    context.Response.AddHeader("Content-Length", fileContent.Length.ToString());

                    //Set the HTTP MIME type of the output stream 
                    context.Response.ContentType = "audio/mp3";

                    //Write the data out to the client. 
                    context.Response.BinaryWrite(fileContent);

                    context.Response.End();
                    return;
                }
                else
                {
                    throw new ApplicationException("File data not found");
                }
            }
            else
            {
                throw new ApplicationException("No filename specified");
            }

            throw new ApplicationException("Error downloading TTSFile");
        }
    }
}