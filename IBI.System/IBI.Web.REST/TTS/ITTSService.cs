﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using System.ServiceModel.Web;

namespace IBI.REST.TTS
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface ITTSService
    {
        [OperationContract]
        TTSData GetTTSData();

        [OperationContract]
        TTSData GetTTSDataFromDate(DateTime fromDate);

        [OperationContract]
        TTSData GetUnfilteredTTSData();

        [OperationContract]
        void SyncAudioFiles();

        [OperationContract]
        void SyncAudioFilesCache();

        [OperationContract]
        [WebGet(
             BodyStyle = WebMessageBodyStyle.Bare,
             RequestFormat = WebMessageFormat.Json,
             ResponseFormat = WebMessageFormat.Json
             //UriTemplate = "GetStopAudioFiles?stopName={stopName}&status={status}" 
            )]
        List<IBI.Shared.Models.TTS.AudioFile> GetStopAudioFiles(string stopName, bool approved, bool rejected, bool unapproved);


        [OperationContract]
        [WebGet(
             BodyStyle = WebMessageBodyStyle.Bare,
             RequestFormat = WebMessageFormat.Json,
             ResponseFormat = WebMessageFormat.Json,
             UriTemplate = "GetAudioFile?fileName={fileName}&voice={voice}"
            )]
       IBI.Shared.Models.TTS.AudioFile GetAudioFileByVoice(string filename, string voice);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        byte[] GenerateAudioFile(string stopName, string sourceText, string voice);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        byte[] GenerateAudioPreview(string stopName, string sourceText, string voice);
        //[OperationContract]
        //[WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        //string GenerateAudioFilePath(string stopName, string sourceText, string voice);


        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        bool SaveAudioFile(string stopName, string sourceText, string voice, string status);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        bool SaveAudioFileMp3(string stopName, string sourceText, string voice, string status, byte[] mp3, string alternateSourceVoiceText);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare)]
        List<String> GetAllSourceVoices();


    }
}
