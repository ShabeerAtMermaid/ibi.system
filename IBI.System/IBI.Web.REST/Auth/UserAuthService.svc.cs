﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;
using IBI.DataAccess;
using IBI.DataAccess.DBModels; 
using System.ServiceModel.Web;
using IBI.REST.Shared;
using IBI.Shared.Models.Accounts;
using IBI.Shared.Diagnostics;
using User = IBI.Shared.Models.Accounts.User;
using Auth = IBI.Shared.Models.Accounts.Auth;
using System.ServiceModel.Activation;

namespace IBI.REST.Auth
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")] 
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class UserAuthService : IUserAuthService
    {

        public IBI.Shared.Models.Accounts.Auth Authenticate(string username, string password, string apiKey)
        {
            using (new IBI.Shared.CallCounter ("Auth.Authenticate"))
            {
                return UserAuthServiceController.Authenticate(username, password, apiKey);
            }
            
        }

        public IBI.Shared.Models.Accounts.Auth Validate(string authToken)
        {
            using (new IBI.Shared.CallCounter("Auth.Validate"))
            {
                return UserAuthServiceController.Validate(authToken);
            }
            
        }

        //public User Authenticate(string username, string password)
        //{

        //    try
        //    {

        //        IBIDataModel dbContext = new IBIDataModel();
        //        User usr = (User)dbContext.IBIUsers.Where(x => x.Username == username).Select(x => new User()
        //        {
        //            UserId = x.UserId,
        //            Username = x.Username,
        //            Fullname = x.FullName,
        //            Password = x.Password

        //        }).FirstOrDefault();

        //        if (usr != null)
        //        {
        //            //user exists - now validate password
        //            if (usr.Password == password)
        //            {
        //                //password is correct
        //                return usr;
        //            }
        //            else
        //            {
        //                return new User("Password is incorrect");
        //            }

        //        }
        //        else
        //            return new User("Username doesn't exist");

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.appendToSystemLog(DataAccess.Diagnostics.Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
        //        throw ex;
        //    }

        //}


        public User GetUser(string userName)
        {
            using (new IBI.Shared.CallCounter("Auth.GetUser"))
            {
                return UserAuthServiceController.GetUser(userName);
            }
            
        }

        public bool ChangePassword(string userName, string newPassword)
        {
            using (new IBI.Shared.CallCounter("Auth.ChangePassword"))
            {
                return UserAuthServiceController.ChangePassword(userName, newPassword);
            }
            
        }

        public bool EditAccountSettings(IBI.Shared.Models.Accounts.User model)
        {
            using (new IBI.Shared.CallCounter("Auth.EditAccountSettings"))
            {
                return UserAuthServiceController.EditAccountSettings(model);
            }
           
        }


        public bool ConfirmAdminPassword(string username, string password)
        {
            using (new IBI.Shared.CallCounter("Auth.ConfirmAdminPassword"))
            {
                return UserAuthServiceController.ConfirmAdminPassword(username, password);
            }
            
        }
    }
}
