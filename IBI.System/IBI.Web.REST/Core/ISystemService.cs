﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace IBI.REST.Core
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface ISystemService
    {
        //[OperationContract]
        //[WebInvoke(Method = "GET",
        //   UriTemplate = "SyncAllSchedules")]
        //void SyncAllSchedules();

        [OperationContract]
        void DumpCurrentStatus();

        [OperationContract]
        void PerformDatabaseMaintenance();

        [OperationContract]
        void SyncAllSupportData();

        [OperationContract]
        void SyncAllBusesData();
    }
}
