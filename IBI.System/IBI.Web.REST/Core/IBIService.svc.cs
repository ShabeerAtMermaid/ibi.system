﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using IBI.REST.Shared;
using IBI.Shared.Diagnostics;
using IBI.DataAccess.DBModels;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Data.Sql;
using Microsoft.SqlServer.Types;
using System.Globalization;
using IBI.Shared;
using IBI.Shared.Models;
using IBI.Shared.Interfaces;
using IBI.Implementation.Schedular;
using System.Xml;
using IBI.Shared;
using System.ServiceModel.Activation;

namespace IBI.REST.Core
{
    [ServiceBehavior(Namespace = "http://schemas.mermaid.dk/IBI")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class IBIService : IIBIService
    {
        public void Ping(String busNo, String clientType, String pingData)
        {
            if (pingData.StartsWith("bus2land:"))
            {

                MessagePing(pingData.Substring(pingData.IndexOf(":") + 1));

                //Logger.appendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, "Bus2Land Ping Done: " + pingData.Substring(pingData.IndexOf(":") + 1));
                return;
            }


            //if (pingData.StartsWith("journeyinfo:"))
            //{
            //    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, "Journey Info Ping: " + pingData.Substring(pingData.IndexOf(":") + 1));

            //    JourneyPing(pingData.Substring(pingData.IndexOf(":") + 1));

            //    Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, "Journey Info Ping Done: " + pingData.Substring(pingData.IndexOf(":") + 1));
            //    return;
            //}

            StringBuilder logMessage = new StringBuilder();
            try
            {

                if (Boolean.Parse(ConfigurationManager.AppSettings["PingsEnabled"]))
                {
                    using (new IBI.Shared.CallCounter("IBIService.Ping"))
                    {
                        
                        Hashtable pingValues = Shared.RestUtil.ExtractPingValues(pingData);

                        int customerID = 0;
                        int.TryParse((String)pingValues["customerid"], out customerID);

                        string macAddress = pingValues.ContainsKey("mac") ? pingValues["mac"].ToString() : "";
                        string journeyNumber = pingValues.ContainsKey("journeynumber") ? pingValues["journeynumber"].ToString() : "0";
                        string schedule = pingValues.ContainsKey("schedule") ? pingValues["schedule"].ToString() : "";
                        string line = pingValues.ContainsKey("line") ? pingValues["line"].ToString() : "0";
                        string destination = pingValues.ContainsKey("destination") ? pingValues["destination"].ToString() : "";
                        string simID = pingValues.ContainsKey("simid") ? pingValues["simid"].ToString() : "";
                        string latitude = pingValues.ContainsKey("lat") ? pingValues["lat"].ToString() : "0";
                        string longitude = pingValues.ContainsKey("lon") ? pingValues["lon"].ToString() : "0";

                        if (string.IsNullOrEmpty(journeyNumber))
                            journeyNumber = "0";


                        if (!string.IsNullOrEmpty(busNo))
                        {
                            //Logger.appendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, new Exception("Ping from Bus:[" + busNo + "], Type:[" + clientType + "]"));

                            if (!string.IsNullOrEmpty(clientType))
                            {
                                SqlConnection con = AppSettings.GetIBIDatabaseConnection();
                                SqlCommand cmd = new SqlCommand("Ping", con);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("BusNo", busNo);
                                cmd.Parameters.AddWithValue("ClientType", clientType);
                                cmd.Parameters.AddWithValue("CustomerId", customerID);
                                cmd.Parameters.AddWithValue("MacAddress", macAddress);
                                cmd.Parameters.AddWithValue("JourneyNumber", journeyNumber);
                                cmd.Parameters.AddWithValue("ScheduleId", string.IsNullOrEmpty(schedule) ? null : schedule);
                                cmd.Parameters.AddWithValue("Line", string.IsNullOrEmpty(line) ? null : line);
                                cmd.Parameters.AddWithValue("Destination", string.IsNullOrEmpty(destination) ? null : destination);
                                cmd.Parameters.AddWithValue("SimID", simID);
                                cmd.Parameters.AddWithValue("Latitude", latitude);
                                cmd.Parameters.AddWithValue("Longitude", longitude);
                                cmd.Parameters.AddWithValue("UpdateLastTime", false);

                                try
                                {
                                    con.Open();
                                    cmd.ExecuteNonQuery();

                                }
                                catch (Exception exInner)
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "Bus: " + busNo + ", Ping Error: " + exInner);
                                }
                                finally
                                {
                                    if (con.State != ConnectionState.Closed)
                                        con.Close();
                                }

                            }
                            else
                            {
                                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Ping received with empty clienttype. Mac: " + macAddress);
                            }
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Ping received with empty busno. Mac: " + macAddress);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, "Exception : " + ex.Message);
            }
        }


        public string MessagePing(string pingData)
        {

            string retVal = "FAILED";

            StringBuilder logMessage = new StringBuilder();

            try
            {
                if (AppSettings.MessagePing == true)
                {
                    using (new IBI.Shared.CallCounter("IBIService.MessagePing"))
                    {
                        pingData = System.Web.HttpContext.Current.Server.UrlDecode(pingData);

                        Hashtable pingValues = Shared.RestUtil.ExtractPingValues(pingData);

                        DateTime? displayTime = null;
                            
                        if(pingValues.ContainsKey("displayTime")){
                            displayTime = DateTime.ParseExact(pingValues["displayTime"].ToString(), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        }
                                                

                        int messageId = pingValues.ContainsKey("messageId") ? int.Parse(pingValues["messageId"].ToString()) : 0;

                        string busNumber = pingValues.ContainsKey("busNumber") ? pingValues["busNumber"].ToString() : "";
                        string customerId = pingValues.ContainsKey("customerId") ? pingValues["customerId"].ToString() : "";

                        string strTimestamp = pingValues.ContainsKey("timestamp") ? pingValues["timestamp"].ToString() : "";

                        DateTime timestamp = DateTime.ParseExact(strTimestamp, "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        string line = pingValues.ContainsKey("line") ? pingValues["line"].ToString() : "";
                        string destination = pingValues.ContainsKey("destination") ? pingValues["destination"].ToString() : "";
                        string nextStop = pingValues.ContainsKey("nextStop") ? pingValues["nextStop"].ToString() : "";

                        //string subject = pingValues.ContainsKey("title") ? pingValues["title"].ToString() : "";
                        string MessageText = pingValues.ContainsKey("messageText") ? pingValues["messageText"].ToString() : "";
                        string macAddress = pingValues.ContainsKey("mac") ? pingValues["mac"].ToString() : "";
                        string latitude = pingValues.ContainsKey("lat") ? pingValues["lat"].ToString() : "0";
                        string longitude = pingValues.ContainsKey("lon") ? pingValues["lon"].ToString() : "0";

                        if (!string.IsNullOrEmpty(busNumber))
                        {

                            if (customerId == "")
                                customerId = DBHelper.GetBusCustomerId(busNumber).ToString();

                            if (displayTime != null && messageId>0) { 
                                //saved display confirmation only
                                try 
                                {
                                    int custId = int.Parse(customerId);
                                    using (IBIDataModel dbContext = new IBIDataModel())
                                    {
                                        bool isNew = false;
                                        var displayConfirmation = dbContext.Land2BusMessages.Where(m => m.MessageId == messageId).FirstOrDefault().Land2BusMessageDisplayConfirmations.Where(b => b.BusNumber == busNumber && b.CustomerId == custId).FirstOrDefault();
                                        
                                        if (displayConfirmation == null) 
                                        {
                                            isNew = true;
                                            displayConfirmation = new DataAccess.DBModels.Land2BusMessageDisplayConfirmations();
                                        }

                                        displayConfirmation.MessageId = messageId;
                                        displayConfirmation.BusNumber = busNumber;
                                        displayConfirmation.CustomerId = custId;
                                        displayConfirmation.DisplayTime = displayTime.Value;


                                        if(isNew){
                                            dbContext.Land2BusMessageDisplayConfirmations.Add(displayConfirmation);
                                        }

                                        dbContext.SaveChanges();
                                        retVal = "SUCCESS";
                                        return retVal;
                                    }    
                                }
                                catch (Exception exDisplay) {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Bus2Land, "Bus: " + busNumber + ", Message display confirmation Error: " + exDisplay);
                                }
                            }

                            if (!string.IsNullOrEmpty(MessageText))
                            {
                                try
                                {
                                    IBI.Shared.Models.Messages.IncomingMessage msg = new IBI.Shared.Models.Messages.IncomingMessage();
                                    msg.BusNumber = busNumber;
                                    msg.CustomerId = int.Parse(customerId);
                                    msg.DateAdded = timestamp;
                                    msg.DateModified = timestamp;
                                    msg.MessageText = MessageText;
                                    //msg.ClientId = "?" to be determined while saving
                                    msg.Destination = destination;
                                    msg.GPS = "";
                                    msg.Latitude = latitude;
                                    msg.Line = line;
                                    msg.Longitude = longitude;
                                    msg.MacAddress = macAddress;
                                    msg.NextStop = nextStop;
                                    msg.IsArchived = false;
                                    if (messageId != 0) {
                                        msg.OutgoingMessageid = messageId; //this would be the original message sent from Land2Bus
                                    }
                                    
                                    bool result = Messaging.MessageServiceController.SaveIncomingMessage(msg);
                                    retVal = result ? "SUCCESS" : "FAILED";

                                }
                                catch (Exception exInner)
                                {
                                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "Bus: " + busNumber + ", MessagePing Error: " + exInner);
                                }
                            }
                            else
                            {
                                Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Message Ping received with empty text message. MAC:" + macAddress);
                            }
                        }
                        else
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.IBI, "Ping received with empty busno. Mac: " + macAddress);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, "Exception : " + ex.Message);
            }

            return retVal;
        }

        public string MessageReplyPing(string messageData)
        {
            string retVal = "FAILED";

            //StringBuilder logMessage = new StringBuilder();

            //try
            //{

            //    using (new IBI.Shared.CallCounter("IBIService.MessageReplyPing"))
            //    {
            //        Hashtable pingValues = Shared.RestUtil.ExtractPingValues(messageData);

            //        string busNumber = pingValues.ContainsKey("busnumber") ? pingValues["busnumber"].ToString() : "";
            //        int customerId = pingValues.ContainsKey("customerid") ? int.Parse(pingValues["customerid"].ToString()) : 0;

            //        DateTime? displayTime = pingValues.ContainsKey("displaytime")
            //            ? DateTime.ParseExact(pingValues["displaytime"].ToString(), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
            //            : null;
                     
            //        int messageId = pingValues.ContainsKey("messageid") ? int.Parse(pingValues["messageid"]) : 0;

            //        string reply = pingValues.ContainsKey("reply") ? pingValues["reply"].ToString() : "";

            //        DateTime? replyTime = pingValues.ContainsKey("replytime")
            //            ? DateTime.ParseExact(pingValues["replytime"].ToString(), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture)
            //            : null;
 
                   
            //                try
            //                {
            //                    IBI.Shared.Models.Messages.IncomingMessage msg = new IBI.Shared.Models.Messages.IncomingMessage();
            //                    msg.BusNumber = busNumber;
            //                    msg.CustomerId = customerId;
            //                    msg.DateAdded = displayTime != null ? displayTime : replyTime;
            //                    msg.DateModified = DateTime.Now;
            //                    msg.MessageText = reply;
            //                    //msg.ClientId = "?" to be determined while saving
            //                    msg.Destination = destination;
            //                    msg.GPS = "";
            //                    msg.Latitude = latitude;
            //                    msg.Line = line;
            //                    msg.Longitude = longitude;
            //                    msg.MacAddress = macAddress;
            //                    msg.NextStop = nextStop;
            //                    msg.IsArchived = false;

            //                    bool result = Messaging.MessageServiceController.SaveIncomingMessage(msg);
            //                    retVal = result ? "SUCCESS" : "FAILED";

            //                }
            //                catch (Exception exInner)
            //                {
            //                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBIData, "Bus: " + busNumber + ", MessagePing Error: " + exInner);
            //                }
            //            }
                       
            //        }
                    
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, "Exception : " + ex.Message);
            //}

            return retVal;
        }


        public string JourneyPing(String pingData)
        {
            return string.Empty;
        }

        public String GetCommand(String busNo, String clientType)
        {
            Thread.Sleep(TimeSpan.FromMinutes(5));

            return String.Empty;
        }


        #region Private Helpers

        private static string AppendStopInfo(string realTimeSchedule, IBI.Shared.Models.BusJourney.StopInfo stopInfo, string busNumber, string line, string fromName, string destination)
        {

            //extract currentStop's info from given XML

            XmlDocument xDoc = new XmlDocument();

            if (realTimeSchedule == null || realTimeSchedule == "")
            {
                realTimeSchedule = String.Format(@"<?xml version=""1.0"" encoding=""utf-16""?>
                                <RealTimeData xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
                                  <BusNumber>{0}</BusNumber>
                                  <Line>{1}</Line>
                                  <JourneyNumber>{2}</JourneyNumber>
                                  <FromName>{3}</FromName>
                                  <DestinationName>{4}</DestinationName>
                                  <JourneyStops>    
                                  </JourneyStops>
                                </RealTimeData>", busNumber, line, stopInfo.JourneyNumber, fromName, destination);
            }

            xDoc.LoadXml(realTimeSchedule);

            XmlNode stops = xDoc.SelectSingleNode("RealTimeData/JourneyStops");
            String xmlStopInfo = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(stopInfo));

            XmlDocument xStopDoc = new XmlDocument();
            xStopDoc.LoadXml(xmlStopInfo);
            XmlNode stop = xStopDoc.SelectSingleNode("StopInfo");
            String xmlStop = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(stopInfo));


            XmlElement newStop = xDoc.CreateElement("StopInfo");
            newStop.InnerXml = stop.InnerXml;

            stops.AppendChild(newStop);

            realTimeSchedule = xDoc.InnerXml;


            return realTimeSchedule;
        }

        #endregion
    }
}
