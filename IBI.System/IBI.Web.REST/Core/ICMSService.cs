﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models;

namespace IBI.REST.Core
{
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBI")]
    public interface ICMSService
    {
        [OperationContract]
        Bus[] getBusList();

        [OperationContract]
        IBI.Shared.Models.Schedule[] getBusJourneyHistory(int busNumber, DateTime date);

        [OperationContract]
        StopInfo[] getJourneyHistory(int busNumber, DateTime date, String line, long journeyNumber);
    }
}
