﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IBI.Shared.Models.Language;
using System.ServiceModel.Web;

namespace IBI.REST.Language
{
    [XmlSerializerFormat]
    [ServiceContract(Namespace = "http://schemas.mermaid.dk/IBILanguage")]
    public interface ILanguageService
    {
        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "GetDCULanguage?language={LANGUAGE}")]
        List<LanguageLabel> GetDCULanguage(string language);

        [OperationContract]
        [WebGet(
            BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Xml,
            UriTemplate = "GetWebLanguage?language={LANGUAGE}")]
        List<LanguageLabel> GetWebLanguage(string language);
    }
}
