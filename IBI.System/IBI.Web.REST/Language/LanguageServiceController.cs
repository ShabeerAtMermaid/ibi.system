﻿using IBI.DataAccess.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IBI.Shared.Models.Language;
using System.Data.Objects.DataClasses;
using System.Reflection;

namespace IBI.REST.Language
{
    public class LanguageServiceController
    {
        private static void AddLanguageTag(List<LanguageLabel> list, LanguageDefinitionsDCU dbLabel)
        {
            LanguageLabel label = new LanguageLabel();
            label.Culture = dbLabel.Culture;
            label.LabelId = dbLabel.Id ?? String.Empty;
            label.Title = dbLabel.Text ?? String.Empty;

            list.Add(label);
        }

        private static void AddLanguageTag(List<LanguageLabel> list, LanguageDefinitionsWeb dbLabel)
        {
            LanguageLabel label = new LanguageLabel();
            /*KHI: Uncomment it after db change
            label.Culture = dbLabel.Culture;
            label.LabelId = dbLabel.Id ?? String.Empty;
            label.Title = dbLabel.Text ?? String.Empty;
            */
            list.Add(label);
        }

        public static List<LanguageLabel> GetDCULanguageTags(string language)
        {
            List<LanguageLabel> lstTitles = new List<LanguageLabel>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {
                try
                {
                    var LanguageTags = dbContext.LanguageDefinitionsDCUs.ToList();

                    foreach (LanguageDefinitionsDCU dbLabel in LanguageTags)
                    {
                        AddLanguageTag(lstTitles, dbLabel);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return lstTitles;
            }
        }

        //KHI: Remove this method after DB change
        private static LanguageLabel GetLabel(string language, LanguageDefinitionsWeb dbLabel)
        {
            return String.Compare(language, "en-US", true) == 0 ? new LanguageLabel(dbLabel.Id, dbLabel.English) : new LanguageLabel(dbLabel.Id, dbLabel.Danish);
        }

        public static List<LanguageLabel> GetWebLanguageTags(string language)
        {
            List<LanguageLabel> lstTitles = new List<LanguageLabel>();
            using (IBIDataModel dbContext = new IBIDataModel())
            {

                try
                {
                    var LanguageTags = dbContext.LanguageDefinitionsWebs.ToList();

                    foreach (LanguageDefinitionsWeb dbLabel in LanguageTags)
                    {
                        lstTitles.Add(GetLabel(language, dbLabel));//KHI:Comment it after db change
                        //KHI: Uncomment it after DB change AddLanguageTag(lstTitles, dbLabel);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return lstTitles;
            }
        }
    }
}