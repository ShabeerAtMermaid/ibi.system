﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
using System.Reflection;

namespace Acapela.Vaas
{
    /// <summary>
    /// This class performs the core functions of the service: generating messages and retrieving generated message. 
    /// 
    /// Generation and retrieving operations are parametrized by Message or MessageExt objects and their results is also stored in instance of this classes. 
    /// Please read Message documentation before using this class.
    /// 
    /// You can access to a ready to use Synthesizer instance through the synthesizer property of the Connector class.
    /// </summary>
    class Synthesizer
    {
        private Message requestedMessage = null;
        private Sound sound = null;
        private Connector connector;

        public String DownloadPath
        {
            get;
            set;
        }

        /// <summary>
        /// [static] Dispatched when a message was obtained as a result to the call of generate/retrieveMessage. Synthesizer 
        /// </summary>
        public static readonly string MESSAGE_OBTAINED = "messageObtained";

        /// <summary>
        /// [static] Dispatched when the download of the sound corresponding to the request message ended. Synthesizer 
        /// </summary>
        public static readonly string SOUND_DOWNLOAD_ENDED = "soundDownloadEnded";

        /// <summary>
        /// [static] Dispatched when the download of the sound corresponding to the request message started. 
        /// </summary>
        public static readonly string SOUND_DOWNLOAD_STARTED = "soundDownloadStarted";

        public class SoundDownloadEndedEventArgs : EventArgs    // TODO: move?
        {
            private string filename;

            public SoundDownloadEndedEventArgs(string filename)
            {
                this.filename = filename;
            }
            public string Filename
            {
                get { return filename; }
                set { filename = value; }
            }
        }

        public event EventHandler MessageObtained;
        public event EventHandler<SoundDownloadEndedEventArgs> SoundDownloadEnded;
        public event EventHandler SoundDownloadStarted;

        /// <summary>
        /// [read-only] The Message instance corresponding to the last request. 
        /// </summary>
        public Message RequestedMessage
        {
            get
            {
                return requestedMessage;
            }
        }

        /// <summary>
        /// [read-only] The Sound instance corresponding to the last request. 
        /// </summary>
        public Sound RequestedSound
        {
            get
            {
                return RequestedSound;
            }
        }

        internal Synthesizer(Connector connector)
        {
            this.connector = connector;
        }

        /// <summary>
        /// Post a sound file generation request on the service.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="downloadAssociatedSound"></param>
        /// <param name="timeOut"></param>
        public void GenerateMessage(Message msg, bool downloadAssociatedSound, uint timeOut)
        {
            requestedMessage = msg;
            // TODO: handle timeout value => pass with http request?
            Dictionary<string, string> ret = SendVaasHttpRequest();
            if (ret["res"] != "OK")
            {
                throw new Exception(ret["err_code"] + "\r\n" + ret["err_msg"]);
            }

            // Save message/sound ID
            msg.id = ret["snd_id"];
            msg.soundURL = ret["snd_url"];
            msg.soundSize = uint.Parse(ret["snd_size"]);
            msg.soundLength = TimeSpan.FromMilliseconds(double.Parse(ret["snd_time"], System.Globalization.NumberFormatInfo.InvariantInfo));
            if (downloadAssociatedSound)
            {
                if (SoundDownloadStarted != null)
                {
                    SoundDownloadStarted(this, EventArgs.Empty);
                }
                string url = ret["snd_url"];
                WebClient client = new WebClient();
                Uri uri = new Uri(url, UriKind.RelativeOrAbsolute);
                string filename = uri.Segments[uri.Segments.Length - 1]; // TODO: correct filename?

                string directoryPath = this.DownloadPath;

                if (String.IsNullOrEmpty(directoryPath))
                    directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                filename = Path.Combine(directoryPath, filename);

                client.DownloadFile(url, filename);

                if (SoundDownloadEnded != null)
                {
                    SoundDownloadEnded(this, new SoundDownloadEndedEventArgs(filename));
                }
            }
        }

        /// <summary>
        /// Post a sound file generation request on the service.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="downloadAssociatedSound"></param>
        public void GenerateMessage(Message msg, bool downloadAssociatedSound)
        {
            GenerateMessage(msg, downloadAssociatedSound, 30000);
        }

        /// <summary>
        /// Post a sound file generation request on the service.
        /// </summary>
        /// <param name="msg"></param>
        public void GenerateMessage(Message msg)
        {
            GenerateMessage(msg, true);
        }

        /// <summary>
        /// Post a message localization/regeneration request to the service. 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="downloadAssociatedSound"></param>
        /// <param name="timeOut"></param>
        public void RetrieveMessage(Message msg, bool downloadAssociatedSound, uint timeOut)
        {
            // TODO:
        }

        /// <summary>
        /// Post a message localization/regeneration request to the service. 
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="downloadAssociatedSound"></param>
        public void RetrieveMessage(Message msg, bool downloadAssociatedSound)
        {
            RetrieveMessage(msg, downloadAssociatedSound, 30000);
        }

        /// <summary>
        /// Post a message localization/regeneration request to the service. 
        /// </summary>
        /// <param name="msg"></param>
        public void RetrieveMessage(Message msg)
        {
            RetrieveMessage(msg, true);
        }

        // TODO: move to connector?
        private Dictionary<string, string> SendVaasHttpRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(connector.serviceIdentifier + "Synthesizer");
            request.Method = "POST";
            //string parameters = "prot_vers=2&cl_env=APACHE_2.2.9_PHP_5.5&cl_vers=1-00&cl_login=EVAL_VAAS&cl_app=EVAL_VAAS_699055&cl_pwd=ovxufkd3&req_voice=heather22k&req_text=hello&";
            string parameters = "prot_vers=2&cl_env=APACHE_2.2.9_PHP_5.5&cl_vers=1-00" +
                 "&cl_login=" + HttpUtility.UrlEncode(connector.username) +
                 "&cl_app=" + HttpUtility.UrlEncode(connector.appIdentifier) +
                 "&cl_pwd=" + HttpUtility.UrlEncode(connector.password) +
                 "&req_voice=" + HttpUtility.UrlEncode(requestedMessage.Voice) +
                 "&req_text=" + HttpUtility.UrlEncode(requestedMessage.Text) + "&"; // TODO: remove last &?
            byte[] buffer = Encoding.ASCII.GetBytes(parameters);
            request.ContentLength = buffer.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            Stream stream = request.GetRequestStream();
            stream.Write(buffer, 0, buffer.Length);
            stream.Close();

            // TODO: split into send and receive parts using Begin/End members...
            // TODO: call event MessageObtained

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            stream = response.GetResponseStream();
            StreamReader sr = new StreamReader(stream);
            string answer = sr.ReadToEnd();

            string[] responseParameters = answer.Split('&');
            Dictionary<string, string> values = new Dictionary<string, string>();
            foreach (string param in responseParameters)
            {
                string[] pair = param.Split('=');
                values.Add(pair[0], pair[1]);
            }
            return values;
        }
    }
}
