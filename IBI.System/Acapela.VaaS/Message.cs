﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acapela.Vaas
{
    /// <summary>
    /// This class contains the details of a VaaS message. 
    /// 
    /// A message is used to communicate with the service about the details of a VaaS message. It is both: 
    /// • the main argument of core functions such as generateMessage and retrieveMessage
    /// • the main result produced by this core functions beside the Sound object
    /// 
    /// The read-only properties can be setted only by the service: soundLength, soundURL, soundSize and accessCounter.
    /// The others can alternatively be setted by you to drive the service or by the service to provide you information. 
    /// 
    /// When you want to generate a message, usually you create a message and set the voice, text and, optionnaly, comment properties.
    /// The API will add the id property and the read-only properties after receiving the service answer. Then you will receive a complete message as result of the call to createMessage.
    /// 
    /// When you want to retrieve an existing message, you create a message and just set its id.
    /// The API will retrieve all the missing properties using the service and you will receive a complete message as result of the call to retrieveMessage.
    /// 
    /// Note that when you want to create a message, you can also set the id property to force the service to create a message with a predifined ID or override an existing message. 
    /// 
    /// If you have specific constraints for the sound format, you might be interested by the MessageExt additionnal features. 
    /// </summary>
    class Message
    {
        private uint accessCounter = 0;
        private string comment = "";
        internal string id = "";
        internal TimeSpan soundLength;
        internal uint soundSize = 0;
        internal string soundURL;
        private string text = "";
        private string voice = "";

        /// <summary>
        /// [read-only] Number of times that this message was accessed (using retrieveMessage function). 
        /// </summary>
        public uint AccessCounter
        {
            get
            {
                return accessCounter;
            }
        }

        /// <summary>
        /// The comment associated to a message. 
        /// </summary>
        public string Comment
        {
            get
            {
                return comment;
            }
        }

        /// <summary>
        /// The identifier of the message. Message 
        /// </summary>
        public string Id
        {
            get
            {
                return id;
            }
        }

        /// <summary>
        /// [read-only] Duration in milliseconds of the sound associated to the message. Message 
        /// </summary>
        public TimeSpan SoundLength
        {
            get
            {
                return soundLength;
            }
        }


        /// <summary>
        /// [read-only] Size in octets of the sound associated to the message. Message 
        /// </summary>
        public uint SoundSize
        {
            get
            {
                return soundSize;
            }
        }

        /// <summary>
        /// [read-only] URL of the sound associated to the message. Message 
        /// </summary>
        public string SoundURL
        {
            get
            {
                return soundURL;
            }
        }

        /// <summary>
        /// The text of the message that will be synthesized. Message 
        /// </summary>
        public string Text
        {
            get
            {
                return text;
            }
        }
        /// <summary>
        /// The voice used to synthesize the text of the message. 
        /// </summary>
        public string Voice
        {
            get
            {
                return voice;
            }
        }

        internal Message(string voice, string text)
        {
            this.voice = voice;
            this.text = text;
        }

        // TODO: make internal?
        public Message()
        {
        }
    }
}
