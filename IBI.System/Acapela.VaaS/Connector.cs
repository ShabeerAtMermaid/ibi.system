﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acapela.Vaas
{
    class Connector
    {
        private string lastError = "";
        private string lastWarning = "";
        private Synthesizer synthesizer = null;
        internal string serviceIdentifier;
        internal string password;
        internal string appIdentifier;
        internal string username;

        /// <summary>
        /// [static] Dispatched when an error occured while calling a method of the API.
        /// </summary>
        public static readonly string ERROR = "error";

        /// <summary>
        /// [static] Dispatched when a warning was received from the service.
        /// </summary>
        public static readonly string WARNING = "warning";

        /// <summary>
        /// [read-only] The VaasError instance describing the error that just happened.
        /// </summary>
        public string LastError
        {
            get
            {
                return lastError;
            }
        }

        /// <summary>
        /// [read-only] The warning message that was just received.
        /// </summary>
        public string LastWarning
        {
            get
            {
                return lastWarning;
            }
        }
        
        public String DownloadPath
        {
            get
            {
                return this.Synthesizer.DownloadPath;
            }
            set
            {
                this.Synthesizer.DownloadPath = value;
            }
        }

        /// <summary>
        /// [read-only] the Synthesizer instance associated to this Connector instance.
        /// </summary>
        public Synthesizer Synthesizer
        {
            get
            {
                return synthesizer;
            }
        }

        /// <summary>
        /// Create a new connector object instance.
        /// </summary>
        /// <param name="serviceIdentifier">serviceIdentifier the service identifier that Acapela gaves to you, do not forget the ending slash "/" if your identifier is an URL.</param>
        /// <param name="customerPassword">customerPassword the password you received from Acapela.</param>
        public Connector(string serviceIdentifier, string appIdentifier, string customerUsername, string customerPassword)
        {
            this.serviceIdentifier = serviceIdentifier;
            this.password = customerPassword;
            this.appIdentifier = appIdentifier;
            this.username = customerUsername;
            synthesizer = new Synthesizer(this);
        }
    }
}
