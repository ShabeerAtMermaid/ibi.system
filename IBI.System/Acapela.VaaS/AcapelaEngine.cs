﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Mermaid.TTS;
using IBI.DataAccess.DBModels;

namespace Acapela.Vaas
{
    public class AcapelaEngine : ITTSEngine
    {
        #region Properties

        private BasicVaas AcapelaAPI
        {
            get;
            set;
        }

        private String Voice
        {
            get;
            set;
        }

        #endregion

        public AcapelaEngine(String serviceUsername, String servicePassword, String serviceURL, String serviceAppId, String voice)
        {
            this.Voice = voice;
            this.AcapelaAPI = new BasicVaas(serviceUsername, servicePassword, serviceURL, serviceAppId);
        }

        private string FixText(string sourceText)
        {
            return sourceText;
        }

        private String GenerateFixedAudio(string text, string targetFilePath)
        {
            String fixedText = text;
            if (this.AcapelaAPI != null)
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    try
                    {
                        
                        IQueryable<Abbreviation> abbreviations = (from x in dbContext.Abbreviations
                                                                  select x);
                        foreach (var abbr in abbreviations)
                        {
                            fixedText = fixedText.Replace(abbr.AbbreviationText, abbr.FullText);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error fixing abbreviations for " + text, ex);
                    }

                }
                this.AcapelaAPI.GenerateMessage(this.Voice, fixedText);

                if (this.AcapelaAPI.RequestedSound != null)
                {
                    FileInfo sourceFile = new FileInfo(this.AcapelaAPI.RequestedSound.Filename);
                    FileInfo targetFile = new FileInfo(targetFilePath);

                    try
                    {
                        if (targetFile.Exists)
                            targetFile.Delete();

                        if (!targetFile.Directory.Exists)
                            targetFile.Directory.Create();

                        File.Move(sourceFile.FullName, targetFile.FullName);
                    }
                    catch (Exception ex)
                    {
                        throw new IOException("Error saving Audio file", ex);
                    }

                    try
                    {
                        if (File.Exists(sourceFile.FullName))
                            sourceFile.Delete();
                    }
                    catch (Exception ex)
                    {
                        throw new IOException("Error removing temporary Audio file", ex);
                    }
                }
                else
                {
                    throw new NullReferenceException("Error downloading Audio file from Acapela. RequestedSound is null");
                }
            }
            else
            {
                throw new ObjectDisposedException("AcapelaEngine", "This TTSEngine has been disposed and cannot be used anymore");
            }
            return fixedText;
        }

        public void GenerateAudioFile(string text, string targetFilePath, ref String fixedText)
        {
            fixedText = GenerateFixedAudio(text, targetFilePath);
        }

        public void GenerateAudioFile(string text, string targetFilePath)
        {
            GenerateFixedAudio(text, targetFilePath);
        }

        public void Dispose()
        {
            this.Voice = null;

            //UNDONE: Dispose AcapelaAPI
            //if (this.AcapelaAPI != null)
            //    this.AcapelaAPI.Dispose();

            this.AcapelaAPI = null;
        }
    }
}
