﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acapela.Vaas
{
    /// <summary>
    /// This class is an extension of the Message class. 
    /// 
    /// You may use it for advanced tunning of the audio format.
    /// 
    /// This class enables you for:
    /// • specifying the compression settings to use for the message generation
    /// • specifying an alternative compression to get the service generating two sounds for two differents targets (mobile phone and computer for instance)
    /// • determinate wether you want the API to download the main sound or the alternative sound.
    /// 
    /// When you need one of this features you create and configure a MessageExt instead of a Message.
    /// Then you pass it to generate/retrieveMessage.
    /// The Message instance sent as a result of this functions will be in fact a MessageExt instance.
    /// 
    /// As for the Message class, the read-write properties can be setted by you when generating a message or by the API when retrieving one. 
    /// One exception: the downloadAltSnd property that is never retrieved by the service as it is an instruction for the Synthesizer class.
    /// </summary>
    class MessageExt : Message
    {
        private string altCompression = DEFAULT_MP3_COMPRESSION;
        private uint altSoundSize = 0;
        private string altSoundURL;
        private string comment;
        private string compression;
        private bool downloadAltSnd;

        /// <summary>
        /// [static] Constant bit rate 16 kbits per seconds MP3 compression. MessageExt 
        /// </summary>
        public static readonly string CBR_16_MP3_COMPRESSION = "CBR_16";

        /// <summary>
        /// [static] Constant bit rate 24 kbits per seconds MP3 compression. MessageExt 
        /// </summary>
        public static readonly string CBR_24_MP3_COMPRESSION = "CBR_24";

        /// <summary>
        /// [static] Constant bit rate 32 kbits per seconds MP3 compression. MessageExt 
        /// </summary>
        public static readonly string CBR_32_MP3_COMPRESSION = "CBR_32";

        /// <summary>
        /// [static] Constant bit rate 48 kbits per seconds MP3 compression. MessageExt 
        /// </summary>
        public static readonly string CBR_48_MP3_COMPRESSION = "CBR_48";

        /// <summary>
        /// [static] Constant bit rate 8 kbits per seconds MP3 compression. MessageExt 
        /// </summary>
        public static readonly string CBR_8_MP3_COMPRESSION = "CBR_8";

        /// <summary>
        /// [static] Default value for (alternative) compression - Will let the service choose the compression to use. MessageExt 
        /// </summary>
        public static readonly string DEFAULT_MP3_COMPRESSION = "";

        /// <summary>
        /// [static] Variable bit rate MP3 compression - Quality level = 5 (9=min, 5=max)  MessageExt 
        /// </summary>
        public static readonly string VBR_5_MP3_COMPRESSION = "VBR_5";

        /// <summary>
        /// [static] Variable bit rate MP3 compression - Quality level = 6 (9=min, 5=max)  MessageExt 
        /// </summary>
        public static readonly string VBR_6_MP3_COMPRESSION = "VBR_6";

        /// <summary>
        /// [static] Variable bit rate MP3 compression - Quality level = 7 (9=min, 5=max)  MessageExt 
        /// </summary>
        public static readonly string VBR_7_MP3_COMPRESSION = "VBR_7";

        /// <summary>
        /// [static] Variable bit rate MP3 compression - Quality level = 8 (9=min, 5=max)  MessageExt 
        /// </summary>
        public static readonly string VBR_8_MP3_COMPRESSION = "VBR_8";

        /// <summary>
        /// [static] Variable bit rate MP3 compression - Quality level = 9 (9=min, 5=max)  
        /// </summary>
        public static readonly string VBR_9_MP3_COMPRESSION = "VBR_9";

        /// <summary>
        /// The compression of an alternative sound to associate to the message. MessageExt 
        /// </summary>
        public string AltCompression
        {
            get
            {
                return altCompression;
            }
        }

        /// <summary>
        /// [read-only] Size in octets of the alternative sound associated to the message. MessageExt 
        /// </summary>
        public uint AltSoundSize
        {
            get
            {
                return altSoundSize;
            }
        }

        /// <summary>
        /// [read-only] URL of the alternative sound associated to the message. 
        /// </summary>
        public string AltSoundURL
        {
            get
            {
                return altSoundURL;
            }
        }

        /// <summary>
        /// The compression of sound associated to the message. 
        /// </summary>
        public string Compression
        {
            get
            {
                return compression;
            }
        }

        /// <summary>
        /// set to true to ask the Synthesizer to dowload the alternative sound instead of the main one. 
        /// </summary>
        public bool DownloadAltSnd
        {
            get
            {
                return downloadAltSnd;
            }
        }
    }
}
