﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acapela.Vaas
{
    class VaasError
    {
        /// <summary>
        /// [read-only] A description message that can have various characteristics (size, line count, style...)
        /// </summary>
        public readonly string Details;

        /// <summary>
        /// [read-only] Defining the category of the error, must be one of the constants defined in this class. 
        /// </summary>
        public readonly string Type;

        /// <summary>
        /// [static] Error type used when the service refused your connexion (usually servicePassword error)  
        /// </summary>
        public static readonly string ACCESS_DENIED_ERROR = "ACCESS_DENIED_ERROR";

        /// <summary>
        /// [static] Error type used when the request failed at connexion step (usually serviceIdentifier error). 
        /// </summary>
        public static readonly string CONNEXION_ERROR = "CONNEXION_ERROR";

        /// <summary>
        /// [static] Error type used when an error related to Flash security occured. 
        /// </summary>
        public static readonly string FLASH_SECURITY_ERROR = "FLASH_SECURITY_ERROR";

        /// <summary>
        /// [static] Error type used when trying to access to a message not available on the service (deleted, moved, never created...)  
        /// </summary>
        public static readonly string ID_NOT_AVAILABLE_ERROR = "ID_NOT_AVAILABLE_ERROR";

        /// <summary>
        /// [static] Error type used when the service refused your request (usually when your account is not allowed to execute it)  
        /// </summary>
        public static readonly string INSUFFICIENT_PRIVILEGE_ERROR = "INSUFFICIENT_PRIVILEGE_ERROR";

        /// <summary>
        /// [static] Error type used when invoking a method with an invalid param/status. 
        /// </summary>
        public static readonly string INVALID_PARAM_ERROR = "INVALID_PARAM_ERROR";

        /// <summary>
        /// [static] Error type used when the service tried to redirect your request and it failed  
        /// </summary>
        public static readonly string REDIRECTION_FAILURE_ERROR = "REDIRECTION_FAILURE_ERROR";

        /// <summary>
        /// [static] Error type used when the request failed at sound generation step (usually voice error). 
        /// </summary>
        public static readonly string SOUND_GENERATION_ERROR = "SOUND_GENERATION_ERROR";

        /// <summary>
        /// [static] Error type used when the requested was interrupted because the time limit was hit. 
        /// </summary>
        public static readonly string TIMEOUT_ERROR = "TIMEOUT";

        /// <summary>
        /// [static] Error type used when an unpredictable error occured. 
        /// </summary>
        public static readonly string UNEXPECTED_ERROR = "UNEXPECTED_ERROR";

        /// <summary>
        /// Creates a new VaasError object instance.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="details"></param>
        public VaasError(string type, string details)
        {
            Type = type;
            Details = details;
        }
    }
}