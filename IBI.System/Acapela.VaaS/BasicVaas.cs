﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Acapela.Vaas
{
    /// <summary>
    /// It enables you to create a message using: 
    /// • the service identifier and password provided by acapela
    /// • the voice identifier (see Voice List on VaaS site)
    /// • the text you want to synthesize.
    /// 
    /// You will have access to: 
    /// • the Sound object corresponding to the message, once it is completly downloaded.
    /// • the identifier of the message if you want to access to the message later or share it.
    /// 
    /// Using this class is the simpliest way to use voice as a service. However, it does not provide access to advanced functions of the service. 
    /// 
    /// Anyway, if it is offering enough for you, you can forget about the rest of package.
    /// </summary>
    class BasicVaas
    {
        /// <summary>
        /// Type of the event dispatched when an error occured while calling generateMessage or retrieveMessage. 
        /// The target of this event is the BasicVaas instance that generated it. You can access to the error description using the lastError property of this instance.
        /// </summary>
        public static readonly string ERROR = "error";

        /// <summary>
        /// Type of the event dispatched as a result of a successfull call to generateMessage or retrieveMessage. It occurs when the ID of the requested message was received and the corresponding Sound objet was loaded. 
        /// The target of this event is the BasicVaas instance that generated it. You can access to the id and the Sound object associated to the message using the requestedId and requestedSound properties of this instance. 
        /// </summary>
        public static readonly string MESSAGE_AVAILABLE = "messageAvailable";

        private string lastError = "";
        private string requestId = "";
        private Connector connector;
        private Sound requestedSound = null;

        /// <summary>
        /// [read-only] The description of the error that just happened.
        /// You should access it only from the error event handler.
        /// </summary>
        public string LastError
        {
            get
            {
                return lastError;
            }
        }

        /// <summary>
        /// [read-only] The ID (identifier) of the message that was just generated.
        /// </summary>
        public string RequestedId
        {
            get
            {
                return requestId;
            }
        }

        /// <summary>
        /// [read-only] The Sound object corresponding to the message that was just generated.
        /// </summary>
        public Sound RequestedSound
        {
            get
            {
                return requestedSound;
            }
        }

        /// <summary>
        /// The password you received from Acapela.
        /// </summary>
        public string ServicePassword;
        //{
        //    get
        //    {
        //        return connector.
        //    }

        /// <summary>
        /// The service URL you received from Acapela.
        /// </summary>
        public string ServiceURL;

        public String DownloadPath
        {
            get
            {
                return this.connector.DownloadPath;
            }
            set
            {
                this.connector.DownloadPath = value;
            }
        }

        /// <summary>
        /// Use it to create a new BasicVaas object instance.
        /// </summary>
        public BasicVaas()
        {
            // TODO: remove?
        }

        public BasicVaas(string serviceUsername, string servicePassword, string serviceURL, string serviceAppId)
        {
            this.ServicePassword = servicePassword;
            this.ServiceURL = serviceURL;
            // TODO: save serviceUsername and serviceAppId???
            this.connector = new Connector(serviceURL, serviceAppId, serviceUsername, servicePassword);
            this.connector.Synthesizer.SoundDownloadEnded += new EventHandler<Synthesizer.SoundDownloadEndedEventArgs>(Synthesizer_SoundDownloadEnded);
        }

        private void Synthesizer_SoundDownloadEnded(object sender, Synthesizer.SoundDownloadEndedEventArgs e)
        {
            // TODO: use sender object to retrieve filename?
            requestedSound = new Sound(e.Filename);
        }

        /// <summary>
        /// This function will post a message generation request to the service.
        ///
        /// The result will be a dispatched event when the request will be treated.
        ///
        /// This event could be: 
        /// • messageAvailable event when message is generated and the corresponding sound downloaded,
        /// • an error event when the operation failed.
        /// </summary>
        /// <param name="voice">identifier of the voice to use for the sound generation. e.g. : "heather22k"</param>
        /// <param name="text">text to synthesize. e.g. : "hello, my name is Heather."</param>
        public void GenerateMessage(string voice, string text)
        {
            Message msg = new Message(voice, text);
            connector.Synthesizer.GenerateMessage(msg, true);
        }

        /// <summary>
        /// This function will post a request to the service in order to retrieve a previously generated message. 
        /// 
        /// The result will be a dispatched event when the request will be treated.
        /// This event could be: 
        /// • messageAvailable event when message is generated and the corresponding sound downloaded,
        /// • an error event when the operation failed.
        /// </summary>
        /// <param name="msgId">ID (identifier) of the message previously generated</param>
        public void RetrieveMessage(string msgId)
        {
            // TODO:
        }
    }
}
