﻿CREATE TABLE [dbo].[ActiveSchedules] (
    [sqlid]          INT          IDENTITY (1, 1) NOT NULL,
    [CustomerId]     INT          NOT NULL,
    [BusNumber]      VARCHAR (50) NOT NULL,
    [ScheduleNumber] INT          NOT NULL,
    CONSTRAINT [PK_Journey_ActiveSchedules] PRIMARY KEY CLUSTERED ([sqlid] ASC)
);

