﻿CREATE TABLE [dbo].[GroupBuses] (
    [GroupId]    INT          NOT NULL,
    [BusNumber]  VARCHAR (50) NOT NULL,
    [CustomerId] INT          NOT NULL,
    CONSTRAINT [PK_GroupBuses] PRIMARY KEY CLUSTERED ([GroupId] ASC, [BusNumber] ASC, [CustomerId] ASC),
    CONSTRAINT [FK_GroupBuses_Buses] FOREIGN KEY ([BusNumber], [CustomerId]) REFERENCES [dbo].[Buses] ([BusNumber], [CustomerId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_GroupBuses_Groups] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[Groups] ([GroupId]) ON DELETE CASCADE ON UPDATE CASCADE
);

