﻿CREATE TABLE [dbo].[Auths] (
    [Id]           BIGINT         IDENTITY (1, 1) NOT NULL,
    [AuthToken]    NVARCHAR (100) NOT NULL,
    [UserId]       INT            NOT NULL,
    [CreatedOn]    DATETIME       CONSTRAINT [DF_Auth_Timestamp] DEFAULT (getdate()) NOT NULL,
    [LastAccessed] DATETIME       CONSTRAINT [DF_Auth_LastAccessed] DEFAULT (getdate()) NOT NULL,
    [ApiId]        INT            NOT NULL,
    CONSTRAINT [PK_Auth] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Auths_Apps] FOREIGN KEY ([ApiId]) REFERENCES [dbo].[Apps] ([ApiId]),
    CONSTRAINT [FK_Auths_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserId]) ON DELETE CASCADE ON UPDATE CASCADE
);

