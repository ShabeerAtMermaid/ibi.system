﻿CREATE TABLE [dbo].[SignItems] (
    [SignId]       INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]   INT            NOT NULL,
    [GroupId]      INT            NULL,
    [ScheduleId]   INT            NULL,
    [Line]         VARCHAR (50)   NULL,
    [MainText]     VARCHAR (255)  NULL,
    [SubText]      VARCHAR (1000) NULL,
    [Name]         VARCHAR (255)  NULL,
    [SortValue]    INT            CONSTRAINT [DF_SignItems_SortOrder] DEFAULT ((0)) NOT NULL,
    [Excluded]     BIT            CONSTRAINT [DF_SignItems_Excluded] DEFAULT ((0)) NOT NULL,
    [DateAdded]    DATETIME       CONSTRAINT [DF_SignItems_DateAdded] DEFAULT (getdate()) NOT NULL,
    [DateModified] DATETIME       CONSTRAINT [DF_SignItems_DateModified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SignItems] PRIMARY KEY CLUSTERED ([SignId] ASC),
    CONSTRAINT [FK_SignItems_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]) ON DELETE CASCADE,
    CONSTRAINT [FK_SignItems_Schedules] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[Schedules] ([ScheduleId]),
    CONSTRAINT [FK_SignItems_SignGroups] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[SignGroups] ([GroupId]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'destination', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems', @level2type = N'COLUMN', @level2name = N'MainText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'via', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems', @level2type = N'COLUMN', @level2name = N'SubText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'destination', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignItems', @level2type = N'COLUMN', @level2name = N'Name';

