﻿CREATE TABLE [dbo].[CustomerAudioFiles] (
    [sqlid]      INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId] INT           NULL,
    [StopName]   VARCHAR (200) NULL,
    [Voice]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_CustomerAudioFiles] PRIMARY KEY CLUSTERED ([sqlid] ASC),
    CONSTRAINT [FK_CustomerAudioFiles_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId]) ON DELETE CASCADE ON UPDATE CASCADE
);

