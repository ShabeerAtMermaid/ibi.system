﻿CREATE FUNCTION [dbo].[Split]
(
   @text nvarchar(max),
   @delimiter nvarchar(100)
) RETURNS @t TABLE
(
   id int identity(1,1), 
   val nvarchar(max)
)
AS
BEGIN
	DECLARE @xml XML
	SET @xml = N'<root><r>' + replace(@text,@delimiter,'</r><r>') + '</r></root>'
 
	INSERT INTO @t(val)
		SELECT r.value('.','varchar(max)') AS item
		FROM @xml.nodes('//root/r') AS records(r) 
  RETURN
END
