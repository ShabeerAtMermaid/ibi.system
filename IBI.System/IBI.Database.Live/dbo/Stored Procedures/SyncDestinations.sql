﻿CREATE PROC [dbo].[SyncDestinations]

 @customerID int,
 @BusNumber varchar(100),
 @Line varchar(255),
 @FromName varchar(255),
 @Destination varchar(255),
 @ViaName varchar(255)

AS

	BEGIN
		--DECLARE  @customerID int

		--SELECT @customerID = [CustomerID] FROM [Clients] WHERE [BusNumber]=@BusNumber
		IF NOT EXISTS(SELECT * FROM [Destinations] 
		WHERE 
		[CustomerID]=@customerID AND [Line]=@Line AND [FromName]=@FromName AND [Destination]=@Destination AND [ViaName]=@ViaName)
		BEGIN
		  INSERT INTO [Destinations]([CustomerID], [Line], [FromName], [Destination], [ViaName], [MoviaEntry], [LastSeen], [Excluded]) VALUES(@customerID, @Line, @FromName, @Destination, @ViaName, 1, CONVERT(VARCHAR(20), GETDATE(), 20), 0)
		END
		ELSE
		BEGIN
		  UPDATE [Destinations] SET [MoviaEntry]=1, [LastSeen]=CONVERT(VARCHAR(20), GETDATE(), 20) WHERE [CustomerID]=@customerID AND [Line]=@Line AND [FromName]=@FromName AND [Destination]=@Destination AND [ViaName]=@ViaName
		END  

	END
