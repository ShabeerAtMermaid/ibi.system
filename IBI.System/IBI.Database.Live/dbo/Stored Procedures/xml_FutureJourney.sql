﻿CREATE PROC [dbo].[xml_FutureJourney]
@JourneyId int
AS
BEGIN
	SELECT
	 j.JourneyId  AS [JourneyId],
	 s.Line AS [Line],  
	 s.FromName AS [FromName],  
	 s.Destination AS [Destination], 
	 s.ViaName AS [ViaName],
	 (
		 select StopId as [@StopId],
		 --StopSequence as [StopSequence],
		 StopName as [StopName], 
		 CONVERT(varchar(100), CAST(StopGPS.STPointN(1).Lat AS decimal(38,13)))   AS [GPSCoordinateNS],
		 CONVERT(varchar(100), CAST(StopGPS.STPointN(1).Long AS decimal(38,13)))   AS [GPSCoordinateEW],
		 Zone as [Zone]
	  	 From JourneyStops
			WHERE JourneyId=@JourneyId
			for xml path('StopInfo'), TYPE, ROOT('JourneyStops')
	  ) as [*]
	  from Journeys j INNER JOIN Schedules s On j.ScheduleId = s.ScheduleId 
	  Where JourneyId = @JourneyId AND BusNumber = '0' AND Active=0
	  for xml path ('Journey')
END
