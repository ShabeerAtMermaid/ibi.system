﻿-- =============================================
-- Author:		NAK
-- =============================================
CREATE PROCEDURE [dbo].[A_CommonMissingStops] 
	@ScheduleNumber int,
	@BusNumber int = 0,
	@J varchar(MAX) = '',
	@StartTime datetime = NULL,
	@EndTime datetime = NULL
AS
BEGIN
	DECLARE @scheduleData xml
	DECLARE @myStopName varchar(100)
	DECLARE @missing bit
	DECLARE @JS varchar(MAX)
	SET @missing = 0
	
	DECLARE @CompareTable table
	(
		journeys varchar(max),
		stopname varchar(100)
	) 
	if ( @J = '' )
	BEGIN
		select top 100 @JS = 
		coalesce (case when @JS = ''
					   then CAST(JourneyNumber as varchar(10))
					   else @JS + ',' + CAST(JourneyNumber as varchar(10))
				   end
				  ,'')
		from Journeys_History WHERE IsLastStop=1 
		AND (@BusNumber=0 OR @BusNumber=BusNumber)
		AND ScheduleNumber=@ScheduleNumber
		AND (@EndTime IS NULL OR (@EndTime IS NOT NULL AND JourneyDate <= @EndTime)) 
		AND  (@StartTime IS NULL OR (@StartTime IS NOT NULL AND JourneyDate >= @StartTime)) 
		ORDER BY JourneyDate DESC
	END
	ELSE
		SET @JS = @J

	IF @JS = ''
		SELECT 'No Journey'
	SET @scheduleData = (SELECT ScheduleXML From Schedules Where ScheduleID=@ScheduleNumber)
    	DECLARE cur CURSOR FOR
			SELECT JData.Stops.value('(StopName/text())[1]', 'varchar(100)') StopName
			FROM 
				@scheduleData.nodes('/Schedule/JourneyStops/StopInfo') AS JData(Stops)

				OPEN cur
				FETCH NEXT FROM cur INTO @myStopName
					WHILE @@FETCH_STATUS = 0   
					BEGIN
						SET @missing = (SELECT Top 1 JourneyNumber FROM JourneyStops_History WHERE StopName=@myStopName AND JourneyNumber IN (SELECT Number FROM splitnumber(@JS,',')))
						IF @missing IS NULL OR @missing = ''
						BEGIN
							INSERT INTO @CompareTable
							SELECT @JS,@myStopName	
						END

						FETCH NEXT FROM cur INTO @myStopName		
					END
			CLOSE cur   
			DEALLOCATE cur
	IF @J = ''
		SELECT * from @CompareTable
	ELSE
		SELECT @myStopName from @CompareTable
END
