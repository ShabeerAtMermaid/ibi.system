﻿CREATE PROC [dbo].[xml_UpdateStop]
	@JourneyId int,
	@JourneyPlannedStartTime datetime,
	@JourneyPlannedEndTime datetime,
	@StopId decimal(19,0),
	@ActualArrival datetime,
	@ActualDeparture datetime,
	@IsLastStop bit,
	@ExternalReference varchar(100),
	@JourneyStops text,
	@StopData text
AS

BEGIN

	--IF EXISTS(Select * from Journeys WHERE JourneyId = @JourneyId)
	/*
	 ///rewrite complete logic of SaveActiveJourney
            /// Update JourneyStops with ActualArrival and ActualDeparture timestamps for this stop, based on StopId(StopNumber)
            /// if this is first update call then update StartTime for Journey table as well
            /// if this is last stop then update Endtime, Active=0 in Journey Table
            /// Update ahead and behind for this stop - Query from DB script(2264)
            /// Check if valid cache file
            ///     if yes
            ///         then update planned times for JourneyStops if they are not popupalted already. 
            ///         Make entries for Estimated times for remaining stops in prediction table
            ///     if no
            ///         do nothing
	*/


/*
'<Stop>
  <PlanDifference>0</PlanDifference>
  <Status />
  <JourneyNumber>314155</JourneyNumber>
  <StopNumber>9025200000006918</StopNumber>
  <BusNumber>1152</BusNumber>
  <StopName>Naverporten</StopName>
  <ActualArrival>2014-03-17T13:56:28</ActualArrival>
  <ActualDeparture>2014-03-17T13:56:55</ActualDeparture>
  <ScheduleNumber>134</ScheduleNumber>
  <PlannedArrival>2014-03-17T13:56:00</PlannedArrival>
  <PlannedDeparture>2014-03-17T13:56:00</PlannedDeparture>
  <FromName>Avedøre st.</FromName>
  <ViaName />
  <IsLastStop>false</IsLastStop>
  <ExternalReference>214</ExternalReference>
  <StopSequence>8</StopSequence>
</Stop>'
*/

DECLARE @StopDataXml xml
SET @StopDataXml = @StopData	


DECLARE @RouteDirectionId int
DECLARE @ScheduleId int 
DECLARE @StopSequence int

DECLARE @JourneyAhead int
DECLARE @JourneyBehind int

SET @RouteDirectionId = (Select TOP 1 s.RouteDirectionId From Schedules s Inner join Journeys j ON j.ScheduleId=s.ScheduleId) 

SET @StopSequence =(
 SELECT StopInfo.data.value('(StopSequence/text())[1]', 'int') AS StopSequence		  
		 FROM @StopDataXml.nodes('/Stop') AS StopInfo(data)  		 
 )
	 
	
	DECLARE @Active bit
	
	-- check if actualdeparture time passed, set the JourneyStops table and return
	Update JourneyStops
		SET ActualDepartureTime = @ActualDeparture
		WHERE JourneyId = @JourneyId AND StopId = @StopId AND ActualArrivalTime IS NOT NULL
	
	IF @@ROWCOUNT>0
		return; 
	
	IF(@IsLastStop = 1)
		SET @Active = 0
	ELSE
		SET @Active = 1
	 
	--UPDATE Journeys
	UPDATE Journeys
		SET PlannedStartTime = @JourneyPlannedStartTime,
			PlannedEndTime = @JourneyPlannedEndTime,
			Active = @Active,
			LastUpdated = GETDATE()
		WHERE PlannedStartTime is null AND JourneyId = @JourneyId
	
	
	
	
	
	--Get Ahead Journey	 

SET @JourneyAhead =(	
SELECT TOP 1
	  j.JourneyId	
FROM
	[JourneyStops] js
	LEFT JOIN	
	[Journeys] j
	ON j.JourneyId=js.JourneyId
	INNER JOIN
	 [Schedules] s
	ON j.ScheduleId=s.ScheduleId 
WHERE s.RouteDirectionId=@RouteDirectionId
	AND js.StopId = @StopId
	AND js.StopSequence = @StopSequence -- we need to get rid of this ... bcoz same route direction may have different schedules.
	AND j.Active=1  -- it may raise some problems if ahead bus has reached last stop ... (we won't get ahead journey for last 2,3 stops)
	AND j.JourneyId != @JourneyId
	AND js.ActualArrivalTime IS NOT NULL	
ORDER BY js.ActualArrivalTime DESC
	)
	

--Update JourneyStops
	Update JourneyStops 
		Set ActualArrivalTime = @ActualArrival,
			ActualDepartureTime = @ActualDeparture,
		    JourneyAhead = @JourneyAhead			
		WHERE JourneyId = @JourneyId AND StopId = @StopId AND StopSequence = @StopSequence
			
--Update Ahead Journey
	UPDATE JourneyStops 
		SET JourneyBehind = @JourneyId
		WHERE JourneyId = @JourneyAhead AND StopId = @StopId AND StopSequence = @StopSequence

-- Make entry in predictionstable for estimated timestamps from XML in parameter 
END