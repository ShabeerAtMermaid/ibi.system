﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace IBI.MoviaRealtimeJourneys
{
    public class MoviaRTFileDownloader
    {
        Log log { get; set; }
        private string FileLastName { get; set; }
        private DateTime FileLastUpdate { get; set; }

        public MoviaRTFileDownloader(Log logger)
        {
            this.FileLastName = "";
            this.FileLastUpdate = new DateTime();
            log = logger;
        }

        #region Events
        public void Dispose()
        {

        }

        #endregion


        #region "Functions"

        public bool DownloadSourceFile(string sourcePath, string user, string pwd, ref string localFilePath)
        {
            try
            {
                // The serverUri should start with the ftp:// scheme.
                if (new Uri(sourcePath).Scheme != Uri.UriSchemeFtp)
                {
                    return false;
                }

                bool bDownloadNew = false;

                // Get the object used to communicate with the server.
                FtpWebRequest request = CreateFtpWebRequest(sourcePath, user, pwd, true);
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                // Get the ServicePoint object used for this request, and limit it to one connection.
                // In a real-world application you might use the default number of connections (2),
                // or select a value that works best for your application.

                ServicePoint sp = request.ServicePoint;
                sp.ConnectionLimit = 1;

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                // The following streams are used to read the data returned from the server.
                Stream responseStream = null;
                StreamReader readStream = null;
                try
                {
                    responseStream = response.GetResponseStream();
                    readStream = new StreamReader(responseStream, System.Text.Encoding.UTF8);

                    if (readStream != null)
                    {
                        // Display the data file name received from the server.
                        string FileToDownload = readStream.ReadToEnd();
                        FileToDownload = FileToDownload.Replace("\r\n", "");
                        DateTime LastModified = ftpFileLastUpdate(sourcePath, user, pwd, FileToDownload);
                        //Console.WriteLine("FileName {0}, LastFileName {1} and FileUpdate {2}, LastFileUpdate {3}", FileToDownload, FileLastName, LastModified, FileLastUpdate);
                        if ((string.Compare(FileToDownload, FileLastName) != 0) || (DateTime.Compare(LastModified, FileLastUpdate) != 0))
                        {
                            //log.WriteLog(string.Format("Downloading latest file {0}", FileToDownload));
                            //DownloadFileAsGZip(sourcePath, user, pwd, FileToDownload, Path.Combine(localFilePath, FileToDownload));
                            localFilePath = Path.Combine(localFilePath, FileToDownload + ".csv");
                            bDownloadNew = DownloadFileAsCSV(sourcePath, user, pwd, FileToDownload, localFilePath);

                            if (bDownloadNew)
                            {
                                log.WriteLog(string.Format("Downloaded latest file {0}", FileToDownload));
                                FileLastName = FileToDownload;
                                FileLastUpdate = LastModified;
                            }
                            else log.WriteLog(string.Format("Failed to downloaded latest file {0}", FileToDownload));
                        }
                       // else Console.WriteLine("Already downloaded latest one.");
                    } //readStream
                    //Console.WriteLine("Downloaded status: {0}", response.StatusDescription);
                }
                finally
                {
                    if (readStream != null)
                    {
                        readStream.Close();
                    }
                    if (response != null)
                    {
                        response.Close();
                    }
                }


                //Console.WriteLine("Banner message: {0}",
                //    response.BannerMessage);

                //Console.WriteLine("Welcome message: {0}",
                //    response.WelcomeMessage);

                //Console.WriteLine("Exit message: {0}",
                //    response.ExitMessage);
                return bDownloadNew;
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            return false;
        }


        #endregion

        #region Private Helper
        private FtpWebRequest CreateFtpWebRequest(string ftpDirectoryPath, string userName, string password, bool keepAlive = false)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ftpDirectoryPath));

            //Set proxy to null. Under current configuration if this option is not set then the proxy that is used will get an html response from the web content gateway (firewall monitoring system)
            request.Proxy = null;

            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = keepAlive;
            request.EnableSsl = true;
            request.Credentials = new NetworkCredential(userName, password);

            return request;
        }

        //private void DownloadFileAsGZip(string sourcePath, string user, string pwd, string sourceFile, string localFilePath)
        //{
        //    int bytesRead = 0;
        //    byte[] buffer = new byte[2048];
        //    FtpWebRequest request = CreateFtpWebRequest(string.Concat(sourcePath, sourceFile), user, pwd, true);
        //    request.Method = WebRequestMethods.Ftp.DownloadFile;
        //    Stream reader = request.GetResponse().GetResponseStream();
        //    FileStream fileStream = new FileStream(localFilePath, FileMode.Create);
        //    while (true)
        //    {
        //        bytesRead = reader.Read(buffer, 0, buffer.Length);
        //        if (bytesRead == 0)
        //            break;
        //        fileStream.Write(buffer, 0, bytesRead);
        //    }
        //    fileStream.Close();       
        //} //DownloadFileAsGZip

        private bool DownloadFileAsCSV(string sourcePath, string user, string pwd, string sourceFile, string localFilePath)
        {
            try
            {
                if (File.Exists(localFilePath))
                    File.Delete(localFilePath);

                FtpWebRequest request = CreateFtpWebRequest(string.Concat(sourcePath, sourceFile), user, pwd, true);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                Stream reader = request.GetResponse().GetResponseStream();

                MemoryStream memoryStream = new MemoryStream();
                reader.CopyTo(memoryStream);
                memoryStream.Position = 0;

                using (FileStream outFile = File.Create(localFilePath))
                {
                    using (GZipStream Decompress = new GZipStream(memoryStream, CompressionMode.Decompress))
                    {
                        // Copy the decompression stream into the output file.
                        Decompress.CopyTo(outFile);
                    }
                }

                return File.Exists(localFilePath);
            }
            catch (Exception ex)
            {
                log.WriteLog(ex);
            }
            return false;

        } //DownloadFileAsCSV

        // Get File Update Time
        private DateTime ftpFileLastUpdate(string sourcePath, string user, string pwd, string FileToDownload)
        {
            FtpWebRequest request_lastupdate = CreateFtpWebRequest(string.Concat(sourcePath, FileToDownload), user, pwd, true);
            request_lastupdate.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            FtpWebResponse respTime = (FtpWebResponse)request_lastupdate.GetResponse();
            DateTime LastModified = respTime.LastModified;
            respTime.Close();
            return LastModified;

            // File size
            //FtpWebRequest request = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://servername/filepath"));
            //request.Proxy = null;
            //request.Credentials = new NetworkCredential("user", "password");
            //request.Method = WebRequestMethods.Ftp.GetFileSize;

            //FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            //long size = response.ContentLength;
            //response.Close();

        } //ftpFileLastUpdate
        

        
        #endregion

    }
} //namespace
