﻿using System.ServiceProcess;

namespace IBI.Server
{
    public partial class IBIService : ServiceBase
    {
        private IBIServer serverInstance;

        public IBIService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.serverInstance = new IBIServer();
        }

        protected override void OnStop()
        {
            this.serverInstance.Dispose();
        }
    }
}
