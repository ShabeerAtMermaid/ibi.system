﻿using System;
using System.Diagnostics;
using System.Timers;
using IBI.Service.Implementation;
using System.Collections;
using IBI.Shared.ConfigSections;

namespace IBI.Server
{
    public class IBIServer : IDisposable
    {
        private Timer ScheduleSyncTimer;
        private Timer DumbStatusTimer;
        private Timer SupportSyncTimer;
        private Timer BusesSyncTimer;
        private Timer AudioSyncTimer;
        private Timer screenshotCleanupTimer;  
        private Timer NavisionSupportSyncTimer;

        private Synchronizer synchronizer = new Synchronizer();

        #region Properties
         

        #endregion


        public IBIServer()
        {
            this.ScheduleSyncTimer = new Timer(TimeSpan.FromSeconds(5).TotalMilliseconds);
            this.ScheduleSyncTimer.Elapsed += new ElapsedEventHandler(ScheduleSyncTimer_Elapsed);
            this.ScheduleSyncTimer.Enabled = true;

            this.DumbStatusTimer = new Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
            this.DumbStatusTimer.Elapsed += new ElapsedEventHandler(DumbStatusTimer_Elapsed);
            this.DumbStatusTimer.Enabled = true;

            this.SupportSyncTimer = new Timer(TimeSpan.FromSeconds(AppSettings.NovoSupportSyncTimer()).TotalMilliseconds);
            this.SupportSyncTimer.Elapsed += new ElapsedEventHandler(NovoSupportSyncTimer_Elapsed);
            this.SupportSyncTimer.Enabled = true;


            this.NavisionSupportSyncTimer = new Timer(TimeSpan.FromSeconds(AppSettings.NavisionSupportSyncTimer()).TotalMilliseconds);
            this.NavisionSupportSyncTimer.Elapsed += new ElapsedEventHandler(NavisionSupportSyncTimer_Elapsed);
            this.NavisionSupportSyncTimer.Enabled = true;



            this.BusesSyncTimer = new Timer(TimeSpan.FromSeconds(2).TotalMilliseconds);
            this.BusesSyncTimer.Elapsed += new ElapsedEventHandler(BusesSyncTimer_Elapsed);
            this.BusesSyncTimer.Enabled = true;

            this.AudioSyncTimer = new Timer(TimeSpan.FromMinutes(30).TotalMilliseconds);
            this.AudioSyncTimer.Elapsed += new ElapsedEventHandler(AudioSyncTimer_Elapsed);
            this.AudioSyncTimer.Enabled = true;

            this.screenshotCleanupTimer = new Timer(TimeSpan.FromSeconds(10).TotalMilliseconds);
            this.screenshotCleanupTimer.Elapsed += screenshotCleanupTimer_Elapsed;
            this.screenshotCleanupTimer.Enabled = true;
             
        }
         
        void screenshotCleanupTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                this.screenshotCleanupTimer.Enabled = false;
                if (AppSettings.ScreenShotCleanupEnabled())
                {
                    synchronizer.CleanScreenshots(AppSettings.ScreenshotCleanUpInterval());
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                this.screenshotCleanupTimer.Interval = TimeSpan.FromHours(1).TotalMilliseconds;
                this.screenshotCleanupTimer.Enabled = true;
            }
        }



        void AudioSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.AudioSyncTimer.Enabled = false;

            //only for testing purpose
            EventLog AudioSynlog = new EventLog();  //only for testing purpose
            AudioSynlog.Log = "Application";
            AudioSynlog.Source = "IBI Server";
            //End only for testing purpose

            try
            {
                //only for testing purpose
                AudioSynlog.WriteEntry("AudioSyncTimer Timer Stared at " + DateTime.Now.ToString(), EventLogEntryType.Information);
                //End only for testing purpose

                if (AppSettings.AudioSyncEnabled())
                {
                    TTSService.TTSServiceClient service = new TTSService.TTSServiceClient();
                    service.SyncAudioFiles();
                    service.SyncAudioFilesCache();
                    service.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                //only for testing purpose
                AudioSynlog.WriteEntry("AudioSyncTimer Timer End at " + DateTime.Now.ToString(), EventLogEntryType.Information);
                //End only for testing purpose

                this.AudioSyncTimer.Interval = AppUtility.DifferenceToMidnight().TotalMilliseconds;
                this.AudioSyncTimer.Enabled = true;
            }
        }

        void BusesSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.BusesSyncTimer.Enabled = false;
            try
            {
                if (AppSettings.BusesSyncEnabled())
                {
                    synchronizer.SyncAllBusesData();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            finally
            {
                this.BusesSyncTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
                this.BusesSyncTimer.Enabled = true;
            }
        }

        private void ScheduleSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.ScheduleSyncTimer.Enabled = false;
            try
            {
                if (AppSettings.ScheduleSyncEnabled())
                {
                    synchronizer.SyncAllSchedules();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);

            }
            finally
            {
                this.ScheduleSyncTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
                this.ScheduleSyncTimer.Enabled = true;
            }
        }

        private void DumbStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.DumbStatusTimer.Enabled = false;

            try
            {
                if (AppSettings.DumpStatusEnabled())
                {
                    SystemService.SystemServiceClient service = new SystemService.SystemServiceClient();
                    service.DumpCurrentStatus();
                    service.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                this.DumbStatusTimer.Interval = TimeSpan.FromMinutes(10).TotalMilliseconds;
                this.DumbStatusTimer.Enabled = true;
            }
        }

        private void NovoSupportSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.SupportSyncTimer.Enabled = false;
            try
            {
                if (AppSettings.NovoTicketSynEnabled())
                {
                    synchronizer.SyncAllSupportData();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                this.SupportSyncTimer.Interval = TimeSpan.FromMinutes(AppSettings.NovoSupportSyncTimer()).TotalMilliseconds;
                this.SupportSyncTimer.Enabled = true;
                Console.WriteLine(DateTime.Now.ToString() + "-Timer enabled");
            }
        }

        private void NavisionSupportSyncTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.NavisionSupportSyncTimer.Enabled = false;
            try
            {
                if (AppSettings.NavisionTicketSynEnabled())
                {
                    synchronizer.SyncAllSupportDataFromNavision();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            finally
            {
                this.NavisionSupportSyncTimer.Interval = TimeSpan.FromMinutes(AppSettings.NavisionSupportSyncTimer()).TotalMilliseconds;
                this.NavisionSupportSyncTimer.Enabled = true;
                Console.WriteLine(DateTime.Now.ToString() + "-Timer enabled");
            }
        }

        private void LogError(Exception ex)
        {
            try
            {
                String message = "";
                Exception exception = ex;

                while (exception != null)
                {
                    message += exception.Message + Environment.NewLine;
                    message += exception.StackTrace + Environment.NewLine;
                    message += "--" + Environment.NewLine;

                    exception = exception.InnerException;
                }

                EventLog log = new EventLog();
                log.Log = "Application";
                log.Source = "IBI Server";
                log.WriteEntry(message, EventLogEntryType.Error);
            }
            catch (Exception)
            {
                // SILENT
            }
        }

        public void Dispose()
        {
            this.ScheduleSyncTimer.Enabled = false;
            this.ScheduleSyncTimer.Dispose();

            this.DumbStatusTimer.Enabled = false;
            this.DumbStatusTimer.Dispose();

            this.SupportSyncTimer.Enabled = false;
            this.SupportSyncTimer.Dispose();

            this.BusesSyncTimer.Enabled = false;
            this.BusesSyncTimer.Dispose();

            this.AudioSyncTimer.Enabled = false;
            this.AudioSyncTimer.Dispose();

            this.screenshotCleanupTimer.Enabled = false;
            this.screenshotCleanupTimer.Dispose();
        }
    }
}
