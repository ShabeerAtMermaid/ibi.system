﻿using IBI.Shared;
using IBI.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Implementation.Schedular
{
    public class MoviaImplementation
    {
        public static string GetBusSchedule(int busNumber, ref string logs)
        {
            string schedule = string.Empty;
            //string logs = string.Empty;
            IBI.Shared.Models.Schedule tmpSchedule = new IBI.Shared.Models.Schedule();
            List<StopInfo> currentSchedule = new List<StopInfo>();
            try
            {
                using (Movia.Services.JourneyProgress.ServiceClient serviceClient = new Movia.Services.JourneyProgress.ServiceClient())
                {
                    Movia.Services.JourneyProgress.JourneyRow[] serviceResult = null;

                    try
                    {
                        serviceClient.ClientCredentials.UserName.UserName = "mermaid";
                        serviceClient.ClientCredentials.UserName.Password = "220711Mer";
                        logs += string.Format("GetJourneyProgress started {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                        using (new IBI.Shared.CallCounter("MoviaImplementation.GetBusSchedule.JourneyProgress"))
                        {
                            serviceResult = serviceClient.GetJourneyProgress(busNumber);
                        }
                        
                        logs += string.Format("GetJourneyProgress End {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        logs += ex.Message;
                    }

                    logs += "serviceResult.Length: " + serviceResult.Length;

                    logs += string.Format("GetJourneyProgress started {0} ---> BusNumber {1} {2}", DateTime.Now, busNumber, Environment.NewLine);
                        
                    for (int i = 0; i < serviceResult.Length; i++)
                    {
                        Movia.Services.JourneyProgress.JourneyRow currentRow = serviceResult[i];

                        if (i == 0)
                        {
                            tmpSchedule.BusNumber = busNumber.ToString();
                            tmpSchedule.Line = currentRow.LineName;
                            tmpSchedule.JourneyNumber = currentRow.JourneyNr.ToString();
                            tmpSchedule.FromName = currentRow.From;
                            tmpSchedule.DestinationName = currentRow.To;
                            tmpSchedule.ViaName = currentRow.Via;

                            string tmpBusNumber = busNumber.ToString();
                            logs += "currentRow.JourneyNr: " + currentRow.JourneyNr;

                        }

                        String[] gpsCoordinates = GPSConverter.UTMToDec(currentRow.EastWest, currentRow.NorthSouth, 32);


                        StopInfo stopData = new StopInfo();
                        stopData.StopNumber = currentRow.StopNr;
                        stopData.StopName = currentRow.StopName;
                        if (!String.IsNullOrEmpty(currentRow.TADT))
                            stopData.ArrivalTime = DateTime.Parse(currentRow.TADT);
                        if (!String.IsNullOrEmpty(currentRow.TDDT))
                            stopData.DepartureTime = DateTime.Parse(currentRow.TDDT);

                        if (!String.IsNullOrEmpty(currentRow.EDDT)) //planned departure time
                            stopData.PlannedDepartureTime = DateTime.Parse(currentRow.EDDT);

                        if (!String.IsNullOrEmpty(currentRow.AADT))
                            stopData.AADT_ArrivalTime = DateTime.Parse(currentRow.AADT);
                        if (!String.IsNullOrEmpty(currentRow.ADDT))
                            stopData.ADDT_DepartureTime = DateTime.Parse(currentRow.ADDT);

                        if (!String.IsNullOrEmpty(currentRow.LMDT))
                            stopData.LMDT_Time = DateTime.Parse(currentRow.LMDT);


                        stopData.GPSCoordinateNS = gpsCoordinates[0];
                        stopData.GPSCoordinateEW = gpsCoordinates[1];
                        stopData.StopSequence = (i + 1).ToString();
                        stopData.Zone = currentRow.Zone.ToString();
                        stopData.IsCheckpoint = (currentRow.Checkpoint == 1);

                        currentSchedule.Add(stopData);
                        System.Threading.Thread.Sleep(AppSettings.SyncScheduleStopInterval());
                    }
                    tmpSchedule.JourneyStops = currentSchedule.ToArray();

                }
                schedule = AppUtility.SafeSqlLiteral(XmlSerializer.Serialize(tmpSchedule));
            }
            catch(Exception ex)
            {
                logs += ex.Message;
            }
            
            return schedule;
        }
    }
}
