﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IBI.DataAccess;


namespace IBI.Implementation.Schedular
{
    public class SignController
    {

        public static bool CheckSignForNewSchedule(IBI.DataAccess.DBModels.IBIDataModel dbContext, IBI.DataAccess.DBModels.Schedule sch, string appSource)
        {
            //Sign Item / Sign group logic 
            IBI.DataAccess.DBModels.SignItem signItem = dbContext.SignItems.Where(i => i.CustomerId == sch.CustomerId && i.MainText == sch.Destination && i.Line == sch.Line && ((sch.ViaName == null && (i.SubText == null || i.SubText == "")) || i.SubText == sch.ViaName)).FirstOrDefault();
            if (signItem != null)
            {
                IBI.DataAccess.DBModels.Schedule existingsch = signItem.Schedule; // dbContext.Schedules.Where(s => s.ScheduleID == signItem.ScheduleId).FirstOrDefault();
                                                                
                if (existingsch != null && sch.ScheduleStops.Count > existingsch.ScheduleStops.Count)
                {
                    signItem.ScheduleId = sch.ScheduleId;
                }
            }
            else
            {
                try
                {
                    //creation of new signItem
                    string groupName = string.Concat("#", sch.Line);
                    IBI.DataAccess.DBModels.SignGroup signGroup = dbContext.SignGroups.Where(g => g.GroupName == groupName && g.CustomerId == sch.CustomerId).FirstOrDefault();


                    if (signGroup == null)
                    {
                        signGroup = new IBI.DataAccess.DBModels.SignGroup();
                        signGroup.CustomerId = sch.CustomerId;
                        signGroup.DateAdded = DateTime.Now;
                        signGroup.DateModified = DateTime.Now;
                        signGroup.Excluded = false;
                        signGroup.GroupName = groupName;
                        signGroup.SortValue = 0;
                        dbContext.SignGroups.Add(signGroup);

                        dbContext.SaveChanges();
                    }


                    IBI.DataAccess.DBModels.SignItem newSignItem = new IBI.DataAccess.DBModels.SignItem();
                    newSignItem.CustomerId = sch.CustomerId;
                    newSignItem.DateAdded = DateTime.Now;
                    newSignItem.DateModified = DateTime.Now;
                    newSignItem.Excluded = false;
                    newSignItem.GroupId = signGroup.GroupId;
                    newSignItem.Line = sch.Line;
                    newSignItem.MainText = sch.Destination;
                    newSignItem.Name = sch.Destination + (String.IsNullOrEmpty(sch.ViaName) ? "" : ", " + sch.ViaName);
                    newSignItem.ScheduleId = sch.ScheduleId;
                    newSignItem.SortValue = 0;
                    newSignItem.SubText = sch.ViaName;
                    dbContext.SignItems.Add(newSignItem);

                    dbContext.SaveChanges();

                    SyncDestinationContentImages(appSource, newSignItem.SignId, signGroup.GroupId);
                    return true;
                }
                catch (Exception ex) {
                    IBI.Shared.Diagnostics.Logger.AppendToSystemLog(Shared.Diagnostics.Logger.EntryTypes.Error, Shared.Diagnostics.Logger.EntryCategories.SIGNMANAGEMENT, ex);
                    return false;
                }
            }

            return false;
        }

        public static bool CheckSignForNewSchedule(int customerId, string line, string destination, string viaName, string appSource)
        {

            using (IBI.DataAccess.DBModels.IBIDataModel dbContext = new DataAccess.DBModels.IBIDataModel())
            {

                try
                {
                    //Sign Item / Sign group logic 
                    IBI.DataAccess.DBModels.SignItem signItem = dbContext.SignItems.Where(i => i.CustomerId == customerId && i.MainText == destination && i.Line == line && ((string.IsNullOrEmpty(viaName) && (i.SubText == null || i.SubText == "")) || i.SubText == viaName)).FirstOrDefault();
                    if (signItem != null)
                    {
                        //IBI.DataAccess.DBModels.Schedule existingsch = signItem.Schedule; // dbContext.Schedules.Where(s => s.ScheduleID == signItem.ScheduleId).FirstOrDefault();

                        //if (existingsch != null && sch.ScheduleStops.Count > existingsch.ScheduleStops.Count)
                        //{
                        //    signItem.ScheduleId = sch.ScheduleId;
                        //}
                    }
                    else
                    {
                        //creation of new signItem
                        string groupName = string.Concat("#", line);
                        IBI.DataAccess.DBModels.SignGroup signGroup = dbContext.SignGroups.Where(g => g.GroupName == groupName && g.CustomerId == customerId).FirstOrDefault();

                        if (signGroup == null)
                        {
                            signGroup = new IBI.DataAccess.DBModels.SignGroup();
                            signGroup.CustomerId = customerId;
                            signGroup.DateAdded = DateTime.Now;
                            signGroup.DateModified = DateTime.Now;
                            signGroup.Excluded = false;
                            signGroup.GroupName = groupName;
                            signGroup.SortValue = 0;
                            dbContext.SignGroups.Add(signGroup);

                            dbContext.SaveChanges();
                        }


                        IBI.DataAccess.DBModels.SignItem newSignItem = new IBI.DataAccess.DBModels.SignItem();
                        newSignItem.CustomerId = customerId;
                        newSignItem.DateAdded = DateTime.Now;
                        newSignItem.DateModified = DateTime.Now;
                        newSignItem.Excluded = false;
                        newSignItem.GroupId = signGroup.GroupId;
                        newSignItem.Line = line;
                        newSignItem.MainText = destination;
                        newSignItem.Name = destination + (String.IsNullOrEmpty(viaName) ? "" : ", " + viaName);
                        //newSignItem.ScheduleId = sch.ScheduleId;
                        newSignItem.SortValue = 0;
                        newSignItem.SubText = viaName;
                        dbContext.SignItems.Add(newSignItem);

                        dbContext.SaveChanges();

                        SyncDestinationContentImages(appSource, newSignItem.SignId, signGroup.GroupId);
                        return true;
                    }
                }
                 
                catch (Exception ex) 
                {
                    IBI.Shared.Diagnostics.Logger.AppendToSystemLog(Shared.Diagnostics.Logger.EntryTypes.Error, Shared.Diagnostics.Logger.EntryCategories.SignQueueProcessor, ex);
                    return false;
                }

            }

            return false;
        }

        private static void SyncDestinationContentImages(string appSource, int signItemId, int? signGroupId = null)
        {

            if (!String.IsNullOrEmpty(signItemId.ToString()))
            {
                //REST call to Sync SignItem Images ------------------------------------------------------------
                string REST_PATH = System.Configuration.ConfigurationSettings.AppSettings["REST_PATH"].ToString();

                Uri baseAddress = new Uri(REST_PATH + "/Schedule/Xml/ScheduleService.svc/");

                string uriTemplate = String.Format("SyncDestinationContentImages?signItems={0}&signGroups={1}&userName={2}", signItemId.ToString(), signGroupId == null ? string.Empty : signGroupId.ToString(), appSource);

                System.Net.WebClient webClient = new System.Net.WebClient();

                string url = baseAddress.AbsoluteUri + uriTemplate;

                string result = webClient.DownloadString(url);

                //string retObj = Newtonsoft.Json.JsonConvert.DeserializeObject<string>(result);
                //------------------------------------------------------------------------------------------------
            }

        }
    }
}
