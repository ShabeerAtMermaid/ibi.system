﻿using IBI.DataAccess.DBModels;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.Journey;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IBI.Implementation.Journey
{
    class JourneyPredictionHandler_Midttrafik : Shared.Interfaces.IJourneyPredictionHandler
    {
        #region Properties
        private const string REST_PATH = "http://realtime-vdv454.visualtrack.dk/{0}/sis/polldata.xml.aspx";

        private int _customerId;
        public int CustomerId
        {
            get
            {
                return _customerId;
            }
            set
            {
                _customerId = value;
            }
        }

        #endregion


        #region Implementation

        public bool FetchJourneyPredictions(int journeyId, string clientRef)
        {

            string log = "No Error";

            try
            {
                string predictionData = ThirdPartyRestCall(clientRef);

                if (bool.Parse(ConfigurationSettings.AppSettings["LogPredictionData"])) { 
                    IBI.Shared.AppUtility.Log2File("PredictionQueueManager", string.Format("Prediction Data --> {0}", predictionData), false);
                }

                if (!string.IsNullOrEmpty(predictionData))
                {
                    ImportPredictions(journeyId, predictionData);

                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION_QUEUE, string.Format("Error fetching prediction for journeyId: {0}, ClientRef [{1}]", journeyId, clientRef, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)));
                IBI.Shared.AppUtility.Log2File("PredictionQueueManager", string.Format("Error fetching prediction for journeyId: {0}, ClientRef [{1}]", journeyId, clientRef, IBI.Shared.Diagnostics.Logger.GetDetailedError(ex)), true);
            }

            return false;
        }


        #endregion


        #region Prediction Importer

        public void ImportPredictions(int journeyId, string predictionData)
        {
            XDocument xDoc = XDocument.Load(GenerateStreamFromString(predictionData));

            if (journeyId <= 0)
                return;

            List<JourneyPrediction> predictions = ParseJourneyEstimate(journeyId, xDoc);


            try
            {

                using (IBIDataModel dbContext = new IBIDataModel())
                {

                    //enabling this block keeps only updated stop estimation in JourneyStopEstimations
                    //********************************************************************************* 
                    var jEstimations = dbContext.JourneyStopEstimations.Where(j => j.JourneyId == journeyId);

                    var looper = from e in jEstimations
                                 group e by e.EstimatedStopSequence into g
                                 select g.OrderByDescending(e => e.EstimationTime).FirstOrDefault() into r
                                 select r;

                    int counter = 0;
                    foreach (var l in looper.OrderBy(j => j.EstimatedStopSequence))
                    {
                        var p = predictions.Where(pr => pr.EstimatedStopSequence == l.EstimatedStopSequence).FirstOrDefault();

                        if (p != null && (p.EstimatedDepartureTime == l.EstimatedDepartureTime && p.EstimatedArrivalTime == l.EstimatedArrivalTime))
                        {
                            predictions.Remove(p);
                        }

                        counter++;
                    }
                    //*********************************************************************************


                    foreach (JourneyPrediction jp in predictions)
                    {
                        DataAccess.DBModels.JourneyStopEstimation jEstimate = new JourneyStopEstimation
                        {
                            EstimatedArrivalTime = jp.EstimatedArrivalTime,
                            EstimatedAtStopSequence = jp.EstimatedAtStopSequence,
                            EstimatedDepartureTime = jp.EstimatedDepartureTime,
                            EstimatedStopSequence = jp.EstimatedStopSequence,
                            EstimationSource = jp.EstimationSource,
                            EstimationTime = jp.EstimationTime,
                            JourneyId = jp.JourneyId
                        };

                        dbContext.JourneyStopEstimations.Add(jEstimate);
                    }

                    dbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.JOURNEY_PREDICTION, Logger.GetDetailedError(ex));
                IBI.Shared.AppUtility.Log2File("PredictionQueueManager", string.Format("Error saving prediction data. {0}", Logger.GetDetailedError(ex)), true);
            }
        }

        // Sample Data :: <DatenAbrufenAntwort><Bestaetigung Zst="2016-03-03T08:38:30+01:00" Ergebnis="ok" Fehlernummer="0"/><WeitereDaten>false</WeitereDaten><AUSNachricht AboID="1"><IstFahrt><LinienID>107</LinienID><FahrtRef><FahrtID><FahrtBezeichner>9ed50ad5-5c5a-4c65-9ab5-c235adfa5e20</FahrtBezeichner><Betriebstag>2016-03-03</Betriebstag></FahrtID><FahrtStartEnde><StartHaltID>745000600</StartHaltID><Startzeit>2016-03-03T07:01:00+01:00</Startzeit><EndHaltID>615100100</EndHaltID><Endzeit>2016-03-03T07:48:00+01:00</Endzeit></FahrtStartEnde></FahrtRef><IstHalt><HaltID>615100100</HaltID><Ankunftszeit>2016-03-03T07:48:00+01:00</Ankunftszeit><Abfahrtszeit>2016-03-03T07:48:00+01:00</Abfahrtszeit><IstAnkunftPrognose>2016-03-03T07:46:30+01:00</IstAnkunftPrognose><IstAbfahrtPrognose>2016-03-03T07:48:00+01:00</IstAbfahrtPrognose></IstHalt></IstFahrt></AUSNachricht></DatenAbrufenAntwort>

        private List<IBI.Shared.Models.Journey.JourneyPrediction> ParseJourneyEstimate(int journeyId, XDocument journey)
        {
            List<IBI.Shared.Models.Journey.JourneyPrediction> jList = new List<IBI.Shared.Models.Journey.JourneyPrediction>();

            try
            {

                if (journey == null || journey.Descendants("IstHalt").Count() == 0)
                    return jList;

                int lastStopSeq, receivedStops, counter;

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    //var jStops = dbContext.JourneyStops.Where(js => js.JourneyId == journeyId);
                    lastStopSeq = dbContext.JourneyStops.Where(js => js.JourneyId == journeyId).Count();
                }

                DateTime estimationTime = FixTimeZone(journey.Descendants("Bestaetigung").Attributes("Zst").FirstOrDefault().Value);

                receivedStops = journey.Descendants("IstHalt").Count();

                counter = 1;
                foreach (var stop in journey.Descendants("IstHalt"))
                {

                    XElement sId, esDepTime, esArrTime;

                    sId = stop.Descendants("HaltID").FirstOrDefault();
                    esArrTime = stop.Descendants("IstAnkunftPrognose").FirstOrDefault();
                    esDepTime = stop.Descendants("IstAbfahrtPrognose").FirstOrDefault();

                    string stopNumber;
                    DateTime? estimatedArrivalTime = null;
                    DateTime? estimatedDepartureTime = null;



                    if (sId != null)
                    {
                        stopNumber = IBI.Shared.ScheduleHelper.TransformStopNo(sId.Value, Shared.ScheduleHelper.StopPrefix.HAFAS);
                    }

                    if (esArrTime != null)
                    {
                        estimatedArrivalTime = FixTimeZone(esArrTime.Value);
                    }
                    if (esDepTime != null)
                    {
                        estimatedDepartureTime = FixTimeZone(esDepTime.Value);
                    }


                    IBI.Shared.Models.Journey.JourneyPrediction j = new Shared.Models.Journey.JourneyPrediction();

                    j.EstimationTime = estimationTime;
                    j.EstimationSource = "MidtTrafik";
                    j.EstimatedAtStopSequence = null;
                    j.JourneyId = journeyId;


                    j.EstimatedDepartureTime = estimatedDepartureTime;
                    j.EstimatedArrivalTime = estimatedArrivalTime;
                    j.EstimatedStopSequence = (lastStopSeq - receivedStops) + counter;

                    jList.Add(j);

                    counter++;
                }//end foreach 

            }
            catch (Exception ex)
            {
                IBI.Shared.AppUtility.Log2File("PredictionQueueManager", string.Format("Error parsing prediction data. {0}", Logger.GetDetailedError(ex)), true);
            }

            return jList;
        }


        #endregion


        #region Private Helper

        private static string ThirdPartyRestCall(string journeyRef)
        {
            string result = string.Empty;

            if (!String.IsNullOrEmpty(journeyRef.ToString()))
            {
                //REST call to Sync SignItem Images ------------------------------------------------------------ 
                Uri url = new Uri(REST_PATH.Replace("{0}", journeyRef));

                System.Net.WebClient webClient = new System.Net.WebClient();

                result = webClient.DownloadString(url);
                //------------------------------------------------------------------------------------------------
            }
            return result;
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static DateTime FixTimeZone(string dateString)
        {

            try
            {
                DateTime targetDate = DateTime.ParseExact(dateString.Substring(0, dateString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                string currentZone = DateTime.Now.ToString("zzz"); // "+02:00"
                int z1Hour = int.Parse(currentZone.Substring(0, currentZone.IndexOf(":")));
                int z1Min = int.Parse(currentZone.Substring(currentZone.IndexOf(":") + 1));

                string futureZone = targetDate.ToString("zzz"); //"+01:00";
                int z2Hour = int.Parse(futureZone.Substring(0, futureZone.IndexOf(":")));
                int z2Min = int.Parse(futureZone.Substring(futureZone.IndexOf(":") + 1));

                TimeSpan t1 = new TimeSpan(z1Hour, z1Min, 0);
                TimeSpan t2 = new TimeSpan(z2Hour, z2Min, 0);

                targetDate = targetDate.AddMinutes(t2.Subtract(t1).TotalMinutes);

                return targetDate;
            }
            catch (Exception ex)
            {
                //do nothing
            }

            return DateTime.ParseExact(dateString.Substring(0, dateString.IndexOf('+')), "yyyy-MM-ddTHH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
        }


        #endregion
    }
}
