//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class PTAJourneysSignonLog
    {
        public int id { get; set; }
        public string BusNumber { get; set; }
        public string Line { get; set; }
        public System.DateTime SignonTime { get; set; }
        public string PTAJourneyNumber { get; set; }
        public bool Success { get; set; }
        public string Result { get; set; }
        public string PTA { get; set; }
    }
}
