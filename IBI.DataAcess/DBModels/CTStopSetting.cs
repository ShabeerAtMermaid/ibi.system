//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class CTStopSetting
    {
        public decimal GID { get; set; }
        public bool CheckPoint { get; set; }
        public string RejseplanenStopNumber { get; set; }
        public Nullable<int> LastTimeToShow { get; set; }
        public Nullable<bool> ExcludeLowPriorityMedia { get; set; }
    
        public virtual Stop Stop { get; set; }
    }
}
