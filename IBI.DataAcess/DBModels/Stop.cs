//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IBI.DataAccess.DBModels
{
    using System;
    using System.Collections.Generic;
    
      
     [Serializable]
    public partial class Stop
    {
        public decimal GID { get; set; }
        public string StopName { get; set; }
        public string OriginalName { get; set; }
        public Nullable<System.DateTime> OriginalCreated { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public string StopSource { get; set; }
        public System.Data.Entity.Spatial.DbGeography OriginalGPS { get; set; }
        public System.Data.Entity.Spatial.DbGeography StopGPS { get; set; }
    
        public virtual CTStopSetting CTStopSetting { get; set; }
    }
}
