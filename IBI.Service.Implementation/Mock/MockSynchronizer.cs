﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBI.Service.Implementation.Mock
{
    class MockSynchronizer
    {
                #region "Variables"
        private Hashtable customerTable = new Hashtable();
        #endregion
        #region "Properties"

        private Hashtable CustomerTable
        {
            get
            {
                return customerTable;
            }
            set
            {
                customerTable = value;
            }
        }

        #endregion
        #region "Implementation"

        public MockSynchronizer()
        {
        }

        public void CleanScreenshots(double days)
        {
            using (new IBI.Shared.CallCounter("MockSynchronizer.CleanScreenshots"))
            {

            }
        }

        public void SyncAllSchedules()
        {
            using (new IBI.Shared.CallCounter("MockSynchronizer.SyncAllSchedules"))
            {
            }
        }

        public void SyncAllBusesData()
        {
            using (new IBI.Shared.CallCounter("MockSynchronizer.SyncAllBusesData"))
            {
            }
        }

        public void SyncAllSupportData()
        {
            using (new IBI.Shared.CallCounter("MockSynchronizer.SyncAllSupportData"))
            {
            }
        }

        #endregion

    }
}
