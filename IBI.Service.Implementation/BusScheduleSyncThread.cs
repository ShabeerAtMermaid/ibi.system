using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using IBI.Shared.Models;
using IBI.DataAccess.DBModels;
using IBI.Implementation.Schedular;
using IBI.Shared.Interfaces;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.BusTree;
using IBI.Shared;

namespace IBI.Service.Implementation
{
    class BusScheduleSyncThread
    {
        #region Variables
        
        string busNumber;
        int customerId;
        private readonly ISyncScheduleFactory schedularFactory = new SyncScheduleFactory();
        
        #endregion
        
        #region Properties
        
        public string BusNumber
        {
            get
            {
                return busNumber;
            }
            set
            {
                busNumber = value;
            }
        }
        
        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }

        public ISyncScheduleFactory SchedularFactory
        {
            get
            {
                return schedularFactory;
            }
        }

        #endregion
       
        #region Implementation
        
        public BusScheduleSyncThread(string busNumber, int customerId)
        {
            this.busNumber = busNumber;
            this.customerId = customerId;
        }
        
        public static void ProcessBusSchedule(Object busObject)
        {
            BusScheduleSyncThread thread = (BusScheduleSyncThread)busObject;
            try
            {
                ISyncSchedular schedular = thread.SchedularFactory.GetSyncSchedular(thread.CustomerId);

                if (schedular != null)
                {
                     schedular.DownloadCurrentSchedule(thread.BusNumber);
                }
                System.Threading.Thread.Sleep(IBI.Shared.AppSettings.SyncScheduleBusInterval());

            }
            catch (Exception ex)
            {
                Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, ex);
            }
            finally
            {
                ThreadPool.QueueUserWorkItem(BusScheduleSyncThread.ProcessBusSchedule, thread);

            }             
        }
         
        
        #endregion
    }
}