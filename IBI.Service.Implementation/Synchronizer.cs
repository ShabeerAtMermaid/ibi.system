﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Web;
using System.Net;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using IBI.Shared.Models;
using IBI.DataAccess.DBModels;
using IBI.Implementation.Schedular;
using IBI.Shared.Interfaces;
using IBI.Shared.Diagnostics;
using IBI.Shared.Models.BusTree;
using IBI.Shared;
using System.Data;
using System.Data.Common;
using System.Xml.Linq;
using System.Reflection;
using System.Data.Objects;

namespace IBI.Service.Implementation
{
    public class Synchronizer
    {
        #region "Variables"
        private Hashtable customerTable = new Hashtable();

        private static int tCounter = 0;
        #endregion
        #region "Properties"

        private Hashtable SyncServiceTable
        {
            get;
            set;
        }

        private Hashtable SyncScheduleTable
        {
            get;
            set;
        }
        private Hashtable CustomerTable
        {
            get
            {
                return customerTable;
            }
            set
            {
                customerTable = value;
            }
        }

        #endregion
        #region "Implementation"

        public Synchronizer()
        {
            int workerThreads = int.Parse(ConfigurationManager.AppSettings["WorkerThreads"]);
            int maxAsyncThreads = int.Parse(ConfigurationManager.AppSettings["MaxAsyncThreads"]);

            ThreadPool.SetMaxThreads(workerThreads, maxAsyncThreads);

            this.SyncServiceTable = new Hashtable();
            this.SyncScheduleTable = new Hashtable();
        }


        public void UpdateJourneyCompletionStatus(int expiryMiuntes)
        {
            AppUtility.Log2File("Journey_History", "Journey Timer Started", true);

            Console.WriteLine("Journey Timer Started");

            try
            {

                using (new IBI.Shared.CallCounter("Synchronizer.UpdateJourneyCompletionStatus"))
                {

                    bool nextIteration = true;
                    int journeyPerIteration = 20;
                    while (nextIteration)
                    {
                        using (IBIDataModel dbContext = new IBIDataModel())
                        {
                            DateTime expiry = DateTime.Now.AddMinutes(-expiryMiuntes);

                            var selectedJourneysAll = (from x in dbContext.Journeys
                                                       //where x.JourneyDate != null && x.JourneyDate < expiry
                                                       where x.StartTime != null && x.StartTime < expiry
                                                       select x);

                            Console.WriteLine(selectedJourneysAll.Count().ToString() + " Journeys are older than 3 hours");
                            AppUtility.Log2File("Journey_History", selectedJourneysAll.Count().ToString() + " Journeys are older than 3 hours", true);

                            nextIteration = selectedJourneysAll.Count() > journeyPerIteration;


                            var selectedJourneys = selectedJourneysAll.Take(journeyPerIteration);

                            List<int> journeys2Delete = new List<int>();

                            int counter = 0;
                            foreach (var j in selectedJourneys)
                            {
                                dbContext.MoveJourneyToHistory(j.JourneyId);

                                Console.WriteLine("Journey " + j.JourneyId.ToString() + " & its stops successfully moved to Hostory Table");
                                AppUtility.Log2File("Journey_History", "Journey " + j.JourneyId.ToString() + " & its stops successfully moved to Hostory Table", true);

                                dbContext.Journeys.Remove(j);

                                counter += 1;
                            }

                            //var journeys2Remove = (from x in dbContext.Journeys
                            //                       where x.JourneyDate != null && x.JourneyDate < expiry
                            //                       select x).Take(journeyPerIteration);
                            //foreach (var journey in journeys2Remove)
                            //{
                            //    dbContext.Journeys.DeleteObject(journey);
                            //}

                            //dbContext.SaveChanges();

                            Console.WriteLine("##" + counter.ToString() + " journeys moved successfully. ##");
                            AppUtility.Log2File("Journey_History", "##" + counter.ToString() + " journeys moved successfully. ##", true);

                            //System.Threading.Thread.Sleep(2000);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                string message = ex.Message + " :: " + ex.StackTrace;

                if (ex.InnerException != null)
                {
                    message += Environment.NewLine + "Inner Exception: " + ex.InnerException.Message + " :: " + ex.InnerException.StackTrace;
                }
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, message);
                AppUtility.Log2File("Journey_History", "!!! ERROR !!! #" + message, true);
            }
        }

        public void CleanScreenshots(double days)
        {
            using (new IBI.Shared.CallCounter("Synchronizer.CleanScreenshots"))
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    List<IBI.DataAccess.DBModels.Client> selectedClients = (from x in dbContext.Clients
                                                                            select x).ToList();
                    foreach (IBI.DataAccess.DBModels.Client client in selectedClients)
                    {
                        if (Directory.Exists(Path.Combine(AppSettings.GetResourceDirectory(), "Screenshots\\" + client.ClientType + "\\" + client.BusNumber)))
                        {
                            DirectoryInfo targetDirectory = new DirectoryInfo(Path.Combine(AppSettings.GetResourceDirectory(), "Screenshots\\" + client.ClientType + "\\" + client.BusNumber));
                            foreach (FileInfo fInfo in targetDirectory.GetFiles())
                            {
                                if (fInfo.LastWriteTime < DateTime.Now.AddDays(-1 * days))
                                {
                                    fInfo.Delete();
                                }
                            }
                        }

                    }
                }
            }
        }



        public void SyncAllSchedules()
        {
            Logger.AppendToSystemLog(Logger.EntryTypes.Information, Logger.EntryCategories.IBI, "SyncAllSchedules [Called]");
            Console.WriteLine("SyncAllSchedules() started");
            using (new IBI.Shared.CallCounter("Synchronizer.SyncAllSchedules"))
            {
                Console.WriteLine("Using started");

                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var selectedBuses = (from x in dbContext.Buses
                                         where x.InOperation == true
                                         select x);

                    Hashtable busList;


                    foreach (var bus in selectedBuses)
                    {
                        if (this.SyncScheduleTable.ContainsKey(bus.CustomerId))
                        {
                            busList = (Hashtable)this.SyncScheduleTable[bus.CustomerId];
                        }
                        else
                        {
                            busList = new Hashtable();
                        }

                        if (busList.ContainsKey(bus.BusNumber))
                        {
                            continue;
                        }
                        //if ( bus.BusNumber == "1137" ) // a test statement for checking a single bus
                        {
                            BusScheduleSyncThread thread = new BusScheduleSyncThread(bus.BusNumber, bus.CustomerId);
                            ThreadPool.QueueUserWorkItem(BusScheduleSyncThread.ProcessBusSchedule, thread);
                            busList[bus.BusNumber] = thread;
                            this.SyncScheduleTable[bus.CustomerId] = busList;

                        }
                    }
                }
            }
        }

        public void SyncAllInService()
        {
            Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, "SyncAllInService [Called]");
            try
            {
                using (new IBI.Shared.CallCounter("Synchronizer.SyncAllInService"))
                {

                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        var selectedBuses = (from x in dbContext.Buses
                                             where x.InOperation == true
                                             select x);
                        Hashtable busList;
                        foreach (var bus in selectedBuses)
                        {
                            if (this.SyncServiceTable.ContainsKey(bus.CustomerId))
                            {
                                busList = (Hashtable)this.SyncServiceTable[bus.CustomerId];
                            }
                            else
                            {
                                busList = new Hashtable();
                            }

                            if (busList.ContainsKey(bus.BusNumber))
                            {
                                //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("There is alreadya thread for bus {0}", bus.BusNumber));
                            
                                continue;
                            }
                            tCounter++;
                            //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("--> Creating a thread for bus {0}", bus.BusNumber));
                            BusInServiceSyncThread thread = new BusInServiceSyncThread(bus.BusNumber, bus.CustomerId);
                            thread.MyGUID = Guid.NewGuid();
                            thread.ThreadNo = tCounter;
                            busList[bus.BusNumber] = thread;
                            this.SyncServiceTable[bus.CustomerId] = busList;
                            ThreadPool.QueueUserWorkItem(BusInServiceSyncThread.ProcessBusInService, thread);
                            //Logger.AppendToSystemLog(Logger.EntryTypes.Warning, Logger.EntryCategories.INSERVICE_SYNC, string.Format("--> Queueing a thread for bus {0}", bus.BusNumber));
                            
                            System.Threading.Thread.Sleep(50);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(Logger.GetDetailedError(ex));
            }
        }

        public void SyncAllBusesData()
        {
            using (new IBI.Shared.CallCounter("Synchronizer.SyncAllBusesData"))
            {
                using (IBIDataModel dbContext = new IBIDataModel())
                {
                    var buses = dbContext.Buses.Where(bus => bus.Groups.Count() == 0).ToList();
                    foreach (var bus in buses)
                    {
                        if (bus.Groups.Count == 0)
                        {
                            var groups = dbContext.Groups.Where(y => y.CustomerId == bus.CustomerId && y.ParentGroupId == null).ToList();
                            IBI.DataAccess.DBModels.Group groupToAdd = null;
                            foreach (var group in groups)
                            {
                                groupToAdd = group;
                            }
                            if (groupToAdd != null)
                            {
                                bus.Groups.Add(groupToAdd);
                            }
                        }
                    }
                    dbContext.SaveChanges();
                }
            }
        }



        public void SyncAllSupportData()
        {
            Hashtable customersTicketDetail = null;

            using (new IBI.Shared.CallCounter("Synchronizer.SyncAllSupportData"))
            {
                Logger.LogMessage("NovoSyncAllSupportData called");
                try
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        bool navisionTicketSynEnabled = (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionTicketSynchronization"])) ? bool.Parse(ConfigurationManager.AppSettings["NavisionTicketSynchronization"]) : false;
                        int[] navisionAccountIDs = (navisionTicketSynEnabled == true & !string.IsNullOrEmpty(ConfigurationManager.AppSettings["NavisionAccountIDs"])) ? Array.ConvertAll(ConfigurationManager.AppSettings["NavisionAccountIDs"].Split(new string[] { ";" }, StringSplitOptions.None), int.Parse) : new int[] { };

                        var selectedBuses = (from x in dbContext.Buses
                                             where !(navisionAccountIDs.Contains(x.CustomerId))
                                             select x);

                        if (selectedBuses.Count() > 0)
                        {
                            customersTicketDetail = GetServiceLevel();

                            foreach (var bus in selectedBuses)
                            {
                                if (bus == null || String.IsNullOrEmpty(bus.BusNumber))
                                    continue;

                                int serviceLevel = 0;
                                string serviceDetail = "";
                                if (customersTicketDetail != null)
                                {
                                    if (customersTicketDetail.Contains(bus.BusNumber.ToString()))
                                    {
                                        string[] tickLevelDetail = (string[])customersTicketDetail[bus.BusNumber.ToString()];
                                        serviceLevel = Int32.Parse(tickLevelDetail[0]);
                                        serviceDetail = tickLevelDetail[1].TrimEnd('|');
                                    }
                                }
                                bus.ServiceLevel = serviceLevel;
                                bus.ServiceDetail = serviceDetail;

                                var selectedClients = (from x in dbContext.Clients
                                                       where x.BusNumber == bus.BusNumber
                                                       select x);
                                foreach (var client in selectedClients)
                                {
                                    client.MarkedForService = GetMarkedForService(client.MacAddress);
                                }
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                    Logger.LogError(ex);
                }
                finally
                {
                    if (customersTicketDetail != null)
                    {
                        customersTicketDetail.Clear();
                        customersTicketDetail = null;
                    }
                    Logger.LogMessage("NovoSyncAllSupportData completed");
                }
            }
        }

        private Hashtable GetServiceLevel()
        {
            // [KHI]: we will return following ServiceCase Attributes
            // 0:  ServiceLevel, 1: TicketNumber, 2: TicketHeadline

            int serviceLevel = 0;
            string serviceDetail = "";
            Hashtable busTickets = null;

            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NovoDatabaseConnection"].ToString()))
            {
                using (new IBI.Shared.CallCounter("Synchronizer.getServiceLevel"))
                {
                    String script = "SELECT customValues.[UDF_VALUES_VARCHAR] AS [BUSNUMBER], cases.[CASE_ID], cases.[CASE_TITLE], levels.[DEPART_ID] AS [LEVEL_ID], levels.[DEPART_NAME] AS [LEVEL]" + Environment.NewLine +
                                    "FROM" + Environment.NewLine +
                                    "  [UDF_VALUES] customValues" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [CUSTOMERS] customers" + Environment.NewLine +
                                    "  ON customValues.[TABLE_REF]=customers.[CUSTOMER_ID]" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [ACCOUNTS] accounts" + Environment.NewLine +
                                    "  ON customers.[ACCOUNT_REF]=accounts.[ACCOUNT_ID]" + Environment.NewLine +
                                    "  INNER JOIN" + Environment.NewLine +
                                    "  [CASES] cases" + Environment.NewLine +
                                    "  ON customValues.[TABLE_REF]=cases.[CUSTOMER_REF]" + Environment.NewLine +
                                    "  INNER JOIN [DEPARTMENTS] levels" + Environment.NewLine +
                                    "  ON cases.[DEPART_REF]=levels.[DEPART_ID]" + Environment.NewLine +
                                    "WHERE customValues.[DEFINITION_REF]=2" + Environment.NewLine +
                                    "  AND cases.[CASE_STATUS]<>8" + Environment.NewLine +
                                    "  AND accounts.[ACCOUNT_ID] IN (" + ConfigurationManager.AppSettings["NovoAccountIDs"].Replace(";", ",") + ") " + Environment.NewLine;

                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        try
                        {
                            connection.Open();

                            using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                            {
                                if (dataReader != null)
                                {
                                    if (dataReader.HasRows)
                                    {
                                        busTickets = new Hashtable();
                                        string[] levelAndDetail;
                                        while (dataReader.Read())
                                        {
                                            serviceLevel = dataReader.GetInt32(dataReader.GetOrdinal("LEVEL_ID"));
                                            switch (serviceLevel)
                                            {
                                                case 2: // "1. level":
                                                    { serviceLevel = 1; break; }

                                                case 3: // "2. level":
                                                    { serviceLevel = 2; break; }

                                                case 4: // "3. level":
                                                    { serviceLevel = 3; break; }

                                                case 6: // "4. level":
                                                    { serviceLevel = 4; break; }

                                                case 7: // "Installation":
                                                    { serviceLevel = 5; break; }

                                                case 5: // "no level (Kundens ansvar / Customer is Responsible)":
                                                    { serviceLevel = 6; break; }

                                                case 1: // "Super Admin":
                                                    { serviceLevel = 7; break; }
                                            }
                                            serviceDetail = "Ticket# " + AppUtility.IsNullStr(dataReader["CASE_ID"]) + "~" + AppUtility.IsNullStr(dataReader["CASE_TITLE"]) + "|";

                                            if (busTickets.Contains(dataReader["BUSNUMBER"]))
                                            {
                                                levelAndDetail = (string[])busTickets[dataReader["BUSNUMBER"]];
                                                levelAndDetail[0] = AppUtility.IsNullStr(serviceLevel, "0");
                                                levelAndDetail[1] += serviceDetail;
                                                busTickets[dataReader["BUSNUMBER"]] = levelAndDetail;
                                            }
                                            else
                                            {
                                                levelAndDetail = new string[2];
                                                levelAndDetail[0] = AppUtility.IsNullStr(serviceLevel, "0");
                                                levelAndDetail[1] += serviceDetail;
                                                busTickets.Add(dataReader["BUSNUMBER"], levelAndDetail);
                                            }
                                        }
                                        return busTickets;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Novo, "NOVO PROB: " + ex);
                        }
                        finally
                        {
                            if (connection != null)
                                connection.Close();
                        }
                    }
                }
            }
            return busTickets;
        }



        private List<int> NavisionCustomerList()
        {
            try
            {
                string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NavisionSynchronization_Settings.xml");
                XDocument xdoc = XDocument.Load(filePath);
                List<int> NavisionCustomerlist = xdoc.Descendants("navistioncustomer")
                               .Select(x => int.Parse(x.Attribute("customerID").Value))
                               .ToList();
                return (NavisionCustomerlist != null && NavisionCustomerlist.Count > 0) ? NavisionCustomerlist : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        private List<int> IBICustomersLinkwithNavision()
        {
            try
            {
                string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NavisionSynchronization_Settings.xml");
                XDocument xdoc = XDocument.Load(filePath);
                List<int> ibiCustomers = xdoc.Descendants("ibicustomer")
                               .Select(x => int.Parse(x.Attribute("customerID").Value))
                               .ToList();
                return (ibiCustomers != null && ibiCustomers.Count > 0) ? ibiCustomers : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        //private Hashtable IBICustomersLinkwithNavision()
        //{
        //    try
        //    {
        //        Hashtable 
        //        string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NavisionSynchronization_Settings.xml");
        //        XDocument xdoc = XDocument.Load(filePath);
        //        List<int> ibiCustomers = xdoc.Descendants("ibicustomer")
        //                       .Select(x => int.Parse(x.Attribute("customerID").Value))
        //                       .ToList();
        //        return (ibiCustomers != null && ibiCustomers.Count > 0) ? ibiCustomers : null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}


        private List<string> NavisionMaskingPatterns(int ibiCustomerId, string busNumber)
        {
            try
            {
                string filePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NavisionSynchronization_Settings.xml");
                XDocument xdoc = XDocument.Load(filePath);
                List<string> maskingPatterns = (from p in xdoc.Descendants("ibicustomer")
                                                where int.Parse(p.Attribute("customerID").Value) == ibiCustomerId
                                                select p.Descendants("maskingpattern")
                                                       .Select(x => x.Value.Replace("#BUSNO#", busNumber)).Distinct()
                                            ).FirstOrDefault().ToList();

                return (maskingPatterns != null && maskingPatterns.Count > 0) ? maskingPatterns : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public void SyncAllSupportDataFromNavision()
        {
            Hashtable customersTicketDetail = null;

            using (new IBI.Shared.CallCounter("Synchronizer.SyncAllSupportData"))
            {
                Logger.LogMessage("SyncAllSupportDataFromNavision called");
                try
                {
                    using (IBIDataModel dbContext = new IBIDataModel())
                    {
                        List<int> ibicustomersLinkwithNavision = IBICustomersLinkwithNavision();
                        if (ibicustomersLinkwithNavision == null) return;

                        var selectedBuses = (from x in dbContext.Buses
                                             where ibicustomersLinkwithNavision.Contains(x.CustomerId)
                                             select x);
                        if (selectedBuses != null && selectedBuses.Count() > 0)
                        {
                            int serviceLevel;
                            string serviceDetail;

                            customersTicketDetail = GetServiceLevelFromNavision();

                            foreach (var bus in selectedBuses)
                            {
                                if (bus == null || String.IsNullOrEmpty(bus.BusNumber))
                                    continue;

                                serviceLevel = 0;
                                serviceDetail = "";
                                if (customersTicketDetail != null && customersTicketDetail.Count > 0)
                                {
                                    List<string> maskingPatterns = NavisionMaskingPatterns(bus.CustomerId, bus.BusNumber);

                                    foreach (string maskingPattern in maskingPatterns)
                                    {
                                        if (customersTicketDetail.Contains(maskingPattern))
                                        {
                                            string[] tickLevelDetail = (string[])customersTicketDetail[maskingPattern];
                                            serviceLevel = Int32.Parse(tickLevelDetail[0]);
                                            serviceDetail += tickLevelDetail[1];
                                        }
                                    }
                                }
                                bus.ServiceLevel = serviceLevel;
                                bus.ServiceDetail = serviceDetail.TrimEnd('|');

                                var selectedClients = (from x in dbContext.Clients
                                                       where x.BusNumber == bus.BusNumber
                                                       select x);

                                foreach (var client in selectedClients)
                                {
                                    client.MarkedForService = GetMarkedForService(client.MacAddress);
                                }
                            }
                            dbContext.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {

                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI, ex);
                    Logger.LogError(ex);
                }
                finally
                {
                    if (customersTicketDetail != null)
                    {
                        customersTicketDetail.Clear();
                        customersTicketDetail = null;
                    }
                    Logger.LogMessage("SyncAllSupportDataFromNavision completed");
                }
            }
        }

        private Hashtable GetServiceLevelFromNavision()
        {
            // [SHA]: we will return following Novision ServiceCase Attributes
            // 1: Level - Hotline/SWAP, 2: Level - Onsite Handling

            List<int> navisionCustomers = NavisionCustomerList();
            if (navisionCustomers == null) return null;

            Hashtable busTickets = null;
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["NavisionDatabaseConnection"].ToString()))
            {
                try
                {
                    using (new IBI.Shared.CallCounter("Synchronizer.getServiceLevel"))
                    {

                        //////String script = "SELECT [LocationID], [TicketID], [Title], [Level], [LevelStatus]" + Environment.NewLine +
                        //////                "FROM [TicketRepl] AS NavisionTickets" + Environment.NewLine +
                        //////                "WHERE ([Level] like '1.%' OR [Level] like '2.%') " + Environment.NewLine +
                        //////                "  AND ([LevelStatus] NOT LIKE '%Closed%' AND [LevelStatus] NOT LIKE '%Solved%') " + Environment.NewLine +
                        //////                "  AND CustomerID IN ('" + string.Join("','", navisionCustomers) + "') ORDER BY [LocationID]" + Environment.NewLine;

                        string ticketLevelStatusID = ConfigurationManager.AppSettings["TicketLevelStatusID"].ToString();
                        ticketLevelStatusID = string.IsNullOrEmpty(ticketLevelStatusID) ? string.Empty : ticketLevelStatusID.Replace(";", "','");

                        String script = "SELECT [LocationID], [TicketID], [Title], [Level], [LevelStatus]" + Environment.NewLine +
                                        "FROM [TicketReplIbi] AS NavisionTickets" + Environment.NewLine +
                                        "WHERE ([TicketLevelStatusID]  IN ('" + ticketLevelStatusID + "') " + Environment.NewLine +
                                        "  AND CustomerID IN ('" + string.Join("','", navisionCustomers) + "')) ORDER BY [LocationID]" + Environment.NewLine;

                        using (SqlCommand command = new SqlCommand(script, connection))
                        {
                            connection.Open();
                            using (new IBI.Shared.CallCounter("Database calls"))
                            {
                                using (SqlDataReader dataReader = command.ExecuteReader(CommandBehavior.CloseConnection))
                                {
                                    if (dataReader != null)
                                    {
                                        if (dataReader.HasRows)
                                        {
                                            busTickets = new Hashtable();
                                            string[] levelAndDetail;
                                            while (dataReader.Read())
                                            {
                                                if (busTickets.Contains(dataReader["LocationID"]))
                                                {
                                                    levelAndDetail = (string[])busTickets[dataReader["LocationID"]];
                                                    levelAndDetail[1] += "Ticket# " + AppUtility.IsNullStr(dataReader["TicketID"]) + "~" + AppUtility.IsNullStr(dataReader["Title"]) + "|";
                                                    busTickets[dataReader["LocationID"]] = levelAndDetail;
                                                }
                                                else
                                                {
                                                    levelAndDetail = new string[2];
                                                    int level;
                                                    bool isNumeric = Int32.TryParse(AppUtility.IsNullStr(dataReader["Level"], "0").Substring(0, 1), out level);
                                                    levelAndDetail[0] = isNumeric ? level.ToString() : "1";
                                                    levelAndDetail[1] += "Ticket# " + AppUtility.IsNullStr(dataReader["TicketID"]) + "~" + AppUtility.IsNullStr(dataReader["Title"]) + "|";
                                                    busTickets.Add(dataReader["LocationID"], levelAndDetail);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return busTickets;
                }
                catch (Exception ex)
                {
                    if (connection != null)
                    {
                        if (connection.State == System.Data.ConnectionState.Open)
                        {
                            connection.Close();
                            connection.Dispose();
                        }
                    }
                    Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.Novo, "Novision PROB: " + ex);
                    throw ex;
                }
            }
        }

        private bool GetMarkedForService(String macAddress)
        {
            using (new IBI.Shared.CallCounter("SystemServiceController.getMarkedForService"))
            {
                using (vTouchDataModel vTouchContext = new vTouchDataModel())
                {
                    var selectedIpPlayers = (from x in vTouchContext.vTouch_Server_Model_IPPlayer
                                             where x.MACAddress == macAddress
                                             select x).ToList();
                    foreach (var player in selectedIpPlayers)
                    {
                        return player.MarkedForService.Value;
                    }
                }

                return false;
            }
        }

        #endregion
    }
}
