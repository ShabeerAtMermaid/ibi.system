﻿using IBI.Shared.ConfigSections;
using IBI.Shared.Diagnostics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml.Linq;

namespace IBI.HaConVDV
{
    public class SubscriptionTimer
    {
        public Timer VdvTimer { get; set; }
        public VdvSubscriptionConfiguration subConfiguration { get; set; }
        bool DataReadyStatus { get; set; }
        //= HaConCustomerConfiguration.GetConfigurations(int.Parse(System.Configuration.ConfigurationManager.AppSettings["HaCon_DefaultCustomer"]));

        private static Int64 _subscriptionCounter;
        private static Int64 SubscriptionCounter
        {
            get
            {
                if (_subscriptionCounter != null && _subscriptionCounter >= 0)
                {
                    _subscriptionCounter += 1;
                    return _subscriptionCounter;
                }
                else
                {
                    _subscriptionCounter = 1;
                    return _subscriptionCounter;
                }
            }
            set
            {
                _subscriptionCounter = value;
            }
        }

        private static Int64 Counter { get; set; }

        private string LogFileName
        {
            get { return string.Format("VDV_Subscription_{0}", subConfiguration.Sender); }
        }
        //public CustomerTimer(){}

        public SubscriptionTimer(VdvSubscriptionConfiguration config)
        {
            Counter++;

            this.subConfiguration = config;

            this.VdvTimer = new Timer(TimeSpan.FromSeconds(10 * (Counter % 2) + 1).TotalMilliseconds);
            this.VdvTimer.Elapsed += VdvTimer_Elapsed;
            this.VdvTimer.Enabled = true;
        }

        public Int64 SubscriptionId { get; set; }

        #region Events


        public void Dispose()
        {
            this.VdvTimer.Enabled = false;
            this.VdvTimer.Dispose();

        }

        private void VdvTimer_Elapsed(object sender, ElapsedEventArgs e)
        {

            int jTimerInterval = 5;
            try
            {

                jTimerInterval = this.subConfiguration.JourneyTimerInterval;

                AppUtility.WriteLine(string.Format("VdvJourneyImporterTimer Hit"));
                this.VdvTimer.Enabled = false;

                if (AppSettings.HaConJourneyImporterEnabled())
                {
                    VdvJourneyImporter();
                }
            }
            catch (Exception ex)
            {
                AppUtility.Log2File(LogFileName, "Error Occured [" + ex.Message + "]\n" + ex.StackTrace, true);
            }
            finally
            {
                this.VdvTimer.Interval = TimeSpan.FromMinutes(jTimerInterval).TotalMilliseconds;
                this.VdvTimer.Enabled = true;
            }
        }

        #endregion


        #region "Functions"


        public void VdvJourneyImporter()
        {
            //CustomerId = .CustomerId;
            //CustomerConfiguration.ServerAddress = new Uri(this.CustomerConfiguration.CustomerConfiguration.ServerAddress);

            Console.WriteLine("HaCon Journey Importer Timer Started");


            try
            {

                using (new IBI.Shared.CallCounter("Synchronizer.HafasJourneyImporter"))
                {
                    //HaconDBController.FillRouteDirections(this.CustomerId);
                    //HaconDBController.FillSchedules(this.CustomerId);

                    int iteration = 1;


                    int totIterations = Convert.ToInt32(((subConfiguration.FutureDaysBuffer * 1440) / subConfiguration.FutureMinutes));


                    DateTime ValidFrom = DateTime.Now;
                    DateTime ValidUntil = ValidFrom;// DateTime.Now;

                    AppUtility.Log2File(LogFileName, "\n\n- PERIODIC JOB STARTED  -\n\n", true);

                    while (iteration <= totIterations)
                    {

                        this.DataReadyStatus = false;

                        //HaconFutureJourneyManager hf = new HaconFutureJourneyManager();

                        if (iteration == 1)
                        {
                            string defaultStartFetchTime = ConfigurationManager.AppSettings["DefaultStartFetchTime"].ToString(); //this date should be in this format : yyyy-MM-ddTHH:mm

                            if (string.IsNullOrEmpty(defaultStartFetchTime))
                            {
                                ValidFrom = DateTime.Now;
                            }
                            else
                            {
                                ValidFrom = DateTime.ParseExact(defaultStartFetchTime, "yyyy-MM-ddTHH:mm", System.Globalization.CultureInfo.InvariantCulture);
                            }
                        }
                        else
                        {
                            ValidFrom = ValidUntil;
                        }

                        ValidUntil = ValidFrom.AddMinutes(subConfiguration.FutureMinutes);

                        DateTime subscriptionTime = DateTime.Now;
                        SubscriptionId = Subscribe(ValidFrom, ValidUntil);


                        DataReadyStatus = WaitForDataReadyStatus(subConfiguration.MaxTimeToWaitForDataReady, subscriptionTime);

                        int chunkNo = 0;

                        bool pendingData = true;
                        while (pendingData)
                        {
                            pendingData = PollData(SubscriptionId, ++chunkNo);
                            System.Threading.Thread.Sleep(5000);
                        }

                        DeleteSubscription();

                        iteration++;
                    }

                    AppUtility.Log2File(LogFileName, "\n\n- PERIODIC JOB ENDED  -\n\n", true);
                    //HaconDBController.ClearZonesCache();
                    //HaconDBController.ClearSchedulesCache();
                    //HaconDBController.ClearRouteCache();
                }
            }
            catch (Exception ex)
            {
                string message = IBI.Shared.Diagnostics.Logger.GetDetailedError(ex); //ex.Message + " :: " + ex.StackTrace;

                //if (ex.InnerException != null)
                //{
                //    message += Environment.NewLine + "Inner Exception: " + ex.InnerException.Message + " :: " + ex.InnerException.StackTrace;
                //}
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, message);
                AppUtility.Log2File(LogFileName, "!!! ERROR !!! #" + message, true);
            }
        }

        private bool WaitForDataReadyStatus(int maxWaitInMinutes, DateTime subscriptionTime)
        {
            Console.WriteLine("Waiting for DataReadyPing from VDV EndPoint");


            bool doWait = true;

            while (doWait)
            {
                //IBI.Shared.MSMQ.VDVQueueManager queue = new Shared.MSMQ.VDVQueueManager(CustomerConfiguration.Sender);
                //string data = queue.ReadNext();

                string data = CheckDataReadyPing();


                if (subscriptionTime.AddMinutes(maxWaitInMinutes) < DateTime.Now)
                {
                    AppUtility.Log2File(LogFileName, "!!! No DataReadyPing() received from HACON through VDV End Point !!! VDV now will try to Fetch/Poll Data forcefully", true);
                    Console.WriteLine("!!! No DataReadyPing() received from HACON through VDV End Point !!! VDV now will try to Fetch/Poll Data forcefully");
                    doWait = false; //forcefully set to true :( even after maxWaitMinutes, we didn't received a DataReadyPing from HaCon
                }
                else
                {
                    if (!string.IsNullOrEmpty(data))
                    {
                        DateTime DataReadyPing = DateTime.ParseExact(data, "MM-dd-yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        if (DataReadyPing >= subscriptionTime && DataReadyPing < DateTime.Now)
                        {//it's a valid subscription response
                            AppUtility.Log2File(LogFileName, "DataReadyPing() successfully received from HACON through VDV End Point :)", true);
                            Console.WriteLine("DataReadyPing() successfully received from HACON through VDV End Point :)");
                            doWait = false; // happily set to true :) we have got DataReadyPing from HaCon
                        }
                    }
                }

                System.Threading.Thread.Sleep(2000);

            }

            return true;

        }


        private void RetrieveServerStatus()
        {
            XDocument requestDoc = new XDocument(
                 new XElement("StatusRequest",
                     new XAttribute("Sender", subConfiguration.Sender),
                     new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"))
                 )
             );

            Uri requestUri = new Uri(subConfiguration.ServerAddress, subConfiguration.ServerStatusUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            AppUtility.Log2File(LogFileName, "Server Status Retreived : " + replyDoc.ToString(), true);
        }

        public Int64 Subscribe(DateTime validFrom, DateTime validUntil)
        {

            StringBuilder log = new StringBuilder();

            Int64 subId = SubscriptionCounter;

            XDocument requestDoc = new XDocument(
                            new XElement("SubscriptionRequest",
                                new XAttribute("Sender", subConfiguration.Sender),
                                new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                new XElement("SISRefSubscription",
                                    new XAttribute("SubscriptionID", subId.ToString()),
                                    new XAttribute("ValidUntilTimeStamp", DateTime.Now.AddMinutes(subConfiguration.SubscriptionValidityMinutes).ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                    new XElement("TimeWindow",
                                        new XElement("ValidFrom", validFrom.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                        new XElement("ValidUntil", validUntil.ToString("yyyy-MM-ddTHH:mm:sszzz"))

                                    )//, new XElement("LineFilter", new XElement("LineID", "1A"))
                                )
                            )
                        );

            log.AppendLine(Environment.NewLine + "################################################################");
            log.AppendLine("Subscription Starts: " + requestDoc.ToString());

            //AppUtility.Log2File("Hacon_" + SubscriptionId.ToString(), "Subscription Starts: " + requestDoc.ToString(), true);

            Uri requestUri = new Uri(subConfiguration.ServerAddress, subConfiguration.SubscriptionUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            log.AppendLine("Subscription Starts Response: " + replyDoc.ToString());

            //AppUtility.Log2File("Hacon_" + SubscriptionId.ToString(), "Subscription Starts Response: " + replyDoc.ToString(), true);

            AppUtility.Log2File(LogFileName, log.ToString(), true);

            return subId;
        }


        public bool PollData(Int64 subId, int chunkNo)
        {
            //string result = string.Empty;
            bool result = false;

            XDocument requestDoc = new XDocument(
                 new XElement("DataSupplyRequest",
                     new XAttribute("Sender", subConfiguration.Sender),
                     new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                     new XElement("AllData", DataReadyStatus ? "false" : "true")
                 )
             );

            AppUtility.Log2File(LogFileName, "DataSupplyRequest : " + requestDoc.ToString());

            DataReadyStatus = true;

            Uri requestUri = new Uri(subConfiguration.ServerAddress, subConfiguration.PollDataUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            if (String.IsNullOrEmpty(replyDoc.ToString()))
            {
                AppUtility.Log2File(LogFileName, "No data returned with Data Supply Request", true);
                return false;
            }

            //import Future Journeys
            //HaconDBController.HCustConfig = CustomerConfiguration;
            HaconDBController.ProcessPollData(replyDoc, SubscriptionId, chunkNo, subConfiguration);

            //replyDoc.Save(Path.Combine(AppSettings.GetResourceDirectory(), "Hafas\\data_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml"));

            AppUtility.Log2File(LogFileName, "DataSupplyRequest: " + replyDoc.ToString().Length + " bytes received", true);
            //this.txtDataSupplyOutput.Text = "Data Received: " + replyDoc.ToString().Length + " bytes received" + Environment.NewLine + "D:\\Data\\IIS\\mermaid.dk\\test01\\Hafas\\App_Data\\data_" + DateTime.Now.ToString("yyyyyMMddHHmmss") + ".xml";

            string pendingData = replyDoc.Root.Element("PendingData").Value;

            AppUtility.Log2File(LogFileName, "PendingData value : " + pendingData, true);

            if (pendingData == "false")
            {
                AppUtility.Log2File(LogFileName, "Pending Data Ends ... call for delete subscription", true);

                //DeleteSubscription();
                result = false;
            }
            else
            {
                //result = DataReadyAnswer();
                result = true;
            }


            return result;
        }

        public void DeleteSubscription()
        {
            //return;
            /*
             <?xml version="1.0" encoding="iso-8859-15"?>
<SubscriptionRequest Sender="Test" TimeStamp="2014-07-18T12:24:58+02:00" >
  <DeleteSubscriptionsAll>true</DeleteSubscriptionsAll>
</SubscriptionRequest>
             */
            StringBuilder log = new StringBuilder();

            DataReadyStatus = false;

            XDocument requestDoc = new XDocument(
                            new XElement("SubscriptionRequest",
                                new XAttribute("Sender", subConfiguration.Sender),
                                new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                new XElement("DeleteSubscriptionsAll", "true")
                            )
                        );


            Uri requestUri = new Uri(subConfiguration.ServerAddress, subConfiguration.SubscriptionUrl);


            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            log.AppendLine("DeleteSubsription Response : " + replyDoc.ToString());
            log.AppendLine("################################################################");
            AppUtility.Log2File(LogFileName, log.ToString(), true);
        }

        private string DataReadyAnswer()
        {
            return string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><DataReadyAnswer ><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></DataReadyAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));


            //XDocument requestDoc = new XDocument(
            //                           new XElement("DataReadyAnswer",
            //                               new XElement("Acknowledge",
            //                               new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
            //                               new XAttribute("Result", "ok"),
            //                               new XAttribute("ErrorNumber", "0")
            //                               )
            //                           )
            //                       );


            //Uri requestUri = new Uri(CustomerConfiguration.ServerAddress, "Mermaid/sisref/status.xml");

            //XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            //Logger.Log("DataReadyAnswer Response : " + requestDoc.ToString());

            //return requestDoc.ToString(); //replyDoc.ToString();
        }


        #endregion

        #region Private Helper

        private XDocument PerformRequest(Uri uri, XDocument data)
        {
            string encoding = "iso-8859-15";

            byte[] requestData = Encoding.GetEncoding(encoding).GetBytes(data.ToString());

            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "text/xml; charset=" + encoding;
            request.ContentLength = requestData.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
                requestStream.Close();
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
                {
                    return XDocument.Parse(responseReader.ReadToEnd());
                }
            }
        }


        private string CheckDataReadyPing()
        {
            string result = string.Empty;
            string url = subConfiguration.DataReadyStatusUrl;

            try
            {
                //REST call to Sync SignItem Images ------------------------------------------------------------
                System.Net.WebClient webClient = new System.Net.WebClient();
                result = webClient.DownloadString(url);
            }
            catch (Exception ex)
            {
                AppUtility.Log2File(LogFileName, "!!! ERROR !!! Failed to read DataReadyPing Status from URL: " + url, true);
            }

            return result;
        }
        #endregion

    }
}
