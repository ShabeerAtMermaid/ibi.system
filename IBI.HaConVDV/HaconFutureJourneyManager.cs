﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
//using IBI.Shared;
using IBI.Shared.Diagnostics;
using IBI.DataAccess.DBModels;
using System.Data.Entity.Spatial;
using System.Configuration;
using IBI.Shared.ConfigSections;
using System.Timers;
//using mermaid.BaseObjects.Diagnostics;

namespace IBI.HaConVDV
{
    public class HaconFutureJourneyManager :IDisposable
    {
        #region Data Members


        private Timer HaConJourneyImporterTimer;

        //private static int HaConCounter { get; set; }
        //public DateTime NextValidFrom { get; set; }
        //public DateTime NextValidUntil { get; set; }

        private Uri serverAddress = null; // new Uri("http://demo.hafas.de/mermaid/vdv/");

        public static bool DataReadyStatus = false;


        public static int CustomerId { get; set;  }


        public HaConCustomerConfiguration HCustConfig { get; set; }

        #endregion


        #region Constructor

        public HaconFutureJourneyManager()
        {
            this.HaConJourneyImporterTimer = new Timer(TimeSpan.FromSeconds(5).TotalMilliseconds);
            this.HaConJourneyImporterTimer.Elapsed += HaConJourneyImporterTimer_Elapsed;            
            this.HaConJourneyImporterTimer.Enabled = true;
            
        }
         

        #endregion

        #region Events


        public void Dispose()
        {
            this.HaConJourneyImporterTimer.Enabled = false;
            this.HaConJourneyImporterTimer.Dispose();

        }

        private void HaConJourneyImporterTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //if (haconTimers.Count == 0)
            //{
            //    // read all customers from config
            //    // create a timer based on each config entry
            //    // add to haconTimers array

            //}

            int jTimerInterval = 5;
            try
            {
                HaConCustomerConfiguration hCustConfig = HaConCustomerConfiguration.GetConfigurations(int.Parse(System.Configuration.ConfigurationManager.AppSettings["HaCon_DefaultCustomer"]));

                jTimerInterval = hCustConfig.JourneyTimerInterval;

                AppUtility.WriteLine("HafasJourneyImporterTimer Hit");
                this.HaConJourneyImporterTimer.Enabled = false;

                if (AppSettings.HaConJourneyImporterEnabled())
                {
                  
                    //if (HaConCounter == 0)
                    //{
                    //    HaconFutureJourneyManager.NextValidFrom = DateTime.Now;
                    //    HaconFutureJourneyManager.NextValidUntil = DateTime.Now.AddMinutes(hCustConfig.FutureMinutes);
                    //    HaConCounter++;
                    //}

                     HaconJourneyImporter(hCustConfig);
                }
            }
            catch (Exception ex)
            {
                AppUtility.Log2File("Hacon_Journeys", "Error Occured [" + ex.Message + "]\n" + ex.StackTrace, true);
            }
            finally
            {
                this.HaConJourneyImporterTimer.Interval = TimeSpan.FromMinutes(jTimerInterval).TotalMilliseconds;
                this.HaConJourneyImporterTimer.Enabled = true;
            }
        }

        #endregion


        #region Private Helper

        private XDocument PerformRequest(Uri uri, XDocument data)
        {
            string encoding = "iso-8859-15";

            byte[] requestData = Encoding.GetEncoding(encoding).GetBytes(data.ToString());

            HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;
            request.Method = "POST";
            request.ContentType = "text/xml; charset=" + encoding;
            request.ContentLength = requestData.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(requestData, 0, requestData.Length);
                requestStream.Close();
            }

            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
                {
                    return XDocument.Parse(responseReader.ReadToEnd());
                }
            }
        }

        #endregion


        public void HaconJourneyImporter( HaConCustomerConfiguration hCustConfig)
        {
            HCustConfig = hCustConfig;
            CustomerId = HCustConfig.CustomerId;
            serverAddress = HCustConfig.ServerAddress; 

            Console.WriteLine("Hafas Journey Importer Timer Started");


            try
            {

                using (new IBI.Shared.CallCounter("Synchronizer.HafasJourneyImporter"))
                { 
                    HaconDBController.FillRoutesOfCurrentCustomer(CustomerId);
                    HaconDBController.FillSchedulesOfCurrentCustomer(CustomerId);

                    HaconFutureJourneyManager.DataReadyStatus = false;

                    //HaconFutureJourneyManager hf = new HaconFutureJourneyManager();

                    DateTime ValidFrom = DateTime.Now;
                    DateTime ValidUntil = DateTime.Now.AddMinutes(hCustConfig.FutureMinutes);


                    SubscribeForUpdates(ValidFrom, ValidUntil);

                    bool pendingData = true;
                    while (pendingData)
                    {
                        pendingData = PollData();
                        System.Threading.Thread.Sleep(5000);
                    }

                    DeleteSubscription();

                    //NextValidFrom = NextValidUntil;
                    //NextValidUntil = NextValidUntil.AddMinutes(hCustConfig.FutureMinutes);
                   

                    HaconDBController.ClearZonesCache();
                    HaconDBController.ClearSchedulesCache();
                    HaconDBController.ClearRouteCache();
                }
            }
            catch (Exception ex)
            {
                string message = IBI.Shared.Diagnostics.Logger.GetDetailedError(ex); //ex.Message + " :: " + ex.StackTrace;

                //if (ex.InnerException != null)
                //{
                //    message += Environment.NewLine + "Inner Exception: " + ex.InnerException.Message + " :: " + ex.InnerException.StackTrace;
                //}
                Logger.AppendToSystemLog(Logger.EntryTypes.Error, Logger.EntryCategories.IBI_SERVER, message );
                AppUtility.Log2File("Hacon", "!!! ERROR !!! #" + message, true);
            }
        }

         
        private void RetrieveServerStatus()
        {
            XDocument requestDoc = new XDocument(
                 new XElement("StatusRequest",
                     new XAttribute("Sender", HCustConfig.HaConName),
                     new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"))
                 )
             );

            Uri requestUri = new Uri(serverAddress, HCustConfig.ServerStatusUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            AppUtility.Log2File("Hacon", "Server Status Retreived : " + replyDoc.ToString(), true);
        }

        public void SubscribeForUpdates(DateTime validFrom, DateTime validUntil)
        {

            StringBuilder log = new StringBuilder();

            XDocument requestDoc = new XDocument(
                            new XElement("SubscriptionRequest",
                                new XAttribute("Sender", HCustConfig.HaConName),
                                new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                new XElement("SISRefSubscription",
                                    new XAttribute("SubscriptionID", "1"),
                                    new XAttribute("ValidUntilTimeStamp", DateTime.Now.AddMinutes(HCustConfig.SubscriptionValidityMinutes).ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                    new XElement("TimeWindow",
                                        new XElement("ValidFrom", validFrom.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                        new XElement("ValidUntil", validUntil.ToString("yyyy-MM-ddTHH:mm:sszzz"))

                                    )//, new XElement("LineFilter", new XElement("LineID", "1A"))
                                )
                            )
                        );

            log.AppendLine(Environment.NewLine + "################################################################");
            log.AppendLine("Subscription Starts: " + requestDoc.ToString());

            //AppUtility.Log2File("Hacon", "Subscription Starts: " + requestDoc.ToString(), true);

            Uri requestUri = new Uri(serverAddress, HCustConfig.SubscriptionUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            log.AppendLine("Subscription Starts Response: " + replyDoc.ToString());

            //AppUtility.Log2File("Hacon", "Subscription Starts Response: " + replyDoc.ToString(), true);

            AppUtility.Log2File("Hacon", log.ToString(), true);
        }


        public bool PollData()
        {
            //string result = string.Empty;
            bool result = false;

            XDocument requestDoc = new XDocument(
                 new XElement("DataSupplyRequest",
                     new XAttribute("Sender", HCustConfig.HaConName),
                     new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                     new XElement("AllData", DataReadyStatus ? "false" : "true")
                 )
             );

            AppUtility.Log2File("Hacon", "DataSupplyRequest : " + requestDoc.ToString());

            DataReadyStatus = true;

            Uri requestUri = new Uri(serverAddress, HCustConfig.PollDataUrl);

            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            if (String.IsNullOrEmpty(replyDoc.ToString()))
            {
                AppUtility.Log2File("Hacon", "No data returned with Data Supply Request", true);
                return false;
            }

            //import Future Journeys
            HaconDBController.HCustConfig = HCustConfig;
            HaconDBController.ProcessFutureJourneys(CustomerId, replyDoc);
             
            //replyDoc.Save(Path.Combine(AppSettings.GetResourceDirectory(), "Hafas\\data_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml"));

            AppUtility.Log2File("Hacon", "DataSupplyRequest: " + replyDoc.ToString().Length + " bytes received", true);
            //this.txtDataSupplyOutput.Text = "Data Received: " + replyDoc.ToString().Length + " bytes received" + Environment.NewLine + "D:\\Data\\IIS\\mermaid.dk\\test01\\Hafas\\App_Data\\data_" + DateTime.Now.ToString("yyyyyMMddHHmmss") + ".xml";

            string pendingData = replyDoc.Root.Element("PendingData").Value;

            AppUtility.Log2File("Hacon", "PendingData value : " + pendingData, true);

            if (pendingData == "false")
            {
                AppUtility.Log2File("Hacon", "Pending Data Ends ... call for delete subscription", true);

                //DeleteSubscription();
                result = false;
            }
            else
            {
                //result = DataReadyAnswer();
                result = true;
            }


            return result;
        }

        private void ExtractFutureJourneys(XDocument replyDoc)
        {
            throw new NotImplementedException();
        }

        public void DeleteSubscription()
        {
            //return;
            /*
             <?xml version="1.0" encoding="iso-8859-15"?>
<SubscriptionRequest Sender="Test" TimeStamp="2014-07-18T12:24:58+02:00" >
  <DeleteSubscriptionsAll>true</DeleteSubscriptionsAll>
</SubscriptionRequest>
             */
            StringBuilder log = new StringBuilder();

            DataReadyStatus = false;

            XDocument requestDoc = new XDocument(
                            new XElement("SubscriptionRequest",
                                new XAttribute("Sender", HCustConfig.HaConName),
                                new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                new XElement("DeleteSubscriptionsAll", "true")
                            )
                        );


            Uri requestUri = new Uri(serverAddress, HCustConfig.SubscriptionUrl);


            XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            log.AppendLine("DeleteSubsription Response : " + replyDoc.ToString());
            log.AppendLine("################################################################");
            AppUtility.Log2File("Hacon", log.ToString(), true);
        }

        private string DataReadyAnswer()
        {
            return string.Format("<?xml version=\"1.0\" encoding=\"iso-8859-15\"?><DataReadyAnswer ><Acknowledge TimeStamp=\"{0}\" Result=\"ok\" ErrorNumber=\"0\"></Acknowledge></DataReadyAnswer>", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"));

             
            //XDocument requestDoc = new XDocument(
            //                           new XElement("DataReadyAnswer",
            //                               new XElement("Acknowledge",
            //                               new XAttribute("TimeStamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz")),
            //                               new XAttribute("Result", "ok"),
            //                               new XAttribute("ErrorNumber", "0")
            //                               )
            //                           )
            //                       );


            //Uri requestUri = new Uri(serverAddress, "Mermaid/sisref/status.xml");

            //XDocument replyDoc = this.PerformRequest(requestUri, requestDoc);

            //Logger.Log("DataReadyAnswer Response : " + requestDoc.ToString());

            //return requestDoc.ToString(); //replyDoc.ToString();
        }

     
    }


 


}
