﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ping
{
    public partial class frmMessagePing : Form
    {
        public frmMessagePing()
        {
            InitializeComponent();
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            DoPing(pingData.Text);
        }


        public void DoPing(string pingData)
        {
            try
            {

                IBIService.IBIServiceClient client = new IBIService.IBIServiceClient();
                errors.Text = client.MessagePing(pingData);
                MessageBox.Show("Pinged successfully without errors");
            }
            catch (Exception ex)
            {
                errors.Text = ex.Message;
                errors.Text += ex.StackTrace;
                MessageBox.Show("Ping Failed");
            }

        }


        private void frmPing_Load(object sender, EventArgs e)
        {

        }
    }
}
